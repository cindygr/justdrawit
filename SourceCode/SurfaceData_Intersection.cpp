/*
 *  SurfaceData_Intersection.cpp
 *  SketchCurves
 *
 *  Created by Cindy Grimm on 2/24/11.
 *  Copyright 2011 Adobe corp. All rights reserved.
 *
 */

#include "SurfaceData.h"
#include "PointConstraint.h"
#include <fitting/FITTools_SVD.H>


const bool s_bTrace = false;

/* Try all pair-wise combinations. Average the points that are the average of all pairs of curves.
 * Could probably do an optimization instead */
void SurfaceData::IntersectionPoint::FindBestIntersectionPoint( const CurveNetwork &in_crvNetwork )
{
	std::vector<R3Pt> aptAvg;
    std::vector<int> aiCrvIndex( m_acrvLocs.size() );

    for ( int i = 0; i < m_acrvLocs.size(); i++ ) {
        aiCrvIndex[i] = GetCurveGroupIndexFromHashId( in_crvNetwork, i );
    }
    
	for ( int i = 0; i < m_acrvLocs.size(); i++ ) {
        if ( aiCrvIndex[i] == -1 ) {
            continue;
        }
		const Curve &crv = in_crvNetwork.GetCurve( aiCrvIndex[i] );
		const std::pair<int,double> iSegApprox = crv.BinarySearch( m_acrvLocs[i].second );

		for ( int j = i+1; j < m_acrvLocs.size(); j++ ) {			
            if ( aiCrvIndex[j] == -1 ) {
                continue;
            }
			const Curve &crvCross = in_crvNetwork.GetCurve( aiCrvIndex[j] );
			const std::pair<int,double> iSegApproxCross = crvCross.BinarySearch( m_acrvLocs[j].second );

			double dBest = 1e30, dS, dT;
			R3Pt ptBest, ptOut;
			// Look for closest point between two segments of the curves
			for ( int iPt1 = WINmax(0, iSegApprox.first - 2); iPt1 < WINmin( crv.NumPts() - 1, iSegApprox.first + 2); iPt1++ ) {
				const R3Line_seg seg1( crv.Pt(iPt1), crv.Pt( iPt1+1 ) );
			
				for ( int iPt2 = WINmax(0, iSegApproxCross.first - 2); iPt2 < WINmin( crvCross.NumPts() - 1, iSegApproxCross.first + 2); iPt2++ ) {
					const R3Line_seg seg2( crvCross.Pt(iPt2), crvCross.Pt( iPt2+1 ) );
					
					seg1.Intersect(seg2, ptOut, dS, dT);
					const double dDist = ::Length( seg1(dS) - seg2(dT) );
					if ( dDist < dBest ) {
						dBest = dDist;
						if ( iSegApprox.first == 0 || iSegApprox.first == crv.NumPts() - 1 ) {
							ptBest = seg2(dT);
						} else if ( iSegApproxCross.first == 0 || iSegApproxCross.first == crvCross.NumPts() - 1 ) {
							ptBest = seg1(dS);
						} else {
							ptBest = Lerp( seg1(dS), seg2(dT), 0.5 );
						}
							
					}
				}
			}
			aptAvg.push_back( ptBest );
		}
	}
	
	R3Pt ptAvg(0,0,0);
	for (int i = 0; i < aptAvg.size(); i++ ) {
		for ( int j = 0; j < 3; j++ ) {
			ptAvg[j] += aptAvg[i][j];
		}
	}
	
	if ( aptAvg.size() ) {
		for ( int j = 0; j < 3; j++ ) {
			ptAvg[j] /= (double) aptAvg.size();
		}
	}
		
	// Store the maximum distance from any curve to the average point
	m_pt = ptAvg;
	m_dDistance = 0.0;
	for ( int i = 0; i < m_acrvLocs.size(); i++ ) {
        if ( aiCrvIndex[i] != -1 ) {
            m_acrvLocs[i].second = in_crvNetwork.GetCurve( aiCrvIndex[i] ).ClosestPointTValue( m_pt );
            m_dDistance = WINmax( m_dDistance, ::Length( in_crvNetwork.GetCurve( aiCrvIndex[i] )( m_acrvLocs[i].second ) - m_pt ) );
        }
	}
}

/* Just see if we have a point constraint in place or not.
 * This will flag any intersection that is the same, but no point constraints */
void SurfaceData::IntersectionPoint::SetNeedsFixing( const CurveNetwork &in_crvNetwork, const double in_dDist )
{
	m_bNeedsFixing = false;
	
	vector< const PointConstraint * > aopPins( m_acrvLocs.size() );

	for ( int i = 0; i < m_acrvLocs.size(); i++ ) {
		const int iCurve = GetCurveGroupIndexFromHashId( in_crvNetwork, i );
		
		if ( iCurve == -1 ) {
            aopPins[i] = NULL;
			continue;
		}
		const CurveGroup &crvGrp = in_crvNetwork.GetCurveGroup( iCurve );
        aopPins[i] = crvGrp.HasConstraintNoEdit( m_acrvLocs[i].second );

		if ( aopPins[i] == NULL ) {
			m_bNeedsFixing = true;
		}
	}	

	if ( m_bNeedsFixing == false ) {
        for ( int i = 0; i < aopPins.size(); i++ ) {
            for ( int j = i+1; j < aopPins.size(); j++ ) {
                if ( aopPins[i] != aopPins[j] ) {
                    m_bNeedsFixing = true;
                }
            }
        }
    }
    
	if ( IsClosed( in_crvNetwork ) ) {
        m_bNeedsFixing = false;
	}
}


/* Set the points for the curves */
void SurfaceData::IntersectionPoint::SetPerCurvePt( const CurveNetwork &in_crvNetwork )
{
	m_aptPerCurve.resize( m_acrvLocs.size() );
	for ( int i = 0; i < m_acrvLocs.size(); i++ ) {
		const int iCurve = GetCurveGroupIndexFromHashId( in_crvNetwork, i );
        if ( iCurve != -1 ) {
            const Curve &crv = in_crvNetwork.GetCurve( iCurve );
            m_aptPerCurve[i] = crv( m_acrvLocs[i].second );
        }
	}
}

/* Only valid if curve ids valid and distance from point to evaluated points still within dist and
 * any stored point/normal constraints still match up/exist */
bool SurfaceData::IntersectionPoint::IsValid( const CurveNetwork &in_crvNetwork, const double in_dDist ) const
{
	for ( int i = 0; i < m_acrvLocs.size(); i++ ) {
		const int iCurve = GetCurveGroupIndexFromHashId( in_crvNetwork, i );
        
		if ( iCurve == -1 ) {
			return false;
		} else {
			const Curve &crv = in_crvNetwork.GetCurve( iCurve );
			if ( ::Length( m_pt - crv( m_acrvLocs[i].second ) ) > in_dDist ) {
				return false;
			}
		}
	}

	if ( IsClosed( in_crvNetwork ) ) {
        return false;
	}
	return true;
}

bool SurfaceData::IntersectionPoint::IsClosed( const CurveNetwork &in_crvNetwork ) const
{
	if ( m_acrvLocs.size() != 2 ) {
        return false;
    }
    
    if ( m_acrvLocs[0].first != m_acrvLocs[1].first ) {
        return false;
    }
    const int iCurve = GetCurveGroupIndexFromHashId( in_crvNetwork, 0 );
    const bool bIsEnd1 = RNIsZero( m_acrvLocs[0].second ) || RNApproxEqual( m_acrvLocs[0].second, 1.0 );
    const bool bIsEnd2 = RNIsZero( m_acrvLocs[1].second ) || RNApproxEqual( m_acrvLocs[1].second, 1.0 );
    if ( bIsEnd1 && bIsEnd2 ) {
        if ( in_crvNetwork.GetCurve( iCurve ).IsClosed() ) {
            return true;
        }
    }
    return false;
}

/* For searching for possible intersections. Basically a light-weight version of IntersectionPoint */
class ClosestPoint
{
public:
    R3Pt m_ptAverage; // Averaged point
    std::vector< std::pair<int,int> > m_aacrvPts; // curve and point index
    double m_dDistance; // Minimum distance between average and curve points
        
    void SetAverageAndDistance( const CurveNetwork &in_crvNetwork ); // Set average point and m_dDistance
    void AddClosestCurvePoints( const CurveNetwork &in_crvNetwork, const double in_dDist ); // Find any points in the curve network that are within dist of m_ptAverage
	
    ClosestPoint &operator=( const ClosestPoint &in_pt ) { m_ptAverage = in_pt.m_ptAverage; m_aacrvPts = in_pt.m_aacrvPts; m_dDistance = in_pt.m_dDistance; return *this; }
    ClosestPoint( const ClosestPoint &in_pt ) { *this = in_pt; }
    ClosestPoint() {}
    ~ClosestPoint() { }
};

/* Average the curve points, then set distance to be the minimum distance between any pair of points
 * This sets the distance of this one to be zero if two points match on two curves, even if 
 * there are some other points that don't agree */
void ClosestPoint::SetAverageAndDistance( const CurveNetwork &in_crvNetwork )
{
    m_ptAverage = R3Pt(0,0,0);
    std::vector< R3Pt > apt( m_aacrvPts.size() );
    for ( int i = 0; i < apt.size(); i++ ) {
        apt[i] = in_crvNetwork.GetCurve( m_aacrvPts[i].first ).Pt( m_aacrvPts[i].second );
    }
    
    m_dDistance = 1e30;
    R3Pt ptAvg(0,0,0);
    for ( int i = 0; i < apt.size(); i++ ) {
        for ( int j = i+1; j < apt.size(); j++ ) {
            const double dLen = ::Length( apt[i] - apt[j] );
            if ( dLen < m_dDistance ) {
                m_ptAverage = Lerp( apt[i], apt[j], 0.5 );
                m_dDistance = dLen;
            }
        }
        for ( int j = 0; j < 3; j++ ) {
            ptAvg[j] += apt[i][j];
        }
    }
    if ( !RNIsZero( m_dDistance ) && apt.size() ) {
        for ( int j = 0; j < 3; j++ ) {
            m_ptAverage[j] = ptAvg[j] / (double) apt.size();
        }
    }
}

/*
 * Search through the entire set of curves looking for any close points
 */
void ClosestPoint::AddClosestCurvePoints( const CurveNetwork &in_crvNetwork, const double in_dDist )
{
    std::vector< std::pair<double,double> > adData;
    boost::dynamic_bitset<> abTry;
    m_aacrvPts.resize(0);
    for ( int iC = 0; iC < in_crvNetwork.NumCrvs(); iC++ ) {
        const Curve &crv = in_crvNetwork.GetCurve(iC);
        const std::pair<R3Pt,R3Pt> &bbox = crv.BoundingBox();
        
		// Ignore any curves that are too far away
        bool bIsInside = true;
        for ( int i = 0; i < 3; i++ ) {
            if ( m_ptAverage[i] < bbox.first[i] - in_dDist ) bIsInside = false;
            if ( m_ptAverage[i] > bbox.second[i] + in_dDist ) bIsInside = false;
        }
        if ( bIsInside == false ) {
            continue;
        }
        
		/* Look at all the points on the curve
		 * Save the percentage along and the distance to this point
		 * abTry has all points that might be close enough 
		 */
        abTry.resize( crv.NumPts() );
        abTry.reset();
        adData.resize( crv.NumPts() );
        for ( int i = 0; i < crv.NumPts(); i++ ) {
            adData[i].first = crv.PercAlong(i);
            adData[i].second = ::Length( crv.Pt(i) - m_ptAverage );
            if ( adData[i].second < in_dDist ) {
                abTry.set(i);
            }
        }
		
		/* For each continguous set of points within distance, look for
		 * the one that is closest */
        while ( abTry.count() ) {
            int iMin = abTry.find_first();
            for ( int i = abTry.find_first(); i != -1; i = abTry.find_next(i) ) {
                if ( adData[i].second < adData[iMin].second ) {
                    iMin = i;
                }
            }
            m_aacrvPts.push_back( std::pair<int,int>( iC, iMin ) );
            
            /* Knock out curve points that are adjacent to this one and inside the selection distance */
            const int iSearchNext = crv.IsClosed() ? iMin + crv.NumPts() : crv.NumPts();
            for ( int i = iMin; i >= 0; i-- ) {
                if ( adData[i].second > in_dDist ) {
                    break;
                }
                abTry.reset(i);
            }
            for ( int i = iMin+1; i < iSearchNext; i++ ) {
                const int iNext = i % crv.NumPts();
                if ( adData[iNext].second > in_dDist ) {
                    break;
                }
                abTry.reset(iNext);
            }
        }
    }
	/* Special case - knock out closed curve */
	if ( m_aacrvPts.size() == 2 ) {
        const Curve &crv = in_crvNetwork.GetCurve(m_aacrvPts[0].first);
		if ( m_aacrvPts[0].first == m_aacrvPts[1].first && crv.IsClosed() ) {
			const int iPt1 = m_aacrvPts[0].second;
			const int iPt2 = m_aacrvPts[1].second;
			if ( iPt1 == 0 && iPt2 == crv.NumPts() - 1 ) {
				m_aacrvPts.resize(1);
			} 
			if ( iPt2 == 0 && iPt1 == crv.NumPts() - 1 ) {
				m_aacrvPts.resize(1);
			} 
		}
	}
}

/*
 * This is an optimization approach, sort of k-means like. Generate candidate sets of point intersetions,
 * find all points on the curves that are within dist, calculate a new average, repeat
 */
void SurfaceData::FindIntersections( const CurveNetwork &in_crvNetwork, const double in_dDist )
{
    if ( in_dDist < 1e-16 ) {
        return;
    }
    m_dDistance = in_dDist;
        
    std::vector< ClosestPoint > aptTry, aptTemp;
    ClosestPoint ptAdd;
    for ( int iC = 0; iC < in_crvNetwork.NumCrvs(); iC++ ) {
		/* Walk along each curve (using half distance as the spacing) and add each point to the list */
        const Curve &crv = in_crvNetwork.GetCurve(iC);
        const double dTSpacing = 0.5 * m_dDistance / crv.Length();
        for ( double dT = 0.0; dT <= 1.0; dT += dTSpacing ) {
            ptAdd.m_ptAverage = crv( dT );            
            aptTry.push_back( ptAdd );
        }
    }

    /* Also push any known curve constraints onto the list */
    for ( int i = 0; i < in_crvNetwork.NumConstraints(); i++ ) {
        if ( in_crvNetwork.GetConstraint(i).IsPoint() ) {
            ptAdd.m_ptAverage = in_crvNetwork.GetConstraint(i).GetPt();
            aptTry.push_back( ptAdd );
        }
    }
    
    bool bChanged = false;
	int iLoops = 10;
    do {
		/* For each point in the list (initially a lot) see if there are any close points.
		   If so, add them into aptTry's list of close points */
        for ( int i = 0; i < aptTry.size(); i++ ) {
            aptTry[i].AddClosestCurvePoints( in_crvNetwork, m_dDistance );
            aptTry[i].SetAverageAndDistance( in_crvNetwork );
        }
		
        // copy over unique ones
        bChanged = false;
		aptTemp.resize(0);
        for ( int i = 0; i < aptTry.size(); i++ ) {
			// Look at any point in aptTry that has more than one point
            if ( aptTry[i].m_aacrvPts.size() > 1 ) {
                int iFound = -1;
                for ( int j = 0; j < aptTemp.size(); j++ ) {
                    if ( ::Length( aptTemp[j].m_ptAverage - aptTry[i].m_ptAverage ) < m_dDistance ) {
                        if ( iFound != -1 ) {
                            if ( aptTemp[iFound].m_dDistance < aptTemp[j].m_dDistance ) {
                                iFound = j;
                            }
                        } else {
                            iFound = j;
                        }
                    }
                }
                if ( iFound == -1 ) {
                    aptTemp.push_back( aptTry[i] );
                } else {
                    if ( aptTemp[iFound].m_dDistance > aptTry[i].m_dDistance ) {
                        aptTemp[iFound] = aptTry[i];
                    }
                    bChanged = true;
                }
            }
        }
		aptTry = aptTemp;
        if ( s_bTrace ) cout << "Found " << aptTry.size() << "\n";
    } while( bChanged == true && iLoops-- > 0 );
    
    /* Now copy over to permanant intersections */
    m_aptIntersection.resize(aptTry.size());
    for ( int i = 0; i < aptTry.size(); i++ ) {
        IntersectionPoint &ipt = m_aptIntersection[i];
        ipt.m_acrvLocs.resize( aptTry[i].m_aacrvPts.size() );
        for ( int j = 0; j < aptTry[i].m_aacrvPts.size(); j++ ) {
            ipt.m_acrvLocs[j].first = aptTry[i].m_aacrvPts[j].first;
            ipt.m_acrvLocs[j].second = in_crvNetwork.GetCurve( aptTry[i].m_aacrvPts[j].first ).PercAlong( aptTry[i].m_aacrvPts[j].second );
        }
        ipt.m_dDistance = aptTry[i].m_dDistance;
        if ( RNIsZero( aptTry[i].m_dDistance ) ) {
            ipt.m_pt = aptTry[i].m_ptAverage;
        } else {
            ipt.FindBestIntersectionPoint( in_crvNetwork );
        }
        ipt.SetPerCurvePt( in_crvNetwork );
    }
    ResetTValues( in_crvNetwork );
    RemoveInvalidAndFixed( in_crvNetwork );
}

