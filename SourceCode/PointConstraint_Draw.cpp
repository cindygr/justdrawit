/*
 *  PointConstraint_Draw.cpp
 *  SketchCurves
 *
 *  Created by Cindy Grimm on 9/30/11.
 *  Copyright 2011 Washington University in St. Louis. All rights reserved.
 *
 */

#include "PointConstraint.h"
#include <FL/GL.H>
#include <utils/Utils_Color.H>
#include <utils/Rn_Line.H>
#include <utils/Rn_Sphere.H>
#include "UserInterface.h"
#include <OpenGL/OGLDraw3D.H>

#include "Curve.h"
#include "ParamFile.h"

void PointConstraint::DrawPoint( const bool in_bIsSelected ) const
{
	const double dScl = m_dSclPoint * g_drawState.StripWidth();
	const UTILSColor &col = in_bIsSelected ? m_colPtSelected : m_colPtUnselected;
    
	R3Matrix matRot;
	R4Matrix matFull;

    glColor4f( col[0], col[1], col[2], 1.0f );

    if ( IsNormal() ) {
		glPushMatrix();
		R3Matrix::MatrixVecToVec( matRot, R3Vec(0,1,0), GetNormal() );
		matFull = R4Matrix::Translation( GetPt() - R3Pt(0,0,0) ) * (R4Matrix) matRot;
		glMultMatrixd( &matFull(0,0) );
		OGLDrawCylinder(R3Pt(0,0,0), dScl * 0.5, dScl );
		glPopMatrix();

    } else {
		OGLDrawSphere( R3Sphere( GetPt(), 0.75 * dScl ) );
    }
}

void PointConstraint::DrawNormal( const bool in_bIsSelected ) const
{
	const double dScl = m_dSclNormal * g_drawState.StripWidth();
	const UTILSColor &col = in_bIsSelected ? m_colNormSelected : m_colNormUnselected;
    
	R3Matrix matRot;
	R4Matrix matFull;

    glColor4f( col[0], col[1], col[2], 1.0f );
        
    glPushMatrix();
    R3Matrix::MatrixVecToVec( matRot, R3Vec(0,1,0), GetNormal() );
    matFull = R4Matrix::Translation( GetPt() - R3Pt(0,0,0) ) * (R4Matrix) matRot;
    glMultMatrixd( &matFull(0,0) );
    OGLDrawArrow( R3Pt(0,0,0), dScl * 4.0, dScl * 0.8 );
    glPopMatrix();
}


/// Get geometry for point constraint
void PointConstraint::SphereGeometry( std::vector<R3Pt> &io_aptVs, std::vector<R3Vec> &io_avecVNs, std::vector< R3Pt_i > &io_aptFs ) const
{
    const int iThetaDiv = 32;
    const int iPhiDiv = 16;
    const double dThetaDiv = 2.0 * M_PI / (double) iThetaDiv;
    const double dPhiDiv = M_PI / (double) iPhiDiv;
	const double dScl = m_dSclPoint * g_drawState.StripWidth();

    const R4Matrix matScl = R4Matrix::Scaling( dScl, dScl, dScl, 1.0 );
	int iVertId = io_aptVs.size();
    const R3Vec &vec = GetPt() - R3Pt(0,0,0);;
    
    const int iVertIdNP = iVertId;
    io_aptVs.push_back( matScl * R3Pt(0,0,1.0) + vec );
    io_avecVNs.push_back( R3Vec(0,0,1) );
    iVertId++;
    
    const int iVertIdSP = iVertId;
    io_aptVs.push_back( matScl * R3Pt(0,0,-1.0) + vec );
    io_avecVNs.push_back( R3Vec(0,0,-1) );
    iVertId++;
    
    Array< Array<int> > aaiVIds( iPhiDiv - 1 );

    for ( int iP = 0; iP < iPhiDiv - 1; iP++ ) {
        const double dPhiCos = cos( M_PI / 2.0 + (iP+1) * dPhiDiv );
        const double dPhiSin = sin( M_PI / 2.0 + (iP+1) * dPhiDiv );

        aaiVIds[iP].need( iThetaDiv );
        for (int iT = 0; iT < iThetaDiv; iT++ ) {
            const R3Pt ptSphere( cos( iT * dThetaDiv ) * dPhiCos, -dPhiSin, sin( iT * dThetaDiv ) * dPhiCos );        
            io_aptVs.push_back( matScl * ptSphere + vec );
            io_avecVNs.push_back( ptSphere - R3Pt(0,0,0) );
            aaiVIds[iP][iT] = iVertId;
            iVertId++;
        }
    }
    
    // Top and bottom of sphere
    for (int iT = 0; iT < iThetaDiv; iT++ ) {
        io_aptFs.push_back( R3Pt_i( iVertIdNP, aaiVIds[0].wrap(iT+1), aaiVIds[0][iT] ) );

        for ( int iP = 0; iP < iPhiDiv - 2; iP++ ) {
            io_aptFs.push_back( R3Pt_i( aaiVIds[iP][iT], aaiVIds[iP+1].wrap(iT+1), aaiVIds[iP].wrap(iT+1) ) );
            io_aptFs.push_back( R3Pt_i( aaiVIds[iP][iT], aaiVIds[iP+1][iT], aaiVIds[iP+1].wrap(iT+1) ) );
        }
        
        io_aptFs.push_back( R3Pt_i( iVertIdSP, aaiVIds.last()[iT], aaiVIds.last().wrap(iT+1) ) );
    }
}
