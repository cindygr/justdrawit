/*
 *  SurfaceData.h
 *  SketchCurves
 *
 *  Created by Cindy Grimm on 2/17/11
 *  Copyright 2011 Adobe Corp. All rights reserved.
 *
 */

#ifndef _SURFACE_DATA_DEFS_H
#define _SURFACE_DATA_DEFS_H


#include "CurveNetwork.h"

#include <utils/Polygonizer.H>

/*
 * This class has two functions, which could probably be split up
 *
 * Function 1: find possible intersections between curves and snap them
 * together (SurfaceData_Fix, SurfaceData_Intersections)
 *
 * Function 2: Sample the curves to create the rbf surface (SurfaceData_Sampling, SurfaceData_Surface)
 */
class SurfaceData {
protected:
	/* Store information about an intersection (or possible intersection) point.
	 * Most of these routines are in SurfaceData_Intersection; the others are in
	 * SurfaceData.cpp */
    class IntersectionPoint {
    public:
        bool m_bNeedsFixing; // Is it snapped?
        R3Pt m_pt;           // Actual point if snapped, otherwise averaged point
        std::vector< std::pair<int,double> > m_acrvLocs; // Curve id and t value
		std::vector< R3Pt > m_aptPerCurve; // Evaluated point on curve
        double m_dDistance; // How far apart are the points?
        
		// Sort by distance (want to merge/keep intersections that are close together)
        bool operator<( const IntersectionPoint &in_pt ) const { return m_dDistance > in_pt.m_dDistance; }
		
		/// Set m_pt
		void FindBestIntersectionPoint( const CurveNetwork &in_crvNetwork );
		/// Look to see if have pin points/points are the same. Finds point/normal constraints if they exist
		void SetNeedsFixing( const CurveNetwork &in_crvNetwork, const double in_dDist );
		///  sets m_aptPerCurve
		void SetPerCurvePt( const CurveNetwork &in_crvNetwork );

		/// Reset all lists to zero
        void Clear();
        
        ///
        int GetCurveGroupIndexFromHashId( const CurveNetwork &in_crvNetwork, const int in_iWhich ) const;
        
		/// Check if a) curve ids still exist and b) curve(t) is still close to m_pt
		bool IsValid( const CurveNetwork &in_crvNetwork, const double in_dDist ) const;
        
        /// See if this represents a closed curve
        bool IsClosed( const CurveNetwork &in_crvNetwork ) const;
		
        void Read(ifstream &in);
        void Write(ofstream &out) const;
		void Print(const CurveNetwork &in_crvNetwork) const;
		
		IntersectionPoint &operator=( const IntersectionPoint &in_pt );
		bool operator==( const IntersectionPoint &in_pt );
        
        IntersectionPoint() ;
		IntersectionPoint( const IntersectionPoint & in_pt ) { *this = in_pt; }
        ~IntersectionPoint() ;
    };
    
    static SurfaceData *s_srfData;

	/* Save the potential and actual intersections and the distance used to find them */
    double m_dDistance;
    std::vector<IntersectionPoint> m_aptIntersection;
	
	/* For generating the rbf matrix. These are constraints for the rbf, not the curves */
    std::vector<R3Ray>        m_arayNormalConstraints; // Point and normal
    std::vector<R3Ray>        m_arayTangentConstraints; // Point and tangent
    std::vector<R3Pt>         m_aptPointConstraints;
	
	/* For generating the surface */
	R3Pt m_ptMin, m_ptMax;
	std::vector<R3Pt>     m_aptSurface;
	std::vector<R3Vec>    m_avecSurface;
	std::vector< R3Pt_i > m_afaceSurface;
	
	/* SurfaceData_Fix.cpp */
	/// If the curves snap together and just a tiny bit dangles off, delete that end
    bool ClipEnds( CurveNetwork &io_crvNetwork, const int in_iIntersection );
	/// Reset the t vaues in case the curves changed slightly
	void ResetTValues( const CurveNetwork &in_crvNetwork  );
	/// Actually fix the potential intersection
	void EstablishCurveConstraints( CurveNetwork &io_crvNetwork, const int in_iIntersection );

	/* These are for generating the rbf constraints (SurfaceData_Sampling.cpp) */
	/// Is there already a constraint that is close to this one?
	bool AlreadyIncluded( const R3Pt &in_pt, const double in_dDist ) const;
	/// Add normal constraints from curves, then add one more between them
	void AddNormalConstraints(const CurveNetwork &in_crvNetwork );
	/// Curvature based - add tangent constraints whenever the curve bends too much
	void AddTangentConstraints( const CurveNetwork &in_crvNetwork );
	/// Curvature and distance based
	void AddPointConstraints( const CurveNetwork &in_crvNetwork );
	

	static int TriProc( int in_i1, int in_i2, int in_i3, VERTICES vs );
	static void VertProc( VERTICES vs );

public:
	/**@name Access */
	//@{
	///
	bool IsValidIntersection( const int in_i ) const { return in_i >= 0 && in_i < m_aptIntersection.size(); }
	/// Average point
	R3Pt IntersectionPt( const int in_iIntersection ) const;
	//@}
	
	/**@name Selection */
	//@{
	/// -1 if none
    int ClosestIntersection( const R3Ray &in_ray ) const;
	//@}
    
	/**@name Find and fix intersections */
	//@{
	/// Search for all valid and potential intersections that are dist apart
    void FindIntersections( const CurveNetwork &in_crvNetwork, const double in_dDist );
	/// Fix an individual one
    void FixIntersection( CurveNetwork &io_crvNetwork, const int in_iIntersection );
	/// Fix them all
	void FixAllIntersections(  CurveNetwork &io_crvNetwork );
	/// Clear them all
	void Clear() ;
	/// Look to see if any of them have become invalid because of curve edits/changes/deletes
	void RemoveInvalidAndFixed( const CurveNetwork &in_crvNetwork );
	//@}
	
	/**@name building the surface */
	//@{
	/// The point/normal/tangent constraints
	void CalcConstraints( const CurveNetwork &in_crvNetwork );
	/// Run polygonizer on the rbf surface
	void BuildSurface(  );
	/// Flip orientation
	void FlipSurface();	
	/// Flip normals
	void FlipNormals();	
	/// Smooth surface
	void SmoothSurface();	
	/// Do the normals from the surface
	void CalcNormalsForSurface();	
	//@}
    
	
	
	SurfaceData &operator=( const SurfaceData &in_srf );
	SurfaceData( const SurfaceData & in_srf ) { *this = in_srf; }
    SurfaceData();
    ~SurfaceData();
    
    void Draw( const int in_iSelected ) const;
	virtual void DrawConstraints( ) const;
	void DrawSurface( ) const;
    
    void Read(ifstream &in);
    void ReadSurface( ifstream &in );

    void Write(ofstream &out) const;
	void WriteSurface( ofstream &out ) const;
	void WriteSTLSurface( const char *in_str ) const;
	void WriteOBJSurface( Array<R3Pt> &io_apt, Array<R3Pt_i> &io_aface, 
                          ofstream &outObj, ofstream &outMtl, const int in_iWhich ) const;
};

#endif
