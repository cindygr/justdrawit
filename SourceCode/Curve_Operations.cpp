/*
 *  Curve_Operations.cpp
 *  SketchCurves
 *
 *  Created by Cindy Grimm on 12/30/10.
 *  Copyright 2010 Washington University in St. Louis. All rights reserved.
 *
 */

#include "Curve.h"
#include <fitting/FITTools_SVD.H>
#include <utils/S4_quaternion.H>
#include "ParamFile.h"

const bool s_bTrace = false;

/*
 * clear all derived data and all data
 */
void Curve::Clear()
{
    m_apt.need(0);
    m_avecTangent.need(0);
    m_adDist.need(0);
    m_adPercAlong.need(0);
    
    m_bPercData = true;
    m_bTangentData = true;
}

/*
 * Add in the point. Makes space in the other data structures, but the
 * only one it actually sets is m_adDist and m_adTimeStamp
 */
void Curve::AddPoint( const R3Pt &in_pt )
{
    // Don't add duplicate points
    if ( m_apt.num() && ApproxEqual( in_pt, m_apt.last() ) )
        return;
    
    m_apt += in_pt;
	
	m_adDist += 0.0;
	if ( m_adDist.num() != 1 ) {
		m_adDist[ m_apt.num() - 2 ] = ::Length( m_apt.last() - m_apt[ m_apt.num() - 2 ] );
		m_adDist.last() += m_adDist[ m_apt.num() - 2 ];
	}
    m_adPercAlong += 0.0;
    m_avecTangent += R3Vec(0,0,1);
	
    m_bPercData = false;
    m_bTangentData = false;
}

void Curve::DeleteLastPoint()
{
    m_apt.sub(1);
    m_adDist.sub(1);
    m_adPercAlong.sub(1);
    m_avecTangent.sub(1);
}

/*
 * Split the segment if necessary. Returns the index of the point .
 *   If the current t value point is within PercScreen of a segment end point, just
 *   return that segment end point
 */
/// Insert a point in the segment, if necessary. 
int Curve::SplitSegment( const double in_dT, const double in_dPercScreen )
{
    const std::pair<int,double> ipt = BinarySearch(in_dT);

    if ( RNIsZero( ipt.second ) ) {
        return ipt.first;
    }
    
    if ( RNApproxEqual( ipt.second, 1.0 ) ) {
        return ipt.first + 1;
    }

    const double dCloseEnough = g_drawState.SelectionSize() * in_dPercScreen;
    const R3Pt pt = Lerp( m_apt[ipt.first], m_apt.clamp(ipt.first + 1), ipt.second );
    const double dDistPrev = ::Length( Pt( ipt.first ) - pt );
    
    if ( dDistPrev < dCloseEnough ) {
        m_apt[ipt.first] = (*this)(in_dT);
        return ipt.first;
    }
    
    const double dDistNext = ::Length( Pt( ipt.first + 1 ) - pt );
    
    if ( dDistNext < dCloseEnough ) {
        m_apt[ipt.first+1] = (*this)(in_dT);
        return ipt.first + 1;
    }
    
    m_apt.addItem( pt, ipt.first+1 );
    m_adPercAlong.addItem( m_adPercAlong[ipt.first] * (1.0 - ipt.second) + m_adPercAlong[ipt.first+1] * ipt.second, ipt.first+1 );
    m_adDist.addItem( m_adDist[ipt.first] * (1.0 - ipt.second) + m_adDist[ipt.first+1] * ipt.second, ipt.first+1 );
    m_avecTangent.addItem( m_avecTangent[ipt.first], ipt.first+1 );
    
    return ipt.first+1;
}

/*
 * Set m_adPercAlong
 * Also resets m_adDist
 */
void Curve::SetDistanceData()
{
    if ( m_bPercData == true ) {
        ASSERT( m_adPercAlong.num() == m_apt.num() );
        return;
    }
    
    double dSumLen = 0.0;
    
    if ( m_apt.num() == 0 ) {
        return;
    }
	m_adDist.need( m_apt.num() );
	m_adPercAlong.need( m_apt.num() );
	
    for ( int i = 0; i < m_apt.num() - 1; i++ ) {
        m_adDist[i] = ::Length( m_apt[i+1] - m_apt[i] );
        dSumLen += m_adDist[i];
    }
    
    m_adDist.last() = dSumLen;
    double dSum = 0.0;
    for ( int i = 0; i < m_adPercAlong.num(); i++ ) {
        m_adPercAlong[i] = dSum / dSumLen;
        dSum += m_adDist[i];
    }

    m_bbox = CalcBoundingBox();

    m_bPercData = true;
}

/*
 * Tangents are set as you would expect (difference of points)
 */
void Curve::SetTangentData( )
{
    if ( m_bTangentData == true ) {
        ASSERT( m_avecTangent.num() == m_apt.num() );
        return;
    }
    
    m_bTangentData = true;

	m_avecTangent.need( m_apt.num() );
	
    if ( m_apt.num() == 0 ) {
        return;
    } else if ( m_apt.num() == 1 ) {
        m_avecTangent[0] = R3Vec(1,0,0);
        return;
    }
    
    for ( int i = 0; i < m_avecTangent.num() - 1; i++ ) {
        m_avecTangent[i] = UnitSafe( m_apt[i+1] - m_apt[i] );
        int iOffset = 2;
        while (RNIsZero( LengthSq( m_avecTangent[i] ) ) && i+iOffset < m_avecTangent.num() ) {
            m_avecTangent[i] = UnitSafe( m_apt[i+iOffset] - m_apt[i] );
            iOffset++;
        }
        if ( RNIsZero( LengthSq( m_avecTangent[i] ) ) ) {
            m_avecTangent[i] = R3Vec(1,0,0);
        }
    }
    
    if ( IsClosed() ) {
        m_avecTangent[0] = UnitSafe( m_apt.wrap(1) - m_apt.last() );
        m_avecTangent.last() = m_avecTangent[0];
    } else {
        m_avecTangent.last() = m_avecTangent[ m_avecTangent.num() - 2 ];
    }
    
}	

/*
 * Call whenever data changes
 */
void Curve::SetAllData(  )
{
	SetDistanceData(); 
	SetTangentData(  ); 
}

/*
 * Erase the curve from start to end
 * Return the 0,start part, keep the end,1.0 part here
 * Bit complicated with closed curves - in this case this
 * curve gets everything that isn't between start and stop
 */
Curve Curve::Erase( const double in_dTStart, const double in_dTEnd )
{
	double dTStart = in_dTStart;
	double dTEnd = in_dTEnd;
	
	// In case t values have wrapped for closed curve
	if ( dTEnd > 1.0 ) dTEnd = dTEnd - 1.0;
	if ( dTStart < 0.0 ) dTStart = dTStart + 1.0;
	
	if ( in_dTEnd > 1.0 ) dTEnd = dTEnd + 1.0;
	if ( in_dTStart < 0.0 ) dTStart = dTStart - 1.0;
	
	Curve crvLeftHalf;
	
	const Array<R3Pt> apt = m_apt;
	const Array<double> adPerc = m_adPercAlong;
	
	const bool bIsClosed = IsClosed();
	if ( bIsClosed ) {
		const std::pair<int,double> iptEnd = BinarySearch( dTEnd < 1.0 ? dTEnd : dTEnd - 1.0);
		const int iEnd = RNIsZero( iptEnd.second ) ? iptEnd.first : iptEnd.first + 1;
		const std::pair<int,double> iptStart = BinarySearch( dTStart < 0.0 ? dTStart + 1.0 : dTStart );
		int iStart = RNIsZero( iptStart.second ) ? iptStart.first : iptStart.first + 1;
		if ( iStart < iEnd ) iStart += m_apt.num();
		
		Clear();
		for ( int i = iEnd; i < iStart; i++ ) {
			AddPoint( apt.wrap(i) );
		}
	} else {
		Clear();
		
		int iStartMe = -1;
		for ( int i = 0; i < apt.num(); i++ ) {
			if ( adPerc[i] <= dTStart ) {
				crvLeftHalf.AddPoint( apt[i] );
			}
			if ( adPerc[i] >= dTEnd ) {
				AddPoint( apt[i] );
				if ( iStartMe == -1 ) {
					iStartMe = i;
				}
			}
		}
        crvLeftHalf.SetAllData();
	}
	SetAllData();
    return crvLeftHalf;
}

/*
 * Force a curve closed by dragging the ends together
 */
void Curve::Close( const double in_dTBracketDragStart, const double in_dTBracketDragEnd )
{
	const Curve crvOrig = *this;
	
	const R3Pt ptClose = Lerp( m_apt[0], m_apt.last(), 0.5 );
	const R3Vec vecMoveStart = ptClose - m_apt[0];
	const R3Vec vecMoveEnd = ptClose - m_apt.last();
	const R3Plane plane( ptClose, vecMoveEnd.Perpendicular() );
	
	const std::pair<double,double> adTBracketStart( 0.0, in_dTBracketDragStart );
	Drag( plane, vecMoveStart, 0.0, adTBracketStart );
	
	const std::pair<double,double> adTBracketEnd( in_dTBracketDragEnd, 1.0 );
	Drag( plane, vecMoveEnd, 1.0, adTBracketEnd );
	m_apt[0] = ptClose;
	m_apt.last() = ptClose;

	m_bPercData = false;
    m_bTangentData = false;
	SetAllData();
}

/*
 * Helper function - fill in any gaps in the sampling
 * Typically 1.75 * average segment length
 */
void Curve::FillGaps( const double in_dGapSize )
{
    for ( int i = 0; i < m_apt.num() - 1; i++ ) {
        double dLen = ::Length( m_apt[i+1] - m_apt[i] );
        while ( dLen > in_dGapSize ) {
            m_apt.addItem( Lerp( m_apt[i], m_apt[i+1], 0.5 ), i+1 );

        }
    }
    
    m_bPercData = false;
    m_bTangentData = false;
    
	SetAllData();
}

/* Smooth points between start and stop. Loop n times */
void Curve::SmoothPoints( const double in_dTStart, const double in_dTEnd, const int in_iNLoops )
{
    static Array<R3Pt> apt;
	
    if ( !m_bPercData ) {
        SetDistanceData();
    }
	
	const std::pair<int,double> iptStart = BinarySearch( in_dTStart );
	const std::pair<int,double> iptEnd = BinarySearch( in_dTEnd );
    
    if ( !IsClosed() ) {
        const int iStart = WINmax(1, ( iptStart.second > 0.0 ) ? iptStart.first + 1 : iptStart.first );
        const int iEnd = WINmin( ( iptEnd.second < 1.0 ) ? iptEnd.first + 1 : iptEnd.first + 2, m_apt.num() - 1 );
        for ( int iLoop = 0; iLoop < in_iNLoops; iLoop++ ) {
            apt = m_apt;
            for ( int i = iStart; i < iEnd; i++ ) {
                m_apt[i] = Lerp( apt[i], Lerp( apt[i-1], apt[i+1], 0.5 ), 0.2 );
            }
        }
    } else {
        const int iStart = ( iptStart.second > 0.0 ) ? iptStart.first + 1 : iptStart.first;
        const int iEnd = ( iptEnd.second < 1.0 ) ? iptEnd.first + 1 : iptEnd.first;

        m_apt.sub(1);
        for ( int iLoop = 0; iLoop < in_iNLoops; iLoop++ ) {
            apt = m_apt;
            for ( int i = iStart; i < iEnd; i++ ) {
                m_apt.wrap(i) = Lerp( apt.wrap(i), Lerp( apt.wrap(i-1), apt.wrap(i+1), 0.5 ), 0.2 );
            }
        }
        const R3Pt ptFirst = m_apt[0];
        m_apt += ptFirst;
    }
    m_bTangentData = false;
    m_bPercData = false;
    SetAllData();
}

/* Legacy. Smooth points and normals between start and stop. Loop n times */
void Curve::SmoothTangents( const double in_dTStart, const double in_dTEnd, const int in_iNLoops )
{
    static Array<R3Pt> apt;
    static Array<R3Vec> avecT;
	
    SetDistanceData();
    SetTangentData();
	
	const std::pair<int,double> iptStart = BinarySearch( in_dTStart );
	const std::pair<int,double> iptEnd = BinarySearch( in_dTEnd );
    const int iStart = WINmax(1, ( iptStart.second < 1.0 ) ? iptStart.first + 1 : iptStart.first );
	const int iEnd = WINmin( ( iptEnd.second < 1.0 ) ? iptEnd.first + 1 : iptEnd.first, m_apt.num() - 1 );
	
	for ( int iLoop = 0; iLoop < in_iNLoops; iLoop++ ) {
		apt = m_apt;
		avecT = m_avecTangent;
		for ( int i = iStart; i < iEnd; i++ ) {
			m_apt[i] = Lerp( apt[i], Lerp( apt[i-1], apt[i+1], 0.5 ), 0.2 );
            m_avecTangent[i] = UnitSafe( avecT.clamp(i-1) * 0.25 + avecT.clamp(i+1) * 0.25 + avecT[i] * 0.5 );
		}
	}
    m_bPercData = false;
    m_bTangentData = false;
    SetAllData();
}



Curve Curve::MakePolygon() const
{
	/// Find the corners (legacy)
    const double dAvg = g_paramFile.GetDouble( "CornerSize" );

    const boost::dynamic_bitset<> abCorners = Corners( dAvg * AverageSegLength() );
    
    Curve crvRet;
    for ( int i = abCorners.find_first(); i != -1; i = abCorners.find_next(i) ) {
        crvRet.AddPoint( Pt(i) );
    }
    
    return crvRet;
}




