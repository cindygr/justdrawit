/*
 *  Instructions.cpp
 *  SketchCurves
 *
 *  Created by Cindy Grimm on 4/4/12.
 *  Copyright 2012 Washington University in St. Louis. All rights reserved.
 *
 */

#include "Instructions.h"
#include "DrawState.h"
#include "UserInterface.h"
#include <FL/Fl_Shared_Image.H>

static char strOverview[] =
"We've broken the process of creating a 3D surface from sketching into three conceptual stages. The first step is just drawing curves in 2D. The second step adds the complexity of drawing curves in 3D. While this is very similar to 2D drawing, controlling what happens in the third dimension is non-trivial, and often requires additional props in the form of drawing planes and drawing surfaces. The third stage involves cleaning up the curves (making sure that they actually intersect) and adding additional surface information in the form of surface normals for the curves. Obviously, it's possible to go back and forth between the different stages. \n\nSketching in 2D: Basics of drawing strokes, how strokes are accumulated into curves, and some tips and tricks for getting it to do what you want. \nSketching in 3D: Controlling drawing surfaces and the camera. \nCurve networks and surfaces: Snapping curves together and ensuring they're oriented the correct way.";

void Instructions::SetOverviewText()
{
    static Fl_Text_Buffer tb;
    
    if (tb.length() == 0 ) {
        tb.append( strOverview );
    }
    
    g_drawState.m_opUI->m_txtInstructions->wrap_mode( 1, 0 );
    g_drawState.m_opUI->m_txtInstructions->buffer(tb);
    Fl_Box *box = g_drawState.m_opUI->m_boxImage;
    Fl_Shared_Image *opIm = g_drawState.GetImage( DrawState::INSTRUCTIONSOVERVIEW );
    box->image( opIm );
    box->align(FL_ALIGN_INSIDE);
    box->redraw();
}

static char str2DText[] =
"The interface tries to mimic a natural pencil-and-paper feel while still accomplishing the desired goal, which is creating curves. As you sketch, the interface is making guesses about how you want your strokes to interact with existing strokes. It tries to join together strokes into continuous curves when it can. It will, of course, get it wrong sometimes. To change what they system does, click on the circle at the end of the stroke to bring up an in-screen menu.\n\nThere is a button in the upper middle labeled '2D drawing'. This button puts the interface in the default 2D drawing mode, which uses a stroke-like rendering and no additional scene geometry.\n\nTo make strokes, just draw. Strokes are drawn in black, with the resulting curve(s) show in blue over the strokes. The interface will do one of the actions on the right with your strokes.\n";

void Instructions::Set2DText()
{
    static Fl_Text_Buffer tb;
    
    if (tb.length() == 0 ) {
        tb.append( str2DText );
    }
    
    g_drawState.m_opUI->m_txtInstructions->wrap_mode( 1, 0 );
    g_drawState.m_opUI->m_txtInstructions->buffer(tb);
    Fl_Box *box = g_drawState.m_opUI->m_boxImage;
    box->image( g_drawState.GetImage( DrawState::INSTRUCTIONS2D ) );
    box->align(FL_ALIGN_INSIDE);
    box->redraw();
}

static char str2DMenuText[] = 
"If the interface chose the wrong action, you can tell it to do a different one by clicking/tapping on one of the grey circles at the ends of the strokes. This brings up the stroke menu (on right).\n\nThe stroke menu items can be picked by 1) Moving the cursor out of the menu over the desired wedge. 2) Clicking/Tapping in the desired item (wedge or center). 3) Clicking down in the desired item then dragging to select a curve to apply the action to (only valid for some items). To dismiss the menu without doing anything, move the cursor out of the menu between the wedges.\n\nA note on the options that require a selected curve: If you don't select a curve it will use the one you were last editing.\n\nIn addition to these per-stroke edits, you can globally set what actions are available using a few check boxes in the upper middle part of the window.\n\nTo select NO action, simply pass the cursor out between the wedges.";

void Instructions::Set2DMenuText()
{
    static Fl_Text_Buffer tb;
    
    if (tb.length() == 0 ) {
        tb.append( str2DMenuText );
    }
    
    g_drawState.m_opUI->m_txtInstructions->wrap_mode( 1, 0 );
    g_drawState.m_opUI->m_txtInstructions->buffer(tb);
    Fl_Box *box = g_drawState.m_opUI->m_boxImage;
    Fl_Shared_Image *opIm = g_drawState.GetImage( DrawState::INSTRUCTIONS2DMENU );
    box->image( opIm );
    box->align(FL_ALIGN_INSIDE);
    box->redraw();
}

static char str2DCameraText[] =
"There are in-screen controls for rotating the drawing surface, changing it's size, and panning it left and right. There are also standard camera controls (top left): zoom changes the size of the canvas. The in-screen controls are invoked by clicking in the background (shift click will force the camera menu up). Most of these are 3D controls: the ones that are of interest for 2D drawing are the rotate zoom (at 9:00 o'clock) and the pan (at 4:30 o'clock). ";

void Instructions::Set2DCameraText()
{
    static Fl_Text_Buffer tb;
    
    if (tb.length() == 0 ) {
        tb.append( str2DCameraText );
    }
    
    g_drawState.m_opUI->m_txtInstructions->wrap_mode( 1, 0 );
    g_drawState.m_opUI->m_txtInstructions->buffer(tb);
    Fl_Box *box = g_drawState.m_opUI->m_boxImage;
    Fl_Shared_Image *opIm = g_drawState.GetImage( DrawState::INSTRUCTIONS2DCAMERA );
    box->image( opIm );
    box->align(FL_ALIGN_INSIDE);
    box->redraw();
}

static char str2DTips[] = "Tips and Tricks";
void Instructions::Set2DTipsText()
{
    static Fl_Text_Buffer tb;
    
    if (tb.length() == 0 ) {
        tb.append( str2DTips );
    }
    
    g_drawState.m_opUI->m_txtInstructions->wrap_mode( 1, 0 );
    g_drawState.m_opUI->m_txtInstructions->buffer(tb);
    Fl_Box *box = g_drawState.m_opUI->m_boxImage;
    Fl_Shared_Image *opIm = g_drawState.GetImage( DrawState::INSTRUCTIONS2DTIPS );
    box->image( opIm );
    box->align(FL_ALIGN_INSIDE);
    box->redraw();
}

static char str3DCurveMenuText[] = 
"This is the menu for editing curves in a more traditional manner (dragging, rotating, scaling, erasing, smoothing) and for adding point (snap) and normal constraints. The menu is invoked by clicking on a curve. If you're having trouble getting a particular curve, try rotating the camera a bit.\n\nTo select NO action, simply pass the cursor out between the wedges.\n\nTo stop dragging/rotating/scaling, click on the drag icon again.\n\nNote, if the drawing plane is visible, the dragging will happen in the draw plane.\n\n";

void Instructions::Set3DCurveMenuText()
{
    static Fl_Text_Buffer tb;
    
    if (tb.length() == 0 ) {
        tb.append( str3DCurveMenuText );
    }
    
    g_drawState.m_opUI->m_txtInstructions->wrap_mode( 1, 0 );
    g_drawState.m_opUI->m_txtInstructions->buffer(tb);
    Fl_Box *box = g_drawState.m_opUI->m_boxImage;
    Fl_Shared_Image *opIm = g_drawState.GetImage( DrawState::INSTRUCTIONS3DCURVEMENU );
    box->image( opIm );
    box->align(FL_ALIGN_INSIDE);
    box->redraw();
}

static char str3DText[] = "This section assumes you have already read the 2D sketching instructions and have played around with drawing  a bit. The actual sketching part of 3D drawing is pretty much the same as drawing in 2D, but there are some additional techniques to help the system figure out exactly where in space you want curves to go. \n\nSpecifically, drawing from other directions, placing a drawing plane, creating an extrusion surface, and creating an inflation surface. ";

void Instructions::Set3DText()
{
    static Fl_Text_Buffer tb;
    
    if (tb.length() == 0 ) {
        tb.append( str3DText );
    }
    
    g_drawState.m_opUI->m_txtInstructions->wrap_mode( 1, 0 );
    g_drawState.m_opUI->m_txtInstructions->buffer(tb);

    Fl_Box *box = g_drawState.m_opUI->m_boxImage;
    Fl_Shared_Image *opIm = g_drawState.GetImage( DrawState::INSTRUCTIONS3D );
    box->image( opIm );
    box->align(FL_ALIGN_INSIDE);
    box->redraw();
}

static char str3DDrawText[] = "See also the 2D section on drawing in general\n\nIn general, use the drawing plane to create new curves and turn it off when you're sketching over new curves. Use the inflation surface to add contouring curves. \n\nAnother thing you can do is create a sequence of parallel cross sections by moving the draw plane forward in increments. Work from back to front.\n\nThe align option on the curve menu, selecting two curves, is useful for positioning the draw plane so that you can draw a contour perpendicular to the selected curves\n\n";
void Instructions::Set3DDrawText()
{
    static Fl_Text_Buffer tb;
    
    if (tb.length() == 0 ) {
        tb.append( str3DDrawText );
    }
    
    g_drawState.m_opUI->m_txtInstructions->wrap_mode( 1, 0 );
    g_drawState.m_opUI->m_txtInstructions->buffer(tb);
    Fl_Box *box = g_drawState.m_opUI->m_boxImage;
    Fl_Shared_Image *opIm = g_drawState.GetImage( DrawState::INSTRUCTIONS3DDRAW );
    box->image( opIm );
    box->align(FL_ALIGN_INSIDE);
    box->redraw();
}

static char str3DDrawPlaneText[] = "The drawing plane can be positioned in space in a variety of ways; when the drawing plane is visible new curves will be placed on it. By moving both the drawing plane and the viewpoint it is usually possible to find a combination that is comfortable to draw on and where you can see what you're doing.\n\nIf the draw plane is visible drags will move the curve parallel to the drawing plane. Otherwise, drags are parallel to the film plane. ";

void Instructions::Set3DDrawPlaneText()
{
    static Fl_Text_Buffer tb;
    
    if (tb.length() == 0 ) {
        tb.append( str3DDrawPlaneText );
    }
    
    g_drawState.m_opUI->m_txtInstructions->wrap_mode( 1, 0 );
    g_drawState.m_opUI->m_txtInstructions->buffer(tb);
    Fl_Box *box = g_drawState.m_opUI->m_boxImage;
    Fl_Shared_Image *opIm = g_drawState.GetImage( DrawState::INSTRUCTIONS3DDRAWPLANE );
    box->image( opIm );
    box->align(FL_ALIGN_INSIDE);
    box->redraw();
}

static char str3DDrawExtrusionText[] = "An extruded surface is sort of like a drawing plane, except that it is made by taking a curve and extruding it in one direction. When an extrusion surface is visible, drawing on it places the curve on that drawing surface.\n\nTo create an extrusion surface, click on the Extrusion surface button on the left. Click on the curve you want to use. Arrows will appear; pick the direction you want the surface to go in. You can now draw on the surface. To change the extrusion surface, click on a curve. This will bring up the arrows again. To hide the surface, click the Extrusion button again. Note that only one of the drawing plane, extrusion surface, or inflation surface can be visible at a time.\n\n";

void Instructions::Set3DDrawExtrusionText()
{
    static Fl_Text_Buffer tb;
    
    if (tb.length() == 0 ) {
        tb.append( str3DDrawExtrusionText );
    }
    
    g_drawState.m_opUI->m_txtInstructions->wrap_mode( 1, 0 );
    g_drawState.m_opUI->m_txtInstructions->buffer(tb);
    Fl_Box *box = g_drawState.m_opUI->m_boxImage;
    Fl_Shared_Image *opIm = g_drawState.GetImage( DrawState::INSTRUCTIONS3DDRAWEXTRUSION );
    box->image( opIm );
    box->align(FL_ALIGN_INSIDE);
    box->redraw();
}

static char str3DDrawInflationText[] = "An inflation surface is useful for drawing contour curves. It is made from two roughly parallel curves. The surface passes through the curves on the edges and is inflated towards the viewer in the interior. You can then draw on the surface.\n\nTo create an inflation surface, click on the Inflation surface button on the left. Mouse down and draw over one (or more) curves to make the left side of the surface. Do the same thing again on the right side. You now have a surface you can draw on. You can draw as many curves as you like on the surface.\n\nTo start a new surface, you can either turn the inflation surface off and on again using the button on the left, or you can click on the sphere at the top of the first curve you drew. This lets you re-draw that curve.";

void Instructions::Set3DDrawInflationText()
{
    static Fl_Text_Buffer tb;
    
    if (tb.length() == 0 ) {
        tb.append( str3DDrawInflationText );
    }
    
    g_drawState.m_opUI->m_txtInstructions->wrap_mode( 1, 0 );
    g_drawState.m_opUI->m_txtInstructions->buffer(tb);
    Fl_Box *box = g_drawState.m_opUI->m_boxImage;
    Fl_Shared_Image *opIm = g_drawState.GetImage( DrawState::INSTRUCTIONS3DDRAWINFLATION );
    box->image( opIm );
    box->align(FL_ALIGN_INSIDE);
    box->redraw();
}

static char str3DTipsText[] = "You can change the camera and the draw plane while dragging. This lets you select the part of the curve you want to drag from one view, rotate the camera, then drag the curve from a different view. To bring up the camera menu, use shift-click (the camera controls on the shadow box and upper left work as-is).\n\nIt gets pretty messy pretty fast. Use the show and hide all buttons on the top to hide most of the curves, then show just the ones you want to edit (tap on the curve and pick the 'H' option).\n\nYou can draw contours at different depths by moving the drawing plane. Turn on grid spacing (under 3D geometry on the left) to get some feedback on how much to move the drawing plane.\n\nThere are three ways to get a curve that's not in the film plane. The first is to use an inflation or extrusion surface as illustrated above. The second is to snap the drawing plane and then draw on it. The third is to draw, then re-draw from another direction.\n\n";

void Instructions::Set3DTipsText()
{
    static Fl_Text_Buffer tb;
    
    if (tb.length() == 0 ) {
        tb.append( str3DTipsText );
    }
    
    g_drawState.m_opUI->m_txtInstructions->wrap_mode( 1, 0 );
    g_drawState.m_opUI->m_txtInstructions->buffer(tb);
    Fl_Box *box = g_drawState.m_opUI->m_boxImage;
    Fl_Shared_Image *opIm = g_drawState.GetImage( DrawState::INSTRUCTIONS3DTIPS );
    box->image( opIm );
    box->align(FL_ALIGN_INSIDE);
    box->redraw();
}



static char strCameraText[] = "There are four ways to control the camera viewpoint. The first is the dials in the top left. These rotate the camera left-right, up-down, and zoom in and out.\n\nThe second method is to use the in-screen camera menu, which is invoked by tapping on the background. (Holding down the shift key will bring up the camera even if the cursor is over something else.)\n\nThe third method is the shadow box. The yellow handles on the shadow box control the camera. Also, clicking on one of the handles on the drawing plane will cause the camera to rotate so that it is looking directly at the drawing plane. Clicking again will flip which side you’re looking at.\n\nFinally, the keys x,X,y,Y,z,Z will position the camera looking down the corresponding axis.\n";

void Instructions::SetCameraText()
{
    static Fl_Text_Buffer tb;
    
    if (tb.length() == 0 ) {
        tb.append( strCameraText );
    }
    
    g_drawState.m_opUI->m_txtInstructions->wrap_mode( 1, 0 );
    g_drawState.m_opUI->m_txtInstructions->buffer(tb);
    Fl_Box *box = g_drawState.m_opUI->m_boxImage;
    Fl_Shared_Image *opIm = g_drawState.GetImage( DrawState::INSTRUCTIONSCAMERA );
    box->image( opIm );
    box->align(FL_ALIGN_INSIDE);
    box->redraw();
}

static char strCameraMenuText[] = "Click on the background to bring up the camera menu. Shift click will bring up the menu even if the cursor is over something else.\n\nTo select NO action, simply pass the cursor out between the wedges.";

void Instructions::SetCameraMenuText()
{
    static Fl_Text_Buffer tb;
    
    if (tb.length() == 0 ) {
        tb.append( strCameraMenuText );
    }
    
    g_drawState.m_opUI->m_txtInstructions->wrap_mode( 1, 0 );
    g_drawState.m_opUI->m_txtInstructions->buffer(tb);
    Fl_Box *box = g_drawState.m_opUI->m_boxImage;
    Fl_Shared_Image *opIm = g_drawState.GetImage( DrawState::INSTRUCTIONSCAMERAMENU );
    box->image( opIm );
    box->align(FL_ALIGN_INSIDE);
    box->redraw();
}

static char strSurfaceText[] = "This stage is primarily concerned with making curves actually intersect and orienting them correctly. The default view (3D surfacing button) draws the curves as ribbons (yellow side is ‘out’) and shows explicit point and normal constraints. The goal is to snap curves together wherever they are close and orient the ribbons so they all face out. Once this looks ok, you can generate a surface. The surface may not look so great; there's tips for fixing some common problems./n/nIt's best to snap curves together first, then fix the normals. Mostly because snapping the curves together will fix a lot of the normal problems./n/nThere is a separate dialog for showing, hiding, and calculating the surface geometry (Surface window checkbox  on left.)\n\n";

void Instructions::SetSurfaceText()
{
    static Fl_Text_Buffer tb;
    
    if (tb.length() == 0 ) {
        tb.append( strSurfaceText );
    }
    
    g_drawState.m_opUI->m_txtInstructions->wrap_mode( 1, 0 );
    g_drawState.m_opUI->m_txtInstructions->buffer(tb);
    Fl_Box *box = g_drawState.m_opUI->m_boxImage;
    Fl_Shared_Image *opIm = g_drawState.GetImage( DrawState::INSTRUCTIONSSURFACE );
    box->image( opIm );
    box->align(FL_ALIGN_INSIDE);
    box->redraw();
}

static char strSurfacePointsText[] = "There are two ways you can snap curves together. One is to manually snap them together using the curve menu (top option, ‘P’). The other is a global search process, which shows all the potential intersections. You can then click to fix them, or fix them all at once. \n\nYou can drag a point constraint by clicking on it, then mousing down and dragging in the 'P' option. This will drag all of the curves.\n\nTo show or hide the pin constraints, click in the upper right ('Pin points'). Note that where there is a pin the curve is drawn 'pinched' to indicate that.";

void Instructions::SetSurfacePointsText()
{
    static Fl_Text_Buffer tb;
    
    if (tb.length() == 0 ) {
        tb.append( strSurfacePointsText );
    }
    
    g_drawState.m_opUI->m_txtInstructions->wrap_mode( 1, 0 );
    g_drawState.m_opUI->m_txtInstructions->buffer(tb);
    Fl_Box *box = g_drawState.m_opUI->m_boxImage;
    Fl_Shared_Image *opIm = g_drawState.GetImage( DrawState::INSTRUCTIONSSURFACEPOINTS );
    box->image( opIm );
    box->align(FL_ALIGN_INSIDE);
    box->redraw();
}

static char strSurfaceNormalsText[] = "First fix all of the normals for the intersection points. You should add a normal at an intersection point if one isn’t there already. At this point you may need to add additional normal constraints to orient the curves the right way. To add a normal constraint, click on the curve location you want to orient to bring up the curve menu, then pick the ‘N’ option. This adds a normal constraint. These constraints can be rotated around by dragging on them. You can show/hide the normal constraints by clicking in the upper right ('Ribbon normals')\n\n";

void Instructions::SetSurfaceNormalsText()
{
    static Fl_Text_Buffer tb;
    
    if (tb.length() == 0 ) {
        tb.append( strSurfaceNormalsText );
    }
    
    g_drawState.m_opUI->m_txtInstructions->wrap_mode( 1, 0 );
    g_drawState.m_opUI->m_txtInstructions->buffer(tb);
    Fl_Box *box = g_drawState.m_opUI->m_boxImage;
    Fl_Shared_Image *opIm = g_drawState.GetImage( DrawState::INSTRUCTIONSSURFACENORMALS );
    box->image( opIm );
    box->align(FL_ALIGN_INSIDE);
    box->redraw();
}

static char strSurfaceTipsText[] = "If there's a lot of wiggles in the curve it's probably a good idea to smooth it.\n\nSometimes when you snap one constraint (or smooth or edit) it will undo an existing one. Which is why it's a good idea to re-check with the global intersection check.\n\nThe surface building code is a bit flakey, and a bit slow. If the surface is bulging out, try adding more normal constraints. If you get something really crazy, you probably have a normal backwards somewhere. You can see what it's actually using for generating the surface by clicking the ‘Constraints’ box in the surface window. Also check for curves that look closed, but aren't, and dangling edges.\n\nFor checking that curves are closed, there's a show all closed curves button in the top menu.\n\nIn general there's two ways to fix the surface. The first is to add more normal constraints. The second is to add additional curves, or to extend ones that already exist. If the surface has actual topological problems (holes where you don't want them, filled surfaces where you do) adding curves is usually the best way to go. Specifically, add a curve that is perpendicular/crosses to the existing ones.\n\n";

void Instructions::SetSurfaceTipsText()
{
    static Fl_Text_Buffer tb;
    
    if (tb.length() == 0 ) {
        tb.append( strSurfaceTipsText );
    }
    
    g_drawState.m_opUI->m_txtInstructions->wrap_mode( 1, 0 );
    g_drawState.m_opUI->m_txtInstructions->buffer(tb);
    Fl_Box *box = g_drawState.m_opUI->m_boxImage;
    Fl_Shared_Image *opIm = g_drawState.GetImage( DrawState::INSTRUCTIONSSURFACETIPS );
    box->image( opIm );
    box->align(FL_ALIGN_INSIDE);
    box->redraw();
}



void Instructions::ChangeRadioButton()
{
    if ( g_drawState.m_opUI->m_bInstructions->value() ) {
        SetOverviewText();
    } else if ( g_drawState.m_opUI->m_bInstructions2D->value() ) {
        Set2DText();
    } else if ( g_drawState.m_opUI->m_bInstructions2DMenu->value() ) {
        Set2DMenuText();
    } else if ( g_drawState.m_opUI->m_bInstructions2DCamera->value() ) {
        Set2DCameraText();
    } else if ( g_drawState.m_opUI->m_bInstructions2DTips->value() ) {
        Set2DTipsText();
    } else if ( g_drawState.m_opUI->m_bInstructions3D->value() ) {
        Set3DText();
    } else if ( g_drawState.m_opUI->m_bInstructions3DDraw->value() ) {
        Set3DDrawText();
    } else if ( g_drawState.m_opUI->m_bInstructions3DDrawPlane->value() ) {
        Set3DDrawPlaneText();
    } else if ( g_drawState.m_opUI->m_bInstructions3DDrawExtrusion->value() ) {
        Set3DDrawExtrusionText();
    } else if ( g_drawState.m_opUI->m_bInstructions3DDrawInflation->value() ) {
        Set3DDrawInflationText();
    } else if ( g_drawState.m_opUI->m_bInstructions3DCurveMenu->value() ) {
        Set3DCurveMenuText();
    } else if ( g_drawState.m_opUI->m_bInstructions3DTips->value() ) {
        Set3DTipsText();
    } else if ( g_drawState.m_opUI->m_bInstructionsCamera->value() ) {
        SetCameraText();
    } else if ( g_drawState.m_opUI->m_bInstructionsCameraMenu->value() ) {
        SetCameraMenuText();
    } else if ( g_drawState.m_opUI->m_bInstructionsSurface->value() ) {
        SetSurfaceText();
    } else if ( g_drawState.m_opUI->m_bInstructionsSurfacePoints->value() ) {
        SetSurfacePointsText();
    } else if ( g_drawState.m_opUI->m_bInstructionsSurfaceNormals->value() ) {
        SetSurfaceNormalsText();
    } else if ( g_drawState.m_opUI->m_bInstructionsSurfaceTips->value() ) {
        SetSurfaceTipsText();
    }
}