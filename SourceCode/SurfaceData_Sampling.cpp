/*
 *  SurfaceData_Sampling.cpp
 *  SketchCurves
 *
 *  Created by Cindy Grimm on 3/2/11.
 *  Copyright 2011 Adobe Corp. All rights reserved.
 *
 */


#include "SurfaceData.h"
#include "ParamFile.h"
#include <fitting/FITTools_FitLine.H>

/* Don't add in a new constraint that's too close to an existing one; the matrix invert
 * will choke */
bool SurfaceData::AlreadyIncluded( const R3Pt &in_pt, const double in_dDist ) const
{
	for ( int i = 0; i < m_aptPointConstraints.size(); i++ ) {
		if ( ::Length( m_aptPointConstraints[i] - in_pt ) < in_dDist ) {
			return true;
		}
	}
	for ( int i = 0; i < m_arayNormalConstraints.size(); i++ ) {
		if ( ::Length( m_arayNormalConstraints[i].Pt() - in_pt ) < in_dDist ) {
			return true;
		}
	}
	for ( int i = 0; i < m_arayTangentConstraints.size(); i++ ) {
		if ( ::Length( m_arayTangentConstraints[i].Pt() - in_pt ) < in_dDist ) {
			return true;
		}
	}
	return false;
}

/* Add in normal constraints from curves, and a point between each one, then go back and see if need to add more */
void SurfaceData::AddNormalConstraints( const CurveNetwork &in_crvNetwork )
{
	m_arayNormalConstraints.resize(0);
	
	
    /*
	const double dDist = m_dDistance * 0.1;
	
	for ( int iC = 0; iC < in_crvNetwork.NumCrvs(); iC++ ) {
		const Curve &crv = in_crvNetwork.GetCurve(iC);
		for ( int i = 0; i < crv.NumNormalConstraints(); i++ ) {
			const R3Pt pt = crv.NormalConstraintPt( i );
			const R3Vec vec = crv.NormalConstraintNormal(i);
			
			if ( !AlreadyIncluded(pt, dDist) ) {
				m_arayNormalConstraints.push_back( R3Ray( pt, vec ) );
			}
			// Find mid point
			double dT = 0.5 * ( crv.NormalConstraintTValue(i) + crv.NormalConstraintTValue( (i+1) % crv.NumNormalConstraints()) );
			if ( dT > 1.0 ) dT -= 1.0;
			
			const R3Pt ptMid = crv( dT );
			const R3Vec vecMid = crv.Normal( dT );
			if ( !AlreadyIncluded(ptMid, dDist) ) {
				m_arayNormalConstraints.push_back( R3Ray( ptMid, vecMid ) );
			}
		}
	}
     */
			
	const std::pair<R3Pt, R3Pt> bbox = in_crvNetwork.BoundingBox();
	
	const double dAng = g_paramFile.GetDouble( "NormalConstraintAngle" );
	const double dMaxLen = ::Length( bbox.first - bbox.second ) * g_paramFile.GetDouble( "NormalMaximumLength" );
	
	const double dNormLen = ::Length( bbox.first - bbox.second ) * 0.01;
	
	// Now do by curvature
	for ( int iCrv = 0; iCrv < in_crvNetwork.NumCrvs(); iCrv++ ) {
		const CurveRibbon &crv = in_crvNetwork.GetCurveRibbon( iCrv );
		if ( crv.NumPts() == 0 ) {
			cerr << "ERR: Surface data normal constraints, curve with no points\n";
			continue;
		}
		
		const double dDist = 2.0 * crv.AverageSegLength();
		
		R3Vec vecLast = crv.GetNormalAtPoint(0);
		R3Vec vecNext = vecLast;
		if ( !AlreadyIncluded( crv.Pt(0), dDist ) ) {
			m_arayNormalConstraints.push_back( R3Ray( crv.Pt(0), UnitSafe(vecLast) * dNormLen ) );
		}
		double dSum = 0.0;
		for ( int iPt = 0; iPt < crv.NumPts(); iPt++ ) {
			vecNext = crv.GetNormalAtPoint(iPt);
			dSum += crv.DistanceSegment(iPt);
			const double dDot = Dot( vecNext, vecLast );
			const double dAngDiff = DotToAngle( dDot );
			if ( dAngDiff > dAng || dSum > dMaxLen ) {
				if ( !AlreadyIncluded( crv.Pt(iPt), dDist ) ) {
					m_arayNormalConstraints.push_back( R3Ray( crv.Pt(iPt), UnitSafe(vecNext) * dNormLen ) );
				}
				vecLast = vecNext;
				dSum = 0.0;
			}
		}
	}
	
	cout << "Added " << m_arayNormalConstraints.size() << " curve normal constraints\n";
}

/* Do just by curvature */
void SurfaceData::AddTangentConstraints( const CurveNetwork &in_crvNetwork )
{
	m_arayTangentConstraints.resize(0);
	
	const std::pair<R3Pt, R3Pt> bbox = in_crvNetwork.BoundingBox();
	
	const double dAng = g_paramFile.GetDouble( "TangentConstraintAngle" );
	const double dMaxLen = ::Length( bbox.first - bbox.second ) * g_paramFile.GetDouble( "TangentMaximumLength" );

    const double dTangLen = ::Length( bbox.first - bbox.second ) * 0.01;

	for ( int iCrv = 0; iCrv < in_crvNetwork.NumCrvs(); iCrv++ ) {
		const Curve &crv = in_crvNetwork.GetCurve( iCrv );
		if ( crv.NumPts() == 0 ) {
			cerr << "ERR: Surface data tangent constraints, curve with no points\n";
			continue;
		}
		
		const double dDist = 2.0 * crv.AverageSegLength();
		
		R3Vec vecLast = crv.GetTangentAtPoint(0);
		R3Vec vecNext = vecLast;
		if ( !AlreadyIncluded( crv.Pt(0), dDist ) ) {
			m_arayTangentConstraints.push_back( R3Ray( crv.Pt(0), vecLast * dTangLen ) );
		}
		double dSum = 0.0;
		for ( int iPt = 0; iPt < crv.NumPts(); iPt++ ) {
			vecNext = crv.GetTangentAtPoint(iPt);
			dSum += crv.DistanceSegment(iPt);
			const double dDot = Dot( vecNext, vecLast );
			const double dAngDiff = DotToAngle( dDot );
			if ( dAngDiff > dAng || dSum > dMaxLen ) {
				if ( !AlreadyIncluded( crv.Pt(iPt), dDist ) ) {
					m_arayTangentConstraints.push_back( R3Ray( crv.Pt(iPt), vecNext * dTangLen ) );
				}
				vecLast = vecNext;
				dSum = 0.0;
			}
		}
	}
	cout << "Added " << m_arayTangentConstraints.size() << " tangent constraints\n";
}

/* Do by distance */
void SurfaceData::AddPointConstraints( const CurveNetwork &in_crvNetwork )
{
	m_aptPointConstraints.resize(0);
	
	FITTools_FitLine oFitLine;
	Array<R3Pt> apt;
	
	const double dAccumErr = g_paramFile.GetDouble( "PointConstraintErrPerc" );
	
	for ( int iCrv = 0; iCrv < in_crvNetwork.NumCrvs(); iCrv++ ) {
		const Curve &crv = in_crvNetwork.GetCurve( iCrv );
		if ( crv.NumPts() == 0 ) {
			cerr << "ERR: Surface data point constraints, curve with no points\n";
			continue;
		}
		
		const double dDist = 2.0 * crv.AverageSegLength();
		const double dActualErr = dAccumErr * crv.AverageSegLength();
		
		if ( !AlreadyIncluded( crv.Pt(0), dDist ) ) {
			m_aptPointConstraints.push_back( crv.Pt(0) );
		}
		apt.need(0);
		apt += crv.Pt(0);
		for ( int iPt = 1; iPt < crv.NumPts(); iPt++ ) {
			apt += crv.Pt(iPt);
			oFitLine.Fit( apt );
			if ( oFitLine.Error() > dActualErr || oFitLine.Line_seg3D().Length() > 3 * dDist || iPt == crv.NumPts() - 1 ) {
				if ( !AlreadyIncluded( crv.Pt(iPt), dDist ) ) {
					m_aptPointConstraints.push_back( crv.Pt(iPt) );
				}
				apt.need(0);
				apt += crv.Pt(iPt);
			}
		}
	}
	cout << "Added " << m_aptPointConstraints.size() << " point constraints\n";
}

void SurfaceData::CalcConstraints( const CurveNetwork &in_crvNetwork )
{
	AddNormalConstraints( in_crvNetwork );
	AddTangentConstraints( in_crvNetwork );
	AddPointConstraints( in_crvNetwork );
	
	BuildSurface();
}