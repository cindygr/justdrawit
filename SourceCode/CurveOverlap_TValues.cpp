/*
 *  CurveOverlap_TValues.cpp
 *  SketchCurves
 *
 *  Created by Cindy Grimm on 3/16/11.
 *  Copyright 2011 Adobe Corp. All rights reserved.
 *
 */

#include "CurveOverlap.h"
#include "Curve.h"
#include "ScreenCurve.h"
#include "ParamFile.h"


/* Take the t values in the overlap region and see if they're monotonically increasing/decreasing */
bool CurveOverlap::IsFoldOver( const OverlapType & in_ot, const bool in_bReverseStroke, const CurveEndType &in_strokeEnd,
							   const std::pair<double,double> &in_dTsStrokeFirstJoin, 
							   const std::pair<double,double> &in_dTsStrokeSecondJoin,
							   const ScreenCurve &in_crvStroke
							  ) const
{
	int iStartTs = 0, iEndTs = in_crvStroke.NumPts();

	if ( in_ot == OVERSTROKE || in_ot == OVERSTROKE_START ) {
		iStartTs = in_crvStroke.Floor( in_dTsStrokeFirstJoin.first );
		iEndTs = in_crvStroke.Ceil( in_dTsStrokeSecondJoin.second );

		if ( iEndTs > in_crvStroke.NumPts() ) {
			cerr << "ERR: IsFoldOver, went past end of curve I\n";
		}
	} else if ( in_ot == MERGE || in_ot == JOIN ) {
		if ( in_strokeEnd == END_CURVE ) {
			iStartTs = in_crvStroke.BinarySearch( in_dTsStrokeFirstJoin.first ).first + 1;			
		} else {
			iEndTs = in_crvStroke.BinarySearch( in_dTsStrokeFirstJoin.second ).first;
		}
		if ( iEndTs > in_crvStroke.NumPts() ) {
			cerr << "ERR: IsFoldOver, went past end of curve II\n";
		}
	} else if ( in_ot == CLOSES ) {
		iStartTs = in_crvStroke.BinarySearch( in_dTsStrokeFirstJoin.first ).first + 1;
		iEndTs = in_crvStroke.NumPts() + in_crvStroke.BinarySearch( in_dTsStrokeSecondJoin.second ).first;		
	}
	if ( s_bTraceTs ) cout << "FoldOver " << iStartTs << " " << iEndTs << " " << in_crvStroke.NumPts() << "\n";
	
	int iLoops = 5;
	bool bIsMonotonic = false;
	std::vector<double> adTs = m_adTsOnOriginalCurve;
	std::vector<double> adTsTemp;
	while ( iLoops >= 0 && bIsMonotonic == false ) {
		bIsMonotonic = true;
		for ( int i = iStartTs; i < iEndTs - 1; i++ ) {
			// Need to wrap for CLOSES case
			const double dT = adTs[ i % in_crvStroke.NumPts() ] - ( i >= in_crvStroke.NumPts() ? 1.0 : 0.0 );
			const double dTNext = adTs[ (i+1) % in_crvStroke.NumPts() ] - ( (i+1) >= in_crvStroke.NumPts() ? 1.0 : 0.0 );
			if ( (!in_bReverseStroke && dT > dTNext) || (in_bReverseStroke && dT < dTNext) ) {
				bIsMonotonic = false;
				break;
			}
		}
		if ( bIsMonotonic == false ) {
			adTsTemp = adTs;
			for ( int i = 1; i < adTs.size() - 1; i++ ) {
				adTs[i] = 0.5 * adTsTemp[i] + 0.5 * ( 0.5 * (adTsTemp[i-1] + adTsTemp[i+1]) );
			}
		}
		iLoops--;
	}

	return !bIsMonotonic;
}

bool CurveOverlap::IsFoldOver() const
{
	return IsFoldOver( GetOverlapType(), m_bReverseStroke, m_strokeEnd, m_dTsStrokeFirstJoin, m_dTsStrokeSecondJoin, m_bReverseStroke ? m_crvStroke.Reverse() : m_crvStroke );
}


/* The t value on the original 3D curve that the stroke point projects to */
double CurveOverlap::TOnOriginalCurve( const int in_iPtOnStroke ) const 
{ 
	if ( m_bReverseStroke ) {
		return m_adTsOnOriginalCurve[ m_adTsOnOriginalCurve.size() - in_iPtOnStroke - 1]; 
	} else {
		return m_adTsOnOriginalCurve[in_iPtOnStroke]; 
	}
}

double CurveOverlap::ConvertTValue( const double in_dT ) const
{
	if ( in_dT <= 0.0 && !m_bCurveClosed ) { return 0.0; }
	if ( in_dT >= 1.0 && !m_bCurveClosed ) { return 1.0; }
	
    double dT = in_dT;
    if ( dT < 0.0 ) dT += 1.0;
    if ( dT > 1.0 ) dT -= 1.0;
    
	const std::pair<int,double> idLoc = m_crvProjected.BinarySearch( dT );
	double dTMap = m_adTsMap[idLoc.first+1] * idLoc.second + m_adTsMap[idLoc.first] * (1.0 - idLoc.second);
    
    if ( in_dT < 0.0 ) dTMap -= 1.0;
    if ( in_dT > 1.0 ) dTMap += 1.0;
    
    return dTMap;
}


void CurveOverlap::ConvertOriginalCurveTValues(  )
{
	m_adTsOnOriginalCurve.resize( m_crvStroke.NumPts() );
	// This is really slow, but it'll work for now
	for ( int i = 0; i < m_adTsOnOriginalCurve.size(); i++ ) {
		m_adTsOnOriginalCurve[i] = ConvertTValue( m_adTsOnCurve[i] );
	}
}

void CurveOverlap::ConvertCurveJoinTValues(  )
{
	m_dTsCurveFirstJoin.first = ConvertTValue( m_dTsCurveFirstJoin.first );
	m_dTsCurveFirstJoin.second = ConvertTValue( m_dTsCurveFirstJoin.second );
	
	if ( m_overlapType == OVERSTROKE ) {
		m_dTsCurveSecondJoin.first = ConvertTValue( m_dTsCurveSecondJoin.first );
		m_dTsCurveSecondJoin.second = ConvertTValue( m_dTsCurveSecondJoin.second );
	}
}

/* Assumes in_crv3D is a 3D version of the stroke curve and that there is a 1-1 correspondence
 * between the points on the screen version of the stroke curve and the 3D one
 */
double CurveOverlap::ConvertStrokeTValue( const Curve &in_crv3D, const double in_dT ) const
{
	if ( in_dT <= 0.0 ) { return 0.0; }
	if ( in_dT >= 1.0 ) { return 1.0; }
	
	const double dTSearch = m_bReverseStroke ? 1.0 - in_dT : in_dT;
	for ( int i = 0; i < m_crvStroke.NumPts() - 1; i++ ) {
		const double dTRatio = ( m_crvStroke.PercAlong( i+1 ) - dTSearch ) / ( m_crvStroke.PercAlong(i+1) - m_crvStroke.PercAlong(i) );
		if ( dTRatio >= 0.0 && dTRatio <= 1.0 ) {
			if ( m_bReverseStroke == true ) {
				const int iN = m_crvStroke.NumPts();
				return in_crv3D.PercAlong(iN - i - 1) * dTRatio + in_crv3D.PercAlong(iN -(i+1) - 1) * (1.0 - dTRatio);
			} else {
				return in_crv3D.PercAlong(i) * dTRatio + in_crv3D.PercAlong(i+1) * (1.0 - dTRatio);
			}
		}
	}
	ASSERT(FALSE);
	return 0.0;
}

/* Specifically convert the join region on the screen stroke to the join region on the 3D version */
std::pair<double,double> CurveOverlap::StrokeMergeTsFirst( const Curve &in_crvStroke3D ) const
{
	std::pair<double,double> adTs = m_dTsStrokeFirstJoin;
	
	adTs.first = ConvertStrokeTValue( in_crvStroke3D, adTs.first );
	adTs.second = ConvertStrokeTValue( in_crvStroke3D, adTs.second );
	
	if ( adTs.first > adTs.second ) {
		cerr << "ERR: StrokeMergeTsFirst, reversed " << m_dTsStrokeFirstJoin.first << " " << m_dTsStrokeFirstJoin.second << " " << adTs.first << " " << adTs.second << "\n";
	}
	
	return adTs;
}

/* Specifically convert the join region on the screen stroke to the join region on the 3D version */
std::pair<double,double> CurveOverlap::StrokeMergeTsSecond( const Curve &in_crvStroke3D ) const
{
	std::pair<double,double> adTs = m_dTsStrokeSecondJoin;
	
	adTs.first = ConvertStrokeTValue( in_crvStroke3D, adTs.first );
	adTs.second = ConvertStrokeTValue( in_crvStroke3D, adTs.second );
	
	assert( adTs.first <= adTs.second );
	return adTs;
}

/* If we were adding a stroke to the last stroke at the end of the curve, then we need to convert the t values
 * from the last added stroke to the entire curve.
 * Assumes that the composite curve largely follows the last stroke added
 */
void CurveOverlap::MapTsToComposite( const Curve &in_crvComposite, const Curve &in_crvLast )
{
	for ( int i = 0; i < m_adTsOnOriginalCurve.size(); i++ ) {
		m_adTsOnOriginalCurve[i] = in_crvComposite.ClosestPointTValue( in_crvLast( m_adTsOnOriginalCurve[i] ) );
	}
	m_dTsCurveFirstJoin.first = in_crvComposite.ClosestPointTValue( in_crvLast( m_dTsCurveFirstJoin.first ) );
	m_dTsCurveFirstJoin.second = in_crvComposite.ClosestPointTValue( in_crvLast( m_dTsCurveFirstJoin.second ) );
	m_dTsCurveSecondJoin.first = in_crvComposite.ClosestPointTValue( in_crvLast( m_dTsCurveSecondJoin.first ) );
	m_dTsCurveSecondJoin.second = in_crvComposite.ClosestPointTValue( in_crvLast( m_dTsCurveSecondJoin.second ) );
}

