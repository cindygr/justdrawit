/*
 *  SurfaceData_Surface.cpp
 *  SketchCurves
 *
 *  Created by Cindy Grimm on 3/2/11.
 *  Copyright 2011 Adobe Corp.. All rights reserved.
 *
 */

#include "SurfaceData.h"
#include <fitting/Fit_RBFHermite.H>
#include <utils/Polygonizer.H>
#include "UserInterface.h"

SurfaceData *SurfaceData::s_srfData = NULL;
int SurfaceData::TriProc( int in_i1, int in_i2, int in_i3, VERTICES vs )
{
	const R3Pt pt = vs.ptr[in_i1].position;
	
	bool bOutside = false;
	for ( int j = 0; j < 3; j++ ) {
		const double dWidth = s_srfData->m_ptMax[j] - s_srfData->m_ptMin[j];
		if ( pt[j] < s_srfData->m_ptMin[j] - dWidth * 0.1 ) {
			bOutside = true;
		}
		if ( pt[j] > s_srfData->m_ptMax[j] + dWidth * 0.1 ) {
			bOutside = true;
		}
	}
	//if ( bOutside == false ) {
		s_srfData->m_afaceSurface.push_back( R3Pt_i( in_i1, in_i2, in_i3 ) );
	//}
	
	return 1;
}

void SurfaceData::VertProc( VERTICES vs )
{
	s_srfData->m_aptSurface.resize( vs.count );
	s_srfData->m_avecSurface.resize( vs.count );
	for ( int i = 0; i < vs.count; i++ ) {
		s_srfData->m_aptSurface[i] = vs.ptr[i].position;
		s_srfData->m_avecSurface[i] = UnitSafe( vs.ptr[i].normal );
	}
}

void SurfaceData::BuildSurface(  )
{
	FITRbfHermite oRBF;
	
	if ( m_arayNormalConstraints.size() == 0 ) {
		return;
	}
	
	if ( g_drawState.m_opUI && !g_drawState.m_opUI->m_bShowMesh->value() ) {
		g_drawState.m_opUI->m_bShowMesh->value(1);
	}
	
	cout << "Building surface\n";
	
	Array< R3Pt > aptLocsOnly( m_aptPointConstraints.size() ); 
	Array< std::pair<R3Pt,R3Vec> > aptNorms( m_arayNormalConstraints.size() );
	Array< std::pair<R3Pt,R3Vec> > aptTangents( m_arayTangentConstraints.size() );
	
	m_ptMin = R3Pt(1e30, 1e30, 1e30);
	m_ptMax = R3Pt( -1e30, -1e30, -1e30 );
	
	for ( int i = 0; i < m_aptPointConstraints.size(); i++ ) {
		aptLocsOnly[i] = m_aptPointConstraints[i];
		for ( int j = 0; j < 3; j++ ) {
			m_ptMin[j] = WINmin( m_ptMin[j], aptLocsOnly[i][j] );
			m_ptMax[j] = WINmax( m_ptMax[j], aptLocsOnly[i][j] );
		}
	}
	for ( int i = 0; i < m_arayNormalConstraints.size(); i++ ) {
		aptNorms[i] = std::pair<R3Pt,R3Vec>( m_arayNormalConstraints[i].Pt(), m_arayNormalConstraints[i].Dir() );
		for ( int j = 0; j < 3; j++ ) {
			m_ptMin[j] = WINmin( m_ptMin[j], aptNorms[i].first[j] );
			m_ptMax[j] = WINmax( m_ptMax[j], aptNorms[i].first[j] );
		}
	}
	for ( int i = 0; i < m_arayTangentConstraints.size(); i++ ) {
		aptTangents[i] = std::pair<R3Pt,R3Vec>( m_arayTangentConstraints[i].Pt(), m_arayTangentConstraints[i].Dir() );
		for ( int j = 0; j < 3; j++ ) {
			m_ptMin[j] = WINmin( m_ptMin[j], aptTangents[i].first[j] );
			m_ptMax[j] = WINmax( m_ptMax[j], aptTangents[i].first[j] );
		}
	}
	oRBF.Set( aptLocsOnly, aptNorms, aptTangents );
	
	const R3Pt ptMid = Lerp( m_ptMin, m_ptMax, 0.5 );
	int iBest = 0;
	double dDist = 1e30;
	for ( int i = 0; i < aptLocsOnly.num(); i++ ) {
		const double dDistPt = ::Length( aptLocsOnly[i] - ptMid );
		if ( dDistPt < dDist ) {
			dDist = dDistPt;
			iBest = i;
		}
	}
	
	cout << "Done building matrix\n";
	
	oRBF.SetThis();
	s_srfData = this;

	m_aptSurface.resize(0);
	m_avecSurface.resize(0);
	m_afaceSurface.resize(0);
	
	const R3Vec vec = m_ptMax - m_ptMin;
	const double dWidth = WINmax( vec[0], WINmax( vec[1], vec[2] ) );
	const double dSize = g_drawState.m_opUI->m_iNumCubes->value() * dWidth;
	double dDistToBdry = 0.0;
	for ( int j = 0; j < 3; j++ ) {
		dDistToBdry = WINmax( aptLocsOnly[iBest][j] - m_ptMin[j], dDistToBdry );
		dDistToBdry = WINmax( m_ptMax[j] - aptLocsOnly[iBest][j], dDistToBdry );
	}
		
	const int iBounds = (int) ( dDistToBdry / (0.25 * dSize + 1e-6) );
	polygonize( FITRbfHermite::DistFunc, dSize, iBounds, aptLocsOnly[iBest], TriProc, VertProc );
			
	FlipSurface();
    CalcNormalsForSurface();
    //SmoothSurface();
	cout << "Done building surface " << iBounds
	<< " faces " << m_afaceSurface.size() << "\n";
	
}

void SurfaceData::FlipSurface()
{
	for ( int i = 0; i < m_afaceSurface.size(); i++ ) {
		const R3Pt_i ipt = m_afaceSurface[i];
		m_afaceSurface[i] = R3Pt_i( ipt[0], ipt[2], ipt[1] );
	}
}

void SurfaceData::FlipNormals()
{
	for ( int i = 0; i < m_avecSurface.size(); i++ ) {
		m_avecSurface[i] *= -1.0;
	}
}

void SurfaceData::SmoothSurface()
{
    Array<R3Pt> aptSmooth( m_aptSurface.size() );
    Array<R3Vec> avecSmooth( m_aptSurface.size() );
    Array<int> aiCount( m_aptSurface.size() );
    aptSmooth.fill( R3Pt(0,0,0) );
    avecSmooth.fill( R3Vec(0,0,0) );
    aiCount.fill( 0 );
	for ( int i = 0; i < m_afaceSurface.size(); i++ ) {
		const R3Pt_i ipt = m_afaceSurface[i];
        for ( int j = 0; j < 3; j++ ) {
            const R3Pt &pt1 = m_aptSurface[ ipt[j] ];
            const R3Pt &pt2 = m_aptSurface[ ipt[(j+1)%3] ];
            for ( int k = 0; k < 3; k++ ) {
                aptSmooth[ ipt[(j+2)%3] ][k] += pt2[k];
                aptSmooth[ ipt[(j+2)%3] ][k] += pt1[k];
            }
            avecSmooth[ ipt[(j+2)%3] ] += m_avecSurface[ ipt[j] ] + m_avecSurface[ ipt[(j+1)%3] ];
            aiCount[ ipt[(j+2)%3] ] += 2;   
        }
	}
    for ( int i = 0; i < aptSmooth.num(); i++ ) {
        if ( aiCount[i] > 0 ) {
            for ( int j = 0; j < 3; j++ ) {
                aptSmooth[i][j] /= (double) aiCount[i];
            }
            avecSmooth[i] = UnitSafe( avecSmooth[i] );
        } else {
            aptSmooth[i] = m_aptSurface[i];
            avecSmooth[i] = m_avecSurface[i];
        }
    }
    for ( int i = 0; i < m_aptSurface.size(); i++ ) {
        m_aptSurface[i] = Lerp( m_aptSurface[i], aptSmooth[i], 0.2 );
        m_avecSurface[i] = avecSmooth[i];
    }
}

void SurfaceData::CalcNormalsForSurface()
{
    Array<R3Vec> avec( m_afaceSurface.size() );
    const double dAng90 = cos( M_PI / 2.0 );
    

    for ( int i = 0; i < avec.num(); i++ ) {
		const R3Pt_i &ipt = m_afaceSurface[i];
        const R3Pt pt1 = m_aptSurface[ ipt[0] ];
        const R3Pt pt2 = m_aptSurface[ ipt[1] ];
        const R3Pt pt3 = m_aptSurface[ ipt[2] ];
        
        const R3Vec v1v2 = UnitSafe( pt1 - pt2 );
        const R3Vec v2v3 = UnitSafe( pt2 - pt3 );
        const R3Vec v3v1 = UnitSafe( pt3 - pt1 );
        const double dDot1 = fabs( Dot( v3v1, v1v2 ) - dAng90 );
        const double dDot2 = fabs( Dot( v1v2, v2v3 ) - dAng90 );
        const double dDot3 = fabs( Dot( v2v3, v3v1 ) - dAng90 );
        
        if ( dDot1 < dDot2 && dDot1 < dDot3 ) {
            avec[i] = Cross( v1v2, -v3v1 );
        } else if ( dDot1 < dDot3 ) {
            avec[i] = Cross( v2v3, -v1v2 );
        } else {
            avec[i] = Cross( v3v1, -v2v3 );
        }
    }
    
    Array<R3Vec> avecSrf( m_avecSurface.size() );
    avecSrf.fill( R3Vec(0,0,0) );
	for ( int i = 0; i < m_afaceSurface.size(); i++ ) {
		const R3Pt_i &ipt = m_afaceSurface[i];
        for ( int j = 0; j < 3; j++ ) {
            avecSrf[ ipt[j] ] += avec[ i ];
        }
	}
    for ( int i = 0; i < avecSrf.num(); i++ ) {
        if ( !RNIsZero( ::Length( avecSrf[i] ) ) && !RNIsZero( ::Length( m_avecSurface[i] ) ) ) {
            m_avecSurface[i] = UnitSafe( m_avecSurface[i] * 0.2 + UnitSafe( avecSrf[i] ) * 0.8 );
        } else if ( RNIsZero( ::Length( m_avecSurface[i] ) ) ) {
            m_avecSurface[i] = UnitSafe( avecSrf[i] );
        }
    }
}


void SurfaceData::WriteSTLSurface( const char *in_str ) const 
{ 
	ofstream out( in_str, ios::out );
	
	if ( !out.good() ) {
		cerr << "ERR: SurfaceData::WriteSurface  can't open file " << in_str << "\n";
		return;
	}
	
	std::string str( in_str );
	if ( str.rfind("/") != str.npos ) {
		str.erase(0, str.rfind("/") + 1 );
	}
	out << "solid " << str.c_str() << "\n";
	for ( int i = 0; i < m_afaceSurface.size(); i++ ) {
		const R3Pt &pt1 = m_aptSurface[ m_afaceSurface[i][0] ];
		const R3Pt &pt2 = m_aptSurface[ m_afaceSurface[i][1] ];
		const R3Pt &pt3 = m_aptSurface[ m_afaceSurface[i][2] ];
		const R3Vec vecNorm = UnitSafe( Cross(pt2 - pt1, pt3 - pt1) );

		out << "facet normal " << vecNorm << "\n";
		out << "  outer loop\n";
		out << "    vertex " << pt1 << "\n";
		out << "    vertex " << pt2 << "\n";
		out << "    vertex " << pt3 << "\n";
		out << "  endloop\n";
		out << "endfacet\n";
	}
	out << "endsolid\n";
	out.close();
    
    str = string( in_str );
    if ( str.rfind(".") ) {
        str.erase( str.rfind(".") );
    }
    str = str + string(".m");
    out.clear();
    out.open( str.c_str(), ios::out );
    out << m_aptSurface.size() << " " << m_afaceSurface.size() << "\n";
    for ( int i = 0; i < m_aptSurface.size(); i++ ) {
        out << m_aptSurface[i] << " " << m_avecSurface[i] << "\n";
    }
    for ( int i = 0; i < m_afaceSurface.size(); i++ ) {
        out << m_afaceSurface[i] << "\n";
    }
    out.close();
}

void SurfaceData::WriteOBJSurface( Array<R3Pt> &io_apt, Array<R3Pt_i> &io_aface,
                                   ofstream &outObj, ofstream &outMtl, const int in_iWhich ) const
{
    const int iNStart = io_apt.num();
 
    if ( m_aptSurface.size() == 0 ) {
        return;
    }
    outObj << "g Srf" << in_iWhich << "\n";
    outObj << "usemtl SrfMtl" << in_iWhich << "\n";
    
    
    outMtl << "newmtl SrfMtl" << in_iWhich << "\n";
    outMtl << "illum 2\n";
    outMtl << "Kd 0.5 0.8 1\n";
    outMtl << "Ka 0.1 0.1 0.1\n";
    outMtl << "Ks 0.75 0.75 0.75\n\n";

    outObj << "g Srf" << in_iWhich << "\n";
    outObj << "usemtl SrfMtl" << in_iWhich << "\n";
    for ( int i = 0; i < m_aptSurface.size(); i++ ) {
        outObj << "v " << m_aptSurface[i] << "\n";
        outObj << "vn " << m_avecSurface[i] << "\n";
        io_apt += m_aptSurface[i];
    }

    R3Pt_i ipt;
    for ( int i = 0; i < m_afaceSurface.size(); i++ ) {
        outObj << "f ";
        for ( int j = 0; j < 3; j++ ) {
            const int iVI = m_afaceSurface[i][j] + iNStart + 1;
            outObj << iVI << "//" << iVI << " ";
            ipt[j] = m_afaceSurface[i][j] + iNStart;
        }
        outObj << "\n";
        io_aface += ipt;
    }
    outObj << "\n";
}

void SurfaceData::ReadSurface( ifstream &in )
{

    string str;
    int iNV, iNF;
    in >> iNV >> iNF;
    m_aptSurface.resize( iNV );
    m_avecSurface.resize( iNV );
    m_afaceSurface.resize( iNF );
    for ( int i = 0; i < iNV; i++ ) {
        in >> m_aptSurface[i] >> m_avecSurface[i];
    }
    for ( int i = 0; i < iNF; i++ ) {
        in >> m_afaceSurface[i];
    }
}

void SurfaceData::WriteSurface( ofstream &out ) const
{
    
    out << m_aptSurface.size() << " " << m_afaceSurface.size() << "\n";

    for ( int i = 0; i < m_aptSurface.size(); i++ ) {
        out << m_aptSurface[i] << m_avecSurface[i] << "\n";
    }
    for ( int i = 0; i < m_afaceSurface.size(); i++ ) {
        out << m_afaceSurface[i] << "\n";
    }
}

