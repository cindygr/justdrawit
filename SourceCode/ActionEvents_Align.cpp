
/*
 *  ActionEvents_Align.cpp
 *  SketchCurves
 *
 *  Created by Cindy Grimm on 2/19/2011
 *  Copyright 2011 Adobe Corp . All rights reserved.
 *
 */

#include "ActionEvents.h"
#include "UserInterface.h"

const bool s_bTrace = false;

/*
 * Create an axis frame from either the curve or the canonical x,y,z axes
 *
 * Stored as a rotation matrix
 */
void ActionEvents::CreateAxis( const R2Pt &in_ptScreen, const R3Ray &in_ray )
{
    if ( !(m_action == CAMERA_ALIGN || m_action == DRAWPLANE_ALIGN) || m_bInFocus == false ) {
        return;
    }
	if ( s_bTrace ) cout << "Create axis ";
    
    R3Pt pt(0,0,0);
    R3Matrix mat;
    if ( OverCurve() ) {
		if ( s_bTrace )cout << "Over curve " << m_iCursorOverCurve << " " << m_dCursorOverCurve << "\n";
        const std::pair<R3Pt, R3Matrix> ptMat = m_crvNetwork.GetCurveRibbon( m_iCursorOverCurve ).Frame( m_dCursorOverCurve );
        pt = ptMat.first;
        mat = ptMat.second.Transpose();
    } else {
		if ( s_bTrace )cout << "scene\n";
		const R3Plane plane( g_drawState.GetCamera().At(), g_drawState.GetCamera().Look() );
        pt = plane.IntersectRay( m_rayDown );
        mat.MakeIdentity();
    }
    const R4Matrix matTrans( R4Matrix::Translation( pt - R3Pt(0,0,0) ) );
    
    m_amatAlignAxis[0] = matTrans * (R4Matrix) (mat);
    m_amatAlignAxis[1] = matTrans * (R4Matrix) (mat * R3Matrix::Rotation(0, M_PI ));
    m_amatAlignAxis[2] = matTrans * (R4Matrix) (mat * R3Matrix::Rotation(2, M_PI * 0.5 ));
    m_amatAlignAxis[3] = matTrans * (R4Matrix) (mat * R3Matrix::Rotation(2, M_PI * -0.5 ));
    m_amatAlignAxis[4] = matTrans * (R4Matrix) (mat * R3Matrix::Rotation(0, M_PI * 0.5 ));
    m_amatAlignAxis[5] = matTrans * (R4Matrix) (mat * R3Matrix::Rotation(0, M_PI * -0.5 ));
    
	BestAxis(in_ptScreen);
}

/*
 * Check distance to end of arrows to determine which is selected.
 *
 * Sets both look and up vectors (m_vecAlignCameraLook, m_vecAlignCameraUp). Tries to pick
 * the up vector that is most aligned with the current one.
 */
void ActionEvents::BestAxis( const R2Pt &in_ptScreen )
{
    if ( !(m_action == CAMERA_ALIGN || m_action == DRAWPLANE_ALIGN) || m_bInFocus == false ) {
        return;
    }
	const double dHeight = g_drawState.MenuSize3D();
	
    m_iAlignCamera = -1;
    double dBest = 1e30;
	int iUp = -1;
	double dBestUp = -1e30;
	int iZMin = -1, iZMax = -1;
	double dZMin = 1e30, dZMax = -1e30;
    for ( int i = 0; i < 6; i++ ) {
        const R3Pt ptEnd = m_amatAlignAxis[i] * R3Pt(0,dHeight,0);
        const R3Pt ptCam = g_drawState.GetCamera().ProjectedPt( ptEnd );
        const R2Pt ptScreen = g_drawState.GetCamera().CameraPt( ptEnd );
        const double dDist = ::Length( ptScreen - m_ptScreenLast );
        if ( dDist < dBest ) {
            dBest = dDist;
            m_iAlignCamera = i;
        }
		if ( ptScreen[1] > dBestUp ) {
			iUp = i;
			dBestUp = ptScreen[1];
		}
		if ( ptCam[2] < dZMin ) {
			dZMin = ptCam[2];
			iZMin = i;
		}
		if ( ptCam[2] > dZMax ) {
			dZMax = ptCam[2];
			iZMax = i;
		}
    }
	
    if ( dBest > 2.0 * g_drawState.ScreenSelectionSize() || m_iAlignCamera == -1 ) {
        m_iAlignCamera = -1;
        m_vecAlignCameraLook = R3Vec(0,1,0);
		m_vecAlignCameraUp = R3Vec(1,0,0);
		
    } else if ( m_iAlignCamera != -1 ) {
        m_vecAlignCameraLook = UnitSafe( m_amatAlignAxis[ m_iAlignCamera ] * R3Vec(0,-1,0) );
		if ( iUp / 2 != m_iAlignCamera / 2 ) {
			m_vecAlignCameraUp = UnitSafe( m_amatAlignAxis[ iUp ] * R3Vec(0,1,0) );
		} else {
			if ( iUp != m_iAlignCamera ) {
				if ( iZMin == m_iAlignCamera ) {
					m_vecAlignCameraUp = UnitSafe( m_amatAlignAxis[(iUp+1)%6] * R3Vec(0,1,0) );
				} else {
					m_vecAlignCameraUp = UnitSafe( m_amatAlignAxis[iZMin] * R3Vec(0,1,0) );
				}
			} else {
				if ( iZMax == m_iAlignCamera ) {
					m_vecAlignCameraUp = UnitSafe( m_amatAlignAxis[(iUp+1)%6] * R3Vec(0,1,0) );
				} else {
					m_vecAlignCameraUp = UnitSafe( m_amatAlignAxis[iZMax] * R3Vec(0,1,0) );
				}
			}
		}
    }
	//if ( s_bTrace ) cout << "Best axis " << m_iAlignCamera << " " << iUp << " " << m_vecAlignCameraLook << " " << m_vecAlignCameraUp << "\n";
}


/* 
 * A cuve normal constraint
 *
 * Save the Frenet frame at the mouse down point. This determines the plane (perpendicular
 * to the tangent vector) that the normal can be rotated around.
 *
 * Save the starting angle in that plane. Mousing up and down adds/subtracts from that initial angle
 */
void ActionEvents::CalcGeometryAtStartNormalConstraint()
{
	if ( m_opCursorOverCurveNormalConstraint == NULL ) {
		cerr << "ERR: ActionEvents::CalcGeometryAtStart - no normal constraint selected \n";
		return;
	}
    
    if ( !OverCurve() ) {
        cerr << "ERR: ActionEvents::CalcGeometryAtStart, not over curve!\n";
        return;
    }
    
	const Curve &crv = m_crvNetwork.GetCurve( m_iCursorOverCurve );
	const R3Vec vecTang = crv.Tangent( m_dCursorOverCurve );
	const R3Vec &vecNorm = m_opCursorOverCurveNormalConstraint->GetNormal();
	const R3Vec vecPerp = UnitSafe( Cross( vecNorm, vecTang ) );
	m_matAlignNormalConstraint = R3Matrix( vecNorm, vecPerp, vecTang ).Transpose();
	
	const R3Vec vecPlane = m_matAlignNormalConstraint * vecNorm;
	m_dAngAtStartNormalConstraint = atan2( vecPlane[1], vecPlane[0] );
	
	if ( s_bTrace ) {
		cout << m_matAlignNormalConstraint * vecNorm << "\n";
		cout << m_matAlignNormalConstraint * vecTang << "\n";
		cout << m_matAlignNormalConstraint * vecPerp << "\n";
	}
}

/*
 * Use the mouse movement to determine both the angle to rotate (amount of movement) and the
 * direction (towards and away from mouse down)
 *
 * Does an incremental update; this seems to work better than an absolute one based on distance from mouse down
 *
 * Always rotate the normal in the plane perpendicular to the tangent
 */
R3Vec ActionEvents::CalcNewNormalConstraint( const R2Pt &in_ptScreen ) 
{
    if ( m_opCursorOverCurveNormalConstraint == NULL ) {
        cerr << "ERR: ActionEvents::CalcNewNormalConstraint, no constraint selected!\n";
        return R3Vec(0,1,0);
    }
    
    if ( !OverCurve() ) {
        cerr << "ERR: ActionEvents::CalcNewNormalConstraint, not over curve!\n";
        return R3Vec(0,1,0);
    }
        
	const Curve &crv = m_crvNetwork.GetCurve( m_iCursorOverCurve );
	const R2Vec vecScreen( in_ptScreen - m_ptScreenLast );
	const R3Vec vecTang = crv.Tangent( m_dCursorOverCurve );
	const R3Vec vecTangCamera = g_drawState.GetCamera().Projection() * vecTang;
	R2Vec vecTangScreen( vecTangCamera[0], vecTangCamera[1] );
	if ( RNIsZero( ::Length( vecTangScreen ) ) ) {
		vecTangScreen = R2Vec(0,1);
	} else {
		vecTangScreen = UnitSafe(vecTangScreen);
		vecTangScreen = R2Vec( - vecTangScreen[1], vecTangScreen[0] );
	}
	
	const double dLenTraveled = ::Length(vecScreen) / (4.0 * g_drawState.ScreenSelectionSize() );
	const double dAngChange = dLenTraveled * M_PI * ( Dot( vecTangScreen, vecScreen ) < 0.0 ? -1.0 : 1.0 );
	
	if ( s_bTrace ) cout << dAngChange << "\n";
	m_dAngAtStartNormalConstraint += dAngChange;
	const R3Matrix matRot = m_matAlignNormalConstraint.Transpose() * R3Matrix::Rotation(2, m_dAngAtStartNormalConstraint);
	const R3Vec vec = matRot * R3Vec(1,0,0);
	
	return vec;
    return m_opCursorOverCurveNormalConstraint->GetNormal();
}

// Helper function
inline R3Vec VecSphere( const double in_dAngX, const double in_dAngY )
{
	return R3Vec( cos( in_dAngX ) * cos( in_dAngY ), sin( in_dAngY ), sin( in_dAngX ) * cos( in_dAngY ) );
}


/*
 * Convert position of mouse relative to mouse down into a location on the sphere
 * basically using lat-long parameterization
 *
 * Sort of looks like you're grabbing the normal because the position on the screen
 * produces (roughly) the same theta/phi values
 */
R3Vec ActionEvents::CalcNewIntersectionNormal( const R2Vec &in_vecScreen ) const
{
	if ( s_bTrace ) cout << "Edit normal " << in_vecScreen << " ";
	const double dAngY = WINminmax( in_vecScreen[1] * M_PI, -M_PI / 2.0, M_PI / 2.0 );
	const double dAngX = -in_vecScreen[0] * M_PI;
	const R3Vec vecInCamSpace = VecSphere( dAngX, dAngY );
	if ( s_bTrace ) cout << "angles " << dAngX << " " << dAngY << "\n";
	return g_drawState.GetCamera().RotationFromXYZ() * vecInCamSpace;
}

