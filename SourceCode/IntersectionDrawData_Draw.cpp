/*
 *  IntersectionDrawData_Draw.cpp
 *  SketchCurves
 *
 *  Created by Cindy Grimm on 7/2/12.
 *  Copyright 2012 Washington University in St. Louis. All rights reserved.
 *
 */

#include <FL/GL.H>
#include "UserInterface.h"
#include <OpenGL/OGLDraw3D.H>
#include "IntersectionDrawData.h"

/* Just spit out geometry, some of which is stored in m_drawData */
void IntersectionDrawData::DrawPointsSpokes( ) const
{    
    glBegin( GL_POINTS );
    
    // These shouldn't matter, but set them anyways
    glNormal3dv( &(m_pin.GetNormal())[0] );
    glTexCoord2f( 0.0f, 0.0f );
    
    for ( int iS = 0; iS < m_aaaptSpokes.size(); iS++ ) {
        for ( int i = 0; i < m_aaaptSpokes[iS].size(); i++ ) {
            glColor4fv( &m_aafColSpoke[iS][i][0] );
            
            for ( int j = 0; j < m_aaaptSpokes[iS][i].size(); j++ ) {
                glVertex3dv( &m_aaaptSpokes[iS][i][j][0] );
            }
        }
    }
    
    glEnd();
}

/* Just spit out geometry, some of which is stored in m_drawData */
void IntersectionDrawData::DrawPointsWebbing( ) const
{    
    glBegin( GL_POINTS );
    
    // These shouldn't matter, but set them anyways
    glNormal3dv( &(m_pin.GetNormal())[0] );
    glTexCoord2f( 0.0f, 0.0f );
    
    for ( int iR = 0; iR < m_aaptTopPolygon.size(); iR++ ) {
        for ( int i = 0; i < m_aaptTopPolygon[iR].size(); i++ ) {
            glColor4fv( &m_aafColWebbing[iR][i][0] );
            glVertex3dv( &m_aaptTopPolygon[iR][i][0] );
            glVertex3dv( &m_aaptBotPolygon[iR][i][0] );
        }
    }
    
    glEnd();
}

void IntersectionDrawData::DrawLineSpokes( ) const
{
    // These shouldn't matter, but set them anyways
    glTexCoord2f( 0.5f, 0.5f );
    
    const GLfloat fTAcross = 1.0 / (GLfloat) s_iTubeDivs;
    for ( int iS = 0; iS < m_aaaptSpokes.size(); iS++ ) {
        // Along tubes
        const std::vector<R4Pt_f> &acol = m_aafColSpoke[iS];
        
        for ( int j = 0; j < s_iTubeDivs; j++ ) {
            const std::vector<R3Pt> &aptTubeStrip = m_aaaptSpokes[iS][j];
            const std::vector<R3Vec> &avecTubeStrip = m_aaavecSpokesNorm[iS][j];
            
            glBegin( GL_LINE_STRIP );        
            for ( int i = 0; i < aptTubeStrip.size(); i++  ) {  
                glColor4fv( &acol[i][0] );
                
                glTexCoord2f( j * fTAcross, 1.0 );
                glNormal3dv( &avecTubeStrip[i][0] );
                glVertex3dv( &aptTubeStrip[i][0] );            
                
            }
            glEnd();
        }
        
        // Around tubes
        for ( int i = 0; i < m_aaaptSpokes[iS][0].size(); i++  ) {  
            
            glColor4fv( &m_aafColSpoke[iS][i][0] );

            glBegin( GL_LINE_STRIP );        
            for ( int j = 0; j < s_iTubeDivs / 2; j++ ) {
                glTexCoord2f( j * fTAcross, 1.0 );
                glVertex3dv( &m_aaaptSpokes[iS][j][i][0] );            
                
            }
            glEnd();
            glBegin( GL_LINE_STRIP );        
            for ( int j = s_iTubeDivs / 2 + 1; j < s_iTubeDivs; j++ ) {
                glTexCoord2f( j * fTAcross, 1.0 );
                glVertex3dv( &m_aaaptSpokes[iS][j][i][0] );            
                
            }
            glEnd();
        }
    }
}


void IntersectionDrawData::DrawLineWebbing( ) const
{
    // These shouldn't matter, but set them anyways
    glTexCoord2f( 0.5f, 0.5f );

    // These shouldn't matter, but set them anyways
    glNormal3dv( &(m_pin.GetNormal())[0] );
    glTexCoord2f( 0.0f, 0.0f );
    /*
    for ( int iR = 0; iR < m_aafPolygonFaces.size(); iR++ ) {

        const std::vector<R4Pt_i> &afFaces = m_aafPolygonFaces[iR];

        // Follow the triangle strip around = should zig and zag
        for ( int i = 0; i < afFaces.size(); i++ ) {
            const R4Pt_i &ipt = afFaces[i];
            glBegin( GL_LINE_LOOP );    
            if ( ipt[0] == 0 ) {
                ASSERT( ipt[1] < m_aaptTopPolygon[iR].size() );
                ASSERT( ipt[2] < m_aaptTopPolygon[iR+1].size() );
                ASSERT( ipt[3] < m_aaptTopPolygon[iR+1].size() );
                
                glColor4fv( &m_aafColWebbing[iR][ ipt[1] ][0] );
                glNormal3dv( &m_aavecTopPolygonNorm[iR][ ipt[1] ][0] );
                glVertex3dv( &m_aaptTopPolygon[iR][ ipt[1] ][0] );
                
                glColor4fv( &m_aafColWebbing[iR + 1 ][ ipt[2] ][0] );
                glNormal3dv( &m_aavecTopPolygonNorm[iR + 1 ][ ipt[2] ][0] );
                glVertex3dv( &m_aaptTopPolygon[iR + 1 ][ ipt[2] ][0] );
                
                glColor4fv( &m_aafColWebbing[iR + 1 ][ ipt[3] ][0] );
                glNormal3dv( &m_aavecTopPolygonNorm[iR + 1 ][ ipt[3] ][0] );
                glVertex3dv( &m_aaptTopPolygon[iR + 1 ][ ipt[3] ][0] );
            } else if ( ipt[0] == 1 ) {
                ASSERT( ipt[1] < m_aaptTopPolygon[iR+1].size() );
                ASSERT( ipt[2] < m_aaptTopPolygon[iR].size() );
                ASSERT( ipt[3] < m_aaptTopPolygon[iR].size() );
                
                glColor4fv( &m_aafColWebbing[iR + 1][ ipt[1] ][0] );
                glNormal3dv( &m_aavecTopPolygonNorm[iR + 1][ ipt[1] ][0] );
                glVertex3dv( &m_aaptTopPolygon[iR + 1][ ipt[1] ][0] );
                
                glColor4fv( &m_aafColWebbing[iR][ ipt[2] ][0] );
                glNormal3dv( &m_aavecTopPolygonNorm[iR][ ipt[2] ][0] );
                glVertex3dv( &m_aaptTopPolygon[iR][ ipt[2] ][0] );
                
                glColor4fv( &m_aafColWebbing[iR][ ipt[3] ][0] );
                glNormal3dv( &m_aavecTopPolygonNorm[iR][ ipt[3] ][0] );
                glVertex3dv( &m_aaptTopPolygon[iR][ ipt[3] ][0] );
            } else {
                ASSERT(FALSE);
            }
            glEnd();

        }
    }
*/
    for ( int iR = 0; iR < m_aaptTopPolygon.size(); iR++ ) {
        /*
        glBegin( GL_LINE_LOOP );
        // Do the loops of points (non-spoke strands of spider web)
        for ( int i = 0; i < m_aaptTopPolygon[iR].size()+1; i++ ) {
            const int iI = i % m_aaptTopPolygon[iR].size();
            glColor4fv( &m_aafColWebbing[iR][iI][0] );
            glNormal3dv( &m_aavecTopPolygonNorm[iR][iI][0] );
            glVertex3dv( &m_aaptTopPolygon[iR][iI][0] );
        }
        glEnd();
         */

        glBegin( GL_LINE_LOOP );
        for ( int i = 0; i < m_aaptBotPolygon[iR].size(); i++ ) {
            glColor4fv( &m_aafColWebbing[iR][i][0] );
            glNormal3dv( &m_aavecBotPolygonNorm[iR][i][0] );
            glVertex3dv( &m_aaptBotPolygon[iR][i][0] );
        }
        glEnd();
    }
}

void IntersectionDrawData::DrawShadow(  const R3Plane &in_plane ) const
{
}

// Just draw as tubes, even into the webbing
void IntersectionDrawData::DrawSpokes( ) const
{
    const GLfloat fTAcross = 1.0f / (GLfloat) (s_iTubeDivs-1);
    for ( int iS = 0; iS < m_aaaptSpokes.size(); iS++ ) {
        //const int iStopAt = m_aSpokeData[iS].NumPts() - m_aSpokeData[iS].NumWeb();
        for ( int j = 0; j < s_iTubeDivs; j++ ) {
            //const bool bStop = ( j == s_iTubeDivs - 1 ) || ( j == (s_iTubeDivs / 2) - 1 );
            const int iNext = (j+1) % s_iTubeDivs;
            
            
            const std::vector<R3Pt> &aptTubeStripPrev = m_aaaptSpokes[iS][j];
            const std::vector<R3Pt> &aptTubeStripNext = m_aaaptSpokes[iS][iNext];
            const std::vector<R3Vec> &avecTubeStripPrev = m_aaavecSpokesNorm[iS][j];
            const std::vector<R3Vec> &avecTubeStripNext = m_aaavecSpokesNorm[iS][iNext];
            
            glBegin( GL_QUAD_STRIP );
            for ( int i = 0; i < aptTubeStripPrev.size(); i++  ) {  
                glColor4fv( &m_aafColSpoke[iS][i][0] );
                
                glNormal3dv( &avecTubeStripPrev[i][0] );
                glTexCoord2f( j * fTAcross, 1.0 );
                glVertex3dv( &aptTubeStripPrev[i][0] );            
                
                glNormal3dv( &avecTubeStripNext[i][0] );
                glTexCoord2f( (j+1) * fTAcross, 1.0 );    
                glVertex3dv( &(aptTubeStripNext[i])[0] );
                
                //if ( bStop && i == iStopAt ) {
                //    break;
                //}
            }
            glEnd();
        }
    }
}


void IntersectionDrawData::DrawWebbing( ) const
{
    // These shouldn't matter, but set them anyways
    glTexCoord2f( 0.5f, 0.5f );
    
    // These shouldn't matter, but set them anyways
    glNormal3dv( &(m_pin.GetNormal())[0] );
    glTexCoord2f( 0.0f, 0.0f );
    
    const R3Pt_i iptRowTop(0,1,1), iptRowBot(1,0,0);
    glBegin( GL_TRIANGLES );        
    for ( int iR = 0; iR < m_aafPolygonFaces.size(); iR++ ) {
        
        const std::vector<R4Pt_i> &afFaces = m_aafPolygonFaces[iR];
        
        for ( int i = 0; i < afFaces.size(); i++ ) {
            const R4Pt_i &ipt = afFaces[i];
            const R3Pt_i &iptRow = (ipt[0] == 0) ? iptRowTop : iptRowBot;
            
            for ( int j = 1; j < 4; j++ ) {
                const int &iRow = iR + iptRow[j-1];
                const int &iCol = ipt[j];
                ASSERT( iCol < m_aaptTopPolygon[iRow].size() );
                
                glColor4fv( &m_aafColWebbing[iRow][ iCol ][0] );
                glNormal3dv( &m_aavecTopPolygonNorm[iRow][ iCol ][0] );
                glVertex3dv( &m_aaptTopPolygon[iRow][ iCol ][0] );
            }
            
            for ( int j = 3; j > 0; j-- ) {
                const int &iRow = iR + iptRow[j-1];
                const int &iCol = ipt[j];
                ASSERT( iCol < m_aaptBotPolygon[iRow].size() );
                
                glColor4fv( &m_aafColWebbing[iRow][ iCol ][0] );
                glNormal3dv( &m_aavecBotPolygonNorm[iRow][ iCol ][0] );
                glVertex3dv( &m_aaptBotPolygon[iRow][ iCol ][0] );
            }
        }
    }
    glEnd();
}


/// Draw with given style in g_drawState
void IntersectionDrawData::Draw( ) const
{
    switch( g_drawState.m_drawingStyle ) {
        case DrawState::POINTS_OR_LINES :
            g_drawState.StartShader( DrawState::LINE_SHADING );
            if ( g_drawState.m_opUI->m_bShowPoints->value() ) {
                DrawPointsWebbing( );
                DrawPointsSpokes( );
            }
            if ( g_drawState.m_opUI->m_bShowLines->value() ) {
                DrawLineWebbing( );
                DrawLineSpokes( );
            }
            g_drawState.EndShader();
            break;
        case DrawState::WEBBING :
            g_drawState.StartShader( DrawState::LINE_SHADING );
            DrawSpokes();
            DrawWebbing();
            g_drawState.EndShader();
            break;
        default :
            break;
    }
}

/// Draw as a shadow (projected and black)
void IntersectionDrawData::DrawShadow( ) const
{
	if ( ! g_drawState.m_opUI->m_bShowShadows->value() ) {
        return;
    }
    
    g_drawState.StartShader( DrawState::SHADOW_SHADING );
    
    glDepthFunc( GL_LEQUAL );
    for ( int iDim = 0; iDim < 3; iDim++ ) {
        if ( iDim == 2 && !g_drawState.m_opUI->m_bShowFloorPlane->value() ) {
            break;
        }
        if ( iDim < 2 && !g_drawState.m_opUI->m_bShowShadowBox->value() ) {
            continue;
        }
        
        /*
        const std::vector< R3Pt > &aptTop = m_aaptTopShadow[iDim];
        const std::vector< R3Pt > &aptBot = m_aaptBotShadow[iDim];
        const std::vector< R3Pt > &aptMid = m_aaptMidShadow[iDim];
        
        glBegin( GL_QUAD_STRIP );
        
        for ( int i = 0; i < aptTop.size(); i++  ) {    
            glColor4f( 0.0, 0.0, 0.2, 1.0 );
            glTexCoord2f( 1.0f, 0.0f );    
            glVertex3dv( &(aptTop[i])[0] );
            
            glTexCoord2f( 0.0f, 0.0f );    
            glVertex3dv( &(aptMid[i])[0] );
        }
        
        glEnd();
        
        glBegin( GL_QUAD_STRIP );
        
        for ( int i = 0; i < aptTop.size(); i++  ) {    
            glTexCoord2f( 0.0f, 0.0f );    
            glVertex3dv( &(aptMid[i])[0] );
            
            glTexCoord2f( 1.0f, 0.0f );    
            glVertex3dv( &(aptBot[i])[0] );
        }
        
        glEnd();
         */
	}
    
    g_drawState.EndShader();
}
