/*
 *  ScreenCurve_CopyEdit.cpp
 *  SketchCurves
 *
 *  Created by Cindy Grimm on 5/19/11.
 *  Copyright 2011 Adobe. All rights reserved.
 *
 */


#include "ScreenCurve.h"

const bool s_bTrace = false;

/*
 * Add a point and update:
 *   Bounding box (ptLowerLeft/UpperRight)
 *   Arc-length parameterization (m_adDistAlong, m_dLength)
 *   Time stamps
 */
void ScreenCurve::AddPoint( const R2Pt &in_pt, const double in_dTimeStamp )
{
    if ( m_apt.num() && ApproxEqual( in_pt, m_apt.last() ) )
        return;
    
    if ( m_apt.num() == 0 ) {
        m_dStartTime = in_dTimeStamp;
        m_dLength = 0.0;
        m_ptLowerLeft = R2Pt( 1e30, 1e30 );
        m_ptUpperRight = R2Pt( -1e30, -1e30 );
    }
    m_dEndTime = in_dTimeStamp;
    
    m_apt += in_pt;
    m_adDistAlong += 0.0;
    
    for ( int i = 0; i < 2; i++ ) {
        m_ptLowerLeft[i] = WINmin( in_pt[i], m_ptLowerLeft[i] );
        m_ptUpperRight[i] = WINmax( in_pt[i], m_ptUpperRight[i] );
    }
    
    const int iN = m_apt.num();
    if ( iN == 2 ) {
        m_adDistAlong[0] = 0.0;
        m_adDistAlong[1] = ::Length( m_apt[1] - m_apt[0] );
		m_dLength = m_adDistAlong[1];
    } 
    
    if ( iN > 2 ) {
        const double dLen = ::Length( m_apt[iN-1] - m_apt[iN-2] );
        m_adDistAlong[iN-1] = m_adDistAlong[iN-2] + dLen;
        m_dLength += dLen;
    }
}

/* Zero it all out */
void ScreenCurve::Clear()
{
	m_ptLowerLeft = R2Pt( 0.0, 0.0 );
	m_ptUpperRight = R2Pt( 0.0, 0.0 );

    m_apt.need(0);
    m_adDistAlong.need(0);
    m_dStartTime = m_dEndTime = 0;
}

/*
 * Average each point with its neighbors, leaving the end points alone
 * Does NOT fix m_adDistAlong/m_dLength/m_ptLowerLeft/m_ptUpperRight, so these
 * may be slightly off.
 *
 * Primarily used to clean up the curve a bit for analysis (not construction of the 3D curve).
 */
void ScreenCurve::Smooth( const int in_iLoops )
{
	Array<R2Pt> apt = m_apt;
    
    for ( int iL = 0; iL < in_iLoops; iL++ ) {
        for ( int i = 1; i < apt.num() - 1; i++ ) {
            m_apt[i] = Lerp( apt[i], Lerp( apt[i-1], apt[i+1], 0.5 ), 0.8 );
        }
        apt = m_apt;
    }
    
    Clear();
    for ( int i = 0; i < apt.num(); i++ ) {
        AddPoint( apt[i] );
    }
}

/*
 * Same as above, but only average the first and last 5 points. Poor man's hook/foldback removal
 */
void ScreenCurve::SmoothEnds(const int in_iLoops)
{
	Array<R2Pt> apt = m_apt;
    for ( int iL = 0; iL < in_iLoops; iL++ ) {
        for ( int i = 1; i < WINmin(5, m_apt.num() - 1); i++ ) {
            m_apt[i] = Lerp( apt[i], Lerp( apt[i-1], apt[i+1], 0.5 ), 0.8 );
        }
        for ( int i = WINmax( 1, m_apt.num() - 6 ); i < m_apt.num() - 1; i++ ) {
            m_apt[i] = Lerp( apt[i], Lerp( apt[i-1], apt[i+1], 0.5 ), 0.8 );
        }
        apt = m_apt;
    }
    
    Clear();
    for ( int i = 0; i < apt.num(); i++ ) {
        AddPoint( apt[i] );
    }
}

void ScreenCurve::MakeUnitLength( const bool in_bRecenter )
{
    double dLen = Length();
    double dLenBest = dLen;
    Array<R2Pt> apt = m_apt;
    
    if ( RNIsZero( dLen ) ) {
        cerr << "ERR: ScreenCurve::MakeUnitLength, Length is zero\n";
        return;
    }
    
    const R2Pt ptCenter = CenterPoint();
    double dScl = 1.0 / dLen;
    for ( int i = 0; i < m_apt.num(); i++ ) {
        m_apt[i] = R2Pt(0,0) + ( m_apt[i] - ptCenter );
        m_apt[i] = R2Pt( m_apt[i][0] * dScl, m_apt[i][1] * dScl );
    }
    dLen = 0.0;
    for ( int i = 0; i < m_apt.num() - 1; i++ ) {
        dLen += ::Length( m_apt[i+1] - m_apt[i] );
    }
    if ( fabs( dLen - 1.0 ) < fabs( dLenBest - 1.0 ) ) {
        dLenBest = dLen;
        apt = m_apt;
    }
    
    double dSclShrink = 0.9;
    double dSclGrow = 1.1;
    bool bShrunkLast = dLen > 1.0;
    int iCount = 50;
    
    while ( !RNApproxEqual( dLen, 1.0 ) && iCount-- > 0 ) {
        if ( dLen < 1.0 ) {
            dScl = dSclGrow;
            bShrunkLast = false;
        } else {
            dScl = dSclShrink;
            bShrunkLast = true;
        }
        for ( int i = 0; i < m_apt.num(); i++ ) {
            m_apt[i] = R2Pt( m_apt[i][0] * dScl, m_apt[i][1] * dScl );
        }
        dLen = 0.0;
        for ( int i = 0; i < m_apt.num() - 1; i++ ) {
            dLen += ::Length( m_apt[i+1] - m_apt[i] );
        }
        cout << "dLen " << dLen << " " << dSclShrink << " " << dSclGrow << "\n";
        if ( fabs( dLen - 1.0 ) < fabs( dLenBest - 1.0 ) ) {
            dLenBest = dLen;
            apt = m_apt;
        } else {
            m_apt = apt;
            dLen = dLenBest;
            
            if ( bShrunkLast == true ) {
                dSclShrink = 0.2 * dSclShrink + 0.8 * 1.0;
            } else {
                dSclGrow = 0.2 * dSclGrow + 0.8 * 1.0;
            }
        }
    }
    
    const R2Vec vecCenter = ( in_bRecenter == true ) ? ptCenter - R2Pt(0,0) : R2Vec(0,0);
    
    Clear();
    for ( int i = 0; i < apt.num(); i++ ) {
        AddPoint( apt[i] + vecCenter );
    }
}

/* Note: Doesn't copy gesture information, just the stuff set in AddPoint */
const ScreenCurve &ScreenCurve::operator=( const ScreenCurve &in_crv )
{
    m_apt = in_crv.m_apt;
    m_adDistAlong = in_crv.m_adDistAlong;
    
    m_dStartTime = in_crv.m_dStartTime;
    m_dEndTime = in_crv.m_dEndTime;
    m_dLength = in_crv.m_dLength;
    m_ptLowerLeft = in_crv.m_ptLowerLeft;
    m_ptUpperRight = in_crv.m_ptUpperRight;
    
    return *this;
}

/*
 * Add in evenly-spaced points between existing points
 * Basically makes sure that the spacing is at least in_dGapSize
 */
ScreenCurve ScreenCurve::FillGaps( const double in_dGapSize ) const
{
	ScreenCurve crvNew;
    for ( int iP = 0; iP < m_apt.num() - 1; iP++ ) {
        const double dLen = ::Length( m_apt[iP+1] - m_apt[iP] );
		const int iDivs = dLen / in_dGapSize;
		crvNew.AddPoint( m_apt[iP] );
		for ( int i = 0; i < iDivs; i++ ) {
			crvNew.AddPoint( Lerp( m_apt[iP], m_apt[iP+1], (i+1) / (iDivs + 1.0) ) );
		}
	}
	
	if ( m_apt.num() )
		crvNew.AddPoint( m_apt.last() );
	
	return crvNew;
}

/* Flip the order. Probably could be a bit smarter and actually
 * flip all the data instead of just calling AddPoint
 */
ScreenCurve ScreenCurve::Reverse( ) const
{
	ScreenCurve crv;
	for ( int i = m_apt.num() - 1; i >= 0; i-- ) {
		crv.AddPoint( m_apt[i] );
	}
	
	return crv;
}


