/*
 *  CurveOverlap_DecisionTree.cpp
 *  SketchCurves
 *
 *  Created by Cindy Grimm on 1/25/11.
 *  Copyright 2011 Adobe Corp.. All rights reserved.
 *
 */



#include "CurveOverlap.h"
#include "Curve.h"
#include "ScreenCurve.h"
#include "ParamFile.h"
#include "UserInterface.h"

/* True if any point is close enough */
bool CurveOverlap::Overlaps() const
{
	
	if ( RNApproxEqual(m_dTMin, m_dTMax, 1e-6) ) {
		if (s_bTraceTree) cout << "T values all same ";
		return false;
	}
	
	if ( m_strokeEnd1.IsOverlapped() == false && m_strokeEnd2.IsOverlapped() == false ) {
		if ( s_bTraceTree ) cout << "Ends don't overlap\n";
		return false;
	}
	
	return true;	
}


/* Check for a join 
 * Looking for the start of the stroke to be in a cone of the curve tangent and not too far away
 * if in_bAllowBackwardJoin is true, then we search for all possible joins/combinations
 *  otherwise, just look for the end of the curve joining the start of the stroke
 */
bool CurveOverlap::IsJoin( const bool in_bAllowBackwardJoin ) 
{
	if ( in_bAllowBackwardJoin == false ) {
		if ( m_strokeJoinEnd1.IsJoined() && !m_strokeJoinEnd1.m_bStrokeReversed ) {
			SetJoinInfo( START_CURVE, true );
			return true;
		}
		return false;
	}
	if ( m_strokeJoinEnd1.IsJoined() ) {
		SetJoinInfo( START_CURVE, true );
		return true;
	}
	if ( m_strokeJoinEnd2.IsJoined() ) {
		SetJoinInfo( END_CURVE, true );
		return true;
	}
	return false;
}

/* Are all of the points in the stroke within select distance of the curve?
 * A little bit of leeway for one or two points to come off a bit.
 * If the stroke passes over the end of the curve then this will be a merge
 */
bool CurveOverlap::AllOfStrokeOverlapsCurve(  ) const
{
	const double dDistSel = g_drawState.ScreenSelectionSize();
	const double dDistLeeway = g_paramFile.GetDouble("OverlapLeeway");
	const double dDistMax = g_paramFile.GetDouble("OverlapMax");
	
	double dMaxDistNotOverlap = 0.0, dSumLenOff = 0.0;
	
	for ( int i = 0; i < m_adDistToCurve.size(); i++ ) {
		if ( m_adDistToCurve[i] > dDistSel ) {
			// Be a little generous
			dMaxDistNotOverlap = WINmax( dDistSel, dMaxDistNotOverlap );

			// Definitely far away
			if ( m_adDistToCurve[i] > dDistLeeway * dDistSel ) {
				if ( s_bTraceTree ) cout << "More than dist " << m_adDistToCurve[i] << " ";
				return false;
			}
			dSumLenOff +=  ::Length(m_crvStroke.Pt(i) - m_crvStroke.Pt( WINmax(i,0) ));
			
			if ( dSumLenOff > dDistMax * dDistSel ) {
				if ( s_bTraceTree ) cout << "More than len " << dSumLenOff << " ";
				return false;
			}
		} else {
			dSumLenOff = 0.0;
		}
	}

	return true;		
}

/* Does the stroke roughly start and stop at the same point as the screen curve?
 * Want to correctly handle the case where someone's scratched back and forth
 * Also need to make sure that every point on the curve is close to the stroke
 */
bool CurveOverlap::StrokeCoversCurve(  ) const
{
	const double dDistSel = g_drawState.ScreenSelectionSize();
	const double dDistLeeway = g_paramFile.GetDouble("StrokeCoversLeeway");

	double dDist = 0.0;
	for ( int i = 0; i < m_crvProjected.NumPts(); i++ ) {
		m_crvStroke.ClosestPointTValue( m_crvProjected.Pt(i), dDist );
		if ( dDist > dDistSel * dDistLeeway ) {
			return false;
		}
	}

	// Easy case
	if ( RNIsZero(m_dTMin) && RNApproxEqual(m_dTMax, 1.0 ) ) {
		if ( s_bTraceTree ) cout << "Stroke covers curve\n";
		return true;
	}
	
	// Now look for amount of overhang from the ends of the curve to the stroke
	double dDistE1 = 0.0;
	double dDistE2 = 0.0;
	m_crvStroke.ClosestPointTValue( m_crvProjected( 0.0 ), dDistE1 );
	m_crvStroke.ClosestPointTValue( m_crvProjected( 1.0 ), dDistE2 );
		
	if ( dDistE1 < dDistSel * dDistLeeway && dDistE2 < dDistSel * dDistLeeway ) {
		if ( s_bTraceTree ) cout << "Stroke covers curve\n";
		return true;
	}
	if ( s_bTraceTree ) cout << "Dist " << dDistE1 << " " << dDistE2 << " " << dDistSel << " ";
	
	return false;
}

/* Look at the overlap region. Follow it along the curve. It's an overhang
 * if the stroke projects to the end of the curve without folding over.
 * Checks both ends and returns if none, one, or both overhang
 */
CurveOverlap::CurveEndType CurveOverlap::FindOverhangs() const
{
    const double dDistSel = g_drawState.ScreenSelectionSize();
    const double dTWithinEnd = (dDistSel * g_paramFile.GetDouble("OverhangLeeway")) / m_crvProjected.Length();
	
    CurveOverlap::CurveEndType et = NO_ENDS;
    
    double dTOnSeg = 0.0, dTDistToSeg = 0.0;
    R2Pt ptOut;
    if ( m_strokeEnd1.IsOverlapped() ) {
        // Middle of the part of the curve the stroke projects to
        const double dTCrvJoin = 0.5 * ( m_strokeEnd1.m_dCrv.first + m_strokeEnd1.m_dCrv.second );
        // End of curve in the direction the stroke is going
        const double dTCrvEnd = ( m_strokeEnd1.m_dCrv.first < m_strokeEnd1.m_dCrv.second ) ? 1.0 : 0.0;
        // Correspondning points
        const R2Pt ptCrvJoin = m_crvProjected( dTCrvJoin );
        const R2Pt ptCrvEnd = m_crvProjected( dTCrvEnd );
        // Segment approximating the end of the curve
        const R2Line_seg segCurve( ptCrvJoin, ptCrvEnd );
        // End of stroke
        const R2Pt ptStrokeEnd = m_crvStroke(1.0);

        // Quick check - if end points close, then just call it an overlap
        if ( ::Length( ptCrvEnd - ptStrokeEnd ) < 2.0 * dDistSel ) {
            et = START_CURVE;
        } else if ( m_crvStroke.Length() - 2.0 * dDistSel > segCurve.Length() ) {
            // If the stroke is shorter than what's left of the curve, then can't overlap ends, so only check if that's not the case
        
            // Now project stroke end point onto approximation of the end of the curve
            segCurve.FindPtOnSeg( ptStrokeEnd, ptOut, dTOnSeg, dTDistToSeg ) ;
            // If overlap, then projects past end of segment. Also check if projected point is within distsel of end of curve
            if ( dTOnSeg >= 1.0 ) {
                et = START_CURVE;
            } else {
                // Look at the projected distance to the curve versus the distance to the end of the curve
                const double dTLenToEnd = ::Length( ptOut - ptCrvEnd );
                
                if ( dTLenToEnd < dTDistToSeg + dDistSel ) {
                    et = START_CURVE;
                }
            }

            // Just a final check to see if they ran off the end of the curve at some point
            int iPassedEnd = -1;
            
            for ( int i = 1; i < m_adTsOnCurve.size(); i++ ) {
                // Projects close enough to end - call it a yes
                if ( m_adTsOnCurve[i] <= dTWithinEnd && m_strokeEnd1.m_bStrokeReversed ) {
                    iPassedEnd = i;
                    break;
                }
                if ( m_adTsOnCurve[i] >= 1.0 - dTWithinEnd && !m_strokeEnd1.m_bStrokeReversed ) {
                    iPassedEnd = i;
                    break;
                }
            }
            // If monotonic (may be far away)
            if ( iPassedEnd != -1 ) {
                const std::pair<double,double> dTStart( 0.0, m_crvStroke.PercAlong(iPassedEnd) );
                if ( !IsFoldOver( MERGE, m_strokeEnd1.m_bStrokeReversed, START_CURVE, dTStart, dTStart, m_crvStroke ) ) {
                    et = START_CURVE;
                }
            }	
        }
    }
    if ( m_strokeEnd2.IsOverlapped() ) {
        // Middle of the part of the curve the stroke projects to
        const double dTCrvJoin = 0.5 * ( m_strokeEnd2.m_dCrv.first + m_strokeEnd2.m_dCrv.second );
        // End of curve in the direction the stroke is going
        const double dTCrvEnd = ( m_strokeEnd2.m_dCrv.first < m_strokeEnd2.m_dCrv.second ) ? 1.0 : 0.0;
        // Correspondning points
        const R2Pt ptCrvJoin = m_crvProjected( dTCrvJoin );
        const R2Pt ptCrvEnd = m_crvProjected( dTCrvEnd );
        // Segment approximating the end of the curve
        const R2Line_seg segCurve( ptCrvJoin, ptCrvEnd );
        // End of stroke
        const R2Pt ptStrokeEnd = m_crvStroke(0.0);
        
        // Quick check - if end points close, then just call it an overlap
        if ( ::Length( ptCrvEnd - ptStrokeEnd ) < 2.0 * dDistSel ) {
            if ( et == START_CURVE ) {
                et = BOTH_ENDS;
            } else {
                et = END_CURVE;
            }
        } else if ( m_crvStroke.Length() - 2.0 * dDistSel > segCurve.Length() ) {
            // If the stroke is shorter than what's left of the curve, then can't overlap ends, so only check if that's not the case
            
            // Now project stroke end point onto approximation of the end of the curve
            segCurve.FindPtOnSeg( ptStrokeEnd, ptOut, dTOnSeg, dTDistToSeg ) ;
            // If overlap, then projects past end of segment. Also check if projected point is within distsel of end of curve
            if ( dTOnSeg >= 1.0 ) {
                if ( et == START_CURVE ) {
                    et = BOTH_ENDS;
                } else {
                    et = END_CURVE;
                }
            } else {
                // Look at the projected distance to the curve versus the distance to the end of the curve
                const double dTLenToEnd = ::Length( ptOut - ptCrvEnd );
                
                if ( dTLenToEnd < dTDistToSeg + dDistSel ) {
                    if ( et == START_CURVE ) {
                        et = BOTH_ENDS;
                    } else {
                        et = END_CURVE;
                    }
                }
            }
            
            // Just a final check to see if they ran off the end of the curve at some point
            int iPassedEnd = -1;
            for ( int i = m_adTsOnCurve.size() - 2; i >= 0; i-- ) {
                // Projects close enough to end - call it a yes
                if ( m_adTsOnCurve[i] <= dTWithinEnd && !m_strokeEnd2.m_bStrokeReversed ) {
                    iPassedEnd = i;
                    break;
                }
                if ( m_adTsOnCurve[i] >= 1.0 - dTWithinEnd && m_strokeEnd2.m_bStrokeReversed ) {
                    iPassedEnd = i;
                    break;
                }
            }
            // If monotonic (may be far away)
            if ( iPassedEnd != -1 ) {
                const std::pair<double,double> dTStart( m_crvStroke.PercAlong(iPassedEnd), 1.0 );
                if ( !IsFoldOver( MERGE, m_strokeEnd2.m_bStrokeReversed, END_CURVE, dTStart, dTStart, m_crvStroke ) ) {
                    if ( et == START_CURVE ) {
                        et = BOTH_ENDS;
                    } else {
                        et = END_CURVE;
                    }
                }
            }			
        }
    }
    
	if ( s_bTraceTree ) cout << " Overhangs " << et << "\n";
	
    return et;
}

/*
 * Special case of above for when the stroke closes the curve
 * In this case, the ends can be merged or joined
 * Need to go over both open ends of the curve, oriented correctly
 */
bool CurveOverlap::Closes() 
{
    double dT0 = 0.0, dT1 = 0.0, dT2 = 0.0, dT3 = 0.0;
	if ( m_strokeEnd1.IsOverlapped() && m_strokeEnd1.IsMergeEnd() ) {
        dT0 = m_strokeEnd1.m_dCrv.first;
        dT1 = m_strokeEnd2.m_dCrv.second;
		if ( m_strokeEnd2.IsOverlapped() && m_strokeEnd2.IsMergeEnd() ) {
            // Not oriented the same way
			if ( m_strokeEnd1.m_bStrokeReversed != m_strokeEnd2.m_bStrokeReversed ) {
				return false;
			}
            dT2 = m_strokeEnd2.m_dCrv.second;
            dT3 = m_strokeEnd2.m_dCrv.first;

			if ( s_bTraceTree ) cout << "Merge merge ";
		} else if ( m_strokeJoinEnd2.IsJoined() && !m_bCurveClosed ) {
            // Not oriented the same way
			if ( m_strokeEnd1.m_bStrokeReversed != m_strokeJoinEnd2.m_bStrokeReversed ) {
				return false;
			}
            dT2 = dT3 = m_strokeJoinEnd2.m_dCrvT;
			if ( s_bTraceTree ) cout << "Merge join ";
		} else {
			return false;
		}
        
        if ( m_strokeEnd1.m_bStrokeReversed ) {
            if ( dT0 > dT3 ) {
                return false;
            }
        } else {
            if ( dT0 < dT3 ) {
                return false;
            }
        }
		SetJoinInfo( START_CURVE, false );

	} else if ( m_strokeJoinEnd1.IsJoined()  && !m_bCurveClosed ) {
        dT0 = dT1 = m_strokeJoinEnd1.m_dCrvT;
		if ( m_strokeEnd2.IsOverlapped() && m_strokeEnd2.IsMergeEnd() ) {
            // Not oriented the same way
			if ( m_strokeJoinEnd1.m_bStrokeReversed != m_strokeEnd2.m_bStrokeReversed ) {
				return false;
			}
            
            dT2 = m_strokeEnd2.m_dCrv.second;
            dT3 = m_strokeEnd2.m_dCrv.first;

			if ( s_bTraceTree ) cout << "Join merge ";
		} else if ( m_strokeJoinEnd2.IsJoined() && !m_bCurveClosed ) {
            // Not oriented the same way
			if ( m_strokeJoinEnd1.m_bStrokeReversed != m_strokeJoinEnd2.m_bStrokeReversed ) {
				return false;
			}
            dT2 = dT3 = m_strokeJoinEnd2.m_dCrvT;
			if ( s_bTraceTree ) cout << "Join join ";
		} else {
			return false;
		}
        if ( m_strokeJoinEnd1.m_bStrokeReversed ) {
            if ( dT0 > dT3 ) {
                return false;
            }
        } else {
            if ( dT0 < dT3 ) {
                return false;
            }
        }
		SetJoinInfo( START_CURVE, true );
	} else {
        if ( s_bTraceTree ) cout << "Close: Nothing\n";
		return false;
	}
    
    if ( s_bTraceTree ) cout << "Closed " << dT0 << " " << dT1 << " " << dT2 << " " << dT3 << "\n";;
	return true;
}

/* Know all of stroke projects to curve - just see if it closes (crosses end points) or not */
bool CurveOverlap::CoversEndOfCurve() 
{
    const double dT0 = m_strokeEnd1.m_dCrv.first;
    const double dT3 = m_strokeEnd2.m_dCrv.first;

    if ( m_strokeEnd1.m_bStrokeReversed ) {
        if ( dT0 > dT3 ) {
            return false;
        }
    } else {
        if ( dT0 < dT3 ) {
            return false;
        }
    }
    
    if ( !m_strokeEnd1.IsOverlapped() ) {
        m_strokeEnd1.ForceOverlapped();
    }
    if ( !m_strokeEnd2.IsOverlapped() ) {
        m_strokeEnd2.ForceOverlapped();
    }
    SetJoinInfo( START_CURVE, false );
    return true;
}

/* Special case if the input curve is closed
 * Returns the overlap type.
 * Sets: m_dTsStroke/CurveFirst/SecondJoin
 *       m_bReverseStroke
 *       m_crv/strokeEnd
 *
 * If the input curve is closed then the only option is an overstroke. 
 * Note that a closing stroke is the same as an overstroke, it's just over the ends of the curve
 */
CurveOverlap::OverlapType CurveOverlap::DecisionTreeClosedCurve( ) 
{
	if ( m_strokeEnd1.IsOverlapped() && m_strokeEnd2.IsOverlapped() ) {
		
		if ( m_strokeEnd1.m_bStrokeReversed && m_strokeEnd2.m_bStrokeReversed ) {
			m_bReverseStroke = true;
		} else if ( m_strokeEnd1.m_bStrokeReversed == false && m_strokeEnd2.m_bStrokeReversed == false  ) {
			m_bReverseStroke = false;
		} else {
			if ( s_bTraceTree ) cout << " Orientation reversed\n";
			return BAD_OVERLAP;
		}
		if ( s_bTraceTree ) cout << " Overstroke\n";
		if ( Closes() ) {
			return CLOSES;
		}
		return OVERSTROKE;
	}
	
	/* Check to see if the stroke overlaps the break in the curve. If we have a 
	 * pure overstroke than there should be two overhangs, ie., it looks like
	 * an overhang from either direction. If we have a partial overstroke then
	 * only one end (the one that's overlapped) will be labeled as an overhang
	 * if the stroke crosses the curve break */
	const CurveOverlap::CurveEndType crvET = FindOverhangs();
	if ( crvET == BOTH_ENDS ) {
		if ( Closes() ) {
            if ( s_bTraceTree ) cout << " Closes\n";
			return CLOSES;
		}
		if ( s_bTraceTree ) cout << " Overstroke\n";
		return OVERSTROKE;
	} 
	
	if ( g_drawState.m_opUI->m_bAllowPartialOverstroke->value() ) {
		if ( m_strokeEnd1.IsOverlapped() ) {
			SetJoinInfo( START_CURVE, false );
		} else if ( m_strokeEnd2.IsOverlapped() ) {
			SetJoinInfo( END_CURVE, false );
		} else {
			if ( s_bTraceTree ) cout << " No end reversed\n";
			return BAD_OVERLAP;
		}
		
		if ( m_strokeEnd1.m_bStrokeReversed && m_strokeEnd2.m_bStrokeReversed ) {
			m_bReverseStroke = true;			
			if ( crvET == START_CURVE || crvET == END_CURVE ) {
				return CLOSES;
			} else {
				return OVERSTROKE_START;
			}
		} else if ( m_strokeEnd1.m_bStrokeReversed == false && m_strokeEnd2.m_bStrokeReversed == false  ) {
			m_bReverseStroke = false;
			if ( crvET == START_CURVE || crvET == END_CURVE ) {
				return CLOSES;
			} else {
				return OVERSTROKE_START;
			}
		} else {
			if ( s_bTraceTree ) cout << " Orientation reversed, one end pinned\n";
			return BAD_OVERLAP;
		}
	}
		
	if ( s_bTraceTree ) cout << " Nothing valid\n";
	return BAD_OVERLAP;
}

/*
 * Do we have a partial overstroke? Ie, the stroke starts in the middle?
 */
bool CurveOverlap::StrokeInMiddle() const
{
	double dLenFromEnd = 0.0;
	if ( m_strokeEnd1.m_dAvgDist < m_strokeEnd2.m_dAvgDist ) {
		if ( m_adTsOnCurve[0] < 0.5 ) {
			dLenFromEnd = m_adTsOnCurve[0] * m_crvProjected.Length();
		} else {
			dLenFromEnd = (1.0 - m_adTsOnCurve[0]) * m_crvProjected.Length();
		}
	} else {
		if ( m_adTsOnCurve.back() < 0.5 ) {
			dLenFromEnd = m_adTsOnCurve.back() * m_crvProjected.Length();
		} else {
			dLenFromEnd = (1.0 - m_adTsOnCurve.back()) * m_crvProjected.Length();
		}
	}
				
	const double dClip = WINmin( g_drawState.ScreenSelectionSize(), m_crvProjected.Length() * 0.1 );
	if ( dLenFromEnd < dClip ) {
		if ( s_bTraceTree ) cout << "Stroke in middle not!\n";
		return false;
	} 
	
	return true;	
}

/* Walk down the decision tree, see OverlapDecisionTree.psd
 * Returns the overlap type.
 * Sets: m_dTsStroke/CurveFirst/SecondJoin
 *       m_bReverseStroke
 *       m_crv/strokeEnd
 *
 * in_bAllowBackwardJoin is true for when we're evaluating how well this stroke merges with the last stroke (not an entire curve)
 * Things that are not allowed in this case: joining/merging with the start of the curve, partial overstroke
 *
 * The tricky part here is when there isn't a clear winner. Overstrokes are pretty obvious:
 * both ends merge with the curve. Joins are pretty obvious: No merge, no overlap, but ends
 * join nicely. Some merges are "nice", they overhang and start at one end of the curve. The
 * question is what to do if it doesn't really overhang, but it overlaps? Most of these weird cases
 * end up getting flagged as partial overstrokes.
 *
 * The other tricky thing is making sure the secondary data (m_dTs, m_bStroke, stroke/curve ends) is set correctly
 * This is largely handled now by the SetJoinInfo call here; but there are probably still a few cases
 * where that info is set in one of the Is??? routines. EG, the Closes routine sets that data. This is not the 
 * best way to do it, since calling Closes will wipe out that data, but...
 *
 * There's UI code in here because the UI state has some this is allowed/not allowed flags. Could
 * stick it elsewhere, or pass in the state, or something cleaner, but hey...
 */
CurveOverlap::OverlapType CurveOverlap::DecisionTree( const bool in_bAllowBackwardJoin ) 
{
	m_bReverseStroke = false;
	m_strokeEnd = m_curveEnd = NO_ENDS;
    const double dDistSel = g_drawState.ScreenSelectionSize();
	
	cout.precision(3);
	if ( s_bTraceTree ) cout << "Selection distance " << dDistSel << "\n";
	
	if ( m_bCurveClosed == true ) {
		if ( s_bTraceTree ) cout << "Curve closed: ";
		if ( in_bAllowBackwardJoin == false ) {
			return BAD_OVERLAP; // Should never really be here, but just to be safe...
		}
		return DecisionTreeClosedCurve();
	}
	if ( Overlaps() ) {
		if ( s_bTraceTree ) cout << "Overlaps: ";
		
		if ( AllOfStrokeOverlapsCurve( ) ) {

			if ( s_bTraceTree ) cout << "All stroke: ";

			SetJoinInfo( START_CURVE, false );
			// Only do a blend with the last stroke
			if ( StrokeCoversCurve( ) && g_drawState.m_opUI->m_bAllowBlends->value() && in_bAllowBackwardJoin == false ) {
				return BLEND;
            } else if ( m_strokeEnd1.m_bStrokeReversed != m_strokeEnd2.m_bStrokeReversed ) {
                return BAD_OVERLAP;
			} else {
				if ( m_adTsOnCurve[0] > m_adTsOnCurve.back() ) {
					m_bReverseStroke = true;
				}
				if ( CoversEndOfCurve() ) {
					return CLOSES;
				}
				return OVERSTROKE;
			}
		} else {
			if ( s_bTraceTree ) cout << "Partial stroke: ";
			if ( m_strokeEnd1.IsOverlapped() && m_strokeEnd2.IsOverlapped() ) {
				
				if ( m_strokeEnd1.m_bStrokeReversed && m_strokeEnd2.m_bStrokeReversed ) {
					m_bReverseStroke = true;
				} else if ( m_strokeEnd1.m_bStrokeReversed == false && m_strokeEnd2.m_bStrokeReversed == false  ) {
					m_bReverseStroke = false;
				} else {
					if ( s_bTraceTree ) cout << " Orientation reversed\n";
					return BAD_OVERLAP;
				}
				if ( s_bTraceTree ) cout << " Overstroke\n";

				if ( Closes() ) {
					return CLOSES;
				}
				SetJoinInfo( START_CURVE, false );
				return OVERSTROKE;
				
			} else if ( m_strokeEnd1.IsOverlapped() == false && m_strokeEnd2.IsOverlapped() == false ) {
				if ( s_bTraceTree ) cout << " Ends don't overlap\n";
				return BAD_OVERLAP;
			}
			
			CurveOverlap::CurveEndType crvET = FindOverhangs();
			if ( crvET != NO_ENDS ) {
				if ( s_bTraceTree ) cout << "Overhangs: ";
				if ( Closes() ) {
					return CLOSES;
				}
				if ( crvET == BOTH_ENDS ) {
					
					// pick better end
					if ( m_strokeEnd1.m_dAvgDist < m_strokeEnd2.m_dAvgDist ) {
						crvET = START_CURVE;
					} else {
						crvET = END_CURVE;
					}
				}
				if ( crvET == START_CURVE ) {
					SetJoinInfo( START_CURVE, false );

					return MERGE;
				}
				if ( crvET == END_CURVE ) {
					SetJoinInfo( END_CURVE, false );
					return MERGE;
				}
				return BAD_OVERLAP;
				
			} else {
				if ( s_bTraceTree ) cout << "Interior: ";
				if ( m_strokeEnd1.m_dAvgDist < m_strokeEnd2.m_dAvgDist ) {
					SetJoinInfo( START_CURVE, false );
					const double dT = 0.5 * ( m_strokeEnd1.m_dCrv.first + m_strokeEnd1.m_dCrv.second );
					const double dLenLeft = (dT < 0.5) ? dT * m_crvProjected.Length() : (1.0 - dT) * m_crvProjected.Length();
                    const double dMergeLenCap = WINmin( g_paramFile.GetDouble("MergeCurvePerc") * m_crvProjected.Length(), g_paramFile.GetDouble("MergeNoOverhang") * dDistSel );
					if ( dLenLeft < dMergeLenCap && m_strokeEnd1.IsMergeEnd() ) {
						return MERGE;
					}
				} else {
					SetJoinInfo( END_CURVE, false );
					
					const double dT = 0.5 * ( m_strokeEnd2.m_dCrv.first + m_strokeEnd2.m_dCrv.second );
					const double dLenLeft = (dT < 0.5) ? dT * m_crvProjected.Length() : (1.0 - dT) * m_crvProjected.Length();
                    const double dMergeLenCap = WINmin( g_paramFile.GetDouble("MergeCurvePerc") * m_crvProjected.Length(), g_paramFile.GetDouble("MergeNoOverhang") * dDistSel );
					if ( dLenLeft < dMergeLenCap && m_strokeEnd2.IsMergeEnd() ) {
						return MERGE;
					}
				}
				if ( in_bAllowBackwardJoin == false && m_strokeEnd == END_CURVE ) {
					// Overstroke start that diverges in the direction of the start of the curve - not allowed for next stroke
					return BAD_OVERLAP;
				}
				if ( StrokeInMiddle() && g_drawState.m_opUI->m_bAllowPartialOverstroke->value() ) {
					SetJoinInfo( START_CURVE, false );
					return OVERSTROKE_START;
				}
				return BAD_OVERLAP;
			}
		}
		
	} else {
		if ( s_bTraceTree ) cout << "No overlap: ";
		const bool bAllowJoin = g_drawState.m_opUI->m_bAllowJoins->value() || ( g_drawState.m_opUI->m_bAllowJoinLast->value() && in_bAllowBackwardJoin == false );
		if ( bAllowJoin && IsJoin( in_bAllowBackwardJoin ) ) {
			if ( Closes() ) {
				return CLOSES;
			}
			return JOIN;
		}
		
		
		return NO_OVERLAP;
	} 
	return NO_OVERLAP;
}


/* Forced merge - figure out which end is best. Return score */
CurveOverlap::CurveEndType CurveOverlap::FindBestMerge() const
{
    const CurveEndType crvEnd = FindOverhangs();
    if ( crvEnd == START_CURVE ) {
		if ( s_bTraceTree ) cout << "Overhang start\n";
        return START_CURVE;
    } else if ( crvEnd == END_CURVE ) {
		if ( s_bTraceTree ) cout << "Overhang end\n";
        return END_CURVE;
    } else if ( crvEnd == BOTH_ENDS ) {
		if ( s_bTraceTree ) cout << "Overhang both\n";
        if ( m_strokeEnd1.m_dAvgDist < m_strokeEnd2.m_dAvgDist ) {
			return START_CURVE;
        } else {
			return END_CURVE;
        }
    } 

	// No end close - make some guesses
	const double dMaxDist = WINmax( m_strokeEnd1.m_dAvgDist, m_strokeEnd2.m_dAvgDist );
	const double dScoreE1 = m_strokeEnd1.m_dAvgDist / dMaxDist + fabs( m_strokeEnd1.m_dAvgAng ) / M_PI;
	const double dScoreE2 = m_strokeEnd2.m_dAvgDist / dMaxDist + fabs( m_strokeEnd2.m_dAvgAng ) / M_PI;
	if ( dScoreE1 < dScoreE2 ) {
		if ( s_bTraceTree ) cout << "Overhang score E1 " << dScoreE1 << " " << dScoreE2 << "\n";
		return START_CURVE;
	}
	if ( s_bTraceTree ) cout << "Overhang score E2 " << dScoreE1 << " " << dScoreE2 << "\n";
	return END_CURVE;
}

/* A modified version of the overlap decision tree.
 * Try all four end combinations (start of stroke with end of curve, etc)
 * Look for: good joins
 *           good merge (increase search distance)
 *
 * TODO: If none of these is good, then figure out which ends match best and force the ends
 */
void CurveOverlap::FindBestMergeOrJoin( const bool in_bAllowClosed ) 
{
	if ( m_overlapType == JOIN || m_overlapType == MERGE ) {
		return;
	}
	if ( m_overlapType == CLOSES && in_bAllowClosed ) {
		return;
	}
	
    const double dDistSel = g_drawState.ScreenSelectionSize();

    const CurveEndType mergeCET = FindBestMerge();
	OverlapInfo &oi = (mergeCET == START_CURVE ) ? m_strokeEnd1 : m_strokeEnd2;

	JoinInfo joinEnd;
	CurveEndType joinCET = NO_ENDS;
	if ( m_strokeJoinEnd1.IsBetter( m_strokeJoinEnd2, s_bTraceTree ) ) {
		joinEnd = m_strokeJoinEnd1;
		joinCET = START_CURVE;
	} else {
		joinEnd = m_strokeJoinEnd2;
		joinCET = END_CURVE;
	}
		
    const double dIsMerge = g_paramFile.GetDouble("MergeVersusJoinDistance");
	if ( oi.IsOverlapped() && oi.IsMergeEnd() ) {
		if ( s_bTraceTree ) cout << " Is overlapped ";
		m_overlapType = MERGE;
	} else if ( ( oi.m_dCrv.second < 1e-6 || oi.m_dCrv.first > 1.0 - 1e-6 ) && ( oi.m_dStroke.second < 1e-6 || oi.m_dStroke.first > 1.0 - 1e-6 ) ) {
		if ( s_bTraceTree ) cout << " Is past ends ";
		m_overlapType = JOIN;
	} else if ( joinEnd.IsJoined( false ) ) {
		if ( s_bTraceTree ) cout << " Is joined ";
		m_overlapType = JOIN;
	} else if ( oi.IsOverlapped() && oi.m_dAvgDist < dIsMerge * dDistSel ) {
		if ( s_bTraceTree ) cout << " Is overlapped and close ";
        m_overlapType = MERGE;
    } else if ( joinEnd.m_dAngCrv < M_PI / 2.0 && joinEnd.m_dAngStroke < M_PI / 2.0 ) {
		if ( s_bTraceTree ) cout << " Joined, not backwards ";
        m_overlapType = JOIN;
    } else {
		if ( s_bTraceTree ) cout << " Bailing, merge ";
        m_overlapType = oi.Score() < joinEnd.Score() ? MERGE : JOIN;
    }
    
	// TODO: If merge/join cuts off a large amount of the curve, pick best end and force merge/join with just end
	
	if ( in_bAllowClosed && Closes() ) {
		m_overlapType = CLOSES;
    } else if ( m_overlapType == MERGE ) {
		SetOverlapData( oi, mergeCET, oi.m_dAvgDist * 1.2 );
		oi.Print();
		SetJoinInfo( mergeCET, false );
	} else if ( m_overlapType == JOIN ) {
		SetJoinInfo( joinCET, true );
	}

	SetJoinTValues();
	ConvertCurveJoinTValues();
    if ( s_bTraceMerge || s_bTraceJoin ) {
        cout << "FindBest: ";
        cout << (m_overlapType == JOIN ? "Join: " : "Merge: ") << oi.m_dAvgDist << " " << joinEnd.m_dDist << " ang " << joinEnd.m_dScore << " crv: ";
        PrintCurveEnd( m_curveEnd );
        cout << ", stroke: ";
        PrintCurveEnd( m_strokeEnd );
        cout << ", " << ( m_bReverseStroke ? " t\n" : " f\n" );
    }
}

/* A forced overstroke. Still have stroke ends; convert to t values */
void CurveOverlap::FindBestOverstroke()
{
	if ( m_overlapType == OVERSTROKE || m_overlapType == OVERSTROKE_START || m_overlapType == CLOSES ) {
		return;
	}
	m_overlapType = OVERSTROKE;
	
	const double dDistSel = g_drawState.ScreenSelectionSize();
	//const double dOverstrokeOverlap = g_paramFile.GetDouble("OverstrokeMinimum");

	double dSearchWidth = dDistSel;
	while ( RNIsZero( m_strokeEnd1.m_dCurveLength ) && dSearchWidth < 0.75 ) {
		SetOverlapData( m_strokeEnd1, CurveOverlap::START_CURVE, dSearchWidth );
		dSearchWidth += 0.5 * dDistSel;
	}
	if ( s_bTraceOverstroke ) cout << "End 1 search width " << dSearchWidth << "\n";

	dSearchWidth = dDistSel;
	while ( RNIsZero( m_strokeEnd2.m_dCurveLength ) && dSearchWidth < 0.75 ) {
		SetOverlapData( m_strokeEnd2, CurveOverlap::END_CURVE, dSearchWidth );
		dSearchWidth += 0.5 * dDistSel;
	}
	if ( s_bTraceOverstroke ) {
		cout << "End 2 search width " << dSearchWidth << "\n";
	
		m_strokeEnd1.Print();
		m_strokeEnd2.Print();
	}
	
	const CurveEndType mergeCET = FindBestMerge();
	SetJoinInfo( mergeCET, false);
	
	if ( m_bCurveClosed ) {
		if ( m_bReverseStroke == false ) {
			if ( m_strokeEnd1.m_dCrv.first > m_strokeEnd2.m_dCrv.second ) {
				m_overlapType = CLOSES;
				if ( s_bTraceTree ) cout << "Crosses join, closing 1\n";
			}
		} else {
			if ( m_strokeEnd2.m_dCrv.first > m_strokeEnd1.m_dCrv.second ) {
				m_overlapType = CLOSES;
				if ( s_bTraceTree ) cout << "Crosses join, closing 2\n";
			}
		}
	}
	m_strokeJoinEnd1.ZeroOut();
	m_strokeJoinEnd2.ZeroOut();
	
	if ( m_overlapType == OVERSTROKE ) {
		const OverlapInfo &oiE2 = ( ( m_strokeEnd == START_CURVE && m_bReverseStroke == false ) || ( m_strokeEnd == END_CURVE && m_bReverseStroke == true ) ) ? m_strokeEnd2 : m_strokeEnd1;
		if ( oiE2.m_dAvgDist > 3.0 * dDistSel && g_drawState.m_opUI->m_bAllowPartialOverstroke->value() ) {
			m_overlapType = OVERSTROKE_START;
		}
	}
	SetJoinTValues();
	ConvertCurveJoinTValues();
}

void CurveOverlap::ForceClosedCurve( const ScreenCurve &in_stroke )
{	
	TryClosedCurve( in_stroke, true );
	
	FindBestClosed();
}

void CurveOverlap::FindBestClosed()
{
	if ( m_overlapType == CLOSES ) {
		return;
	}
	
    FindBestMergeOrJoin(true);
	m_overlapType = CLOSES;
	
	SetJoinTValues();
	ConvertCurveJoinTValues();
}

void CurveOverlap::ForceStrokeEnd( const CurveEndType &in_endType ) 
{	
	if ( in_endType == START_CURVE ) {
		SetJoinInfo( in_endType, false );
	} else {
		SetJoinInfo( in_endType, false );
	}
}

/* Tricky part here is making sure that one stroke end gets assigned to one curve, the
 * other end to the other curve, and that all the orientations line up
 */
void CurveOverlap::FindBestCombined( CurveOverlap &io_oi )
{
	FindBestMerge();
	io_oi.FindBestMerge();
	
	m_overlapType = MERGE;
	io_oi.m_overlapType = MERGE;
	if ( Score() < io_oi.Score() ) {
		const bool bStartStroke = ( StrokeEnd() == START_CURVE && m_bReverseStroke == false ) || ( StrokeEnd() == END_CURVE && m_bReverseStroke == true );
		if ( bStartStroke ) {
			io_oi.ForceStrokeEnd( END_CURVE );
		} else {
			io_oi.ForceStrokeEnd( START_CURVE );
		}
		io_oi.SetJoinTValues();
		io_oi.ConvertCurveJoinTValues();
	} else {
		const bool bStartStroke = ( io_oi.StrokeEnd() == START_CURVE && io_oi.m_bReverseStroke == false ) || ( io_oi.StrokeEnd() == END_CURVE && io_oi.m_bReverseStroke == true );
		if ( bStartStroke ) {
			ForceStrokeEnd( END_CURVE );
		} else {
			ForceStrokeEnd( START_CURVE );
		}
		SetJoinTValues();
		ConvertCurveJoinTValues();
	}
	const OverlapInfo &oiE1 = ( ( m_strokeEnd == START_CURVE && m_bReverseStroke == false ) || ( m_strokeEnd == END_CURVE && m_bReverseStroke == true ) ) ? m_strokeEnd1 : m_strokeEnd2;
	const OverlapInfo &oiE2 = ( (io_oi.m_strokeEnd == START_CURVE && io_oi.m_bReverseStroke == false ) || ( io_oi.m_strokeEnd == END_CURVE && io_oi.m_bReverseStroke == true ) ) ? io_oi.m_strokeEnd2 : io_oi.m_strokeEnd1;

	PrintState();
	oiE1.Print();
	io_oi.PrintState();
	oiE2.Print();
}

