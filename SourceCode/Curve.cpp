/*
 *  Curve.cpp
 *  SketchCurves
 *
 *  Created by Cindy Grimm on 8/4/10.
 *  Copyright 2010 Washington University in St. Louis. All rights reserved.
 *
 */

#include "Curve.h"

const bool s_bTrace = false;

// Fix t/index values for closed curves
int Curve::IndexWrap( const int in_iPt ) const
{
    if ( IsClosed() ) {
        if ( in_iPt < 0 ) {
            return in_iPt + NumPts();
        } else if ( in_iPt >= NumPts() ) {
            return in_iPt - NumPts();
        }
    }
    
    return in_iPt;
}

// Fix t/index values for closed curves
int Curve::IndexClamp( const int in_iPt ) const
{
    if ( IsClosed() ) {
        if ( in_iPt < 0 ) {
            return WINminmax( in_iPt + NumPts(), 0, NumPts()-1 );
        } else if ( in_iPt >= NumPts() ) {
            return WINminmax( in_iPt - NumPts(), 0, NumPts()-1 );
        }
    }
    
    return WINminmax( in_iPt, 0, NumPts()-1 );
}

/* binary search for t value */
double Curve::TWrap( const double in_dT ) const
{
    if ( IsClosed() ) {
        if ( in_dT < 0.0 ) {
            return in_dT + 1.0;
        } else if ( in_dT > 1.0 ) {
            return in_dT - 1.0;
        }
    }
    
    return WINminmax( in_dT, 0.0, 1.0 );
}

/* binary search for t value */
R3Pt Curve::operator()( const double in_dT ) const
{
    if ( m_apt.num() == 0 ) return R3Pt(0,0,0);

    const std::pair<int,double> adT = BinarySearch( in_dT );
    return Lerp( m_apt[adT.first], m_apt.clamp(adT.first + 1), adT.second );
}

R3Vec Curve::Tangent( const double in_dT ) const
{
    if ( m_apt.num() == 0 ) return R3Vec(1,0,0);

    const double dT = TWrap( in_dT );
    
    const std::pair<int,double> adT = BinarySearch( dT );
    return UnitSafe( m_avecTangent[adT.first] * (1.0 - adT.second) + m_avecTangent.clamp(adT.first + 1) * adT.second );
}

double Curve::PercAlongWrap( const int in_iPt ) const 
{ 
    if ( in_iPt < 0 && in_iPt > -NumPts() ) {
        return m_adPercAlong[in_iPt + NumPts()] - 1.0; 
        
    } else if ( in_iPt >= NumPts() && in_iPt < 2 * NumPts() ) {
        return m_adPercAlong[in_iPt - NumPts()] + 1.0; 
        
    } else if ( in_iPt >= 0 && in_iPt < NumPts() ) {
        return m_adPercAlong[in_iPt]; 
    }
    cerr << "ERR: Curve::PercALongWrap, index not in valid range " << in_iPt << "\n";
    return 0.5;
}

const Curve &Curve::operator=( const Curve &in_crv )
{
    m_apt = in_crv.m_apt;
    m_adDist = in_crv.m_adDist;
    m_avecTangent = in_crv.m_avecTangent;
    m_adPercAlong = in_crv.m_adPercAlong;
    m_bPercData = in_crv.m_bPercData;
    m_bTangentData = in_crv.m_bTangentData;
	m_bbox = in_crv.m_bbox;
    
    return *this;
}

/* 
 * Resample the curve at the given percentage of the average segment length
 */
Curve Curve::ResampleRelativeSize( const double in_dPercOfAvg ) const
{
    Curve crvOut;
    if ( m_apt.num() < 3 )
        return crvOut;
    
    ASSERT( in_dPercOfAvg > 0.0 );
    
    Array<double> adDist( m_apt.num() );
    
    if ( m_bPercData == true ) {
        adDist = m_adDist;
    } else {
        double dSumLen = 0.0;    
        for ( int i = 0; i < m_apt.num() - 1; i++ ) {
            adDist[i] = ::Length( m_apt[i+1] - m_apt[i] );
            dSumLen += adDist[i];
        }
        adDist.last() = dSumLen;
    }
    
    const double dAvg = in_dPercOfAvg * ( adDist.last() / (m_apt.num() - 1.0) );
    const double dLen = adDist.last();
    const int iNSamples = WINminmax( 1 + (int) (dLen / dAvg), 10, 1000 );
    const double dStep = dLen / (double) iNSamples;
    
    double dAlong = 0.0, dLeft = dStep;
    int iSeg = 0;
    
    crvOut.AddPoint( m_apt[0] );
    while ( iSeg < m_apt.num() - 1 ) {
        if ( adDist[iSeg] - dAlong > dLeft ) {
            dAlong += dLeft;
            crvOut.AddPoint( Lerp( m_apt[iSeg], m_apt[iSeg+1], dAlong / adDist[iSeg] ) );
            dLeft = dStep;
        } else {
            dLeft -= adDist[iSeg] - dAlong;
            ASSERT( dLeft >= 0.0 );
            iSeg++;
            dAlong = 0.0;
        }
    }
    crvOut.AddPoint( m_apt.last() );
	crvOut.SetAllData();

	if ( s_bTrace ) {
		cout << "Resampling " << iNSamples << " " << crvOut.m_apt.num() << "\n";
	}
    return crvOut;
}

/* 
 * Resample the curve so that the segments are the given size
 */
Curve Curve::ResampleAbsoluteSize( const double in_dSize ) const
{
    if ( m_apt.num() < 3 ) 
        return *this;
    
    Curve crvOut;
    ASSERT( in_dSize > 0.0 );
    
    Array<double> adDist( m_apt.num() );
    
    if ( m_bPercData == true ) {
        adDist = m_adDist;
    } else {
        double dSumLen = 0.0;    
        for ( int i = 0; i < m_apt.num() - 1; i++ ) {
            adDist[i] = ::Length( m_apt[i+1] - m_apt[i] );
            dSumLen += adDist[i];
        }
        adDist.last() = dSumLen;
    }
    
    const double dLen = adDist.last();
    const int iNSamples = WINminmax( 1 + (int) (dLen / in_dSize), 10, 1000 );
    const double dStep = dLen / (double) iNSamples;
    
    double dAlong = 0.0, dLeft = dStep;
    int iSeg = 0;
    
    crvOut.AddPoint( m_apt[0] );
    while ( iSeg < m_apt.num() - 1 ) {
        if ( adDist[iSeg] - dAlong > dLeft ) {
            dAlong += dLeft;
            crvOut.AddPoint( Lerp( m_apt[iSeg], m_apt[iSeg+1], dAlong / adDist[iSeg] ) );
            dLeft = dStep;
        } else {
            dLeft -= adDist[iSeg] - dAlong;
            ASSERT( dLeft >= 0.0 );
            iSeg++;
            dAlong = 0.0;
        }
    }
    crvOut.AddPoint( m_apt.last() );
	crvOut.SetAllData();

	if ( s_bTrace ) {
		cout << "Resampling " << iNSamples << " " << crvOut.m_apt.num() << "\n";
	}
    return crvOut;
}

/*
 * Reflect around the plane. For doing symmetry
 */
Curve Curve::Reflect( const R3Plane &in_plane ) const
{
    Curve crv(*this);
    for ( int i = 0; i < m_apt.num(); i++ ) {
        const R3Pt ptClose = in_plane.ProjectOnPlane( m_apt[i] );
        const R3Vec vec = m_apt[i] - ptClose;
        crv.m_apt[i] = m_apt[i] - vec * 2.0;
    }
    
    crv.m_bPercData = false;
    crv.m_bTangentData = false;

	crv.SetAllData();
	// TODO: Fix constraints
	
    return crv;
}

/* Probably could reverse in-place */
Curve Curve::Reverse( ) const
{
	Curve crv;
	for ( int i = m_apt.num() - 1; i >= 0; i-- ) {
		crv.AddPoint( m_apt[i] );
	}
    crv.m_bPercData = false;
    crv.m_bTangentData = false;
    crv.SetAllData();
	
	// TODO: Fix constraints
	return crv;
}

/*
 * Project the curve onto the plane
 * Keeps everything the same, just puts the points on the plane
 */
Curve Curve::Project( const R3Plane &in_plane ) const
{
    Curve crvOut = *this;
    
    for ( int i = 0; i < NumPts(); i++ ) {
        crvOut.m_apt[i] = in_plane.ProjectOnPlane( m_apt[i] );
    }
    return crvOut;
}


/*
 * Project the curve onto the plane
 * Keeps everything the same, just puts the points on the plane
 *  Then rotates the entire system so that z is in the up direction of the plane
 *  Used for drag (which undoes the rotation/translation after movement)
 */
Curve Curve::ProjectAndRotate( const R3Plane &in_plane ) const
{
    Curve crvOut = *this;
    
    R3Matrix matRot;
    R3Matrix::MatrixVecToVec( matRot, in_plane.Normal(), R3Vec(0,0,1) );
    const R4Matrix matTrans = (R4Matrix) matRot * R4Matrix::Translation( (*this)(0.5) - R3Pt(0,0,0) );
    
    for ( int i = 0; i < NumPts(); i++ ) {
        crvOut.m_apt[i] = matTrans * in_plane.ProjectOnPlane( m_apt[i] );
        crvOut.m_apt[i][2] = 0.0;
    }
    
    if ( IsClosed() ) {
        const R3Pt ptAvg = Lerp( crvOut.m_apt[0], crvOut.m_apt.last(), 0.5 );
        crvOut.m_apt[0] = ptAvg;
        crvOut.m_apt.last() = ptAvg;
    }
    
    return crvOut;
}

/*
 * All the other properties will be re-built upon read
 */
void Curve::Write( ofstream &out ) const
{
    out << m_apt.num() << "\n";
    for ( int i = 0; i < m_apt.num(); i++ ) {
        out << " " << m_apt[i] << " " << "\n";
    }
    out << "Tangents\n";
    for ( int i = 0; i < m_apt.num(); i++ ) {
        out << "  " << m_avecTangent[i] << "\n";
    }
}

void Curve::Read( ifstream &in )
{
    Clear();
    std::string str;
    int iN;
    in >> iN;
    
    ASSERT( in.good() );
    
    R3Pt pt;
    for ( int i = 0; i < iN; i++ ) {
        in >> pt;
        AddPoint( pt );
    }
    SetDistanceData();
    
    in >> str; ASSERT( !str.compare("Tangents") );
    m_avecTangent.need(iN);
    for ( int i = 0; i < iN; i++ ) {
        in >> m_avecTangent[i];
    }
	m_bTangentData = true;
	
    m_bbox = CalcBoundingBox();
}

void Curve::ReadSimple( ifstream &in )
{
    Clear();
    std::string str;
    int iN;
    in >> iN;
    
    ASSERT( in.good() );
    
    R3Pt pt(0,0,0);
    double dT;
    R3Vec vec(0,1,0);
    int iIndex;
    for ( int i = 0; i < iN; i++ ) {
        in >> iIndex >> dT >> pt >> vec;
        in >> pt[0] >> pt[1];
        dT = i / (double) iN;
        iIndex = i;
        AddPoint( pt );
    }
    SetAllData();
}

void Curve::ReadPatchMaker( ifstream &in )
{
    Clear();
    std::string str;
    int iN;
    in >> iN;
    
    ASSERT( in.good() );
    
    R3Pt pt;
    for ( int i = 0; i < iN; i++ ) {
        in >> pt;
        AddPoint( pt );
    }
    SetAllData();
}

void Curve::ReadKaran( ifstream &in )
{
    Clear();
    char str[500];
    for ( int i = 0; i < 5; i++ ) {
        in.getline( str, 500 );
    }
    int iN;
    double dT;
    in >> iN;
    for ( int i = 0; i < iN; i++ ) {
        in >> dT;
    }
    
    in >> iN;
    R3Pt pt;
    for ( int i = 0; i < iN; i++ ) {
        in >> pt;
        AddPoint( pt );
    }
    SetAllData();
    
    in.getline( str, 500 );
    in.getline( str, 500 );
    ASSERT( iN != 0 );
}



