/*
 *  CurveGroup.cpp
 *  SketchCurves
 *
 *  Created by Cindy Grimm on 9/2/10.
 *  Copyright 2010 Washington University in St. Louis. All rights reserved.
 *
 */

#include "CurveGroup.h"
#include "PointConstraint.h"
#include <OpenGL/OGLObjs_Camera.H>

CurveGroup &CurveGroup::operator=( const CurveGroup &in_crvGroup )
{
    m_crvRibbon = in_crvGroup.m_crvRibbon;
    m_crvRibbonReflect = in_crvGroup.m_crvRibbonReflect;
    
    m_aStrokeHistory = in_crvGroup.m_aStrokeHistory;
    m_bInGeometryUpdate = in_crvGroup.m_bInGeometryUpdate;
    
    return *this;
}

CurveGroup::CurveGroup( const CurveGroup &in_crvGroup )
:
m_iHashId( -1 ),
m_bDoFade(false),
m_bReflected( false),
m_crvDraw( m_crvRibbon ),
m_crvDrawReflect( m_crvRibbonReflect ),
m_bInGeometryUpdate(false)

{
    *this = in_crvGroup;
}


const PointConstraint &CurveGroup::GetConstraint( const int in_i ) const
{
    if ( in_i >= 0 && in_i < NumConstraints() ) {
        return *m_aopPins[in_i];
    }
    
    cerr << "ERR: CurveGroup::GetConstraint, bad constraint number " << in_i << " " << NumConstraints() << "\n";
    static PointConstraint s_pinBad;
    
    return s_pinBad;
}


const ScreenCurve &CurveGroup::LastStroke2D() const 
{ 
	static ScreenCurve crvEmpty;
	if ( m_aStrokeHistory.num() == 0 ) {
		cerr << "ERR: CurveGroup::LastStroke2D, accessing empty stroke history\n";
		return crvEmpty;
	}
	
	if ( m_aStrokeHistory.num() > 1 && m_aStrokeHistory[ m_aStrokeHistory.num() - 2 ].IsBlend() ) {
		return m_aStrokeHistory[ m_aStrokeHistory.num() - 2 ].Stroke2D();
	}
	
	return LastAction().Stroke2D();
}

const Curve &CurveGroup::LastStroke3D() const 
{ 
	static Curve crvEmpty;
	if ( m_aStrokeHistory.num() == 0 ) {
		cerr << "ERR: CurveGroup::LastStroke3D, accessing empty stroke history\n";
		return crvEmpty;
	}
	
	for ( int i = m_aStrokeHistory.num() - 1; i >= 0; i-- ) {
		if ( m_aStrokeHistory[i].GetMergeType() != StrokeHistory::BLEND_OVERSTROKE )
			return m_aStrokeHistory[i].Stroke3D();
	}
	
	return crvEmpty;
}


StrokeHistory::MergeType CurveGroup::LastStrokeAction() const
{
	return LastAction().GetMergeType();
}

const StrokeHistory &CurveGroup::LastAction() const
{
	if ( m_aStrokeHistory.num() == 0 ) {
        static StrokeHistory shRet;
		cerr << "ERR: CurveGroup::LastAction last stroke action, no actions!\n";
		return shRet;
	} 
	
    for ( int i = m_aStrokeHistory.num() - 1; i > 0; i-- ) {
        if ( m_aStrokeHistory[i].GetTimeStamp() != m_aStrokeHistory[i-1].GetTimeStamp() ) {
            return m_aStrokeHistory[i];
        }
    }
    
    return m_aStrokeHistory[0];
}

StrokeHistory &CurveGroup::SetLastAction() 
{
	if ( m_aStrokeHistory.num() == 0 ) {
        static StrokeHistory shRet;
		cerr << "ERR: CurveGroup::LastAction last stroke action, no actions!\n";
		return shRet;
	} 
	
    for ( int i = m_aStrokeHistory.num() - 1; i > 0; i-- ) {
        if ( m_aStrokeHistory[i].GetTimeStamp() != m_aStrokeHistory[i-1].GetTimeStamp() ) {
            return m_aStrokeHistory[i];
        }
    }
    
    return m_aStrokeHistory[0];
}

R3Pt CurveGroup::StrokeStartPt() const
{
	if ( m_aStrokeHistory.num() == 0 ) {
		cerr << "ERR: CurveGroup::StrokeStartPt last stroke action, no actions!\n";
		return R3Pt(1e30, 1e30, 1e30);
	} 
	return LastAction().StrokeStartPt();
}

R3Pt CurveGroup::StrokeEndPt() const
{
	if ( m_aStrokeHistory.num() == 0 ) {
		cerr << "ERR: CurveGroup::StrokeEndPt last stroke action, no actions!\n";
		return R3Pt(1e30, 1e30, 1e30);
	} 
	return LastAction().StrokeEndPt();
}

R3Pt CurveGroup::SelectRegionStartPt() const
{
	if ( m_aStrokeHistory.num() == 0 ) {
		cerr << "ERR: CurveGroup::SelectRegionStartPt last stroke action, no actions!\n";
		return R3Pt(1e30, 1e30, 1e30);
	} 
	
	return CompositeCurve()( LastAction().SelectRegionStartT() );
}

R3Pt CurveGroup::SelectRegionEndPt() const
{
	if ( m_aStrokeHistory.num() == 0 ) {
		cerr << "ERR: CurveGroup::SelectRegionEndPt last stroke action, no actions!\n";
		return R3Pt(1e30, 1e30, 1e30);
	} 
	return CompositeCurve()( LastAction().SelectRegionEndT() );
}

void CurveGroup::Finalize()
{
	StrokeHistory shSave( m_aStrokeHistory.last() );
	m_aStrokeHistory.clearcompletely();
	
	shSave.StartNew( CompositeCurve() );
	m_aStrokeHistory += shSave;
}

void CurveGroup::Reflect( const R3Plane &in_plane )
{
    m_planeReflect = in_plane;
    m_bReflected = true;
    
    // TODO: actually create and reflect curve
}

void CurveGroup::ForceUpdateDrawData()
{
    m_crvDraw.ForceUpdate();
}

CurveGroup::~CurveGroup()
{
    // Most of the time there should be no point constraints left
    RemoveCurveFromPointConstraints();
}

CurveGroup::CurveGroup( const int in_iId )
: m_iHashId( in_iId ),
 m_bDoFade(false),
 m_bReflected( false),
 m_crvDraw( m_crvRibbon ),
 m_crvDrawReflect( m_crvRibbonReflect ),
 m_bInGeometryUpdate(false)
{
    m_aStrokeHistory.preallocate(20);
}



void CurveGroup::Write( ofstream &out, const int in_iId ) const
{
    out << "Fade " << ( m_bDoFade ? "1\n" : "0\n" );
    out << "Reflect " << ( m_bReflected ? "1\n" : "0\n" );
    if ( m_bReflected ) {
        m_planeReflect.Write(out);
    }
    m_crvRibbon.Write(out);
    
    out << "CurveFragments " << m_aStrokeHistory.num() << " ";
    for ( int i = 0; i < m_aStrokeHistory.num(); i++ ) {
		out << "Stroke\n";
		m_aStrokeHistory[i].Write(out);
    }
    out << "\n\n";
}

void CurveGroup::WriteSimple( ofstream &out ) const
{
    m_crvRibbon.WriteSimple(out);
    if ( IsReflected() ) {
        m_crvRibbonReflect.WriteSimple(out);
    }
}


void CurveGroup::Read( ifstream &in )
{
    std::string str;
    
    in >> str; ASSERT( !str.compare( "Fade" ) );
    in >> m_bDoFade;

    ASSERT( in.good() );

    in >> str; ASSERT( !str.compare( "Reflect" ) );
    in >> m_bReflected;
    
    ASSERT( in.good() );

    if ( m_bReflected ) {
        m_planeReflect.Read(in);
    }

    ASSERT( in.good() );

    m_crvRibbon.Read(in);
    
    ASSERT( in.good() );
    if ( m_bReflected ) {
        Reflect( m_planeReflect );
    }
    
    int iN;    
    in >> str; ASSERT( !str.compare( "CurveFragments" ) );
    in >> iN;
    m_aStrokeHistory.need(iN);
    for ( int i = 0; i < m_aStrokeHistory.num(); i++ ) {
		in >> str; ASSERT( !str.compare( "Stroke" ) );
		m_aStrokeHistory[i].Read(in);
    }
}

void CurveGroup::ReadKaran( ifstream &in )
{
    Curve crv;
    crv.ReadKaran(in);
    ASSERT( in.good() );
    
    ScreenCurve crvScreen;
	for ( int i = 0; i < crv.NumPts(); i++ ) {
		crvScreen.AddPoint( g_drawState.GetCamera().CameraPt( crv.Pt(i) ), i );
	}
    
	NewCompositeCurve( crvScreen, crv );
}

void CurveGroup::ReadSimple( ifstream &in )
{
    Curve crv;
    crv.ReadSimple(in);
    ASSERT( in.good() );
    
    ScreenCurve crvScreen;
	for ( int i = 0; i < crv.NumPts(); i++ ) {
		crvScreen.AddPoint( g_drawState.GetCamera().CameraPt( crv.Pt(i) ), i );
	}
    
	NewCompositeCurve( crvScreen, crv );
}


void CurveGroup::ReadTangentNormal( ifstream &in )
{
    string str;
    in >> str; ASSERT( !str.compare( "Curve" ) );
    int iCrvId;
    in >> iCrvId;
    
    in >> str; ASSERT( !str.compare( "samples" ) );

    m_crvRibbon.ReadTangentNormal(in);
    m_crvDraw.GeometryChanged();
}

void CurveGroup::PrintHistory( const bool in_bPrintPins ) const 
{
    cout << "CG " << HashId() << " SH " << m_aStrokeHistory.num() << "\n";
    for ( int i = 0; i < m_aStrokeHistory.num(); i++ ) {
        cout << "  ";
        m_aStrokeHistory[i].PrintHistory();
    }
    
    if ( in_bPrintPins ) {
        for ( int i = 0; i < NumConstraints(); i++ ) {
            GetConstraint(i).PrintHistory();
        }
    }
}