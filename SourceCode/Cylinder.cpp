//
//  Cylinder.cpp
//  SketchCurves
//
//  Created by Cindy Grimm on 8/9/12.
//  Copyright (c) 2012 Washington University in St. Louis. All rights reserved.
//

#include <iostream>
#include <algorithm>
#include <utils/Crv_Basis.H>
#include "DrawState.h"
#include "ParamFile.h"
#include "Cylinder.h"
#include "CurveNetwork.h"
#include "UserInterface.h"

static Cylinder *s_opCyl = NULL;

void Cylinder::Polygon::SetMap()
{
    const double dDesiredOffset = g_drawState.m_opUI->m_dCylinderOffset->value();
    
    m_dMapShift = 0.0;
    m_dMapScale = 1.0;
    if ( NumPts() == 0 ) {
        return;
    }
    
    ///cout << "Setting offset " << dDesiredOffset << ": ";
    for ( int j = 0; j < NumPts(); j++ ) {
        const double dValue = ImplicitRaw( Lerp( EdgePt(j, 0.5), R2Pt(0,0), dDesiredOffset ) );
        
        m_dMapShift += dValue;
        //cout << dValue << " ";
    }
    m_dMapShift = m_dMapShift / NumPts();
    //cout << "\n";
    
    const double dValueMiddle = Implicit( R2Pt(0,0) );
    if ( !RNIsZero( dValueMiddle ) ) {
        m_dMapScale = 1.0 / dValueMiddle;
    }
}

void Cylinder::Polygon::SetMapExplicit( const double in_dShift, const double in_dScale )
{
    m_dMapScale = in_dScale;
    m_dMapShift = in_dShift;
}

void Cylinder::Polygon::SetScale()
{
    const R2Polygon polyOrig( Polygon2D() );
    Array<R2Pt> aptOffEdge( m_aSeg.num() );
    const double dDesiredOffset = g_drawState.m_opUI->m_dCylinderOffset->value();
    
    for (int i = 0; i < aptOffEdge.num(); i++ ) {
        aptOffEdge[i] = Lerp( EdgePt(i, 0.5), R2Pt(0,0), dDesiredOffset );
    }
        
    m_dScale = 1.0;
    
    double dScaleUnitRadius = Radius() * (1.0 - dDesiredOffset);
    if ( RNIsZero(dScaleUnitRadius) ) {
        return;
    }
    
    SetPolygon( polyOrig.Pts(), dScaleUnitRadius );
    /*
    double dBestScale = 1.0 / dScaleUnitRadius;
    double dBestAvg = 1e30;
    for ( int iLoop = 0; iLoop < 30; iLoop++ ) {
        for ( double dScl = 0.75; dScl < 2.25; dScl += 0.05 ) {
            SetPolygon( polyOrig.Pts(), dScaleUnitRadius * dScl );
            double dAvg = 0.0;
            for ( int i = 0; i < aptOffEdge.num(); i++ ) {
                dAvg += Implicit( aptOffEdge[i] );
            }
            cout << dAvg << " ";
            if ( fabs( dAvg ) < dBestAvg ) {
                dBestScale = m_dScale;
                dBestAvg = fabs( dAvg );
            }
        }
        dScaleUnitRadius = dBestScale;
    }
    SetPolygon( polyOrig.Pts(), dBestScale );
     */
}

R2Polygon Cylinder::Polygon::Polygon2D() const
{
    R2Polygon poly( m_aSeg.num() );
    for ( int i = 0; i < m_aSeg.num(); i++ ) {
        poly[i] = m_aSeg[i].P1();
    }
    return poly;
}

double Cylinder::Polygon::SetPolygon( const Array<R2Pt> &in_apt, const double in_dScale )
{
    m_aSeg.need( in_apt.num() );
    m_adEdgeWeight.need( in_apt.num() );
    m_aSegNorm.need( in_apt.num() );
    
    m_dLenTotal = 0.0;
    m_dScale = in_dScale;
    double dSum = 0.0;
    for ( int i = 0; i < in_apt.num(); i++ ) {
        m_aSeg[i] = R2Line_seg( R2Pt( in_apt[i][0] * m_dScale, in_apt[i][1] * m_dScale ), 
                                R2Pt( in_apt.wrap(i+1)[0] * m_dScale, in_apt.wrap(i+1)[1] * m_dScale ) );
        m_adEdgeWeight[i] = WINmax( m_aSeg[i].Length(), 1e-12 );
        m_dLenTotal += ::Length( in_apt[i] - in_apt.wrap(i+1) );
        const R2Vec vecEdge = UnitSafe( in_apt.wrap(i+1) - in_apt[i]  );
        m_aSegNorm[i] = R2Vec( -vecEdge[1], vecEdge[0] );
        
        if ( Dot( m_aSegNorm[i], R2Pt(0,0) - m_aSeg[i].P1() ) < 0.0 ) {
            m_aSegNorm[i] *= -1.0;
        }
        dSum += m_adEdgeWeight[i];
    }
    
    for ( int i = 0; i < m_adEdgeWeight.num(); i++ ) {
        m_adEdgeWeight[i] /= m_adEdgeWeight.num();
        //m_adEdgeWeight[i] /= dSum;
    }
    
    m_dClamp = 1e-6 * m_dLenTotal / (2.0 * M_PI);

    return m_dLenTotal;
}

void Cylinder::Polygon::EvalFromEdge( const double in_dImplicit ) const
{
    cout << "From edge: ";
    for ( int i = 0; i < m_aSeg.num(); i++ ) {
        cout << "Pt " << m_aSeg[i](0.5) << "\n";
        cout << "  ";
        for ( double dT = 0.0; dT < 1.0; dT += 0.075 ) {
            const R2Pt ptEdge( m_aSeg[i](0.5) );
            const R2Pt ptUnScale( ptEdge[0] / m_dScale, ptEdge[1] / m_dScale );
            cout << Implicit( Lerp( R2Pt(0,0), ptUnScale, dT ) ) - in_dImplicit << " ";
        }
        cout << "\n";
    }
    cout << "\n";    
}


double Cylinder::Polygon::Create( const Array<R2Pt> &in_apt, const R4Matrix &in_mat, const double in_dT )
{
    m_mat = in_mat;
    m_dT = in_dT;


    SetPolygon( in_apt );
    SetMap();
    
    WINbool bSuc;
    const R4Matrix matInv = in_mat.Inverse(bSuc);

    m_aptCorners.need( in_apt.num() );
    for ( int i = 0; i < in_apt.num(); i++ ) {
        m_aptCorners[i] = matInv * R3Pt( in_apt[i][0], in_apt[i][1], 0.0 );
    }
    
    SetMap();
    return m_dLenTotal;
}



double Cylinder::Polygon::ImplicitRaw( const R2Pt &in_pt ) const
{
    double dSum = 1.0;
    //double dSumLog = 0.0;
    R2Pt ptOut;
    const R2Pt ptScale( in_pt[0] * m_dScale, in_pt[1] * m_dScale );
    
    ASSERT( !isnan( in_pt[0] ) && !isnan(in_pt[1] ) );
    for ( int i = 0; i < m_adEdgeWeight.num(); i++ ) {
        
        const double dDist = Dot( m_aSegNorm[i], ptScale - m_aSeg[i].P1() );
        dSum *= pow( dDist, m_adEdgeWeight[i] );
        //const double dLog = m_adEdgeWeight[i] * log( WINmax( dDist, m_dClamp ) ); // zero at boundary, negative inside
        //dSumLog += dLog;
        //cout << dDist << " ";
    }
    if ( dSum > m_dClamp ) {
        return log( dSum );
    }
    
    return log( m_dClamp );
}

double Cylinder::Polygon::Implicit( const R2Pt &in_pt ) const
{
    const double dRaw = ImplicitRaw(in_pt);
    return ( dRaw - m_dMapShift ) * m_dMapScale;
}


Cylinder::Polygon Cylinder::Polygon::operator=( const Polygon &in_poly )
{
    m_aSeg = in_poly.m_aSeg;
    m_aSegNorm = in_poly.m_aSegNorm;
    m_adEdgeWeight = in_poly.m_adEdgeWeight;
    m_aptCorners = in_poly.m_aptCorners;
    m_dLenTotal = in_poly.m_dLenTotal;
    m_dClamp = in_poly.m_dClamp;
    m_dScale = in_poly.m_dScale;
    m_dMapShift = in_poly.m_dMapShift;
    m_dMapScale = in_poly.m_dMapScale;
    m_dT = in_poly.m_dT;
    m_mat = in_poly.m_mat;
    
    return *this;
}

Cylinder::Polygon::Polygon()
{
    m_dClamp = 1e-6;
    m_dScale = 1.0;
    m_dMapScale = 1.0;
    m_dMapShift = 0.0;
}

void Cylinder::Polygon::Write( ofstream &out ) const
{
    out << m_aptCorners.num() << " " << m_dT << " " << m_dLenTotal << " " << m_dClamp << "\n";
    for ( int i = 0; i <  m_aptCorners.num(); i++ ) {
        m_aSeg[i].Write(out);
        out << m_aptCorners[i] << m_adEdgeWeight[i] << m_aSegNorm[i] << "\n";
    }
}

void Cylinder::Polygon::Read(ifstream &in)
{
    int iN;
    in >> iN >> m_dT >> m_dLenTotal >> m_dClamp;
    m_aptCorners.need(iN);
    m_aSeg.need(iN);
    m_adEdgeWeight.need(iN);
    for ( int i = 0; i < iN; i++ ) {
        m_aSeg[i].Read(in);
        in >> m_aptCorners[i] >> m_adEdgeWeight[i] >> m_aSegNorm[i];
    }
}

R4Matrix Cylinder::CreateMatrix( const double in_dT ) const
{
    const R3Vec vecBiNorm = UnitSafe( Cross( m_crvAxis.Tangent(in_dT), m_crvAxis.Normal(in_dT) ) );
    R3Matrix matRot;
    R3Matrix::MatrixVecToVec(matRot, vecBiNorm, m_crvAxis.Normal(in_dT), R3Vec(1,0,0), R3Vec(0,1,0) );
    
    return (R4Matrix) matRot * R4Matrix::Translation( R3Pt(0,0,0) - m_crvAxis(in_dT) );    
}

double Cylinder::CreateCrossSection( Polygon &io_poly, const Curve &in_curve )
{
    const R3Pt ptCenter = in_curve.CenterPoint();
    R3Vec vecLine, vecPlane;
    in_curve.IsPlanar( vecLine, vecPlane );
    const R3Plane plane(ptCenter, vecPlane); // plane of cross section

    // Find the point on the curve that intersects the plane
    R3Pt ptBest, ptOut;
    double dBest = 1e30, dT = 0.0, dTSeg = 0.0;
    bool bFound = false;
    for ( int i = 0; i < m_crvAxis.NumPts() - 1; i++ ) {
        const R3Line_seg seg( m_crvAxis.Pt(i), m_crvAxis.Pt(i+1) );
        if ( plane.Intersect( seg, dTSeg, ptOut ) ) {
            if ( ::Length( ptOut - ptCenter ) < dBest ) {
                dBest = ::Length( ptOut - ptCenter );
                dT = (1.0 - dTSeg) * m_crvAxis.PercAlong(i) + dTSeg * m_crvAxis.PercAlong(i+1);                
                ptBest = ptOut;
                bFound = true;
            }
        }
    }
    // Off end - pick closest
    if ( bFound == false ) {
        if ( ::Length( in_curve(0.0) - ptCenter ) < ::Length( in_curve(1.0) - ptCenter ) ) {
            dT = 0.0;
        } else {
            dT = 1.0;
        }
    }
    
    const R4Matrix mat = CreateMatrix( dT );
    
    Array<R2Pt> aptPoly( in_curve.NumPts() );
    for ( int i = 0; i < aptPoly.num(); i++ ) {
        const R3Pt ptPlane = mat * in_curve.Pt(i);
        
        aptPoly[i] = R2Pt( ptPlane[0], ptPlane[1] );
    }
    
    return io_poly.Create( aptPoly, mat, dT );
}

double Cylinder::Implicit( const R3Pt &in_pt ) const
{
    double dDist;
    R3Pt ptOut;
    
    static CRVBasis crvBasis( 2, 0.0, 2.0 );
    static CRVHermite1 crvEnd;
    
    //static double sdScale = crvBasis(1.0);
    if ( crvEnd.NumPts() < 2 ) {
        crvEnd.Add( R1Pt(0.0), R1Vec(1.0) );
        crvEnd.Add( R1Pt(0.0), R1Vec(1.0) );
        crvEnd.Add( R1Pt(0.5), R1Vec(1.0) );
        crvEnd.Add( R1Pt(1.0), R1Vec(1.0) );
    }
    const double dT = m_crvAxis.ClosestPointTValue( in_pt, dDist, ptOut );
    
    int iWhich = -1;
    double dPerc = 0.0;
    if ( dT <= m_apoly[0].m_dT ) {
        iWhich = 0;
        dPerc = 0.0;
    } else if ( dT >= m_apoly[ m_apoly.size() - 1 ].m_dT ) {
        iWhich = m_apoly.size() - 1;
        dPerc = 0.0;
    } else {
        for ( int i = 0; i < m_apoly.size() - 1; i++ ) {
            if ( dT <= m_apoly[i+1].m_dT && dT >= m_apoly[i].m_dT ) {
                iWhich = i;
                dPerc = (dT - m_apoly[i].m_dT ) / WINmax( m_apoly[i+1].m_dT - m_apoly[i].m_dT, 0.00000001 );
                break;
            }
        }
    }    
    
    const R3Pt ptCrv = m_crvAxis(dT);
    const R3Vec vecTangent = m_crvAxis.Tangent( dT );
    const R3Plane planeEnd( ptCrv, vecTangent );
    const R3Pt ptEnd = planeEnd.ProjectOnPlane( in_pt );
    const R3Vec vecInPlane = ptEnd - m_crvAxis(dT);

    const R3Vec vecNorm = m_crvAxis.Normal( dT );
    const R3Vec vecBiNorm = Cross( vecTangent, vecNorm );

    const R2Pt ptPlane( Dot( vecBiNorm, vecInPlane ), Dot( vecNorm, vecInPlane ) );
    
    static Polygon poly;
    if ( ( dT > 0.0 && dT < 1.0 ) && iWhich+1 < m_apoly.size() ) {
        const Polygon &polyPrev = m_apoly[iWhich];
        const Polygon &polyNext = m_apoly[(iWhich+1)%m_apoly.size()];

        /*
        const double dPolyPrev = polyPrev.Implicit( ptPlane );
        const double dPolyNext = polyNext.Implicit( ptPlane );
        const double dBlend = crvBasis( dPerc ) / sdScale;
        const double dImplicitCrossSection = ( (1.0 - dPerc) * dPolyPrev + dPerc * dPolyNext );
        ASSERT( dPerc >= 0.0 && dPerc <= 1.0 );

        return dImplicitCrossSection;
         */

        R2Polygon poly2D = polyPrev.Polygon2D();
        for (int j = 0; j < poly2D.Num_pts(); j++ ) {
            poly2D[j] = Lerp( polyPrev.EdgePt(j, 0.0), polyNext.EdgePt(j,0.0), dPerc );
        }
        poly.SetPolygon( poly2D.Pts() );
        poly.SetMapExplicit( (1.0 - dPerc) * polyPrev.Shift() + dPerc * polyNext.Shift(),
                             (1.0 - dPerc) * polyPrev.Scale() + dPerc * polyNext.Scale() );

        //cout << polyPrev.Implicit( ptPlane ) << " " << poly.Implicit(ptPlane) << " " << polyNext.Implicit( ptPlane ) << "\n";
        return poly.Implicit( ptPlane );
    }
    
    const double dLengthTang = fabs( Dot( (in_pt - ptCrv), vecTangent ) ) / m_apoly[iWhich].Radius();
    
    const double dPolygon = m_apoly[iWhich].Implicit( ptPlane ) - m_dAvgImplicit;
    const double dBlendEnd = (dLengthTang < 1.0) ? crvEnd( dLengthTang )[0] : dLengthTang;
    
    //if ( dLengthTang > 1.0 ) {
    //    return -100.0;
    //} else {
    //    return dPolygon;
    //}
    return dPolygon - dBlendEnd;
    //return dPolygon - dBlendEnd * dValMiddle;
    
    /*
    // Basically rotate the last cross section around the end of the axis curve
    // need positive and minus because not symmetric

    const R3Vec vecBiNormRotated = UnitSafe( Rejection(vecNorm, vecTo) );

    const R2Pt ptPlanePlus( Dot( vecBiNormRotated, vecTo ), ptPlane[1] );
    const R2Pt ptPlaneMinus( -ptPlanePlus[0], ptPlanePlus[1] );
    const double dAlphaBlend = 0.5 * (1.0 + Dot( vecBiNorm, vecBiNormRotated ) );
    ASSERT( dAlphaBlend >= 0.0 && dAlphaBlend <= 1.0 );

    const double dDot1 = Dot(vecBiNormRotated, vecNorm); // Should be perpendicular
    ASSERT( RNIsZero( dDot1, 1e-5 ) );
    const R3Vec vecPlaneNorm = Cross( vecBiNormRotated, vecNorm );
    const double dDot2 = Dot( vecTo, vecPlaneNorm );
    ASSERT( RNIsZero( dDot2 ) ); // vecTo lies in plane of vecBiNormRotated and vecNorm

    const double dImplicitPlus = m_apoly[iWhich].Implicit( ptPlanePlus );
    const double dImplicitMinus = m_apoly[iWhich].Implicit( ptPlaneMinus );
    
    //return dImplicitPlus - m_apoly[iWhich].m_dImplicit;
    return ( dAlphaBlend * dImplicitPlus + (1.0 - dAlphaBlend) * dImplicitMinus ) - m_apoly[iWhich].m_dImplicit;
     */
}

void Cylinder::SetSelf()
{
    s_opCyl = this;
}

double Cylinder::ImplicitStatic( const R3Pt &in_pt )
{
    if ( s_opCyl ) {
        return s_opCyl->Implicit( in_pt );
    }
    
    return -100.0;    
}

void Cylinder::BuildSurface()
{
    s_srfData = this;
    
	m_aptSurface.resize(0);
	m_avecSurface.resize(0);
	m_afaceSurface.resize(0);
	
	const double dWidth = WINmax( m_crvAxis.Length(), m_dAvgRadius * 2.0 );
	const double dSize = 0.2 * g_drawState.m_opUI->m_iNumCubes->value() * dWidth;
    
	const int iBounds = (int) ( dWidth / (0.25 * dSize + 1e-6) );
    SetSelf();
    const int iCross = m_apoly.size() / 2;
    const R3Pt ptCross = Lerp( m_apoly[iCross].EdgePt3D(0, 0.5 ), m_crvAxis( m_apoly[iCross].m_dT ), g_drawState.m_opUI->m_dCylinderOffset->value() );
	polygonize( &Cylinder::ImplicitStatic, dSize, iBounds, ptCross, TriProc, VertProc );
    
	FlipNormals();
    CalcNormalsForSurface();
    SmoothSurface();
    SmoothSurface();
    
	cout << "Done building surface " << iBounds
	<< " faces " << m_afaceSurface.size() << "\n";

}

void Cylinder::SetCylinderOffset()
{
    for ( int i = 0; i < m_apoly.size(); i++ ) {
        //m_apoly[i].SetScale();
        m_apoly[i].SetMap();
    }
}

void Cylinder::Create( const CurveNetwork &in_crvs )
{
    if ( in_crvs.NumCrvs() < 2 ) {
        cerr << "ERR: Cylinder::Create No cross sections\n";
        return;
    }
    const int iBetween = g_drawState.m_opUI->m_iCylinderBetween->value();
    
    m_crvAxis = in_crvs.GetCurveRibbon(0);
    m_crvAxis.SmoothPoints(0.0, 1.0, 5);
    Array<int> aiPins(0);
    Array<R3Vec> avecNorms;
    const CurveGroup &crvGrp = in_crvs.GetCurveGroup(0);
    for ( int i = 0; i < crvGrp.NumConstraints(); i++ ) {
        if ( crvGrp.GetConstraint(i).IsNormal() ) {
            aiPins += crvGrp.GetConstraint(i).GetPtIndex( crvGrp );
            avecNorms += crvGrp.GetConstraint(i).GetNormal();
        }
    }
    m_crvAxis.SetNormalData( avecNorms, aiPins );

    m_apoly.resize( in_crvs.NumCrvs() - 1 );
    
    m_dAvgRadius = 0.0;
    cout << "Create\n";
    for ( int i = 1; i < in_crvs.NumCrvs(); i++ ) {
        const Curve crvPoly = in_crvs.GetCurve(i).MakePolygon();
        const double dCircum = CreateCrossSection( m_apoly[i-1], crvPoly );
        m_dAvgRadius += dCircum / (2.0 * M_PI);
    }
    sort( m_apoly.begin(), m_apoly.end() );
    vector<Polygon> apolySave = m_apoly;

    Polygon poly;
    R2Polygon poly2D;
    m_apoly.resize( 0 );
    for ( int i = 0; i < apolySave.size() - 1; i++ ) {
        const R2Polygon polyStart = apolySave[i].Polygon2D();
        const R2Polygon polyEnd = apolySave[i+1].Polygon2D();
                
        for ( int j = 0; j < iBetween+2; j++ ) {
            if ( i < apolySave.size() - 2 && j == iBetween + 1 )
                break;
            
            const double dMorph = (double) j / (iBetween + 1.0);
            poly2D = polyStart.Morph( polyEnd, R2Pt(0,0), R2Pt(0,0), dMorph );
            const double dTAxis = (1.0 - dMorph) * apolySave[i].m_dT + dMorph * apolySave[i+1].m_dT;
            poly.Create( poly2D.Pts(), CreateMatrix(dTAxis),  dTAxis );
            poly.SetMapExplicit( (1.0 - dMorph) * apolySave[i].Shift() + dMorph * apolySave[i+1].Shift(),
                                 (1.0 - dMorph) * apolySave[i].Scale() + dMorph * apolySave[i+1].Scale() );
            
            m_apoly.push_back(poly);
            m_dAvgRadius += poly.Radius();
        }
    }
    
    m_dAvgRadius += poly.Radius();

    sort( m_apoly.begin(), m_apoly.end() );
    
    m_dAvgRadius /= (double) m_apoly.size();
    
    m_dAvgImplicit = 0.0;
        
    m_dAvgImplicit /= (double) m_apoly.size();
    cout << "Create " << m_dAvgImplicit << "\n";

    cout << "Polygons:\n";
    for ( int i = 0; i < m_apoly.size(); i++ ) {
        m_apoly[i].EvalFromEdge( m_dAvgImplicit );
    }
    cout << "Polygons 3D:\n";
    for ( int i = 0; i < m_apoly.size(); i++ ) {
        const R3Pt ptCrv = m_crvAxis( m_apoly[i].m_dT );
        cout << "Polygon " << i << "\n";
        for ( int j = 0; j < m_apoly[i].NumPts(); j++ ) {
            for ( double dT = 0.0; dT <= 1.1; dT += 0.075 ) {
                const R3Pt pt3D = Lerp( ptCrv, m_apoly[i].EdgePt3D( j, 0.5 ), dT );
                cout << Implicit( pt3D ) << " ";
            }
            cout << "\n";
        }
    }

    cout << "Along axis: ";
    for ( double dT = 0.0; dT < 1.01; dT += 0.05 ) {
        const R3Pt pt = m_crvAxis(dT) + m_crvAxis.Normal(dT) * ( m_dAvgRadius * 0.9 );
        cout << Implicit(pt) << " ";
    }
    cout << "\n";
    for ( double dT = 0.0; dT < 1.01; dT += 0.05 ) {
        const R3Pt pt = m_crvAxis(dT) + Cross( m_crvAxis.Tangent(dT), m_crvAxis.Normal(dT) ) * ( m_dAvgRadius * 0.9 );
        cout << Implicit(pt) << " ";
    }
    cout << "\n";
    cout << "Off axis: ";
    const R3Vec v1 = UnitSafe( 0.21 * m_crvAxis.Normal(0.0) + 0.51 * m_crvAxis.Tangent(0.0) );
    const R3Vec v2 = UnitSafe( 0.81 * m_crvAxis.Normal(0.0) - 0.31 * m_crvAxis.Tangent(0.0) );
    const R3Vec v3 = UnitSafe( 0.3 * Cross( m_crvAxis.Tangent(0.0), m_crvAxis.Normal(0.0) ) + 0.2 * m_crvAxis.Normal(0.0) );
    for ( double dT = 0.0; dT < 1.01; dT += 0.05 ) {
        const R3Pt pt1 = m_crvAxis(0.0) + v1 * dT * m_dAvgRadius;
        const R3Pt pt2 = m_crvAxis(0.0) + v2 * dT * m_dAvgRadius;
        const R3Pt pt3 = m_crvAxis(0.0) + v3 * dT * m_dAvgRadius;
        cout << Implicit(pt1) << " " << Implicit(pt2) << " " << Implicit(pt3) << "\n";
    }
    cout << "\n";
}

void Cylinder::Write( ofstream &out ) const
{
    out << m_apoly.size() << "\n";
    for ( int i = 0; i < m_apoly.size(); i++ ) {
        m_apoly[i].Write(out);
    }
}

void Cylinder::Read( ifstream &in )
{
    int iN;
    in >> iN;
    m_apoly.resize(iN);
    for ( int i = 0; i < iN; i++ ) {
        m_apoly[i].Read(in);
    }
}
