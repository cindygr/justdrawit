/*
 *  ActionEvents_Curve.cpp
 *  SketchCurves
 *
 *  Created by Cindy Grimm on 9/9/10.
 *  Copyright 2010 Washington University in St. Louis. All rights reserved.
 *
 */

#include "ActionEvents.h"
#include "UserInterface.h"

const bool s_bTrace = false;

/*
 * Set m_iCursorOverCurveIndex, m_iCursorOverTs, and m_ptRotateScaleFixed
 */
void ActionEvents::StartDrag() 
{
    if ( !OverCurve() ) {
        cerr << "ERR: ActionEvents::StartDrag, no valid curve " << m_iCursorOverCurve << "\n";
    }
    
    const Curve &crv = m_crvNetwork.GetCurve(m_iCursorOverCurve);
    
    m_iCursorOverCurveTs.first = crv.FloorWrap( m_dCursorOverCurveTs.first );
    m_iCursorOverCurveTs.second = crv.CeilWrap( m_dCursorOverCurveTs.second );
    m_iCursorOverCurveIndex = crv.ClosestIndex( m_dCursorOverCurve );
    
    if ( m_iCursorOverCurveTs.first < 0 ) m_iCursorOverCurveTs.first += crv.NumPts();
    if ( m_iCursorOverCurveTs.second >= crv.NumPts() ) m_iCursorOverCurveTs.second -= crv.NumPts();
    
    m_ptRotateScaleFixed = m_crvNetwork.GetCurve( m_iCursorOverCurve )( m_dCursorOverCurve );
    
    if ( s_bTraceCurveSelection ) {
        cout << "ActionEvents::StartDrag Indices: " << m_iCursorOverCurveTs.first << " " << m_iCursorOverCurveIndex << " " << m_iCursorOverCurveTs.second << "\n";
    }
    
}

/*
 * Set t values from m_iCursorOverCurveIndex, m_iCursorOverTs, and m_ptRotateScaleFixed
 */
void ActionEvents::EndDrag() 
{
    if ( !OverCurve() ) {
        cerr << "ERR: ActionEvents::EndDrag, no valid curve " << m_iCursorOverCurve << "\n";
    }
    
    const Curve &crv = m_crvNetwork.GetCurve(m_iCursorOverCurve);
    
    if ( m_dCursorOverCurveTs.first < 0.0 ) {
        m_dCursorOverCurveTs.first = crv.PercAlong( m_iCursorOverCurveTs.first ) - 1.0;
    } else if ( m_iCursorOverCurveTs.first >= 0 && m_iCursorOverCurveTs.first < crv.NumPts() ) {
        m_dCursorOverCurveTs.first = crv.PercAlong( m_iCursorOverCurveTs.first );
    }
    if ( m_dCursorOverCurveTs.second > 1.0 ) {
        m_dCursorOverCurveTs.second = crv.PercAlong( m_iCursorOverCurveTs.second ) + 1.0;
    } else if ( m_iCursorOverCurveTs.second >= 0 && m_iCursorOverCurveTs.second < crv.NumPts() ) {
        m_dCursorOverCurveTs.second = crv.PercAlong( m_iCursorOverCurveTs.second );
    }
    if ( m_iCursorOverCurveIndex >= 0 && m_iCursorOverCurveIndex < crv.NumPts() ) {
        m_dCursorOverCurve = crv.PercAlong( m_iCursorOverCurveIndex );
    }
    
    m_iCursorOverCurveTs.first = -1;
    m_iCursorOverCurveTs.second = -1;
    m_iCursorOverCurveIndex = -1;
    if ( s_bTraceCurveSelection ) {
        cout << "ActionEvents::EndDrag ts: " << m_dCursorOverCurveTs.first << " " << m_dCursorOverCurve << " " << m_dCursorOverCurveTs.second << "\n";
    }
    
}

/*
 * Move the actual curve
 *
 * Determines the vector to move along based on the drawing plane and if the user clicked at the
 * top of the drag icon (m_bIsPerpendicularDrag)
 *
 * Region is stored in dCursorOverCurve* stuff
 *
 * For a drag of the entire curve, just move the curve the last little bit moved (between mouse_last and mouse_now)
 *
 * For a drag of a region drag from mouse down to current mouse position. This requires storing the curve at mouse
 * down, then applying the entire mouse movement to the curve (as a single vector). If you don't do this, but try to
 * do incremental changes to the curve, it just introduces wiggles.
 *
 * Previous curve state is stored in CurveGroup::StrokeHistory::m_crvFinal. Hence the whole StartTransform/RestartTransform thing - 
 * you need to save the curve on a mouse down before you start editing.
 *
 * Region versus not is determined by m_dCursorOverTs; if the selected region is (0,1), move the entire curve.
 */
void ActionEvents::MoveCurve( const R2Pt &in_ptNext, const R3Ray &in_ray )
{
    // Set in ActivateHotSpot
    const R3Plane plane = g_drawState.GetShadowBox().GetPlane( m_actionDragClick.second );
	const R3Pt ptNew = plane.IntersectRay( in_ray );
	
	if ( !OverCurve() ) {
		return;
	}
	
	if ( !IsSubset() ) {
		const R3Pt ptOld = plane.IntersectRay( m_rayLast );
		R3Vec vecDrag = ptNew - ptOld;
		if ( m_bIsPerpendicularDrag ) {
			vecDrag = plane.Normal() * ::Length( vecDrag );
			if ( in_ptNext[1] < m_ptScreenLast[1] ) {
				vecDrag *= -1;
			}
		}
		
		m_crvNetwork.Transform( m_iCursorOverCurve, R4Matrix::Translation( vecDrag ), false );
		if ( s_bTrace ) cout << "Entire curve\n";
	} else {
        // For this one, use the drawing plane
		const R3Pt ptOld = plane.IntersectRay( m_rayDown );
		R3Vec vecDrag = ptNew - ptOld;
		if ( m_bIsPerpendicularDrag ) {
			vecDrag = plane.Normal() * ::Length( vecDrag );
			if ( in_ptNext[1] < m_ptScreenDown[1] ) {
				vecDrag *= -1;
			}
		}
		
		if ( s_bTrace ) cout << "Drag vec " << vecDrag << " ts " << m_dCursorOverCurveTs.first << " " << m_dCursorOverCurveTs.second << " " << m_dCursorOverCurve << "\n";
		m_crvNetwork.Drag( m_iCursorOverCurve, plane, vecDrag, 
						  m_dCursorOverCurve, m_dCursorOverCurveTs );
		
	}
}

/*
 * Just rotate the curve so the second mouse down is aligned on a line with the cursor
 * and drag point
 *
 */
void ActionEvents::RotateCurve( const R2Pt &in_ptNext )
{
	if ( !OverCurve() ) {
		return;
	}
    
	const R2Vec vecDown = m_ptScreenLast - m_ptRotateScale.first;
	const R2Vec vecOrig = m_ptRotateScale.second - m_ptRotateScale.first;
	
	if ( RNIsZero( ::Length( vecDown ) ) || RNIsZero( ::Length( vecOrig ) ) ) {
		return;
	}
    
    const R3Plane plane = g_drawState.GetShadowBox().GetPlane( m_actionDragClick.second );

    const double dAngDown = atan2( vecDown[1], vecDown[0] );
    const double dAngOrig = atan2( vecOrig[1], vecOrig[0] );
	
    const R3Matrix matRotToOrig = R3Matrix::Rotation( UnitSafe( plane.Normal() ), dAngOrig );
    const R3Matrix matRotToDown = R3Matrix::Rotation( UnitSafe( plane.Normal() ), -dAngDown );
    const R3Matrix matRot = matRotToDown * matRotToOrig;
    
    const R4Matrix matTransform = R4Matrix::Translation( m_ptRotateScaleFixed ) * (R4Matrix) matRot * R4Matrix::Translation( R3Pt(0,0,0) - m_ptRotateScaleFixed );
    
    m_crvNetwork.Transform( m_iCursorOverCurve, matTransform, true );
    
}

/*
 * Rotate and scale the entire curve
 *
 * Uniform scale. 
 *
 */
void ActionEvents::RotateScaleCurve( const R2Pt &in_ptNext )
{
	if ( !OverCurve() ) {
		return;
	}
    
    
	const R2Vec vecDown = m_ptScreenLast - m_ptRotateScale.first;
	const R2Vec vecOrig = m_ptRotateScale.second - m_ptRotateScale.first;
    const double dLenDown = ::Length( vecDown );
    const double dLenOrig = ::Length( vecOrig );
	
	if ( RNIsZero( dLenDown ) || RNIsZero( dLenOrig ) ) {
		return;
	}
    
    const R3Plane plane = g_drawState.GetShadowBox().GetPlane( m_actionDragClick.second );

    const double dAngDown = atan2( vecDown[1], vecDown[0] );
    const double dAngOrig = atan2( vecOrig[1], vecOrig[0] );
	
    const double dLen = dLenDown / dLenOrig;
    const R3Matrix matRotToOrig = R3Matrix::Rotation( R3Vec(0,0,1), dAngOrig );
    const R3Matrix matRotToDown = R3Matrix::Rotation( R3Vec(0,0,1), -dAngDown );
    const R3Matrix matScl = R3Matrix::Scaling( dLen, dLen, 1.0 );
    const R3Matrix matRotScl = matRotToDown * matScl * matRotToOrig;
    
    R3Matrix matRotateToPlane;
    R3Matrix::MatrixVecToVec( matRotateToPlane, R3Vec(0,0,1), UnitSafe( plane.Normal() ) );

    const R4Matrix matTransform = 
        R4Matrix::Translation( m_ptRotateScaleFixed ) * (R4Matrix) matRotateToPlane 
        * (R4Matrix) matRotScl
        * (R4Matrix) matRotateToPlane.Transpose() * R4Matrix::Translation( R3Pt(0,0,0) - m_ptRotateScaleFixed );
    
    m_crvNetwork.Transform( m_iCursorOverCurve, matTransform, true );
}


void ActionEvents::ScaleCurve( const R2Pt &in_ptNext, const R3Ray &in_ray )
{
	if ( !OverCurve() ) {
		return;
	}
    
	const R2Vec vecDown = m_ptScreenLast - m_ptRotateScale.first;
	const R2Vec vecOrig = m_ptRotateScale.second - m_ptRotateScale.first;
    const double dLenDown = ::Length( vecDown );
    const double dLenOrig = ::Length( vecOrig );
	
	if ( RNIsZero( dLenDown ) || RNIsZero( dLenOrig ) ) {
		return;
	}

    const OGLObjsCamera &cam = g_drawState.GetCamera();
    
    R3Plane plane = g_drawState.GetShadowBox().GetPlane( m_actionDragClick.second );
    if ( m_actionDragClick.second == ShadowBox::DRAWING_PLANE ) {
        plane = R3Plane( m_ptRotateScaleFixed, plane.Normal() );
    }
    const R3Ray rayThroughFixed( cam.From(), cam.RayFromEye( m_ptRotateScale.first ) );
    const R3Ray rayThroughScale( cam.From(), cam.RayFromEye( m_ptRotateScale.second ) );
    
    const R3Pt ptRayThroughFixed = plane.IntersectRay( rayThroughFixed );
    const R3Pt ptRayThroughScale = plane.IntersectRay( rayThroughScale );
    const R3Vec vecDs = UnitSafe( ptRayThroughScale - ptRayThroughFixed );

    R3Matrix matRotateToPlane;
    R3Matrix::MatrixVecToVec( matRotateToPlane, R3Vec(1,0,0), R3Vec(0,0,1), vecDs, UnitSafe( plane.Normal() ) );

    const double dAngOrig = atan2( vecOrig[1], vecOrig[0] );

    const R3Matrix matRotToOrig = R3Matrix::Rotation2D( dAngOrig );
    const R2Vec vecInY = matRotToOrig * Rejection( vecOrig, vecDown );
	
    const double dLenX = dLenDown / dLenOrig;
    const double dLenY = dLenX * (1.0 + vecInY[1]);
    const R3Matrix matScl = R3Matrix::Scaling( dLenX, dLenY, 1.0 );
    const R3Matrix matRotScl = matRotateToPlane.Transpose() * matScl * matRotateToPlane;
    

    const R4Matrix matTransform = 
    R4Matrix::Translation( m_ptRotateScaleFixed )  
    * (R4Matrix) matRotScl
    * R4Matrix::Translation( R3Pt(0,0,0) - m_ptRotateScaleFixed );
    
    m_crvNetwork.Transform( m_iCursorOverCurve, matTransform, true );
}


/*
 * Basically drag two curves together
 *
 * The original curve click is saved in m_iSnapCurveFirst. The second curve and
 * curve location is in m_iCursorOverCurve and m_dCursorOverCurve.
 *
 * T values: Let the t values slide within a window of 2-3 times the selection distance. Use
 *  the original t values if the closest distance is at the end point of that t window. This
 *  lets the user be a bit fuzzy about clicking the closest point between the two curves.
 *
 * If near the end of a curve, snap to the end of the curve
 */
pair<double,double> ActionEvents::AdjustSnapTValues() const
{
	const Curve &crv1 = m_crvNetwork.GetCurve( m_iSnapCurveFirst.first );
	const Curve &crv2 = m_crvNetwork.GetCurve( m_iCursorOverCurve );
    
	double dCrv1 = m_iSnapCurveFirst.second, dCrv2 = m_dCursorOverCurve;
	R3Pt ptMiddle( Lerp( crv1(dCrv1), crv2(dCrv2), 0.5 ) );
	
    if ( RNIsZero( crv1.Length() ) || RNIsZero( crv2.Length() ) ) {
        cerr << "ERR: ActionEvents::AdjustSnapTValues Curve length is zero " << crv1.Length() << " " << crv2.Length() << "\n";
        return pair<double,double>( dCrv1, dCrv2 );
    }
    
    const double dSelDist = 3.0 * g_drawState.SelectionSize();
    const double dClampCrv1 = dSelDist / crv1.Length();
    const double dClampCrv2 = dSelDist / crv2.Length();
    /* First possibility - search for closest point between the two, limiting search range */
	double dDist;
	R3Pt ptClosest1, ptClosest2;
	for ( int i = 0; i < 3; i++ ) {
		dCrv1 = crv1.ClosestPointTValue( ptMiddle, dDist, ptClosest1 );
		dCrv2 = crv2.ClosestPointTValue( ptMiddle, dDist, ptClosest2 );
        dCrv1 = WINminmax( dCrv1, m_iSnapCurveFirst.second - dClampCrv1, m_iSnapCurveFirst.second + dClampCrv1 );
        dCrv2 = WINminmax( dCrv2, m_dCursorOverCurve - dClampCrv2, m_dCursorOverCurve + dClampCrv2 );
		ptMiddle = Lerp( ptClosest1, ptClosest2, 0.5 );
	}
    
    /* If found at end of range, just set back to what it was */
    if ( RNApproxEqual( fabs( dCrv1 - m_iSnapCurveFirst.second ), dClampCrv1, 1e-6 ) ) {
        dCrv1 = m_iSnapCurveFirst.second;
    }
    if ( RNApproxEqual( fabs( dCrv2 - m_dCursorOverCurve ), dClampCrv2, 1e-6 ) ) {
        dCrv2 = m_dCursorOverCurve;
    }
    
    // Now clamp to ends
    if ( dCrv1 < dClampCrv1 / 6.0 ) {
        dCrv1 = 0.0;
    }
    if ( dCrv2 < dClampCrv2 / 6.0 ) {
        dCrv2 = 0.0;
    }
    if ( 1.0 - dCrv1 < dClampCrv1 / 6.0 ) {
        dCrv1 = 1.0;
    }
    if ( 1.0 - dCrv2 < dClampCrv2 / 6.0 ) {
        dCrv2 = 1.0;
    }

    /* Now clamp to any existing point constraints that are close enough */
    const PointConstraint *opPC = m_crvNetwork.HasConstraintNoEdit( m_iCursorOverCurve, m_dCursorOverCurve );
    if ( opPC ) {
        dCrv2 = opPC->GetTValue( m_crvNetwork.GetCurveGroup( m_iCursorOverCurve ) );
    }
    
    return pair<double,double>(dCrv1, dCrv2);
}

void ActionEvents::SnapCurvesTogether()
{
	if ( m_iSnapCurveFirst.first == -1 || !OverCurve() ) {
		cerr << "ERR: SnapCurvesTogether, no curve " << m_iSnapCurveFirst.first << " " << m_iCursorOverCurve << "\n";
		return;
	}
	
    const pair<double,double> dCrvs = AdjustSnapTValues();
    
    PointConstraint *opNewPin = m_crvNetwork.NewSnapCurves( m_iCursorOverCurve, dCrvs.second, m_iSnapCurveFirst.first, dCrvs.first );
    
    if ( opNewPin && opNewPin->NumCurves() > 1 && !opNewPin->IsNormal() ) {
        opNewPin->ConvertToPointAndNormalConstraint( m_crvNetwork.Center() );
        opNewPin->ReEstablish();
    }
    
    m_iSnapCurveFirst.first = -1;
    m_iSnapCurveFirst.second = 0.0;
}

/*
 * Pick which curve is selected.
 * Correctly handles hidden curves and z values
 */
double ActionEvents::CurveSelected( const R3Ray &in_ray, int &out_iWhich, double &out_dT ) const
{
	double dClosest = 1e30;
	out_iWhich = -1;
	out_dT = -1.0;
	
	double dDist, dT;
	double dZBest = 1e30;
	R3Pt ptOut;

	int iIgnoreCurve = -1;
    if ( m_menuWhich == MENU_CURVE && m_menuSelection == MENU_CURVE_SNAP && m_action == MENU_SELECT_CURVE ) {
		iIgnoreCurve = m_iSnapCurveFirst.first;
	}
    
	const OGLObjsCamera &cam = g_drawState.GetCamera();
	const double dSelDist = g_drawState.SelectionSize();
	for ( int i = 0; i < m_crvNetwork.NumCrvs(); i++) {
		if ( ( m_action == MENU_DRAG || m_action == MENU_SELECT_CURVE ) && m_crvNetwork.IsHidden(i) ) {
			continue;
		}
        // Don't snap curve to itself
        if ( iIgnoreCurve == i ) {
            continue;
        }
        
		m_crvNetwork.GetCurve(i).ClosestPoint( in_ray, dT, dDist );
		bool bIsBetter = out_iWhich == -1 ? true : false;
		double dZ = cam.ProjectedPt( m_crvNetwork.GetCurve(i)( dT ) )[2];
		
		if ( dDist <= dSelDist && dClosest <= dSelDist ) {
			if ( dZ < dZBest ) {
				bIsBetter = true;
			}
		} else if ( dDist <= dSelDist && dClosest > dSelDist ) {
			bIsBetter = true;
		} else if ( dDist > dSelDist && dClosest <= dSelDist ) {
			bIsBetter = false;
		} else {
			bIsBetter = dDist < dClosest;
		}
		
		if ( bIsBetter ) {
			out_iWhich = i;
			dClosest = dDist;
			out_dT = dT;
			dZBest = dZ;
		}
	}   
	
	return dClosest;
}


/* Check the cursor location against the drag location and the (potential) close second
 * location on the curve, projected onto the given plane.
 * Call once for each visible plane. 
 *
 * The following three are activated from DRAG_CURVE by clicking on the appropriate icon
 * Scale/rotate/rotate-scale Icon appears when cursor is close to curve but not near drag icon
 *    Scale: Cursor is below curve
 *    Roate/scale: Cursor is above curve
 *    Rotate only: Cursor is on the curve
 *
 * Selection distance is set by dDragSize
 */
 
const bool s_bTraceDrag = false;
ActionEvents::Action ActionEvents::WhichDragAction( const R3Plane &in_plane, const R2Pt &in_pt, double &out_dTOnCrv ) const
{
    /* Matrices that map canonical coordinate system to 3D
     * Rotation aligns the x axis with the tangent of the curve, projected onto the plane
     *  z-axis is aligned with plane normal
     * Then takes the entire coordinate system to the 3D curve point, projected onto the plane
     *
     * Returns the t value of the closest point to the curve, when the curve is projected
     *   into that plane
     */
    const R3Vec vecPlaneNorm = in_plane.Normal();
    
    IsCursorCloseToActiveCurve( in_plane, out_dTOnCrv );
    
    const R4Matrix matRotScl = MatrixRotateScaleIcon( in_plane, out_dTOnCrv );
    const R4Matrix matDrag = MatrixDragIcon( IsSubset() ? out_dTOnCrv : m_dCursorOverCurve, in_plane, false );
    
    const OGLObjsCamera &cam = g_drawState.GetCamera();
	const Curve &crv = m_crvNetwork.GetCurve( m_iCursorOverCurve);
    /* project the curve point onto the plane, then project it onto the screen */
    const R2Pt ptCrv = cam.CameraPt( in_plane.ProjectOnPlane( crv( out_dTOnCrv ) ) );
    
    R2Polygon poly(4);
    const double dDragSize = g_paramFile.GetDouble("CurveDragSize") * g_drawState.SelectionSize();
    for ( int i = 0; i < 4; i++ ) {
        const int iX = i / 2;
        const int iY = ((i+3) % 4) / 2;
        const R3Pt pt( -dDragSize + 2.0 * dDragSize * iX, -dDragSize + 2.0 * dDragSize * iY, 0.0 );
        const R3Pt ptMap = matDrag * pt;
        poly[i] = cam.CameraPt( ptMap );
    }

    if ( poly.InsideQuick( in_pt ) ) {
        return DRAG_CURVE;
    }

    if ( IsSubset() ) {
        return NO_ACTION;
    }
    
    const R3Pt ptTop( 0.0, -dDragSize, 0.0 );
    
    const R2Pt ptScreenTop = cam.CameraPt( matRotScl * R3Pt( 0.0, dDragSize, 0.0 ) );
    const R2Pt ptScreenBot = cam.CameraPt( matRotScl * R3Pt( 0.0, -dDragSize, 0.0 ) );
    
    const R2Line_seg seg( ptScreenTop, ptScreenBot );
    double dTOnSeg, dDistToSeg;
    R2Pt ptOut;
    seg.FindPtOnSeg( in_pt, ptOut, dTOnSeg, dDistToSeg );
    
    const double dLenSeg = seg.Length();
    if ( dTOnSeg <= 0.0 || dTOnSeg >= 1.0 || dDistToSeg > dLenSeg ) {
        return NO_ACTION;
    }
    if ( dTOnSeg < 0.25 ) {
        if ( s_bTraceDrag ) cout << "Above, rotate scale\n";
        return ROTATE_SCALE_CURVE;
    }
    if ( dTOnSeg > 0.75 ) {
        if ( s_bTraceDrag ) cout << "Below, scale\n";
        return SCALE_CURVE;
    }
    
    if ( s_bTraceDrag ) cout << "On curve - rotate\n";
    return ROTATE_CURVE;
}

/*
 * Determine what (if anything) is the current selected drag action
 * 1) If cursor is in square around mouse down point, return drag action
 * 2) If cursor is within drag distance of curve, return rotate (on curve),
 *    rotate-scale (below curve) or scale (above curve).
 * 3) Also return which plane the selection happened in
 *    If drawing plane is visible, use drawing plane, otherwise use back wall
 *    If shadow box is visible, also check side wall and floor
 *
 * In case of overlap, main curve takes precedence over shadow box walls
 */
pair<ActionEvents::Action, ShadowBox::Surface>
ActionEvents::WhichDragAction( const R2Pt &in_pt, double &out_dTOnCrv ) const
{
    pair<ActionEvents::Action, ShadowBox::Surface> ret( NO_ACTION, ShadowBox::DRAWING_PLANE );
    if ( !OverCurve() ) {
        return ret;
    }

    /// Use either the drawing plane or the view plane, whichever is visible
    const R3Plane planeDraw( m_ptRotateScaleFixed, g_drawState.GetShadowBox().GetPlane( ShadowBox::DRAWING_PLANE ).Normal() );

    const Action action = WhichDragAction( planeDraw, in_pt, out_dTOnCrv );
    if ( action != NO_ACTION ) {
        ret.first = action;
        return ret;
    }
    
    const double dSave = out_dTOnCrv;
    
    if ( g_drawState.m_opUI->m_bShowShadowBox->value() ) {
        const Action actionFloor = WhichDragAction( g_drawState.GetShadowBox().FloorPlane(), in_pt, out_dTOnCrv );
        if ( actionFloor != NO_ACTION ) {
            if ( s_bTraceDrag ) cout << "Floor\n";

            ret.first = actionFloor;
            ret.second = ShadowBox::FLOOR;

            return ret;
        }
        const Action actionSide = WhichDragAction( g_drawState.GetShadowBox().SideWallPlane(), in_pt, out_dTOnCrv );
        if ( actionSide != NO_ACTION ) {
            if ( s_bTraceDrag ) cout << "Sidewall\n";

            ret.first = actionSide;
            ret.second = ShadowBox::SIDEWALL;
            return ret;
        }
    }

    if ( IsSubset() ) {
        out_dTOnCrv = dSave;
    }
    
    return ret;
}

/*
 * Expand the region selected based on the new m_dCursorOverCurve
 * If close to the end, snap selection to the end
 */
void ActionEvents::SetCursorOverTs( )
{
	if ( !OverCurve() ) {
        cerr << "ERR: ActionEvents::SetCursorOverTs, not over curve\n";
        return;
    }
    
    const double dPrevFirst = m_dCursorOverCurveTs.first;
    const double dPrev = m_dCursorOverCurve;
    const double dPrevSecond = m_dCursorOverCurveTs.second;

	if ( m_dCursorOverCurveTs.first == -1.0 ) {
		m_dCursorOverCurveTs.first = m_dCursorOverCurve;
    }
	if ( m_dCursorOverCurveTs.second == -1.0 ) {
		m_dCursorOverCurveTs.second = m_dCursorOverCurve;
    }
    
    const Curve &crv = m_crvNetwork.GetCurve( m_iCursorOverCurve );

    if ( crv.IsClosed() ) {
        if ( m_dCursorOverCurve < 0.0 ) {
            m_dCursorOverCurveTs.first = WINmin( m_dCursorOverCurve, m_dCursorOverCurveTs.first );
        } else if ( m_dCursorOverCurve > 1.0 ) {
            m_dCursorOverCurveTs.second = WINmax( m_dCursorOverCurve, m_dCursorOverCurveTs.second );
            
        } else {
            const double dDistFirst = WINmin( fabs( m_dCursorOverCurveTs.first - m_dCursorOverCurve ), fabs( m_dCursorOverCurveTs.first + 1.0 - m_dCursorOverCurve ) );
            const double dDistSecond = WINmin( fabs( m_dCursorOverCurveTs.second - m_dCursorOverCurve ), fabs( m_dCursorOverCurveTs.second - 1.0 - m_dCursorOverCurve ) );
            if ( dDistFirst < dDistSecond ) {
                if ( s_bTrace ) cout << "Closer end 1\n";
                if ( fabs( m_dCursorOverCurveTs.first - m_dCursorOverCurve ) < fabs( m_dCursorOverCurveTs.first + 1.0 - m_dCursorOverCurve ) ) {
                    if ( s_bTrace ) cout << "Case 1\n";

                    m_dCursorOverCurveTs.first = WINmin( m_dCursorOverCurveTs.first, m_dCursorOverCurve );
                } else {
                    m_dCursorOverCurveTs.first = WINmin( m_dCursorOverCurveTs.first, m_dCursorOverCurve - 1.0 );
                }
            } else {
                if ( s_bTrace ) cout << "Closer end 2\n";
                if ( fabs( m_dCursorOverCurveTs.second - m_dCursorOverCurve ) < fabs( m_dCursorOverCurveTs.second - 1.0 - m_dCursorOverCurve ) ) {
                    if ( s_bTrace ) cout << "Case 1\n";
                    m_dCursorOverCurveTs.second = WINmax( m_dCursorOverCurveTs.second, m_dCursorOverCurve );
                } else {
                    m_dCursorOverCurveTs.second = WINmax( m_dCursorOverCurveTs.second, m_dCursorOverCurve + 1.0 );
                }
            }
        }
        if ( m_dCursorOverCurveTs.first < 0.0 && m_dCursorOverCurveTs.second >= 1.0 ) {
            m_dCursorOverCurveTs.second = m_dCursorOverCurveTs.first + 1.0;
        }
    } else {
		m_dCursorOverCurveTs.first = WINmin( m_dCursorOverCurve, m_dCursorOverCurveTs.first );
		m_dCursorOverCurveTs.second = WINmax( m_dCursorOverCurve, m_dCursorOverCurveTs.second );

		const double dTCloseEnd = g_drawState.SelectionSize() / crv.Length();
        
		if ( m_dCursorOverCurveTs.first < dTCloseEnd ) {
			m_dCursorOverCurveTs.first = 0.0;
		}
		if ( m_dCursorOverCurveTs.second > 1.0 - dTCloseEnd ) {
			m_dCursorOverCurveTs.second = 1.0;
		}
    }

    if ( s_bTraceCurveSelection ) {
        if ( m_dCursorOverCurveTs.first != dPrevFirst || m_dCursorOverCurve != dPrev || m_dCursorOverCurveTs.second != dPrevSecond ) {
            cout << "ActionEvents::SetCursorOverTs ts: " << m_dCursorOverCurveTs.first << " " << m_dCursorOverCurve << " " << m_dCursorOverCurveTs.second << "\n";
        }
    }
    
}

/*
 * Update the selected curve region. Correctly handle scrubbing along the curve so that
 * t values are correct when scrubbing over a self-intersection
 *
 * Sets m_dCursorOverCurve and m_dCursorOverCurveTs (through SetCursorOverTs())
 */
void ActionEvents::SelectedCurveRegion( const R3Ray &in_ray )
{
	if ( !OverCurve() ) {
		cerr << "ERR: In ActionEvents::SelectedCurveRegion - no curve selected!!!\n";			
	} else {
		
		double dDist, dT = m_dCursorOverCurve;
		m_crvNetwork.GetCurve(m_iCursorOverCurve).ClosestPoint( in_ray, dT, dDist, true );
		
		if ( dDist < g_drawState.SelectionSize() ) {
			if ( m_action == DRAG_CURVE ) {
				m_dCursorOverCurve = WINminmax( dT, m_dCursorOverCurveTs.first, m_dCursorOverCurveTs.second );
			} else if ( m_action == MENU_SELECT_CURVE_REGION ) {
				m_dCursorOverCurve = dT;
				SetCursorOverTs();
			}
		}
	}
    if ( s_bTraceCurveSelection ) {
        cout << "ActionEvents::SelectedCurveRegion ts: " << m_dCursorOverCurveTs.first << " " << m_dCursorOverCurve << " " << m_dCursorOverCurveTs.second << "\n";
    }
}


/* Clear curve selected, t value selected, and region selected */
void ActionEvents::ClearCurveSelected()
{
    if ( s_bTraceCurveSelection ) {
        if ( m_dCursorOverCurve != -1.0 || m_dCursorOverCurveTs.first != -1.0 || m_dCursorOverCurveTs.second != -1.0 ) {
            cout << "ActionEvents::ClearCurveSelected ts: " << m_dCursorOverCurveTs.first << " " << m_dCursorOverCurve << " " << m_dCursorOverCurveTs.second << "\n";
        }
    }
    
    if ( m_iCursorOverCurve != -1 && m_iCursorOverCurve < m_crvNetwork.NumCrvs() ) {
        m_crvNetwork.ClearSelectionRegion( m_iCursorOverCurve );
    }

	m_iCursorOverCurve = -1;
	m_dCursorOverCurve = -1.0;
	m_dCursorOverCurveTs.first = -1.0;
	m_dCursorOverCurveTs.second = -1.0;
    
    m_iCursorOverCurveIndex = -1;
	m_iCursorOverCurveTs.first = -1;
	m_iCursorOverCurveTs.second = -1;
    
    m_dTDragClick = -1.0;
}

/// Also set the color correctly
void ActionEvents::SetCurveSelected( const int in_iCurve, const double in_dT )
{
    if ( in_iCurve < -1 || in_iCurve >= m_crvNetwork.NumCrvs() ){
        cerr << "ERR: SetCurveSelected, bad curve id " << in_iCurve << " " << m_crvNetwork.NumCrvs() << "\n";
        return;
    }
    
    if ( s_bTraceCurveSelection ) {
        if ( m_dCursorOverCurve != -1.0 || m_dCursorOverCurveTs.first != -1.0 || m_dCursorOverCurveTs.second != -1.0 ) {
            cout << "ActionEvents::SetCurveSelected old: " << m_iCursorOverCurve << " new: " << m_iCursorOverCurve << "\n";
        }
    }
    
    if ( m_iCursorOverCurve != -1 && m_iCursorOverCurve < m_crvNetwork.NumCrvs() ) {
        m_crvNetwork.ClearSelectionRegion( m_iCursorOverCurve );
    }
    
    m_iCursorOverCurve = in_iCurve;
    m_dCursorOverCurve = in_dT;
    
    if ( m_iCursorOverCurve != -1 && m_iCursorOverCurve < m_crvNetwork.NumCrvs() ) {
        m_crvNetwork.SetSelectionRegion( m_iCursorOverCurve, std::pair<double,double>( m_dCursorOverCurve, m_dCursorOverCurve ) );
    }
}

/*
 * Pop last action off of the stack
 */
void ActionEvents::Undo()
{
    if ( s_bTrace ) cout << "Undo\n";
    /// TODO: Undo
}

// Delete all of the curves
void ActionEvents::Clear()
{
	if ( m_outLogFile.good() ) m_outLogFile << "Clear\n";
	
	m_aUndo.need(0);
	m_crvNetwork.Clear();
	m_srfData.Clear();
	m_srfInflation.Clear();
	
	// Just in case in some persistent state like dragging
	m_action = NO_ACTION;
	m_menuWhich = NO_MENU_SELECTED;
	m_menuSelection = NO_MENU_SELECTED;
	ClearCurveSelected();
	
}

/// set all hide flags to false
void ActionEvents::ShowAll()
{
	if ( m_outLogFile.good() ) m_outLogFile << "ShowAll\n";
	
	for ( int i = 0; i < m_crvNetwork.NumCrvs(); i++ ) {
		if ( m_crvNetwork.IsHidden(i) ) {
			m_crvNetwork.ToggleHideCurve(i);
		}
	}
}

/// set all hide flags to false for closed curves, true for non-closed
void ActionEvents::ShowAllClosed()
{
	if ( m_outLogFile.good() ) m_outLogFile << "ShowAllClosed\n";
	
	for ( int i = 0; i < m_crvNetwork.NumCrvs(); i++ ) {
		if ( m_crvNetwork.GetCurve(i).IsClosed() ) {
			if ( m_crvNetwork.IsHidden(i) ) {
				m_crvNetwork.ToggleHideCurve(i);
			}
		} else {
			if ( !m_crvNetwork.IsHidden(i) ) {
				m_crvNetwork.ToggleHideCurve(i);
			}
		}
	}
}

/// set all hide flags to true
void ActionEvents::HideAll()
{
	if ( m_outLogFile.good() ) m_outLogFile << "HideAll\n";
	
	for ( int i = 0; i < m_crvNetwork.NumCrvs(); i++ ) {
		if ( ! m_crvNetwork.IsHidden(i) ) {
			m_crvNetwork.ToggleHideCurve(i);
		}
	}
}

// Delete stroke history for all curves
void ActionEvents::FinalizeAll()
{
	if ( m_outLogFile.good() ) m_outLogFile << "FinalizeAll\n";
	
	for ( int i = 0; i < m_crvNetwork.NumCrvs(); i++ ) {
		m_crvNetwork.FinalizeAll();
	}
}



/*
 * Legacy: Select a curve projected onto the drawing plane
 * Use to be any wall could be a drawing plane; since this isn't true any more, this is sort of boring
 */
double ActionEvents::CurveDrawingWallSelected( const R3Ray &in_ray, int &out_iWhich, double &out_dT ) const
{
	double dClosest = 1e30;
	out_iWhich = -1;
	out_dT = -1.0;
	
	double dDist, dT;
	R3Pt ptOut;
	const R3Plane plane = g_drawState.DrawingPlane( );
	for ( int i = 0; i < m_crvNetwork.NumCrvs(); i++) {
		m_crvNetwork.GetCurve(i).Project(plane).ClosestPoint( in_ray, dT, dDist );
		if ( dDist < dClosest ) {
			out_iWhich = i;
			dClosest = dDist;
			out_dT = dT;
		}
	}   
	
	return dClosest;
}

/*
 * Legacy: Select a curve's shadow
 */
double ActionEvents::CurveShadowSelected( const R3Ray &in_ray, ShadowBox::Surface &out_srf, int &out_iWhich, double &out_dT ) const
{
	double dClosest = 1e30;
	out_iWhich = -1;
	out_dT = -1.0;
	
	double dDist, dT;
	R3Pt ptOut;
	R3Plane plane;
	ShadowBox::Surface srf;
	for ( int j = 0; j < 3; j++ ) {
		if ( j == 0 ) {
			plane = m_shadowbox.BackWallPlane();
			srf = ShadowBox::BACKWALL;
		} else if ( j == 1 ) {
			plane = m_shadowbox.SideWallPlane();
			srf = ShadowBox::SIDEWALL;
		} else {
			plane = m_shadowbox.FloorPlane();
			srf = ShadowBox::FLOOR;
		}
		
		for ( int i = 0; i < m_crvNetwork.NumCrvs(); i++) {
			m_crvNetwork.GetCurve(i).Project(plane).ClosestPoint( in_ray, dT, dDist );
			if ( dDist < dClosest ) {
				out_iWhich = i;
				dClosest = dDist;
				out_dT = dT;
				out_srf = srf;
			}
		}   
	}
	
	return dClosest;
}

/*
 * Legacy: Select a curve, a curve's shadow, or a curve projected onto the drawing plane, whichever is closest
 */
bool ActionEvents::CurveSelectedBest( const R3Ray &in_ray, ShadowBox::Surface &out_srf, int &out_iWhich, double &out_dT ) const
{
	int iCrvSpace, iCrvSelectedWall, iCrvAnyShadow;
	double dCrvSpaceT, dCrvSelectedWallT, dCrvAnyShadowT;
	ShadowBox::Surface srfAnyShadow;
	
	const double dCrvSpaceDist = CurveSelected( in_ray, iCrvSpace, dCrvSpaceT );
	const double dCrvSelectedWallDist = CurveDrawingWallSelected( in_ray, iCrvSelectedWall, dCrvSelectedWallT );
	const double dCrvAnyShadowDist = CurveShadowSelected( in_ray, srfAnyShadow, iCrvAnyShadow, dCrvAnyShadowT );
	
	out_iWhich = -1;
	out_dT = 1e30;
	out_srf = ShadowBox::NO_SURFACE;
	if ( dCrvSpaceDist < g_drawState.SelectionSize() ) {
		out_iWhich = iCrvSpace;
		out_dT = dCrvSpaceT;
		out_srf = ShadowBox::NO_SURFACE;
		return true;
	} else if ( dCrvSelectedWallDist < g_drawState.SelectionSize() ) {
		out_iWhich = iCrvSelectedWall;
		out_dT = dCrvSelectedWallT;
		out_srf = ShadowBox::DRAWING_PLANE;
		return true;
	} else if ( dCrvAnyShadowDist < g_drawState.SelectionSize() ) {
		out_iWhich = iCrvAnyShadow;
		out_dT = dCrvAnyShadowT;
		out_srf = srfAnyShadow;
		return true;
	}
	return false;
}


