/*
 *  InflationSurface.cpp
 *  SketchCurves
 *
 *  Created by Cindy Grimm on 4/27/11.
 *  Copyright 2011 Adobe Corp. All rights reserved.
 *
 */

#include "InflationSurface.h"
#include <OpenGL/OGLDraw3D.H>
#include <OpenGL/OGLObjs_Camera.H>
#include "UserInterface.h"
#include "ParamFile.h"

const bool s_bTrace = false;

/* I'm sure this could be faster... */
inline R3Pt IntersectTriangle( const R3Ray & in_ray,
							   const R2Pt & in_ptScreen, 
                               const R3Pt &in_pt1, const R3Pt &in_pt2, const R3Pt &in_pt3,
                               double &out_dDist )
{	
    static R3Polygon s_poly(3);
	static R2Polygon s_poly2D(3);
    s_poly[0] = in_pt1;
    s_poly[1] = in_pt2;
    s_poly[2] = in_pt3;
	
	for ( int i = 0; i < 3; i++ ) {
		s_poly2D[i] = g_drawState.GetCamera().CameraPt( s_poly[i] );
	}
	
	const R3Plane plane( in_pt1, s_poly.Normal() );
	if ( s_poly2D.InsideQuick( in_ptScreen ) ) {
		out_dDist = 0.0;
		return plane.IntersectRay(in_ray);
	}
	
	out_dDist = ::Length( in_ptScreen - s_poly2D.Closest_pt( in_ptScreen ) );
	return plane.IntersectRay(in_ray);
}

/* Loop through all triangles and keep closest intersection found */
R3Pt InflationSurface::IntersectSurface( const R2Pt &in_ptScreen ) const
{
    const OGLObjsCamera &cam = g_drawState.GetCamera();
	const R3Ray ray( cam.From(), cam.RayFromEye(in_ptScreen) );
    
	cout.precision(8);
	
    R3Pt ptRet;
    double dDistBest = 1e30;
	double dZBest = 1e30;
    double dDist1, dDist2;
    for ( int i = 0; i < m_iWidth - 1; i++ ) {
        for ( int j = 0; j < m_iHeight - 1; j++ ) {
            const R3Pt ptTri1 = IntersectTriangle( ray, in_ptScreen, m_aaptGrid[i][j], m_aaptGrid[i][j+1], m_aaptGrid[i+1][j], dDist1 );
            const R3Pt ptTri2 = IntersectTriangle( ray, in_ptScreen, m_aaptGrid[i+1][j+1], m_aaptGrid[i+1][j], m_aaptGrid[i][j+1], dDist2 );

            const double dZ1 = cam.ProjectedPt(ptTri1)[2];
            const double dZ2 = cam.ProjectedPt(ptTri2)[2];
			if ( RNIsZero( dDist1 ) ) {
				if ( !RNIsZero( dDistBest ) || dZ1 < dZBest ) {
					ptRet = ptTri1;
					dDistBest = dDist1;
					dZBest = dZ1;
				}
			} else if ( dDist1 < dDistBest ) {
				ptRet = ptTri1;
				dDistBest = dDist1;
				dZBest = dZ1;
			}
			if ( RNIsZero( dDist2 ) ) {
				if ( !RNIsZero( dDistBest ) || dZ2 < dZBest ) {
					ptRet = ptTri2;
					dDistBest = dDist2;
					dZBest = dZ2;
				}
			} else if ( dDist2 < dDistBest ) {
				ptRet = ptTri2;
				dDistBest = dDist2;
				dZBest = dZ2;
			}
        }
    }
    return ptRet;
}

/* Convert screen curve to 3D curve by intersecting with surface
 * Takes closest point if no intersection */
Curve InflationSurface::DrawOnSurface( const ScreenCurve &in_crvScreen ) const
{
    Curve crv;
    
    if ( m_bValid ) {
        for ( int i = 0; i < in_crvScreen.NumPts(); i++ ) {
            crv.AddPoint( IntersectSurface( in_crvScreen.Pt(i) ) );
        }
    }

    crv.SetAllData();
    return crv;
}

/* For each curve point, walk out in in_vecDir some amount to make the surface */
void InflationSurface::AlignToCurve( const Curve &in_crv, const R3Vec &in_vecDir )
{
    const OGLObjsCamera &cam = g_drawState.GetCamera();

    const R3Plane plane( in_crv.CenterPoint(), cam.Look() );
    const R3Pt ptTopLeft = plane.IntersectRay( R3Ray( cam.From(), cam.RayFromEye( R2Pt(-1.0,-1.0) ) ) );
    const R3Pt ptLowRight = plane.IntersectRay( R3Ray( cam.From(), cam.RayFromEye( R2Pt(1.0,1.0) ) ) );
    const double dDiagonal = ::Length( ptTopLeft - ptLowRight );
    const double dScl = 0.25 * dDiagonal / sqrt(2.0); // how far to walk out
    //const double dScl = in_crv.Length() / (m_iWidth - 1.0);
    
    const double dTSampleForGrid = 1.0 / (m_iWidth - 1.0);
    for ( int i = 0; i < m_iWidth; i++ ) {
        const R3Pt pt = in_crv( i * dTSampleForGrid );
        
        for ( int j = 0; j < m_iHeight; j++ ) {
            const double dPerc = (double) j / ( m_iHeight - 1.0 );
            const double dHeight = (dPerc - 0.5) * dScl; 
            ASSERT( dPerc >= 0.0 && dPerc <= 1.0 );
            m_aaptGrid[i][j] = pt + in_vecDir * dHeight;
        }
    }
    
    m_crvLeft = in_crv;
    m_crvRight.Clear();
    
    SetNormals();
    m_bValid = true;
}

/* clear curves */
void InflationSurface::Clear()
{
	m_crvLeft.Clear();
	m_crvRight.Clear();
	m_bValid = false;
}

/* Put half circles between the bounding curve points */
void InflationSurface::BuildSurface()
{
    const OGLObjsCamera &cam = g_drawState.GetCamera();
    const R3Vec vec11 = m_crvRight(0.0) - m_crvLeft(0.0);
    const R3Vec vec12 = m_crvRight(0.0) - m_crvLeft(1.0);
    const R3Vec vec21 = m_crvRight(1.0) - m_crvLeft(0.0);
    const R3Vec vec22 = m_crvRight(1.0) - m_crvLeft(1.0);
    const double dDot1122 = fabs( Dot( UnitSafe( vec11 ), UnitSafe( vec22 ) ) );
    const double dDot1221 = fabs( Dot( UnitSafe( vec12 ), UnitSafe( vec21 ) ) );
	const double dLen1122 = ::Length( vec11 ) + ::Length( vec22 );
	const double dLen1221 = ::Length( vec12 ) + ::Length( vec21 );
	if ( dLen1221 < 0.8 * dLen1122 ) {
        m_crvLeft = m_crvLeft.Reverse();
	} else if ( ! (dLen1122 < 0.8 * dLen1221 ) ) {
		// Lengths about the same - check angle
		if ( dDot1221 > dDot1122 ) {
			m_crvLeft = m_crvLeft.Reverse();
		}
	}

    if ( s_bTrace ) {
        cout << "Building surface " << m_crvLeft.NumPts() << " " << m_crvRight.NumPts() << "\n";
    }
    const double dTSampleForGrid = 1.0 / (m_iHeight - 1.0);
    const R3Vec vecLook = - cam.Look();
    for ( int i = 0; i < m_iHeight; i++ ) {
        const R3Pt ptLeft = m_crvLeft( i * dTSampleForGrid);
        const R3Pt ptRight = m_crvRight( i * dTSampleForGrid);
        const R3Vec vecBetween = ptRight - ptLeft;
        const R3Vec vecTang1 = m_crvLeft.Tangent(i * dTSampleForGrid);
        const R3Vec vecTang2 = m_crvRight.Tangent(i * dTSampleForGrid);
        const R3Vec vecTang = Dot( vecTang1, vecTang2 ) < 0.0 ? UnitSafe( vecTang1 - vecTang2 ) : UnitSafe( vecTang1 + vecTang2 );
        const R3Vec vecNorm = UnitSafe( Rejection( vecTang, vecLook ) );
        const double dMaxOffset = ::Length( vecBetween ) * 0.5;
        
        for ( int j = 0; j < m_iWidth; j++ ) {
            const double dPerc = (double) j / ( m_iWidth - 1.0 );
            const double dHeight = dMaxOffset * sqrt( 1.0 - pow( 2.0 * (0.5 - dPerc), 2 ) ); 
            ASSERT( dPerc >= 0.0 && dPerc <= 1.0 );
            m_aaptGrid[j][i] = Lerp( ptLeft, ptRight, dPerc ) + vecLook * dHeight;
        }
    }

    SetNormals();
    m_bValid = true;
}

void InflationSurface::SetNormals()
{
    for ( int i = 0; i < m_aaptGrid.num(); i++ ) {
        const int iNextRow = i == m_aaptGrid.num() - 1 ? i - 1 : i+1;
        for ( int j = 0; j < m_aaptGrid[i].num(); j++ ) {
            const int iNextCol = j == m_aaptGrid[i].num() - 1 ? j - 1 : j+1;
            const R3Vec vec1 = m_aaptGrid[iNextRow][j] - m_aaptGrid[i][j];
            const R3Vec vec2 = m_aaptGrid[i][iNextCol] - m_aaptGrid[i][j];
            m_aavecGrid[i][j] = UnitSafe( Cross( vec1, vec2 ) );
            if ( i == m_aaptGrid.num()-1 || j == m_aaptGrid[i].num() - 1 ) {
                m_aavecGrid[i][j] *= -1.0;
            }
        }
    }
    m_aavecGrid.last().last() *= -1.0;
}

/*
 * Walk along the screen curve and look for the closest intersections in the curve network
 * Only keep points that had intersections
 * 
 * Then resample curve to get evenly spaced points
 */
void InflationSurface::CreateBoundaryCurve( const CurveNetwork &in_crvNetwork, const ScreenCurve &in_crvScreen, const bool in_bDoLeft )
{
    Curve &crv = in_bDoLeft ? m_crvLeft : m_crvRight;
    Curve crvTemp;

    if ( in_bDoLeft ) m_bValid = false;
    
    const int iNSamples = 4 * m_iHeight;
    const double dTSample = 1.0 / ( iNSamples - 1.0 );
    
    const OGLObjsCamera &cam = g_drawState.GetCamera();
    const double dDistAllowed = 3.0 * g_drawState.SelectionSize();

    int iWhich = 0;
    double dT = 0.0;
    for ( int i = 0; i < iNSamples; i++ ) {
        const R3Ray ray( cam.From(), cam.RayFromEye( in_crvScreen( i * dTSample ) ) );
        const double dDist = in_crvNetwork.ClosestCurve( ray, true, iWhich, dT );
        if ( dDist < dDistAllowed ) {
            crvTemp.AddPoint( in_crvNetwork.GetCurve( iWhich )(dT) );
        }
    }
    
    if ( crvTemp.NumPts() < 2 ) {
        cerr << "ERR: Boundary curve not over any unhidden curves\n";
        return;
    }
    crvTemp.SetAllData();
    
    crv.Clear();
    const double dTSampleForGrid = 1.0 / (m_iHeight - 1.0);
    for ( int i = 0; i < m_iHeight; i++ ) {
        crv.AddPoint( crvTemp( i * dTSampleForGrid ) );
    }
    crv.SetAllData();
    
    if ( m_crvLeft.NumPts() && m_crvRight.NumPts() && in_bDoLeft == false ) {
        BuildSurface();
    }
    //crv.SetStateBoundary();
}

/* Did they click on one of the spheres at either end of the curves/surface? */
int InflationSurface::SelectSurface( const R2Pt &in_ptScreen, double &out_dZ ) const
{
    out_dZ = 1e30;
    
	if ( g_drawState.m_opUI && !( g_drawState.m_opUI->m_bShowInflationSrf->value() || g_drawState.m_opUI->m_bShowExtrusionSrf->value() ) ) {
		return -1;
	}
	
    const OGLObjsCamera &cam = g_drawState.GetCamera();
    const R3Line line( cam.From(), cam.RayFromEye( in_ptScreen ) );
    
    const double dDistCrv1Top = line.Dist_to_line( m_crvLeft(0.0) );
    const double dDistCrv1Bot = line.Dist_to_line( m_crvLeft(1.0) );
    const double dDistCrv2Top = line.Dist_to_line( m_crvRight(0.0) );
    const double dDistCrv2Bot = line.Dist_to_line( m_crvRight(1.0) );
    const double dDistSrf = line.Dist_to_line( m_aaptGrid[0][ m_iHeight / 2 ] );
    
    const double dDistSel = g_paramFile.GetDouble("IntersectionSrfSelSize") * g_drawState.PixelWidth();
    if ( m_bValid && dDistSrf < dDistCrv1Top && dDistSrf < dDistCrv1Bot && dDistSrf < dDistCrv2Top && dDistSrf < dDistCrv2Bot && dDistSrf < dDistSel ) {
        out_dZ = dDistSrf;
        return 4;
    } else if ( dDistCrv1Top < dDistCrv1Bot && dDistCrv1Top < dDistCrv2Top && dDistCrv1Top < dDistCrv2Bot && dDistCrv1Top < dDistSel ) {
        out_dZ = dDistCrv1Top;
        return 0;
    } else if ( dDistCrv1Bot < dDistCrv2Top && dDistCrv1Bot < dDistCrv2Bot && dDistCrv1Bot < dDistSel ) {
        out_dZ = dDistCrv1Bot;
        return 1;
    } else if ( dDistCrv2Top < dDistCrv2Bot && dDistCrv2Top < dDistSel ) {
        out_dZ = dDistCrv2Top;
        return 2;
    } else if ( dDistCrv2Bot < dDistSel ) {
        out_dZ = dDistCrv2Bot;
        return 3;
    }
    return -1;
}

void InflationSurface::SetSize( const int in_iW, const int in_iH )
{
    m_iWidth = WINminmax( in_iW, 3, 150 );
    m_iHeight = WINminmax( in_iH, 3, 150 );
    
    m_aaptGrid.need( m_iWidth );
    m_aavecGrid.need( m_iWidth );
    for ( int i = 0; i < m_aaptGrid.num(); i++ ) {
        m_aaptGrid[i].need( m_iHeight );
        m_aavecGrid[i].need( m_iHeight );
    }
}

InflationSurface::InflationSurface() 
{ 
    m_bValid = false;
    SetSize(40,10); 
}


void InflationSurface::Draw( const int in_iSel, const EditAction in_editAction ) const
{
    const UTILSColor colSrf = g_paramFile.GetColor( "IntersectionSrfCol" );
    const UTILSColor col = g_paramFile.GetColor( "IntersectionSrfCurveCol" );
    const UTILSColor colSel = g_paramFile.GetColor( "IntersectionSrfSelected" );
    const double dDistSel = g_paramFile.GetDouble("IntersectionSrfSelSize") * g_drawState.PixelWidth();

    if ( m_bValid ) {
        g_drawState.StartShader( DrawState::SURFACE_SHADING );	
        glDisable( GL_CULL_FACE );
        
        if ( g_drawState.m_opUI->m_bSmoothShade->value() ) {
            glShadeModel(GL_SMOOTH);
        } else {
            glShadeModel(GL_FLAT);
        }
        glColor4f( colSrf[0], colSrf[1], colSrf[2], 0.75f );
        
        glNormal3d( 0.0, 0.0, 1.0 );
        for ( int i = 0; i < m_aaptGrid.num() - 1; i++ ) {
            glBegin( GL_QUAD_STRIP );
            
            for ( int j = 0; j < m_aaptGrid[i].num(); j++ ) {
                glNormal3dv( &m_aavecGrid[i][j][0] );
                glVertex3dv( &m_aaptGrid[i][j][0] );
                glNormal3dv( &m_aavecGrid[i+1][j][0] );
                glVertex3dv( &m_aaptGrid[i+1][j][0] );
            }
            glEnd();
        }
        
        if ( g_drawState.m_opUI->m_bShowEdges->value() ) {
            glColor4f( 0.0, 0.0, 0.0, 1.0f );
            for ( int i = 0; i < m_aaptGrid.num(); i++ ) {
                glBegin( GL_LINE_STRIP );
                for ( int j = 0; j < m_aaptGrid[i].num(); j++ ) {
                    glNormal3dv( &m_aavecGrid[i][j][0] );
                    glVertex3dv( &m_aaptGrid[i][j][0] );
                }
                glEnd();
            }
            for ( int i = 0; i < m_iWidth; i++ ) {
                glBegin( GL_LINE_STRIP );
                for ( int j = 0; j < m_iHeight; j++ ) {
                    glNormal3dv( &m_aavecGrid[j][i][0] );
                    glVertex3dv( &m_aaptGrid[j][i][0] );
                }
                glEnd();
            }
        }
        
        if ( in_editAction != DRAW_CURVE ) {
            if ( in_iSel == 4 ) {
                glColor4f( colSel[0], colSel[1], colSel[2], 1.0f );
            } else {
                glColor4f( col[0], col[1], col[2], 1.0f );
            }
            OGLDrawSphere( R3Sphere( m_aaptGrid[0][ m_iHeight / 2 ], dDistSel ) );
        }        
        g_drawState.EndShader();
    }
    
    //m_crvLeft.DrawAsBoundary();    
    //m_crvRight.DrawAsBoundary();
    
    g_drawState.StartShader( DrawState::SURFACE_SHADING );	
    glEnable( GL_CULL_FACE );

    if ( m_crvLeft.NumPts() && in_editAction != EDIT_LEFT_CURVE ) {
        if ( in_iSel == 0 ) glColor4f( colSel[0], colSel[1], colSel[2], 1.0f );
        else glColor4f( col[0], col[1], col[2], 1.0f );
        OGLDrawSphere( R3Sphere( m_crvLeft(0.0), dDistSel ) );
        
        if ( in_iSel == 1 ) glColor4f( colSel[0], colSel[1], colSel[2], 1.0f );
        else glColor4f( col[0], col[1], col[2], 1.0f );
        OGLDrawSphere( R3Sphere( m_crvLeft(1.0), dDistSel ) );
    }
    
    if ( m_crvRight.NumPts() && in_editAction != EDIT_RIGHT_CURVE ) {
        if ( in_iSel == 2 ) glColor4f( colSel[0], colSel[1], colSel[2], 1.0f );
        else glColor4f( col[0], col[1], col[2], 1.0f );
        OGLDrawSphere( R3Sphere( m_crvRight(0.0), dDistSel ) );
        
        if ( in_iSel == 3 ) glColor4f( colSel[0], colSel[1], colSel[2], 1.0f );
        else glColor4f( col[0], col[1], col[2], 1.0f );
        OGLDrawSphere( R3Sphere( m_crvRight(1.0), dDistSel ) );
    }       
    
    g_drawState.EndShader();
}
