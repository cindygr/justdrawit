/*
 *  Webbing.h
 *  SketchCurves
 *
 *  Created by Cindy Grimm on 6/27/12.
 *  Copyright 2012 Washington University in St. Louis. All rights reserved.
 *
 */

#ifndef WEBBING_DEFS_H_
#define WEBBING_DEFS_H_

#include "SurfaceData.h"

class Webbing : private SurfaceData
{
private:
    static const CurveNetwork *s_aopCrvs;
    static double m_dFalloff, m_dSpacing;
    static double SrfFunc( const R3Pt &in_pt );
    
public:
    void BuildSurface( const CurveNetwork &in_crv );
    void Draw() const { SurfaceData::DrawSurface(); }
    
    Webbing() {}
    ~Webbing() {}
};

#endif