/*
 *  CurveNetwork_Edit.cpp
 *  SketchCurves
 *
 *  Created by Cindy Grimm on 5/19/11 (copied from CurveNetwork.cpp).
 *  Copyright 2011 Adobe. All rights reserved.
 *
 */


#include "CurveNetwork.h"
#include <OpenGL/OGLObjs_Camera.H>
#include "DrawState.h"
#include "CurveGroupManager.h"
#include "PointConstraint.h"

const bool s_bTrace = false;
const bool s_bTraceUndo = false;

CurveGroup *
CurveNetwork::FindOrResurrectCurve( const int in_iCurveHashId )
{
    CurveGroup *opCurve = NULL;
    for ( int iP = 0; iP < m_aopCrv.num(); iP++ ) {
        if ( m_aopCrv[iP]->HashId() == in_iCurveHashId ) {
            opCurve = m_aopCrv[iP];
            return opCurve;
        }
    }
    
    for ( int iP = 0; iP < m_aopCrvGraveyard.num(); iP++ ) {
        if ( m_aopCrvGraveyard[iP]->HashId() == in_iCurveHashId ) {
            opCurve = m_aopCrvGraveyard[iP];
            m_aopCrv += opCurve;
            m_aopCrvGraveyard.del(iP,1);
            break;
        }
    }
    
    return opCurve;
}


// Create a new group.
void CurveNetwork::NewCurve( const ScreenCurve &in_crv ) 
{
    if ( in_crv.NumPts() < 2 ) {
        cerr << "ERR: CurveNetwork::NewCurve, curve with no points, not creating\n";
        return;
    }

    m_aopCrv += g_CrvGrpManager.CreateCurveGroup();
    m_aopCrv.last()->NewCurve( in_crv );
	
	m_iCurveLastStroke = NumCrvs() - 1;
	m_iSaveLastEdited = m_iLastEdited;
	m_iLastEdited = NumCrvs() - 1;
}

// For reading curves from somewhere else
void CurveNetwork::NewCompositeCurve( const Curve &in_crv ) 
{
    if ( in_crv.NumPts() < 2 ) {
        cerr << "ERR: CurveNetwork::NewCompositeCurve, curve with no points, not creating\n";
        return;
    }
    
	ScreenCurve crv;
	for ( int i = 0; i < in_crv.NumPts(); i++ ) {
		crv.AddPoint( g_drawState.GetCamera().CameraPt( in_crv.Pt(i) ), i );
	}
	
    NewCompositeCurve( crv, in_crv );
}


// Create a new group.
void CurveNetwork::NewClosedCurve( const CurveOverlap &in_overlap, const ScreenCurve &in_crv )
{
    if ( in_crv.NumPts() < 3 ) {
        cerr << "ERR: CurveNetwork::NewClosedCurve, curve with no points, not creating\n";
        return;
    }

    m_aopCrv += g_CrvGrpManager.CreateCurveGroup();
    m_aopCrv.last()->NewClosedCurve( in_overlap, in_crv );
	
	m_iCurveLastStroke = NumCrvs() - 1;
	m_iSaveLastEdited = m_iLastEdited;
	m_iLastEdited = NumCrvs() - 1;
}

// Create a new group. For reading curves in from somewhere else
void CurveNetwork::NewCompositeCurve( const ScreenCurve &in_crvScreen, const Curve &in_crv ) 
{
    if ( in_crv.NumPts() < 3 ) {
        cerr << "ERR: CurveNetwork::NewClosedCurve, curve with no points, not creating\n";
        return;
    }
    
    m_aopCrv += g_CrvGrpManager.CreateCurveGroup();
    m_aopCrv.last()->NewCompositeCurve( in_crvScreen, in_crv );
	
	m_iCurveLastStroke = NumCrvs() - 1;
	m_iSaveLastEdited = m_iLastEdited;
	m_iLastEdited = NumCrvs() - 1;
}


/*
 * Input is two curves to combine. Choose the one with the lower index to be the final
 * curve; the other curve is deleted.
 *
 * Take all of the pins on the deleted curve and copy them to the new curve. These are the pins that are stored 
 * in the stroke history
 */
void CurveNetwork::CombineCurves( const ScreenCurve &in_crv2D, const int in_iE1, const int in_iE2 )
{
	const int iE1 = WINmin( in_iE1, in_iE2 );
	const int iE2 = WINmax( in_iE1, in_iE2 );
	
    CurveGroup &crvGrpKeep = SetCurveGroup( iE1 );
    CurveGroup &crvGrpRemove = SetCurveGroup( iE2 );

    if ( s_bTraceCombine ) {
        cout << "COMBINE start " << CurrentTime() << "\n";
        
        crvGrpKeep.PrintHistory( true );
        crvGrpRemove.PrintHistory( true );
    }

	crvGrpKeep.Combine( s_acrvOverlap[ iE1 ], crvGrpRemove, s_acrvOverlap[ iE2 ], in_crv2D );

    for ( int i = 0; i < crvGrpRemove.NumConstraints(); i++ ) {
        PointConstraint *opPinAdd = crvGrpRemove.SetConstraint(i);
        
        const int iPinOld = opPinAdd->GetPtIndex( crvGrpRemove );
        const double dT = crvGrpKeep.CompositeCurve().ClosestPointTValue( crvGrpRemove.CompositeCurve().Pt( iPinOld ) );
        
        opPinAdd->RemoveCurveGroup( &crvGrpRemove );
        if ( opPinAdd->HasCurveGroup( crvGrpKeep ) ) {
            cerr << "Warning: CurveNetwork::CombineCurves trying to pin curve to itself, removing " << opPinAdd->HashId() << "\n";
        } else {
            crvGrpKeep.AddConstraint( opPinAdd, dT, false );
        }
    }
    
    DeleteCurve( iE2 );

	m_iCurveLastStroke = -1;
	m_iSaveLastEdited = m_iLastEdited;
	m_iLastEdited = iE1;
	
    PropagateEdits();
    
    if ( s_bTraceCombine ) {
        cout << "COMBINE end\n";
        
        crvGrpKeep.PrintHistory( true );
        crvGrpRemove.PrintHistory( true );
    }
}

/* Erase part of the specified curve.
 * Throws out any left-over bits that are too small
 * If anything left, store it in the current index
 * May add a new curve group
 */
void CurveNetwork::Erase( const int in_i, const double in_dTStart, const double in_dTEnd )
{
	if ( in_i < 0 || in_i >= NumCrvs() ) {
        cerr << "ERR: CurveNetwork::Erase invalid curve " << in_i << " " << NumCrvs() << "\n";
		return;
	}
	if ( s_bTraceErase ) {
        cout << "\nERASE start: Curve before: " << in_dTStart << " " << in_dTEnd << "\n";
        GetCurveGroup(in_i).PrintHistory( true );
    }
	m_iSaveLastEdited = m_iLastEdited;
	m_iCurveLastStroke = m_iLastEdited = -1;
	
	if ( RNApproxEqual( in_dTStart, 0.0 ) && RNApproxEqual( in_dTEnd, 1.0 ) ) {
        if ( s_bTraceErase) cout << "Deleting\n";
		DeleteCurve( in_i );
	} else {
        
		const double dSelDist = g_drawState.SelectionSize();
		
        CurveGroup *opCrvGroupLeft = SetCurveGroup( in_i ).Erase( in_dTStart, in_dTEnd );
		const bool bKeepLeft = ( opCrvGroupLeft->CompositeCurve().Length() < dSelDist ) ? false : true;
		const bool bKeepRight = GetCurve( in_i ).Length() < dSelDist ? false : true;
        
        if (s_bTraceErase ) {
            cout << "New curve\n";
            opCrvGroupLeft->PrintHistory( true );
        }
        
		if ( bKeepLeft && bKeepRight ) {
			m_aopCrv += opCrvGroupLeft;
			
			m_iLastEdited = in_i;

		} else if ( bKeepLeft && !bKeepRight ) {
            // This shouldn't really happen anymore because we're keeping the longer piece
            // in this curve - keep this if statement just in case
            // Delete right and add left in
            DeleteCurve( in_i );
            m_aopCrv += opCrvGroupLeft;
            m_iLastEdited = m_aopCrv.num();
            cerr << "Warning: CurveNetwork::Erase, keeping new piece and deleting old\n";
			
		} else if ( !bKeepLeft && bKeepRight ) {
            opCrvGroupLeft->RemoveCurveFromPointConstraints();
            // Safe to delete here because nothing put in it
            g_CrvGrpManager.DeleteCurveGroup( opCrvGroupLeft );
            opCrvGroupLeft = NULL;
            
		} else if ( !bKeepLeft && !bKeepRight ) {
            opCrvGroupLeft->RemoveCurveFromPointConstraints();
            // Safe to delete here because nothing put in it, but put current curve into graveyard
            // Delete left and right
            g_CrvGrpManager.DeleteCurveGroup( opCrvGroupLeft );
            opCrvGroupLeft = NULL;

			DeleteCurve(in_i);
		}
        if ( s_bTraceErase ) {
            if ( bKeepRight ) {
                cout << "Keeping original\n";
                GetCurveGroup(in_i).PrintHistory( true );
            }
            if ( bKeepLeft ) {
                cout << "Keeping new\n";
                opCrvGroupLeft->PrintHistory( true );
            }
        }
    }
    
    // In case any pin constraints with only one curve
    RemoveEmptyConstraints();
    
    PropagateEdits();
    
    if ( s_bTraceErase ) {
        cout << "ERASE done\n\n";
    }
}

/* Put the deleted curve in the graveyard, after removing any constraints. Then remove
 * any constraints that only belonged to this curve
 */
void CurveNetwork::DeleteCurve( const int in_i )
{	
	m_iLastEdited = m_iSaveLastEdited;
	m_iCurveLastStroke = m_iSaveLastEdited = -1;
	
    CurveGroup *opCrvKill = m_aopCrv[in_i];
    m_aopCrv[ in_i ] = m_aopCrv.last();
    m_aopCrv.sub(1);
    
    // remove constraints
    opCrvKill->RemoveCurveFromPointConstraints();
    
    m_aopCrvGraveyard += opCrvKill;

    // clean up
    RemoveEmptyConstraints();
}

/* This sets the starting curve in the stroke history */
void CurveNetwork::StartTransform( const int in_i )
{
	if ( in_i < 0 || in_i >= NumCrvs() ) {
        cerr << "ERR: CurveNetwork::StartTransform, invalid curve " << in_i << " " << NumCrvs() << "\n";
		return;
	}

	if ( in_i != m_iLastEdited ) {
		m_iCurveLastStroke = m_iSaveLastEdited = -1;
	}
	m_iLastEdited = in_i;	
	
    if ( s_bTraceDrag ) cout << "DRAG start " << in_i << "\n";

	SetCurveGroup(in_i).StartTransform();
}


void CurveNetwork::Transform( const int in_i, const R4Matrix &in_mat, const bool in_bReset )
{
	if ( in_i < 0 || in_i >= NumCrvs() ) {
        cerr << "ERR: CurveNetwork::Transform, invalid curve " << in_i << " " << NumCrvs() << "\n";
		return;
	}
    
	if ( in_i != m_iLastEdited ) {
		m_iCurveLastStroke = m_iSaveLastEdited = -1;
	}
	m_iLastEdited = in_i;
	
    if ( s_bTraceDrag ) cout << "Dragging " << in_i << "\n";
	SetCurveGroup(in_i).Transform( in_mat, in_bReset );

    PropagateEdits();
}


void CurveNetwork::Drag( const int in_i, const R3Plane &in_plane, const R3Vec &in_vec, const double in_dTDrag, const std::pair<double,double> &in_dTBracket )
{
	if ( in_i < 0 || in_i >= NumCrvs() ) {
        cerr << "ERR: CurveNetwork::Drag, invalid curve " << in_i << " " << NumCrvs() << "\n";
		return;
	}

	if ( in_i != m_iLastEdited ) {
		m_iCurveLastStroke = m_iSaveLastEdited = -1;
	}
	m_iLastEdited = in_i;
	
    if ( s_bTraceDrag ) cout << "Partial drag " << in_dTBracket.first << " " << in_dTDrag << " " << in_dTBracket.second << "\n";

	SetCurveGroup(in_i).Drag( in_plane, in_vec, in_dTDrag, in_dTBracket );

    PropagateEdits();
}

void CurveNetwork::CloseCurve( const int in_i ) 
{
	if ( in_i < 0 || in_i >= NumCrvs() ) {
        cerr << "ERR: CurveNetwork::CloseCurve, invalid curve " << in_i << " " << NumCrvs() << "\n";
		return;
	}

	m_iSaveLastEdited = m_iLastEdited;
	m_iLastEdited = in_i;
	
	m_iCurveLastStroke = -1;

    // TODO: Invoke a drag and a pin point to close the curve
}


void CurveNetwork::Smooth( const int in_i, const double in_dTStart, const double in_dTEnd )
{
	if ( in_i < 0 || in_i >= NumCrvs() ) {
        cerr << "ERR: CurveNetwork::Smooth, invalid curve " << in_i << " " << NumCrvs() << "\n";
		return;
	}
	
	m_iSaveLastEdited = m_iLastEdited;
	m_iLastEdited = in_i;
	m_iCurveLastStroke = -1;
	
    SetCurveGroup(in_i).Smooth( in_dTStart, in_dTEnd, 10 );
    
    PropagateEdits();    
}

void CurveNetwork::RepeatLastSmooth( )
{
	if ( m_iLastEdited < 0 || m_iLastEdited >= NumCrvs() ) {
        cerr << "ERR: CurveNetwork::RepeatLastSmooth, invalid curve " << m_iLastEdited << " " << NumCrvs() << "\n";
		return;
	}
	if ( s_bTrace ) cout << "Smoothing\n";
	
    SetCurveGroup(m_iLastEdited).RepeatSmooth();
    
    PropagateEdits();
}

/* Resurrect the curve and put the constraints back */
void CurveNetwork::RestoreCombine( const StrokeHistory &in_sh, CurveGroup &io_crvGroupOrig )
{
	if ( s_bTraceUndo ) cout << "Undo: combine\n";
	
	int iRemoved = -1;
	for ( int i = 0; i < m_aopCrvGraveyard.num(); i++ ) {
		if ( m_aopCrvGraveyard[i]->HashId() == in_sh.CurveHashIdOrig() ) {
			iRemoved = i;
			break;
		}
	}
	if ( iRemoved == -1 ) {
		cerr << "ERR: Undoing combine, curve " << in_sh.CurveHashIdOrig() << " not in graveyard\n";
	} else {
		if ( s_bTraceUndo ) {
			cout << "Recovering\n";
			m_aopCrvGraveyard[ iRemoved ]->PrintHistory( true );
		}
		m_aopCrv += m_aopCrvGraveyard[ iRemoved ];
		m_aopCrvGraveyard.del( iRemoved, 1 );
		
		// Copy all pin constraints back
		RestoreConstraintsCombine( io_crvGroupOrig, *m_aopCrv.last(), in_sh.PinHashId(), in_sh.GetTimeStamp() );
		
		if ( s_bTraceUndo ) {
			cout << "Restored:\n";
			m_aopCrv.last()->PrintHistory( true );
		}
	}
	io_crvGroupOrig.FinishedUndo();
}

/* Put constraints back where they were */
void CurveNetwork::RestoreStroke( const StrokeHistory &in_sh, CurveGroup &io_crvGroupOrig )
{
	if ( s_bTraceUndo ) cout << "Undo: changed curve\n";
	for ( int i = 0; i < io_crvGroupOrig.NumConstraints(); i++ ) {
		PointConstraint *opPin = io_crvGroupOrig.SetConstraint(i);
		if ( !opPin->HadCurveGroupPast( io_crvGroupOrig, in_sh.GetTimeStamp() ) ) {
			cerr << "ERR: CurveNetwork::Undo, curve did not have constraint " << opPin->HashId() << "\n";
		} else {
			// Fix t value
			const double dT = opPin->GetTValuePast( io_crvGroupOrig, in_sh.GetTimeStamp() );
			opPin->AddCurveGroup( &io_crvGroupOrig, io_crvGroupOrig.CompositeCurve().ClosestIndex(dT), dT );
		}
	}
	
	// Update all point constraints to new positions
	io_crvGroupOrig.FinishedUndo();
}

/* Some of this may be redundent. Basically, make sure all the curves
 * are moved to their new constraints and that the
 * changed curves are updated
 */
void CurveNetwork::UndoCleanup( const StrokeHistory &in_sh )
{
    for ( int i = 0; i < m_aopPins.num(); i++ ) {
        m_aopPins[i]->ReEstablish();
    }
    
    // Anything that was attached to a curve constraint, fix
    ReEstablishConstraints( in_sh.PinHashId() );
    
    PropagateEdits();
}

/*
 * For the most part, just pop the last action off of the stack and reset the curve
 *
 * Recovering from an erase
 *  What can happen on a partial erase (a full erase is a delete): 
 *    One piece left: that piece is stored in the curve that's left
 *    Two pieces left: the longer piece is stored in the curve that's left, the new piece is
 *      stored in a new curve group
 *    The pins that were kept on the original curve are stored in stroke history
 *  In the case of just one piece left, replace any removed pins and rollback the curve
 *  In the case of two pieces left, it's possible that the other half has been substantially
 *    edited. In this case, rollback just the piece that has been edited and put the constraints back in
 *  If both pieces are still much the same as the original curve, rollback the other half as well. Only keep
 *    the original half (the one that wasn't created in the erase) since that has the full history.
 *
 */
void CurveNetwork::UndoLastAction( const int in_i )
{
	m_iCurveLastStroke = -1;
	m_iSaveLastEdited = m_iLastEdited;
	m_iLastEdited = in_i;
	
    CurveGroup &crvGrp = SetCurveGroup(in_i);

    if ( s_bTraceUndo ) {
        cout << "UNDO: Start ";
        crvGrp.PrintHistory( true );
    }
    
    const StrokeHistory sh = crvGrp.UndoLastAction(  );
            
    if ( s_bTraceUndo ) {
        cout << "action " << StrokeHistory::GetMergeTypeString( sh.GetMergeType() );
        cout << " time " << sh.GetTimeStamp() << "\n";
    }
    
	if ( sh.GetMergeType() == StrokeHistory::ERASE ) {
        if ( s_bTraceUndo ) cout << "Undo: erasing\n";
        
        CurveGroup *opCrvGrpOrig = FindOrResurrectCurve( sh.CurveHashIdOrig() );
        CurveGroup *opCrvGrpRemove = NULL;
        int iRemove = -1;
        for ( int i = 0; i < NumCrvs(); i++ ) {
            if ( m_aopCrv[i]->HashId() == sh.CurveHashIdNew() ) {
                opCrvGrpRemove = m_aopCrv[i];
                iRemove = i;
                break;
            }
        }
        
        if ( opCrvGrpOrig == NULL ) {
            cerr << "ERR: CurveNetwork::UndoLastAction, couldn't find curve to resurrect " << sh.CurveHashIdOrig() << "\n";
            
        } else if ( opCrvGrpRemove == NULL ) {
            // Only one piece was left after erase. Put constraints back
            if ( *opCrvGrpOrig != crvGrp ) {
                cerr << "ERR: CurveNetwork::UndoLastAction, erase should have crv grps same\n";
            }
            
        } else {
            if ( s_bTraceUndo ) {
                cout << "Erase, orig curve\n";
                opCrvGrpOrig->PrintHistory(true);
                if ( opCrvGrpRemove ) {
                    cout << "Erase, replacement piece curve\n";
                    opCrvGrpRemove->PrintHistory(true);
                }
            }

            // See if the other piece is still substantially the same as the original curve
            if ( *opCrvGrpOrig != crvGrp ) {
                // User clicked on new part of the curve
                if ( opCrvGrpOrig->LastAction().GetTimeStamp() == sh.GetTimeStamp() ) {
                    // restore crv grp original, delete new bit
                    opCrvGrpOrig->UndoLastAction();
                    DeleteCurve( iRemove );
                }
            } else {
                // User clicked on original part of the curve
                if ( opCrvGrpRemove->LastAction().GetTimeStamp() == sh.GetTimeStamp() ) {
                    // just delete new bit - already rolled back
                    DeleteCurve( iRemove );
                }
            }
        }
        RestoreConstraintsInEraseRegion( sh.PinHashId(), sh.GetTimeStamp() );
        opCrvGrpOrig->FinishedUndo();
        if ( opCrvGrpRemove ) {
            opCrvGrpRemove->FinishedUndo();
        }

        if ( s_bTraceUndo ) {
            cout << "Erase, orig curve\n";
            opCrvGrpOrig->PrintHistory(true);
            if ( opCrvGrpRemove ) {
                cout << "Erase, replacement piece curve\n";
                opCrvGrpRemove->PrintHistory(true);
            }
        }
    } else if ( sh.GetMergeType() == StrokeHistory::COMBINE ) {
		RestoreCombine( sh, crvGrp );
        
    } else if ( sh.GetMergeType() == StrokeHistory::ADDED_CONSTRAINT ) {
        if ( s_bTraceUndo ) cout << "Undo: Added constraint\n";
        
        if ( sh.PinHashId().num() != 1 ) {
            cerr << "ERR: CurveNetwork::UndoLastAction add, no stored pin id\n";
        } else {
            RemoveConstraintFromCurve( crvGrp, sh.PinHashId()[0] );
        }
    } else if ( sh.GetMergeType() == StrokeHistory::REMOVED_CONSTRAINT ) {
        if ( s_bTraceUndo ) cout << "Undo: Removed constraint\n";
        
        if ( sh.PinHashId().num() != 1 ) {
            cerr << "ERR: CurveNetwork::UndoLastAction remove, no stored pin id\n";
        } else {
            AddConstraintToCurve( crvGrp, sh.PinHashId()[0], sh.GetTimeStamp() );
        }
    } else if ( sh.GetMergeType() == StrokeHistory::CHANGED_CONSTRAINT ) {
        if ( s_bTraceUndo ) cout << "Undo: Changed constraint\n";
        
        if ( sh.PinHashId().num() != 1 ) {
            cerr << "ERR: CurveNetwork::UndoLastAction changed, no stored pin id\n";
        } else {
            RollbackConstraintAction( sh.PinHashId()[0], sh.GetTimeStamp() );
        }

    } else if ( sh.GetMergeType() == StrokeHistory::NO_ACTION || sh.GetMergeType() == StrokeHistory::FIRST_STROKE || sh.GetMergeType() == StrokeHistory::FIRST_STROKE_CLOSED ) {
        if ( s_bTraceUndo ) cout << "Undo: deleting empty curve\n";
        
		DeleteCurve( in_i );
	} else {
        if ( s_bTraceUndo ) cout << "Undo: changed curve\n";
        for ( int i = 0; i < crvGrp.NumConstraints(); i++ ) {
            PointConstraint *opPin = crvGrp.SetConstraint(i);
            if ( !opPin->HadCurveGroupPast( crvGrp, sh.GetTimeStamp() ) ) {
                cerr << "ERR: CurveNetwork::Undo, curve did not have constraint " << opPin->HashId() << "\n";
            } else {
                // Fix t value
                const double dT = opPin->GetTValuePast( crvGrp, sh.GetTimeStamp() );
                opPin->AddCurveGroup( &crvGrp, crvGrp.CompositeCurve().ClosestIndex(dT), dT );
            }
        }
        
        // Update all point constraints to new positions
        crvGrp.FinishedUndo();
    }
    
	UndoCleanup( sh );
	
    if ( s_bTraceUndo ) {
        cout << "Result\n";
        crvGrp.PrintHistory( true );
    }
    
}


void CurveNetwork::Clear()
{
    while ( m_aopPins.num() ) {
        RemoveConstraint( m_aopPins[0] );
    }
    
    while ( m_aopCrv.num() ) {
        DeleteCurve( 0 );
    }
    
    for ( int i = 0; i < m_aopPinGraveyard.num(); i++ ) {
        delete m_aopPinGraveyard[i];
    }
    m_aopPinGraveyard.need(0);
    
    for ( int i = 0; i < m_aopCrvGraveyard.num(); i++ ) {
        g_CrvGrpManager.DeleteCurveGroup( m_aopCrvGraveyard[i] );
        m_aopCrvGraveyard[i] = NULL;
    }
    m_aopCrvGraveyard.need(0);

	m_iSaveLastEdited = m_iLastEdited = -1;
	m_iCurveLastStroke = -1;
	
}


