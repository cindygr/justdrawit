/*
 *  CurveRibbon.cpp
 *  SketchCurves
 *
 *  Created by Cindy Grimm on 9/20/11.
 *  Copyright 2011 Washington University in St. Louis. All rights reserved.
 *
 */

#include "CurveRibbon.h"

R3Vec CurveRibbon::Normal( const double in_dT ) const
{
    if ( m_avecNormal.num() == 0 ) return R3Vec(0,0,1);
    
    const std::pair<int,double> adT = BinarySearch( in_dT );
    return UnitSafe( m_avecNormal[adT.first] * (1.0 - adT.second), m_avecNormal.clamp(adT.first + 1) * adT.second );
}

std::pair<R3Pt, R3Matrix> CurveRibbon::Frame( const double in_dT ) const
{
    if ( NumPts() == 0 ) return std::pair<R3Pt, R3Matrix>( R3Pt(0,0,0), R3Matrix::Identity() );
    
    const std::pair<int,double> adT = BinarySearch( in_dT );
    const int iNext = WINmin( adT.first, NumPts() - 1 );
    const R3Pt pt = Lerp( Pt( adT.first ), Pt( iNext ), adT.second );
    const R3Vec vecTang = UnitSafe( GetTangentAtPoint( adT.first ) * (1.0 - adT.second) + GetTangentAtPoint( iNext ) * adT.second );
    const R3Vec vecNorm = UnitSafe( m_avecNormal[adT.first] * (1.0 - adT.second) + m_avecNormal[iNext] * adT.second );    
    
    const R3Matrix mat( R3Matrix( vecTang, vecNorm, Cross(vecTang, vecNorm) ).Transpose() );
    return std::pair<R3Pt, R3Matrix>(pt, mat);
}

/* Average of normals */
R3Vec CurveRibbon::AverageNormal() const
{
    if ( NumPts() < 2 ) {
        return R3Vec(0,0,1);
    }
    
    R3Vec vecAvg(0,0,0);
    for ( int i = 0; i < m_avecNormal.num(); i++ ) {
        vecAvg += m_avecNormal[i];
    }
    
    return UnitSafe( vecAvg );
}

/*
 * Sorta sucks, could be better. Looks for the best look (normal) and up (bi-normal) view vectors
 */
std::pair<R3Vec,R3Vec> CurveRibbon::GetBestView( const std::pair<double,double> &in_dTs ) const
{
    
    const double dMid = 0.5 * ( in_dTs.first + in_dTs.second );
    
    return std::pair<R3Vec, R3Vec>( Normal( dMid ), Cross( Normal(dMid), Tangent(dMid) ) );
}


void CurveRibbon::Clear()
{
    Curve::Clear();
    m_avecNormal.need(0);
}

CurveRibbon &CurveRibbon::operator=( const CurveRibbon &in_crv )
{
    Curve::operator=( in_crv );
    m_avecNormal = in_crv.m_avecNormal;
    
    return *this;
}

/*
void CurveRibbon::SetToCurveNoNormals( const Curve &in_crv )
{
        
    Curve::operator=( in_crv );
    
    SetNormalsSimple();
}
*/

void CurveRibbon::SetToCurve( const Curve &in_crv )
{
    Curve::operator=( in_crv );

}

/*
 * All the other properties will be re-built upon read
 */
void CurveRibbon::Write( ofstream &out ) const
{
    Curve::Write(out);
    
    out << "Normals " << m_avecNormal.num() << "\n";
    for ( int i = 0; i < m_avecNormal.num(); i++ ) {
        out << m_avecNormal[i] << "\n";
    }
}

void CurveRibbon::WriteSimple( ofstream &out ) const
{
    out << NumPts() << "\n";
    for ( int i = 0; i < NumPts(); i++ ) {
        out << " " << i << "   " << PercAlong(i) << " " << Pt(i) << " " << m_avecNormal[i] << "\n";
    }
    out.flush();
}

void CurveRibbon::Read( ifstream &in )
{
    Curve::Read( in );
    
    std::string str;
    int iN;
    
    in >> str; ASSERT( !str.compare("Normals") );
    in >> iN;

    ASSERT( in.good() );
    m_avecNormal.need(iN);
    for ( int i = 0; i < iN; i++ ) {
        in >> m_avecNormal[i];
    }
    ASSERT( in.good() );
}

void CurveRibbon::ReadTangentNormal( ifstream &in )
{
    int iNSamples = 0;
    in >> iNSamples;
    
    Array<int> aiT(iNSamples);
    Array<R3Vec> avec(iNSamples);
    
    double dT;
    R3Vec vecT;
    for ( int i = 0; i < iNSamples; i++ ) {
        in >> dT >> vecT >> avec[i];
        aiT[i] = ClosestIndex(dT);
    }
    SetNormalData( avec, aiT );
    
    ASSERT( in.good() );
}
