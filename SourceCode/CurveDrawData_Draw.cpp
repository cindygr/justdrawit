/*
 *  CurveDrawData_Draw.cpp
 *  SketchCurves
 *
 *  Created by Cindy Grimm on 9/27/11.
 *  Copyright 2011 Washington University in St. Louis. All rights reserved.
 *
 */


#include <FL/GL.H>
#include "CurveDrawData.h"
#include "UserInterface.h"
#include <OpenGL/OGLDraw3D.H>

void CurveDrawData::DrawPoints( ) const
{
    glBegin( GL_POINTS );

    // These shouldn't matter, but set them anyways
    glNormal3dv( &(m_vecView)[0] );
    glTexCoord2f( 0.0f, 0.0f );
    
    for ( int i = 0; i < GetCurve().NumPts(); i++ ) {
        glColor4fv( &m_afCol[i][0] );
        glVertex3dv( &GetCurve().Pt(i)[0] );
    }
    
    glEnd();
}


void CurveDrawData::DrawLine( ) const
{
    glBegin( GL_LINE_STRIP );

    // These shouldn't matter, but set them anyways
    glNormal3dv( &(m_vecView)[0] );
    glTexCoord2f( 0.5f, 0.5f );
    
    for ( int i = 0; i < GetCurve().NumPts(); i++  ) {    
        glColor4fv( &m_afCol[i][0] );
        glVertex3dv( &GetCurve().Pt(i)[0] );
    }
    
	glEnd();
}

void CurveDrawData::DrawViewFacingStrip( ) const
{
	glEnable( GL_BLEND );
	glDepthFunc( GL_LEQUAL);
	glBlendFunc( GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA );
	
    glBegin( GL_QUAD_STRIP );
	
    glColor4f( 0.0, 0.0, 0.0, 1.0 );
    
    glNormal3dv( &m_vecView[0] );
    for ( int i = 0; i < m_aptTopStrip.size(); i++  ) {            
        glColor4fv( &m_afCol[i][0] );

        const double dS = GetCurve().PercAlong(i);
		glTexCoord2f( dS, 0.0 );    
		glVertex3dv( &(m_aptTopStrip[i])[0] );
		
		glTexCoord2f( dS, 1.0 );    
		glVertex3dv( &(m_aptBotStrip[i])[0] );
    }
	
	glEnd();
}


void CurveDrawData::DrawRibbon( ) const
{
    const GLfloat fTAcross = 1.0f / ( s_iRibbonDivs - 1.0f );
	for ( int j = 0; j < s_iRibbonDivs - 1; j++ ) {        
		glBegin( GL_QUAD_STRIP );
		
        const std::vector<R3Pt> &aptRibbonStripPrev = m_aaptRibbonStrip[j];
        const std::vector<R3Pt> &aptRibbonStripNext = m_aaptRibbonStrip[j+1];
        const std::vector<R3Vec> &avecRibbonStripPrev = m_aavecRibbonStripNorm[j];
        const std::vector<R3Vec> &avecRibbonStripNext = m_aavecRibbonStripNorm[j+1];
        
		for ( int i = 0; i < aptRibbonStripPrev.size(); i++  ) {    
            glColor4fv( &m_afCol[i][0] );
			
            glNormal3dv( &avecRibbonStripPrev[i][0] );
            glTexCoord2f( j * fTAcross, 1.0 );
            glVertex3dv( &aptRibbonStripPrev[i][0] );            
            
            glNormal3dv( &avecRibbonStripNext[i][0] );
            glTexCoord2f( (j+1) * fTAcross, 1.0 );    
            glVertex3dv( &(aptRibbonStripNext[i])[0] );
            
        }
        
		glEnd();
    }
}

void CurveDrawData::DrawTube( ) const
{
    const GLfloat fTAcross = 1.0f / (GLfloat) (s_iTubeDivs-1);
	for ( int j = 0; j < s_iTubeDivs; j++ ) {
		const int iNext = (j+1) % s_iTubeDivs;
        
		glBegin( GL_QUAD_STRIP );
		
        const std::vector<R3Pt> &aptTubeStripPrev = m_aaptTubeStrip[j];
        const std::vector<R3Pt> &aptTubeStripNext = m_aaptTubeStrip[iNext];
        const std::vector<R3Vec> &avecTubeStripPrev = m_aavecTubeStripNorm[j];
        const std::vector<R3Vec> &avecTubeStripNext = m_aavecTubeStripNorm[iNext];
        
		for ( int i = 0; i < aptTubeStripPrev.size(); i++  ) {    
            glColor4fv( &m_afCol[i][0] );
			
            glNormal3dv( &avecTubeStripPrev[i][0] );
            glTexCoord2f( j * fTAcross, 1.0 );
            glVertex3dv( &aptTubeStripPrev[i][0] );            
            
            glNormal3dv( &avecTubeStripNext[i][0] );
            glTexCoord2f( (j+1) * fTAcross, 1.0 );    
            glVertex3dv( &(aptTubeStripNext[i])[0] );
            
        }
        
		glEnd();
    }
}

void CurveDrawData::DrawWebbing( ) const
{
    const GLfloat fTAcross = 1.0f / (GLfloat) (s_iTubeDivs-1);
	for ( int j = 0; j < s_iTubeDivs; j++ ) {
		const int iNext = (j+1) % s_iTubeDivs;
        
		
        const std::vector<R3Pt> &aptTubeStripPrev = m_aaptWebbingStrip[j];
        const std::vector<R3Pt> &aptTubeStripNext = m_aaptWebbingStrip[iNext];
        const std::vector<R3Vec> &avecTubeStripPrev = m_aavecWebbingStripNorm[j];
        const std::vector<R3Vec> &avecTubeStripNext = m_aavecWebbingStripNorm[iNext];
        
        for ( int iSeg = 0; iSeg < m_aiWebbingDraw.size(); iSeg++ ) {
            glBegin( GL_QUAD_STRIP );
            for ( int i = m_aiWebbingDraw[iSeg].Start(); i < m_aiWebbingDraw[iSeg].End(); i++  ) {  
                const int iIndex = i % aptTubeStripNext.size();
                
                glColor4fv( &m_afCol[iIndex][0] );
                
                glNormal3dv( &avecTubeStripNext[iIndex][0] );
                glTexCoord2f( (j+1) * fTAcross, 1.0 );    
                glVertex3dv( &(aptTubeStripNext[iIndex])[0] );
                
                glNormal3dv( &avecTubeStripPrev[iIndex][0] );
                glTexCoord2f( j * fTAcross, 1.0 );
                glVertex3dv( &aptTubeStripPrev[iIndex][0] );            
                
            }
            glEnd();
        }       
        /* uncomment to draw entire tube
            glBegin( GL_QUAD_STRIP );
            for ( int i = 0; i < aptTubeStripNext.size(); i++  ) {  
                const int iIndex = i % aptTubeStripNext.size();
                
                glColor4fv( &m_afCol[iIndex][0] );
                
                glNormal3dv( &avecTubeStripPrev[iIndex][0] );
                glTexCoord2f( j * fTAcross, 1.0 );
                glVertex3dv( &aptTubeStripPrev[iIndex][0] );            
                
                glNormal3dv( &avecTubeStripNext[iIndex][0] );
                glTexCoord2f( (j+1) * fTAcross, 1.0 );    
                glVertex3dv( &(aptTubeStripNext[iIndex])[0] );
                
            }
            glEnd();
         */
    }
}

void CurveDrawData::DrawVectors( ) const
{
	const double dWidth = 4.0 * g_drawState.StripWidth();
    
	glBegin( GL_LINES );
	glTexCoord2f( 0.0f, 0.0f );    
    
	for ( int i = 0; i < GetCurve().NumPts(); i++ ) {
        glColor4fv( &m_afCol[i][0] );
        
		glVertex3dv( &GetCurve().Pt(i)[0] );
		glVertex3dv( &( GetCurve().Pt(i) + GetCurve().GetNormalAtPoint(i) * dWidth * 0.5 )[0] );
        
		glColor4f(m_afCol[i][0] * 0.5, m_afCol[i][1] * 0.5, m_afCol[i][2] * 0.5, 1.0f );
		glVertex3dv( &GetCurve().Pt(i)[0] );
		glVertex3dv( &( GetCurve().Pt(i) + GetCurve().GetTangentAtPoint(i) * dWidth * 0.5 )[0] );
	}
	glEnd();
}

void CurveDrawData::Draw() const
{
    switch( g_drawState.m_drawingStyle ) {
        case DrawState::POINTS_OR_LINES :
            g_drawState.StartShader( DrawState::LINE_SHADING );
            if ( g_drawState.m_opUI->m_bShowPoints->value() ) {
                DrawPoints( );
            }
            if ( g_drawState.m_opUI->m_bShowLines->value() ) {
                DrawLine( );
            }
            g_drawState.EndShader();
            break;
        case DrawState::SCREEN_STROKES :
            g_drawState.StartShader( DrawState::LINE_STROKE_SHADING );
            DrawViewFacingStrip( );
            g_drawState.EndShader();
            break;
        case DrawState::TUBES :
            g_drawState.StartShader( DrawState::LINE_SHADING );
            DrawTube( );
            g_drawState.EndShader();
            break;
        case DrawState::RIBBONS :
            g_drawState.StartShader( DrawState::LINE_SHADING );
            DrawRibbon();
            g_drawState.EndShader();
            break;
        case DrawState::WEBBING :
            g_drawState.StartShader( DrawState::LINE_SHADING );
            DrawWebbing();
            g_drawState.EndShader();
            break;
    }

	if ( g_drawState.m_opUI->m_bShowVectors->value() ) {
        g_drawState.StartShader( DrawState::LINE_SHADING );
		DrawVectors(  );
        g_drawState.EndShader();
	}
}

void CurveDrawData::DrawShadow( ) const
{
	if ( ! g_drawState.m_opUI->m_bShowShadows->value() ) {
        return;
    }
    
    g_drawState.StartShader( DrawState::SHADOW_SHADING );
    
    glDepthFunc( GL_LEQUAL );
    for ( int iDim = 0; iDim < 3; iDim++ ) {
        if ( iDim == 2 && !g_drawState.m_opUI->m_bShowFloorPlane->value() ) {
            break;
        }
        if ( iDim < 2 && !g_drawState.m_opUI->m_bShowShadowBox->value() ) {
            continue;
        }
        
        const std::vector< R3Pt > &aptTop = m_aaptTopShadow[iDim];
        const std::vector< R3Pt > &aptBot = m_aaptBotShadow[iDim];
        const std::vector< R3Pt > &aptMid = m_aaptMidShadow[iDim];
        
        glBegin( GL_QUAD_STRIP );
        
        for ( int i = 0; i < aptTop.size(); i++  ) {    
            glColor4f( 0.0, 0.0, 0.2, 1.0 );
            glTexCoord2f( 1.0f, 0.0f );    
            glVertex3dv( &(aptTop[i])[0] );
            
            glTexCoord2f( 0.0f, 0.0f );    
            glVertex3dv( &(aptMid[i])[0] );
        }
        
        glEnd();
        
        glBegin( GL_QUAD_STRIP );
        
        for ( int i = 0; i < aptTop.size(); i++  ) {    
            glTexCoord2f( 0.0f, 0.0f );    
            glVertex3dv( &(aptMid[i])[0] );
            
            glTexCoord2f( 1.0f, 0.0f );    
            glVertex3dv( &(aptBot[i])[0] );
        }
        
        glEnd();
	}
    
    g_drawState.EndShader();
}

void CurveDrawData::TubeGeometry( std::vector<R3Pt> &io_aptVs, std::vector<R3Vec> &io_avecVNs, std::vector< R3Pt_i > &io_aptFs ) const
{
    int iVStart = io_aptVs.size();
    Array< Array<int> > aaiIndex( m_aaptTubeStrip.size() );
    
    // Add points to list
	for ( int j = 0; j < m_aaptTubeStrip.size(); j++ ) {
        aaiIndex[j].need( m_aaptTubeStrip[j].size() );
        for ( int i = 0; i < m_aaptTubeStrip[j].size(); i++  ) {  
            io_aptVs.push_back( m_aaptTubeStrip[j][i] );
            io_avecVNs.push_back( m_aavecTubeStripNorm[j][i] );
            aaiIndex[j][i] = iVStart;
            iVStart++;
        }
    }
    
    // Add faces
    for ( int j = 0; j < m_aaptTubeStrip.size(); j++ ) {
        const int iNext = (j+1) % s_iTubeDivs;
        const Array<int> &aiTubeStripPrev = aaiIndex[j];
        const Array<int> &aiTubeStripNext = aaiIndex[iNext];
        for ( int i = 0; i < aiTubeStripPrev.num() - 1; i++  ) {    
            io_aptFs.push_back( R3Pt_i( aiTubeStripPrev[i], aiTubeStripPrev[i+1], aiTubeStripNext[i+1] ) );
            io_aptFs.push_back( R3Pt_i( aiTubeStripPrev[i], aiTubeStripNext[i+1], aiTubeStripNext[i] ) );
        }
    }
}

/* TODO: Detect corners and make vecup be zero there */
void CurveDrawData::DrawAsFragment(  const Curve &in_crv  ) 
{
	if ( g_drawState.m_opUI->m_bShowLines->value() ) {
        g_drawState.StartShader( DrawState::LINE_SHADING );
    } else {
        g_drawState.StartShader( DrawState::FRAGMENT_SHADING );
    }
	
	glEnable( GL_BLEND );
	glDepthFunc( GL_LEQUAL);
	glBlendFunc( GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA );
	
    glBegin( GL_QUAD_STRIP );
	
    glColor4fv( &s_colFragment[0] );
    
    const R3Vec vecNorm = g_drawState.GetCamera().Look();
    const R3Vec vecUpCamera = g_drawState.GetCamera().Up();
    const double dScl = ( g_drawState.m_opUI->m_bShowLines->value() ? 0.1 : 0.75 ) * g_drawState.StripWidth();
    const double dTooSmall = g_drawState.StripWidth() * 0.001;
    R3Vec vecUp;

    glNormal3dv( &vecNorm[0] );
    for ( int i = 0; i < in_crv.NumPts(); i++  ) {            

        vecUp = Cross( vecNorm, in_crv.GetTangentAtPoint(i) );
		const double dLenUp = ::Length( vecUp );
		
		if ( !RNIsZero( dLenUp, dTooSmall ) ) {
			vecUp = vecUp * (dScl / dLenUp);
		} else {
            vecUp = R3Vec(0,0,0);
        }
        //vecUp = vecUpCamera * dScl;

        const R3Pt ptUp = in_crv.Pt(i) + vecUp;
        const R3Pt ptDown = in_crv.Pt(i) - vecUp;

		glTexCoord2f( in_crv.PercAlong(i), 1.0f );    
		glVertex3dv( &(ptUp)[0] );
		
		glTexCoord2f( in_crv.PercAlong(i), 0.0f );    
		glVertex3dv( &(ptDown)[0] );
    }
	
	glEnd();
	
	g_drawState.EndShader();
}

void CurveDrawData::DrawAsBoundary( const Curve &in_crv ) 
{
    g_drawState.StartShader( DrawState::LINE_SHADING );
    
    //DrawTube( );
    
    g_drawState.EndShader();
}
