/*
 *  CurveGroupCreator.h
 *  SketchCurves
 *
 *  Created by Cindy Grimm on 9/11/11.
 *  Copyright 2011 Washington University in St. Louis. All rights reserved.
 *
 */

#ifndef _CURVE_GROUP_MANAGER_DEFS_H_
#define _CURVE_GROUP_MANAGER_DEFS_H_

#include "CurveGroup.h"

class CurveGroupManager {
private:
    static int s_iCurveIds;
    static Array<CurveGroup *> s_aopCrvGrps;
    
public:
    /**@name Creating/deleting curve groups */
    //@{
    ///
    CurveGroup *CreateCurveGroup();
    ///
    void DeleteCurveGroup( CurveGroup *in_opCG );
    ///
    const CurveGroup &InvalidCurveGroup() const;
    ///
    int NextId() const { return s_iCurveIds; }
    //@}
    
    CurveGroupManager();
    ~CurveGroupManager();
    
    /**@name for reading and writing */
    //@{
    /// Only valid after Write is called
    int WriteId( const CurveGroup *in_op );
    /// Only valid after Read is called
    CurveGroup *CurveFromReadId( const int in_iId );    
    
    /// Also sets up mapping from hash id to write id
    void Write( ofstream &out );
    ///
    void Read( ifstream &in );
    //@}
};

extern CurveGroupManager g_CrvGrpManager;

#endif

