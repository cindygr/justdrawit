/*
 *  CurveNetwork_Stroke.cpp
 *  SketchCurves
 *
 *  Created by Cindy Grimm on 5/6/11.
 *  Copyright 2011 Adobe Corp. All rights reserved.
 *
 */

#include "CurveNetwork.h"
#include "DrawState.h"
#include "UserInterface.h"
#include <OpenGL/OGLObjs_Camera.H>
#include "CurveGroupManager.h"

const bool s_bTrace = false;

/* These are temporary storage for determining which curve to apply the stroke to */
boost::dynamic_bitset<> CurveNetwork::s_abValid;
Array<CurveOverlap> CurveNetwork::s_acrvOverlap;


bool CurveNetwork::IsLastActionStroke(  ) const
{
	if ( m_iLastEdited < 0 || m_iLastEdited >= NumCrvs() ) {
		return false;
	}
	
	const CurveGroup &crvGrp = GetCurveGroup( m_iLastEdited );
	if ( crvGrp.NumStrokes() == 0 ) {
		return false;
	}
	
	if ( crvGrp.LastStrokeAction() > StrokeHistory::COMBINE ) {
		return false;
	}
	
	return true;
}

bool CurveNetwork::IsClickOnLastStroke( const R2Pt &in_ptScreen ) const
{
	if ( !IsLastActionStroke() ) {
		return false;
	}
	
	const CurveGroup &crvGrp = GetCurveGroup(m_iLastEdited);
	const R3Pt pt1 = crvGrp.StrokeStartPt();
	const R3Pt pt2 = crvGrp.StrokeEndPt();
	
	const R2Pt ptScreen1 = g_drawState.GetCamera().CameraPt( pt1 );
	const R2Pt ptScreen2 = g_drawState.GetCamera().CameraPt( pt2 );
	
	const double dDistSel = g_drawState.ScreenSelectionSize();
	if ( ::Length( ptScreen1 - in_ptScreen ) < dDistSel ) {
		return true;
	}
	if ( ::Length( ptScreen2 - in_ptScreen ) < dDistSel ) {
		return true;
	}
	return false;
}

bool CurveNetwork::IsClickOnLastSelectRegion( const R2Pt &in_ptScreen ) const
{
	if ( !IsLastActionSmooth() ) {
		return false;
	}
	
	const CurveGroup &crvGrp = GetCurveGroup( m_iLastEdited );
	const R3Pt pt1 = crvGrp.SelectRegionStartPt();
	const R3Pt pt2 = crvGrp.SelectRegionEndPt();
	
	const R2Pt ptScreen1 = g_drawState.GetCamera().CameraPt( pt1 );
	const R2Pt ptScreen2 = g_drawState.GetCamera().CameraPt( pt2 );
	
	const double dDistSel = g_drawState.ScreenSelectionSize();
	if ( ::Length( ptScreen1 - in_ptScreen ) < dDistSel ) {
		return true;
	}
	if ( ::Length( ptScreen2 - in_ptScreen ) < dDistSel ) {
		return true;
	}
	return false;
}

/* For setting text box in GUI */
std::string CurveNetwork::LastStrokeAction() const
{
	if ( m_iCurveLastStroke < 0 || m_iCurveLastStroke >= NumCrvs() ) {
		return std::string("None");
	}
	const StrokeHistory::MergeType mt = GetCurveGroup( m_iCurveLastStroke ).LastStrokeAction();
	return StrokeHistory::GetMergeTypeString( mt );
}



void CurveNetwork::DoAllCompares( const ScreenCurve &in_crv2D ) const
{
	s_acrvOverlap.need( NumCrvs() + 2 );
	s_abValid.resize( NumCrvs() + 2 );
	s_abValid.reset();
	
	for ( int i = 0; i < NumCrvs(); i++ ) {
		if ( IsHidden(i) == false ) {
            if ( s_bTraceProcessStroke ) cout << "Compare curve " << GetCurveGroup(i).HashId() << "\n";
			if ( s_acrvOverlap[i].TryMatch( GetCurve(i), in_crv2D, (i == m_iCurveLastStroke) ? false : true ) ) {
				if ( ! s_acrvOverlap[i].IsBehindDrawPlane( GetCurve(i) ) ) {
                    s_abValid.set(i);
                    if ( s_bTraceProcessStroke ) cout << "Valid\n";
                } else {
                    if ( s_bTraceProcessStroke ) cout << "Not valid, behind drawing plane\n";
                }
			} else {
                if ( s_bTraceProcessStroke ) cout << "Not Valid\n";
            }                
		}
	}
	
	if ( s_acrvOverlap[ IndexOverlapClosedStroke() ].TryClosedCurve( in_crv2D ) ) {
		s_abValid.set( IndexOverlapClosedStroke() );
	}
	
	if ( m_iCurveLastStroke != -1 ) {
		const CurveGroup &crvGrp = GetCurveGroup( m_iCurveLastStroke );
		CurveOverlap &crvOverlap = s_acrvOverlap[ IndexOverlapLastStroke() ];
		if ( !crvGrp.CompositeCurve().IsClosed() && crvGrp.NumStrokes() > 0 && IsHidden( m_iCurveLastStroke ) == false ) {
			if ( s_bTraceProcessStroke ) {
				cout << "Curve last stroke " << m_iCurveLastStroke << " ";		
			}
			 
			const Curve crv3D = crvGrp.LastStroke3D();
			if ( crvOverlap.TryMatch( crv3D, in_crv2D, false ) ) {
				switch ( crvOverlap.GetOverlapType() ) {
					case CurveOverlap::BLEND : // will be processed as an overstroke
						s_abValid.set( IndexOverlapLastStroke() );
						break;
						
					case CurveOverlap::MERGE :
						if ( crvOverlap.CurveEnd() == CurveOverlap::END_CURVE && !crvOverlap.ReverseStroke() ) {
							s_abValid.set( IndexOverlapLastStroke() );
						}
						break;
						
					case CurveOverlap::OVERSTROKE_START :
						if ( crvOverlap.StartOverstroke() > 0.1 && crvOverlap.MaxTCurve() > 0.6 ) {
							s_abValid.set( IndexOverlapLastStroke() );
						}
						break;
						
					case CurveOverlap::JOIN :
						s_abValid.set( IndexOverlapLastStroke() );
						break;
						
					default :
						break;
				}
				if ( s_bTraceProcessStroke ) {
                    if ( s_abValid[ IndexOverlapLastStroke() ] ) {
                        cout << "Valid\n";
                    } else {
                        cout << "Not Valid\n";
                    }
				}
			}
		}
	}
	if ( s_bTraceProcessStroke ) {
		cout << "\n\nSummary\n";
		for ( int i = 0; i < NumCrvs(); i++ ) {
			if ( s_abValid[i] ) {
				cout << "Curve " << GetCurveGroup(i).HashId() << " valid, action ";
				s_acrvOverlap[i].PrintState();
			}
		}
		if ( s_abValid[ IndexOverlapClosedStroke() ] ) {
			cout << "Closed curve valid, action ";
			s_acrvOverlap[ IndexOverlapClosedStroke() ].PrintState();
		}
		if ( s_abValid[ IndexOverlapLastStroke() ] ) {
			cout << "Last stroke valid, action ";
			s_acrvOverlap[ IndexOverlapLastStroke() ].PrintState();
		}
	}
}

int CurveNetwork::BestMerge( const CurveOverlap::CurveEndType in_endOfStroke ) const
{
	int iBest = -1;
	double dZMin = 1e30, dZMax = -1e30;
	bool bIsBehindBest = false;
	for ( int i = s_abValid.find_first(); i != -1; i = s_abValid.find_next(i) ) {
		dZMin = WINmin( dZMin, s_acrvOverlap[i].DepthMin() );
		dZMax = WINmax( dZMax, s_acrvOverlap[i].DepthMax() );
	}
	for ( int i = s_abValid.find_first(); i != -1; i = s_abValid.find_next(i) ) {
		if ( s_acrvOverlap[i].GetOverlapType() == CurveOverlap::MERGE && i < NumCrvs() ) {
			if ( ( s_acrvOverlap[i].StrokeEnd() == in_endOfStroke && s_acrvOverlap[i].ReverseStroke() == false ) ||
				( s_acrvOverlap[i].StrokeEnd() != in_endOfStroke && s_acrvOverlap[i].ReverseStroke() == true ) ) {
				
				const bool bIsBehindPlane = i < NumCrvs() ? s_acrvOverlap[i].IsBehindDrawPlane( GetCurve(i) ) : false;
				
				if ( iBest == -1 ) {
					iBest = i;
					bIsBehindBest = bIsBehindPlane;
				} else if ( bIsBehindBest == true && bIsBehindPlane == false ) {
					iBest = i;
					bIsBehindBest = bIsBehindPlane;
				} else if ( !( bIsBehindBest == false && bIsBehindPlane == true ) ) {
					if ( s_acrvOverlap[i].IsBetterMatch( s_acrvOverlap[ iBest ], dZMin, dZMax ) ) {
						iBest = i;
						bIsBehindBest = bIsBehindPlane;
					}
				}
			}
		}
	}
	return iBest;
}

int CurveNetwork::BestAction(  ) const
{
	cout.precision(6 );
	int iBest = s_abValid.find_first();
	bool bIsBehindBest = iBest == -1 || iBest >= NumCrvs() ? false : s_acrvOverlap[iBest].IsBehindDrawPlane( GetCurve(iBest) );
	double dZMin = 1e30, dZMax = -1e30;
	for ( int i = s_abValid.find_first(); i != -1; i = s_abValid.find_next(i) ) {
		dZMin = WINmin( dZMin, s_acrvOverlap[i].DepthMin() );
		dZMax = WINmax( dZMax, s_acrvOverlap[i].DepthMax() );
	}
	
	if ( s_bTrace ) cout << "Best " << iBest << ( bIsBehindBest ? " t " : " f " );
	for ( int i = s_abValid.find_next(iBest); i != -1; i = s_abValid.find_next(i) ) {
		const bool bIsBehindPlane = i < NumCrvs() ? s_acrvOverlap[i].IsBehindDrawPlane( GetCurve(i) ) : false;
		if ( s_bTrace ) cout << i << " " << iBest << ( bIsBehindPlane ? " t " : " f " );

		if ( bIsBehindBest == true && bIsBehindPlane == false ) {
			iBest = i;
			bIsBehindBest = bIsBehindPlane;
		} else if ( !( bIsBehindBest == false && bIsBehindPlane == true ) ) {
			if ( s_acrvOverlap[i].IsBetterMatch( s_acrvOverlap[ iBest ], dZMin, dZMax ) ) {
				iBest = i;
				bIsBehindBest = bIsBehindPlane;
				if ( s_bTrace ) cout << i << " " << iBest << ( bIsBehindPlane ? " t " : " f " );
			}
		}
	}
	if ( s_bTrace ) {
		cout << " of " << NumCrvs() << "\n";
		if ( iBest != -1 ) {
			s_acrvOverlap[iBest].PrintState();
		}
	}
	
	/// TODO: Probably should be an interface toggle.
	if ( bIsBehindBest ) {
		return -1;
	}
	return iBest;
}


StrokeHistory::MergeType CurveNetwork::IsBlend( const ScreenCurve &in_crv, ScreenCurve &out_crvScreen ) const
{
	static std::vector<ScreenCurve> s_acrv, s_acrvTemp;
	if ( g_drawState.m_opUI->m_bAllowBlends->value() ) {
		s_acrv = CurveOverlap::SplitAtFoldbacks( in_crv );
	} else {
		out_crvScreen = in_crv;
		return StrokeHistory::NO_ACTION;
	}
	
	const bool bBlend = g_drawState.m_opUI->m_bAllowBlends->value() && s_acrv.size() > 1;
	out_crvScreen = bBlend ? StrokeHistory::Blend( s_acrv, g_drawState.ScreenSelectionSize() ) : in_crv;
	
	if ( m_iCurveLastStroke != -1 ) {
		const CurveGroup &crvGrp = GetCurveGroup( m_iCurveLastStroke );
		CurveOverlap crvOverlap;
		if ( !crvGrp.CompositeCurve().IsClosed() && crvGrp.NumStrokes() > 0 && IsHidden( m_iCurveLastStroke ) == false ) {
			crvOverlap.TryMatch( crvGrp.LastStroke3D(), in_crv, false );
			
			if ( crvOverlap.GetOverlapType() == CurveOverlap::BLEND ) {
				s_acrvTemp = crvGrp.GetBlendStack();
				for ( int i = 0; i < s_acrvTemp.size(); i++ ) {
					s_acrv.push_back( s_acrvTemp[i] );
				}
				out_crvScreen = StrokeHistory::Blend( s_acrv, g_drawState.ScreenSelectionSize() );
				return StrokeHistory::BLEND_OVERSTROKE;
			}
		}
	}
	return bBlend ? StrokeHistory::BLEND_SCRATCH : StrokeHistory::NO_ACTION;			
}

/* Priority order:
 *   See if stroke joins current curve to another one. If so, merge two curves.
 *   See if stroke joins last stroke. If so, add to that curve.
 *   See if stroke joins two curves. If so, merge two curves.
 *   Try best action.
 */   
void CurveNetwork::ProcessCurve( const ScreenCurve &in_crvPreSplit2D )
{
	ScreenCurve crv2D = in_crvPreSplit2D;
	const StrokeHistory::MergeType bBlend = IsBlend( in_crvPreSplit2D, crv2D );

    if ( s_bTraceProcessStroke ) cout << "\nProcessCurve\n";
    
	// Replace the last stroke history action with an overstroke
	if ( bBlend == StrokeHistory::BLEND_OVERSTROKE ) {
        if ( s_bTraceProcessStroke ) cout << "Doing blend\n";

		SetCurveGroup( m_iCurveLastStroke ).AddBlendOverstroke( in_crvPreSplit2D );
	}
	
	m_iSaveLastEdited = m_iLastEdited;
	
	DoAllCompares( crv2D );
	const int iMergeE1 = BestMerge( CurveOverlap::START_CURVE );
	const int iMergeE2 = BestMerge( CurveOverlap::END_CURVE );
    int iBest = BestAction();
    
    /* When to add to last stroke: If can continue oversketch or blend with curve is valid */
    bool bAddToLastStroke = s_abValid[ IndexOverlapLastStroke() ] || bBlend == StrokeHistory::BLEND_OVERSTROKE;
    if ( bAddToLastStroke && m_iCurveLastStroke != -1 && s_abValid[ m_iCurveLastStroke ] ) {
        // Now check against adding to last curve versus adding to last stroke. We use
        // last stroke only if the last action was a partial stroke or blend
        const CurveGroup &crvGrp = GetCurveGroup( m_iCurveLastStroke );
        const StrokeHistory::MergeType shLastAction = crvGrp.LastStrokeAction();
		const CurveOverlap &crvOverlap = s_acrvOverlap[ IndexOverlapLastStroke() ];
        
        // Look at only valid cases
        bAddToLastStroke = false;
        iBest = m_iCurveLastStroke;
        if ( crvOverlap.GetOverlapType() == CurveOverlap::BLEND ) {
            bAddToLastStroke = true; // blend stroke with last stroke
        } else { // is a merge/overstroke/join
            if ( shLastAction == StrokeHistory::OVERSKETCH_START || shLastAction == StrokeHistory::OVERSKETCH ) {
                bAddToLastStroke = true; // blend stroke with last stroke
            }                
        }
    }
	
	if ( bBlend != StrokeHistory::BLEND_OVERSTROKE && s_abValid[ IndexOverlapLastStroke() ] && s_acrvOverlap[ IndexOverlapClosedStroke() ].GetOverlapType() != CurveOverlap::BLEND && iMergeE2 != -1 ) {
        if ( s_bTraceProcessStroke ) cout << "Doing combine with last stroke curve " << GetCurveGroup( m_iCurveLastStroke ).HashId() << " " << GetCurveGroup( iMergeE2 ).HashId() << "\n";

		// Use this stroke to blend two existing ones
		const CurveGroup &crvGrp = GetCurveGroup( m_iCurveLastStroke );
		
		s_acrvOverlap[IndexOverlapLastStroke()].MapTsToComposite( crvGrp.CompositeCurve(), crvGrp.LastStroke3D() );
		s_acrvOverlap[m_iCurveLastStroke] = s_acrvOverlap[IndexOverlapLastStroke()];
		
		CombineCurves( crv2D, m_iCurveLastStroke, iMergeE2 );
		
	} else if ( bAddToLastStroke ) {
		// Now do whatever the best action is
		if ( s_acrvOverlap[m_iCurveLastStroke].GetOverlapType() == CurveOverlap::CLOSES ) {
            if ( s_bTraceProcessStroke ) cout << "Closing active curve " << GetCurveGroup( m_iCurveLastStroke ).HashId()  << "\n";

			SetCurveGroup( m_iCurveLastStroke ).AddStroke( s_acrvOverlap[ m_iCurveLastStroke ], crv2D );
		} else {
            if ( s_bTraceProcessStroke ) cout << "Continuing overstroke active curve " << GetCurveGroup( m_iCurveLastStroke ).HashId()  << "\n";

			// If the last action was an overstroke_start, continue overstroke, otherwise, just add stroke in
			const CurveGroup &crvGrp = GetCurveGroup( m_iCurveLastStroke );
			const StrokeHistory &shLast = GetCurveGroup( m_iCurveLastStroke ).LastAction();
			
			// Also check that the last drawing plane and this one are fairly similar, at least in view direction
			const double dDot = fabs( Dot( shLast.DrawPlane().Normal(), g_drawState.GetCamera().Look() ) );

			if ( dDot > 0.9 && ( crvGrp.LastStrokeAction() == StrokeHistory::OVERSKETCH_START || crvGrp.LastStrokeAction() == StrokeHistory::OVERSKETCH ) ) {
				SetCurveGroup( m_iCurveLastStroke ).AddMultistrokeOverstroke( s_acrvOverlap[ IndexOverlapLastStroke() ], crv2D );
			} else if ( s_acrvOverlap[m_iCurveLastStroke].GetOverlapType() != CurveOverlap::NO_OVERLAP ) {
                SetCurveGroup( m_iCurveLastStroke ).AddStroke( s_acrvOverlap[ m_iCurveLastStroke ], crv2D );
            } else {
                NewCurve( crv2D );
            }
		}
		
    } else if ( iBest == m_iCurveLastStroke && m_iCurveLastStroke != -1 ) {
        SetCurveGroup( m_iCurveLastStroke ).AddStroke( s_acrvOverlap[ m_iCurveLastStroke ], crv2D );
        
	} else if ( iMergeE1 != -1 && iMergeE2 != -1 ) {
        if ( s_bTraceProcessStroke ) cout << "Doing combine with two curves " << GetCurveGroup( iMergeE1 ).HashId() << " " << GetCurveGroup( iMergeE2 ).HashId() << "\n";
		CombineCurves( crv2D, iMergeE1, iMergeE2 );
		
	} else if ( iBest != -1 ) {
		if ( iBest == IndexOverlapClosedStroke() ) {
            if ( s_bTraceProcessStroke ) cout << "New closed curve \n";

			NewClosedCurve( s_acrvOverlap[iBest], crv2D );
		} else if ( iBest < NumCrvs() ) {
            if ( s_bTraceProcessStroke ) cout << "Adding stroke other curve " << GetCurveGroup(iBest).HashId() << "\n";
			
			m_iLastEdited = iBest;
			
			// If they extended from the end, go back to stroke completion
			if ( SetCurveGroup( iBest ).AddStroke( s_acrvOverlap[iBest], crv2D ) ) {
				m_iCurveLastStroke = iBest;
			} else {
				m_iCurveLastStroke = -1;
			}
		} else {
            NewCurve( crv2D );
        }
	} else {
		NewCurve( crv2D );
	}
	
	// replace the composite 2D screen curve with the real one
	if ( m_iLastEdited >= 0 && m_iLastEdited < NumCrvs() && bBlend == StrokeHistory::BLEND_SCRATCH ) {
		SetCurveGroup( m_iLastEdited ).AddBlendToHistory( in_crvPreSplit2D );
	}

    PropagateEdits();
}


/*
 * Cases:
 *  Last stroke created a new curve. 
 *     Delete: Simply deletes the new curve
 *     Overstroke: Delete the new curve and apply the overstroke to the input curve
 *                 If no input curve then uses the last touched curve
 *                 If input curve is curve that was added, just delete
 *     Join: Delete the new curve and join it to:
 *                 If input curve, join it to that curve
 *                 If input curve is the current curve, do a close
 *                 If no input curve, join to the last touched curve
 *     New curve: In essense, do nothing - delete the stroke and add it back in
 *  Last stroke was extending an existing curve (merge, join, close)
 *     Delete: remove the stroke from the curve
 *     Overstroke: If input curve is selected, overstroke on that curve
 *                 If no input curve is selected, overstroke the last edited curve
 *     Join: If input curve selected, do best merge/join/close with that curve
 *           If input curve is same as last touched or no input curve
 *                 If was close, switch to merge/join
 *                 If was merge/join, switch to close
 *     New curve: Remove stroke from curve and make a new one
 *  Last stroke was an overstroke
 *     Delete: remove the stroke from the curve
 *     Overstroke: Apply the overstroke to the selected curve
 *     Join: Apply the join to the selected curve instead
 *     New: Remove curve and add
 *     
 */
int CurveNetwork::ReProcessStroke( const StrokeHistory::MergeType &in_type, const int in_iCurve )
{
	// Roll back last stroke
	if ( s_bTraceUndo ) cout << "Rolling back stroke " << m_iLastEdited << " saved " << m_iSaveLastEdited << " input " << in_iCurve << "\n";
	
	const int iSaveLastEdited = m_iSaveLastEdited;
	m_iSaveLastEdited = m_iLastEdited;
	
	if ( m_iLastEdited < 0 || m_iLastEdited >= NumCrvs() ) {
		cerr << "ERR: Reprocessing stroke,Warning, no stroke to roll back\n";
		return -1;
	}
	CurveGroup &crvGrp = *m_aopCrv[ m_iLastEdited ];
	
	if ( s_bTraceUndo ) {
		crvGrp.PrintHistory( true );
	}
	
	const StrokeHistory sh = crvGrp.RollBackLastStroke();

	if ( s_bTraceUndo ) {
		crvGrp.PrintHistory( true );
	}
	
	if ( !sh.WasStroke() ) {
		cerr << "ERR: Reprocessing stroke,Last action was not a stroke action " << sh.GetMergeType() << "\n";
		crvGrp.FinishedUndo();
		return -1;
	}
	
	// First put back the combine curve if last stroke was combine
	if ( sh.GetMergeType() == StrokeHistory::COMBINE ) {
		RestoreCombine( sh, crvGrp );
	} else if ( sh.GetMergeType() == StrokeHistory::FIRST_STROKE || sh.GetMergeType() == StrokeHistory::FIRST_STROKE_CLOSED ) {
		// Should be safe to completely delete, but keep just to be on the safe side
		DeleteCurve( m_iLastEdited );
		m_iLastEdited = -1;
	} else {
		if ( crvGrp.NumStrokes() == 0 ) {
			cerr << "ERR: CurveNetwork::ReProcessStroke, returned curve empty but not first stroke " << m_iLastEdited << " " << StrokeHistory::GetMergeTypeString( sh.GetMergeType() ) << "\n";
			DeleteCurve( m_iLastEdited );
			m_iLastEdited = -1;
		} else {
			RestoreStroke( sh, crvGrp );
		}
	}
	
	// At this point we should be exactly the same as before we applied the stroke; now just apply
	const ScreenCurve crvPreSplit = sh.Stroke2D();
	ScreenCurve crv2D = crvPreSplit;
	const StrokeHistory::MergeType bBlend = IsBlend( crvPreSplit, crv2D );
	
	const bool bDoNewClose = ( in_iCurve == -1 || in_iCurve == m_iLastEdited ) && ( ( sh.GetMergeType() == StrokeHistory::FIRST_STROKE || sh.GetMergeType() == StrokeHistory::FIRST_STROKE_CLOSED ) && in_type == StrokeHistory::CLOSE );
	const bool bDoClose = in_iCurve == m_iSaveLastEdited && in_type == StrokeHistory::COMBINE;

	m_iCurveLastStroke = -1;
	
	int iToApply = (in_iCurve == -1) ? iSaveLastEdited : in_iCurve;
	
	
	// Re-apply
	if ( in_type == StrokeHistory::FIRST_STROKE ) {
		NewCurve( crv2D );			
		iToApply = m_aopCrv.num() - 1;
		
	} else if ( in_type == StrokeHistory::FIRST_STROKE_CLOSED || bDoNewClose ) {
		CurveOverlap crvOverlap;
		crvOverlap.ForceClosedCurve( crv2D );
		
		NewClosedCurve( crvOverlap, crv2D );
		
		iToApply = m_aopCrv.num() - 1;
	} else if ( in_type == StrokeHistory::ERASE ) {
		m_iLastEdited = m_iSaveLastEdited = m_iCurveLastStroke = -1;
		iToApply = -1;
		
	} else if ( in_type == StrokeHistory::OVERSKETCH ) {
		if ( iToApply < 0 || iToApply >= NumCrvs() ) {
			cerr << "ERR: Reprocessing stroke, no valid curve\n";
			ProcessCurve(crv2D);
		} else {
			if ( SetCurveGroup( iToApply ).AddStrokeForced( in_type, crv2D ) ) {
				m_iCurveLastStroke = iToApply;
			}
			m_iLastEdited = iToApply;
		}
	} else if ( in_type == StrokeHistory::CLOSE || bDoClose ) {
		if ( iToApply < 0 || iToApply >= NumCrvs() || GetCurve( iToApply ).IsClosed() ) {
			cerr << "ERR: Reprocessing stroke, no valid curve\n";
			ProcessCurve(crv2D);
		} else {
			// Chances are, they just want to change the merge/join to a closed merge/join
			if ( sh.GetMergeType() >= StrokeHistory::MERGE && sh.GetMergeType() <= StrokeHistory::JOIN_REVERSE && in_iCurve == -1 && m_iLastEdited != -1 ) {
				iToApply = m_iLastEdited;
			}
			
			SetCurveGroup( iToApply ).AddStrokeForced( StrokeHistory::CLOSE, crv2D );
			m_iLastEdited = iToApply;
		}
		
	} else if ( in_type == StrokeHistory::MERGE ) {
		if ( iToApply == -1 ) iToApply = m_iLastEdited;
		if ( iToApply < 0 || iToApply >= NumCrvs() || GetCurve( iToApply ).IsClosed() ) {
			cerr << "ERR: Reprocessing stroke, no valid curve\n";
			ProcessCurve(crv2D);
		} else {
			if ( SetCurveGroup( iToApply ).AddStrokeForced( StrokeHistory::MERGE, crv2D, false ) ) {
				m_iCurveLastStroke = iToApply;
			}
			m_iLastEdited = iToApply;
		}
		
	} else if ( in_type == StrokeHistory::COMBINE ) {
		const bool bValidCurve1 = in_iCurve >= 0 && in_iCurve < NumCrvs();
		const bool bValidCurve2 = m_iSaveLastEdited >= 0 && m_iSaveLastEdited < NumCrvs();
		const bool bClosedCurve1 = bValidCurve1 ? GetCurve(in_iCurve).IsClosed() : false;
		const bool bClosedCurve2 = bValidCurve2 ? GetCurve(m_iSaveLastEdited).IsClosed() : false;
		if ( !bValidCurve1 || !bValidCurve2 || bClosedCurve1 || bClosedCurve2 || in_iCurve == m_iSaveLastEdited ) {
			cerr << "ERR: Reprocessing stroke, no valid curve\n";
			ProcessCurve(crv2D);
		} else {
			s_acrvOverlap[in_iCurve].FindBestCombined( s_acrvOverlap[m_iSaveLastEdited] );
			CombineCurves( crv2D, in_iCurve, m_iSaveLastEdited );
		}
	} else {
		cerr << "ERR: Reprocessing stroke, no known type " << in_type << "\n";
		iToApply = -1;
	}
	// Add in the blend action again
	if ( m_iLastEdited >= 0 && m_iLastEdited < NumCrvs() && bBlend == StrokeHistory::BLEND_SCRATCH ) {
		SetCurveGroup( m_iLastEdited).AddBlendToHistory( crvPreSplit );
	}
	
	if ( s_bTraceUndo && iToApply != -1 ) m_aopCrv[iToApply]->PrintHistory(true);
	
	UndoCleanup( sh );
	
	return iToApply;
}

