/*
 *  ActionEvents_Draw.cpp
 *  SketchCurves
 *
 *  Created by Cindy Grimm on 12/29/10.
 *  Copyright 2010 Washington University in St. Louis. All rights reserved.
 *
 */


#include "ActionEvents.h"
#include "UserInterface.h"
#include "ParamFile.h"
#include <OpenGL/OGLDraw3D.H>
#include <OpenGL/OGLDraw2D.H>

/* The circle around the cursor. Basically a quad strip that fades out on the
 * inner and outer rings
 */
void ActionEvents::DrawSelectCircle( const R2Pt &in_pt, const UTILSColor &in_color ) const
{
	const int iDivs = 16;
	const double dTheta = 2.0 * M_PI / (double) iDivs;
    
	g_drawState.StartShader( DrawState::SHADOW_SHADING );
    
	const double dCircleSize = g_drawState.SelectionSize();
	
	const R3Plane plane( g_drawState.GetCamera().At(), g_drawState.GetCamera().Look() );
	const R3Pt ptCircle = plane.IntersectRay( R3Ray( g_drawState.GetCamera().From(), g_drawState.GetCamera().RayFromEye( in_pt ) ) );
	const R3Vec vecRight = g_drawState.GetCamera().Right() * dCircleSize;
	const R3Vec vecUp = g_drawState.GetCamera().Up() * dCircleSize;
	
	glDisable( GL_DEPTH_TEST );
	glBegin( GL_QUAD_STRIP );
	
	for ( int i = 0; i < iDivs+1; i++ ) {
		const R3Vec vecOut = vecRight * cos( i * dTheta ) + vecUp * sin( i * dTheta );
		
		glColor4f( (GLfloat) in_color[0], (GLfloat) in_color[1], (GLfloat) in_color[2], 1.0f );
		glTexCoord2f( 0.5f, 0.0f );	
		glVertex3dv( &(ptCircle + vecOut * 0.7)[0] );
        
		glColor4f( (GLfloat) in_color[0], (GLfloat) in_color[1], (GLfloat) in_color[2], 1.0f );
		glTexCoord2f( 0.0f, 0.0f );	
		glVertex3dv( &(ptCircle + vecOut * 0.85)[0] );
	}
	glEnd();
    
	glBegin( GL_QUAD_STRIP );
	
	for ( int i = 0; i < iDivs+1; i++ ) {
		const R3Vec vecOut = vecRight * cos( i * dTheta ) + vecUp * sin( i * dTheta );
		
		glColor4f( (GLfloat) in_color[0], (GLfloat) in_color[1], (GLfloat) in_color[2], 1.0f );
		glTexCoord2f( 0.0f, 0.0f );	
		glVertex3dv( &(ptCircle + vecOut * 0.85)[0] );
        
		glColor4f( (GLfloat) in_color[0], (GLfloat) in_color[1], (GLfloat) in_color[2], 1.0f );
		glTexCoord2f( 0.5f, 0.0f );	
		glVertex3dv( &(ptCircle + vecOut)[0] );
	}
	glEnd();
	
	g_drawState.EndShader();
}

/* The circle around the cursor. Basically a quad strip that fades out on the
 * inner and outer rings
 */
void ActionEvents::DrawSelectCross( const R2Pt &in_pt, const UTILSColor &in_color ) const
{
	g_drawState.StartShader( DrawState::SHADOW_SHADING );
    
	const double dCrossSize = g_drawState.SelectionSize();
    const double dCrossWidth = 0.15;
	
	const R3Plane plane( g_drawState.GetCamera().At(), g_drawState.GetCamera().Look() );
	const R3Pt ptCircle = plane.IntersectRay( R3Ray( g_drawState.GetCamera().From(), g_drawState.GetCamera().RayFromEye( in_pt ) ) );
	const R3Vec vecRight = g_drawState.GetCamera().Right() * dCrossSize;
	const R3Vec vecUp = g_drawState.GetCamera().Up() * dCrossSize;
	
	glDisable( GL_DEPTH_TEST );

    glColor4f( (GLfloat) in_color[0], (GLfloat) in_color[1], (GLfloat) in_color[2], 1.0f );

	glBegin( GL_POLYGON );
    glTexCoord2f( 0.1f, 0.0f );	
    glVertex3dv( &(ptCircle + vecUp - vecRight * dCrossWidth)[0] );
    glVertex3dv( &(ptCircle - vecUp - vecRight * dCrossWidth)[0] );
    glTexCoord2f( 0.75f, 0.0f );	
    glVertex3dv( &(ptCircle - vecUp)[0] );
    glVertex3dv( &(ptCircle + vecUp)[0] );
    glEnd();

	glBegin( GL_POLYGON );
    glVertex3dv( &(ptCircle + vecUp)[0] );
    glVertex3dv( &(ptCircle - vecUp)[0] );
    glTexCoord2f( 0.1f, 0.0f );	
    glVertex3dv( &(ptCircle - vecUp + vecRight * dCrossWidth)[0] );
    glVertex3dv( &(ptCircle + vecUp + vecRight * dCrossWidth)[0] );
    glEnd();
    
	glBegin( GL_POLYGON );
    glVertex3dv( &(ptCircle + vecRight - vecUp * dCrossWidth)[0] );
    glVertex3dv( &(ptCircle - vecRight - vecUp * dCrossWidth)[0] );
    glTexCoord2f( 0.75f, 0.0f );	
    glVertex3dv( &(ptCircle - vecRight)[0] );
    glVertex3dv( &(ptCircle + vecRight)[0] );
    glEnd();
	
	glBegin( GL_POLYGON );
    glVertex3dv( &(ptCircle + vecRight)[0] );
    glVertex3dv( &(ptCircle - vecRight)[0] );
    glTexCoord2f( 0.1f, 0.0f );	
    glVertex3dv( &(ptCircle - vecRight + vecUp * dCrossWidth)[0] );
    glVertex3dv( &(ptCircle + vecRight + vecUp * dCrossWidth)[0] );
    glEnd();
	
	g_drawState.EndShader();
}

/*
 * Is the cursor over the arrow that's not tangent with the screen?
 */
bool ActionEvents::IsOverTopBottomDragIcon( const R2Pt &in_pt ) const
{
	if ( !OverCurve() ) return false;
	
    const double dDragSize = g_paramFile.GetDouble("CurveDragSize") * g_drawState.SelectionSize();
    const double dSelSize = g_drawState.ScreenSelectionSize();

    const R4Matrix mat = MatrixDragIcon( m_dCursorOverCurve, GetShadowBox().GetPlane( m_actionDragClick.second ), false );
    
    const R3Pt ptE1( 0.0, -dDragSize, 0.0 );
    const R3Pt ptE2( 0.0, dDragSize, 0.0 );
    
    const OGLObjsCamera &cam = g_drawState.GetCamera();
    const R2Pt ptScreenE1 = cam.CameraPt( mat * ptE1 );
    const R2Pt ptScreenE2 = cam.CameraPt( mat * ptE2 );

	const double dLenE1 = ::Length( ptScreenE1 - in_pt );
	const double dLenE2 = ::Length( ptScreenE2 - in_pt );
    
	if ( dLenE1 < 0.5 * dSelSize || dLenE2 < 0.5 * dSelSize ) {
		return true;
	}
	return false;
}


const R3Matrix ActionEvents::CurveAlignment() const
{
    if ( !OverCurve() ) {
        return R3Matrix::Identity();
    }
    
	const Curve &crv = m_crvNetwork.GetCurve( m_iCursorOverCurve);
    const R3Pt ptTick = crv( m_dCursorOverCurve );    
	const R3Vec vecTick = crv.Tangent( m_dCursorOverCurve );
	const R3Pt ptTang = ptTick + vecTick;
	
	const R2Pt ptScreen = g_drawState.GetCamera().CameraPt( ptTick );
	const R2Pt ptScreenTang = g_drawState.GetCamera().CameraPt( ptTang );
	const R2Vec vecTangScreen = ptScreen - ptScreenTang;
	const double dAngRot = RNIsZero(::Length( vecTangScreen )) ? 0.0 : atan2( vecTangScreen[1], vecTangScreen[0] );
	
	// Counteract aspect ratio scaling
	return R3Matrix::Translation( ptScreen - R2Pt(0,0) ) * R3Matrix::Scaling( 1.0, g_drawState.GetCamera().GetAspectRatio(), 1.0 ) * R3Matrix::Rotation(2, -dAngRot);    
}

/*
 * Draw the four-arrow drag icon. Rotate it out of plane if we're doing in-out dragging
 *
 * Matrix does the following:
 *  Rotate around the x axis back into the plane (if in-out dragging)
 *  Rotate so that x-axis points along tangent projected into plane
 *  Translate so that located at curve point projected onto plane
 *
 */
R4Matrix ActionEvents::MatrixDragIcon( const double in_dTCrv, const R3Plane &in_plane, const bool in_bIsPerp ) const
{
    const Curve &crv = m_crvNetwork.GetCurve( m_iCursorOverCurve);
    
    const R3Vec vecPlaneNorm = in_plane.Normal();
    R3Vec vecDs, vecDt;
    in_plane.TangentPlane( vecDs, vecDt );
    
    const R3Pt ptTick = crv( in_dTCrv );    
	const R3Vec vecTick = crv.Tangent( in_dTCrv );
    
    const R3Vec vecOffset = vecPlaneNorm * g_drawState.StripWidth() * 0.5;	    
    const R3Pt ptCenter = in_plane.ProjectOnPlane(ptTick);
    
    const R3Matrix matRotToPlane( vecDs, vecDt, vecPlaneNorm );
    
    const R3Vec vecInPlane = UnitSafe( Rejection( vecPlaneNorm, vecTick ) );
    const double dDot = Dot( vecDs, vecInPlane );
    const double dAng = acos( dDot );
    const R3Vec vecCross = Cross( vecDs, vecInPlane );
    const double dDir = Dot( vecCross, vecPlaneNorm ) > 0.0 ? -1.0 : 1.0;
    
    const R3Matrix matRotBack = in_bIsPerp ? R3Matrix::Rotation(0, -M_PI/2.75) : R3Matrix::Identity();
    const R3Matrix matRotToTang = R3Matrix::Rotation(2, dDir * dAng );
    const R4Matrix mat = R4Matrix::Translation( ptCenter ) * 
    (R4Matrix) matRotToPlane *
    (R4Matrix) matRotToTang * 
    (R4Matrix) matRotBack;
    
    return mat;
}

/*
 * Matrix that does the following:
 *   Rotate the x axis to the curve's tangent, projected into the plane
 *     z axis goes to the plane normal
 *
 * Move the entire thing so that the origin goes to the curve point, projected onto the plane
 *
 */
R4Matrix ActionEvents::MatrixRotateScaleIcon( const R3Plane &in_plane, const double in_dTOnCurve ) const
{
    const Curve &crv = m_crvNetwork.GetCurve( m_iCursorOverCurve);
    
    const R3Vec vecPlaneNorm = in_plane.Normal();
    R3Vec vecDs, vecDt;
    in_plane.TangentPlane( vecDs, vecDt );

    const R3Pt ptTick = crv( in_dTOnCurve );    
	const R3Vec vecTick = crv.Tangent( in_dTOnCurve );
    
    const R3Vec vecOffset = vecPlaneNorm * g_drawState.StripWidth() * 0.5;	    
    const R3Pt ptCenter = in_plane.ProjectOnPlane(ptTick);
    
    const R3Matrix matRotToPlane( vecDs, vecDt, vecPlaneNorm );
    
    const R3Vec vecInPlane = UnitSafe( Rejection( vecPlaneNorm, vecTick ) );
    const double dDot = Dot( vecDs, vecInPlane );
    const double dAng = acos( dDot );
    const R3Vec vecCross = Cross( vecDs, vecInPlane );
    const double dDir = Dot( vecCross, vecPlaneNorm ) > 0.0 ? -1.0 : 1.0;
    
    const R3Matrix matRotToTang = R3Matrix::Rotation(2, dDir * dAng );
    const R4Matrix mat = R4Matrix::Translation( ptCenter ) * 
    (R4Matrix) matRotToPlane *
    (R4Matrix) matRotToTang;
    
    return mat;
}


/*
 * Do the actual drawing of the drag icon
 *
 * Size of the icon is determined by CurveDragSize in the params file; this is a percentage
 * of the selection size*
 *
 * If tube drawing, do 3D arrows, otherwise, do texture
 */
void ActionEvents::DrawDragIcon( const double in_dTCrv, const R3Plane &in_plane, const bool in_bIsDrag, const bool in_bIsSelected ) const
{
    
    const R4Matrix mat = MatrixDragIcon( in_dTCrv, in_plane, m_bIsPerpendicularDrag );
    
    const double dDragSize = g_paramFile.GetDouble("CurveDragSize") * g_drawState.SelectionSize();
    
    glPushMatrix();
    glMultMatrixd( &mat(0,0) );
    
    const bool bDo3D = g_drawState.m_drawingStyle == DrawState::TUBES || g_drawState.m_drawingStyle == DrawState::RIBBONS;

    /* Get the location of the drag icon in 3D, projected it onto the wall */
    const R3Pt ptCrv = m_crvNetwork.GetCurve( m_iCursorOverCurve )( in_dTCrv );
    const R3Pt ptPlane = in_plane.ProjectOnPlane( ptCrv );

    /* Color choice: only if drag icon AND same wall is this color yellow */
    if ( in_bIsDrag && in_bIsSelected && m_actionDragClick.first != NO_ACTION ) {
        glColor4f( 1.0f, 1.0f, 0.2f, 1.0 );
    } else {
        glColor4f( 0.2f, 1.0f, 0.2f, 1.0 );
    }
    
    if ( bDo3D ) {
        /* Draw four arrows */
        g_drawState.StartShader( DrawState::SURFACE_SHADING );
        
        OGLDrawSphere(R3Sphere( R3Pt(0.0, 0.0, 0.0), dDragSize * 0.3 ) );
        
        /* only draw the arrows if this is a drag action */
        if ( in_bIsDrag == true ) {
            OGLDrawArrow(R3Pt(0.0, 0.0, 0.0), dDragSize, dDragSize * 0.3 );
            glRotated( 90.0, 0.0, 0.0, 1.0 );
            OGLDrawArrow(R3Pt(0.0, 0.0, 0.0), dDragSize, dDragSize * 0.3 );
            glRotated( 90.0, 0.0, 0.0, 1.0 );
            OGLDrawArrow(R3Pt(0.0, 0.0, 0.0), dDragSize, dDragSize * 0.3 );
            glRotated( 90.0, 0.0, 0.0, 1.0 );
            OGLDrawArrow(R3Pt(0.0, 0.0, 0.0), dDragSize, dDragSize * 0.3 );
        }
        g_drawState.EndShader();

    } else {
        /* Still draw this in 3D, but basically a texture plane centered at the given point */
        const double dZOffset = g_drawState.StripWidth() * 0.25;	
        if ( in_bIsDrag == true ) {
            g_drawState.StartMenuShader( DrawState::ICON_DRAG, true );
            glDisable( GL_DEPTH_TEST );
            
            glBegin( GL_QUADS );
            
            glColor4f( 0.0f, 0.0f, 0.0f, 1.0 );
            
            glTexCoord2f( 0.0f, 0.0f );	
            glVertex3d( -dDragSize, -dDragSize, dZOffset );
            glTexCoord2f( 1.0f, 0.0f );	
            glVertex3d( dDragSize, -dDragSize, dZOffset );
            glTexCoord2f( 1.0f, 1.0f );	
            glVertex3d( dDragSize, dDragSize, dZOffset );
            glTexCoord2f( 0.0f, 1.0f );	
            glVertex3d( -dDragSize, dDragSize, dZOffset );
            glEnd();
            g_drawState.EndShader();
        } else {
            /* If not drag action, just draw a circle */
            g_drawState.StartShader( DrawState::LINE_SHADING );
            glDisable( GL_DEPTH_TEST );

            glBegin( GL_QUAD_STRIP );
            const double dDiv = 2.0 * M_PI / 32.0;
            for ( double dT = 0.0; dT <= 2.0 * M_PI + 0.5 * dDiv; dT += dDiv ) {
                glVertex3d( cos(dT) * dDragSize * 0.5, sin(dT) * dDragSize * 0.5, dZOffset );
                glVertex3d( cos(dT) * dDragSize * 0.35, sin(dT) * dDragSize * 0.35, dZOffset );
            }
            glEnd();
            g_drawState.EndShader();
        }
    }
    
    glPopMatrix();
    
    /* Draw the selection box 
    const OGLObjsCamera &cam = g_drawState.GetCamera();

    const R4Matrix matDrag = MatrixDragIcon( in_plane, false );
    double dTOnCrv;
    const R4Matrix matRotScl = MatrixRotateScaleIcon( in_plane, m_dTDragCurve );

    OGLBegin2D(cam.Width(), cam.Height());
    R2Polygon poly(4);
    glBegin( GL_LINE_LOOP );
    cout << in_plane.Normal() << "\n";
    for ( int i = 0; i < 4; i++ ) {
        const int iX = i / 2;
        const int iY = ((i+3) % 4) / 2;
        const R3Pt pt( -dDragSize + 2.0 * dDragSize * iX, -dDragSize + 2.0 * dDragSize * iY, 0.0 );
        const R3Pt ptMap = matDrag * pt;
        poly[i] = cam.CameraPt( ptMap );
        glVertex2d( poly[i][0], poly[i][1] );
    }

    glEnd();
    glBegin( GL_LINES );
    const R2Pt ptScreenTop = cam.CameraPt( matRotScl * R3Pt( 0.0, dDragSize, 0.0 ) );
    const R2Pt ptScreenBot = cam.CameraPt( matRotScl * R3Pt( 0.0, -dDragSize, 0.0 ) );
    glVertex2d( ptScreenTop[0], ptScreenTop[1] );
    glVertex2d( ptScreenBot[0], ptScreenBot[1] );
    glEnd();
    OGLEnd2D();
    */
}

void ActionEvents::DrawRotateScaleIcon( const R3Plane &in_plane, const Action in_action ) const
{
    const R4Matrix mat = MatrixRotateScaleIcon( in_plane, m_dTDragClick );
    
    const double dDragSize = g_paramFile.GetDouble("CurveDragSize") * g_drawState.SelectionSize();
    
    const bool bDo3D = g_drawState.m_drawingStyle == DrawState::TUBES || g_drawState.m_drawingStyle == DrawState::RIBBONS;
    
    if ( bDo3D == false ) glDisable( GL_DEPTH_TEST );
    glTexCoord2f( 0.0f, 0.0f );	
    glLineWidth(2.0f);
    glEnable( GL_LINE_SMOOTH );
    
	g_drawState.StartShader( DrawState::SHADOW_SHADING );
        
    glPushMatrix();
    glMultMatrixd( &mat(0,0) );
    
    /* Draw a little black line along the curve */
    const double dZOffset = 0.01;//m_actionDragClick.second == ShadowBox::DRAWING_PLANE ? dDragSize * 0.01 : 0.0;
    if ( in_action != DRAG_CURVE && in_action != NO_ACTION ) {
        glColor3f(0.2f, 0.2f, 0.2f);

        if ( bDo3D ) {
            glPushMatrix();
            glRotated(90, 0, 0, 1);
            OGLDrawCylinder( R3Pt(0,0,dZOffset), dDragSize * 0.6, dDragSize * 0.1 );
            glPopMatrix();
            glPushMatrix();
            glRotated(-90, 0, 0, 1);
            OGLDrawCylinder( R3Pt(0,0,dZOffset), dDragSize * 0.6, dDragSize * 0.1 );
            glPopMatrix();
        } else {
            glBegin( GL_LINES );
            glVertex3d( -dDragSize * 0.5, 0.0, dZOffset );
            glVertex3d(  dDragSize * 0.5, 0.0, dZOffset );
            glEnd();
        }
    }
    
    
    const double dAngRot = M_PI;
    if ( in_action == ROTATE_CURVE || in_action == ROTATE_SCALE_CURVE ) {
        glColor3f( 0.6f, 0.4f, 0.4f );
        
        if ( bDo3D ) {
            glBegin( GL_QUAD_STRIP );
            const double dOutside = dDragSize;
            const double dInside = dDragSize * 0.8;
            const double dThickness = dDragSize * 0.15;
            glNormal3d( 0,0,-1 );
            for ( int i = 0; i < 16; i++ ) {
                const double dTheta = dAngRot * 0.5 - dAngRot * (i / 15.0);
                glVertex3d( cos(dTheta) * dOutside, sin(dTheta) * dOutside, -dThickness );
                glVertex3d( cos(dTheta) * dInside, sin(dTheta) * dInside, -dThickness );
            };
            glEnd();

            glBegin( GL_QUAD_STRIP );
            glNormal3d( 0,0,1 );
            for ( int i = 0; i < 16; i++ ) {
                const double dTheta = dAngRot * 0.5 - dAngRot * (i / 15.0);
                glVertex3d( cos(dTheta) * dOutside, sin(dTheta) * dOutside, dThickness );
                glVertex3d( cos(dTheta) * dInside, sin(dTheta) * dInside, dThickness );
            };
            glEnd();
            
            glBegin( GL_QUAD_STRIP );
            const double dOffset = dOutside - dInside;

            glVertex3d( -dOffset, dOutside - dOffset * 0.5, dThickness );
            glVertex3d( -dOffset, dOutside - dOffset * 0.5, -dThickness );

            glVertex3d( 0.0, dInside - dOffset, dThickness );
            glVertex3d( 0.0, dInside - dOffset, -dThickness );
            for ( int i = 0; i < 16; i++ ) {
                const double dTheta = dAngRot * 0.5 - dAngRot * (i / 15.0);
                glNormal3d( -cos(dTheta), -sin(dTheta), 0.0 );
                glVertex3d( cos(dTheta) * dInside, sin(dTheta) * dInside, dThickness );
                glVertex3d( cos(dTheta) * dInside, sin(dTheta) * dInside, -dThickness );
            };

            for ( int i = 0; i < 16; i++ ) {
                const double dTheta = -dAngRot * 0.5 + dAngRot * (i / 15.0);
                glNormal3d( cos(dTheta), sin(dTheta), 0.0 );
                glVertex3d( cos(dTheta) * dOutside, sin(dTheta) * dOutside, dThickness );
                glVertex3d( cos(dTheta) * dOutside, sin(dTheta) * dOutside, -dThickness );
            };
            glVertex3d( 0.0, dOutside + dOffset, dThickness );
            glVertex3d( 0.0, dOutside + dOffset, -dThickness );

            glVertex3d( -dOffset, dOutside - dOffset * 0.5, dThickness );
            glVertex3d( -dOffset, dOutside - dOffset * 0.5, -dThickness );
            
            glEnd();
            
            glBegin( GL_TRIANGLES );
            
            glVertex3d( 0.0, dInside - dOffset, dThickness );
            glVertex3d( 0.0, dOutside + dOffset, dThickness );
            glVertex3d( -dOffset, dOutside - dOffset * 0.5, dThickness );
            
            glColor3f( 0,1,0 );
            glVertex3d( 0.0, dInside - dOffset, -dThickness );
            glVertex3d( 0.0, dOutside + dOffset, -dThickness );
            glVertex3d( -dOffset, dOutside - dOffset * 0.5, -dThickness );

            glEnd();
            
        } else {
            glBegin( GL_LINE_STRIP );
            const double dThetaE1 = dAngRot * 0.5;
            const R2Pt ptEnd1( dDragSize * cos(dThetaE1), dDragSize * sin(dThetaE1) );
            const R2Pt ptEnd1a( dDragSize * cos(dThetaE1) + dDragSize * 0.5, dDragSize * sin(dThetaE1) - dDragSize * 0.25 );
            const R2Pt ptEnd1b( dDragSize * cos(dThetaE1) + dDragSize * 0.5, dDragSize * sin(dThetaE1) + dDragSize * 0.25 );
            glVertex3d( ptEnd1[0],  ptEnd1[1], dZOffset );
            glVertex3d( ptEnd1a[0], ptEnd1a[1], dZOffset );
            glVertex3d( ptEnd1[0],  ptEnd1[1], dZOffset );
            glVertex3d( ptEnd1b[0], ptEnd1b[1], dZOffset );
            glVertex3d( ptEnd1[0],  ptEnd1[1], dZOffset );
            for ( int i = 0; i < 16; i++ ) {
                const double dTheta = dAngRot * 0.5 - dAngRot * (i / 15.0);
                const R2Pt pt( dDragSize * cos(dTheta), dDragSize * sin(dTheta) );
                glVertex3d( pt[0], pt[1], dZOffset );
            };
            glEnd();
            
        }
    }
    
    if ( in_action == SCALE_CURVE || in_action == ROTATE_SCALE_CURVE ) {
        glColor3f( 0.5f, 0.3f, 0.3f );
        
        if ( bDo3D ) {
            OGLDrawSphere(R3Sphere( R3Pt(0.0, 0.0, 0.0), dDragSize * 0.3 ) );
        } else {
            glBegin( GL_LINE_LOOP );
            const double dSizeCircle = dDragSize * 0.3;
            for ( int i = 0; i < 32; i++ ) {
                const double dTheta = (i / 32.0) * 2.0 * M_PI;
                glVertex3d( cos(dTheta) * dSizeCircle, sin(dTheta) * dSizeCircle, dZOffset );
            };
            glEnd();
        }
    }
    
    glPopMatrix();
    
    const bool bIsMouseDrag = Fl::event_state() & FL_BUTTON1;
    
    if ( in_action == SCALE_CURVE && bIsMouseDrag ) {
        const Curve &crv = m_crvNetwork.GetCurve( m_iCursorOverCurve);
        const R3Pt ptStart = in_plane.ProjectOnPlane( crv( m_dCursorOverCurve ) );
        const R3Pt ptEnd = in_plane.IntersectRay( R3Ray( g_drawState.GetCamera().From(), g_drawState.GetCamera().RayFromEye(m_ptRotateScale.second) ) );
        R3Matrix matRot;
        R3Matrix::MatrixVecToVec( matRot, UnitSafe( ptEnd - ptStart ), R3Vec(0,1,0) );
        const R4Matrix matLine = R4Matrix::Translation( ptStart ) * (R4Matrix) matRot;
        
        glColor4f( 0.6, 0.6, 0.6, 1.0 );
        if ( bDo3D ) {
            glPushMatrix();
            glMultMatrixd( &matLine(0,0) );
            
            OGLDrawArrow(R3Pt(0.0, 0.0, 0.0), ::Length(ptEnd - ptStart), dDragSize * 0.3 );
            glPopMatrix();
        } else {
            glBegin( GL_LINES );
            glVertex3dv( & ptStart[0] );
            glVertex3dv( & ptEnd[0] );
            glEnd();
        }
    }

	g_drawState.EndShader();
    
}


void ActionEvents::DrawDragIcon( const double in_dTCrv ) const
{
	if ( !OverCurve() ) {
		return;
	}
    
	if ( m_action != DRAG_CURVE && m_action != SCALE_CURVE && m_action != ROTATE_CURVE && m_action != ROTATE_SCALE_CURVE ) {
		return;
	}
	
    /* Always draw the drag icon for the drawing plane
     * Will draw arrows if this is not a rotate/scale action. Will draw arrows yellow if the drag
     * action in the viewing plane is selected */
    const ShadowBox &sb = g_drawState.GetShadowBox();
    const bool bDrawArrows = m_actionDragClick.first == DRAG_CURVE || m_actionDragClick.first == NO_ACTION;
    const R3Plane planeDraw( m_crvNetwork.GetCurve( m_iCursorOverCurve )( in_dTCrv ), sb.GetPlane( ShadowBox::DRAWING_PLANE ).Normal() );
    DrawDragIcon( in_dTCrv, planeDraw, 
                  bDrawArrows, m_actionDragClick.second == ShadowBox::DRAWING_PLANE );
    
    /* Draw rotate scale icons if cursor is close to the curve, but not on the drag icon */
    if ( m_actionDragClick.second == ShadowBox::DRAWING_PLANE ) {
        DrawRotateScaleIcon( planeDraw, m_actionDragClick.first );
    }
    
    /* If the walls are visible... */
    if ( g_drawState.m_opUI->m_bShowShadowBox->value() ) {
        /* Draw drag icon, with arrows if this is a drag action. Will draw in yellow if this is
         * a drag action in the corresponding wall */
        DrawDragIcon( in_dTCrv, sb.FloorPlane(), bDrawArrows, m_actionDragClick.second == ShadowBox::FLOOR );
        DrawDragIcon( in_dTCrv, sb.SideWallPlane(), bDrawArrows, m_actionDragClick.second == ShadowBox::SIDEWALL );

        /* Draw rotate-scale icons if rotate scale in corresponding wall */
        if ( m_actionDragClick.second == ShadowBox::FLOOR ) {
            DrawRotateScaleIcon( sb.FloorPlane(), m_actionDragClick.first );
        }
        if ( m_actionDragClick.second == ShadowBox::SIDEWALL ) {
            DrawRotateScaleIcon( sb.SideWallPlane(), m_actionDragClick.first );
        }
    }
}

/*
 * Draw one of the green lines perpendicular to the screen space tangent of the curve
 */
void ActionEvents::DrawTick( const double in_dTCrv ) const
{
	double dTCrv = in_dTCrv;
	if ( dTCrv < 0.0 ) dTCrv += 1.0;
	if ( dTCrv > 1.0 ) dTCrv -= 1.0;
	
	const Curve &crv = m_crvNetwork.GetCurve( m_iCursorOverCurve);
    const R3Pt ptTick = crv( dTCrv );
	const R2Pt ptScreen = g_drawState.GetCamera().CameraPt( ptTick );
    
    const double dTDelta = g_drawState.CurveSpacingSize() / crv.Length();
    const double dTStart = WINmax( 0.0, dTCrv - dTDelta );
    const double dTEnd = WINmin( 1.0, dTCrv + dTDelta );
    
    const bool bIsOffEnd = RNApproxEqual(in_dTCrv, 0.0) || RNApproxEqual(in_dTCrv, 1.0);

	const double dDragSize = (bIsOffEnd ? 0.5 : 1.0 ) * g_paramFile.GetDouble("CurveDragSize") * g_drawState.ScreenSelectionSize();
	
    const R3Vec vecCrvDir = crv( dTEnd ) - crv( dTStart );
	const R2Pt ptScreenTang = g_drawState.GetCamera().CameraPt( ptTick + vecCrvDir );
	const R2Vec vecTang = ApproxEqual( ptScreen, ptScreenTang ) ? R2Vec(1,0) : UnitSafe( ptScreenTang - ptScreen );
	const R2Vec vecNorm = R2Vec( vecTang[1], -vecTang[0] ) * dDragSize;
    
	const UTILSColor colTick = g_paramFile.GetColor("ColorOfTick");
    
	g_drawState.StartShader( DrawState::SCREENSPACE_SHADING );
	glDisable( GL_DEPTH_TEST );
    
	glLineWidth( (GLfloat) g_paramFile.GetDouble("TickMarkLineWidth") );
	
    glBegin( GL_LINES );
	
	glNormal3dv( &g_drawState.m_vecView[0] );
    if ( bIsOffEnd ) {
        glColor4f( (GLfloat) colTick[0], (GLfloat) colTick[1] * 0.25, (GLfloat) colTick[2] * 0.25, 1.0f );
    } else {
        glColor4f( (GLfloat) colTick[0], (GLfloat) colTick[1], (GLfloat) colTick[2], 1.0f );
    }
    
	glTexCoord2f( 0.0f, 0.0f );	
	glVertex2dv( &(ptScreen - vecNorm)[0]);
	glVertex2dv( &(ptScreen + vecNorm)[0]);
    
    glEnd();
    
	g_drawState.EndShader();
}

/*
 * Draw the drag icon and, if a region is selected, the region ticks as well
 */
void ActionEvents::DrawTicks( ) const
{
	if ( !OverCurve() ) {
		return;
	}
	
    if ( m_action == DRAG_CURVE || m_action == ROTATE_CURVE || m_action == ROTATE_SCALE_CURVE || m_action == SCALE_CURVE || m_action == MENU_SELECT_CURVE_REGION ) {
        
		const Curve &crv = m_crvNetwork.GetCurve( m_iCursorOverCurve);
        
        if ( m_iCursorOverCurveIndex != -1 ) {
            DrawDragIcon( crv.PercAlong( m_iCursorOverCurveIndex ) );            
        } else {
            DrawDragIcon( m_dCursorOverCurve );            
        }
		if ( IsSubset() ) {
            if ( m_iCursorOverCurveTs.first != -1 ) {
                DrawTick( crv.PercAlong( m_iCursorOverCurveTs.first ) );
            } else {
                DrawTick( StartRegion() );
            }
            if ( m_iCursorOverCurveTs.second != -1 ) {
                DrawTick( crv.PercAlong( m_iCursorOverCurveTs.second ) );
            } else {
                DrawTick( EndRegion() );
            }
		}
	}
}

/*
 * Selection has changed - update the curve colors
 */
void ActionEvents::UpdateCurveSelectionColors( )
{
	for ( int i = 0; i < m_crvNetwork.NumCrvs(); i++ ) {
		if ( i == m_iCursorOverCurve ) {
            if ( IsSubset() ) {
                m_crvNetwork.SetSelectionRegion( m_iCursorOverCurve, m_dCursorOverCurveTs );
            } else {
                m_crvNetwork.SetSelectionRegion( m_iCursorOverCurve, std::pair<double,double>( m_dCursorOverCurve, m_dCursorOverCurve ) );
            }
		} else {
			m_crvNetwork.ClearSelectionRegion( i );
		}
	}
}


// pass on to shadow box
void ActionEvents::DrawShadowBox() const
{
    GetShadowBox().Draw( m_iCursorOverShadowBox );
}

// pass on to shadow box
void ActionEvents::DrawDrawingPlane() const
{
	GetShadowBox().DrawDrawingPlane( m_iCursorOverShadowBox );
}

/* Draw the 6 axes
 * m_amatAlignAxis is set up in CreateAxis
 * TODO: Draw just three axes in the case of the extrusion surface
 */
void ActionEvents::DrawAlignmentAxes() const
{
    if ( !(m_action == CAMERA_ALIGN || m_action == DRAWPLANE_ALIGN) || m_bInFocus == false ) {
        return;
    }
    g_drawState.StartShader( DrawState::LINE_SHADING );	
	const double dDist = g_drawState.MenuSize3D();
	
    glMatrixMode( GL_MODELVIEW );
    for ( int i = 0; i < 6; i++ ) {
        glPushMatrix();
        glMultMatrixd( &m_amatAlignAxis[i](0,0) );
        
        if ( m_iAlignCamera == i ) {
            glColor4f( 1.0, 1.0, 1.0, 1.0 );
        } else {
            const float fRed = (i / 2 == 0) ? 1.0 : (i%2) ? 0.2 : 0.0;
            const float fGreen = (i / 2 == 1) ? 1.0 : (i%2) ? 0.2 : 0.0;
            const float fBlue = (i / 2 == 2) ? 1.0 : (i%2) ? 0.2 : 0.0;
            const float fAlpha = i%2 ? 0.5 : 1.0;
            glColor4f( fRed, fGreen, fBlue, fAlpha );
        }
        
        OGLDrawArrow( R3Pt(0,0,0), dDist, dDist * 0.2 );
        glPopMatrix();
    }
    
    g_drawState.EndShader();
}


/* Handle a bit of the what to draw 
 *
 * Drawing strokes, y/n
 * Draw curve, regardless
 * Draw point and normal constraints
 * Draw reflected curves
 * Draw shadows
 */
void ActionEvents::DrawCurves( ) const
{
    for ( int i = 0; i < m_crvNetwork.NumCrvs(); i++ ) {
        if ( g_drawState.m_opUI->m_bShowFragments->value() ) {
            m_crvNetwork.GetCurveGroup(i).DrawFragments( );
        }
        
		m_crvNetwork.GetCurveGroup(i).DrawCurve() ;
    }
    
    // Draw webbing
    for ( int i = 0; i < m_crvNetwork.NumConstraints(); i++ ) {
        m_crvNetwork.GetConstraint(i).Draw();
    }
}

/* Draw potential and found intersections and intersection normals */
void ActionEvents::DrawSurface() const
{
    if ( !g_drawState.m_opUI ) {
        return;
    }
    
	if ( g_drawState.m_opUI->m_bShowMesh->value() ) {    
		m_srfData.DrawSurface();
        m_srfWeb.Draw();
        for ( int i = 0; i < m_aopPatchSurfaces.num(); i++ ) {
            m_aopPatchSurfaces[i]->DrawSurface();
        }
	}
}



void ActionEvents::UpdateDrawData()
{
    m_crvNetwork.UpdateDrawData();
    
    if ( m_bCheckConsistency ) {
        m_crvNetwork.CheckConsistency();
        m_bCheckConsistency = false;
    }
}

/* Draw two spheres and a cylinder between them for snapping curves.
   Alters the t value of the snap curve
 */
void ActionEvents::DrawSnapCylinder() const
{
	if ( m_menuWhich != MENU_CURVE || m_menuSelection != MENU_CURVE_SNAP || m_action != MENU_SELECT_CURVE || m_iSnapCurveFirst.first == -1 ) {
		return;
	}
	
	const UTILSColor col = g_paramFile.GetColor("ColorPointConstraints");
	const double dRad = 0.1 * g_drawState.SelectionSize();
	const double dSphereSize = g_drawState.StripWidth();
	
    g_drawState.StartShader( DrawState::LINE_SHADING );	

	glColor4f( col[0], col[1], col[2], 1.0f );
	
    glMatrixMode( GL_MODELVIEW );
	
	R3Matrix matRot;
    const Curve &crv1 = m_crvNetwork.GetCurve( m_iSnapCurveFirst.first );

	if ( !OverCurve() ) {
        OGLDrawSphere( R3Sphere( crv1( m_iSnapCurveFirst.second ), dSphereSize ) );
	} else {
        const pair<double,double> dCrvs = AdjustSnapTValues();
        const Curve &crv2 = m_crvNetwork.GetCurve( m_iCursorOverCurve );
        
        const R3Pt ptClosest1 = crv1( dCrvs.first );
        const R3Pt ptClosest2 = crv2( dCrvs.second );
        
        
        if ( ::Length( ptClosest2 - ptClosest1 ) > 0.0 ) {
            R3Matrix::MatrixVecToVec(matRot, R3Vec(0,1,0), UnitSafe( ptClosest2 - ptClosest1 ) );
            const R4Matrix mat = R4Matrix::Translation( ptClosest1) * (R4Matrix) matRot;
            
            glPushMatrix();
            glMultMatrixd( &mat(0,0) );
            OGLDrawCylinder( R3Pt(0,0,0), ::Length( ptClosest2 - ptClosest1 ), dRad );
            glPopMatrix();
            OGLDrawSphere( R3Sphere( ptClosest1, dSphereSize ) );
            OGLDrawSphere( R3Sphere( ptClosest2, dSphereSize ) );
        } else {
            OGLDrawSphere( R3Sphere( ptClosest1, dSphereSize ) );
        }
    }
	
	
    g_drawState.EndShader();
	
}


/// Draw the circle around the cursor
void ActionEvents::DrawSelectCursor() const
{	
    if ( m_bInFocus == false ) {
        return;
    }
    
    if ( g_drawState.m_opUI->m_bShowSelectCircle->value() && m_action == NO_ACTION ) {
        DrawSelectCross( m_ptScreenLast, UTILSColor(0.0, 0.5, 0.0 ) );
    } else if ( m_inLogFile.good() ) {
        DrawSelectCircle( m_ptScreenLast, UTILSColor::YELLOW );
    }
}

// Draw the selection circles at the ends of the stroke
void ActionEvents::DrawLastStrokeCircles( ) const
{
	if ( !g_drawState.m_opUI->m_bShowStrokeCircles->value() ) {
		return;
	}
	if ( m_action != NO_ACTION || !m_crvNetwork.IsLastActionStroke() ) {
		return;
	}
	
	const CurveGroup &crvGrp = m_crvNetwork.GetLastEditedCurveGroup();
	const R2Pt pt1 = g_drawState.GetCamera().CameraPt( crvGrp.StrokeStartPt() );
	const R2Pt pt2 = g_drawState.GetCamera().CameraPt( crvGrp.StrokeEndPt() );

	const UTILSColor col = m_bCursorOverLastStroke ? UTILSColor::GREEN : UTILSColor::GREY;

	DrawSelectCircle( pt1, col );
	DrawSelectCircle( pt2, col );
}

// Draw the circles at the end of the smooth region
// TODO: Make this look a little bit less like the last stroke circles
void ActionEvents::DrawRepeatSmooth( ) const
{
	if ( !g_drawState.m_opUI->m_bShowStrokeCircles->value() ) {
		return;
	}
	if ( m_action != NO_ACTION || !m_crvNetwork.IsLastActionSmooth() ) {
		return;
	}
	
	const CurveGroup &crvGrp = m_crvNetwork.GetLastEditedCurveGroup();
	
	const R2Pt pt1 = g_drawState.GetCamera().CameraPt( crvGrp.SelectRegionStartPt() );
	const R2Pt pt2 = g_drawState.GetCamera().CameraPt( crvGrp.SelectRegionEndPt() );
	
	const UTILSColor col = m_bCursorOverLastSmooth ? UTILSColor::GREEN : UTILSColor::BLUE;
	
	DrawSelectCircle( pt1, col );
	DrawSelectCircle( pt2, col );
}

void ActionEvents::DrawConstraints() const
{    
    if ( g_drawState.m_opUI->m_bShowCurvePointConstraints->value() ) {
        const PointConstraint *opPC = m_opCursorOverCurvePointConstraint;
        if ( opPC == NULL && OverCurve() ) {
            if ( m_menuSelection == MENU_CURVE_SNAP ) {
                opPC = m_crvNetwork.HasConstraintNoEdit( m_iCursorOverCurve, m_dCursorOverCurve );
            }
        }
        m_crvNetwork.DrawPointConstraints( opPC );
    }
    
    if ( g_drawState.m_opUI->m_bShowCurveNormalConstraints->value() ) {
        const PointConstraint *opPC = m_opCursorOverCurveNormalConstraint;
        if ( opPC == NULL && OverCurve() ) {
            if ( m_menuSelection == MENU_CURVE_NORMAL ) {
                opPC = m_crvNetwork.HasConstraintNoEdit( m_iCursorOverCurve, m_dCursorOverCurve );
            }
        }
        m_crvNetwork.DrawNormalConstraints( opPC );
    }
}

// Draw the inflation/extrusion surface
void ActionEvents::DrawInflationSurface() const 
{ 
    if ( g_drawState.m_opUI->m_bShowInflationSrf->value() ) {
        m_srfInflation.Draw( m_iCursorOverInflationSrf, m_inflationSrfEditAction ); 
    }
    if ( g_drawState.m_opUI->m_bShowExtrusionSrf->value() ) {
        m_srfInflation.Draw( m_iCursorOverInflationSrf, m_inflationSrfEditAction ); 
    }
}

