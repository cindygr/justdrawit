/*
 *  PointConstraint.h
 *  SketchCurves
 *
 *  Created by Cindy Grimm on 9/15/11.
 *  Copyright 2011 Washington University in St. Louis. All rights reserved.
 *
 */

#ifndef _POINT_CONSTRAINT_DEFS_H_
#define _POINT_CONSTRAINT_DEFS_H_

#include <utils/Rn_Defs.H>
#include <utils/R3_Plane.H>
#include <utils/Utils_Color.H>
#include "IntersectionDrawData.h"

class CurveGroup;

/* A constraint on a point of one (or more) curves
 * Can be a constraint on a point or a normal or both
 *
 * Constraints are kept both as a t value, a point index on the curve, and a point in space
 *
 * This is because curves can be re-sampled, extended, etc - so the "true" constraint is
 * the point in space. The t values and point indices are around to a) save finding them
 * again and b) for re-establishing, which point to drag
 *
 * Undo: Every action that changes the point, vector, or curve info gets saved for undo.
 *  These actions are timestamped. Before the action is performed, the current state
 *  is put in the history list, then the current time stamp of the data is set to the current
 *  time (kept by CurveNetwork).
 *
 */
 
class PointConstraint {
public:
    typedef enum {
        INVALID = -1,
        POINT_ONLY,
        NORMAL_ONLY,
        POINT_AND_NORMAL
    } ConstraintType;
    
    typedef enum {
        NO_ACTION,
        NEW_POINT,
        NEW_NORMAL,
        NEW_POINT_NORMAL,
        ADDED_CURVEGROUP,
        REMOVED_CURVEGROUP,
        CHANGED_POINT,
        CHANGED_NORMAL,
        CHANGED_TYPE,
        UNDO
    } ActionType;
    
private:
    class CurveInfo {
    public:
        CurveGroup *m_opCG;
        double      m_dT;
        int         m_iPtIndex;
        
        CurveInfo &operator=( const CurveInfo & );
        CurveInfo( const CurveInfo &in_ci ) { *this = in_ci; }
        CurveInfo() { }
        ~CurveInfo() { }
        
        void Print() const;
    };
    
    static double m_dSclNormal, m_dSclPoint;
    static UTILSColor m_colPtSelected, m_colPtUnselected, m_colNormSelected, m_colNormUnselected;
    
    class ConstraintData {
    public:
        int                m_iTimeStamp;
        ActionType         m_action;
        Array< CurveInfo > m_aci;
        R3Pt               m_pt;
        R3Vec              m_vec;
        ConstraintType     m_type;
        
        bool IsPoint() const { return m_type == POINT_ONLY || m_type == POINT_AND_NORMAL; }
        bool IsNormal() const { return m_type == NORMAL_ONLY || m_type == POINT_AND_NORMAL; }
        int NumCurves() const { return m_aci.num(); }
        bool HasCurveGroup( const CurveGroup &in_grp ) const;
        double GetTValue( const CurveGroup &in_grp ) const;
        int GetPtIndex( const CurveGroup &in_grp ) const;
        R3Pt GetPt( const CurveGroup &in_grp ) const;
        R3Pt GetPt( const int in_i ) const;

        ConstraintData &operator=( const ConstraintData & );
        ConstraintData( const ConstraintData &in_ci ) { *this = in_ci; }
        ConstraintData();
        ~ConstraintData() { m_iTimeStamp = -1; }
        
        void Print() const;
    };  
    
    const int          m_iHashId;
    
    // Keep the history. m_data is the current one.
    Array< ConstraintData > m_aHistory;
    ConstraintData          m_data;
    bool                    m_bUpdated;
    IntersectionDrawData    m_drawData; // For webbing
    
    void SetNormalConstraint();
    
    void TimeStamp();
    
public:
    /**@name accessors */
    //@{
    ///
    int HashId() const { return m_iHashId; }
    ///
    const R3Pt &GetPt() const { return m_data.m_pt; }
    ///
    const R3Vec &GetNormal() const { return m_data.m_vec; }
    ///
    bool IsPoint() const { return m_data.IsPoint(); }
    ///
    bool IsNormal() const { return m_data.IsNormal(); }
    ///
    int NumCurves() const { return m_data.NumCurves(); }
    ///
    bool NeedsUpdating() const { return !m_bUpdated; }
    ///
    const CurveGroup &GetCurveGroup( const int in_iWhich ) const { return *m_data.m_aci[in_iWhich].m_opCG; }
    ///
    double GetTValue( const int in_iWhich ) const { return m_data.m_aci[in_iWhich].m_dT; }
    ///
    double GetTValue( const CurveGroup &in_grp ) const { return m_data.GetTValue(in_grp); }
    ///
    int GetPtIndex( const int in_iWhich ) const { return m_data.m_aci[in_iWhich].m_iPtIndex; }
    ///
    int GetPtIndex( const CurveGroup &in_grp ) const { return m_data.GetPtIndex(in_grp); }
    /// all of the curves are hidden
    bool IsHidden() const;
    ///
    bool HasCurveGroup( const CurveGroup &in_grp ) const { return m_data.HasCurveGroup(in_grp); }
    ///
    bool HadCurveGroupPast( const CurveGroup &in_grp, const int in_iTimeStamp ) const;
    ///
    double GetTValuePast( const CurveGroup &in_grp, const int in_iTimeStamp ) const;
    //@}
    
    /**@name Creating and deleting */
    //@{
    /// Add the curve group to the pin, or update the index and t values. 
    void AddCurveGroup( CurveGroup *in_opCG, const int in_iPtIndex, const double in_dT );
    /// Returns number of curves left
    int RemoveCurveGroup( CurveGroup *in_opCG );
    /// Essentially a delete
    void RemoveAllCurveGroups( );
    ///
    void SetPointConstraint( const R3Pt &in_pt );
    ///
    void SetNormalConstraint( const R3Vec &in_vec );
    ///
    void SetPointAndNormalConstraint( const R3Pt &in_pt, const R3Vec &in_vec );
    ///
    void ConvertToPointAndNormalConstraint( const R3Pt &in_ptCenter );
    /// Merge the two lists - don't reset the pt, though
    void Merge( const PointConstraint &in_pin );
    //@}

	/// Call when param file is re-read
	static void ParamFileUpdated();	
    
    /**@name For finding and editing. Can either update and propagate (in_bDelayed = false)
     * or update and assume someone's going to call Update() later. */
    //@{
	/// Returns 1e30 if not point constraint
	bool SelectPointConstraint( const R3Ray &in_ray, double &out_dDist, double &out_dZ ) const;
	/// Returns 1e30 if not point constraint
	bool SelectNormalConstraint( const R3Ray &in_ray, double &out_dDist, double &out_dZ ) const;
	/// Assumes in_vec is in in_plane
    void DragPointConstraint( const R3Vec &in_vec, const bool in_bDelayed );
    ///
    void EditNormal( const R3Vec &in_vecNewNorm, const bool in_bDelayed );
    ///
    void CurveMovedReprojectNormal( );
    /// For use after an undo. Just calls reestablish on the curve groups
    void ReEstablish() ;
    /// Call if in_bDelayed was true
    void DelayedUpdate();
    /// Call to update colors
    void SelectedRegionChanged();
    //@}

    /**@name For undoing */
    //@{
    /* Put the state back to the history entry with this time stamp. Re-establish all
     * constraints for valid curves. 
     * @param in_bChangePointNormal: true, change the point and the normal as well as the curve list
     * @param in_bDragCurves: true, drag the curves to the new point constraints. Otherwise,
     *        if it differs too much, don't add that curvegroup in
     */
    void Rollback( const int in_iTimeStampLast, 
                   const Array< CurveGroup * > &in_aopActiveCurves, 
                   const bool in_bChangePointNormal, const bool in_bDragCurves );
    ///@}
    
    /// Call to set update variables based on adjacent curve update values
    void PropagateUpdateFromCurves();
    ///
    void ForceUpdateDrawData() { m_drawData.ForceUpdate(); }
    ///
    void UpdateDrawData() { m_drawData.Update(); }
    ///
    void DrawPoint( const bool in_bIsSelected ) const;
    ///
    void DrawNormal( const bool in_bIsSelected ) const;
    ///
    void Draw() const { m_drawData.Draw(); }
    ///
    void DrawShadow() const { m_drawData.DrawShadow(); }
    /// Get geometry for point constraint
    void SphereGeometry( std::vector<R3Pt> &io_aptVs, std::vector<R3Vec> &io_avecVNs, std::vector< R3Pt_i > &io_aptFs ) const;
    
    static std::string GetTypeString( const ConstraintType in_type );
    static std::string GetActionString( const ActionType in_action );
    
    bool operator== ( const PointConstraint &in_pin ) const { return in_pin.m_iHashId == m_iHashId; }
    PointConstraint( );
    ~PointConstraint();

    /// Check consistency of constraints
    void CheckConsistency() const;
    
    void Write( ofstream &out, const Array<int> &in_aiMapId ) const;
    void Read( ifstream &in, const Array< CurveGroup * > &in_aopCrv ) ;
    void Print() const;
    void PrintHistory() const;
    
};

#endif
