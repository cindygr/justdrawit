/*
 *  CurveNetwork_Draw.cpp
 *  SketchCurves
 *
 *  Created by Cindy Grimm on 9/23/11.
 *  Copyright 2011 Washington University in St. Louis. All rights reserved.
 *
 */


#include "CurveNetwork.h"
#include "PointConstraint.h"

void CurveNetwork::DrawPointConstraints( const PointConstraint *in_opSel ) const
{
    g_drawState.StartShader( DrawState::SURFACE_SHADING );	
    glEnable( GL_CULL_FACE );
    
    for ( int i = 0; i < NumConstraints(); i++ ) {
        if ( m_aopPins[i]->IsPoint() ) {
            m_aopPins[i]->DrawPoint( m_aopPins[i] == in_opSel );
        }
    }
    
    g_drawState.EndShader();
}

///
void CurveNetwork::DrawNormalConstraints( const PointConstraint *in_opSel ) const
{
    g_drawState.StartShader( DrawState::SURFACE_SHADING );	
    glEnable( GL_CULL_FACE );
    
    for ( int i = 0; i < NumConstraints(); i++ ) {
        if ( m_aopPins[i]->IsNormal() ) {
            m_aopPins[i]->DrawNormal( m_aopPins[i] == in_opSel );
        }
    }
    
    g_drawState.EndShader();
}

void CurveNetwork::UpdateDrawData() 
{
    for ( int i = 0; i < NumConstraints(); i++ ) {
        if ( m_aopPins[i]->IsPoint() ) {
            m_aopPins[i]->PropagateUpdateFromCurves();
        }
    }
    
    for ( int i = 0; i < NumCrvs(); i++ ) {
        SetCurveGroup(i).UpdateDrawData();
    }
    for ( int i = 0; i < NumConstraints(); i++ ) {
        if ( m_aopPins[i]->IsPoint() ) {
            m_aopPins[i]->UpdateDrawData();
        }
    }
}

