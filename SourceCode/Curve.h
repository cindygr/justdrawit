/*
 *  Curve.h
 *  SketchCurves
 *
 *  Created by Cindy Grimm on 8/4/10.
 *  Copyright 2010 Washington University in St. Louis. All rights reserved.
 *
 * Thoughts
 * 1) use ratio of distance to speed to control filtering/sampling
 * 2) add an explicit corner detector
 *   - use to break bi laplacian fitting
 *
 */

#ifndef _CURV_DEFS_H_
#define _CURV_DEFS_H_

#include <utils/Rn_Defs.H>
#include <utils/R3_Plane.H>
#include <utils/Utils_Color.H>
#include <boost/dynamic_bitset.hpp>

#include "DrawState.h"
#include "ShadowBox.h"
#include "CurveDrawData.h"

class PointConstraint;

/*
 * 3D Curve class
 *
 * Because this was getting too slow, I moved to doing an explicit secondary
 * data update rather than updating the data every time AddPoint is called. 
 * This means that you need to call SetAllData to actually update things. The update
 * has three pieces that you might want to do independently:
 *   Update the arc-length parameterization/bounding box
 *   Update the tangents
 *
 * Some of the data needed to draw the curves (tubes, ribbons) is not
 * light-weight to calculate. So it's been moved to the CurveDrawData class,
 * which has a has-things-changed update mechanism for the camera/colors. For
 * the geometric data, you're expected to explicitly update the geometry
 * whenever it's changed. 
 *   
 * Sept 2011: 
 *  Moved all of the draw data/functions into the CurveDraw class
 *  Moved all of the point and normal constraints into the CurveGroup class
 *
 */
class Curve {
protected:
    // Primary data - just a list of points
    Array<R3Pt> m_apt;

    // Derived tangents and normals
    Array<R3Vec> m_avecTangent;
    
    // Derived bounding box. Calculated by SetAllData
    std::pair<R3Pt,R3Pt> m_bbox;
    
    // m_adDist[i] is the distance between point i and i+1
    // m_adDist.last() is the length of the curve
    // m_adPercAlong[i] is the percentage along the curve (in range 0..1)
    Array<double> m_adDist, m_adPercAlong;

    // Have these been updated?
    bool m_bPercData;    // m_adDist, m_adPercAlong, m_bbox
	bool m_bTangentData; // m_avecTangent, m_avecNormal, m_drawData geometry data
    
    /// Different drag options. returns offsets to points, but doesn't move them. Currently uses the Cubic one
	/// In Curve_Drag
    Array<R3Vec> Drag2DLaplacian( const R3Vec &in_vec, const double in_dTDrag, const std::pair<double,double> &in_dTBracket, const bool in_bIsClosed ) const;
    Array<R3Vec> Drag2DQuadraticFalloff( const R3Vec &in_vec, const double in_dTDrag, const std::pair<double,double> &in_dTBracket ) const;
    Array<R3Vec> Drag2DCubicFalloff( const R3Vec &in_vec, const double in_dTDrag, const std::pair<double,double> &in_dTBracket ) const;
	
    /// For internal use only
    void DeleteLastPoint();
    
	// actually calculate the bounding box. In Curve_Properties. Called in SetDistanceData
    const std::pair<R3Pt,R3Pt> CalcBoundingBox() const;

    /// For re-establishing pins
    std::pair<double,double> BracketTs( const Array< PointConstraint * > &in_aopPins, const int in_iWhich ) const;
    
public:
	/**@name Accessors */
	//@{
	///
    int NumPts() const { return m_apt.num(); }
	/// If is closed, then allow wrap
    const R3Pt &Pt( const int in_i ) const { return m_apt[ IndexWrap(in_i) ]; }
	/// If is closed, then allow wrap. Otherwise, clamp to 0,n-1
    const R3Pt &PtClamp( const int in_i ) const { return m_apt[ IndexClamp(in_i) ]; }
	///
    const Array<R3Pt> &Pts() const { return m_apt; }
	/// The calculated tangent
	const R3Vec &GetTangentAtPoint( const int in_i ) const { return m_avecTangent[ IndexWrap(in_i) ]; };
	/// The calculated tangent
	const R3Vec &GetTangentAtPointClamp( const int in_i ) const { return m_avecTangent[ IndexClamp(in_i) ]; };
	/// Evaluate at the t value in_dT in [0,1] (linear interpolation). If curve is closed, shift t into 0,1 first
    R3Pt operator()( const double in_dT ) const;
	/// Evaluate at the t value in_dT in [0,1] (linear interpolation). If curve is closed, shift t into 0,1 first
	R3Vec Tangent( const double in_dT ) const;
	///
	const std::pair<R3Pt,R3Pt> &BoundingBox() const { return m_bbox; }
    /// Returns the t value of the point in [0,1]
    double PercAlong( const int in_iPt ) const { return m_adPercAlong[in_iPt]; }
    /// Returns the t value of the point in [-1,2]. If i pt < 0, add -1, if iPt > numpts, add 1
    double PercAlongWrap( const int in_iPt ) const;
    /// Same as above, for closed curves, but clamps to [0,1] for open curves
    double PercAlongClamp( const int in_iPt ) const { return m_adPercAlong[ IndexClamp(in_iPt) ]; }
    /// Returns index in [0,npts-1]. If closed, will map index to [0,npts-1]
    int IndexWrap( const int in_iPt ) const;
    /// Returns index in [0,npts-1]. If closed, will map index to [0,npts-1]. If not closed, will clamp
    int IndexClamp( const int in_iPt ) const;
    /// Returns t value in [0,1]. If closed, will map t < 0 to t+1 and t >1 to t-1
    double TWrap( const double in_dT ) const;
	// Distance from segment i to i+1
    double DistanceSegment( const int in_iPt ) const { return m_adDist[ IndexWrap(in_iPt) ]; }
	//@}

	/**@name Searching/closest point. In Curve_Search */
	//@{
	/// Returns the segment (i, i+1) and percentage along that segment [0.0,1.0) for the given input t value in_dT in [0,1]
	std::pair<int,double> BinarySearch(const double in_dT) const;
	/// Returns the point index that is less than or equal to in_dT (in [0,.., NumPts() - 1])
	int Floor(const double in_dT) const;
	/// Returns the point index that is greater than or equal to in_dT (in [0,.., NumPts()])
	int Ceil(const double in_dT) const;
    
	/// if t < 0, returns Floor( in_dT + 1.0 ) - NumPts(). If t > 1.0, returns Floor( in_dT - 1.0 ) + NumPts()
	int FloorWrap(const double in_dT) const;
	/// if t < 0, returns Ceil( in_dT + 1.0 ) - NumPts(). If t > 1.0, returns Ceil( in_dT - 1.0 ) + NumPts()
	int CeilWrap(const double in_dT) const;
    /// if t < 0, returns ClosestIndex( in_dT + 1.0 ) - NumPts(). If t > 1.0, returns ClosestIndex( in_dT - 1.0 ) + NumPts()
    int ClosestIndexWrap( const double in_dT ) const;
    
    /// Returns the closest point index (in [0,...NumPts()-1])
    int ClosestIndex( const double in_dT ) const;
	/// Returns the segment (i, i+1) and percentage along that segment [0.0,1.0) for the closest point
    std::pair<int, double> ClosestPointSegLocation( const R3Pt &in_pt, double &out_dT, R3Pt &out_ptClosest ) const;
    /// Return t value, and pass back distance and closest point in out_dDist and out_ptClosest
    double ClosestPointTValue( const R3Pt &in_pt, double &out_dDist, R3Pt &out_ptClosest ) const;
    /// Return t value
    double ClosestPointTValue( const R3Pt &in_pt ) const;
	/// Find the closest point to the ray. in_bSearchClose means look at nearby t values first
    R3Pt ClosestPoint( const R3Ray &in_ray, double &out_dT, double &out_dDist, const bool in_bSearchClose = false ) const;
    // return the distance to the closest point and t value in out_dT (wrapper around ClosestPoint)
    double ClosestPointDistance( const R3Ray &in_ray, double &out_dT ) const;
    /// Returns average and maximum distance from in_crv to this curve (one-sided)
    pair<double,double> DistanceToCurve( const Curve &in_crv ) const;
	//@}
	
	/**@name Operations on the curve that actually change the curve. In Curve_Operations */
	//@{
	///
    virtual void Clear();
    /// Primary method for creating curve. Call SetAllData when done
    void AddPoint( const R3Pt &in_pt );
    /// Make sure PercAlong and bounding box are set
    void SetDistanceData();
    /// Set tangents and normals for the curve. Expensive. Set in_bDoNormals to false if there are normal constraints
    void SetTangentData( );
    /// Call this when done with AddPoint or after editing.
    virtual void SetAllData() ;
    /// Return left half of curve, if any. Operates on the curve in place. start and stop should be in (0,1)
    Curve Erase( const double in_dTStart, const double in_dTEnd );
	/// Force a curve closed by dragging the ends together
	void Close( const double in_dTBracketDragStart = 0.5, const double in_dTBracketDragEnd = 0.5 );
	/// Legacy. Fills any gaps in the curve that are this size. Original points are kept.
    void FillGaps( const double in_dGapSize );
	/// Average the points, keeping end points the same
    void SmoothPoints( const double in_dTStart = 0.0, const double in_dTEnd = 1.0, const int in_iNLoops = 1 );
	/// Average the points and the tangents/normals, keeping end points the same
    void SmoothTangents( const double in_dTStart = 0.0, const double in_dTEnd = 1.0, const int in_iNLoops = 1 );
    /// Insert a point in the segment, if necessary. 
    virtual int SplitSegment( const double in_dT, const double in_dPercScreen = 0.0 );
    ///
    Curve MakePolygon() const;
	//@}
	
	/**@name Affine operations applied to the curve. In Curve_Drag */
	//@{
	/// Drag the entire curve
    void Drag( const R3Vec &in_vec ); // Shift curve
	/// Drag just the selected part. Assumes in_vec is in in_plane
    void Drag( const R3Plane &in_plane, const R3Vec &in_vec, const double in_dTDrag, const std::pair<double,double> &in_dTBracket, const bool in_bUseLaplacian = false );
	/// Transform the entire curve by some combination of rotation, translation, scale
    void Transform( const R4Matrix &in_mat );
	//@}
	
	/**@name Properties of the curve */
	//@{
	/// Behaves correctly even if distance data is not set
	double Length() const;
	/// Average of all the curve points
    R3Pt  CenterPoint() const;
	/// Returns a line if this is actually linear, otherwise normal of plane
	bool IsPlanar( R3Vec &out_vecLine, R3Vec &out_vecNormal ) const;
	/// First point same as last?
	bool IsClosed() const;
	/// Average segment length. Behaves correctly even if distance data not set
    double AverageSegLength() const;	
	/// Static method
    static double Curvature( const R3Pt &in_ptPrev, const R3Pt &in_pt, const R3Pt &in_ptNext ) ;
	/// Curvature at a point
    double Curvature( const int in_iIndex ) const;
	/// Average curvature across curve
    double AverageCurvature() const;
	/// Average the tangents along the curve
    R3Vec AverageTangent() const;
	/// Find the corners (legacy)
    boost::dynamic_bitset<> Corners( const double in_dDist ) const;
	//@}        

	/**@name Resample, reflect, reverse. All of these return a curve. In Curve.cpp */
	//@{	
	/// copy curve
    const Curve &operator=( const Curve &in_crv );
    /// Percentage of average segment distance. If in_dPercOfAvg < 1.0, more points, > 1.0, fewer points
    Curve ResampleRelativeSize( const double in_dPercOfAvg ) const;    
	/// Use absolute size
    Curve ResampleAbsoluteSize( const double in_dAbsoluteSize ) const;    
	/// Reflect the curve around the plane
    Curve Reflect( const R3Plane &in_plane ) const;
	/// Reverse the order of the curve
	Curve Reverse( ) const;
	/// Note: this preserves the original distances
    Curve Project( const R3Plane &in_plane ) const;
    // Note: This preserves the *original* curvature/perc along, etc
    Curve ProjectAndRotate( const R3Plane &in_plane ) const;
	//@}
    
    Curve( const Curve &in_crv ) { *this = in_crv; }
    Curve() { Clear(); }
    virtual ~Curve() { }
    
    virtual void Write( ofstream &out ) const;
    virtual void Read( ifstream &in );
	// Legacy read for Karan Singh's ILoveSketch files
    void ReadKaran( ifstream &in );
    void ReadSimple( ifstream &in );
    void ReadPatchMaker( ifstream &in );
};

#endif
