/*
 *  CurveGroup_Edit.cpp
 *  SketchCurves
 *
 *  Created by Cindy Grimm on 5/19/11 (copied from CurveGroup.cpp).
 *  Copyright 2011 Adobe. All rights reserved.
 *
 */


#include "CurveGroup.h"
#include "CurveGroupManager.h"
#include "PointConstraint.h"
#include "CurveNetwork.h"

/* Project stroke onto current drawing plane or view plane */
void CurveGroup::NewCurve( const ScreenCurve &in_curveTemp )
{
	m_aStrokeHistory.need(1);
	
    m_crvRibbon.SetToCurve( AddZValue( in_curveTemp ).ResampleAbsoluteSize( g_drawState.CurveSpacingSize() )  );
	m_aStrokeHistory.last().StartNew( m_crvRibbon );
	m_aStrokeHistory.last().SetConstructionHistory( in_curveTemp, g_drawState.DrawingPlane() );

    GeometryChanged( m_aStrokeHistory.last().GetMergeType() );
}

/*
 * Snap the new curve closed if need be
 * Then add as above
 */
void CurveGroup::NewClosedCurve( const CurveOverlap &in_crvOverlap, const ScreenCurve &in_curveTemp )
{
	m_aStrokeHistory.need(1);
	
	m_aStrokeHistory.last().StartNewClosed( in_crvOverlap, AddZValue( in_curveTemp ), m_crvRibbon );	
	m_aStrokeHistory.last().SetConstructionHistory( in_curveTemp, g_drawState.DrawingPlane() );

    GeometryChanged( m_aStrokeHistory.last().GetMergeType() );
}

/*
 * In this case the 3D curve is handed to us, along with it's 2D projection
 */
void CurveGroup::NewCompositeCurve( const ScreenCurve &in_curveTemp, const Curve &in_curve )
{
	m_aStrokeHistory.need(1);
	
    m_crvRibbon.SetToCurve( in_curve );
	m_aStrokeHistory.last().StartNew( in_curve );
	
	m_aStrokeHistory.last().SetConstructionHistory( in_curveTemp, g_drawState.DrawingPlane() );
    
    GeometryChanged( m_aStrokeHistory.last().GetMergeType() );
}

/*
 * In this case the 3D curve is handed to us
 */
void CurveGroup::NewCompositeCurve( const Curve &in_curve )
{
    ScreenCurve crvScreen;
    for ( int i = 0; i < in_curve.NumPts(); i++ ) {
        crvScreen.AddPoint( g_drawState.GetCamera().CameraPt( in_curve.Pt(i) ), i );
    }

    NewCompositeCurve( crvScreen, in_curve );
}

/*
 * An erase can happen at the beginning, middle, or end of a curve. If at the middle,
 * we need to create a new curve to hold the remaining bit. Store the hash ids of the
 * original and new curve, as well as all of the pins that belonged to the original curve.
 *
 * For the new bit, copy the curve over and add all the pins. This makes the stroke history
 * identical (in terms of curve and pin constraints) to the original so that rollback will work properly
 */
CurveGroup *CurveGroup::Erase( const double in_dTStart, const double in_dTEnd )
{
    // Create a new curve group and set its first piece to be this curve
    CurveGroup *crvGroupNewPiece = g_CrvGrpManager.CreateCurveGroup();
    
    // Set up the new curve so that it has a first stroke and add constraints 
    crvGroupNewPiece->m_aStrokeHistory.add(1);
    crvGroupNewPiece->m_aStrokeHistory[0].StartNew( CompositeCurve() );
    crvGroupNewPiece->m_crvRibbon = m_crvRibbon;
    
    /// Add all the pins into the new piece so they;re there for a rollback. 
    Array< PointConstraint * > aopPinsMiddle;
    Array<int> aiPins( NumConstraints() );
    for ( int i = 0; i < NumConstraints(); i++ ) {
        double dT = GetConstraint(i).GetTValue( *this );
        if ( CompositeCurve().IsClosed() ) {
            if ( dT < in_dTStart && in_dTEnd > 1.0 ) {
                dT += 1.0;
            } else if ( dT > in_dTEnd && in_dTStart < 0.0 ) {
                dT -= 1.0;
            }
        }
        if ( dT > in_dTStart && dT < in_dTEnd ) {
            aopPinsMiddle += m_aopPins[i];
        }
        crvGroupNewPiece->AddConstraint( SetConstraint(i), dT, false );
        aiPins[i] = GetConstraint(i).HashId();
    }

    // Seperate, on the call stack, the erase from the curve creation in the new curve
    CurveNetwork::IncrementTime();
    
    // for the erase
	m_aStrokeHistory.add(1);
    crvGroupNewPiece->m_aStrokeHistory.add(1);
    
    // When we come back, the lhs of the curve (if any) will be in this curve group,
    // the remainder in the new curve group
	m_aStrokeHistory.last().Erase( in_dTStart, in_dTEnd, 
                                   HashId(), crvGroupNewPiece->HashId(),
                                   aiPins,
                                   m_crvRibbon, 
                                   crvGroupNewPiece->m_aStrokeHistory.last(), crvGroupNewPiece->m_crvRibbon );
	
	m_bDoFade = true;
    crvGroupNewPiece->m_bDoFade = true;
	
    // Remove constraints from middle piece. 
    for ( int i = 0; i < aopPinsMiddle.num(); i++ ) {
        aopPinsMiddle[i]->RemoveCurveGroup( this );
        aopPinsMiddle[i]->RemoveCurveGroup( crvGroupNewPiece );
        // If this is a pin constraint and only one curve left, get rid of it
        if ( aopPinsMiddle[i]->NumCurves() == 1 && !aopPinsMiddle[i]->IsNormal() ) {
            aopPinsMiddle[i]->RemoveAllCurveGroups();
        }
    }
    
    /// Propagate edit will delete constraints that are not valid
    GeometryChanged( StrokeHistory::ERASE );
    crvGroupNewPiece->GeometryChanged( StrokeHistory::ERASE );
    
    return crvGroupNewPiece;
}

/*
 * Dragging from current position to new one
 * Store the un-dragged curve in new stroke history
 */
void CurveGroup::StartTransform()
{
	
	m_aStrokeHistory.add(1);
	m_aStrokeHistory.last().StartTransform( m_crvRibbon );
	
	m_bDoFade = true;
	ScreenCurve crv;
	m_aStrokeHistory.last().SetConstructionHistory( crv, g_drawState.DrawingPlane() );
}

/* 
 * We're dragging from mouse down to here. Use saved curve in stroke history as restart point.
 * Fix the drag t value if it lies outside of the bracket t values
 */
void CurveGroup::Drag( const R3Plane &in_plane, const R3Vec &in_vec, const double in_dTDrag, const std::pair<double, double> & in_dTBracket )
{
    double dTDrag = in_dTDrag;
    if ( CompositeCurve().IsClosed() ) {
        if ( dTDrag < in_dTBracket.first ) {
            dTDrag += 1.0;
        } else if ( dTDrag > in_dTBracket.second ) {
            dTDrag -= 1.0;
        }
    }
    
	m_aStrokeHistory.last().Drag( in_plane, in_vec, dTDrag, in_dTBracket, m_crvRibbon );
    GeometryChanged(  m_aStrokeHistory.last().GetMergeType() );
}

/* Do actual transform */
void CurveGroup::Transform( const R4Matrix &in_mat, const bool in_bReset )
{
	m_aStrokeHistory.last().Transform( in_mat, m_crvRibbon, in_bReset );
    
    GeometryChanged( m_aStrokeHistory.last().GetMergeType() );
}


void CurveGroup::Smooth( const double in_dTStart, const double in_dTEnd, const int in_iNLoops )
{
	
    if ( m_aStrokeHistory.num() && LastAction().GetMergeType() != StrokeHistory::SMOOTH ) {
        m_aStrokeHistory.add(1);
        m_aStrokeHistory.last().Smooth( in_dTStart, in_dTEnd, in_iNLoops, m_crvRibbon );
    } else {    
        SetLastAction().Smooth( in_dTStart, in_dTEnd, in_iNLoops, m_crvRibbon );
    }

    GeometryChanged( StrokeHistory::SMOOTH );
}

/* Use the t values stored in the last smooth action to repeat the smooth */
void CurveGroup::RepeatSmooth()
{
    if ( LastAction().GetMergeType() == StrokeHistory::SMOOTH ) {
        SetLastAction().RepeatSmooth( m_crvRibbon );
        
        GeometryChanged(  m_aStrokeHistory.last().GetMergeType() );
    } else if ( LastAction().WasStroke() ) {
		// t values from join saved
        SetLastAction().RepeatSmooth( m_crvRibbon );
	} else {
		cerr << "ERR: Curve Group, repeat smooth of empty curve\n";
    }
}

/* Undo the last action
 *   Note: This gets treated as if it's a new action, in the sense that changes get propagated
 * to other curves.
 *
 * 1) Transforms, smooths: return to previous curve, pop one off of stack, no stroke out
 * 2) Join, merge, 
 */
StrokeHistory CurveGroup::UndoLastAction( )
{
    static StrokeHistory sh;

    if ( m_aStrokeHistory.num() == 0 ) {
        cerr << "ERR: CurveGroup::UndoLastAction, empty stack!\n";
        return sh;
    }
 
    // iUndoLevel is the stroke history we're currently at
    int iUndoLevel = m_aStrokeHistory.num() - 1;
    for ( int i = m_aStrokeHistory.num() - 1; i >= 0; i-- ) {
        if ( i > iUndoLevel ) {
            continue;
        }
        
        // See if we have a remove/add due to an undo in the stack history
        if ( i > 0 && m_aStrokeHistory[i].GetTimeStamp() == m_aStrokeHistory[i-1].GetTimeStamp() ) {
            iUndoLevel = i - 1;
            continue;
        }
        if ( m_aStrokeHistory[i].GetMergeType() == StrokeHistory::UNDO ) {
            iUndoLevel = m_aStrokeHistory[i].UndoLevel();
            continue;
        }
        break;
    }
    
    // Previous level
    const int iCurLevel = iUndoLevel;
    iUndoLevel--;
    while ( iUndoLevel > 0 && m_aStrokeHistory[iUndoLevel].GetTimeStamp() == m_aStrokeHistory[iUndoLevel-1].GetTimeStamp() ) {
        iUndoLevel--;
    }
    
    if ( m_aStrokeHistory.last().GetMergeType() != StrokeHistory::UNDO ) {
        m_aStrokeHistory.add(1);
    }
    
    if ( iUndoLevel < 0 ) {
        m_aStrokeHistory.last().Undo( iUndoLevel, sh.Final() );
        m_crvRibbon.SetToCurve( sh.Final() );
    } else {
        m_aStrokeHistory.last().Undo( iUndoLevel, m_aStrokeHistory[ iUndoLevel ].Final() );
        m_crvRibbon.SetToCurve( m_aStrokeHistory[ iUndoLevel ].Final() );
    }

    if ( iCurLevel < 0 ) {
        return sh;
    }
    
    return m_aStrokeHistory[iCurLevel];
}


void CurveGroup::Clear()
{
    m_aStrokeHistory.clearcompletely();
    RemoveCurveFromPointConstraints();
}

void CurveGroup::UpdateReflection()
{
    if ( !IsReflected() ) {
        return;
    }
    
    // TODO: Reset refelected curve geometry
    m_crvDrawReflect.GeometryChanged();
}

void CurveGroup::GeometryChanged( const StrokeHistory::MergeType in_action )
{
    Array<int> s_aiPins;
    Array<R3Vec> s_avec;
    
    if ( m_crvRibbon.NumPts() == 0 ) {
        return;
    }
    
    PropagateEdit(in_action);
    
    s_aiPins.need(0);
    s_avec.need(0);
    for ( int i = 0; i < NumConstraints(); i++ ) {
        if ( GetConstraint(i).IsNormal() ) {
            s_aiPins += GetConstraint(i).GetPtIndex( *this );
            s_avec += GetConstraint(i).GetNormal();
        }
    }
    
    m_crvRibbon.SetNormalData( s_avec, s_aiPins );
    m_crvDraw.GeometryChanged();
    
    if ( IsReflected() ) {
        UpdateReflection();
    }
}

