/*
 *  Curve_Search.cpp
 *  SketchCurves
 *
 *  Created by Cindy Grimm on 5/19/11. Copied from Curve.cpp
 *  Copyright 2010 Washington University. All rights reserved.
 *
 */


#include "Curve.h"
#include <utils/Rn_Line_seg.H>

const bool s_bTrace = false;

/*
 * Figures out which segment the t value lies in
 * in_dT is in [0,1]
 * returns the segment (pt i to pt i+1) and the
 *  distance along that segment [0,1)
 */
std::pair<int,double> Curve::BinarySearch(const double in_dT) const
{
	if ( m_apt.num() < 1 ) return std::pair<int,double>( 0, 0.0 );
	
    const double dT = TWrap( in_dT );
    
	int iLow = 0, iHigh = m_apt.num() - 1;
	int iMid = (int) ( 0.5 * (iHigh + iLow) + 0.5 );
	
	while ( iHigh - iLow > 1 ) {
		const double dPercMid = PercAlong(iMid);
		if ( RNApproxEqual( dPercMid, dT ) ) {
			return std::pair<int,double>( iMid, 0.0 );
		} else if ( dPercMid < dT ) {
			iLow = iMid;
		} else if ( dPercMid > dT ) {
			iHigh = iMid;
		} else {
            ASSERT(FALSE);
        }
        iMid = (int) ( 0.5 * (iHigh + iLow) + 0.5 );
        if ( iMid == iHigh || iMid == iLow ) {
            ASSERT( iHigh - iLow < 2 );
        }
    }
    const double dPercLow = PercAlong(iLow);
    const double dPercHigh = PercAlong(iHigh);
	
    const double dPerc = (dT - dPercLow) / (dPercHigh - dPercLow);
    return std::pair<int,double>(iLow, dPerc );
}

int Curve::Floor(const double in_dT) const
{
    std::pair<int,double> ipt = BinarySearch(in_dT);
    
    if ( RNApproxEqual( ipt.second, 1.0 ) ) {
        return WINmin( ipt.first + 1, NumPts() - 1 );
    }
    
    return ipt.first;
}

int Curve::Ceil(const double in_dT) const
{
    std::pair<int,double> ipt = BinarySearch(in_dT);
    
    if ( RNApproxEqual( ipt.second, 1.0 ) ) {
        return WINmin( ipt.first + 2, NumPts() );
    }
    
    if ( RNApproxEqual( ipt.second, 0.0 ) ) {
        return ipt.first;
    }
    
    return ipt.first + 1;
}

/// if t < 0, returns Floor( in_dT + 1.0 ) - NumPts(). If t > 1.0, returns Floor( in_dT - 1.0 ) + NumPts()
int Curve::FloorWrap(const double in_dT) const
{
    if ( in_dT < 0.0 && in_dT >= -1.0 ) {
        return Floor( in_dT + 1.0 ) - NumPts();
    } else if ( in_dT >= 0.0 && in_dT <= 1.0 ) {
        return Floor( in_dT );
    } else if ( in_dT > 1.0 && in_dT < 2.0 ) {
        return Floor( in_dT - 1.0 ) + NumPts();
    }
    cerr << "ERR: Curve::FloorWrap, t value out of range " << in_dT << "\n";
    return 0;
}

/// if t < 0, returns Ceil( in_dT + 1.0 ) - NumPts(). If t > 1.0, returns Ceil( in_dT - 1.0 ) + NumPts()
int Curve::CeilWrap(const double in_dT) const
{
    if ( in_dT < 0.0 && in_dT >= -1.0 ) {
        return Ceil( in_dT + 1.0 ) - NumPts();
    } else if ( in_dT >= 0.0 && in_dT <= 1.0 ) {
        return Ceil( in_dT );
    } else if ( in_dT > 1.0 && in_dT < 2.0 ) {
        return Ceil( in_dT - 1.0 ) + NumPts();
    }
    cerr << "ERR: Curve::CeilWrap, t value out of range " << in_dT << "\n";

    return 0;
}

/// if t < 0, returns Floor( in_dT + 1.0 ) - NumPts(). If t > 1.0, returns Floor( in_dT - 1.0 ) + NumPts()
int Curve::ClosestIndexWrap(const double in_dT) const
{
    if ( in_dT < 0.0 && in_dT >= -1.0 ) {
        return ClosestIndex( in_dT + 1.0 ) - NumPts();
    } else if ( in_dT >= 0.0 && in_dT <= 1.0 ) {
        return ClosestIndex( in_dT );
    } else if ( in_dT > 1.0 && in_dT < 2.0 ) {
        return ClosestIndex( in_dT - 1.0 ) + NumPts();
    }
    cerr << "ERR: Curve::ClosestIndexWrap, t value out of range " << in_dT << "\n";
    return 0;
}


int Curve::ClosestIndex( const double in_dT ) const
{
    std::pair<int,double> ipt = BinarySearch(in_dT);
    
    if ( ipt.second < 0.5 ) {
        return ipt.first;
    }
    
    return ipt.first + 1;
}

/*
 * Finds the closest point
 * returns the segment (pt i to pt i+1) and the
 *  distance along that segment [0,1) of the closest point
 *
 * out_dDist and out_ptClosest hold the distance to the closest point and the actual closest point
 */
std::pair<int, double> Curve::ClosestPointSegLocation( const R3Pt &in_pt, double &out_dDist, R3Pt &out_ptClosest ) const
{
    R3Line_seg seg;
    
    double dT, dDist;
    double dTBest = 0.0, dDistBest = 1e30;
    int iBest = 0;
    R3Pt ptClosest;
    for ( int i = 0; i < NumPts() - 1; i++ ) {
        seg = R3Line_seg( m_apt[i], m_apt[i+1] );
        seg.FindPtOnSeg( in_pt, ptClosest, dT, dDist );
        if ( dDist < dDistBest ) {
            dTBest = dT;
            iBest = i;
            dDistBest = dDist;
            out_ptClosest = ptClosest;
            out_dDist = dDistBest;
        }
    }
    
    return std::pair<int,double>( iBest, dTBest );
}

/* Find the t value of the closest point on the polyine
 * Also returns the distance and the closest point 
 */
double Curve::ClosestPointTValue( const R3Pt &in_pt, double &out_dDist, R3Pt &out_ptClosest ) const
{
    if ( NumPts() < 2 ) {
        if ( NumPts() == 0 ) {
            out_dDist = 1e30;
            out_ptClosest = R3Pt(1e30, 1e30, 1e30);
            return 0.0;
        }
        out_dDist = ::Length( in_pt - Pt(0) );
        out_ptClosest = Pt(0);
        return 0.0;
    }
    
    const std::pair<int,double> loc = ClosestPointSegLocation( in_pt, out_dDist, out_ptClosest );
    return PercAlong(loc.first) * (1.0 - loc.second) + PercAlong(loc.first+1) * loc.second;
}

/* Wrapper function */
double Curve::ClosestPointTValue( const R3Pt &in_pt ) const
{
    double dDist = 1e30;
    R3Pt ptClosest(0,0,0);
    
    return ClosestPointTValue(in_pt, dDist, ptClosest );
}


/* Return the actual point. Find closest point to the ray.
 * if SearchClose is true, then look along the curve first, rather than
 * searching the entire curve.
 * This correcly deals with curves that cross over themselves if the
 * user is mousing along the curve
 */
R3Pt Curve::ClosestPoint( const R3Ray &in_ray, double &out_dT, double &out_dDist, const bool in_bSearchClose ) const
{
    int iClosest = 0;
    const double dTLast = out_dT;
    out_dDist = 1e30;
    out_dT = 1e30;
    
    if ( m_apt.num() == 0 || RNIsZero( ::Length(in_ray.Dir() ) ) ) {
        return R3Pt(0,0,0);
    }
	
    const R3Line line( in_ray.Pt(), UnitSafe( in_ray.Dir() ) );
    
    const double dDistSel = g_drawState.SelectionSize();
	const double dTSel = 2.0 * dDistSel / Length();
    const double dTStart = dTLast - dTSel;
    const double dTEnd = dTLast + dTSel;
    
    double dT, dDist, dTLine = 0.0;
    R3Pt ptOut;
	
    if ( in_bSearchClose == true ) {
		for ( int i = 0; i < m_apt.num(); i++ ) {
			double dTAlong = PercAlong(i);
			if ( IsClosed() ) {
				if ( dTAlong < dTStart ) {
					dTAlong += 1.0;
				} else if ( dTAlong > dTEnd ) {
					dTAlong -= 1.0;
				}
			}
			if ( dTAlong >= dTStart && dTAlong <= dTEnd ) {
				line.FindPtOnLine( m_apt[i], ptOut, dT, dDist );
				
				if ( dDist < out_dDist && dT > 0.0 ) {
					
					iClosest = i;
					out_dDist = dDist;
					dTLine = dT;
					out_dT = dTAlong;
				}
			}
		}
		if ( out_dDist < 2.0 * dDistSel ) {
			return line(dTLine);
		}
		if ( s_bTrace ) cout << "Dist " << out_dDist << " " << dDistSel << " t " << out_dT << "\n";		
    }
    
    for ( int i = 0; i < m_apt.num(); i++ ) {
        line.FindPtOnLine( m_apt[i], ptOut, dT, dDist );
		
        if ( dDist <= out_dDist && dT > 0.0 ) {
			
            iClosest = i;
            out_dDist = dDist;
            dTLine = dT;
        }
    }
	out_dT = PercAlong(iClosest);
    return line(dTLine);
}

/* Wrapper function - returns the distance to the closest point on the ray (for selection) */
double Curve::ClosestPointDistance( const R3Ray &in_ray, double &out_dT ) const
{
    double dDist;
    ClosestPoint(in_ray, out_dT, dDist);
    return dDist;
}

pair<double,double> Curve::DistanceToCurve( const Curve &in_crv ) const
{
    pair<double,double> dRet( 0.0, 0.0 );
    double dDist = 0.0;
    R3Pt ptOut;
    
    if ( in_crv.NumPts() == 0 ) {
        return dRet;
    }
    
    for ( int i = 0; i < in_crv.NumPts(); i++ ) {
        ClosestPointTValue( in_crv.Pt(i), dDist, ptOut );
        dRet.first += dDist;
        dRet.second = WINmax( dRet.second, dDist );
    }
    dRet.first /= (double) in_crv.NumPts();
    
    return dRet;
}

