/*
 *  SurfaceData.cpp
 *  SketchCurves
 *
 *  Created by Cindy Grimm on 2/18/2011
 *  Copyright 2011 Adobe Corp. All rights reserved.
 *
 */

#include "SurfaceData.h"
#include "ParamFile.h"
#include "DrawState.h"
#include "UserInterface.h"

/* Not very fast - but search through the curve network list to find this curve */
int SurfaceData::IntersectionPoint::GetCurveGroupIndexFromHashId( const CurveNetwork &in_crvNetwork, const int in_iWhich ) const
{
    if ( in_iWhich < 0 || in_iWhich >= m_acrvLocs.size() ) {
        cerr << "ERR: SurfaceData::IntersectionPoint::GetCurveGroupIndexFromHashId bad index " << in_iWhich << "\n";
        return -1;
    }
    const int iHashId = m_acrvLocs[in_iWhich].first;
    for ( int iC = 0; iC < in_crvNetwork.NumCrvs(); iC++ ) {
        if ( in_crvNetwork.GetCurveGroup(iC).HashId() == iHashId ) {
            return iC;
        }
    }
    return -1;
}

/* Zero out all lists */
void SurfaceData::IntersectionPoint::Clear()
{
    m_bNeedsFixing = true;
    m_acrvLocs.resize(0);
    m_aptPerCurve.resize(0);
    m_dDistance = 1e30;
}

/* Copy */
SurfaceData::IntersectionPoint &SurfaceData::IntersectionPoint::operator=( const IntersectionPoint &in_pt )
{
	m_bNeedsFixing = in_pt.m_bNeedsFixing;
	m_pt = in_pt.m_pt;
	m_acrvLocs = in_pt.m_acrvLocs;
	m_aptPerCurve = in_pt.m_aptPerCurve;
	m_dDistance = in_pt.m_dDistance;
	
	return *this;
}

SurfaceData::IntersectionPoint::IntersectionPoint() 
{
    Clear();
}

SurfaceData::IntersectionPoint::~IntersectionPoint() 
{
    m_acrvLocs.resize(0);
    m_dDistance = 1e30;
}


/* Compare */
bool SurfaceData::IntersectionPoint::operator==( const IntersectionPoint &in_pt )
{
	if ( m_bNeedsFixing != in_pt.m_bNeedsFixing ) {
		return false;
	}
	
	if ( !ApproxEqual( m_pt, in_pt.m_pt ) ) {
		return false;
	}
	
	if ( !RNApproxEqual( m_dDistance, in_pt.m_dDistance ) ) {
		return false;
	}
	
	if ( m_acrvLocs.size() != in_pt.m_acrvLocs.size() ) {
		return false;
	}
	
	for ( int i = 0; i < m_acrvLocs.size(); i++ ) {
		if ( m_acrvLocs[i].first != in_pt.m_acrvLocs[i].first ) {
			return false;
		}
		if ( m_acrvLocs[i].second != in_pt.m_acrvLocs[i].second ) {
			return false;
		}
	}
	
	return true;
}

void SurfaceData::IntersectionPoint::Read(ifstream &in)
{
    std::string strTag, str;
    int iN = 0;
    in >> strTag >> str;
	assert( !strTag.compare("Fix") );
    if (!str.compare("t") ) {
        m_bNeedsFixing = true;
    } else if ( !str.compare("f") ) {
        m_bNeedsFixing = false;
    } else {
        cerr << "ERR: Reading Intersection point, unknown value for boolean " << str << "\n";
        return;
    }
	in >> strTag >> m_pt;	
	assert( !strTag.compare("Point") );
    
	in >> strTag >> m_dDistance;	
	assert( !strTag.compare("Distance") );
	
	in >> strTag >> iN;	
	assert( !strTag.compare("Curves") );
	
	m_acrvLocs.resize(iN);
	m_aptPerCurve.resize(iN);
    for ( int i = 0; i < iN; i++ ) {
		in >> strTag;	
		assert( !strTag.compare("Crv") );
        in >> m_acrvLocs[i].first >> m_acrvLocs[i].second >> m_aptPerCurve[i];
    }
}

void SurfaceData::IntersectionPoint::Write(ofstream &out) const
{
    out << "Fix " << ( m_bNeedsFixing ? "t " : " f " );
	out << "Point " << m_pt;
	out << "Distance " << m_dDistance;
	out << " Curves " << m_acrvLocs.size() << "\n";
    for ( int i = 0; i < m_acrvLocs.size(); i++ ) {
        out << " Crv " << m_acrvLocs[i].first << " " << m_acrvLocs[i].second << " " << m_aptPerCurve[i] << " ";
    }
}

void SurfaceData::IntersectionPoint::Print( const CurveNetwork &in_crvNetwork ) const
{
	if ( m_bNeedsFixing == true ) {
		cout << "Needs fixing ";
	} else {
		cout << "No fixing ";
	}
	cout.precision(4);
	cout << "Dist " << m_dDistance;
	cout << " Pt " << m_pt;
	cout << " Crvs " << m_acrvLocs.size() << "\n";
    for ( int i = 0; i < m_acrvLocs.size(); i++ ) {
        cout << "  " << m_acrvLocs[i].first << " t " << m_acrvLocs[i].second;
		if ( i < m_aptPerCurve.size() ) {
			if ( ApproxEqual(m_aptPerCurve[i], m_pt) ) {
				cout << " Pt same ";
			} else {
				cout << " " << m_aptPerCurve[i];
			}
		}
		const Curve &crv = in_crvNetwork.GetCurve( m_acrvLocs[i].first );
		if ( ApproxEqual( crv( m_acrvLocs[i].second ), m_pt ) ) {
			cout << "Eval same ";
		} else {
			cout << crv( m_acrvLocs[i].second );
		}
    }
	cout << "\n";
}

/* return the average point, provided still a valid intersection */
R3Pt SurfaceData::IntersectionPt( const int in_iIntersection ) const
{
	if ( in_iIntersection < 0 || in_iIntersection > m_aptIntersection.size() - 1 ) {
        cerr << "ERR: Surface data IntersectionPt, bad intersection " << in_iIntersection << "\n";
        return R3Pt(0,0,0);
    }
    
    return m_aptIntersection[ in_iIntersection ].m_pt;
}

/*
 * Look for closest intersection
 * Skip intersections that are not valid
 */
int SurfaceData::ClosestIntersection( const R3Ray &in_ray ) const
{
	if ( g_drawState.m_opUI && !g_drawState.m_opUI->m_bShowIntersections->value() ) {
		return -1;
	}
	
    if ( RNIsZero( ::Length( in_ray.Dir() ) ) ) {
        return -1;
    }
    
    const R3Line line( in_ray.Pt(), UnitSafe( in_ray.Dir() ) );
    
    double dT, dDist = 0.0;
    R3Pt ptOut;
    
    
    int iClosest = -1;
    double dClosest = 1e30;
    for ( int i = 0; i < m_aptIntersection.size(); i++ ) {
        line.FindPtOnLine( m_aptIntersection[i].m_pt, ptOut, dT, dDist );
        if ( dDist < dClosest ) {
            dClosest = dDist;
            iClosest = i;
        }
		for ( int j = 0; j < m_aptIntersection[i].m_aptPerCurve.size(); j++ ) {
			line.FindPtOnLine( m_aptIntersection[i].m_aptPerCurve[j], ptOut, dT, dDist );
			if ( dDist < dClosest ) {
				dClosest = dDist;
				iClosest = i;
			}
		}
    }
    if ( dClosest < g_drawState.SelectionSize() ) {
        return iClosest;
    }
    return -1;
}

SurfaceData &SurfaceData::operator=( const SurfaceData &in_srf )
{
	m_dDistance = in_srf. m_dDistance;
	m_aptIntersection = in_srf.m_aptIntersection;
	m_arayNormalConstraints = in_srf.m_arayNormalConstraints;
	m_arayTangentConstraints = in_srf.m_arayTangentConstraints;
	m_aptPointConstraints = in_srf.m_aptPointConstraints;
	
	m_ptMin = in_srf.m_ptMin;
	m_ptMax = in_srf.m_ptMax;

	m_aptSurface = in_srf.m_aptSurface;
	m_avecSurface = in_srf.m_avecSurface;
	m_afaceSurface = in_srf.m_afaceSurface;
	
	return *this;
}

void SurfaceData::Clear() 
{ 
	m_aptIntersection.resize(0); 
    m_arayNormalConstraints.resize(0); 
    m_arayTangentConstraints.resize(0); 
    m_aptPointConstraints.resize(0); 
	
	m_aptSurface.resize(0);
	m_avecSurface.resize(0);
	m_afaceSurface.resize(0);
}

void SurfaceData::RemoveInvalidAndFixed( const CurveNetwork &in_crvNetwork )
{
	std::vector<IntersectionPoint> aptKeep;
	for ( int i = 0; i < m_aptIntersection.size(); i++ ) {
		if ( m_aptIntersection[i].IsValid( in_crvNetwork, m_dDistance ) ) {
            m_aptIntersection[i].SetNeedsFixing( in_crvNetwork, m_dDistance );

            if ( m_aptIntersection[i].m_bNeedsFixing ) {
                aptKeep.push_back( m_aptIntersection[i] );
            }
		}		
	}
	m_aptIntersection = aptKeep;

	/*
	cout << "After\n";
	for ( int i = 0; i < m_aptIntersection.size(); i++ ) {
		m_aptIntersection[i].Print( in_crvNetwork );
	}
	 */
	
}

SurfaceData::SurfaceData()
: m_dDistance(0.1)
{
}

SurfaceData::~SurfaceData()
{
	Clear();
}

void SurfaceData::Read( ifstream &in )
{
    std::string str;
    in >> str;
    if ( !str.compare("SurfaceData") ) {
        int iN = 0;
        in >> m_dDistance >> iN;
        m_aptIntersection.resize(iN);
        for ( int i = 0; i < iN; i++ ) {
            m_aptIntersection[i].Read(in);
        }
        
    } else {
        Clear();
    }
    
    in >> str;
    if ( !str.compare("Mesh") ) {
        ReadSurface(in);
    }
}

void SurfaceData::Write(ofstream &out) const
{
    out << "SurfaceData " << m_dDistance << " " << m_aptIntersection.size() << "\n";
    for ( int i = 0; i < m_aptIntersection.size(); i++ ) {
        m_aptIntersection[i].Write(out);
    }
    out << "Mesh\n";
    WriteSurface( out );
}
