/*
 *  CurveDrawData.cpp
 *  SketchCurves
 *
 *  Created by Cindy Grimm on 3/8/11.
 *  Copyright 2011 Adobe Corp.. All rights reserved.
 *
 */

#include "CurveDrawData.h"
#include "DrawState.h"
#include "ParamFile.h"
#include "CurveRibbon.h"


void CurveDrawData::ForceUpdate() 
{ 
    DrawData::ForceUpdate(); 
    m_bUpdateView = m_bUpdateTube = m_bUpdateRibbon = m_bUpdateWebbingSplits = m_bUpdateWebbing = m_bUpdateShadow = true;
}

CurveDrawData &CurveDrawData::operator=( const CurveDrawData &in_data ) 
{
    cerr << "Warning: Should not be calling CurveDrawData::operator=\n";

    DrawData::operator=( in_data );
    
	m_dTOnCrv = in_data.m_dTOnCrv;
	m_afCol = in_data.m_afCol;
	m_adScale = in_data.m_adScale;
	m_aptTopStrip = in_data.m_aptTopStrip;
	m_aptBotStrip = in_data.m_aptBotStrip;
    for ( int i = 0; i < 3; i++ ) {
        m_aaptTopShadow[i] = in_data.m_aaptTopShadow[i];
        m_aaptBotShadow[i] = in_data.m_aaptBotShadow[i];
        m_aaptMidShadow[i] = in_data.m_aaptMidShadow[i];
    }
	m_aaptTubeStrip = in_data.m_aaptTubeStrip;
	m_aaptRibbonStrip = in_data.m_aaptRibbonStrip;
	m_aaptWebbingStrip = in_data.m_aaptWebbingStrip;
	m_aavecTubeStripNorm = in_data.m_aavecTubeStripNorm;
	m_aavecRibbonStripNorm = in_data.m_aavecRibbonStripNorm;
	m_aavecWebbingStripNorm = in_data.m_aavecWebbingStripNorm;
    m_aiWebbingSplits = in_data.m_aiWebbingSplits;
    m_aiWebbingDraw = in_data.m_aiWebbingDraw;
	m_bUpdateView = in_data.m_bUpdateView;
	m_bUpdateTube = in_data.m_bUpdateTube;
	m_bUpdateRibbon = in_data.m_bUpdateRibbon;
	m_bUpdateWebbing = in_data.m_bUpdateWebbing;
	m_bUpdateShadow = in_data.m_bUpdateShadow;
    m_bUpdateWebbingSplits = in_data.m_bUpdateWebbingSplits;
	
	return *this;
}

CurveDrawData::CurveDrawData( const CurveRibbon &in_crv )
: 
m_dTOnCrv(-1.0, -1.0 ),
m_crv( in_crv )
{
    ForceUpdate();
}

