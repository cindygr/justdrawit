/* Copyright 1994, Brown Computer Graphics Group.  All Rights Reserved. */

#ifndef _FITTING_FITTOLSCONSTRAINED_LS
#define _FITTING_FITTOLSCONSTRAINED_LS

#include <WINSystemDefines.H>
#include <utils/Mesh_Array.H>

/* ----------------------- Classes ----------------------------- */


/** \class FITTools_LSConstrained FITTools_LSConstrained.H fitting/FITTools_LSConstrained.H
  \brief Solve a least squares system of equations with both absolute and desired constraints
  \ingroup FittingMatrix

    Given an over-constrained set of constraints $Qx = c$.
    Given an under-determined set of absolute constraints $Ax = b$,
    find $x$ s.t. $Ax - b$ is minimized and $Qx=c$ is satisfied. <br>
   DETAILS   :	$A$ is $a X n$, $Q$ is $d X n$, $d < n$, $x$ is $n X m$ <br>

  Note that d must be less than n, i.e., the hard constraints must not out-number
  the number of variables.

<br>   Files: 
 - include/utils/Fitting_LSConstrained.H
 - src/utils/fitting/FITTools_LSConstrained.cpp */
class FITTools_LSConstrained  {
private:
    /**@name These should be const, but fortran wants int pointers <sigh>*/
    //@{
    int m_iNDesired;     ///< A = a X n where a is m_iNDesired (number of rows, soft constraints)
    int m_iNConstrained; ///< Q = d X n where d is m_iNConstrained (number of rows, hard constraints)
    int m_iNVariables;   ///< A = a X n where n is m_iNVariables (number of columns)
    int m_iNDimension;   ///< x = n X m where m is m_iNDimension, the number of columns in the solution vector
    
    /**@name Data storage */
    //@{
    Array<double> m_adDesiredData;         ///< The A matrix.
    Array<double> m_adConstrainedData;     ///< The Q matrix.
    Array<double> m_adSolutionData;        ///< The x matrix.
    Array<double> m_adRhsData;             ///< The b matrix.
    Array<double> m_adRhsConstrainedData;  ///< The c matrix.

    WINbool m_bRes;
    
public:
    /**@name Accessors */
    //@{
    /// Number of rows in A
    int N_desired()   const { return m_iNDesired; }
    /// Number of rows in Q
    int N_constrained()   const { return m_iNConstrained; }
    /// Number of columns in A
    int N_variables() const { return m_iNVariables; }
    /// Number of columns in b
    int N_dimension() const { return m_iNDimension; }
    /// x(r,c)
    const double &Solution(const int in_iRow, const int in_iCol) const;
    //@}

    /**@name Set matrix */
    //@{
    /// A(r,c)
    double & Desired(const int in_iRow, const int in_iCol);
    /// A(r,c)
    double & Desired_clapack(const int in_iRow, const int in_iCol) { return Desired( in_iRow, in_iCol ); }
    /// Q(r,c)
    double & Constrained(const int in_iRow, const int in_iCol);
    /// b(r,c)
    double &Rhs(const int in_iRow, const int in_iCol);
    /// b(r,c)
    double &Rhs_clapack(const int in_iRow, const int in_iCol) { return Rhs( in_iRow, in_iCol ); }
    /// b(r,c)
    double &RhsConstrained(const int in_iRow, const int in_iCol);
    ///
    void Clear();
    /// Ax = b, Subject to Qx = c, A = in_iDesired * in_iVariables, b = in_iDesired * in_iDimension. Q = in_iConstrained * in_iVariables
    void Resize(const int in_iDesired, const int in_iConstrained, const int in_iVariables, const int in_iDimension);
    //@}

    /**@name Solve */
    //@{
    ///
    WINbool Solve();
    //@}

    /**@name Debug utilities  Format: column number value... = rhs.
       Follows this with the sum of the coefficients multiplying each variable.*/
    //@{
    ///
    void   Print( ostream &, WINbool in_bDoTrace  );
    //@}

    
    /// Ax = b, Subject to Qx = c, A = in_iDesired * in_iVariables, b = in_iDesired * in_iDimension. Q = in_iConstrained * in_iVariables
    FITTools_LSConstrained (const int in_iDesired, const int in_iConstrained, const int in_iVariables, const int in_iDimension);
    ///
    ~FITTools_LSConstrained ();

    static void Test();
};

inline double &FITTools_LSConstrained::Desired(const int in_iRow, const int in_iCol) 
{
   ASSERT(in_iRow >= 0 && in_iRow < m_iNDesired);
   ASSERT(in_iCol >= 0 && in_iCol < m_iNVariables);
   return m_adDesiredData[in_iCol * m_iNDesired + in_iRow];
}

inline double &FITTools_LSConstrained::Constrained(const int in_iRow, const int in_iCol) 
{
   ASSERT(in_iRow >= 0 && in_iRow < m_iNConstrained);
   ASSERT(in_iCol >= 0 && in_iCol < m_iNVariables);
   return m_adConstrainedData[in_iCol * m_iNConstrained + in_iRow];
}

inline double &FITTools_LSConstrained::Rhs(const int in_iRow, const int in_iCol) 
{
   ASSERT(in_iRow >= 0 && in_iRow < m_iNDesired);
   ASSERT(in_iCol >= 0 && in_iCol < m_iNDimension);
   return m_adRhsData[in_iCol * m_iNDesired + in_iRow];
}

inline double &FITTools_LSConstrained::RhsConstrained(const int in_iRow, const int in_iCol) 
{
   ASSERT(in_iRow >= 0 && in_iRow < m_iNConstrained);
   ASSERT(in_iCol >= 0 && in_iCol < m_iNDimension);
   return m_adRhsConstrainedData[in_iCol * m_iNConstrained + in_iRow];
}

inline const double &FITTools_LSConstrained::Solution(const int in_iRow, const int in_iCol) const
{
   ASSERT(in_iRow >= 0 && in_iRow < m_iNVariables);
   ASSERT(in_iCol >= 0 && in_iCol < m_iNDimension);
   return m_adSolutionData[in_iCol * m_iNVariables + in_iRow];
}



/* -------------------------- Private Globals  ----------------------------- */

#endif
