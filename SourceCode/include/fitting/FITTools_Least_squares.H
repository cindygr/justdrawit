/* Copyright 1994, Brown Computer Graphics Group.  All Rights Reserved. */

#ifndef _FITTING_FITTOLS_LS
#define _FITTING_FITTOLS_LS

#include <WINSystemDefines.H>
#include <utils/Mesh_Array.H>

/* ----------------------- Classes ----------------------------- */


/** \class FITTools_LS FITTools_Least_squares.H fitting/FITTools_Least_squares.H

  \brief Solve a least squares system of equations.
  \ingroup FittingMatrix

    Given an under-determined set of absolute constraints $Ax = b$,
    find $x$ s.t. $Ax - b$ is minimized. <br>
   DETAILS   :	$A$ is $a X n$, $x$ is $n X m$ <br>

   There are two solvers underlying this, a numerical recipe SVD 
   FITToolsSVD (slower, but have source code) and Intel's mkl solver. 
   Since FITToolsSVD uses row column order and LAPACK is column
   order there are different accessors for the two different
   types. LAPACK is defined for both WIN32 and the SUNS; I don't know
   yet about the SGI. The slower version is _utils, the spiffy faster is _lapack. 

  This has now been moved from the Lapack libraries to Intel's mkl libraries. 

  However, only the Intel libraries support sparse matrices.

  To use sparse matrices, the rows must be added in order. However, the columns
  are sorted for you. Rows are added one at a time.

  For all approaches, must allocate (via Resize or constructor) memory first.

  Note that the A matrix data is overwritten when Solve is called. So calling Print after calling
  Solve will result in garbage.

<br>   Files: 
 - include/utils/Fitting_Least_squares.H
 - src/utils/fitting/FITTools_LS.cpp */
class FITTools_LS  {
private:
    /**@name These should be const, but fortran wants int pointers <sigh> */
    //@{
    int m_iNDesired;    ///< A = a X n where a is m_iNDesired (number of rows)
    int m_iNVariables;  ///< A = a X n where n is m_iNVariables (number of columns)
    int m_iNDimension;  ///< x = n X m where m is m_iNDimension, the number of columns in the solution vector
    //@}

    /**@name Data storage */
    //@{
    Array<double> m_adDesiredData;   ///< The A matrix.
    Array<double> m_adSolutionData;  ///< The x matrix
    Array<double> m_adRhsData;       ///< The b matrix

    //@}

    WINbool m_bRes; ///< Did we get a valid solution?
    
public:
    /**@name Accessors */
    //@{
    /// Number of rows in A
    int N_desired()   const { return m_iNDesired; }
    /// Number of columns in A
    int N_variables() const { return m_iNVariables; }
    /// Number of columns in b
    int N_dimension() const { return m_iNDimension; }
    //@}

    /**@name Use lapack, non-sparse matrices (most common usage) */
    //@{
    /// A(r,c)
    double & Desired (const int in_iRow, const int in_iCol);
    /// b(r,c)
    double &Rhs      (const int in_iRow, const int in_iCol);
    /// Returns TRUE if successful
    WINbool Solve();
    /// x(r,c). Only valid after solve called
    const double &Solution  (const int in_iRow, const int in_iCol) const;
    //@}

    /**@name All matrix types */
    //@{
    /// Actually deletes data
    void Clear();
    /// Resizes data if necessary
    void Resize( const int in_iNDesired, const int in_iNVariables, const int in_iDimension );
    //@}

    /**@name Debug utilities 
       Only valid for FITToolsSvd version, except for print. Print only prints
       out the non-zero elements for each row. Format: column number value... = rhs.
       Follows this with the sum of the coefficients multiplying each variable. */
    //@{
    ///
    void   Print( ostream & );
    ///
    void   Check( const double dMinA, const double dMaxA, const double dMinR, const double dMaxR ) ;
    //@}

    
    /// Ax = b, A = in_iDesired * in_iVariables, b = in_iDesired * in_iDimension
    FITTools_LS (const int in_iDesired, const int in_iVariables, const int in_iDimension );
    ///
    ~FITTools_LS ();

    static void Test();
};


inline double &FITTools_LS::Desired(const int in_iRow, const int in_iCol) 
{
   ASSERT(in_iRow >= 0 && in_iRow < m_iNDesired);
   ASSERT(in_iCol >= 0 && in_iCol < m_iNVariables);
   return m_adDesiredData[in_iCol * m_iNDesired + in_iRow];
}

inline double &FITTools_LS::Rhs(const int in_iRow, const int in_iCol) 
{
   ASSERT(in_iRow >= 0 && in_iRow < m_iNDesired);
   ASSERT(in_iCol >= 0 && in_iCol < m_iNDimension);
   return m_adRhsData[in_iCol * m_iNDesired + in_iRow];
}

inline const double &FITTools_LS::Solution(const int in_iRow, const int in_iCol) const
{
   ASSERT(in_iRow >= 0 && in_iRow < m_iNVariables);
   ASSERT(in_iCol >= 0 && in_iCol < m_iNDimension);
   return m_adSolutionData[in_iCol * m_iNVariables + in_iRow];
}



/* -------------------------- Private Globals  ----------------------------- */

#endif
