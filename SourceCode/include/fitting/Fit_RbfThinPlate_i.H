// kernel: f(r) = (r / sigma)^2 log( r/sigma )
// r = (x-s)^2 + (y-t)^2
// a0 + a1 s + a2 t + sum wi (d(x,y - s,t) / sigmai)^2 log( d(x,y - s,t) / sigma )
//
template<class PtType, class VecType>
inline
PtType FITRbfThinPlateTC<PtType,VecType>::Eval( const R2Pt &in_pt ) const
{
    PtType ptRes;

    for ( int i = 0; i < ptRes.Dim(); i++ ) {
        ptRes[i] = this->m_aptPoly[0][i] + this->m_aptPoly[1][i] * in_pt[0] + this->m_aptPoly[2][i] * in_pt[1];
    }

    for ( FORINT i = 0; i < this->m_aptRad.num(); i++ ) {
        const double dDistSq = LengthSq( this->m_aptRad[i] - in_pt ) /  this->m_adSigmaSq[i];
        if ( RNIsZero( dDistSq ) )
            continue;

        const double dPhi = dDistSq * log( sqrt( dDistSq ) );
        const PtType &ptLambda = this->m_aptLambda[i];

        for ( int j = 0; j < ptRes.Dim(); j++ )
            ptRes[j] += ptLambda[j] * dPhi;
    }

    return ptRes;
}

// (r / sigma)^2 log( r/sigma )    deriv of log x = 1/x
//
// (r / sigma)^2 * 1/(r / sigma) + 2 (s-x) / sigma^2 log(r/sigma)
// (r / sigma) + 2 (s-x) / sigma^2 log(r/sigma)
//                               
// http://www.derivative-calculator.net/
// lg(sqrt(( (x-s)*(x-s)+(y-t)*(y-t) ) / g)) * ( (x-s)*(x-s)+(y-t)*(y-t) ) / g
//
// -2 * (x-s) * log( sqrt(r/sigma) ) / sigma - (x-s) / sigma
template<class PtType, class VecType>
inline
VecType FITRbfThinPlateTC<PtType,VecType>::DerivS( const R2Pt &in_pt ) const
{
    VecType vecRes;

    for ( int i = 0; i < vecRes.Dim(); i++ ) {
        vecRes[i] = this->m_aptPoly[1][i];
    }

    for ( FORINT i = 0; i < this->m_aptRad.num(); i++ ) {
        const double dXDist = ( this->m_aptRad[i][0] - in_pt[0] ) / this->m_adSigmaSq[i];
        // dDist == 0 implies dXDist == 0
        if ( RNIsZero( dXDist ) )
            continue;

        const double dDistSq = LengthSq( this->m_aptRad[i] - in_pt );
        const double dLog = log( sqrt(dDistSq /  this->m_adSigmaSq[i]) );
        const double dPhiDs = -2.0 * dXDist * dLog - dXDist;

        const PtType &ptLambda = this->m_aptLambda[i];
        for ( int j = 0; j < vecRes.Dim(); j++ )
            vecRes[j] += ptLambda[j] * dPhiDs;
    }

    return vecRes;
}

template<class PtType, class VecType>
inline
VecType FITRbfThinPlateTC<PtType,VecType>::DerivT( const R2Pt &in_pt ) const
{
    VecType vecRes;

    for ( int i = 0; i < vecRes.Dim(); i++ ) {
        vecRes[i] = this->m_aptPoly[2][i];
    }

    for ( FORINT i = 0; i < this->m_aptRad.num(); i++ ) {
        const double dYDist = ( this->m_aptRad[i][1] - in_pt[1] )  / this->m_adSigmaSq[i];

        // dDist == 0 implies dYDist == 0
        if ( RNIsZero( dYDist ) )
            continue;

        const double dDistSq = LengthSq( this->m_aptRad[i] - in_pt );
        const double dLog = log( sqrt(dDistSq /  this->m_adSigmaSq[i]) );
        const double dPhiDt = -2.0 * dYDist * dLog - dYDist /  this->m_adSigmaSq[i];

        const PtType &ptLambda = this->m_aptLambda[i];
        for ( int j = 0; j < vecRes.Dim(); j++ )
            vecRes[j] += ptLambda[j] * dPhiDt;
    }

    return vecRes;
}

//                         2                          2          2
//                2 (s - x)               Sqrt[(s - x)  + (t - y) ]
//        1 + ------------------- + 2 Log[-------------------------]
//                   2          2                   sigma
//            (s - x)  + (t - y)
//Out[9]= ----------------------------------------------------------
//                                       2
//                                 sigma
// http://www.derivative-calculator.net/
// lg(sqrt(( (x-s)*(x-s)+(y-t)*(y-t) ) / g)) * ( (x-s)*(x-s)+(y-t)*(y-t) ) / g
//
// -2 * (x-s) * log( sqrt(r/sigma) ) - (x-s) / sigma
// 2 * log( sqrt(r/sigma) ) / sigma + 2 * (x-s)^2 / ( sigma * (x-s)*(x-s)+(y-t)*(y-t) )    + 1/sigma
template<class PtType, class VecType>
inline
VecType FITRbfThinPlateTC<PtType,VecType>::DerivSS( const R2Pt &in_pt ) const
{
    VecType vecRes;

    RNZero( vecRes );
    for ( int i = 0; i < this->m_aptRad.num(); i++ ) {
        const double dXDist = this->m_aptRad[i][0] - in_pt[0];
        const double dDistSq = LengthSq( this->m_aptRad[i] - in_pt );

        if ( RNIsZero( dDistSq ) )
            continue;

        const double dLog = log( sqrt( dDistSq / this->m_adSigmaSq[i] ) );
        const double dPhiDss = ( 1.0 + 2 * dXDist * dXDist / dDistSq + 2.0 * dLog ) /  this->m_adSigmaSq[i];

        const PtType &ptLambda = this->m_aptLambda[i];
        for ( int j = 0; j < vecRes.Dim(); j++ )
            vecRes[j] += ptLambda[j] * dPhiDss;
    }
    return vecRes;
}

template<class PtType, class VecType>
inline
VecType FITRbfThinPlateTC<PtType,VecType>::DerivTT( const R2Pt &in_pt ) const
{
    VecType vecRes;

    RNZero( vecRes );

    for ( int i = 0; i < this->m_aptRad.num(); i++ ) {
        const double dYDist = in_pt[1] - this->m_aptRad[i][1];
        const double dDistSq = LengthSq( this->m_aptRad[i] - in_pt );

        if ( RNIsZero( dDistSq ) )
            continue;

        const double dLog = log( sqrt( dDistSq /  this->m_adSigmaSq[i] ) );
        const double dPhiDtt = ( 1.0 + 2 * dYDist * dYDist / dDistSq + 2.0 * dLog ) /  this->m_adSigmaSq[i];

        const PtType &ptLambda = this->m_aptLambda[i];
        for ( int j = 0; j < vecRes.Dim(); j++ )
            vecRes[j] += ptLambda[j] * dPhiDtt;
    }
    return vecRes;
}

//              2 (s - x) (t - y)
//Out[14]= ----------------------------
//              2         2          2
//         sigma  ((s - x)  + (t - y) )
template<class PtType, class VecType>
inline
VecType FITRbfThinPlateTC<PtType,VecType>::DerivST( const R2Pt &in_pt ) const
{
    VecType vecRes;

    RNZero( vecRes );

    for ( int i = 0; i < this->m_aptRad.num(); i++ ) {
        const double dXDist = in_pt[0] - this->m_aptRad[i][0];
        const double dYDist = in_pt[1] - this->m_aptRad[i][1];
        if ( RNIsZero( dXDist ) || RNIsZero( dYDist ))
            continue;

        const double dDistSq = dXDist * dXDist + dYDist * dYDist;
        const double dPhiDst = 2.0 * dXDist * dYDist / ( dDistSq *  this->m_adSigmaSq[i] );

        const PtType &ptLambda = this->m_aptLambda[i];
        for ( int j = 0; j < vecRes.Dim(); j++ )
            vecRes[j] += ptLambda[j] * dPhiDst;
    }
    return vecRes;
}

template<class PtType, class VecType>
inline
void FITRbfThinPlateTC<PtType,VecType>::AllEval( const R2Pt &in_pt,
                               PtType &out_pt,
                               VecType &out_vecDs, VecType &out_vecDt,
                               VecType &out_vecDss, VecType &out_vecDtt,
                               VecType &out_vecDst ) const
{
    for ( int i = 0; i < out_pt.Dim(); i++ ) {
        out_pt[i] = this->m_aptPoly[0][i] + this->m_aptPoly[1][i] * in_pt[0] + this->m_aptPoly[2][i] * in_pt[1];
        out_vecDs[i] = this->m_aptPoly[1][i];
        out_vecDt[i] = this->m_aptPoly[2][i];
        out_vecDss[i] = 0.0;
        out_vecDtt[i] = 0.0;
        out_vecDst[i] = 0.0;
    }

    for ( FORINT i = 0; i < this->m_aptRad.num(); i++ ) {
        const double dXDist = in_pt[0] - this->m_aptRad[i][0];
        const double dYDist = in_pt[1] - this->m_aptRad[i][1];
        const double dDistSq =  dXDist * dXDist + dYDist * dYDist;
        if ( RNIsZero( dDistSq ) )
            continue;

        const double dLog = log( sqrt(dDistSq /  this->m_adSigmaSq[i]) );
        const double dPhiDs = ( dXDist * (1.0 + 2.0 * dLog) ) /  this->m_adSigmaSq[i];
        const double dPhiDt = ( dYDist * (1.0 + 2.0 * dLog) ) /  this->m_adSigmaSq[i];

        const double dPhi = dDistSq * dLog /  this->m_adSigmaSq[i];

        const double dPhiDss = ( 1.0 + 2 * dXDist * dXDist / dDistSq + 2.0 * dLog ) /  this->m_adSigmaSq[i];
        const double dPhiDtt = ( 1.0 + 2 * dYDist * dYDist / dDistSq + 2.0 * dLog ) /  this->m_adSigmaSq[i];
        const double dPhiDst = 2.0 * dXDist * dYDist / ( dDistSq *  this->m_adSigmaSq[i] );
        
        const PtType &ptLambda = this->m_aptLambda[i];
        for ( int j = 0; j < out_pt.Dim(); j++ ) {
            out_pt[j] += ptLambda[j] * dPhi;
            out_vecDs[j] += ptLambda[j] * dPhiDs;
            out_vecDt[j] += ptLambda[j] * dPhiDt;
            out_vecDss[j] += ptLambda[j] * dPhiDss;
            out_vecDtt[j] += ptLambda[j] * dPhiDtt;
            out_vecDst[j] += ptLambda[j] * dPhiDst;
        }
    }
}

template<class PtType, class VecType>
inline
void FITRbfThinPlateTC<PtType,VecType>::AllEvalSub( const R2Pt &in_pt,
                                  PtType &out_pt,
                                  VecType &out_vecDs, VecType &out_vecDt ) const
{
    for ( int i = 0; i < out_pt.Dim(); i++ ) {
        out_pt[i] = this->m_aptPoly[0][i] + this->m_aptPoly[1][i] * in_pt[0] + this->m_aptPoly[2][i] * in_pt[1];
        out_vecDs[i] = this->m_aptPoly[1][i];
        out_vecDt[i] = this->m_aptPoly[2][i];
    }

    for ( FORINT i = 0; i < this->m_aptRad.num(); i++ ) {
        const double dXDist = in_pt[0] - this->m_aptRad[i][0];
        const double dYDist = in_pt[1] - this->m_aptRad[i][1];
        const double dDistSq =  dXDist * dXDist + dYDist * dYDist;
        if ( RNIsZero( dDistSq ) )
            continue;

        const double dLog = log( sqrt(dDistSq /  this->m_adSigmaSq[i]) );
        const double dPhiDs = ( dXDist * (1.0 + 2.0 * dLog) ) /  this->m_adSigmaSq[i];
        const double dPhiDt = ( dYDist * (1.0 + 2.0 * dLog) ) /  this->m_adSigmaSq[i];

        const double dPhi = dDistSq * dLog /  this->m_adSigmaSq[i];
        
        const PtType &ptLambda = this->m_aptLambda[i];
        for ( int j = 0; j < out_pt.Dim(); j++ ) {
            out_pt[j] += ptLambda[j] * dPhi;
            out_vecDs[j] += ptLambda[j] * dPhiDs;
            out_vecDt[j] += ptLambda[j] * dPhiDt;
        }
    }
}

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

///
template<class PtType, class VecType>
inline
FITRbfThinPlateTC<PtType,VecType> &FITRbfThinPlateTC<PtType,VecType>::operator=( const FITRbfThinPlateTC<PtType,VecType> &in_func )
{
    this->FITRbfTC<PtType,VecType>::operator=( in_func );
    this->m_adSigmaSq = in_func.m_adSigmaSq;

    return *this;
}

template<class PtType, class VecType>
inline
void FITRbfThinPlateTC<PtType,VecType>::Read( ifstream &in )
{
    FITRbfTC<PtType,VecType>::Read(in);
    this->m_adSigmaSq.need( this->m_aptRad.num() );

    for ( int i = 0; i <  this->m_adSigmaSq.num(); i++ ) {
        in >>  this->m_adSigmaSq[i];
    }
}

template<class PtType, class VecType>
inline
void FITRbfThinPlateTC<PtType,VecType>::Write( ofstream &out ) const
{
    FITRbfTC<PtType,VecType>::Write(out);

    for ( int i = 0; i <  this->m_adSigmaSq.num(); i++ )
        out <<  this->m_adSigmaSq[i] << " ";

    out << "\n";
}

template<class PtType, class VecType>
inline
void FITRbfThinPlateTC<PtType,VecType>::WriteBinary( ofstream &out ) const
{
    FITRbfTC<PtType,VecType>::WriteBinary(out);
    for ( int i = 0; i <  this->m_adSigmaSq.num(); i++ )
		out.write( (const char *) & this->m_adSigmaSq[i], sizeof(double) );
}

template<class PtType, class VecType>
inline
void FITRbfThinPlateTC<PtType,VecType>::ReadBinary( ifstream &in ) 
{
    FITRbfTC<PtType,VecType>::ReadBinary(in);

    this->m_adSigmaSq.need( this->m_aptRad.num() );

    for ( int i = 0; i <  this->m_adSigmaSq.num(); i++ )
		in.read( (char *) & this->m_adSigmaSq[i], sizeof(double) );
}

