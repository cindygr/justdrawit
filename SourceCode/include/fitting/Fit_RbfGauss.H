// Fit_RBFGauss.H Gaussian bumps
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_FITRbfGaussTC_H__C622E9A3_1A9D_422D_BAF4_50E33C53C211__INCLUDED_)
#define AFX_FITRbfGaussTC_H__C622E9A3_1A9D_422D_BAF4_50E33C53C211__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include <utils/Rn_Defs.H>
#include <utils/Mesh_Array.H>
#include <fitting/Fit_RBF.H>

/** \class FITRbfGaussTC Fit_RbfGauss.H fitting/Fit_RbfGauss.H 
     \ingroup FittingElems
     \brief Radial Basis function fitting with gaussian kernel

 * Kernel: f(d) = e^{-d^2/sigma^2}
 * Total: f(u,v) a0 + a1 u + a2 v + sum_i wi e^{ -||u,v - xi,yi|| / sigmai^2}
 *     where ai form a polynomial, the xi,yi are the centers of the gaussians,
 *     and sigmai is a scale factor to account for the point distribution in
 *     the plane.
 */
template< class PtType, class VecType >
class FITRbfGaussTC : public FITRbfTC<PtType, VecType>
{
protected:
    double EvalDistSq( const double in_dDistSq, const int in_i ) const { return exp( -in_dDistSq /  this->m_adSigmaSq[in_i] ); }

public:
    /**@name Editing */
    //@{
    //@}

    ///
    PtType Eval( const R2Pt &in_pt ) const;

    ///
    VecType DerivS( const R2Pt &in_pt ) const;
    ///
    VecType DerivT( const R2Pt &in_pt ) const;
    ///
    VecType DerivSS( const R2Pt &in_pt ) const;
    ///
    VecType DerivTT( const R2Pt &in_pt ) const;
    ///
    VecType DerivST( const R2Pt &in_pt ) const;
    ///
    void AllEval( const R2Pt &in_pt,
                  PtType &out_pt,
                  VecType &out_vecDs, VecType &out_vecDt,
                  VecType &out_vecDss, VecType &out_vecDtt,
                  VecType &out_vecDst ) const;
    ///
    void AllEvalSub( const R2Pt &in_pt,
                     PtType &out_pt,
                     VecType &out_vecDs, VecType &out_vecDt ) const;

    ///
    FITRbfGaussTC &operator=( const FITRbfGaussTC<PtType, VecType> &in_func );

    FITRbfGaussTC( const FITRbfGaussTC<PtType, VecType> &in_func ) { *this = in_func; }
    FITRbfGaussTC();
    virtual ~FITRbfGaussTC();

    void Write( ofstream &out ) const;
    void Read( ifstream &in );
    void WriteBinary( ofstream &out ) const;
    void ReadBinary( ifstream &in );
};

#include "Fit_RbfGauss_i.H"

typedef FITRbfGaussTC<R3Pt, R3Vec> FITRbfGauss;

#endif // !defined(AFX_FITRbfGaussTC_H__C622E9A3_1A9D_422D_BAF4_50E33C53C211__INCLUDED_)

