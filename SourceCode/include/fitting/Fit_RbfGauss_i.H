// d(x,y - s,t) = sqrt( (x0 - s)^2 + (y0 - t)^2 )

// a0 + a1 s + a2 t + sum wi e^{d(x,y - s,t)^2  / sigmai^2}
template<class PtType, class VecType>
inline
PtType FITRbfGaussTC<PtType,VecType>::Eval( const R2Pt &in_pt ) const
{
    PtType ptRes;

    // polynomial
    for ( int i = 0; i < ptRes.Dim(); i++ ) {
        ptRes[i] = this->m_aptPoly[0][i] + this->m_aptPoly[1][i] * in_pt[0] + this->m_aptPoly[2][i] * in_pt[1];
    }

    for ( FORINT i = 0; i < this->m_aptRad.num(); i++ ) {
        // d(x,y - s,t)^2
        const double dDistSq = LengthSq( this->m_aptRad[i] - in_pt );

        // Kernel e^{d^2/sigma^2}
        const double dPhi = EvalDistSq( dDistSq, i ); // exp( -dDistSq / this->m_adSigmaSq[i] );

        if ( !RNIsZero( dPhi, 1e-12 ) ) {
            // multiply by weight
            const PtType &ptLambda = this->m_aptLambda[i];

            for ( int j = 0; j < ptRes.Dim(); j++ )
                ptRes[j] += ptLambda[j] * dPhi;
        }
    }

    return ptRes;
}

// D/du e^x = dx/du e^x
template<class PtType, class VecType>
inline
VecType FITRbfGaussTC<PtType,VecType>::DerivS( const R2Pt &in_pt ) const
{
    VecType vecRes;

    // polynomial derivative
    for ( int i = 0; i < vecRes.Dim(); i++ ) {
        vecRes[i] = this->m_aptPoly[1][i];
    }

    for ( FORINT i = 0; i < this->m_aptRad.num(); i++ ) {
        // d(x,y - s,t)^2
        const double dDistSq = LengthSq( this->m_aptRad[i] - in_pt );
        // d/ds (s-x)^2 + (t-y)^2 = 2 (s - x)
        const double dDistSqDeriv = 2.0 * (this->m_aptRad[i][0] - in_pt[0]) / this->m_adSigmaSq[i];

        // Kernel e^{d^2/sigma^2}
        const double dPhi = exp( -dDistSq / this->m_adSigmaSq[i] );

        //
        const double dPhiDs = dDistSqDeriv * dPhi;

        // multiply by weight
        const PtType &ptLambda = this->m_aptLambda[i];
        for ( int j = 0; j < vecRes.Dim(); j++ )
            vecRes[j] += ptLambda[j] * dPhiDs;
    }

    return vecRes;
}

template<class PtType, class VecType>
inline
VecType FITRbfGaussTC<PtType,VecType>::DerivT( const R2Pt &in_pt ) const
{
    VecType vecRes;

    for ( int i = 0; i < vecRes.Dim(); i++ ) {
        vecRes[i] = this->m_aptPoly[2][i];
    }

    for ( FORINT i = 0; i < this->m_aptRad.num(); i++ ) {
        // d(x,y - s,t)^2
        const double dDistSq = LengthSq( this->m_aptRad[i] - in_pt );
        // d/ds (s-x)^2 + (t-y)^2 = 2 (s - x)
        const double dDistSqDeriv = 2.0 * (this->m_aptRad[i][1] - in_pt[1]) / this->m_adSigmaSq[i];

        // Kernel e^{d^2/sigma^2}
        const double dPhi = exp( -dDistSq / this->m_adSigmaSq[i] );

        //
        const double dPhiDt = dDistSqDeriv * dPhi;

        // multiply by weight
        const PtType &ptLambda = this->m_aptLambda[i];
        for ( int j = 0; j < vecRes.Dim(); j++ )
            vecRes[j] += ptLambda[j] * dPhiDt;
    }
    return vecRes;
}

// d/ds  dx e^x = dx^2/ds e^x + dx dx e^x
template<class PtType, class VecType>
inline
VecType FITRbfGaussTC<PtType,VecType>::DerivSS( const R2Pt &in_pt ) const
{
    VecType vecRes;

    RNZero( vecRes );

    for ( int i = 0; i < this->m_aptRad.num(); i++ ) {
        // d(x,y - s,t)^2
        const double dDistSq = LengthSq( this->m_aptRad[i] - in_pt );
        // d/ds (s-x)^2 + (t-y)^2 = 2 (s - x)
        const double dDistSqDeriv = 2.0 * (this->m_aptRad[i][0] - in_pt[0]) / this->m_adSigmaSq[i];

        // d/ds  2 (s - x) = 2.0
        const double dDistSqDerivDeriv = -2.0 / this->m_adSigmaSq[i];

        // Kernel e^{d^2/sigma^2}
        const double dPhi = exp( -dDistSq / this->m_adSigmaSq[i] );

        //
        const double dPhiDss = dDistSqDerivDeriv * dPhi + dDistSqDeriv * dDistSqDeriv * dPhi;

        // multiply by weight
        const PtType &ptLambda = this->m_aptLambda[i];
        for ( int j = 0; j < vecRes.Dim(); j++ )
            vecRes[j] += ptLambda[j] * dPhiDss;
    }
    return vecRes;
}

template<class PtType, class VecType>
inline
VecType FITRbfGaussTC<PtType,VecType>::DerivTT( const R2Pt &in_pt ) const
{
    VecType vecRes;

    RNZero( vecRes );

    for ( int i = 0; i < this->m_aptRad.num(); i++ ) {
        // d(x,y - s,t)^2
        const double dDistSq = LengthSq( this->m_aptRad[i] - in_pt );
        // d/ds (s-x)^2 + (t-y)^2 = 2 (s - x)
        const double dDistSqDeriv = 2.0 * (this->m_aptRad[i][1] - in_pt[1]) / this->m_adSigmaSq[i];

        // d/ds  2 (s - x) = 2.0
        const double dDistSqDerivDeriv = -2.0 / this->m_adSigmaSq[i];

        // Kernel e^{d^2/sigma^2}
        const double dPhi = exp( -dDistSq / this->m_adSigmaSq[i] );

        //
        const double dPhiDtt = dDistSqDerivDeriv * dPhi + dDistSqDeriv * dDistSqDeriv * dPhi;

        // multiply by weight
        const PtType &ptLambda = this->m_aptLambda[i];
        for ( int j = 0; j < vecRes.Dim(); j++ )
            vecRes[j] += ptLambda[j] * dPhiDtt;
    }
    return vecRes;
}


// d/ dt  dx e^x = 0 e^x + dx dy e^x
template<class PtType, class VecType>
inline
VecType FITRbfGaussTC<PtType,VecType>::DerivST( const R2Pt &in_pt ) const
{
    VecType vecRes;

    RNZero( vecRes );

    for ( int i = 0; i < this->m_aptRad.num(); i++ ) {
        // d(x,y - s,t)^2
        const double dDistSq = LengthSq( this->m_aptRad[i] - in_pt );
        // d/ds (s-x)^2 + (t-y)^2 = 2 (s - x)
        const double dDistSqDerivU = 2.0 * (this->m_aptRad[i][0] - in_pt[0]) / this->m_adSigmaSq[i];
        const double dDistSqDerivV = 2.0 * (this->m_aptRad[i][1] - in_pt[1]) / this->m_adSigmaSq[i];

        // Kernel e^{d^2/sigma^2}
        const double dPhi = exp( -dDistSq / this->m_adSigmaSq[i] );

        //
        const double dPhiDst = dDistSqDerivU * dDistSqDerivV * dPhi;

        // multiply by weight
        const PtType &ptLambda = this->m_aptLambda[i];
        for ( int j = 0; j < vecRes.Dim(); j++ )
            vecRes[j] += ptLambda[j] * dPhiDst;
    }
    return vecRes;
}

template<class PtType, class VecType>
inline
void FITRbfGaussTC<PtType,VecType>::AllEval( const R2Pt &in_pt,
                            PtType &out_pt,
                            VecType &out_vecDs, VecType &out_vecDt,
                            VecType &out_vecDss, VecType &out_vecDtt,
                            VecType &out_vecDst ) const
{

    for ( int i = 0; i < out_pt.Dim(); i++ ) {
        out_pt[i] = this->m_aptPoly[0][i] + this->m_aptPoly[1][i] * in_pt[0] + this->m_aptPoly[2][i] * in_pt[1];
        out_vecDs[i] = this->m_aptPoly[1][i];
        out_vecDt[i] = this->m_aptPoly[2][i];
        out_vecDss[i] = 0.0;
        out_vecDst[i] = 0.0;
        out_vecDtt[i] = 0.0;
    }

    for ( FORINT i = 0; i < this->m_aptRad.num(); i++ ) {
        // d(x,y - s,t)^2
        const double dDistSq = LengthSq( this->m_aptRad[i] - in_pt );
        // d/ds (s-x)^2 + (t-y)^2 = 2 (s - x)
        const double dDistSqDerivU = 2.0 * (this->m_aptRad[i][0] - in_pt[0]) / this->m_adSigmaSq[i];
        const double dDistSqDerivV = 2.0 * (this->m_aptRad[i][1] - in_pt[1]) / this->m_adSigmaSq[i];

        // d/ds  2 (s - x) = 2.0
        const double dDistSqDerivDeriv = -2.0 / this->m_adSigmaSq[i];

        // Kernel e^{d^2/sigma^2}
        const double dPhi = exp( -dDistSq / this->m_adSigmaSq[i] );

        //
        const double dPhiDs  = dDistSqDerivU * dPhi;
        const double dPhiDt  = dDistSqDerivV * dPhi;
        const double dPhiDss = dDistSqDerivDeriv * dPhi + dDistSqDerivU * dDistSqDerivU * dPhi;
        const double dPhiDtt = dDistSqDerivDeriv * dPhi + dDistSqDerivV * dDistSqDerivV * dPhi;
        const double dPhiDst = dDistSqDerivU * dDistSqDerivV * dPhi;

        // multiply by weight
        const PtType &ptLambda = this->m_aptLambda[i];
        for ( int iD = 0; iD < out_pt.Dim(); iD++ ) {
            out_pt[iD]     += ptLambda[iD] * dPhi;
            out_vecDs[iD]  += ptLambda[iD] * dPhiDs;
            out_vecDt[iD]  += ptLambda[iD] * dPhiDt;
            out_vecDss[iD] += ptLambda[iD] * dPhiDss;
            out_vecDst[iD] += ptLambda[iD] * dPhiDst;
            out_vecDtt[iD] += ptLambda[iD] * dPhiDtt;
        }
    }
}

template<class PtType, class VecType>
inline
void FITRbfGaussTC<PtType,VecType>::AllEvalSub( const R2Pt &in_pt,
                                                      PtType &out_pt,
                                                      VecType &out_vecDs, VecType &out_vecDt ) const
{
    for ( int i = 0; i < out_pt.Dim(); i++ ) {
        out_pt[i] = this->m_aptPoly[0][i] + this->m_aptPoly[1][i] * in_pt[0] + this->m_aptPoly[2][i] * in_pt[1];
        out_vecDs[i] = this->m_aptPoly[1][i];
        out_vecDt[i] = this->m_aptPoly[2][i];
    }

    for ( FORINT i = 0; i < this->m_aptRad.num(); i++ ) {
        // d(x,y - s,t)^2
        const double dDistSq = LengthSq( this->m_aptRad[i] - in_pt );
        // d/ds (s-x)^2 + (t-y)^2 = 2 (s - x)
        const double dDistSqDerivU = 2.0 * (this->m_aptRad[i][0] - in_pt[0]) /  this->m_adSigmaSq[i];
        const double dDistSqDerivV = 2.0 * (this->m_aptRad[i][1] - in_pt[1]) /  this->m_adSigmaSq[i];

        // Kernel e^{d^2/sigma^2}
        const double dPhi = exp( -dDistSq /  this->m_adSigmaSq[i] );

        //
        const double dPhiDs  = dDistSqDerivU * dPhi;
        const double dPhiDt  = dDistSqDerivV * dPhi;

        // multiply by weight
        const PtType &ptLambda = this->m_aptLambda[i];
        for ( int iD = 0; iD < out_pt.Dim(); iD++ ) {
            out_pt[iD]     += ptLambda[iD] * dPhi;
            out_vecDs[iD]  += ptLambda[iD] * dPhiDs;
            out_vecDt[iD]  += ptLambda[iD] * dPhiDt;
        }
    }
}



//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

template<class PtType, class VecType>
inline
FITRbfGaussTC<PtType,VecType>::FITRbfGaussTC()
{
}

template<class PtType, class VecType>
inline
FITRbfGaussTC<PtType,VecType>::~FITRbfGaussTC()
{

}

///
template<class PtType, class VecType>
inline
FITRbfGaussTC<PtType,VecType> &FITRbfGaussTC<PtType,VecType>::operator=( const FITRbfGaussTC<PtType,VecType> &in_func )
{
    FITRbfTC<PtType,VecType>::operator=( in_func );

     this->m_adSigmaSq = in_func.m_adSigmaSq;

    return *this;
}

template<class PtType, class VecType>
inline
void FITRbfGaussTC<PtType,VecType>::Read( ifstream &in )
{
    FITRbfTC<PtType,VecType>::Read(in);
     this->m_adSigmaSq.need( this->m_aptRad.num() );

    for ( int i = 0; i <  this->m_adSigmaSq.num(); i++ )
        in >>  this->m_adSigmaSq[i];
}

template<class PtType, class VecType>
inline
void FITRbfGaussTC<PtType,VecType>::ReadBinary( ifstream &in )
{
	FITRbfTC<PtType,VecType>::ReadBinary(in);
	
     this->m_adSigmaSq.need( this->m_aptRad.num() );

    for ( int i = 0; i <  this->m_adSigmaSq.num(); i++ )
		in.read( (char *) & this->m_adSigmaSq[i], sizeof(double) );
}

template<class PtType, class VecType>
inline
void FITRbfGaussTC<PtType,VecType>::Write( ofstream &out ) const
{
    FITRbfTC<PtType,VecType>::Write(out);

    for ( int i = 0; i <  this->m_adSigmaSq.num(); i++ )
        out <<  this->m_adSigmaSq[i] << " ";

    out << "\n";
}

template<class PtType, class VecType>
inline
void FITRbfGaussTC<PtType,VecType>::WriteBinary( ofstream &out ) const
{
    FITRbfTC<PtType,VecType>::WriteBinary(out);

    for ( int i = 0; i <  this->m_adSigmaSq.num(); i++ )
		out.write( (const char *) & this->m_adSigmaSq[i], sizeof(double) );
}

