#ifndef _FITTING_SVD_DEFS
#define _FITTING_SVD_DEFS

#include <utils/Mesh_Array.H>
#include <utils/Rn_Defs.H>

/** \class FITToolsSVDLapack FITTools_SVD.H fitting/FITTools_SVD.H
    \ingroup FittingMatrix
    \brief Lapack Singular value decomposition (Also gives Eigen vectors and Eigen values)

  Factor a matrix into U D V^t, where U (Eigen vectors) and V are orthogonal, and D is a diagonal 
  matrix (eigen values).

  This does singular value decomposition, returning the eigen values/eigen vectors.

  If you don't need, say V, then it is faster not to compute it.

  Note that the A matrix data is overwritten when Solve is called. So calling Print after calling
  Solve will result in garbage.

<br>   Files: 
- include/fitting/FITTools_SVD.H (templated) */
class FITToolsSVDLapack {
private:
    long int m_iNRows;    ///< m
    long int m_iNCols;    ///< n

    /**@name Matrix storage. A is primary */
    //@{
    Array<double> m_adA;
    Array<double> m_adU, m_adVt, m_adD;
    //@}

    /**@name Scratch - store here */
    //@{
    Array<double> m_adE, m_adTauQ, m_adTauP, m_adWork;
    long int iWork, iInfo;
    //@}

    bool m_bCalcU, ///< Calculate the U matrix, y/n
	     m_bCalcVt;///< Calculate the V matrix, y/n

public:
    /**@name Accessors */
    //@{
    /// Number of rows in A
    int NRows()   const { return m_iNRows; }
    /// Number of columns in A
    int NCols() const { return m_iNCols; }
    /// 
    const double &A( const int in_iR, const int in_iC ) const;
    ///
    const double &U( const int in_iR, const int in_iC ) const;
    ///
    const double &Vt( const int in_iR, const int in_iC ) const;
    /// Diagonal
    const double &Sigma( const int in_i ) const;
    //@}

    /**@name Set up */
    //@{
    ///
    double &A( const int in_iR, const int in_iC );
    /// By default, don't calculate. Call if you want U.
    void CalcU() { m_bCalcU = true; }
    /// By default, don't calculate. Call if you want V.
    void CalcVt() { m_bCalcVt = true; }
    /// Delete all the data
    void Clear();
    /// Resize if necessary
    void Resize( const int in_iNRows, const int in_iNCols );
    //@}

    /**@name Solve */
    //@{
    /// Note: A is overwritten by solve
    bool Solve();
    //@}

    ///
    FITToolsSVDLapack( const int in_iNRows, const int in_iNCols, const bool in_bCalcU = false, const bool in_bCalcVt = false )
        : m_bCalcU( in_bCalcU ), m_bCalcVt( in_bCalcVt ) { Resize( in_iNRows, in_iNCols ); }
    ///
    ~FITToolsSVDLapack() { }

    /**@name Debuging */
    //@{
    void CheckOrthoU() const;
    ///
    void CheckOrthoVt() const;
    // Need to reset A for this call to work (A is overwritten by solve)
    void CheckAisUSVt() const;
    //@}
};

inline const double &FITToolsSVDLapack::A(const int in_iRow, const int in_iCol) const
{
   ASSERT(in_iRow >= 0 && in_iRow < m_iNRows);
   ASSERT(in_iCol >= 0 && in_iCol < m_iNCols);
   return m_adA[ (int) ( in_iCol * m_iNRows + in_iRow )];
}

inline const double &FITToolsSVDLapack::U(const int in_iRow, const int in_iCol) const
{
   ASSERT(in_iRow >= 0 && in_iRow < m_iNRows);
   ASSERT(in_iCol >= 0 && in_iCol < m_iNCols);
   return m_adU[ (int) ( in_iCol * m_iNRows + in_iRow ) ];
}

inline const double &FITToolsSVDLapack::Vt(const int in_iRow, const int in_iCol) const
{
   ASSERT(in_iRow >= 0 && in_iRow < m_iNRows);
   ASSERT(in_iCol >= 0 && in_iCol < m_iNCols);
   return m_adVt[ (int) ( in_iCol * m_iNRows + in_iRow )];
}

inline const double &FITToolsSVDLapack::Sigma(const int in_i) const
{
   ASSERT(in_i >= 0 && in_i < m_iNCols);
   return m_adD[in_i];
}

inline double &FITToolsSVDLapack::A(const int in_iRow, const int in_iCol) 
{
   ASSERT(in_iRow >= 0 && in_iRow < m_iNRows);
   ASSERT(in_iCol >= 0 && in_iCol < m_iNCols);
   return m_adA[  (int) ( in_iCol * m_iNRows + in_iRow ) ];
}

inline void FITToolsSVDLapack::Clear()
{
    if ( m_adA.num() ) m_adA.memfill(0);
    if ( m_adU.num() ) m_adU.memfill(0);
    if ( m_adVt.num() ) m_adVt.memfill(0);
    if ( m_adD.num() ) m_adD.memfill(0);
}

inline void FITToolsSVDLapack::Resize( const int in_iR, const int in_iC )
{
    ASSERT( in_iR > 0 && in_iC > 0 );
    m_iNRows = in_iR;
    m_iNCols = in_iC;
    m_adA.need( in_iR * in_iC );
    m_adA.memfill(0);
}


inline void FITToolsSVDLapack::CheckOrthoU() const
{
    ASSERT( m_bCalcU == TRUE );
    for ( int iC = 0; iC < m_iNCols; iC++ ) {
        for ( int iC2 = iC+1; iC2 < m_iNCols; iC2++ ) {
            double dSum = 0.0;
            for ( int i = 0; i < m_iNRows; i++ ) 
                dSum += U( i, iC ) * U( i, iC2 );
            ASSERT( RNIsZero( dSum, 1e-6 ) );
        }
    }
}

inline void FITToolsSVDLapack::CheckOrthoVt() const
{
    ASSERT( m_bCalcVt == TRUE );
    for ( int iC = 0; iC < m_iNCols; iC++ ) {
        for ( int iC2 = iC+1; iC2 < m_iNCols; iC2++ ) {
            double dSum = 0.0;
            double dSum2 = 0.0;
            for ( int i = 0; i < 3; i++ ) {
                dSum += Vt( i, iC ) * Vt( i, iC2 );
                dSum2 += Vt( iC, i ) * Vt( iC2, i );
            }
            ASSERT( RNIsZero( dSum, 1e-6 ) );
            ASSERT( RNIsZero( dSum2, 1e-6 ) );
        }
    }
}

inline void FITToolsSVDLapack::CheckAisUSVt() const
{
    ASSERT( m_bCalcU == TRUE && m_bCalcVt == TRUE );
    for ( int iR = 0; iR < m_iNRows; iR++ ) {
        for ( int iC = 0; iC < m_iNCols; iC++ ) {
            double dSum = 0;
            for ( int iS = 0; iS < 3; iS++ ) {
                dSum += U(iR, iS) * m_adD[iS] * Vt(iS,iC);
            }
            const double dCompare = A(iR, iC);
            ASSERT( RNApproxEqual( dSum, dCompare, 1e-6 ) );
        }
    }
}


#endif

