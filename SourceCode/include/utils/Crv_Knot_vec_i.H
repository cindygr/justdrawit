inline double CRVKnot_vec::BasisCenter( const int in_iWhich ) const
{
    if ( Order() % 2 == 1 ) {
        const int iOffset = (Order() + 2) / 2;
        return ( Knot( in_iWhich + iOffset ) + Knot( in_iWhich + iOffset + 1 ) ) * 0.5;
    }

    const int iOffset = (Order()+3) / 2;
    return Knot( in_iWhich + iOffset );
}

// Information on the knot vector
inline int CRVKnot_vec::Num_knots() const 
{ 
   return m_adKnots.num(); 
}
inline double CRVKnot_vec::First_knot()    const { return m_adKnots[0]; }
inline double CRVKnot_vec::Knot(int which) const { return m_adKnots[which]; }
inline double CRVKnot_vec::Last_knot()     const { return m_adKnots.last(); }

// Number of points
inline int CRVKnot_vec::Num_pts() const 
{ 
   return WINmax(0, Num_knots() - (Order()+2) ); 
}

inline int CRVKnot_vec::Num_segments() const 
{ 
   return WINmax(0, Num_knots() - 2*(Order()+1) - 1); 
}

inline double CRVKnot_vec::Segment_start_t(int i) const 
{
   return Knot(i + Order() + 1);
}

inline double CRVKnot_vec::Segment_end_t(int i) const 
{
   return Knot(i + Order() + 2);
}

inline double CRVKnot_vec::Segment_perc_t(int i, double in_dPerc) const 
{
   return Segment_start_t(i) + (Segment_end_t(i) - Segment_start_t(i)) * in_dPerc;
}

inline WINbool CRVKnot_vec::Span( const double in_dT, int &out_iStart, int &out_iEnd ) const
{
   if ( in_dT < m_adKnots[0] ) return FALSE;
   if ( in_dT > m_adKnots.last() ) return FALSE;

   const int f_k = Floor_knot(in_dT);
   const int c_k = Ceil_knot(in_dT);
   out_iStart = WINmax(0, f_k - (Order()+1) - 1);
   out_iEnd = WINmin(c_k+(Order()+1), Num_pts());

   return TRUE;
}

// The parameter points at which the curve becomes valid
inline double CRVKnot_vec::Start_t()   const 
{ 
   return Knot(Order()+1); 
}

inline double CRVKnot_vec::End_t() const 
{ 
    return Knot(Num_pts()); 
}

inline double CRVKnot_vec::Perc_t(double in_dPerc) const 
{ 
    return Start_t() + (End_t() - Start_t()) * in_dPerc;
}

inline const Array<double> CRVKnot_vec::Knots() const { return m_adKnots; }

// The basis
inline double CRVKnot_vec::Basis(double in_dT, int in_i) const 
{
    return (in_i >= 0 && in_i < m_aBasis.num()) ? m_aBasis[in_i](in_dT) : 0;
}
inline double CRVKnot_vec::BasisDerivative(double in_dT, int in_i) const 
{
    return (in_i >= 0 && in_i < m_aBasis.num()) ? 
          m_aBasis[in_i].Derivative(in_dT) : 0;
}
   
inline double CRVKnot_vec::BasisSecondDerivative(double in_dT, int in_i) const 
{
    return (in_i >= 0 && in_i < m_aBasis.num()) ? 
          m_aBasis[in_i].SecondDerivative(in_dT) : 0;
}

// Operations on the knot vector
inline void CRVKnot_vec::Set_knots(double in_dStart, double in_dEnd) 
{
   int n_intvs = Num_knots() - (2 * (Order()+2) - 1);
   if (n_intvs < 1) return;
   double div = (in_dEnd - in_dStart) / (n_intvs);
   Set_knots(in_dStart - (Order()+1) * div, 
             in_dEnd + (Order()+1) * div,
             0, Num_knots()-1);
}

inline void CRVKnot_vec::Set_knots(double in_dStart, double in_dEnd,
                                    double in_dEndSpacing) 
{
   int n_intvs = Num_knots() - (2 * (Order()+2) - 1);
   if (n_intvs < 1) return;
   double div = (in_dEnd - in_dStart) / (n_intvs);
   Set_knots(in_dStart - (Order()+1) * div, 
             in_dEnd + (Order()+1) * div,
             0, Num_knots()-1);
   Set_knots( in_dStart - (Order()+1) * in_dEndSpacing,
              in_dStart,
              0, (Order() + 1) );
   Set_knots( in_dEnd,
              in_dEnd + (Order()+1) * in_dEndSpacing,
              Num_knots() - (Order() + 1) - 1,
              Num_knots() - 1);
}

/* ---------------------------------------------------------------
 * DESCR   :	Floor and ceiling knot values
 * -------------------------------------------------------------- */
inline int CRVKnot_vec::Ceil_knot(double in_dT) const
{
    if (in_dT >= Last_knot()) return Num_knots()-1;
    
    for (int i = Num_knots() - 1; i >= 0; i--) {
        if (in_dT > Knot(i)) return (i+1);
        if (RNApproxEqual(in_dT, Knot(i))) return i;
    }
   
   return 0;
}

inline int CRVKnot_vec::Floor_knot(double in_dT) const 
{
   if (in_dT <= First_knot()) return 0;

   for (int i = 0; i < Num_knots(); i++) {
      if (RNApproxEqual(in_dT, Knot(i))) return i;
      if (in_dT < Knot(i)) return (i-1);
   }
   
   return Num_knots()-1;
}

inline int CRVKnot_vec::Segment(double in_dT) const 
{
    const int iKnot = Floor_knot( in_dT );
    return WINminmax( iKnot - (Order() + 1), 0, Num_segments() - 1 );
}

/* ------------------------------------------------------------
 * DESCR   :	Make a copy of the given curve
 * ------------------------------------------------------------ */
inline CRVKnot_vec::CRVKnot_vec(const CRVKnot_vec &in_crvknot) 
: CRVSpace(in_crvknot)
{
    CRVBasis::SetDefaultOrder(Order());
    m_adKnots = in_crvknot.m_adKnots;
    m_aBasis = in_crvknot.m_aBasis;
}

#ifdef DEBUG
inline WINbool CRVKnot_vec::CheckKnotVec(WINbool in_bCheckSum) const
{
   for (int i = 0; i < m_adKnots.num()-1; i++) 
     ASSERT(m_adKnots[i+1] >= m_adKnots[i]);
   
   for (FORINT i = 0; i < Num_pts(); i++) {
      for (int j = 0; j < Order()+3; j++)
        ASSERT(Knot(i+j) == m_aBasis[i].Knot(j));
   }
   if (in_bCheckSum == TRUE) {
      double save_prec = RNEpsilon_d;
      RNEpsilon_d = 1e-8;
      double div = 1.0 / (double) (Order() + 3);
      for (int i = 0; i < Num_segments(); i++) {
         for (double t = 0.0; t <= 1.0; t += div) {
            double sum = 0;
            for (int k = 0; k < Num_basis(); k++)
              sum += Basis(k)(Segment_perc_t(i, t));
            ASSERT(RNApproxEqual(sum, 1.0));
         }
      }
      RNEpsilon_d = save_prec;
   }
   return TRUE;
}

#else
inline WINbool CRVKnot_vec::CheckKnotVec(WINbool ) const { return TRUE; }
#endif


