#ifndef _OGL_SHADER_H_DEFS
#define _OGL_SHADER_H_DEFS

#if !defined(powerpc) && !defined(__linux__) && !defined(__APPLE__)
#define WIN32_LEAN_AND_MEAN
#include <windows.h>
#endif

#include <OpenGL/OGLRoutines.H>
#include <OpenGL/OGLRoutines_State.H>
#include <utils/Rn_Defs.H>
#include <map>
#include <iostream>
#include <vector>
#include <string>

#if defined(powerpc) || defined(__APPLE__)
#  include <OpenGL/gl.h>
#  include <OpenGL/glu.h>
#  include <OpenGL/glext.h>
#else
#  include <GL/gl.h>
//#  include <GL/glaux.h>
#  include <GL/glExt.h>
#  include <GL/glProc.h>
#endif

#define GLERROR {                                                       \
        GLenum code = glGetError();                         \
        while (code!=GL_NO_ERROR)                           \
        {                                                   \
            std::cout << __FILE__;                          \
            std::cout << ':' << __LINE__;                   \
            std::cout << ' ' << (const char *) gluErrorString(code) << std::endl; \
            code = glGetError();                            \
        }                                                   \
    }

#define __PROGRAMMABLE_PIPELINE__
//#undef __PROGRAMMABLE_PIPELINE__

#ifdef __PROGRAMMABLE_PIPELINE__

#define DEBUG_LEVEL 1

//#define debug(lvl, string) if(DEBUG_LEVEL && lvl >= DEBUG_LEVEL) { fprintf(fStream, "%f(%i): %s\n", __FILE__, __LINE__, string); }

#include <Cg/cg.h>
#include <Cg/cgGL.h>

extern void CgErrorCallback();

/** \class OGLShader2 OGLShader.h OpenGL/OGLShader.h
    \ingroup Drawing
    \brief A wrapper around vertex and fragment shaders

  This class is a starting point for writing a shader in the FlTK viewer
  */
class OGLShader2 {
public:
	OGLShader2();
	~OGLShader2();

	static void Init();
	
	/* These together read the entire shader from a file */
	bool LoadShaderFromFile(const char *pFileName);
	bool SetTechnique(const char *pTech);
	bool StartPass();
	bool EndPass();
	bool Done() { 
		if(complete) {
			complete = false;
			firstPass = true;
			return true;
		}
		return false;
	};
	/* End entire shader */
	
	/* These read individual vertex/fragment programs from files */
	void SetNumberVertexShaders( const int in_iN );
	void LoadVertexShader( const int in_iWhich, const char *in_strFileName, const char *in_strFuncName );
	void SetNumberFragmentShaders( const int in_iN );
	void LoadFragmentShader( const int in_iWhich, const char *in_strFileName, const char *in_strFuncName );
	
	void BindVertexProgram( const int in_iWhich );
	void BindFragmentProgram( const int in_iWhich );
	
	void DisablePrograms();

	// must have vertex/fragment program active
	void SetOGLMatrix4x4VP( const char * in_strName, const R4Matrix &in_mat);
	void SetColorFP( const char * in_strName, const UTILSColor &in_col);
	void SetPointFP( const char * in_strName, const R3Pt &in_pt );
	void SetVectorFP( const char * in_strName, const R3Vec &in_vec );
	void SetTextureIdFP( const char * in_strName, const GLuint in_iTexId );
	/* End individual programs */

	void Verbose(bool pV) {verbose = pV;};

	bool SetMatrix4x4BySemantic(const char * pSemantic, float pMat[4][4]);
	bool SetMatrix4x4BySemantic(const char * pSemantic, float *pMat);

	// Necessary to flip!
	bool SetOGLMatrix4x4BySemantic(const char * pSemantic, const R4Matrix &pMat);
	bool SetOGLMatrix4x4ArrayByName(const char * pName, R4Matrix *pMat, int pNum);
	bool SetOGLMatrix4x4ArrayElementByName(const char * pName, const R4Matrix &pMat, int pElement);
	// NOT necessary to flip
	bool SetMatrix4x4BySemantic(const char * pSemantic, R4Matrix pMat);

	bool SetMatrix3x3BySemantic(const char * pSemantic, float pMat[3][3]);
	bool SetMatrix3x3BySemantic(const char * pSemantic, float *pMat);

	bool SetFloatArrayByName(const char * pName, double * pArray, int pNum);
	bool SetFloatArrayElementByName(const char * pName, double pArray, int pElement);     
	bool SetFloat2ArrayByName(const char * pName, double * pArray, int pNum);
	bool SetFloat2ArrayElementByName(const char * pName, R2Pt pPt, int pElement);
	bool SetFloat3ArrayByName(const char * pName, double * pArray, int pNum);
	bool SetFloat3ArrayElementByName(const char * pName, R3Pt pPt, int pElement);
	bool SetFloat4ArrayByName(const char * pName, double * pArray, int pNum);
	bool SetFloat4ArrayElementByName(const char * pName, R4Pt in_pt , int pElement );
	

	bool SetFloat4BySemantic(const char * pSemantic, const UTILSColor pColor);
	bool SetFloat4BySemantic(const char * pSemantic, float pVec[4]);
	bool SetFloat4ByName(const char * pName , R4Pt pVec );
	bool SetFloat3BySemantic(const char * pSemantic, float pVec[3]);
	bool SetFloat3ByName(const char * pName , R3Pt  );
	bool SetFloat3BySemantic(const char * pSemantic, R3Pt pVec);
	bool SetFloat2BySemantic(const char * pSemantic, float pVec[2]);
	bool SetFloat2BySemantic(const char * pSemantic, const R2Pt_f &pVec);
	bool SetFloat2ByName(const char * pName, const R2Pt & );

	bool SetDouble3BySemantic(const char * pSemantic, const double *) ;

	bool SetBoolArrayByName(const char* pName , bool* pArray , int pNum );
	bool SetVector4BySemantic(const char * pSemantic, const UTILSColor pColor) { return SetFloat4BySemantic(pSemantic, pColor); };
	bool SetVector4BySemantic(const char * pSemantic, float pVec[4]) { return SetFloat4BySemantic(pSemantic, pVec); };
	bool SetVector3BySemantic(const char * pSemantic, float pVec[3]) { return SetFloat3BySemantic(pSemantic, pVec); };
	bool SetVector3BySemantic(const char * pSemantic, R3Pt pVec) { return SetFloat3BySemantic(pSemantic, pVec); };
	bool SetVector2BySemantic(const char * pSemantic, float pVec[2]) { return SetFloat2BySemantic(pSemantic, pVec); };
	bool SetVector2BySemantic(const char * pSemantic, const R2Pt_f &pVec) { return SetFloat2BySemantic(pSemantic, pVec); };

	bool SetTextureBySemantic(const char * pSemantic, GLuint pID);

	bool SetFloatBySemantic(const char * pSemantic, float pValue);
	bool SetFloatByName(const char * pName , double pValue );
	bool SetBoolBySemantic(const char * pSemantic, bool pValue);
	bool SetIntBySemantic(const char * pSemantic, int pValue);
	bool SetIntArrayElementByName(const char * pName , int pValue , int pElement );
	bool SetIntArrayByName(const char * pName , int* pArray , int pNum );
	bool SetInt4ArrayByName(const char * pName , int* pArray , int pNum );

	int GetNumPasses() { return numPasses; };

	bool SetParameterVariabilityByName(const char *pSemantic, CGenum pVar);

	void EnableTextureMaps();
	void DisableTextureMaps();

	bool Validate();

	const char * GetTechnique(int pIndex);


	void Release();

private:
	CGeffect effect;
	CGtechnique technique;
	CGcontext context;
	CGpass pass;
	
	CGprofile m_CgVertexProfile, m_CgFragmentProfile;
	std::vector<CGprogram> m_aCgVertexProgram, m_aCgFragmentProgram;
	int m_iActiveVP, m_iActiveFP;
	
	bool verbose;
	int numPasses;
	int numTechniques;
	bool loaded;
	bool complete;
	bool firstPass;
	std::vector<CGtechnique> techniqueList;
	std::vector<const char *> namesList;
	std::vector< pair<GLuint, CGparameter> > m_aTexIds;
	
	void CheckForCgError( const char * situation );
};

#endif

#endif
