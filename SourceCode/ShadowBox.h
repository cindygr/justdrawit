/*
 *  ShadowBox.h
 *  SketchCurves
 *
 *  Created by Cindy Grimm on 8/30/10.
 *  Copyright 2010 Washington University in St. Louis. All rights reserved.
 *
 */

#ifndef _SHADOW_BOX_DEFS_H_
#define _SHADOW_BOX_DEFS_H_

#include <utils/R3_Plane.H>

class OGLObjsCamera;

/*
 * The shadow box and the draw plane
 *
 * The box knows how to center itself on the camera viewing volume. By default,
 * it is always centered so that the back wall is parallel to the view plane
 *
 * Note: It use to be that the shadow box was fixed in space and the camera went
 * around it, so it as an actual 3D object in the scene. So whenever the camera
 * is changed the shadow box needs to be re-centered.
 *
 */ 
class ShadowBox {
public:
    typedef enum {
        NO_SURFACE = -1,
        BACKWALL,
        SIDEWALL,
        FLOOR,
        DRAWING_PLANE
    } Surface;
    
private:
	/* Vectors defining the geometry of the box and plane. Only m_vecPlaneNorm is normalized.
	 * m_vecX, m_vecY, and m_vecZ are all the same length.
	 * m_vecDrawingPlaneX and m_vecDrawingPlaneY are the same length (half the width of the drawing plane
	 */	
    R3Pt m_ptCorner; // Lower left corner
    R3Vec m_vecX, m_vecY, m_vecZ; // The vector to the right, up, and towards the viewer (x,y,z in camera space)
    R3Pt m_ptDrawingPlaneCenter;  // Where the drawing plane is in space
	R3Vec m_vecDrawingPlaneX, m_vecDrawingPlaneY; // Vector from the center to the edge (half the width of the draw plane)
	R3Vec m_vecPlaneNorm; // plane normal, unit length.
    
	/* Save how big the selection items are when the param file is read in (set in UpdateParams)
	 * m_dPad and m_dCameraSetback control how much padding around the object there is
	 */
	double m_dSizeSquare, m_dSizeCorners, m_dSizeAxis, m_dPad, m_dCameraSetback;
    
	// 0-3 are the four corners of the drawing plane, starting in the lower left and going counter clockwise
	// 4-8 are the free-rotation corners 
	// 9 is scale arrow
	// 10-15 are the axis-aligned rotation sides
	int CursorOverDrawingPlaneCorner( const R2Pt &in_ptScreen, double &out_dZ ) const;
	int CursorOverBoxCorner( const R2Pt &in_ptScreen, double &out_dZ ) const;
	int CursorOverEdge( const R2Pt &in_ptScreen, double &out_dZ ) const;
	int CursorOverDrawingPlaneSide( const R2Pt &in_ptScreen, double &out_dZ ) const;
	int CursorOverShadowBoxSquares( const R2Pt &in_ptScreen, double &out_dZ ) const;

	// Draw a square starting at in_pt and ending at pt + in_vecX + in_vecY
    void DrawSquare( const R3Pt &in_pt, const R3Vec &in_vecX, const R3Vec &in_vecY ) const;
	// Same as above, but change color based on selection
    void DrawSelectSquare( const R3Pt &in_pt, const R3Vec &in_vecX, const R3Vec &in_vecY, const bool in_bCursorOver ) const;
	
	// Grid on walls
	void DrawGrid() const;
	void DrawGridDrawingPlane() const; // Grid on drawing plane
    void DrawBoxCorners( const int in_iCorner ) const; // The balls at the corners
    void DrawBoxAxis( const int in_iAxis ) const; // The fat cylinders along the edges
    
public:
	/**@name Access */
	//@{
	/// Center of the box
    R3Pt GetCenter() const { return m_ptCorner + (m_vecX + m_vecY + m_vecZ) * 0.5; }
	/// Lower left corner
    const R3Pt &GetCorner() const { return m_ptCorner; }
	/// Get the end point of the x,y,z axes (right, top, towards the viewer)
    R3Pt GetAxis( const int in_iWhich ) const;
	/// Where the drawing plane is centered
    R3Pt GetDrawingPlaneCenter( ) const { return m_ptDrawingPlaneCenter; }
	/// Vector from the center of the plane to the right edge
	R3Vec GetDrawingPlaneXVec() const { return m_vecDrawingPlaneX; }
	/// Vector from the center of the plane to the bottom edge
	R3Vec GetDrawingPlaneYVec() const { return m_vecDrawingPlaneY; }
	/// Size of the box. 
	double Size() const { return ::Length( m_vecX ); }
	/// Planes containing the geometry
    R3Plane BackWallPlane() const;
	/// 
    R3Plane SideWallPlane() const;
	/// 
    R3Plane FloorPlane() const;    
	
	/// 
    R3Plane DrawingPlane( ) const;
	/* If Drawing plane, return drawing plane if visible or view plane if not */
    R3Plane GetPlane( const Surface in_srfType ) const;
	//@}
    
	/**@name Set or change the drawing plane */
	//@{ 
	/// Reset to middle of shadow box
	void ResetDrawingPlane();
	/// Just set it. Width/height will be determined by Size()
    void SetDrawingPlane( const R3Pt &in_pt, const R3Vec &in_vecNorm ); // Point on the curve
	/// Snap it to the center and two of the axes, whichever are most parallel to the view plane
	void SnapDrawingPlane( const std::pair< R3Pt, R3Matrix > &in_aFrame );
	/// Mouse down on drag square
	void StartDragDrawingPlane( const int in_iWhichCorner );
	/// Mouse down on cylinder on the edge
	void StartRotateDrawingPlane( const int in_iWhichCorner );
	/// Actually move the drawing plane (call on drag)
	void DragDrawingPlane( const R2Pt &in_ptDown, const R2Pt &in_ptCur );
	/// Actually rotate the drawing plane (call on drag)
	void RotateDrawingPlane( const R2Pt &in_ptDown, const R2Pt &in_ptCur );
	//@}
	
	void AlignBox( const OGLObjsCamera &in_cam, const bool in_bCenterDrawingPlane );
    
	/**@name Selection 
	 * The first routine returns an integer (-1 if no selection). What this integer
	 * means is given in the Is* routines
	 */
	//@{
	/// See if we're over anything. Returns -1 if no. Returns the camera depth value of the selection in out_dZ
	int CursorOverSelectable( const R2Pt &in_ptScreen, double &out_dZ ) const;
	/// Lower left corner (closest to the viewer)
	bool IsZoom( const int in_iSelectable ) const { return in_iSelectable == 9; }
	/// In one of the four squares of the plane
	bool IsDraggingPlane( const int in_iSelectable ) const { return in_iSelectable >= 0 && in_iSelectable < 4; }
	/// One of the outside corners (except the lower left)
	bool IsTrackball( const int in_iSelectable ) const { return in_iSelectable >= 4 && in_iSelectable < 9; }
	/// Cylinders on the top and bottom outside edges
	bool IsUpDownRotation( const int in_iSelectable ) const { return in_iSelectable > 9 && in_iSelectable < 12; }
	/// Cylinders on the left and right outside edges
	bool IsLeftRightRotation( const int in_iSelectable ) const { return in_iSelectable > 11 && in_iSelectable < 14; }
	/// Cylinders on the top left, bottom right outside edges
	bool IsSpinRotation( const int in_iSelectable ) const { return in_iSelectable > 13 && in_iSelectable < 16; }
	/// Cylinders on edges of draw plane
	bool IsSpinDrawPlane( const int in_iSelectable ) const { return in_iSelectable > 15 && in_iSelectable < 20; }
	/// Tab in the lower left
	bool IsCenterDrawPlane( const int in_iSelectable ) const { return in_iSelectable == 20; }
	/// Tab in the lower right
	bool IsShowHideDrawPlane( const int in_iSelectable ) const { return in_iSelectable == 21; }
	/// Intersect the ray with the visible part of the draw plane
	bool CursorOverDrawingPlane( const R3Ray &in_ray ) const;
	//@}
	
	/// Call when param file is re-read
	void UpdateParams();
	
	/**@name Drawing routines */
	//@{
	///
    void Draw( const int in_iCursorOverSelectable ) const;
	///
    void DrawDrawingPlane( const int in_iCursorOverSelectable ) const;
	//@}
    
	///
    ShadowBox();
	///
    ~ShadowBox() {}
    
	///
    ShadowBox &operator=( const ShadowBox &in_sb ) ;
	///
    ShadowBox( const ShadowBox &in_sb ) { *this = in_sb; }
};

#endif
