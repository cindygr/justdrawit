//
//  Cylinder.h
//  SketchCurves
//
//  Created by Cindy Grimm on 8/9/12.
//  Copyright (c) 2012 Washington University in St. Louis. All rights reserved.
//

#ifndef SketchCurves_Cylinder_h
#define SketchCurves_Cylinder_h

#include "CurveNetwork.h"
#include "SurfaceData.h"

class Cylinder : private SurfaceData
{
public:
    class Polygon {
    private:
        Array<R2Line_seg> m_aSeg;
        Array<R2Vec>      m_aSegNorm;
        Array<double>     m_adEdgeWeight;
        Array<R3Pt>       m_aptCorners;
        double            m_dLenTotal, m_dClamp, m_dScale;
        double            m_dMapShift, m_dMapScale;

        double ImplicitRaw( const R2Pt &in_pt ) const;

    public:
        double            m_dT;
        R4Matrix          m_mat;
        
        int NumPts() const { return m_aSeg.num(); }
        double Radius() const { return m_dLenTotal / (2.0 * M_PI); }
        double Create( const Array<R2Pt> &in_apt, const R4Matrix &in_mat, const double in_dT );
        double SetPolygon( const Array<R2Pt> &in_apt, const double in_dScale = 1.0 );
        double Implicit( const R2Pt &in_pt ) const;
        double Shift() const { return m_dMapShift; }
        double Scale() const { return m_dMapScale; }
        R2Pt EdgePt( const int in_iSeg, const double in_dPerc ) const { return m_aSeg[in_iSeg]( in_dPerc ); }
        R3Pt EdgePt3D( const int in_iSeg, const double in_dPerc ) const { return Lerp( m_aptCorners[in_iSeg], m_aptCorners.wrap(in_iSeg+1), in_dPerc ); }
        void SetMap();
        void SetMapExplicit( const double in_dShift, const double in_dScale );
        void SetScale();
        R2Polygon Polygon2D() const;
        
        // debug routine
        void EvalFromEdge( const double in_dImplicit ) const;

        Polygon operator=( const Polygon &in_poly );
        bool operator< ( const Polygon &in_poly ) const { return m_dT < in_poly.m_dT; }
        
        void Draw() const;
        
        Polygon();
        ~Polygon() {}
        
        void Write( ofstream &out ) const;
        void Read( ifstream &in );
        
    };
    
private:
    vector<Polygon> m_apoly;
    
    CurveDrawData m_crvAxisDraw;
    CurveRibbon   m_crvAxis;
    double        m_dAvgRadius, m_dAvgImplicit;
    
    R4Matrix CreateMatrix( const double in_dT ) const;
    double CreateCrossSection( Polygon &io_poly, const Curve &in_curve );
    
public:
    double Implicit( const R3Pt &in_pt ) const;
    
    void SetSelf();
    static double ImplicitStatic( const R3Pt &in_pt );
    
    void Create( const CurveNetwork &in_crvs );
    void BuildSurface();
    void SetCylinderOffset();
    
    void Draw() const;
    void DrawSurface() const { SurfaceData::DrawSurface(); }
    
    Cylinder() : m_crvAxisDraw( m_crvAxis ) {}
    ~Cylinder() {}
    
    void Write( ofstream &out ) const;
    void Read( ifstream &in );
};

#endif
