/*
 *  CurveDrawData_Geometry.cpp
 *  SketchCurves
 *
 *  Created by Cindy Grimm on 9/27/11.
 *  Copyright 2011 Washington University in St. Louis. All rights reserved.
 *
 */

#include "CurveDrawData.h"
#include "DrawState.h"
#include "ParamFile.h"
#include "CurveRibbon.h"
#include "UserInterface.h"

const bool s_bTrace = false;
const bool s_bTraceWebbing = false;

/* View-facing strip. Need top and bottom of strip */
void CurveDrawData::UpdateStrip()
{
    if ( s_bTrace ) cerr << "Updating strip " << m_vecView << "\n";
    
    m_aptTopStrip.resize( GetCurve().NumPts() );
    m_aptBotStrip.resize( GetCurve().NumPts() );
    
    const double dTooSmall = g_drawState.StripWidth() * 0.001;
    
    R3Vec vecUp;
    for ( int i = 0; i < GetCurve().NumPts(); i++ ) {
        vecUp = Cross( m_vecView, m_crv.GetTangentAtPoint(i) );
		const double dLenUp = ::Length( vecUp );
		
		if ( !RNIsZero( dLenUp, dTooSmall ) ) {
			vecUp = vecUp * (1.5 * m_adScale[i] / dLenUp);
		}
        
        m_aptTopStrip[i] = GetCurve().Pt(i) + vecUp;
        m_aptBotStrip[i] = GetCurve().Pt(i) - vecUp;
    }
    
    m_bUpdateView = false;
}

/* Tube geometry. Tapers set by scale. Basically a set of quad strips along curve, slightly offset out */
void CurveDrawData::UpdateTube( ) 
{
    if ( s_bTrace ) cerr << "Updating tube\n";
    
    m_aaptTubeStrip.resize( s_iTubeDivs );
    m_aavecTubeStripNorm.resize( s_iTubeDivs );
    
    for ( int i = 0; i < s_iTubeDivs; i++ ) {
        m_aaptTubeStrip[i].resize( GetCurve().NumPts() );
        m_aavecTubeStripNorm[i].resize( GetCurve().NumPts() );
    }
    
    R3Matrix matRot;
	const double dTAcross = 1.0 / (double) ( s_iTubeDivs - 1.0 );
    for ( int j = 0; j < GetCurve().NumPts(); j++ ) {
        const R3Pt &pt = GetCurve().Pt(j);
        m_aavecTubeStripNorm[0][j] = GetCurve().GetNormalAtPoint(j);
        m_aaptTubeStrip[0][j] = pt + m_aavecTubeStripNorm[0][j] * m_adScale[j];
        
		matRot = R3Matrix::Rotation( GetCurve().GetTangentAtPoint(j), dTAcross * 2.0 * M_PI );

        for ( int i = 1; i < s_iTubeDivs; i++ ) {
            m_aavecTubeStripNorm[i][j] = UnitSafe( matRot * m_aavecTubeStripNorm[i-1][j] );
            m_aaptTubeStrip[i][j] = pt + m_aavecTubeStripNorm[i][j] * m_adScale[j];
        }
    }
    
    m_bUpdateTube = false;
}

// Pairs of indices - where to start and where to end in webbing array
void CurveDrawData::SetWebbingSplits( const Array<double> &in_adPins )
{
    const int iDivs = WINmax( 1, GetCurve().IsClosed() ? in_adPins.num() : in_adPins.num() + 1 );
    m_aiWebbingSplits.resize( iDivs );
    
    if ( s_bTraceWebbing ) cout << "\nSetWebbingSplits: Is closed " << GetCurve().IsClosed() << " " << in_adPins.num() << " pts " << GetCurve().NumPts() << "\n";
    m_bUpdateWebbingSplits = false;
    
    if ( in_adPins.num() == 0 ) {
        if ( s_bTraceWebbing ) cout << "No pins\n";
        // Handles closed or open w/no pins
        ASSERT( m_aiWebbingSplits.size() == 1 );
        m_aiWebbingSplits[0].m_iBracketPt.first = 0; 
        m_aiWebbingSplits[0].m_iBracketPt.second = GetCurve().NumPts();
        m_aiWebbingDraw = m_aiWebbingSplits;
        return;
    }

    // Just in case no pins on the ends
    if ( !GetCurve().IsClosed() ) {
        m_aiWebbingSplits[0].m_iBracketPt.first = 0;
        m_aiWebbingSplits[ m_aiWebbingSplits.size()-1 ].m_iBracketPt.second = GetCurve().NumPts();
    }
    
    // Will set maximum limits for all segments - essentially pin to pin
    for ( int i = 0; i < in_adPins.num(); i++ ) {
        const int iPin = GetCurve().ClosestIndexWrap( in_adPins[i] );
        
        m_aiWebbingSplits[i].m_iBracketPt.second = iPin;
        m_aiWebbingSplits[(i+1)%m_aiWebbingSplits.size()].m_iBracketPt.first = iPin;
    }
    if ( GetCurve().IsClosed() && in_adPins.num() == 1 && GetCurve().NumPts() > 4 ) {
        m_aiWebbingSplits[0].m_iBracketPt.first += 1;
        m_aiWebbingSplits[0].m_iBracketPt.second -= 1;
    }
    
    // Now walk back from pins
    R3Line lineE1, lineE2;
    for ( int iSeg = 0; iSeg < m_aiWebbingSplits.size(); iSeg++ ) {
        const int iPinSave = m_aiWebbingSplits[iSeg].m_iBracketPt.first;
        int iPinEndSave = m_aiWebbingSplits[iSeg].m_iBracketPt.second;
        int &iPin = m_aiWebbingSplits[iSeg].m_iBracketPt.first;
        int &iPinEnd = m_aiWebbingSplits[iSeg].m_iBracketPt.second;

        const R3Pt ptAtPin = GetCurve().Pt(iPin);
        const R3Plane planeAtPin( ptAtPin, GetCurve().GetNormalAtPoint(iPin) );
        const R3Pt ptAtPinEnd = GetCurve().PtClamp(iPinEnd);
        const R3Plane planeAtPinEnd( ptAtPinEnd, GetCurve().GetNormalAtPointClamp(iPinEnd) );

        if ( iPinEnd < iPin ) {
            iPinEnd += GetCurve().NumPts(); 
            iPinEndSave += GetCurve().NumPts(); 
        }
        if ( iPinEnd - iPin < 4 ) {
            continue;
        }
        if ( GetCurve().IsClosed() || iSeg != 0 ) {
            iPin++;
        }
        if ( GetCurve().IsClosed() || iSeg != m_aiWebbingSplits.size()-1 ) {
            iPinEnd--;
        }
        
        // Need to leave at least one segment in place
        double dErrPlaneE1 = planeAtPin.DistToPlane( GetCurve().Pt(iPin+1) );
        double dErrPlaneE2 = planeAtPinEnd.DistToPlane( GetCurve().Pt(iPinEnd-1) );
        const double dAvg = GetCurve().AverageSegLength();
        const double dLineFit = 2.0 * dAvg;
        const double dPlaneFit = 4.0 * dAvg;
        
        if ( s_bTraceWebbing ) cout << "Start " << iPin << " " << iPinEnd << " " << dAvg << " " << dLineFit << " " << dPlaneFit << "\n";
        while ( iPinEnd - iPin > 3 ) {
            double dErrLineE1 = 0.0, dErrLineE2 = 0.0;
            if ( !ApproxEqual( ptAtPin, GetCurve().Pt(iPin+1) ) ) {
                lineE1 = R3Line( ptAtPin, GetCurve().Pt(iPin+1) );

                for ( int i = iPinSave+1; i < iPin; i++ ) {
                    dErrLineE1 = WINmax( dErrLineE1, lineE1.Dist_to_line( GetCurve().Pt(i) ) );
                }
            }
            if ( !ApproxEqual( ptAtPinEnd, GetCurve().Pt(iPinEnd-1) ) ) {
                lineE2 = R3Line( ptAtPinEnd, GetCurve().Pt(iPinEnd-1) );
                for ( int i = iPinEnd+1; i < iPinEndSave; i++ ) {
                    dErrLineE2 = WINmax( dErrLineE2, lineE2.Dist_to_line( GetCurve().Pt(i) ) );
                }
            }
        
            bool bE1Ok = dErrLineE1 < dLineFit && dErrPlaneE1 < dPlaneFit;
            bool bE2Ok = dErrLineE2 < dLineFit && dErrPlaneE2 < dPlaneFit;
            if ( !GetCurve().IsClosed() ) {
                if ( iSeg == 0 ) bE1Ok = false;
                if ( iSeg == m_aiWebbingSplits.size() - 1 ) bE2Ok = false;
            }
            if ( !bE1Ok && !bE2Ok ) {
                break;
            } else if ( !bE1Ok ) {
                iPinEnd--;
                dErrPlaneE2 = WINmax( dErrPlaneE2, planeAtPinEnd.DistToPlane( GetCurve().Pt(iPinEnd) ) );
            } else if ( !bE2Ok || (iPinEnd - iPin) < 4 ) {
                iPin++;
                dErrPlaneE1 = WINmax( dErrPlaneE1, planeAtPin.DistToPlane( GetCurve().Pt(iPin) ) );
            } else {
                iPinEnd--;
                dErrPlaneE2 = WINmax( dErrPlaneE2, planeAtPinEnd.DistToPlane( GetCurve().Pt(iPinEnd) ) );
                iPin++;
                dErrPlaneE1 = WINmax( dErrPlaneE1, planeAtPin.DistToPlane( GetCurve().Pt(iPin) ) );
            }
            //cout << "  " <<dErrLineE1 << " " << dErrLineE2 << " " << dErrPlaneE1 << " " << dErrPlaneE2 << "\n";
        }
        if ( s_bTraceWebbing ) cout << "End " << iPin << " " << iPinEnd << "\n";
        
    }
    if ( s_bTraceWebbing ) {
        for ( int i = 0; i < m_aiWebbingSplits.size(); i++ ) {
            cout << " Pin " << GetCurve().ClosestIndexWrap( in_adPins.wrap(i) ) << " bracket " << m_aiWebbingSplits[i].Start() << ", " << m_aiWebbingSplits[i].Mid() << ", " << m_aiWebbingSplits[i].End() << "\n";
        }
    }
    
    if ( GetCurve().IsClosed() ) {
        m_aiWebbingDraw.resize(0); // has pins
    } else {
        m_aiWebbingDraw.resize(2); // Just do ends
        m_aiWebbingDraw[0].m_iBracketPt.first = 0;
        m_aiWebbingDraw[0].m_iBracketPt.second = m_aiWebbingSplits[0].Mid();
        m_aiWebbingDraw[1].m_iBracketPt.first = m_aiWebbingSplits[iDivs-1].Mid();
        m_aiWebbingDraw[1].m_iBracketPt.second = GetCurve().NumPts();
        if ( s_bTraceWebbing ) {
            cout << "Draw 0," << m_aiWebbingDraw[0].m_iBracketPt.second << " and ";
            cout << m_aiWebbingDraw[1].m_iBracketPt.first << "," << m_aiWebbingDraw[1].m_iBracketPt.second << "\n";
        }
    }
}

const R3Matrix &CurveDrawData::WebbingRotateNormal( const int in_iAroundTube )
{
    static CRVHermite1 s_crvRotNorm;
    static vector<R3Matrix> s_amatRotNorm;
    
    if ( s_crvRotNorm.NumPts() == 0 ) {
        const double dShift = M_PI / 16.0;
        s_crvRotNorm.Add( R1Pt(-M_PI/2.0), R1Vec(0.1) );         // x-axis
        
        s_crvRotNorm.Add( R1Pt(-dShift), R1Vec(0.1) ); // group around straight up 
        s_crvRotNorm.Add( R1Pt(0.0), R1Vec(0.1) );
        s_crvRotNorm.Add( R1Pt(dShift), R1Vec(0.1) );
        
        s_crvRotNorm.Add( R1Pt(M_PI/2.0), R1Vec(0.1) );             // -x-axis
        
        s_crvRotNorm.Add( R1Pt(M_PI - dShift ), R1Vec(0.1) ); // group around straight down
        s_crvRotNorm.Add( R1Pt(M_PI ), R1Vec(0.1) ); 
        s_crvRotNorm.Add( R1Pt(M_PI + dShift ), R1Vec(0.1) ); 
        
        s_crvRotNorm.Add( R1Pt(3.0 * M_PI / 2.0), R1Vec(0.1) );         // x-axis
    }
    if ( s_amatRotNorm.size() != s_iTubeDivs ) {        
        s_amatRotNorm.resize( s_iTubeDivs );
        if ( s_bTraceWebbing ) cout << "Webbing Rotate normal ";
        for ( int i = 0; i < s_iTubeDivs; i++ ) {
            const double dT = (i + 0.5) / (double) s_iTubeDivs;
            s_amatRotNorm[i] = R3Matrix::Rotation(2, s_crvRotNorm(dT)[0] );
            if ( s_bTraceWebbing ) cout << s_crvRotNorm(dT)[0] << " ";
        }            
        
        if ( s_bTraceWebbing ) cout << "\n";
    }
    
    return s_amatRotNorm[in_iAroundTube];
}

const R3Matrix &CurveDrawData::WebbingRotate( const int in_iAroundTube )
{
    static vector<R3Matrix> s_amatRotNorm;
    
    if ( s_amatRotNorm.size() != s_iTubeDivs ) {        
        s_amatRotNorm.resize( s_iTubeDivs );
        if ( s_bTraceWebbing ) cout << "Webbing Rotate ";
        for ( int i = 0; i < s_iTubeDivs; i++ ) {
            const double dT = -M_PI / 2.0 + 2.0 * M_PI * (i + 0.5) / (double) s_iTubeDivs;
            s_amatRotNorm[i] = R3Matrix::Rotation(2, dT);
            if ( s_bTraceWebbing ) cout << dT << " ";
        }            
        if ( s_bTraceWebbing ) cout << "\n";
    }
    
    return s_amatRotNorm[in_iAroundTube];
}

double CurveDrawData::WebbingScaling( const int in_iAroundTube )
{
    static CRVHermite1 s_crv;
    
    const double dHeightTop = 1.25;
    const double dHeightBottom = 0.75;
    const double dWidth = 2.0;
    const double dFallOff = 0.7, dShrink = 0.7;
    if ( s_crv.NumPts() == 0 ) {
        s_crv.Add( R1Pt(dWidth), R1Vec(0.1) );
        s_crv.Add( R1Pt(dShrink * (dHeightTop * dFallOff + dWidth * (1.0 - dFallOff))), R1Vec(0.1) );
        s_crv.Add( R1Pt(dHeightTop), R1Vec(0.1) );
        s_crv.Add( R1Pt(dShrink * (dHeightTop * dFallOff + dWidth * (1.0 - dFallOff))), R1Vec(0.1) );
        s_crv.Add( R1Pt(dWidth), R1Vec(0.1) );
        s_crv.Add( R1Pt(dShrink * (dHeightBottom * dFallOff + dWidth * (1.0 - dFallOff))), R1Vec(0.1) );
        s_crv.Add( R1Pt(dHeightBottom), R1Vec(0.1) );
        s_crv.Add( R1Pt(dShrink * (dHeightBottom * dFallOff + dWidth * (1.0 - dFallOff))), R1Vec(0.1) );
        s_crv.Add( R1Pt(dWidth), R1Vec(0.1) );

        if ( s_bTraceWebbing ) {
            cout << "Webbing scaling ";
            for ( int i = 0; i < s_iTubeDivs; i++ ) {
                const double dT = (i + 0.5) / (double) s_iTubeDivs;
                cout << dT << " ";
            }
            cout << "\n";
            for ( int i = 0; i < s_iTubeDivs; i++ ) {
                const double dT = (i + 0.5) / (double) s_iTubeDivs;
                cout << s_crv(dT) << " ";
            }
            cout << "\n";
        }
    }
    const double dT = (in_iAroundTube + 0.5) / (double) s_iTubeDivs;
    return s_crv(dT)[0];
}

/* Webbing geometry. Tapers set by scale. Basically a set of quad strips along curve, slightly offset out */
void CurveDrawData::UpdateWebbing( ) 
{
    if ( s_bTrace || s_bTraceWebbing ) cerr << "Updating webbing\n";
    
    m_aaptWebbingStrip.resize( s_iTubeDivs );
    m_aavecWebbingStripNorm.resize( s_iTubeDivs );
    
    for ( int i = 0; i < s_iTubeDivs; i++ ) {
        m_aaptWebbingStrip[i].resize( GetCurve().NumPts() );
        m_aavecWebbingStripNorm[i].resize( GetCurve().NumPts() );
    }
    
    R3Matrix matRot, matRotBack;

    for ( int j = 0; j < GetCurve().NumPts(); j++ ) {
        const R3Pt &pt = GetCurve().Pt(j);
        const R3Vec &vecNorm = GetCurve().GetNormalAtPoint(j);
        const R3Vec &vecTang = GetCurve().GetTangentAtPoint(j);
        R3Matrix::MatrixVecToVec(matRot, vecTang, vecNorm, R3Vec(0,0,1), R3Vec(0,1,0));
        matRotBack = matRot.Transpose();

        for ( int i = 0; i < s_iTubeDivs; i++ ) {
            const double dFat = m_adScale[j] * WebbingScaling( i );
            const R3Vec vecToNormY = matRot * vecNorm;
            const R3Vec vecOut = matRotBack * WebbingRotate( i ) * vecToNormY;
            m_aaptWebbingStrip[i][j] = pt + vecOut * dFat;
            
            m_aavecWebbingStripNorm[i][j] = matRotBack * WebbingRotateNormal( i ) * vecToNormY;
            if ( i >= s_iTubeDivs / 2 ) m_aavecWebbingStripNorm[i][j] *= -1;
        }
    }
    
    m_bUpdateWebbing = false;
}

/* Ribbon geometry. Tapers set by scale. Basically a set of quad strips along top of curve, slightly curving down */
void CurveDrawData::UpdateRibbon( ) 
{
    if ( s_bTrace ) cerr << "Updating ribbon\n";
    
    m_aaptRibbonStrip.resize( s_iRibbonDivs );
    m_aavecRibbonStripNorm.resize( s_iRibbonDivs );
    
    for ( int i = 0; i < s_iRibbonDivs; i++ ) {
        m_aaptRibbonStrip[i].resize( GetCurve().NumPts() );
        m_aavecRibbonStripNorm[i].resize( GetCurve().NumPts() );
    }
    
    const double dTAcross = 1.0f / ( s_iRibbonDivs - 1.0 );
    const double dHeight = 2.0 * g_drawState.StripHeight();

    for ( int j = 0; j < GetCurve().NumPts(); j++ ) {
        const R3Pt &pt = GetCurve().Pt(j);
        const R3Vec &vecNorm = GetCurve().GetNormalAtPoint(j);
        const R3Vec &vecTang = GetCurve().GetTangentAtPoint(j);
        
        const R3Vec vecCross = UnitSafe( Cross( vecNorm, vecTang ) );
        
        for ( int i = 0; i < s_iRibbonDivs; i++ ) {
            const double dPerc = 4.0 * (i * dTAcross - 0.5); 
            const double dHeightOff = dHeight * pow( (i * dTAcross - 0.5), 2.0 ); 
            
            if ( j == 0 ) cout << dPerc << " ";
            //m_aaptRibbonStrip[i][j] = pt + ( vecCross * ( dPerc * 4.0 * m_adScale[i] ) - vecNorm * dHeightOff );
            m_aaptRibbonStrip[i][j] = pt + vecCross * ( dPerc * m_adScale[j] ) - vecNorm * dHeightOff;
            m_aavecRibbonStripNorm[i][j] = UnitSafe( vecNorm + vecCross * (0.5 * dPerc) );
        }
    }

    m_bUpdateRibbon = false;
}

/* Shadow geometry. Strip along wall, midline slightly raised */
void CurveDrawData::UpdateShadow( ) 
{
    if ( s_bTrace ) cerr << "Updating shadow\n";
    
    R3Plane aplane[3];
    
    aplane[0] = g_drawState.GetShadowBox().FloorPlane();
    aplane[1] = g_drawState.GetShadowBox().SideWallPlane();
    aplane[2] = R3Plane( R3Pt(0,0,0), R3Vec(0,1,0) );
    
    R3Vec vecDs, vecDt;
    
    for (int iDim = 0; iDim < 3; iDim++ ) {
        const R3Plane &plane = aplane[iDim];
        
        const R3Vec vecPlaneNorm = plane.Normal();
        plane.TangentPlane(vecDs, vecDt);
        vecDt.Normalize();
        const R3Vec vecOffsetMid = vecPlaneNorm * m_dStripWidth * 0.25;	

        std::vector< R3Pt > &aptTop = m_aaptTopShadow[iDim];
        std::vector< R3Pt > &aptBot = m_aaptBotShadow[iDim];
        std::vector< R3Pt > &aptMid = m_aaptMidShadow[iDim];

        aptTop.resize( GetCurve().NumPts() );
        aptBot.resize( GetCurve().NumPts() );
        aptMid.resize( GetCurve().NumPts() );
        
        for ( int i = 0; i < aptTop.size(); i++ ) {
            const R3Vec vecTangWall = Rejection( vecPlaneNorm, GetCurve().GetTangentAtPoint(i) );
            R3Vec vecUp = Cross( vecPlaneNorm, vecTangWall );
            const double dLenUp = ::Length( vecUp );
            
            if ( RNIsZero( dLenUp, 1e-6 ) ) {
                vecUp = vecDt * m_adScale[i];
            } else {
                vecUp = vecUp * (m_adScale[i] / dLenUp);
            }
            
            const R3Pt pt = plane.ProjectOnPlane( GetCurve().Pt(i) ) + vecOffsetMid;
            aptBot[i] = pt - vecUp;
            aptMid[i] = pt + vecOffsetMid;
            aptTop[i] = pt + vecUp;
        }
    }
}
    
void CurveDrawData::UpdateGeometry(  )
{
    if ( m_bUpdateView && g_drawState.m_drawingStyle == DrawState::SCREEN_STROKES ) {
        UpdateStrip();
    }
    
    if ( m_bUpdateTube && g_drawState.m_drawingStyle == DrawState::TUBES ) {
        UpdateTube();
    }
    
    if ( m_bUpdateRibbon && g_drawState.m_drawingStyle == DrawState::RIBBONS ) {
        UpdateRibbon();
    }
    
    if ( m_bUpdateWebbing && g_drawState.m_drawingStyle == DrawState::WEBBING ) {
        UpdateWebbing();
    }
    
    if ( m_bUpdateShadow && g_drawState.m_opUI->m_bShowShadows->value() ) {
        UpdateShadow();
    }
}
