/*
 *  ShadowBox.cpp
 *  SketchCurves
 *
 *  Created by Cindy Grimm on 8/30/10.
 *  Copyright 2010 Washington University in St. Louis. All rights reserved.
 *
 */

#include <utils/Rn_Line_seg.H>
#include <OpenGL/OGLObjs_Camera.H>

#include "ShadowBox.h"
#include "DrawState.h"
#include "ParamFile.h"
#include "UserInterface.h"

/* Get the point at the end of the axis */
R3Pt ShadowBox::GetAxis( const int in_iWhich ) const
{
    switch( in_iWhich ) {
        case 0 : return m_ptCorner + m_vecX;
        case 1 : return m_ptCorner + m_vecY;
        case 2 : return m_ptCorner + m_vecZ;
    }
    return m_ptCorner;
}

// Make a plane out of the back wall
R3Plane ShadowBox::BackWallPlane() const
{
    return R3Plane( m_ptCorner, m_vecZ );
}

// Make a plane out of the side wall
R3Plane ShadowBox::SideWallPlane() const
{
    return R3Plane( m_ptCorner, m_vecX );
}

// Make a plane out of the floor
R3Plane ShadowBox::FloorPlane() const
{
    return R3Plane( m_ptCorner, m_vecY );
}

// Make a plane out of the drawing plane
R3Plane ShadowBox::DrawingPlane() const
{
    return R3Plane( m_ptDrawingPlaneCenter, m_vecPlaneNorm );
}

R3Plane ShadowBox::GetPlane( const Surface in_srfType ) const
{
    switch (in_srfType) {
        case SIDEWALL : return SideWallPlane();
        case FLOOR : return FloorPlane();
        case BACKWALL : return BackWallPlane();
        case DRAWING_PLANE : 
            if ( !g_drawState.m_opUI ) {
                return R3Plane( m_ptDrawingPlaneCenter, -m_vecPlaneNorm );
            }
            if ( !g_drawState.m_opUI->m_bShowDrawingPlane->value() ) {
                return R3Plane( g_drawState.GetCamera().At(), -g_drawState.GetCamera().Look() );
            }
            return R3Plane( m_ptDrawingPlaneCenter, -m_vecPlaneNorm );
        default :
            return R3Plane( m_ptDrawingPlaneCenter, -m_vecPlaneNorm );
    }
    return DrawingPlane();
}

// Reset back to middle
void ShadowBox::ResetDrawingPlane()
{
	SetDrawingPlane( GetCenter(), -m_vecZ );
}

// Set to this point and norm. Try to set the x and y axes
// of the drawing plane to be parallel to the x and y axes of the shadow box
void ShadowBox::SetDrawingPlane( const R3Pt &in_pt, const R3Vec &in_vecNorm )
{
	m_vecPlaneNorm = UnitSafe( in_vecNorm );
	
	const double dAngInBox = Dot( UnitSafe( m_vecZ ), UnitSafe( in_vecNorm ) );
	
	if ( fabs( dAngInBox ) > 0.1 ) {
		m_vecDrawingPlaneX = Rejection( m_vecPlaneNorm, UnitSafe( m_vecX ) );
	} else {
		m_vecDrawingPlaneX = Rejection( m_vecPlaneNorm, UnitSafe( g_drawState.GetCamera().Look() ) );
	}
	m_vecDrawingPlaneY = Cross( m_vecDrawingPlaneX, m_vecPlaneNorm ) ;
	m_vecDrawingPlaneX = UnitSafe(m_vecDrawingPlaneX ) * ::Length( m_vecX ) * 0.5;
	m_vecDrawingPlaneY = UnitSafe( m_vecDrawingPlaneY ) * ::Length( m_vecY ) * 0.5;
	m_ptDrawingPlaneCenter = in_pt;
}

/* Figure out which direction of the given frame is most aligned with the
 * current drawing plane normal and snap it to that, centering on the given point */
void ShadowBox::SnapDrawingPlane( const std::pair< R3Pt, R3Matrix > &in_aFrame )
{
	const R3Vec vecX = in_aFrame.second * R3Vec(1,0,0);
	const double dAngX = fabs( Dot( vecX, m_vecPlaneNorm ) );
	const R3Vec vecY = in_aFrame.second * R3Vec(0,1,0);
	const double dAngY = fabs( Dot( vecY, m_vecPlaneNorm ) );
	const R3Vec vecZ = in_aFrame.second * R3Vec(0,0,1);
	const double dAngZ = fabs( Dot( vecZ, m_vecPlaneNorm ) );
	
	if ( dAngX > dAngY && dAngX > dAngZ ) {
		SetDrawingPlane( in_aFrame.first, vecX );
	} else if ( dAngY > dAngZ ) {
		SetDrawingPlane( in_aFrame.first, vecY );
	} else {
		SetDrawingPlane( in_aFrame.first, vecZ );
	}
}

/* Store the starting point of the edit and which corner. Editing is absolute; basically
 * see if the mouse is forwards or backwards along the normal from the staring place */
static R3Pt s_ptOriginal;
static int s_iWhichCorner;
void ShadowBox::StartDragDrawingPlane( const int in_iWhichCorner )
{
	s_iWhichCorner = in_iWhichCorner;
	
	switch ( in_iWhichCorner) {
		case 0 :
			s_ptOriginal = m_ptDrawingPlaneCenter - m_vecDrawingPlaneX - m_vecDrawingPlaneY;
			break;
		case 1 :
			s_ptOriginal = m_ptDrawingPlaneCenter + m_vecDrawingPlaneX - m_vecDrawingPlaneY;
			break;
		case 2 :
			s_ptOriginal = m_ptDrawingPlaneCenter + m_vecDrawingPlaneX + m_vecDrawingPlaneY;
			break;
		case 3 :
			s_ptOriginal = m_ptDrawingPlaneCenter - m_vecDrawingPlaneX + m_vecDrawingPlaneY;
			break;
		default:
			cerr << "ERR: ShadowBox start drag drawing plane bad corner " << in_iWhichCorner << "\n";
			break;
	}
}

/* Store the starting point of the rotate. Editing is absolute, once the
 * type of edit has been locked. 
 * Once the mouse has moved far enough, lock to rotating (perpendicular to edge
 * of drawing plane) or to spinning (parallel to edge)
 */
/// The original drawing plane vectors; these will be rotated to the new ones
static R3Vec s_vecSaveX, s_vecSaveY, s_vecSaveNorm;
// The vector of the clicked edge in screen space
static R2Vec s_vecScreen;
// Lock the direction as soon as they've moved far enough
static bool s_bLocked = false;
static bool s_bIsAround = false;
void ShadowBox::StartRotateDrawingPlane( const int in_iWhichSide )
{
	s_iWhichCorner = in_iWhichSide - 16;
	
	s_vecSaveX = m_vecDrawingPlaneX;
	s_vecSaveY = m_vecDrawingPlaneY;
    s_vecSaveNorm = m_vecPlaneNorm;
	switch ( s_iWhichCorner) {
		case 0 :
			s_ptOriginal = m_ptDrawingPlaneCenter + m_vecDrawingPlaneX;
			s_vecScreen = g_drawState.GetCamera().CameraPt( m_ptDrawingPlaneCenter - m_vecDrawingPlaneX ) - g_drawState.GetCamera().CameraPt( s_ptOriginal );
			break;
		case 1 :
			s_ptOriginal = m_ptDrawingPlaneCenter - m_vecDrawingPlaneX;
			s_vecScreen = g_drawState.GetCamera().CameraPt( m_ptDrawingPlaneCenter + m_vecDrawingPlaneX ) - g_drawState.GetCamera().CameraPt( s_ptOriginal );
			break;
		case 2 :
			s_ptOriginal = m_ptDrawingPlaneCenter + m_vecDrawingPlaneY;
			s_vecScreen = g_drawState.GetCamera().CameraPt( m_ptDrawingPlaneCenter - m_vecDrawingPlaneY ) - g_drawState.GetCamera().CameraPt( s_ptOriginal );
			break;
		case 3 :
			s_ptOriginal = m_ptDrawingPlaneCenter - m_vecDrawingPlaneY;
			s_vecScreen = g_drawState.GetCamera().CameraPt( m_ptDrawingPlaneCenter + m_vecDrawingPlaneY ) - g_drawState.GetCamera().CameraPt( s_ptOriginal );
			break;
		default:
			cerr << "ERR: ShadowBox start rotate drawing plane bad side " << in_iWhichSide << "\n";
			break;
	}
	s_vecScreen = UnitSafe( s_vecScreen );
    s_bLocked = false;
    s_bIsAround = false;
}

/* Actually move the drawing plane along the normal. Try to keep the square under the cursor.
 * Project a line segment through the corner in the direction of the normal to the screen
 * Project the original and the current mouse point on the segment.
 * Move the drawing plane that far along the corresponding 3D segment
 * Doesn't really get depth values correct, but close enough */
void ShadowBox::DragDrawingPlane( const R2Pt &in_ptDown, const R2Pt &in_ptCur )
{
	const R3Vec vecOffset = m_vecPlaneNorm * (1.2 * ::Length( m_vecDrawingPlaneX ) );
	// 3D segment from the corner in the direction of the normal
	const R3Line_seg seg( s_ptOriginal - vecOffset, s_ptOriginal + vecOffset );
	const OGLObjsCamera &cam = g_drawState.GetCamera();
	// Projected 2D segment
	const R2Line_seg seg2D( cam.CameraPt(seg.P1()), cam.CameraPt(seg.P2()) );
	
	// Find positions on 2D segment
	R2Pt ptClosest;
	double dT = 0.0, dTOrig = 0.0, dDist;
	seg2D.FindPtOnSeg( in_ptCur, ptClosest, dT, dDist );
	seg2D.FindPtOnSeg( in_ptDown, ptClosest, dTOrig, dDist );
	
	// Use same t value to get point on 3D segment
	const R3Pt ptSeg = seg(0.5 + dT - dTOrig);
	// Offset into the center of the plane based on which corner we're moving.
	switch ( s_iWhichCorner ) {
		case 0 :
			m_ptDrawingPlaneCenter = ptSeg + m_vecDrawingPlaneX + m_vecDrawingPlaneY;
			break;
		case 1 :
			m_ptDrawingPlaneCenter = ptSeg - m_vecDrawingPlaneX + m_vecDrawingPlaneY;
			break;
		case 2 :
			m_ptDrawingPlaneCenter = ptSeg - m_vecDrawingPlaneX - m_vecDrawingPlaneY;
			break;
		case 3 :
			m_ptDrawingPlaneCenter = ptSeg + m_vecDrawingPlaneX - m_vecDrawingPlaneY;
			break;
		default:
			cerr << "ERR: ShadowBox drag drawing plane bad corner " << s_iWhichCorner << "\n";
			break;
	}
}

/* Either rotate or spin the drawing plane
 *
 * s_bLocked happens once the mouse is moved far enough. Either spin (mouse along edge)
 * or rotate (mouse perpendicular to edge)
 *
 * once locked, need to figure out what to do. 
 *
 * TODO: This isn't quite right - I don't get the rotate direction correct all of the 
 * time, so the plane doesn't stay under the mouse - ie, the plane may rotate
 * in the wrong direction. 
 */
void ShadowBox::RotateDrawingPlane( const R2Pt &in_ptDown, const R2Pt &in_ptCur )
{
	// s_vecScreen is the direction of the edge on the screen
	const double dDot = Dot( in_ptDown - in_ptCur, s_vecScreen );
    double dAng = dDot * M_PI * 0.5; // Angle with edge
	const double dDotAround = Dot( in_ptDown - in_ptCur, R2Vec( -s_vecScreen[1], s_vecScreen[0] ) );
    double dAngAround = dDotAround * M_PI * 0.5; // Angle with vector perpendicular to edge
    
	// Set locked
    if ( s_bLocked == false ) {
        if ( ::Length( in_ptDown - in_ptCur ) > g_drawState.ScreenSelectionSize() ) {
            s_bLocked = true;
        }
        if ( fabs( dAng ) < fabs( dAngAround ) ) {
            s_bIsAround = true; 
        } else {
            s_bIsAround = false;
        }
    }
	
	// Do one or the other
    if ( s_bIsAround == true ) {
		dAng = 0.0;
    } else {
        dAngAround = 0.0;
    }
	
    const double dDotLook = Dot( s_vecSaveNorm, g_drawState.GetCamera().Look() );
    double dDir = s_iWhichCorner < 2 ? ( s_iWhichCorner % 2 ? -1.0 : 1.0 ) : ( s_iWhichCorner % 2 ? 1.0 : -1.0 ) ;
    if ( dDotLook < 0.0 ) dDir *= -1.0;
    
	const R3Matrix matRot = R3Matrix::Rotation( s_iWhichCorner < 2 ? UnitSafe( s_vecSaveY ) : UnitSafe( s_vecSaveX ), dAng * dDir );
    const double dDirAround = dDotLook < 0.0 ? -1.0 : 1.0;
	const R3Matrix matRotAround = R3Matrix::Rotation( Cross( s_vecSaveX, s_vecSaveY ), dAngAround * dDirAround );
    
	// Rotate the vectors around the normal (dAngAround) or around the plane's x or y vector, depending on what edge they clicked on
    m_vecDrawingPlaneX = ( matRot * matRotAround ) * s_vecSaveX;
    m_vecDrawingPlaneY = ( matRot * matRotAround ) * s_vecSaveY;
	
	m_vecPlaneNorm = Cross( UnitSafe( m_vecDrawingPlaneX ), UnitSafe( m_vecDrawingPlaneY ) );
}

/*
 * This does it's best to place the shadow box in the scene based on the camera.
 * This and the DrawState::CenterCamera routine are tightly linked; if you change that
 * one, it will affect this one.
 *
 * Essentially, the CenterCamera routine places the camera a set distance from the object
 * so that the zoom is roughly the same, depending on the size of the object. This routine
 * relies on the object being that far away and the zoom being set correctly to place the
 * shadow box so that the object is contained inside of it (left-right, up-down, and depth).
 *
 * If the zoom changes there isn't any way to place to shadow box so that it contains the
 * curves and is the right size. Which is why it gets bigger and smaller when changing the
 * zoom.
 *
 * Could possibly have a mode that makes sure the shadow box is always the same size, but lets
 * the curves stick out. See old versions of this code, which used a fixed location for the 
 * corners on the screen and set the sizes from there.
 */
void ShadowBox::AlignBox( const OGLObjsCamera &in_cam, const bool in_bCenterDrawingPlane )
{
	const R3Pt ptCenter = in_cam.At();
	const R3Pt ptFrom = in_cam.From();
	const double dDistBack = ::Length( ptCenter - ptFrom );
	const double dWidth = dDistBack / m_dCameraSetback;
	
	const double dBoxSize = m_dPad * dWidth;
	
	m_vecX = in_cam.Right() * dBoxSize;
	m_vecY = in_cam.Up() * dBoxSize;
	m_vecZ = -UnitSafe( in_cam.Look() ) * dBoxSize;
	m_ptCorner = ptCenter - ( m_vecX + m_vecY + m_vecZ ) * 0.5;
	
	if ( in_bCenterDrawingPlane == false  ) {
		SetDrawingPlane( m_ptDrawingPlaneCenter, m_vecPlaneNorm );
	} else {
		SetDrawingPlane( ptCenter, UnitSafe( m_vecZ ) );
	}
	/*
	 cout << "Align " << ptCenter << " center " << m_ptCorner + ( m_vecX + m_vecY + m_vecZ ) * 0.5 << " ";
	 cout << " corner " << in_cam.CameraPt( m_ptCorner );
	 cout << " top " << in_cam.CameraPt( m_ptCorner + m_vecY );
	 cout << " right " << in_cam.CameraPt( m_ptCorner + m_vecX );
	 cout << " front " << in_cam.CameraPt( m_ptCorner + m_vecZ ) << "\n";
	 */
}

/* Is it over one of the corners? For each sphere, project to the screen, then measure
 * distance to projected point
 *
 * Doesn't correctly handle aspect ratio
 */
int ShadowBox::CursorOverBoxCorner( const R2Pt &in_ptScreen, double &out_dZ ) const
{
	const double dSizeCorner = m_dSizeCorners * g_drawState.PixelWidth();
	
	static std::vector<R3Pt> s_aptCorner(5);
	s_aptCorner[0] = m_ptCorner + m_vecX;
	s_aptCorner[1] = m_ptCorner + m_vecY;
	s_aptCorner[2] = m_ptCorner + m_vecX + m_vecY;
	s_aptCorner[3] = m_ptCorner + m_vecY + m_vecZ;
	s_aptCorner[4] = m_ptCorner + m_vecX + m_vecZ;
	
	const R3Vec vecExtent( UnitSafe( m_vecX ) * dSizeCorner );
	for ( int i = 0; i < s_aptCorner.size(); i++ ) {
		const R2Pt ptCam = g_drawState.GetCamera().CameraPt( s_aptCorner[i] );
		const R2Pt ptCamExtent = g_drawState.GetCamera().CameraPt( s_aptCorner[i] + vecExtent );
		const double dLen = ::Length( in_ptScreen - ptCam );
		const double dLenExtent = ::Length( ptCam - ptCamExtent );
		if ( dLen < dLenExtent ) {
			out_dZ = g_drawState.GetCamera().ProjectedPt( s_aptCorner[i] )[2];;
			return 4 + i;
		}
	}
	return -1;
}

/* Is it over one of the select cylinders on the edge?
 * Create a 3D segment for each cylinder
 * Project the cylinder and see if the cursor projects to the projected 2D segment
 */
int ShadowBox::CursorOverEdge( const R2Pt &in_ptScreen, double &out_dZ ) const
{
	const double dSizeEdge = m_dSizeCorners * g_drawState.PixelWidth();
	const double dLenEdge = m_dSizeAxis * g_drawState.PixelWidth();
	const R3Vec vecX = UnitSafe( m_vecX ) * ( 0.5 * dLenEdge );
	const R3Vec vecY = UnitSafe( m_vecY ) * ( 0.5 * dLenEdge );
	const R3Vec vecZ = UnitSafe( m_vecZ ) * ( 0.5 * dLenEdge );
	
	static std::vector<R3Pt> s_aptEdgeE1(6), s_aptEdgeE2(6);
	s_aptEdgeE1[0] = m_ptCorner + m_vecZ + 0.5 * m_vecX - vecX;
	s_aptEdgeE2[0] = m_ptCorner + m_vecZ + 0.5 * m_vecX + vecX;
	s_aptEdgeE1[1] = m_ptCorner + m_vecY + 0.5 * m_vecX - vecX;
	s_aptEdgeE2[1] = m_ptCorner + m_vecY + 0.5 * m_vecX + vecX;
	s_aptEdgeE1[2] = m_ptCorner + m_vecX + 0.5 * m_vecY - vecY;
	s_aptEdgeE2[2] = m_ptCorner + m_vecX + 0.5 * m_vecY + vecY;
	s_aptEdgeE1[3] = m_ptCorner + m_vecZ + 0.5 * m_vecY - vecY;
	s_aptEdgeE2[3] = m_ptCorner + m_vecZ + 0.5 * m_vecY + vecY;
	s_aptEdgeE1[4] = m_ptCorner + m_vecX + 0.5 * m_vecZ - vecZ;
	s_aptEdgeE2[4] = m_ptCorner + m_vecX + 0.5 * m_vecZ + vecZ;
	s_aptEdgeE1[5] = m_ptCorner + m_vecY + 0.5 * m_vecZ - vecZ;
	s_aptEdgeE2[5] = m_ptCorner + m_vecY + 0.5 * m_vecZ + vecZ;
	
	double dT, dDist;
	R2Pt ptClosest;
	const double dDistExt = ::Length( g_drawState.GetCamera().CameraPt( s_aptEdgeE1[0] ) - g_drawState.GetCamera().CameraPt( s_aptEdgeE1[0] + UnitSafe( m_vecX ) * dSizeEdge ) );
	for ( int i = 0; i < s_aptEdgeE1.size(); i++ ) {
		const R2Line_seg seg( g_drawState.GetCamera().CameraPt( s_aptEdgeE1[i] ), g_drawState.GetCamera().CameraPt( s_aptEdgeE2[i] ) );
		seg.FindPtOnSeg( in_ptScreen, ptClosest, dT, dDist );
		
		if ( dT >= 0.0 && dT < 1.0 && dDist < dDistExt ) {
			out_dZ = g_drawState.GetCamera().ProjectedPt( Lerp( s_aptEdgeE1[i], s_aptEdgeE2[i], 0.5 ) )[2];;
			return 10 + i;
		}
	}
	return -1;
}

/* Is the cursor over the edge of the drawing plane?
 * Create 3D line segments for each cylinder, then intersect the segment with the ray from the camera
 * Keep ones that are within the width and length of the cylinder
 *
 * This is a bit more elaborate (but more accurate) than projecting the cylinders as we did in the
 * box edge case. This is necessary because the draw plane edges can be at odd angles with the view
 * camera.
 */
int ShadowBox::CursorOverDrawingPlaneSide( const R2Pt &in_ptScreen, double &out_dZ ) const
{
	const R3Vec vecRay = g_drawState.GetCamera().RayFromEye( in_ptScreen );
	
	const double dSizeSquare = m_dSizeSquare * g_drawState.PixelWidth();
	
	const R3Line_seg segCamera( g_drawState.GetCamera().From(), g_drawState.GetCamera().From() + vecRay * 1000.0 );
	
	R3Pt       apt[4];
	R3Line_seg aseg[4];
	double     adSs[4], adTs[4], adDists[4];

	aseg[0] = R3Line_seg( m_ptDrawingPlaneCenter + m_vecDrawingPlaneX - m_vecDrawingPlaneY * dSizeSquare * 0.5, 
						  m_ptDrawingPlaneCenter + m_vecDrawingPlaneX + m_vecDrawingPlaneY * dSizeSquare * 0.5 );
	aseg[1] = R3Line_seg( m_ptDrawingPlaneCenter - m_vecDrawingPlaneX - m_vecDrawingPlaneY * dSizeSquare * 0.5, 
						  m_ptDrawingPlaneCenter - m_vecDrawingPlaneX + m_vecDrawingPlaneY * dSizeSquare * 0.5 );
	aseg[2] = R3Line_seg( m_ptDrawingPlaneCenter + m_vecDrawingPlaneY - m_vecDrawingPlaneX * dSizeSquare * 0.5, 
						  m_ptDrawingPlaneCenter + m_vecDrawingPlaneY + m_vecDrawingPlaneX * dSizeSquare * 0.5 );
	aseg[3] = R3Line_seg( m_ptDrawingPlaneCenter - m_vecDrawingPlaneY - m_vecDrawingPlaneX * dSizeSquare * 0.5, 
						  m_ptDrawingPlaneCenter - m_vecDrawingPlaneY + m_vecDrawingPlaneX * dSizeSquare * 0.5 );
	
	int iBest = -1;
	double dBest = 1e30;
	for (int i = 0; i < 4; i++ ) {
		segCamera.Intersect( aseg[i], apt[i], adSs[i], adTs[i] );
		if ( adTs[i] >= 0.0 && adTs[i] <= 1.0 ) {
			adDists[i] = ::Length( segCamera( adSs[i] ) - aseg[i]( adTs[i] ) );
			if ( adDists[i] < dSizeSquare * 0.25 ) {
				if ( iBest == -1 ) {
					iBest = i; 
					dBest = adDists[i]; 
				} else if ( adDists[i] < 0.9 * dBest || ( adDists[i] < dBest * 1.1 && adTs[i] < adTs[iBest] && adTs[i] > 0.0 ) ) {
					iBest = i; 
					dBest = adDists[i]; 
				}
			}
		}
	}

	if ( iBest == -1 ) return -1;
	
	out_dZ = g_drawState.GetCamera().ProjectedPt( aseg[iBest](0.5) )[2];
	
	return 16 + iBest;
}

/* Intersect the ray with the corners of the drawing plane
 * Basically, intersect the ray with the plane and rotate the result so that the drawing plane
 * is aligned with the x,y plane. Then it's just a matter of checking the x,y values to see
 * if they're in the boxes
 */
int ShadowBox::CursorOverDrawingPlaneCorner( const R2Pt &in_ptScreen, double &out_dZ ) const
{
	const R3Vec vecRay = g_drawState.GetCamera().RayFromEye( in_ptScreen );
	if ( RNIsZero( Dot( vecRay, m_vecPlaneNorm ), 1e-4 ) ) {
		return -1;
	}
	
	const double dSizeSquare = m_dSizeSquare * g_drawState.PixelWidth();
	
	const R3Matrix matToOrigin = R3Matrix( UnitSafe( m_vecDrawingPlaneX ), UnitSafe( m_vecDrawingPlaneY ), m_vecPlaneNorm ).Transpose();
	const R3Plane plane( m_ptDrawingPlaneCenter, m_vecPlaneNorm );
	const R3Pt pt = plane.IntersectRay( R3Ray( g_drawState.GetCamera().From(), vecRay ) );
	
	const R3Vec vecPlane = matToOrigin * ( pt - m_ptDrawingPlaneCenter );
	
	const double dLenX = ::Length( m_vecDrawingPlaneX );
	const double dLenY = ::Length( m_vecDrawingPlaneY );
	const double dClipX = dLenX - dSizeSquare;
	const double dClipY = dLenY - dSizeSquare;
	if ( fabs( vecPlane[0] ) < dClipX || fabs( vecPlane[0] ) > dLenX ) {
		return -1;
	}
	if ( fabs( vecPlane[1] ) < dClipY || fabs( vecPlane[1] ) > dLenY ) {
		return -1;
	}
	if ( vecPlane[0] < 0.0 && vecPlane[1] < 0.0 ) {
		// Lower left
		out_dZ = g_drawState.GetCamera().ProjectedPt( m_ptDrawingPlaneCenter - m_vecDrawingPlaneX - m_vecDrawingPlaneY )[2];;
		return 0;
	}
	if ( vecPlane[0] > 0.0 && vecPlane[1] < 0.0 ) {
		// Lower right
		out_dZ = g_drawState.GetCamera().ProjectedPt( m_ptDrawingPlaneCenter + m_vecDrawingPlaneX - m_vecDrawingPlaneY )[2];;
		return 1;
	}
	if ( vecPlane[0] > 0.0 && vecPlane[1] > 0.0 ) {
		// Upper right
		out_dZ = g_drawState.GetCamera().ProjectedPt( m_ptDrawingPlaneCenter + m_vecDrawingPlaneX + m_vecDrawingPlaneY )[2];;
		return 2;
	}
	if ( vecPlane[0] < 0.0 && vecPlane[1] > 0.0 ) {
		// Upper left
		out_dZ = g_drawState.GetCamera().ProjectedPt( m_ptDrawingPlaneCenter - m_vecDrawingPlaneX + m_vecDrawingPlaneY )[2];;
		return 3;
	}
	return -1;
}

/* Cursor over the little boxes in the front of the shadow box.
 * Take the 3D square and project it to 2D, then do a 2D inside-out test */
int ShadowBox::CursorOverShadowBoxSquares( const R2Pt &in_ptScreen, double &out_dZ ) const
{
	const double dSelSize = m_dSizeSquare * g_drawState.PixelWidth();
	const R3Vec vecShiftX = UnitSafe( m_vecX ) * dSelSize;
	const R3Vec vecShiftZ = UnitSafe( m_vecZ ) * dSelSize;
	
	const OGLObjsCamera &cam = g_drawState.GetCamera();
	
	R2Polygon poly(4);
	poly[0] = cam.CameraPt( m_ptCorner + m_vecZ + vecShiftX * 0.5 + vecShiftZ * 0.5 );
	poly[1] = cam.CameraPt( m_ptCorner + m_vecZ + vecShiftX * 1.5 + vecShiftZ * 0.5 );
	poly[2] = cam.CameraPt( m_ptCorner + m_vecZ + vecShiftX * 1.5 + vecShiftZ * 1.5 );
	poly[3] = cam.CameraPt( m_ptCorner + m_vecZ + vecShiftX * 0.5 + vecShiftZ * 1.5 );
	
	if ( poly.Inside( in_ptScreen ) ) {
		out_dZ = cam.ProjectedPt( FloorPlane().IntersectRay( R3Ray( cam.From(), cam.RayFromEye(in_ptScreen) ) ) )[2];
		return 20;
	}
	
	poly[0] = cam.CameraPt( m_ptCorner + m_vecZ + m_vecX - vecShiftX * 1.5 + vecShiftZ * 0.5 );
	poly[1] = cam.CameraPt( m_ptCorner + m_vecZ + m_vecX - vecShiftX * 0.5 + vecShiftZ * 0.5 );
	poly[2] = cam.CameraPt( m_ptCorner + m_vecZ + m_vecX - vecShiftX * 0.5 + vecShiftZ * 1.5 );
	poly[3] = cam.CameraPt( m_ptCorner + m_vecZ + m_vecX - vecShiftX * 1.5 + vecShiftZ * 1.5 );
	
	if ( poly.Inside( in_ptScreen ) ) {
		out_dZ = cam.ProjectedPt( FloorPlane().IntersectRay( R3Ray( cam.From(), cam.RayFromEye(in_ptScreen) ) ) )[2];
		return 21;
	}
	return -1;
}

/* Call each of the above in turn
 * z sorting isn't too hard, because only the draw plane stuff can lie on top of the shadow
 * box stuff */
int ShadowBox::CursorOverSelectable( const R2Pt &in_ptScreen, double &out_dZ ) const
{
	out_dZ = 1e30;
	int iRet = -1;
	if ( g_drawState.m_opUI->m_bShowShadowBox->value()  ) {
		const R3Pt ptEndAxis = m_ptCorner + m_vecZ;
		const R3Pt ptEndAxisEndArrow = ptEndAxis + UnitSafe( m_vecX ) * ( m_dSizeCorners * g_drawState.PixelWidth() );
		const R2Pt ptCam = g_drawState.GetCamera().CameraPt( ptEndAxis );
		const R2Pt ptCamEnd = g_drawState.GetCamera().CameraPt( ptEndAxisEndArrow );
		
		const double dLen = ::Length( ptCam - in_ptScreen );
		const double dLenOutside = ::Length( ptCamEnd - ptCam );
		double dZBox;
		if ( dLen < dLenOutside ) {
			out_dZ = g_drawState.GetCamera().ProjectedPt( ptEndAxis )[2];
			iRet = 9;
		}
	
		const int iBoxCorner = CursorOverBoxCorner( in_ptScreen, dZBox );
		if ( iBoxCorner != -1 ) {
			iRet = iBoxCorner;
			out_dZ = dZBox;
		}
		
		const int iEdge = CursorOverEdge( in_ptScreen, dZBox );
		if ( iEdge != -1 ) {
			iRet = iEdge;
			out_dZ = dZBox;
		}

		const int iSquare = CursorOverShadowBoxSquares( in_ptScreen, dZBox );
		if ( iSquare != -1 ) {
			iRet = iSquare;
			out_dZ = dZBox;
		}
	}
	
	double dZPlane = 1e30;
	if ( g_drawState.m_opUI->m_bShowDrawingPlane->value() ) {
		const int iDrawingPlaneCorner = CursorOverDrawingPlaneCorner( in_ptScreen, dZPlane );
		
		if ( iDrawingPlaneCorner != -1 && dZPlane < out_dZ ) {
			out_dZ = dZPlane;
			return iDrawingPlaneCorner;
		}
		
		const int iSide = CursorOverDrawingPlaneSide( in_ptScreen, dZPlane );
		if ( iSide != -1 && dZPlane < out_dZ ) {
			out_dZ = dZPlane;
			return iSide;
		}
	}
	
	return iRet;
}

/* This is a stand-alone function for just checking if the ray intersects the visible part of the drawing plane */
bool ShadowBox::CursorOverDrawingPlane( const R3Ray &in_ray ) const
{
	if ( RNIsZero( Dot( in_ray.Dir(), m_vecPlaneNorm ), 1e-4 ) ) {
		return false;
	}
	
	const R3Matrix matToOrigin = R3Matrix( UnitSafe( m_vecDrawingPlaneX ), UnitSafe( m_vecDrawingPlaneY ), m_vecPlaneNorm ).Transpose();
	const R3Plane plane( m_ptDrawingPlaneCenter, m_vecPlaneNorm );
	const R3Pt pt = plane.IntersectRay( in_ray );
	
	const R3Vec vecPlane = matToOrigin * ( pt - m_ptDrawingPlaneCenter );
	
	const double dLenX = ::Length( m_vecDrawingPlaneX );
	const double dLenY = ::Length( m_vecDrawingPlaneY );
	if ( fabs( vecPlane[0] ) > dLenX ) {
		return false;
	}
	if ( fabs( vecPlane[1] ) > dLenY ) {
		return false;
	}
	return true;
}


ShadowBox::ShadowBox() : 
m_dSizeCorners(35),
m_dSizeSquare(100),
m_dSizeAxis(50),
m_dCameraSetback(3.0),
m_dPad(1.0),
m_ptDrawingPlaneCenter(1e30, 1e30, 1e30),
m_vecPlaneNorm(0,0,1)
{ 
}

ShadowBox &ShadowBox::operator=( const ShadowBox &in_sb ) 
{
    m_ptCorner = in_sb.m_ptCorner;
    m_vecX = in_sb.m_vecX;
    m_vecY = in_sb.m_vecY;
    m_vecZ = in_sb.m_vecZ;
	m_ptDrawingPlaneCenter = in_sb.m_ptDrawingPlaneCenter;
	m_vecDrawingPlaneX = in_sb.m_vecDrawingPlaneX;
	m_vecDrawingPlaneY = in_sb.m_vecDrawingPlaneY;
	m_vecPlaneNorm = in_sb.m_vecPlaneNorm;
    
	m_dSizeSquare = in_sb.m_dSizeSquare;
	m_dSizeCorners = in_sb.m_dSizeCorners;
	m_dSizeAxis = in_sb.m_dSizeAxis;
	m_dPad = in_sb.m_dPad;
	m_dCameraSetback = in_sb.m_dCameraSetback;
	
    return *this;
}

/* Read in all shadow box parameters (mostly sizes of selection objects and camera padding) and
 * store them so they don't have to be recomputed
 */
void ShadowBox::UpdateParams()
{
	m_dSizeCorners = g_paramFile.GetDouble("ShadowBoxCorner");
	m_dSizeSquare = g_paramFile.GetDouble("ShadowBoxSquare");
	m_dSizeAxis = g_paramFile.GetDouble("ShadowBoxAxis");
	m_dPad = g_paramFile.GetDouble("ShadowBoxPad");
	m_dCameraSetback = g_paramFile.GetDouble("CameraSetBack");
}

