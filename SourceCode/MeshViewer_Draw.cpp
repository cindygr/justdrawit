/*
 *  MeshViewer_Draw.cpp
 *  SketchCurves
 *
 *  Created by Cindy Grimm on 12/29/10.
 *  Copyright 2010 Washington University in St. Louis. All rights reserved.
 *
 */

#include <FL/GL.H>
#include "MeshViewer.h"
#include "UserInterface.h"
#include <OpenGL/OGLDraw3D.H>

void MeshViewer::DrawFloorPlane() const
{
	const double dDivs = g_drawState.m_opUI->m_dGridSpacing->value();
	
	glLineWidth( (GLfloat) 1.5f );
	glColor4f( 0.6f, 0.6f, 0.6f, 1.0f );
	
    glEnable( GL_DEPTH_TEST );
    glDisable( GL_LIGHTING );
    
	glBegin( GL_LINES );
	glNormal3d( 0.0, 1.0, 0.0 );
    const int iDivs = 1 + (int) ( 2.0 * dDivs );
    const double dScaleOut = 3.0;
    const double dSpacing = 2.0 * dScaleOut / (double) iDivs;
	for ( double dX = -dScaleOut; dX < dScaleOut + dSpacing * 0.5; dX += dSpacing ) {
		glVertex3d( dX, 0.0, -dScaleOut );
		glVertex3d( dX, 0.0,  dScaleOut );
		glVertex3d( -dScaleOut, 0.0, dX );
		glVertex3d(  dScaleOut, 0.0, dX );
	}
	
	glEnd();    

	glLineWidth( (GLfloat) 3.0f );
	glColor4f( 1.0f, 0.0f, 0.0f, 1.0f );
	
	glBegin( GL_LINES );
    glVertex3d( -dScaleOut, 0.0, 0.0 );
    glVertex3d(  dScaleOut, 0.0, 0.0 );
    glEnd();

	glColor4f( 0.0f, 0.0f, 1.0f, 1.0f );
	
	glBegin( GL_LINES );
    glVertex3d( 0.0, 0.0, -dScaleOut );
    glVertex3d( 0.0, 0.0,  dScaleOut );
    glEnd();
    
    g_drawState.StartShader( DrawState::SURFACE_SHADING );	
	glColor4f( 0.7f, 0.7f, 0.7f, 0.25f );
    glBegin( GL_QUADS );
    glVertex3d( -dScaleOut, 0.0f, -dScaleOut );
    glVertex3d(  dScaleOut, 0.0f, -dScaleOut );
    glVertex3d(  dScaleOut, 0.0f,  dScaleOut );
    glVertex3d( -dScaleOut, 0.0f,  dScaleOut );
    glEnd();
    g_drawState.EndShader( );
}

void MeshViewer::DrawAxes() const
{
    glEnable( GL_LIGHTING );
    glEnable( GL_DEPTH_TEST );

	g_drawState.StartShader( DrawState::SURFACE_SHADING );	

    glColor4f( 0.0f, 1.0f, 0.0f, 1.0f );
	OGLDrawArrow( R3Pt(0,0,0), 1.0, 0.05 );

    glColor4f( 1.0f, 0.0f, 0.0f, 1.0f );
	glPushMatrix();
    glRotated( -90, 0,0,1 );
	OGLDrawArrow( R3Pt(0,0,0), 0.8, 0.05 );
	glPopMatrix();

    glColor4f( 0.0f, 0.0f, 1.0f, 1.0f );
	glPushMatrix();
    glRotated( -90, 1,0,0 );
	OGLDrawArrow( R3Pt(0,0,0), 0.6, 0.05 );
	glPopMatrix();
    
    glColor4f( 0.5f, 0.5f, 0.5f, 1.0f );
    OGLDrawSphere( R3Sphere( R3Pt(0,0,0), 0.05 ) );
    g_drawState.EndShader( );

}


void MeshViewer::draw()
{
	static WINbool s_bInitd = FALSE;
    
    if ( s_bInitd == FALSE ) {
        /* These initializations need to be delayed until OpenGL is initialized */
		OGLShader2::Init();

        // Set up gl lists for the sphere, cone, cylinder
        OGLDrawSetDivisions(32, 16);
		
		// Toggle the interface into 2D drawing mode
		m_opUI->m_btnDrawing2D->callback()(m_opUI->m_btnDrawing2D, NULL);
		
		// Pass along the variables into the global variable
		g_drawState.m_opUI = m_opUI;
        g_drawState.m_instructions.ChangeRadioButton();
		g_drawState.SetShadowBox( &m_events.SetShadowBox() );
		
		// Read the shader
        g_drawState.ReadShaders();
		
		// This is here to make sure the shadow box is initiaized first
		/// Center the camera at 1,1,1 with size 1
		const std::pair<R3Pt, R3Pt> ptLLUR( R3Pt(0,0,0), R3Pt(2,2,2) );
		g_drawState.CenterCamera( ptLLUR, true );
		// Force the animation to finish
		while ( g_drawState.NextCamera() == false ) ;
		
		// Dump starting state
		m_events.LogGUI(); 
		m_events.LogCamera();

		s_bInitd = TRUE;
    }
    
    glClearColor( 1.0f, 1.0f, 1.0f, 1.0f );
    
    glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );
    
    g_drawState.GetCamera().SetOpenGLCamera();
    
	// Draw interface elements
	if ( m_opUI->m_bShowShadowBox->value() ) {
		m_events.DrawShadowBox();
	}

    m_events.UpdateDrawData();
    
    m_events.DrawCurves(  );
    m_events.DrawTicks(  );
    
    m_events.DrawIntersections();
    m_events.DrawConstraints();
    m_events.DrawSurface();
    m_events.DrawSelectCursor(  );
	m_events.DrawAlignmentAxes();
	m_events.DrawLastStrokeCircles();
	m_events.DrawRepeatSmooth();
	m_events.DrawSnapCylinder();
	m_events.DrawInflationSurface();
	
	if ( m_opUI->m_bShowDrawingPlane->value() ) {
		m_events.DrawDrawingPlane();
	}
    
    if ( m_opUI->m_bShowAxes->value() ) {
        DrawAxes();
    }
    
    if ( m_opUI->m_bShowFloorPlane->value() ) {
        DrawFloorPlane();
    }
    
    if ( m_opUI->m_bShowMesh->value() ) {
        m_cyl.DrawSurface();
    }
    if ( m_opUI->m_bShowCylinder->value() ) {
        m_cyl.Draw();
    }
    
	// Draw menu last
    m_events.DrawMenu();
    
	// Draw curve they're making
	if ( m_events.IsDrawingAction() ) {
		m_events.GetScreenCurve().Draw();
	}

	if ( glGetError() ) {
		cerr << "ERR: Meshviewer draw: GLError on draw " << glGetError() << "\n";
	}
	
	
	s_iCountRedraw--;
}

