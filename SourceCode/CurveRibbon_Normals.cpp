/*
 *  CurveRibbon_Normals.cpp
 *  SketchCurves
 *
 *  Created by Cindy Grimm on 9/20/11.
 *  Copyright 2011 Washington University in St. Louis. All rights reserved.
 *
 */

#include "CurveRibbon.h"
#include <fitting/FITTools_SVD.H>

const bool s_bTrace = false;

const bool CheckValidVec( const R3Vec &in_vec)
{
    if ( isnan( in_vec[0]) == true ) {
        ASSERT(false);
        return false;
    }
    return true;
}

void CurveRibbon::SetNormalsSimple()
{
    m_avecNormal.need( NumPts() );
    
    for ( int i = 0; i < NumPts(); i++ ) {
        m_avecNormal[i] = GetTangentAtPoint(i).Perpendicular();
        CheckValidVec( m_avecNormal[i] );
    }
}

/*
 * Given a list of normals and their point locations, walk around the curve propagating normals
 * Assumes tangents set
 * Assumes a closed curve - first interval is last->first. So if not closed, then set an empty
 * interval as the first interval
 */
void CurveRibbon::PropagateNormals( const Array< R3Vec > &in_avecNorm, const Array< int > &in_aiPts )
{
    R3Matrix matRot;
    
    m_avecNormal.need( NumPts() );

    if ( s_bTrace ) {
        cout << "Normals\n";
        for ( int i = 0; i < in_avecNorm.num(); i++ ) {
            cout << in_aiPts[i] << " " << in_avecNorm[i] << "\n";
        }
    }
    for ( int i = 0; i < in_avecNorm.num(); i++ ) {
        CheckValidVec( in_avecNorm[i] );
    }
    
    /// If closed, then we have one fewer intervals 
    const int iNumIntervals = IsClosed() ? in_avecNorm.num() : in_avecNorm.num() + 1;
    for ( int i = 0; i < iNumIntervals; i++ ) {
        // If closed, then intervals go from previous pin point to this one
        // Otherwise, need to clamp intervals to 0, m_apt.num()
        int iStartInterval = in_aiPts.wrap(i-1);
        int iEndInterval = in_aiPts.clamp(i);
        
        R3Vec vecStart = in_avecNorm.wrap(i-1);
        R3Vec vecEnd = in_avecNorm.clamp(i);
        
        if ( i == 0 ) {
            if ( IsClosed() ) {					
                iEndInterval += NumPts();
            } else {
                vecStart = vecEnd; // Start from front of curve and use first given normal
                iStartInterval = 0; 
            }
        } else if ( i == in_avecNorm.num() ) {
            vecEnd = vecStart; // Take the last valid normal and propagate to the end
            iEndInterval = NumPts(); // only go to end of curve
        }
        
        // If the normals differ enough to have a cross product, rotate around that as an axis
        // Otherwise, if vectors are the same, don't rotate
        // If they point in opposite directions, rotate around best-guess local plane normal
        const R3Vec vecCross = Cross( vecEnd, vecStart );
        const int iIndex = ( (int) ( 0.5 * ( iStartInterval + iEndInterval ) ) + NumPts() ) % NumPts();
        const R3Vec vecAtMid = Pt(iIndex) - CenterPoint();
        const R3Vec vecAxis = Cross( vecStart, vecAtMid );
        const double dLenCross = ::Length( vecCross );
        const double dLenAxis = ::Length( vecAxis );

        const double dDot = Dot( vecStart, vecEnd );
        if ( s_bTrace ) cout << "Dot " << dDot << " " << vecAtMid << " " << vecCross << "\n";
        if ( RNIsZero( dLenCross, 1e-4 ) && RNIsZero( dLenAxis, 1e-4 ) ) {
            R3Matrix::MatrixVecToVec(matRot, vecStart, vecEnd );
        } else if ( RNIsZero( dLenCross, 1e-4 )) {
            matRot = R3Matrix::Rotation( vecAxis, DotToAngle(dDot) );
        } else {
            matRot = R3Matrix::Rotation( vecCross, DotToAngle(dDot) );
        }
        
        const S4Quaternion squatStart( R3Matrix::Identity() );
        const S4Quaternion squatEnd( matRot );
        
        for ( int iI = iStartInterval; iI < iEndInterval; iI++ ) {
            const double dPerc = ( iI - iStartInterval ) * (1.0 / WINmax(1, iEndInterval - iStartInterval - 1));
            const S4Quaternion squat = SLerp( squatStart, squatEnd, dPerc );
            const R3Vec vecInterp = squat.R3RotationMatrix() * vecStart;
            //const R3Vec vecInterp = UnitSafe( vecStart * (1.0 - dPerc) + vecEnd * dPerc );
            const int iIndex = (iI + NumPts()) % NumPts();
            const R3Vec vecNorm = Rejection( GetTangentAtPoint(iIndex), vecInterp );
            
            if ( RNIsZero( ::Length( vecNorm ) ) ) {
                cerr << "Warning: zero cross normal with constraint " << iI << "\n";
                //TODO: Could call recursively with last valid normals
            } else {
                m_avecNormal.wrap(iI) = UnitSafe( vecNorm );
                CheckValidVec( m_avecNormal.wrap(iI) );
            }
        }
    }
}

const double cdFlip = -0.5;
static Array<int> CountFlips( const Array<R3Vec> &in_avecNorms, const int in_iStart, const int in_iEnd ) 
{
    Array<int> aiCount;
    const int iEnd = (in_iEnd < in_iStart) ? in_iEnd + in_avecNorms.num() : in_iEnd;
    for ( int i = in_iStart; i < iEnd-1; i++ ) {
        if ( Dot( in_avecNorms.wrap(i+1), in_avecNorms.wrap(i) ) < cdFlip ) {
            aiCount += i;
        }
    }
    return aiCount;
}

static void FixFlips( const Array<int> &in_aiCount, const Array<R3Vec> &in_avecTangent, Array<R3Vec> &io_avecNormal, const int in_iStart, const int in_iEnd )
{
    // This will insure at most one flip
    const int iEnd = (in_iEnd < in_iStart) ? in_iEnd + in_avecTangent.num() : in_iEnd;
    for ( int i = in_iStart; i < iEnd; i++ ) {
        if ( Dot( io_avecNormal.wrap(i), io_avecNormal.wrap(i-1) ) < cdFlip ) {
            io_avecNormal.wrap(i) *= -1;
        }
    }
    R3Matrix matRot;
    if ( in_aiCount.num() % 2 == 1 && in_iEnd - in_iStart > 3 ) {
        // Add a slow pi rotation
        const double dAng = M_PI / (double) ( iEnd - in_iStart - 1.0 );
        for ( int i = in_iStart; i < iEnd; i++ ) {
            matRot = R3Matrix::Rotation( in_avecTangent.wrap( i ), (i - in_iStart) * dAng );
            
            io_avecNormal.wrap( i ) = matRot * io_avecNormal.wrap( i );
            CheckValidVec( io_avecNormal.wrap( i ) );
        }
    }
}

static void FixFlipsStart( Array<R3Vec> &io_avecNormal, const int in_iEnd )
{
    for ( int i = in_iEnd-1; i >= 0; i-- ) {
        if ( Dot( io_avecNormal[i], io_avecNormal.clamp(i+1) ) < cdFlip ) {
            io_avecNormal[i] *= -1;
        }
    }
}

static void FixFlipsEnd( Array<R3Vec> &io_avecNormal, const int in_iStart )
{
    for ( int i = in_iStart+1; i < io_avecNormal.num(); i++ ) {
        if ( Dot( io_avecNormal[i], io_avecNormal.clamp(i-1) ) < cdFlip ) {
            io_avecNormal[i] *= -1;
        }
    }
}

void CurveRibbon::FixNormalFlips( const Array< int > &in_aiPts )
{
    Array<int> aiCount;
    for ( int iSeg = 0; iSeg < in_aiPts.num() - 1; iSeg++ ) {
        aiCount = CountFlips( m_avecNormal, in_aiPts[0], in_aiPts[iSeg+1] );
        if ( aiCount.num() > 0 ) {
            if ( !IsClosed() && iSeg == 0 ) {
                FixFlipsStart( m_avecNormal, in_aiPts[iSeg+1] );
            } else if ( !IsClosed() && iSeg == in_aiPts.num() - 2 ) {
                FixFlipsEnd( m_avecNormal, in_aiPts[iSeg] );
            } else {
                FixFlips( aiCount, m_avecTangent, m_avecNormal, in_aiPts[iSeg], in_aiPts[iSeg+1] );
            }
        }
    }
    if ( IsClosed() ) {
        aiCount = CountFlips( m_avecNormal, in_aiPts.last(), in_aiPts[0] );
        if ( aiCount.num() > 0 ) {
            FixFlips( aiCount, m_avecTangent, m_avecNormal, in_aiPts.last(), in_aiPts[0] );
        }
    }
}

/*
 * So this is a bit complicated, to say the least.
 * Basically, tangents are set as you would expect (difference of points)
 * If the curve is linear than the view direction or draw plane is used as the normal direction
 * If the curve is planar than the normal is parallel to the plane
 *    Otherwise, fit a plane and pretend its planar.
 * If the curve is closed then the normal points out from the centroids
 *
 * If there are normal constraints set, abandon all of the above and spherically interpolate the 
 * normals between constraints
 */
void CurveRibbon::SetNormalData( const Array< R3Vec > &in_avecNorm, const Array< int > &in_aiPts )
{
    /*
     * First try - take a ray from the centerpoint through each point. Look
     * for places where the tangent and that ray are perpendicular. If
     * at least one of these, propagate normals
     * If straight line, then this will fail
     */
    Array<int> s_aiPins;
    Array<R3Vec> s_avecNorms;
    
    s_aiPins.need(0);
    s_avecNorms.need(0);

    if ( in_aiPts.num() ) {
        s_aiPins = in_aiPts;
        s_avecNorms = in_avecNorm;
    } else {
    
        double dLenLast = 0.0;
        const double dSpacing = AverageSegLength() * ( NumPts() < 20 ? 1.0 : 4.0 );
        
        const R3Pt ptCenter = CenterPoint();
        
        m_avecNormal.need( NumPts() );
        
        R3Vec vecNormLast(0,0,0);
        if ( s_bTrace ) cout << "spacing " << dSpacing << "\n";
        // Make a stab at setting the normals using the vector perpendicular to
        // the tangent and going out from the center of the curve
        for ( int i = 0; i < m_avecNormal.num() - 1; i++ ) {
            R3Vec vecToCenter = Pt(i) - ptCenter;
            const double dLenVecToCenter = ::Length( vecToCenter );
            if ( s_bTrace ) cout << " lenCenter " << dLenVecToCenter << " ";
            
            if ( dLenVecToCenter < dSpacing * 0.25 ) {
                if ( s_bTrace ) cout << "Too small\n";
                continue;
            }
            vecToCenter = vecToCenter / dLenVecToCenter;
            
            const double dDot = fabs( Dot( GetTangentAtPoint(i), vecToCenter ) );
            if ( s_bTrace ) cout << " Dot " << dDot << " lenlast " << dLenLast;
            if ( ( dLenLast > dSpacing || ( s_aiPins.num() == 0 && !IsClosed() ) ) && fabs( dDot ) < 0.7 ) {
                const R3Vec vecNormTry = UnitSafe( Rejection( GetTangentAtPoint(i), vecToCenter ) );
                const double dDotDiff = Dot( vecNormTry, vecNormLast );
                if ( s_bTrace ) cout << " diff " << dDotDiff << " ";
                if ( dDotDiff < 0.9 ) {
                    if ( s_bTrace ) cout << "\n";

                    s_avecNorms += vecNormTry;
                    vecNormLast = s_avecNorms.last();
                    s_aiPins += i;
                    dLenLast = 0.0;
                    if ( s_bTrace ) cout << s_avecNorms.last() << "\n";
                }
            } else { 
                dLenLast += DistanceSegment(i);
            }
        } 
        for ( int i = 1; i < s_avecNorms.num(); i++ ) {
            if ( Dot( s_avecNorms[i], s_avecNorms[i-1] ) < 0.0 ) {
                s_avecNorms[i] *= -1.0;
            }
        }
    }
    
    if ( s_aiPins.num() > 0 ) {
        PropagateNormals( s_avecNorms, s_aiPins );
        s_aiPins = in_aiPts; 
        if ( !IsClosed() ) {
            // Make sure first and last points added
            if ( s_aiPins.num() == 0 ) {
                s_aiPins += 0;
                s_aiPins += m_avecNormal.num();
            } else {
                if ( s_aiPins[0] != 0 ) {
                    s_aiPins.add( 0, 0 );
                }
                if ( s_aiPins.last() != m_avecNormal.num() ) {
                    s_aiPins += m_avecNormal.num();
                }
            }
        } else {
            // Set some sort of split point
            if ( s_aiPins.num() == 0 ) {
                for ( int i = 0; i < m_avecNormal.num(); i++ ) {
                    if ( Dot( m_avecNormal[i], m_avecNormal.wrap(i+1) ) > cdFlip ) {
                        s_aiPins += i+1;
                        s_aiPins += i;
                        break;
                    }
                }
            }
        }
        FixNormalFlips( s_aiPins );
        
    } else {
    
        /* Second try - assume a plane and set the normals to be in
         * that plane
         */
        
        R3Vec vecPlane, vecTang;
        if ( !IsPlanar( vecTang, vecPlane ) ) {
            FITToolsSVDLapack oSVD( NumPts(), 3, true, true );
            
            const R3Pt ptCenter = CenterPoint();
            
            for ( int i = 0; i < NumPts(); i++ ) {
                for ( int j = 0; j < 3; j++ ) {
                    oSVD.A( i, j ) = Pt(i)[j] - ptCenter[j];
                }
            }
            oSVD.Solve();
            
            vecPlane = R3Vec( oSVD.Vt( 2, 0 ), oSVD.Vt( 2, 1 ), oSVD.Vt( 2, 2 ) );
        }
        
        const R3Vec vecOut = UnitSafe( CenterPoint() - R3Pt(0,0,0) );
        if ( fabs( Dot( vecOut, vecPlane ) ) > 0.75 ) {
            vecPlane = vecOut;
        }
        if ( Dot( vecPlane, g_drawState.m_vecView ) < 0.0 ) {
            vecPlane *= -1;
        }
        
        for ( int i = 0; i < m_avecNormal.num(); i++ ) {
            m_avecNormal[i] = Cross( GetTangentAtPoint(i), vecPlane );
            if ( RNIsZero( ::Length( m_avecNormal[i] ) ) ) {
                cerr << "warning: zero cross normal\n";
                m_avecNormal[i] = GetTangentAtPoint(i).Perpendicular();
            } else {
                m_avecNormal[i] = UnitSafe( m_avecNormal[i] );
            }
            CheckValidVec( m_avecNormal[i] );
        }
    }
    if ( IsClosed() ) {
        m_avecNormal.last() = m_avecNormal[0];
    } else if ( m_avecNormal.num() > 1 ) {
        m_avecNormal[ m_avecNormal.num() - 1 ] = m_avecNormal[ m_avecNormal.num() - 2 ];
    }	
}

/* Just smooth the normals some */
void CurveRibbon::SmoothNormals( const double in_dTStart, const double in_dTEnd, const int in_iNLoops )
{
    for ( int iL = 0; iL < in_iNLoops; iL++ ) {
    }
    //TODO
}




