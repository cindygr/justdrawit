// MeshViewer_Logfile.cpp: Playback of logged events
//
// Copyright Feb 25th, 2011, Cindy Grimm, Adobe Corp
//
//////////////////////////////////////////////////////////////////////

#include <FL/GL.H>
#include "MeshViewer.h"
#include "UserInterface.h"
#include <time.h>

#include <FL/Fl.H>
#include <FL/Fl_Shared_Image.H>
#include <FL/Fl_PNG_Image.H>
#define PNG_DEBUG 3
#include <png.h>


/* Keep doing call backs until no more actions.
 * Have to be a little careful in case we invoked a camera action that
 * does an animation - pause while the animation continue, then return
 * to logging playback */
void MeshViewer::Playback_cb( void *in_data )
{
    MeshViewer *opMe = static_cast<MeshViewer *>( in_data );
    
    if ( !opMe->m_events.InPlayback() ) {
        return;
    }
    
    if ( !opMe->m_opUI->m_bPausePlayback->value() ) {
		if ( g_drawState.IsCameraAnimation() ) {
			// If we're still processing the camera animation, wait to do the next action
            Fl::repeat_timeout( 0.1, MeshViewer::Playback_cb, (void *) opMe );
		} else if ( opMe->m_events.NextStepPlayback() ) {
            Fl::repeat_timeout( opMe->m_opUI->m_dPlaybackRate->value(), MeshViewer::Playback_cb, (void *) opMe );
            
            opMe->Redraw();
			// If we happened to do a center camera because of a read, start the camera animation
			if ( g_drawState.IsCameraAnimation() ) {
				opMe->StartCameraAnimation();
			}
        }
    }
	
}

/* Put the first time out on the stack */
void MeshViewer::StartPlayback( const char *in_str )
{
    if ( m_events.StartPlayback( in_str ) ) {
        Fl::add_timeout( 0.001, MeshViewer::Playback_cb, this );
    }
}

/* If they paused, ie the callbacks stopped, re-start */
void MeshViewer::RestartPlayback()
{
    if ( m_events.InPlayback() ) {
        Fl::add_timeout( 0.001, MeshViewer::Playback_cb, this );
    }
}

/* Note: This is copied from http://zarb.org/~gc/html/libpng.html */
static void write_png_file(const char *file_name,
						   const int in_iWidth, 
						   const int in_iHeight, 
						   const png_byte in_iBitDepth,
						   const png_byte in_iColorType,
						   const Array<unsigned char> &aucData)
{
	/* create file */
	FILE *fp = fopen(file_name, "wb");
	if (!fp) {
		cerr << "[write_png_file] File " << file_name << " could not be opened for writing\n";
		return;
	}	
	
	/* initialize stuff */
	png_structp png_ptr = png_create_write_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);
	
	if (!png_ptr) {
		cerr << "[write_png_file] png_create_write_struct failed\n";
		return;
	}
	
	png_infop info_ptr = png_create_info_struct(png_ptr);
	if (!info_ptr) {
		cerr << "[write_png_file] png_create_info_struct failed\n";
		return;
	}
	/*
	if (setjmp(png_jmpbuf(png_ptr))) {
		cerr << "[write_png_file] Error during init_io\n";
		return;
	}
     */
	
	png_init_io(png_ptr, fp);
	
	
	/* write header */
    /*
	if (setjmp(png_jmpbuf(png_ptr))) {
		cerr << "[write_png_file] Error during writing header\n";
		return;
	}
     */
	
	png_set_IHDR(png_ptr, info_ptr, in_iWidth, in_iHeight,
				 in_iBitDepth, in_iColorType, PNG_INTERLACE_NONE,
				 PNG_COMPRESSION_TYPE_BASE, PNG_FILTER_TYPE_BASE);
	
	png_write_info(png_ptr, info_ptr);
	
	
	/* write bytes */
    /*
	if (setjmp(png_jmpbuf(png_ptr))) {
		cerr << "[write_png_file] Error during writing bytes\n";
		return;
	}
     */
	
	Array<png_bytep> row_pointers( in_iHeight );
	for ( int i = 0; i < in_iHeight; i++ ) {
		row_pointers[in_iHeight - i - 1] = (png_bytep) &aucData[ i * 3 * in_iWidth ];
	}
	png_write_image(png_ptr, row_pointers);
	
	
	/* end write */
    /*
	if (setjmp(png_jmpbuf(png_ptr))) {
		cerr << "[write_png_file] Error during end of write\n";
	}
     */
	
	png_write_end(png_ptr, NULL);
	
	fclose(fp);
}

/* Dump the current state, as best as possible
 * Includes an image, the curves, the log file, and the param file
 */
void MeshViewer::DumpFile( const char *in_str ) 
{
	std::string strBase( in_str );
	if ( strBase.find( ".txt" ) != strBase.npos ) {
		strBase.erase( strBase.find( ".txt" ) );
	}

	const int iWidth = g_drawState.GetCamera().Width();
	const int iHeight = g_drawState.GetCamera().Height();
	Array<unsigned char> aucData( iWidth * iHeight * 3 );
	aucData.fill(0);
	glPixelStorei( GL_PACK_ROW_LENGTH, iWidth );
	glPixelStorei( GL_PACK_ALIGNMENT, 1 );
	glPixelStorei( GL_UNPACK_ROW_LENGTH, iWidth );
	glPixelStorei( GL_UNPACK_ALIGNMENT, 1 );
	glPixelStorei( GL_PACK_LSB_FIRST, 1 );
	glPixelStorei( GL_UNPACK_LSB_FIRST, 1 );
	glReadPixels(0, 0, iWidth, iHeight, GL_RGB, GL_UNSIGNED_BYTE, &aucData[0] );
	
	write_png_file( (strBase + std::string(".png") ).c_str(), iWidth, iHeight, 8, PNG_COLOR_TYPE_RGB, aucData);
	
	WriteCurves( ( strBase + "_dump.crv" ).c_str() );
	m_events.LogFileFlush();
	
	const std::string strFromLogfile = m_events.CurrentLogfileName();
	const std::string strToLogFile = strBase + ".txt";
	std::string strCommand = "cp \"" + strFromLogfile + "\" \"" + strToLogFile + "\"";
	cout << strCommand << "\n";
	system( strCommand.c_str() );
	
	const std::string strFromParamfile = g_paramFile.ParamFileName();
	const std::string strToParamFile = strBase + "_params.txt";
	strCommand = "cp " + strFromParamfile + " " + strToParamFile;
	cout << strCommand << "\n";
	system( strCommand.c_str() );
	
}