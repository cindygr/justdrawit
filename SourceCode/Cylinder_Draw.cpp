//
//  Cylinder_Draw.cpp
//  SketchCurves
//
//  Created by Cindy Grimm on 8/12/12.
//  Copyright (c) 2012 Washington University in St. Louis. All rights reserved.
//

#include <iostream>
#include "Cylinder.h"
#include "ParamFile.h"
#include <OpenGL/OGLDraw3D.H>
#include <FL/GL.H>
#include "UserInterface.h"
#include <utils/Rn_Sphere.H>

void Cylinder::Polygon::Draw() const
{
    g_drawState.StartShader( DrawState::SURFACE_SHADING );	
    glEnable( GL_CULL_FACE );
    
    R3Matrix matRot;
    R4Matrix matFull;
    
    const UTILSColor col = g_paramFile.GetColor("CylinderPolygonColor");
    const double dTransp = g_paramFile.GetDouble("CylinderPolygonTransparency");
    const double dSize = g_drawState.StripWidth() * 2.0;
    
    glColor4f( col[0], col[1], col[2], (GLfloat) dTransp );
    for ( int i = 0; i < m_aSeg.num(); i++ ) {
        OGLDrawSphere( R3Sphere( m_aptCorners[i], dSize ) );

        glPushMatrix();
        const double dLen = ::Length( m_aptCorners[i] - m_aptCorners.wrap(i+1) );
        R3Matrix::MatrixVecToVec( matRot, R3Vec(0,1,0), UnitSafe( m_aptCorners.wrap(i+1) - m_aptCorners[i]) );
        matFull = R4Matrix::Translation( m_aptCorners[i] ) * (R4Matrix) matRot;
        glMultMatrixd( &matFull(0,0) );
        OGLDrawCylinder(R3Pt(0,0,0), dLen, dSize * 0.5);
        glPopMatrix();
    }
    
    g_drawState.EndShader();
}

void Cylinder::Draw() const
{
    m_crvAxisDraw.Draw();
    for ( int i = 0; i < m_apoly.size(); i++ ) {
        m_apoly[i].Draw();
    }
}