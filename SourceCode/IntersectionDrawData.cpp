/*
 *  IntersectionDrawData.cpp
 *  SketchCurves
 *
 *  Created by Cindy Grimm on 6/30/12.
 *  Copyright 2012 Washington University in St. Louis. All rights reserved.
 *
 */

#include "IntersectionDrawData.h"
#include "PointConstraint.h"

void IntersectionDrawData::ForceUpdate() 
{ 
    DrawData::ForceUpdate(); 
    m_bUpdateWebbing = m_bUpdateShadow = m_bUpdateColor = true;
}


IntersectionDrawData::IntersectionDrawData( const PointConstraint &in_pin )
: m_pin( in_pin )
{
    ForceUpdate();
}

