/*
 *  CurveGroupCreator.cpp
 *  SketchCurves
 *
 *  Created by Cindy Grimm on 9/11/11.
 *  Copyright 2011 Washington University in St. Louis. All rights reserved.
 *
 */

#include "CurveGroupManager.h"
#include "CurveGroup.h"

int CurveGroupManager::s_iCurveIds = 0;
Array<CurveGroup *> CurveGroupManager::s_aopCrvGrps;

CurveGroup *CurveGroupManager::CreateCurveGroup()
{
    CurveGroup *opOut = new CurveGroup( s_iCurveIds++ );
    s_aopCrvGrps += opOut;
    
    return opOut;
}

void CurveGroupManager::DeleteCurveGroup( CurveGroup *in_opCG )
{
    const int iIndex = s_aopCrvGrps.index( in_opCG );
    if ( iIndex != -1 ) {
        s_aopCrvGrps[iIndex] = NULL;
    }
    delete in_opCG;
}


///
CurveGroup *CreateDuplicateCurveGroup( const CurveGroup &in_crvGrp )
{

    //TODO
    return NULL;
}

const CurveGroup &CurveGroupManager::InvalidCurveGroup() const
{
    static CurveGroup s_crvGrp(-1);
    
    return s_crvGrp;
}

CurveGroupManager::CurveGroupManager()
{
}

CurveGroupManager::~CurveGroupManager()
{
    for ( int i = 0; i < s_aopCrvGrps.num(); i++ ) {
        if ( s_aopCrvGrps[i] ) {
            delete s_aopCrvGrps[i];
        }
    }
}

// This should be the only instance around
CurveGroupManager g_CrvGrpManager;
