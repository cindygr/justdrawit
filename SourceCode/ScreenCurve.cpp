/*
 *  ScreenCurve.cpp
 *  SketchCurves
 *
 *  Created by Cindy Grimm on 9/8/10.
 *  Copyright 2010 Washington University in St. Louis. All rights reserved.
 *
 */

#include "ScreenCurve.h"

const bool s_bTrace = false;


/*
 * Evaluate the curve. in_dT in [0,1]
 */
R2Pt ScreenCurve::operator()( const double in_dT ) const
{
    if ( m_apt.num() == 0 ) return R2Pt(0,0);
	if ( m_apt.num() == 1 ) return m_apt[0];
	
	const std::pair<int,double> ipt = BinarySearch( in_dT );
	
	// Probably don't need clamp, but be safe
    return Lerp( m_apt.clamp(ipt.first), m_apt.clamp(ipt.first+1), ipt.second );
}

/* 
 * Tangent of the curve. 
 * Uses the segment found.
 * If at corner, uses the segment joining previous and next points
 */
R2Vec ScreenCurve::Tangent( const double in_dT ) const
{
    if ( m_apt.num() < 2 ) return R2Vec(1,1);
    
	const std::pair<int,double> ipt = BinarySearch( in_dT );

	if ( RNIsZero( ipt.second ) ) {
		return UnitSafe( m_apt.clamp(ipt.first+1) - m_apt.clamp(ipt.first-1) );
	}
	if ( RNApproxEqual( ipt.second, 1.0 ) ) {
		return UnitSafe( m_apt.clamp(ipt.first+2) - m_apt.clamp(ipt.first) );
	}
	
	return UnitSafe( m_apt.clamp(ipt.first+1) - m_apt.clamp(ipt.first) );
}

/*
 * Figures out which segment the t value lies in
 * in_dT is in [0,1]
 * returns the segment (pt i to pt i+1) and the
 *  distance along that segment [0,1)
 */
std::pair<int,double> ScreenCurve::BinarySearch(const double in_dT) const
{
	if ( m_apt.num() < 1 ) return std::pair<int,double>( 0, 0.0 );
	
	int iLow = 0, iHigh = m_apt.num() - 1;
	int iMid = (int) ( 0.5 * (iHigh + iLow) + 0.5 );
	
	while ( iHigh - iLow > 1 ) {
		const double dPercMid = PercAlong(iMid);
		if ( RNApproxEqual( dPercMid, in_dT ) ) {
			return std::pair<int,double>( iMid, 0.0 );
		} else if ( dPercMid < in_dT ) {
			iLow = iMid;
		} else if ( dPercMid > in_dT ) {
			iHigh = iMid;
		} else {
            ASSERT(FALSE);
        }
        iMid = (int) ( 0.5 * (iHigh + iLow) + 0.5 );
        if ( iMid == iHigh || iMid == iLow ) {
            ASSERT( iHigh - iLow < 2 );
        }
    }
    const double dPercLow = PercAlong(iLow);
    const double dPercHigh = PercAlong(iHigh);
	
    const double dPerc = (in_dT - dPercLow) / (dPercHigh - dPercLow);
    return std::pair<int,double>(iLow, dPerc );
}

int ScreenCurve::Floor(const double in_dT) const
{
    std::pair<int,double> ipt = BinarySearch(in_dT);
    
    if ( RNApproxEqual( ipt.second, 1.0 ) ) {
        return WINmin( ipt.first + 1, NumPts() - 1 );
    }
    
    return ipt.first;
}

int ScreenCurve::Ceil(const double in_dT) const
{
    std::pair<int,double> ipt = BinarySearch(in_dT);
    
    if ( RNApproxEqual( ipt.second, 1.0 ) ) {
        return WINmin( ipt.first + 2, NumPts() );
    }
    
    if ( RNApproxEqual( ipt.second, 0.0 ) ) {
        return ipt.first;
    }
    
    return ipt.first + 1;
}


/*
 * Standard closest point search
 * Returns the t value of the closest point
 *   and the distance to the closest point in out_dDist
 */
double ScreenCurve::ClosestPointTValue( const R2Pt &in_pt, double &out_dDist ) const
{
    R2Line_seg seg;
    
    double dT, dDist;
    double dTBest = 0.0, dDistBest = 1e30;
	
	R2Pt ptClosest;
    for ( int i = 0; i < NumPts() - 1; i++ ) {
        seg = R2Line_seg( m_apt[i], m_apt[i+1] );
        seg.FindPtOnSeg( in_pt, ptClosest, dT, dDist );
        if ( dDist < dDistBest ) {
            dTBest = (1.0 - dT) * PercAlong(i) + dT * PercAlong(i+1);
            //iBest = i;
            dDistBest = dDist;
            out_dDist = dDistBest;
        }
    }
    
    return dTBest;
}


/*
 * More horizontal than vertical?
 */
bool ScreenCurve::Horizontal() const
{
    const R2Vec vec = m_ptUpperRight - m_ptLowerLeft;
    if ( vec[0] > vec[1] )
        return true;
    return false;
}

/*
 * Closed if the first and last point are the same
 */
bool ScreenCurve::IsClosed() const
{
	if ( m_apt.num() == 0 ) {
		return false;
	}
	
	return ApproxEqual( m_apt[0], m_apt.last() );
}

/*
 * All the other properties will be re-built upon read
 */
void ScreenCurve::Write( ofstream &out ) const
{
    out << m_apt.num() << "\n";
    for ( int i = 0; i < m_apt.num(); i++ ) {
        out << " " << m_apt[i] << "\n";
    }
	out << m_dStartTime << " " << m_dEndTime << "\n";
}

void ScreenCurve::Read( ifstream &in )
{
    Clear();
    int iN;
    in >> iN;
    
    R2Pt pt;
    for ( int i = 0; i < iN; i++ ) {
        in >> pt;
        AddPoint( pt );
    }
	in >> m_dStartTime >> m_dEndTime;
}



