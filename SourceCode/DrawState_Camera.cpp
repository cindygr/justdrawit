/*
 *  DrawState_Camera.cpp
 *  SketchCurves
 *
 *  Created by Cindy Grimm on 2/24/2011
 *  Copyright 2011 Adobe corp. All rights reserved.
 *
 */


#include "DrawState.h"
#include "UserInterface.h"
#include "ParamFile.h"

#include <FL/Fl.H>
#include <FL/Fl_Shared_Image.H>
#include <FL/Fl_PNG_Image.H>

const bool s_bTrace = false;
const bool s_bTraceCameraAlign = false;
const bool s_bTraceCameraAnimation = false;

/* Store the initial and final camera data that needs to be interpolated */
static R3Pt s_ptTargetCenter, s_ptCurrentCenter;
static double s_dTargetDistance, s_dCurrentDistance;
static S4Quaternion s_matRotTarget, s_matRotCurrent;
static double s_dTargetScale, s_dCurrentScale;
static double s_dTargetZoom, s_dCurrentZoom;
static double s_dTargetNear, s_dCurrentNear;
static double s_dTargetFar, s_dCurrentFar;
static double s_dTAlong = 1.0;

/* Save the starting camera point; at each time step this camera will be interpolated % along to the new one */
static OGLObjsCamera s_camSave;

/* Store the starting and ending points of the animation. Rotation
 * is interpolated with a slerp, and the at points with linear interpolation
 * Zoom is also linearly interpolated, which looks a little weird, but there you go.
 * Also interpolate near and far planes */
void DrawState::SetCameraAnimationData( const OGLObjsCamera &in_camNew ) const
{
	s_ptCurrentCenter = m_camera.At();	
	s_dCurrentDistance = ::Length( m_camera.From() - m_camera.At() );
	s_matRotCurrent = S4Quaternion( m_camera.GetQuaternion() );
	if ( s_bTraceCameraAnimation ) {
		cout << s_matRotTarget * R3Vec(1,0,0) << m_camera.Right() << "\n";
		cout << s_matRotTarget * R3Vec(0,1,0) << m_camera.Up() << "\n";
		cout << s_matRotTarget * R3Vec(0,0,1) << -m_camera.Look() << "\n";
	}
	s_dCurrentZoom = m_camera.GetZoom();
	s_dCurrentScale = sin( s_dCurrentZoom / 2.0 ) * s_dCurrentDistance * 2.0;
	
	s_dCurrentNear = m_camera.GetNear();
	s_dCurrentFar = m_camera.GetFar();

	s_ptTargetCenter = in_camNew.At();	
	s_dTargetDistance = ::Length( in_camNew.From() - in_camNew.At() );
	s_matRotTarget = S4Quaternion( in_camNew.GetQuaternion() );
	s_dTargetZoom = in_camNew.GetZoom();
	s_dTargetScale = sin( s_dTargetZoom / 2.0 ) * s_dTargetDistance * 2.0;
	
	s_dTargetNear = in_camNew.GetNear();
	s_dTargetFar = in_camNew.GetFar();
	
	if ( s_bTraceCameraAlign && s_bTraceCameraAnimation ) {
		cout << "Starting animation\n";
		m_camera.Print();
		in_camNew.Print();
		cout << s_matRotCurrent << " " << s_matRotTarget << "\n";
		
		cout << s_matRotCurrent.R3RotationMatrix().Transpose() * R3Vec(1,0,0) << " " << m_camera.Right() << "\n";
		cout << s_matRotCurrent.R3RotationMatrix().Transpose() * R3Vec(0,1,0) << " " << m_camera.Up() << "\n";
		cout << s_matRotCurrent.R3RotationMatrix().Transpose() * R3Vec(0,0,1) << " " << -m_camera.Look() << "\n\n";

		cout << s_matRotTarget.R3RotationMatrix().Transpose() * R3Vec(1,0,0) << " " << in_camNew.Right() << "\n";
		cout << s_matRotTarget.R3RotationMatrix().Transpose() * R3Vec(0,1,0) << " " << in_camNew.Up() << "\n";
		cout << s_matRotTarget.R3RotationMatrix().Transpose() * R3Vec(0,0,1) << " " << -in_camNew.Look() << "\n";
	}
	
	s_camSave = m_camera;
	s_dTAlong = 0.0;
}

/*
 * Has s_dTAlong gotten to 1.0 yet? if so, Stop. OTherwise, interpolate the camera between the saved current and target.
 */
bool DrawState::NextCamera()
{
	s_dTAlong += 0.05;

	if ( s_dTAlong > 1.0 + 0.5 * 0.05 ) {
		if ( s_bTraceCameraAnimation ) m_camera.Print();
		return true;
	} 

	const R3Pt ptAt = Lerp( s_ptCurrentCenter, s_ptTargetCenter, s_dTAlong );
	const S4Quaternion quat = SLerp(s_matRotCurrent, s_matRotTarget, s_dTAlong);
	const R3Matrix mat = quat.R3RotationMatrix().Transpose();
	const double dLen = (1.0 - s_dTAlong) * s_dCurrentDistance + s_dTAlong * s_dTargetDistance;
	const double dScale = (1.0 - s_dTAlong) * s_dCurrentScale + s_dTAlong * s_dTargetScale;
	const double dZoom = 2.0 * asin( 0.5 * dScale / dLen );


	m_camera = s_camSave;
	m_camera.SetFromAtUp( ptAt + (mat * R3Vec(0,0,1) * dLen), ptAt, mat * R3Vec(0,1,0) );
	m_camera.SetZoom( dZoom );
	m_camera.SetNearFar( (1.0 - s_dTAlong) * s_dCurrentNear + s_dTAlong * s_dTargetNear, (1.0 - s_dTAlong) * s_dCurrentFar + s_dTAlong * s_dTargetFar );
    UpdateCamera();
	if ( m_opShadowBox ) {
		m_opShadowBox->SetDrawingPlane( m_opShadowBox->GetCenter(), m_camera.Look() );
	}

	if ( s_bTraceCameraAnimation ) {
		cout << "dT along " << s_dTAlong << " " << quat << "\n";
	}
	
	return false;
}

/* Are we doing a camera interpolation? */
bool DrawState::IsCameraAnimation() const
{
	if ( RNIsZero(s_dTAlong) ) 
		return true;
	
	return false;
}


// Screen size is 0.01 of 2.0 on the drawing plane
void DrawState::UpdateCamera()
{
    m_vecView = UnitSafe( GetCamera().Look() );
    m_vecUp = UnitSafe( GetCamera().Up() );
    m_vecRight = UnitSafe( GetCamera().Right() );
    
    const R3Plane plane( GetCamera().At(), m_vecView );
    const R3Ray ray1( GetCamera().From(), GetCamera().RayFromEye(R2Pt(-1.0,0)) );
    const R3Ray ray2( GetCamera().From(), GetCamera().RayFromEye(R2Pt(1.0,0)) );
    const R3Ray ray3( GetCamera().From(), GetCamera().RayFromEye(R2Pt(0.0,0)) );
    
    if ( m_opUI ) {
        const double dWidthFilmPlane = ::Length( plane.IntersectRay( ray1 ) - plane.IntersectRay(ray2) );
		if ( GetCamera().Width() > 0 ) {
			const double dPixelSize = dWidthFilmPlane / GetCamera().Width();
			m_dScreenSize = g_paramFile.GetDouble("ScreenSelectWidthInPixels") * dPixelSize;
			m_dPixelWidth = dPixelSize;
            m_dPixelWidth2D = 2.0 / GetCamera().Width();
		} else {
			m_dScreenSize = 0.1;
            m_dPixelWidth2D = 0.01;
		}
    } else {
        m_dScreenSize = 0.01;
		m_dPixelWidth = 0.1;
        m_dPixelWidth2D = 0.01;
    }
    
	// Make sure the shadow box is tracking the camera
	if ( m_opShadowBox ) {
		m_opShadowBox->AlignBox( GetCamera(), false );
	}
}


/* Zero out the spin and rotate gui values */
void DrawState::ZeroCameraControls( )
{
	m_dLeftRightLast = 0.0;
	m_dUpDownLast = 0.0;

	if ( m_opUI ) {
		m_opUI->m_sldZoom->value( 1.0 - GetCamera().GetZoom() / M_PI );
		m_opUI->m_rolUpDown->value(0.0);
		m_opUI->m_rolLeftRight->value(0.0);
	}
}

// Rotate the camera left right with the dial in the upper left
void DrawState::LeftRight( const double in_d )
{
	m_camera.RotateLeft( (m_dLeftRightLast - in_d) * M_PI );
	m_dLeftRightLast = in_d;
	
	UpdateCamera();
}

// Rotate the camera up down with the dial in the upper left
void DrawState::UpDown( const double in_d )
{
	m_camera.RotateUp( (m_dUpDownLast - in_d) * M_PI );
	m_dUpDownLast = in_d;
	
	UpdateCamera();
}


/*
 * Warning: Changing this will change the way the shadow box works (AlignBox).
 * Changing the near/far parameters will change the way lighting works. Right now
 * the lighting depth is hacked based on the near/far params set here. (Really should
 * calculate the min/max depth of the scene and use that to shade the lighting).
 */
void DrawState::CenterCamera( const std::pair<R3Pt,R3Pt> &in_ptLLUR, const bool in_bCenterDrawingPlane )
{
	const R3Pt ptCenter = Lerp( in_ptLLUR.first, in_ptLLUR.second, 0.5 );
	const R3Vec vec = in_ptLLUR.second - in_ptLLUR.first;
	const double dSize = WINmax( fabs( vec[0] ), WINmax( fabs( vec[1] ), fabs( vec[2] ) ) );
	const double dScale = (RNIsZero(dSize) ? 5.0 : dSize);
	
	const R3Vec vecView = UnitSafe( GetCamera().Look() );
	const double dCameraSetback = g_paramFile.GetDouble("CameraSetBack");
	const double dDistBack = dCameraSetback * dScale;
	
	const double dPad = g_paramFile.GetDouble("CenterCameraPadding");
	const double dZoomX = 2.0 * asin( 0.5 * dScale / (dPad * dDistBack) );
	const double dSclX = 1.0 / tan( dZoomX / 2.0 );
	const double dZoomY = 2.0 * atan( GetCamera().GetAspectRatio() / dSclX );
	
	OGLObjsCamera cam = m_camera;
	cam.SetFromAtUp( ptCenter - vecView * dDistBack, ptCenter, m_camera.Up() );
	cam.SetNearFar( WINmax( 0.001, 0.25 * dDistBack ), WINmax( 1.0, 2.0 * dDistBack ) );
	if ( GetCamera().Width() < GetCamera().Height() ) {
		cam.SetZoom( dZoomX );
	} else {
		cam.SetZoom( dZoomY );
	}
	
	SetCameraAnimationData( cam );
	
	ZeroCameraControls();
	
	UpdateCamera();
}

/* Rotate the camera to be looking down the draw plane normal
 * Choose the side that has most of the curves behind the draw plane
 * If already oriented, flip
 */
void DrawState::CenterCameraOnDrawingPlane( const std::pair<R3Pt,R3Pt> &in_ptLLUR )
{
	if ( !m_opShadowBox ) {
		cerr << "ERR: DrawState::CenterCameraOnDrawingPlane, no shadow box\n";
		return;
	}
	
	if ( RNApproxEqual( fabs( Dot( m_opShadowBox->DrawingPlane().Normal(), GetCamera().Look() ) ), 1.0 ) ) {
		ZeroCameraControls();

		m_camera.SetFromAtUp( m_camera.At() + m_camera.Look() * ::Length( GetCamera().From() - GetCamera().At() ), m_camera.At(), m_camera.Up() );
	} else {
		const R3Pt ptCenter = m_opShadowBox->GetDrawingPlaneCenter();
		R3Vec vecNorm = m_opShadowBox->DrawingPlane().Normal();
		const R3Vec vecDiag = in_ptLLUR.second - in_ptLLUR.first;
		const R3Vec vecToCenter = in_ptLLUR.second - ptCenter;
		if ( Dot(vecNorm, vecDiag) < 0.0 ) {
			vecNorm *= -1.0;
		}
		
		if ( ::Length( vecToCenter ) < 0.5 * ::Length( vecDiag ) ) {
			vecNorm *= -1.0;
		}

		const R3Vec vecLook = UnitSafe( vecNorm ) * ::Length( GetCamera().From() - GetCamera().At() );
		
		R3Vec vecUp = GetCamera().Up();
		
		if ( fabs( Dot( vecUp, vecNorm ) ) > 0.95 ) {
			const R3Vec vecX = UnitSafe( m_opShadowBox->GetDrawingPlaneXVec() );
			const R3Vec vecY = UnitSafe( m_opShadowBox->GetDrawingPlaneYVec() );
			
			vecUp = vecX;
			const R3Vec vecCamUp = m_camera.Up();
			if ( Dot( vecCamUp, vecUp ) < Dot( vecCamUp, - vecX ) ) {
				vecUp = -vecX;
			}
			if ( Dot( vecCamUp, vecUp ) < Dot( vecCamUp, vecY ) ) {
				vecUp = vecY;
			}
			if ( Dot( vecCamUp, vecUp ) < Dot( vecCamUp, -vecY ) ) {
				vecUp = -vecY;
			}
		}		
		OGLObjsCamera cam = m_camera;
		cam.SetFromAtUp( ptCenter - vecLook, ptCenter, vecUp );
		
		SetCameraAnimationData( cam );
	}
	ZeroCameraControls();

	UpdateCamera();
}

/* Pick the view direction that is closest to the current one */
void DrawState::AlignCamera( const R3Matrix &in_mat )
{
	static Array<R3Vec> s_avecOrtho(6);
	s_avecOrtho[0] = in_mat * R3Vec(1,0,0);
	s_avecOrtho[1] = in_mat * R3Vec(-1,0,0);
	s_avecOrtho[2] = in_mat * R3Vec(0,1,0);
	s_avecOrtho[3] = in_mat * R3Vec(0,-1,0);
	s_avecOrtho[4] = in_mat * R3Vec(0,0,1);
	s_avecOrtho[5] = in_mat * R3Vec(0,0,-1);

	if ( s_bTrace ) cout << "Aligning camera\n";
	
	const R3Vec vecLook = GetCamera().Look();
	const R3Vec vecUp = GetCamera().Up();

	int iBestZ = -1, iBestUp = -1;
	double dBestZ = 1e30, dBestUp = 1e30;
	for ( int i = 0; i < s_avecOrtho.num(); i++ ) {
		const double dDot = Dot( s_avecOrtho[i], vecLook );
		const double dAng = DotToAngle( dDot );
		
		if ( dAng < dBestZ ) {
			iBestZ = i;
			dBestZ = dAng;
		}

		const double dDotUp = Dot( s_avecOrtho[i], vecUp );
		const double dAngUp = DotToAngle( dDotUp );
		if ( dAngUp < dBestUp ) {
			iBestUp = i;
			dBestUp = dAngUp;
		}
	}
	if ( iBestZ == iBestUp ) {
		cerr << "ERR: ALign camera, same z and up\n";
		iBestUp = ( iBestZ +1 ) % 6;
	}
	AlignCamera( s_avecOrtho[iBestZ], s_avecOrtho[iBestUp] );
}

/* Use the given look and up vectors. Stay same size */
void DrawState::AlignCamera( const R3Vec &in_vecLook, const R3Vec &in_vecUp )
{
    if ( RNIsZero( ::Length( in_vecLook ) ) || RNIsZero( ::Length( in_vecUp ) ) ) {
        cerr << "ERR: Align camera, vecs are zero length\n";
        return;
    }
    
    const R3Pt ptAt = GetCamera().At();
    const double dLen = ::Length( GetCamera().From() - ptAt );
	
	OGLObjsCamera cam = m_camera;
	cam.SetFromAtUp( ptAt - (in_vecLook * dLen), ptAt, in_vecUp );
	
	SetCameraAnimationData( cam );

    UpdateCamera();

	ZeroCameraControls();
	
}

/* Use the given look vector, and the same up vector if possible. Stay same size */
void DrawState::AlignCamera( const R3Vec &in_vec )
{
    if ( RNIsZero( ::Length( in_vec ) ) ) {
        cerr << "ERR: Align camera, vec is zero length\n";
        return;
    }
    
    R3Vec vecUp(0,1,0);
    
    if ( fabs( in_vec[1] ) > 0.7 ) {
        if ( fabs( in_vec[0] ) > 0.7 ) {
            vecUp = R3Vec(0,0,1);
        } else {
            vecUp = R3Vec(1,0,0);
        }
    }
    
    AlignCamera( in_vec, vecUp );
}

/* Pin the upper left down. Rotate/spin based on motion around point, zoom in and out based on change in distance to pin point */
void DrawState::RotateScale( const R2Pt &in_ptLast, const R2Pt &in_ptNext )
{
	m_camera.RotateScale( in_ptLast, in_ptNext );
	UpdateCamera();

	ZeroCameraControls();
	
}

/* Rotate around up vector by mouse movement */
void DrawState::RotateLeftRight( const R2Pt &in_ptLast, const R2Pt &in_ptNext )
{
	const double dAng = M_PI * (in_ptLast[0] - in_ptNext[0]);
	m_camera.RotateLeft( dAng );
	UpdateCamera();

	ZeroCameraControls();
	
}

/* Rotate around right vector by mouse movement */
void DrawState::RotateUpDown( const R2Pt &in_ptLast, const R2Pt &in_ptNext )
{
	const double dAng = - M_PI * (in_ptLast[1] - in_ptNext[1]);
	m_camera.RotateUp( dAng );
	UpdateCamera();

	ZeroCameraControls();
	
}

/* Rotate around up look by mouse movement */
void DrawState::RotateSpin( const R2Pt &in_ptLast, const R2Pt &in_ptNext )
{
	const double dAngLast = atan2( in_ptLast[1], in_ptLast[0] );
	const double dAngNext = atan2( in_ptNext[1], in_ptNext[0] );
	double dAngChange = dAngNext - dAngLast;
	if ( dAngLast < -0.75 * M_PI && dAngNext > 0.75 * M_PI ) {
		dAngChange = dAngNext - ( dAngLast + 2.0 * M_PI );
	} else if ( dAngLast > 0.75 * M_PI && dAngNext < -0.75 * M_PI ) {
		dAngChange = ( dAngNext + 2.0 * M_PI ) - dAngLast;
	}
	m_camera.SpinClockwise(dAngChange);
	UpdateCamera();

	ZeroCameraControls();
	
}

/* Standard track ball. Somewhat bad */
void DrawState::Trackball( const R2Pt &in_ptLast, const R2Pt &in_ptNext )
{
	//cout << GetCamera().At() << GetShadowBox().GetExtent() << " " << in_ptLast << " " << in_ptNext << "\n";
	
	m_camera.RotateAroundSphere( in_ptLast, in_ptNext, GetCamera().At(), 0.5);
	UpdateCamera();

	ZeroCameraControls();
	
}

/// Pan film plane
void DrawState::Pan( const R2Pt &in_ptLast, const R2Pt &in_ptNext )
{
	m_camera.Pan( in_ptLast, in_ptNext );
	UpdateCamera();

}

/// Save the current camera
void DrawState::StartDollyZoom()
{
	s_camSave = m_camera;
}

/// Change zoom and distance to camera simultaneously
void DrawState::DollyZoom( const R2Pt &in_ptDown, const R2Pt &in_ptNext )
{
	m_camera = s_camSave;
	m_camera.DollyZoom( in_ptDown, in_ptNext );
	UpdateCamera();

	ZeroCameraControls();
	
}

/* Always lower left corner. Just map distance in/out */
void DrawState::Zoom( const R2Pt &in_ptLast, const R2Pt &in_ptNext )
{
	const R2Vec vec(0.2,0.2);
	const R2Vec vecToCursor = in_ptNext - in_ptLast;
	const double dMove = ::Length( vecToCursor ) / ::Length(vec);
	const double dDir = Dot( vec, vecToCursor ) > 0.0 ? 1.0 : -1.0;
	
	const double dZoom = WINminmax( m_camera.GetZoom() + dMove * dDir * (0.25 * M_PI), 0.05 * M_PI, 0.9 * M_PI );
	
	m_camera.SetZoom( dZoom );
	UpdateCamera();
	ZeroCameraControls();	
}

void DrawState::SetZoom( const double in_dZoom )
{
	m_camera.SetZoom( in_dZoom );
	UpdateCamera();
}

void DrawState::ResizeCamera( const int in_iW, const int in_iH )
{
	m_camera.SetSize( in_iW, in_iH );
	UpdateCamera();
}

/* x,y,z,X,Y,Z change to those axes */
void DrawState::HandleKeystroke( const unsigned char in_c, const bool in_bShift, const bool in_bCtrl )
{
	m_camera.HandleKeystroke( in_c, in_bShift, in_bCtrl );
	UpdateCamera();

	ZeroCameraControls();
}

///
void DrawState::ReadCamera( ifstream &in )
{
    if ( in.good() ) {
        m_camera.Read( in );
        UpdateCamera();
    }
}

