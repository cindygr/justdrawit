/*
 *  SurfaceData_Fix.cpp
 *  SketchCurves
 *
 *  Created by Cindy Grimm on 2/18/2011
 *  Copyright 2011 Adobe Corp. All rights reserved.
 *
 */

#include "SurfaceData.h"
#include "ParamFile.h"
#include "PointConstraint.h"

const bool s_bTrace = false;

/* If the snap point is within distance of the end of the curve, erase the end
 * of the curve
 *
 * This is potentially a bit fragile, and may break other constraints, because it changes
 * the arc length parameterization of the curve. I'm also reasonably sure it keeps the
 * intersection point, and throws away everything after it, but haven't really tested
 * it completely to make sure
 */
bool SurfaceData::ClipEnds( CurveNetwork &io_crvNetwork, const int in_iIntersection )
{
    IntersectionPoint &ipt = m_aptIntersection[in_iIntersection];
    
    const double dClip = m_dDistance;
    bool bRet = false;
    
    for ( int i = 0; i < ipt.m_acrvLocs.size(); i++ ) {
        const int iCrvIndex = ipt.GetCurveGroupIndexFromHashId( io_crvNetwork, i );
        const double dT = ipt.m_acrvLocs[i].second;
        
        if ( RNIsZero(dT) || RNApproxEqual( dT, 1.0 ) || iCrvIndex == -1) {
            continue;
        }
        
        const CurveGroup &crvGrp = io_crvNetwork.GetCurveGroup(iCrvIndex);
        
		if ( crvGrp.CompositeCurve().IsClosed() ) {
			continue;
		}
        
        if ( dT < 0.5 ) {
            const double dLeft = dT * crvGrp.CompositeCurve().Length();
            if ( dLeft < dClip && dLeft < 0.15 * crvGrp.CompositeCurve().Length() ) {
                bool bHasConstraintOnEnd = false;
                for ( int iP = 0; iP < crvGrp.NumConstraints(); iP++ ) {
                    if ( crvGrp.GetConstraint(iP).GetTValue( crvGrp ) < dT ) {
                        bHasConstraintOnEnd = true;
                    }
                }
                if ( bHasConstraintOnEnd == false ) {
                    io_crvNetwork.Erase( iCrvIndex, 0.0, dT );
                    bRet = true;
                }
            }
        } else {
            const double dLeft = (1.0 - dT) * crvGrp.CompositeCurve().Length();
            if ( dLeft < dClip && dLeft < 0.15 * crvGrp.CompositeCurve().Length() ) {
                bool bHasConstraintOnEnd = false;
                for ( int iP = 0; iP < crvGrp.NumConstraints(); iP++ ) {
                    if ( crvGrp.GetConstraint(iP).GetTValue( crvGrp ) > dT ) {
                        bHasConstraintOnEnd = true;
                    }
                }
                if ( bHasConstraintOnEnd == false ) {
                    io_crvNetwork.Erase( iCrvIndex, dT, 1.0 );
                    bRet = true;
                }
                bRet = true;
            }
        }
    }
    
    return bRet;
}

/* Reset the stored t values in case the curve changed slightly */
void SurfaceData::ResetTValues( const CurveNetwork &in_crvNetwork )
{
	for ( int i = 0; i < m_aptIntersection.size(); i++ ) {
		// Also resets constraint information
		m_aptIntersection[i].SetNeedsFixing( in_crvNetwork, m_dDistance );
		
		for ( int j = 0; j < m_aptIntersection[i].m_acrvLocs.size(); j++ ) {
            const int iCrvIndex = m_aptIntersection[i].GetCurveGroupIndexFromHashId( in_crvNetwork, j );
            if ( iCrvIndex == -1 ) continue;
            
			const Curve &crv = in_crvNetwork.GetCurve( iCrvIndex );
			if ( !ApproxEqual( m_aptIntersection[i].m_aptPerCurve[j], crv( m_aptIntersection[i].m_acrvLocs[j].second ) ) ) {
				m_aptIntersection[i].m_acrvLocs[j].second = crv.ClosestPointTValue( m_aptIntersection[i].m_pt );
				m_aptIntersection[i].m_aptPerCurve[j] = crv( m_aptIntersection[i].m_acrvLocs[j].second );
			}
		}
	}
	
	if ( s_bTrace )	{
		cout << "Resetting t values\n";
		for ( int i = 0; i < m_aptIntersection.size(); i++ ) {
			m_aptIntersection[i].Print( in_crvNetwork );
		}
		cout << "Done\n";
	}
}

/* Add point and normal constraints where they don't exist, then drag */
void SurfaceData::EstablishCurveConstraints( CurveNetwork &io_crvNetwork, const int in_iIntersection )
{
    if ( in_iIntersection < 0 || in_iIntersection >= m_aptIntersection.size() ) {
		cerr << "ERR: SurfaceData EstablishCurveConstraints bad intersection " << in_iIntersection << "\n";		
		return;
	}
	
	IntersectionPoint &ipt = m_aptIntersection[ in_iIntersection ];
	
	if ( ipt.m_acrvLocs.size() == 2 && ipt.m_acrvLocs[0].first == ipt.m_acrvLocs[1].first ) {
        const int iCrvIndex = ipt.GetCurveGroupIndexFromHashId( io_crvNetwork, 0 );
        const bool bIsEnd1 = RNIsZero( ipt.m_acrvLocs[0].second ) || RNApproxEqual( ipt.m_acrvLocs[0].second, 1.0 );
        const bool bIsEnd2 = RNIsZero( ipt.m_acrvLocs[1].second ) || RNApproxEqual( ipt.m_acrvLocs[1].second, 1.0 );
        if ( bIsEnd1 && bIsEnd2 ) {
			cout << "Closing curve\n";
			io_crvNetwork.CloseCurve( iCrvIndex ); 
			ipt.m_bNeedsFixing = false;
			const CurveRibbon &crv = io_crvNetwork.GetCurveRibbon( iCrvIndex );
			ipt.m_pt = crv.Pt(0);
			ipt.m_aptPerCurve[0] = crv.Pt(0);
			ipt.m_aptPerCurve[1] = crv.Pt( crv.NumPts() - 1 );
			return;
		}
	}
    
    vector<int> aiCrvId;
    vector<double> adCrvId;
    for ( int i = 0; i < ipt.m_acrvLocs.size(); i++ ) {
        const int iCrvId = ipt.GetCurveGroupIndexFromHashId( io_crvNetwork, i );
        if ( iCrvId != -1 ) {
            aiCrvId.push_back( iCrvId );
            adCrvId.push_back( ipt.m_acrvLocs[i].second );
        }
    }

    PointConstraint *opPin = NULL;    
    for ( int i = 1; i < aiCrvId.size(); i++ ) {
        opPin = io_crvNetwork.NewSnapCurves( aiCrvId[0], adCrvId[0], aiCrvId[i], adCrvId[i] );
    }
    
    if ( opPin ) {
        opPin->DragPointConstraint( opPin->GetPt() - ipt.m_pt, false );
        opPin->ConvertToPointAndNormalConstraint( io_crvNetwork.Center() );
    }

	cout << "Fixed constraint\n";
	ipt.Print( io_crvNetwork );
    opPin->Print();
}

/* 
 * First put any point constraints in; if they don't exist, create them and drag
 * the curves together
 * Then clip ends (if necessary) and reset the t values for everyone since the curves may have moved.
 */
void SurfaceData::FixIntersection( CurveNetwork &io_crvNetwork, const int in_iIntersection )
{
    if ( in_iIntersection < 0 || in_iIntersection >= m_aptIntersection.size() ) {
		cerr << "ERR: SurfaceData Fix intersection bad intersection " << in_iIntersection << "\n";		
		return;
	}
	if ( !m_aptIntersection[ in_iIntersection ].IsValid( io_crvNetwork, m_dDistance ) ) {
		cerr << "ERR: Fix intersectionCurves changed; can't find matching intersection\n";
		return;
	}
	
	EstablishCurveConstraints( io_crvNetwork, in_iIntersection );
    RemoveInvalidAndFixed( io_crvNetwork );
    
    // Should be ok, but...
    if ( ClipEnds( io_crvNetwork, in_iIntersection ) ) {
		RemoveInvalidAndFixed( io_crvNetwork );
	}
	
	ResetTValues(io_crvNetwork);
}

void SurfaceData::FixAllIntersections( CurveNetwork &io_crvNetwork )
{
	for ( int i = 0; i < m_aptIntersection.size(); i++ ) {
		if ( RNIsZero( m_aptIntersection[i].m_dDistance ) && m_aptIntersection[i].m_bNeedsFixing ) {
			FixIntersection( io_crvNetwork, i );
		}
	}
	for ( int i = 0; i < m_aptIntersection.size(); i++ ) {
		if ( m_aptIntersection[i].m_bNeedsFixing ) {
			FixIntersection( io_crvNetwork, i );
		}
	}
}


