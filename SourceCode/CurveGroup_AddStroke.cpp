/*
 *  CurveGroup_AddStroke.cpp
 *  SketchCurves
 *
 *  Created by Cindy Grimm on 1/20/11.
 *  Copyright 2011 Adobe Corp.. All rights reserved.
 *
 */


#include "CurveGroup.h"
#include "CurveOverlap.h"
#include "ScreenCurve.h"
#include "DrawState.h"
#include "PointConstraint.h"
#include <OpenGL/OGLObjs_Camera.H>

const bool s_bTraceDecisions = false;

std::vector<ScreenCurve> CurveGroup::GetBlendStack() const
{
	static std::vector<ScreenCurve> s_acrv, s_acrvTemp;
	s_acrv.resize(0);
	for ( int i = m_aStrokeHistory.num() - 2; i >= 0; i-- ) {
		if ( m_aStrokeHistory[i].GetMergeType() != StrokeHistory::BLEND_OVERSTROKE ) {
			return s_acrv;
		}
		
		
		s_acrvTemp = CurveOverlap::SplitAtFoldbacks( m_aStrokeHistory[i].Stroke2D() );
		for ( int i = 0; i < s_acrvTemp.size(); i++ ) {
			s_acrv.push_back( s_acrvTemp[i] );
		}
	}
	return s_acrv;
}

void CurveGroup::AddBlendToHistory( const ScreenCurve &in_crv )
{
	if ( m_aStrokeHistory.num() == 0 ) {
		cerr << "ERR CurveGroup::AddBlendToHistory, no history!\n";
		return;
	}
	
	// Use the original stroke, not the processed one
	StrokeHistory sh;
	sh.BlendScratch(in_crv, m_aStrokeHistory.last().DrawPlane() );
	m_aStrokeHistory.addItem( sh, m_aStrokeHistory.num() - 1 );
}

void CurveGroup::AddBlendOverstroke( const ScreenCurve &in_crv )
{
	if ( m_aStrokeHistory.num() == 0 ) {
		cerr << "ERR CurveGroup::AddBlendOverstroke, no history!\n";
		return;
	}
	
	// Replace the last saved action with the blend
	m_aStrokeHistory.last().BlendOverstroke( in_crv );
}


/*
 * This is called when the user forces a merge/join/blend/oversketch of the last stroke with this curve
 * This means we already know the desired action; it's just a matter of figuring out the best merge/join/blend values
 */
bool CurveGroup::AddStrokeForced( const StrokeHistory::MergeType &in_action, const ScreenCurve &in_crv2D, const bool in_bAllowClosed )
{
	
	CurveOverlap crvOverlap( CompositeCurve(), in_crv2D );
	crvOverlap.PrintState();

	if ( s_bTraceDecisions ) { cout << "Adding stroke, Forced: "; crvOverlap.PrintState(); }
	
	const ScreenCurve crv2DReverse = in_crv2D.Reverse();
	switch ( in_action ) {
		case StrokeHistory::BLEND_OVERSTROKE :
			/*
			 TODO: Remove forced blend
			m_aStrokeHistory.add(1);
			m_acrv += in_crv3D;
			m_aStrokeHistory.last().Blend( m_aStrokeHistory, m_acrv, crvOverlap );
			m_aStrokeHistory.last().SetConstructionHistory( in_crv2D, g_drawState.DrawingPlane() );
			 */
			break;
			
		case StrokeHistory::MERGE :
		case StrokeHistory::JOIN :
			// If we aren't already a merge or join, will try to find best match
			crvOverlap.FindBestMergeOrJoin( in_bAllowClosed );
			break;
			
		case StrokeHistory::CLOSE :
			// If we aren't already a merge or join, will try to find best match
			crvOverlap.FindBestClosed( );
			break;
			
		case StrokeHistory::OVERSKETCH :
			// If we aren't already an oversketch, will try to find start and stop points
			crvOverlap.FindBestOverstroke( );
			break;
			
		default :
			assert(false);
			return false;
	}

	return AddStroke( crvOverlap, in_crv2D );
}


/* 
 * This looks at how to add the stroke into the curve. Assumes curve overlap is already created.
 *   This is called when the system determines that the new stroke best matches this curve
 *
 * Options are: 
 *   1) Blend the entire stroke
 *   2) Merge on either end
 *   3) Join on either end
 *   4) Over sketch
 *   5) Close a curve
 *
 * This method calls the appropriate AddZValue method to promote the stroke to 3D, then
 * adds a StrokeHistory instance to the stack. The StrokeHistory instance is responsible
 * for actually building the composite curve
 */
bool CurveGroup::AddStroke( const CurveOverlap &in_crvOverlap, const ScreenCurve &in_crv2D )
{
	if ( s_bTraceDecisions ) { cout << "\nAdding stroke, handled: "; in_crvOverlap.PrintState(); }
	
	bool bRet = false;
	const ScreenCurve crv2DReverse = in_crv2D.Reverse();
	const Curve crvComposite = CompositeCurve();
	Curve crvStroke3D;

	switch ( in_crvOverlap.GetOverlapType() ) {
		case CurveOverlap::BLEND :	
			// Processed as an overstroke - this is post blending with the last stroke
		case CurveOverlap::OVERSTROKE :
			crvStroke3D = AddZValueOversketch( in_crvOverlap, crvComposite, ( in_crvOverlap.ReverseStroke() ? crv2DReverse : in_crv2D ) );
			
			m_aStrokeHistory.add(1);
			m_aStrokeHistory.last().Overstroke( in_crvOverlap, crvComposite, crvStroke3D, false, m_crvRibbon );			
			m_aStrokeHistory.last().SetConstructionHistory( in_crv2D, g_drawState.DrawingPlane() );
			break;
			
		case CurveOverlap::MERGE :
			crvStroke3D = AddZValueExtend( in_crvOverlap, crvComposite, ( in_crvOverlap.ReverseStroke() ? crv2DReverse : in_crv2D ) );

			m_aStrokeHistory.add(1);
			m_aStrokeHistory.last().Merge( in_crvOverlap, crvComposite, crvStroke3D, m_crvRibbon );			
			m_aStrokeHistory.last().SetConstructionHistory( in_crv2D, g_drawState.DrawingPlane() );
				
			if ( in_crvOverlap.CurveEnd() == CurveOverlap::END_CURVE ) {
				bRet = true;
			}
			break;
			
		case CurveOverlap::OVERSTROKE_START :
			crvStroke3D = AddZValueOversketch( in_crvOverlap, crvComposite, ( in_crvOverlap.ReverseStroke() ? crv2DReverse : in_crv2D ) );

			m_aStrokeHistory.add(1);
			m_aStrokeHistory.last().Overstroke( in_crvOverlap, crvComposite, crvStroke3D, true, m_crvRibbon );			
			m_aStrokeHistory.last().SetConstructionHistory( in_crv2D, g_drawState.DrawingPlane() );
			
			bRet = true;
			break;
			
		case CurveOverlap::JOIN :
			if ( crvComposite.IsClosed() ) {
				cerr << "ERR: Joining closed curve\n";
			}
			crvStroke3D = AddZValueExtend( in_crvOverlap, crvComposite, ( in_crvOverlap.ReverseStroke() ? crv2DReverse : in_crv2D ) );

			m_aStrokeHistory.add(1);			
			m_aStrokeHistory.last().Join( in_crvOverlap, crvComposite, crvStroke3D, m_crvRibbon );
			m_aStrokeHistory.last().SetConstructionHistory( in_crv2D, g_drawState.DrawingPlane() );
			
			if ( in_crvOverlap.CurveEnd() == CurveOverlap::END_CURVE ) {
				bRet = true;
			}
			break;
			
		case CurveOverlap::CLOSES :
			crvStroke3D = AddZValueClosed( in_crvOverlap, crvComposite, ( in_crvOverlap.ReverseStroke() ? crv2DReverse : in_crv2D ) );

			m_aStrokeHistory.add(1);
			m_aStrokeHistory.last().Close( in_crvOverlap, crvComposite, crvStroke3D, m_crvRibbon );
			m_aStrokeHistory.last().SetConstructionHistory( in_crv2D, g_drawState.DrawingPlane() );
			break;
			
		default :
			// Can happen after a blend
			NewCurve( in_crv2D );
			break;
	}
	
    GeometryChanged( m_aStrokeHistory.last().GetMergeType() );
    
	return bRet;
}

/*
 * An overstroke made up of multiple strokes.
 * Merge/join this stroke into the existing overstroke
 * Then call overstroke
 */
bool CurveGroup::AddMultistrokeOverstroke( const CurveOverlap &in_crvOverlap, const ScreenCurve &in_crv2D )
{
	bool bRet = false;
	
	// The 3D version of the 2D stroke that was added to the curve as an overstroke	
	const Curve crvLast3DStroke = m_aStrokeHistory.last().Stroke3D();
	
	// Now take the input 2D curve and merge it into the last 3D stroke
	StrokeHistory shMerge;
	CurveOverlap crvOverlap( crvLast3DStroke, in_crv2D, false );
	crvOverlap.FindBestMergeOrJoin( false );
	
	//Store the result
    Curve crvMergeWithOverstroke;
	
	const Curve crvStrokeMerged = AddZValueExtend( crvOverlap, crvLast3DStroke, ( crvOverlap.ReverseStroke() ? in_crv2D.Reverse() : in_crv2D ) );
	shMerge.Merge(crvOverlap, crvLast3DStroke, crvStrokeMerged, crvMergeWithOverstroke );
	
	const OGLObjsCamera &cam = g_drawState.GetCamera();
	
	// Now take the 3D merged curve and project back to 2D to add into the original curve
	ScreenCurve crvScreen;
	for ( int i = 0; i < crvMergeWithOverstroke.NumPts(); i++ ) {
		crvScreen.AddPoint( cam.CameraPt( crvMergeWithOverstroke.Pt(i) ), i );
	}
	
	// Now search back for the curve before we started multi-stroke overstroking
	Curve crvOrig;
	for ( int i = m_aStrokeHistory.num() - 1; i >= 0; i-- ) {
		crvOrig = m_aStrokeHistory[i].Final();
		if ( m_aStrokeHistory[i].GetMergeType() != StrokeHistory::OVERSKETCH_START && m_aStrokeHistory[i].GetMergeType() != StrokeHistory::BLEND_SCRATCH ) {
			break;
		}
	}
	
	CurveOverlap crvOverlapOverstroke( crvOrig, crvScreen, true );
	m_aStrokeHistory.add(1);
	if ( crvOverlapOverstroke.GetOverlapType() == CurveOverlap::OVERSTROKE ) {
		const Curve crvStroke3D = AddZValueOversketch( crvOverlapOverstroke, crvOrig, crvOverlapOverstroke.ReverseStroke() ? crvScreen.Reverse() : crvScreen );
		m_aStrokeHistory.last().Overstroke( crvOverlapOverstroke, crvOrig, crvStroke3D, false, m_crvRibbon );	
		if ( s_bTraceDecisions ) cout << "Finished multistroke overstroke\n";
		
	} else if ( crvOverlapOverstroke.GetOverlapType() == CurveOverlap::OVERSTROKE_START ) {
		const Curve crvStroke3D = AddZValueOversketch( crvOverlapOverstroke, crvOrig, crvOverlapOverstroke.ReverseStroke() ? crvScreen.Reverse() : crvScreen );
		m_aStrokeHistory.last().Overstroke( crvOverlapOverstroke, crvOrig, crvStroke3D, true, m_crvRibbon );					
		bRet = true;

		if ( s_bTraceDecisions ) cout << "Continuing multistroke overstroke\n";
	} else if ( crvOverlapOverstroke.GetOverlapType() == CurveOverlap::MERGE ) {
		const Curve crvStroke3D = AddZValueExtend( crvOverlapOverstroke, crvOrig, crvOverlapOverstroke.ReverseStroke() ? crvScreen.Reverse() : crvScreen );
		m_aStrokeHistory.last().Merge( crvOverlapOverstroke, crvOrig, crvStroke3D, m_crvRibbon );					
		bRet = true;

		if ( s_bTraceDecisions ) cout << "Multistroke became merge\n";

	} else {
		crvOverlapOverstroke.FindBestOverstroke();
		const Curve crvStroke3D = AddZValueOversketch( crvOverlapOverstroke, crvOrig, crvOverlapOverstroke.ReverseStroke() ? crvScreen.Reverse() : crvScreen );
		m_aStrokeHistory.last().Overstroke( crvOverlapOverstroke, crvOrig, crvStroke3D, false, m_crvRibbon );					
		
		if ( s_bTraceDecisions ) cout << "Multistroke: forcing overstroke\n";
	}
	// Store the actual 2D stroke, not the faked-up one, for undo
	m_aStrokeHistory.last().SetConstructionHistory( in_crv2D, g_drawState.DrawingPlane() );

    GeometryChanged( m_aStrokeHistory.last().GetMergeType() );
    
	return bRet;
	
}

/*
 * Combine the two curves together
 *
 * The deleted curve will be stored in the graveyard so it can be resurrected later
 * Store which pins were on the old curve so they can be put back in on an undo
 */
void CurveGroup::Combine( const CurveOverlap &in_crvOverlapMe, 
						        CurveGroup &in_crvGrp, const CurveOverlap &in_crvOverlapOther, 
						  const ScreenCurve &in_crv2D )
{
	const Curve crvComposite = m_crvRibbon;
	
	m_aStrokeHistory.add(1);
	const Curve crvStroke3D = AddZValueCombine( in_crvOverlapMe, crvComposite, in_crvOverlapOther, in_crvGrp.CompositeCurve(), in_crv2D );
	
	m_aStrokeHistory.last().Combine( in_crvOverlapMe, crvComposite, 
                                     in_crvOverlapOther, in_crvGrp.CompositeCurve(), 
                                     crvStroke3D, 
                                     in_crvGrp.HashId(),
                                     in_crvGrp.m_aopPins,
                                     m_crvRibbon );
	m_aStrokeHistory.last().SetConstructionHistory( in_crv2D, g_drawState.DrawingPlane() );
    
    GeometryChanged( StrokeHistory::COMBINE );
}

/*
 * This would be a simple matter of just getting rid of the last stroke history action to the previous
 * time step, except for two cases:
 *
 * Combine: Have to reconstruct the second curve/resurrect it from the graveyard. This info
 * is carried in StrokeHistory, along with the 2D curve
 * Blend: If there was a blend, have to pop off last two actions
 */
StrokeHistory CurveGroup::RollBackLastStroke(  )
{
	static StrokeHistory sh;
	
    if ( m_aStrokeHistory.num() == 0 ) {
        cerr << "ERR: CurveGroup::RollBackLastStroke, empty stack!\n";
        return sh;
    }

	// Go back until first stroke history with last time stamp
	int iDel = m_aStrokeHistory.num() - 1;
    for ( int i = m_aStrokeHistory.num() - 1; i >= 0; i-- ) {
        // See if we have a remove/add due to an undo in the stack history
        if ( i > 0 && m_aStrokeHistory[i].GetTimeStamp() == m_aStrokeHistory[i-1].GetTimeStamp() ) {
            iDel = i-1;
            continue;
        }
        break;
    }

    sh = m_aStrokeHistory[ iDel ];
	
	m_aStrokeHistory.sub( m_aStrokeHistory.num() - iDel );
	
	if ( iDel != 0 ) {
		m_crvRibbon.SetToCurve( m_aStrokeHistory.last().Final() );
	} else {
		m_crvRibbon.Clear();
	}
	
	return sh;

	// Geometry changed will be called by calling function
	/*
	if ( m_aStrokeHistory.num() <= 1 ) {
		m_aStrokeHistory.need(0);
		return true;
	}
	
	const bool bIsBlend = ( m_aStrokeHistory.num() > 1 && m_aStrokeHistory[ m_aStrokeHistory.num() - 2 ].IsBlend() );
	
	if ( m_aStrokeHistory.last().GetMergeType() == StrokeHistory::COMBINE ) {
		// On a combine, the blend will be added to the stroke history after combining
		out_crvSplit = *this;
		int iSplit = 0;
		for ( int i = 0; i < m_aStrokeHistory.num(); i++ ) {
			if ( m_aStrokeHistory[i].GetMergeType() == StrokeHistory::FIRST_STROKE ) {
				if ( i != 0 && m_aStrokeHistory[i-1].GetMergeType() == StrokeHistory::BLEND_SCRATCH ) {
					iSplit = i-1;
				} else {
					iSplit = i;
				}
			}
		}
		m_aStrokeHistory.sub( m_aStrokeHistory.num() - iSplit );
		
		out_crvSplit.m_aStrokeHistory.del( 0, iSplit );
		out_crvSplit.m_aStrokeHistory.sub( 1 );
		if ( bIsBlend ) 
			out_crvSplit.m_aStrokeHistory.sub(1);

        if ( m_aStrokeHistory.num() ) {
            m_crvRibbon.SetToCurve( m_aStrokeHistory.last().Final() );
        }
        
        if ( out_crvSplit.m_aStrokeHistory.num() ) {
            out_crvSplit.m_crvRibbon.SetToCurve( out_crvSplit.m_aStrokeHistory.last().Final() );
        }

	} else {
		m_aStrokeHistory.sub(1);
		if ( bIsBlend )
			m_aStrokeHistory.sub(1);
        
        if ( m_aStrokeHistory.num() ) {
            m_crvRibbon.SetToCurve( m_aStrokeHistory.last().Final() );
        }
        
	}
	
    if ( m_aStrokeHistory.num() ) {
        GeometryChanged(  m_aStrokeHistory.last().GetMergeType()  );
    }

	return m_aStrokeHistory.num() == 0 ? true : false;
	 */
}

