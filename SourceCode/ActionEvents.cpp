/*
 *  ActionEvents.cpp
 *  SketchCurves
 *
 *  Created by Cindy Grimm on 9/9/10.
 *  Copyright 2010 Washington University in St. Louis. All rights reserved.
 *
 */

#include "ActionEvents.h"
#include "PointConstraint.h"
#include "UserInterface.h"

const bool s_bTrace = false;
const bool s_bTraceUndo = true;

char ActionEvents::s_astrAction[ActionEvents::NUM_ACTIONS+1][50] = 
{
"NO_ACTION",
"MENU", 
"MENU_DRAG", 
"MENU_SELECT_CURVE",
"MENU_SELECT_CURVE_REGION",
"CAMERA_SCALE_ROTATE",
"CAMERA_PAN",
"CAMERA_DOLLY_ZOOM",
"CAMERA_UP_DOWN",
"CAMERA_LEFT_RIGHT",
"CAMERA_SPIN",
"CAMERA_ZOOM",
"CAMERA_TRACKBALL", 
"CAMERA_ALIGN",
"DRAG_CURVE", 
"ROTATE_CURVE",
"SCALE_CURVE",
"ROTATE_SCALE_CURVE",
"EDIT_CURVE_NORMAL",
"EDIT_CURVE_POINT_CONSTRAINT",
"EDIT_INTERSECTION_NORMAL",
"SMOOTHING",
"DRAWING", 
"EXTRUSION_SURFACE",           
"INFLATION_SURFACE",           
"DRAWPLANE_ALIGN",
"DRAWPLANE_DRAG",           
"DRAWPLANE_ROTATE",           
"NUM_ACTIONS"
};

char ActionEvents::s_astrSelection[ActionEvents::NUM_SELECTION_TYPES+1][50] = 
{
"NO_SELECTION",
"CURVE",
"POINT_CONSTRAINT",
"NORMAL_CONSTRAINT",
"POTENTIAL_INTERSECTION",
"SHADOW_BOX",
"LAST_STROKE",
"SMOOTHING_CIRCLE",
"INFLATION_SURFACE",
"ALIGN_CAMERA",
"DRAG",
"PERPENDICULAR_DRAG",
"ROTATE",
"ROTATE_AND_SCALE",
"SCALE",
"NUM_SELECTION_TYPES"
};

/*
 * Very brain-dead undo stack. Just save curves and surface data.
 */
void ActionEvents::PushUndo( const int in_iCrv, const StrokeHistory::MergeType in_action )
{
    CurveNetwork::IncrementTime();

    int iCrv = -1;
    if ( in_iCrv >= 0 && in_iCrv < m_crvNetwork.NumCrvs() ) {
        iCrv = m_crvNetwork.GetCurveGroup( in_iCrv ).HashId();
    }
    m_aUndo += pair<R2Pt_i, StrokeHistory::MergeType>( R2Pt_i( iCrv, CurveNetwork::CurrentTime() ), in_action );
    
    if ( s_bTraceUndo ) {
        cout << "Time " << CurveNetwork::CurrentTime() << " curve " << in_iCrv << " action ";
        cout << StrokeHistory::GetMergeTypeString( in_action ) << "\n";
    }
}

/*
 * Get two points and the Frenet frame at those points
 *
 * Points are the mouse down and current mouse location
 *
 * Take a vector between the two points. The plane should contain this line. For
 * the plane normal, use the average of the tangents of the two curves at those points _unless_ this
 * vector points too much in the direction of the line between the points, in which
 * case use the normal of the curves.
 *
 * Be a little careful averaging vectors, in case they point in the opposite direction
 *
 * Points can be from the same curve
 */
void ActionEvents::SetDrawingPlaneTwoPoints()
{
	int iCurveStart = 0;
	double dCurveStart = 0.0;
	
	CurveSelected( m_rayMenuDown, iCurveStart, dCurveStart );
	
	if ( iCurveStart == -1 || !OverCurve() ) {
		cerr << "ERR: SetDrawingPlaneTwoPoints, no curve " << iCurveStart << " " << m_iCursorOverCurve << "\n";
		return;
	}
	
    const Curve &crv1 = m_crvNetwork.GetCurve(iCurveStart);
    const Curve &crv2 = m_crvNetwork.GetCurve(m_iCursorOverCurve);
	
    const R3Vec vecBetweenRaw = crv1(dCurveStart) - crv2( m_dCursorOverCurve );
    const R3Vec vecBetween = UnitSafe( crv1(dCurveStart) - crv2( m_dCursorOverCurve ) );
	const R3Vec vecTang1 = crv1.Tangent(dCurveStart);
	const R3Vec vecTang2 = crv2.Tangent(m_dCursorOverCurve);
	const R3Vec vecTang = Dot( vecTang1, vecTang2 ) < 0.0 ? UnitSafe( vecTang1 - vecTang2 ) : UnitSafe( vecTang1 + vecTang2 );
	const double dDotTang = fabs( Dot( vecBetween, vecTang ) );
    
	if ( RNIsZero( ::Length( vecBetweenRaw ), 1e-6 ) ) {
		m_shadowbox.SetDrawingPlane( crv1(dCurveStart), vecTang );
	} else {
	
		if ( dDotTang < 0.75 ) {
			m_shadowbox.SetDrawingPlane( Lerp( crv1(dCurveStart), crv2( m_dCursorOverCurve ), 0.5 ), UnitSafe( Rejection( vecBetween, vecTang ) ) );
		} else {
			m_shadowbox.SetDrawingPlane( Lerp( crv1(dCurveStart), crv2( m_dCursorOverCurve ), 0.5 ), UnitSafe( Cross( vecTang1, vecTang2 ) ) );
		}
	}
}

/*
 * If the curve is planar, snap the plane to the plane the curve is embedded in
 *
 * If not, pick one of the tangent/norm vectors at the selected point, depending
 * on which is closest to the draw plane
 */
void ActionEvents::SetDrawingPlaneBestFit()
{
	if ( !OverCurve() ) {
		cerr << "ERR: SetDrawingPlaneBestFit, no curve " << m_iCursorOverCurve << "\n";
		return;
	}
	const CurveRibbon &crv = m_crvNetwork.GetCurveRibbon(m_iCursorOverCurve);
	
	R3Vec vecLine, vecNormal;
	if ( crv.IsPlanar(vecLine, vecNormal) ) {
		m_shadowbox.SetDrawingPlane( crv( m_dCursorOverCurve ), vecNormal );
	} else {
		m_shadowbox.SnapDrawingPlane( crv.Frame( m_dCursorOverCurve ) );
	}
}

/*
 * Follow where the mouse drew, finding all curves within selection distance
 * Accumulate into a bounding box
 * Snap the camera to that bounding box
 */
void ActionEvents::CenterCameraRegion()
{
    std::pair<R3Pt,R3Pt> bbox( R3Pt(1e30, 1e30, 1e30), R3Pt( -1e30, -1e30, -1e30 ) );
    
    g_drawState.CenterCamera(bbox, false);
    
    if ( m_curveTempScreen.NumPts() < 4 ) {
        g_drawState.CenterCamera(m_crvNetwork.BoundingBox(), false);
    }
	ScreenCurve crvResample( m_curveTempScreen );
	crvResample.FillGaps( g_drawState.ScreenSelectionSize() * 0.5 );
	
    const double dTSample = g_drawState.ScreenSelectionSize() / crvResample.Length();
    const OGLObjsCamera &cam = g_drawState.GetCamera();
    int iWhich = 0;
    double dWhich = 0.0;
    const double dDistAllowed = g_drawState.SelectionSize();
    int iFound = 0;
    for ( double dT = 0.0; dT < 1.0; dT += dTSample ) {
        const R3Ray ray( cam.From(), cam.RayFromEye( crvResample(dT) ) );
        const double dDist = CurveSelected( ray, iWhich, dWhich );
        if ( dDist < dDistAllowed ) {
            const R3Pt pt = m_crvNetwork.GetCurve(iWhich)(dWhich);
            for ( int j = 0; j < 3; j++ ) {
                bbox.first[j] = WINmin( bbox.first[j], pt[j] );
                bbox.second[j] = WINmax( bbox.second[j], pt[j] );
            }
            iFound++;
        }
    }
	
    if ( iFound > 2 ) {
        g_drawState.CenterCamera(bbox, false);
    } else {
        g_drawState.CenterCamera(m_crvNetwork.BoundingBox(), false);
    }
}

/*
 * Put the current action/menu item into the gui boxes in the upper left
 */
void ActionEvents::UpdateGUIState() const
{
	g_drawState.m_opUI->m_strAction->value( s_astrAction[ m_action ] );
	g_drawState.m_opUI->m_strMenu->value( s_astrMenu[ m_menuSelection ] );
	g_drawState.m_opUI->m_strLastAction->value( m_crvNetwork.LastStrokeAction().c_str() );
	g_drawState.m_opUI->m_strSelection->value( s_astrSelection[ m_activeSelection ] );
	g_drawState.m_opUI->m_strTap->value( m_bIsTap ? "True" : "False" );
    
    char strN[5];
    sprintf( strN, "%d", m_iCursorOverCurve );
	g_drawState.m_opUI->m_iCurveSel->value( strN );
}

/*
 * camera action 
 */
bool ActionEvents::IsCameraAction() const
{
	if ( m_action >= CAMERA_SCALE_ROTATE && m_action <= CAMERA_ALIGN )
		return true;
	
	return false;
}

/* 
 * Actually drawing curve or on inflation surface
 */
bool ActionEvents::IsDrawingAction() const
{
	if ( Fl::event_shift() ) {
		return false;
	}
	if ( m_action == DRAWING || m_action == NO_ACTION ) {
		return true;
	}
    if ( m_action == INFLATION_SURFACE && m_inflationSrfEditAction != InflationSurface::NO_EDIT ) {
        return true;
    }
    if ( m_action == EXTRUSION_SURFACE && m_inflationSrfEditAction != InflationSurface::NO_EDIT ) {
        return true;
    }
	return false;
}


/* Not completely up to date - doesn't dump complete state */
void ActionEvents::PrintState() const
{
    cout << " Action " << s_astrAction[m_action] << " ";
	PrintMenuState();

    cout << "Number of curves " << m_crvNetwork.NumCrvs() << " Selected ";
    if ( OverCurve() ) {
		cout << " over " << m_iCursorOverCurve << " " << m_dCursorOverCurve;
		if ( IsSubset() ) cout << " region " << m_dCursorOverCurveTs.first << " " << m_dCursorOverCurveTs.second << "\n";
	}
	if ( m_iCursorOverIntersection != -1 ) {
		cout << " Intersection " << m_iCursorOverIntersection;
	}
	if ( m_opCursorOverCurveNormalConstraint != NULL ) {
		cout << " NormalConstraint ";
        m_opCursorOverCurveNormalConstraint->Print();
	}
	if ( m_opCursorOverCurvePointConstraint != NULL ) {
		cout << " PointConstraint ";
        m_opCursorOverCurvePointConstraint->Print();
	}
	if ( m_iCursorOverShadowBox != -1 ) {
		cout << " Shadow box " << m_iCursorOverShadowBox;
	}
	if ( m_bCursorOverLastStroke ) {
		cout << " Over stroke ";
	} 
	if ( m_bCursorOverLastSmooth ) {
		cout << " Over smooth ";
	} 
	if ( m_iAlignCamera != -1 ) {
		cout << " Axis " << m_iAlignCamera;
	} 
	cout << "\n";
}

void ActionEvents::ParamDataChanged()
{
    m_crvNetwork.ParamDataChanged();
}

const bool ActionEvents::s_bTraceCurveSelection = false;

ActionEvents::ActionEvents()
:
m_action( NO_ACTION ),
m_pushAction( NO_ACTION ),
m_menuSelection( NO_MENU_SELECTED ),
m_dCursorOverCurve(-1.0),
m_iCursorOverShadowBox(-1),
m_iCursorOverCurve(-1),
m_iCursorOverIntersection(-1),
m_bInFocus( false ),
m_dCursorOverCurveTs( std::pair<double,double>(0.5,0.5) ),
m_vecAlignCameraLook(0,0,1),
m_vecAlignCameraUp(0,1,0),
m_iAlignCamera(-1),
m_bIsTap(false),
m_opCursorOverCurveNormalConstraint( NULL ),
m_opCursorOverCurvePointConstraint( NULL ),
m_bCursorOverLastStroke(false),
m_bCursorOverLastSmooth(false),
m_inflationSrfEditAction( InflationSurface::EDIT_LEFT_CURVE ),
m_bIsPerpendicularDrag(false),
m_iSnapCurveFirst(-1,0.0),
m_actionDragClick( NO_ACTION, ShadowBox::DRAWING_PLANE ),
m_activeSelection(NO_SELECTION)
{
    m_amatAlignAxis.resize(6);

    ClearCurveSelected();

    m_aUndo.need(60);
    m_aUndo.need(0);
}

ActionEvents::~ActionEvents()
{
    if ( m_outLogFile.good() ) {
        EndLogging();
        
    }
    if ( m_inLogFile.good() ) m_inLogFile.close();
    
    WriteProfile( true );
}

/* Probably only need to write the curves, but having the intersection data around is handy */
void ActionEvents::Write( ofstream &out ) const
{
    m_crvNetwork.Write(out);
    m_srfData.Write(out);
}

void ActionEvents::WriteMayaCurves( const string &in_strFile ) 
{
    SetMayaCurvesDirectory( in_strFile );
    m_crvNetwork.WriteMaya( in_strFile );
}

void ActionEvents::WriteAdobeCurves( const string &in_strFile ) 
{
    SetAdobeCurvesDirectory( in_strFile );
    m_crvNetwork.WriteAdobe( in_strFile );
}

void ActionEvents::WriteCurvesAsOffFile( const char *in_str )
{
    m_crvNetwork.WriteCurveTubeGeometry( string(in_str) );
}

void ActionEvents::Read( ifstream &in )
{
    m_crvNetwork.Read(in);
    // If point constraints aren't visible, show them
    if ( g_drawState.m_opUI && !g_drawState.m_opUI->m_bShowCurvePointConstraints->value() && m_crvNetwork.NumConstraints() ) {
        g_drawState.m_opUI->m_bShowCurvePointConstraints->value(1);
        g_drawState.m_opUI->m_bShowCurveNormalConstraints->value(1);
        LogGUI();
    }
    
    if ( !in.eof() ) {
        m_srfData.Read(in);
    } else {
		m_srfData.Clear();
	}
	
	if ( m_outLogFile.good() ) {
		m_outLogFile << "Read\n";
		m_crvNetwork.Write( m_outLogFile );
		//m_srfData.Write( m_outLogFile );
	}
}

void ActionEvents::ReadMaya( ifstream &in )
{
    m_crvNetwork.ReadMaya(in);
    m_srfData.Clear();
}

void ActionEvents::ReadSurface( ifstream &in )
{
    m_srfData.ReadSurface( in );
}

void ActionEvents::ReadSimple( const string &in_strFile )
{
    m_crvNetwork.Clear();
    ifstream in( in_strFile.c_str(), ios::in );
    
    R3Pt pt(0,0,0);
    Curve crv;
    ScreenCurve scrv;

    //m_crvNetwork.ReadSimple(in);
    
    /*
    while (!in.eof()) {
//        in >> pt[0] >> pt[1];
//        scrv.AddPoint( R2Pt(pt[0], pt[1]) );
        in >> pt[0] >> pt[1] >> pt[2];

        crv.AddPoint(pt);
    }
//    m_crvNetwork.ProcessCurve( scrv );
    crv.SetAllData();
    m_crvNetwork.NewCompositeCurve( scrv, crv );
    return;
    */
    char str[1024];
    while (in.peek() == '#') {
        in.getline(str, 1024);
    }
    int iNCrvs;
    in >> iNCrvs;
    int iNPts;
    char c;
    for ( int i = 0; i < iNCrvs; i++ ) {
        in >> iNPts;
        scrv.Clear();
        crv.Clear();
        for ( int j = 0; j < iNPts; j++ ) {
            in >> pt[0];
            in >> c;
            in >> pt[1];
            crv.AddPoint(pt);
            scrv.AddPoint( R2Pt(pt[0], pt[1]) );
        }
        crv.SetAllData();
        m_crvNetwork.NewCompositeCurve( scrv, crv );
    }
    return;
    /*
    const size_t iEnd = in_strFile.find( ".txt" );
    const size_t iEndCrv = in_strFile.find( ".crv" );
    string strSimple = in_strFile, strTangNorm = in_strFile;
    
    if ( iEnd == string::npos && iEndCrv == string::npos ) {
        strSimple = strSimple + ".txt";
        strTangNorm = strTangNorm + "OutTang.txt";
    } else if ( iEnd != string::npos ) {
        strSimple.erase( iEnd );
        strTangNorm.erase( iEnd );
    }
    ifstream in( in_strFile.c_str(), ios::in );
    if ( in.good() ) {
        m_crvNetwork.ReadSimple(in);
    } else {
        cerr << "ERR: ActionEvents::ReadSimple, file not found " << strSimple << "\n";
        return;
    }
    in.close();
    in.clear();
    
    in.open( strTangNorm.c_str(), ios::in );
    if ( in.good() && 0 !=strTangNorm.compare( strSimple ) ) {
        m_crvNetwork.ReadTangentNormal(in);
    }
    
    in.close();    
     */
    m_srfData.Clear();
}
void ActionEvents::ReadPatchMaker( const string &in_strFile )
{
    ifstream in( in_strFile.c_str(), ios::in );
    if ( in.good() ) {
        m_crvNetwork.ReadPatchMaker(in, m_aaiPatches, m_aabReversePatch );
    } else {
        cerr << "ERR: ActionEvents::ReadPatchMaker, file not found " << in_strFile << "\n";
        return;
    }
    in.close();
    in.clear();
    m_srfData.Clear();
}


void ActionEvents::AddNormalsFromCurves( const string &in_strCrvFile )
{
    CurveNetwork nw;
    ifstream in( in_strCrvFile.c_str(), ios::in );
    nw.Read(in);
    in.close();
    m_crvNetwork.AddNormalsFromCurves( nw );
}

/*
#include <fitting/FITCrv_Fit_curveTC.H>
#include <utils/Crv_Nurb_sampledTC.H>
*/
void ActionEvents::WriteNurbs( const char *in_strFile ) 
{
    /*
    CurveNetwork &crvs = m_crvNetwork;
    CurveNetwork crvCheck;

    ofstream outNurb( ( in_strFile + string("Nurbs.crv") ).c_str(), ios::out );
    ofstream outTang( ( in_strFile + string("Tang.txt") ).c_str(), ios::out );
    outTang << "## Format: t-value tangent, normal\n";
    outNurb << "NCurves " << crvs.NumCrvs() << "\n";
    
    CRVFit3TC fitCrv;
    
    const double dLenNormSample = 5.0 * g_drawState.CurveSpacingSize();
    Array<R3Pt> apt;
    for ( int i = 0; i < crvs.NumCrvs(); i++ ) {
        const CurveRibbon &crv = crvs.GetCurveRibbon(i);
        
        const int iNSamples = WINmax(3, (int) ( crv.Length() / dLenNormSample ) );
        
        outTang << "Curve " << i << " samples " << iNSamples << "\n";
        const double dDeltaT = 1.0 / (iNSamples - 1.0);
        for ( int j = 0; j < iNSamples; j++ ) {
            const double dT = j * dDeltaT;
            cout << dT << "   ";
            outTang << dT << "   " << crv.Tangent( dT ) << "  " << crv.Normal( dT ) << "\n";
        }
        cout << "\n";
        
        fitCrv.ClearPoints();
        for ( int iP = 0; iP < crv.NumPts(); iP++ ) {
            fitCrv.AddPoint( crv.Pt(iP), crv.PercAlong(iP) );
        }
        fitCrv.EvenTs(0.0, 1.0);
        fitCrv.FitSlow( 0.1 * crv.AverageSegLength() );
        
        // Should properly refine here, but...
        fitCrv.Drag( crv.Pt(0), 0.0 );
        fitCrv.Drag( crv.Pt( crv.NumPts() - 1), 1.0 );
        
        const CurveGroup &crvGrp = crvs.GetCurveGroup(i);
        for ( int iP = 0; iP < crvGrp.NumConstraints(); iP++ ) {
            const PointConstraint &pin = crvGrp.GetConstraint(iP);
            if ( !pin.IsPoint() ) {
                continue;
            }
            
            fitCrv.Drag( pin.GetPt(), pin.GetTValue( crvGrp ) );
            cout << pin.GetPt() << "\n";
            cout << fitCrv.Crv()( pin.GetTValue( crvGrp ) ) << "\n";
        }
        fitCrv.Drag( crv.Pt(0), 0.0 );
        fitCrv.Drag( crv.Pt( crv.NumPts() - 1), 1.0 );
        cout << crv.Pt(0) << crv.Pt( crv.NumPts() - 1 ) << "\n";
        cout << fitCrv.Crv()(0.0) << " " << fitCrv.Crv()(1.0) << "\n\n";
        
        const CRVNub3TC &crvFit = fitCrv.Crv();
        CRVNub_sampled3TC crvSample( crvFit, 0.1 * crv.AverageSegLength(), crv.AverageSegLength() );
        crvSample.Samples( apt );
        
        Curve crvOut;
        for ( int iP = 0; iP < apt.num(); iP++ ) {
            crvOut.AddPoint( apt[iP] );
        }
        crvOut.SetAllData();
        crvCheck.NewCompositeCurve( crvOut );
        crvFit.Write( outNurb );
    }
    outNurb.close();
    outTang.close();
    
    ofstream out( ( in_strFile + string("Check.crv") ).c_str(), ios::out );
    crvCheck.Write(out);
    out.close();
    
    ofstream outIntersection( ( in_strFile + string("Intersections.txt") ).c_str(), ios::out );
    
    m_srfData.Write( outIntersection );
    outIntersection.close();
     */
}


void ActionEvents::WriteOBJSurface( const char *in_str ) const
{
    string str = in_str;
    string strNoEnd = in_str;
    if ( strNoEnd.find_last_of('.') != string::npos ) {
        strNoEnd.erase( str.find_last_of('.'), string::npos );
    }
    ofstream outOff( ( strNoEnd + ".off" ).c_str(), ios::out );
    ofstream outObj( ( strNoEnd + ".obj" ).c_str(), ios::out );
    ofstream outMtl( ( strNoEnd + ".mtl" ).c_str(), ios::out );
    if ( !outOff.good() ) {
        cerr << "ERR: ActionEvents::WriteOBJSurface, couldn't write file " << in_str << "\n";
        outOff.close();
        outObj.close();
        outMtl.close();
        return;
    }
    // Collect points/faces
    string strFileName = strNoEnd;
    if ( strFileName.find_last_of('/') != string::npos ) {
        strFileName.erase( 0, strFileName.find_last_of('/') + 1 );
    }
    
    outObj << "mtllib " << strFileName << ".mtl\n\n";
    
    Array<R3Pt> apt;
    Array<R3Pt_i> aface;
    m_srfData.WriteOBJSurface( apt, aface, outObj, outMtl, 0 );
    for ( int i = 0; i < m_aopPatchSurfaces.num(); i++ ) {
        m_aopPatchSurfaces[i]->WriteOBJSurface( apt, aface, outObj, outMtl, i+1 );
    }

    outOff << "OFF\n";
    outOff << apt.num() << " " << aface.num() << " " << 3 * aface.num() / 2 << "\n";
    for ( int i = 0; i < apt.num(); i++ ) {
        outOff << apt[i] << "\n";
    }
    for ( int i = 0; i < aface.num(); i++ ) {
        outOff << "3 " << aface[i] << "\n";
    }
    
    
    outObj.close();
    outOff.close();
    outMtl.close();
}

