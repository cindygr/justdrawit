/*
 *  DrawState.h
 *  SketchCurves
 *
 *  Created by Cindy Grimm on 9/13/10.
 *  Copyright 2010 Washington University in St. Louis. All rights reserved.
 *
 */

#ifndef _DRAW_STATE_DEFS_H_
#define _DRAW_STATE_DEFS_H_

#include <utils/Rn_Defs.H>
#include <utils/R3_Plane.H>
#include <utils/Utils_Color.H>
#include <OpenGL/OGLShader.h>
#include <OpenGL/OGLObjs_Camera.H>
#include "ShadowBox.h"
#include "Instructions.h"

class UserInterface;
class ShadowBox;
class OGLShader2;
class Fl_Shared_Image;

/* Keep drawing state and other global information
 *
 * Sort of a catch-all for a bunch of stuff
 *
 * Camera: Handle moving the camera around the scene. 
 * Shaders: Handle reading in textures, shaders, invoking shaders
 * Textures: Handle reading in textures
 * Fltk gui: Has a pointer to that gui
 *
 * Most everything is based off of number of pixels and pixel size - ie, things should stay the same size even if the
 * window size changes. But almost all of the code is in camera coordiantes ([-1,1]X[-1,1]) so we do the conversion
 * here and store it.
 *
 * m_dpixelWidth, m_vec*, m_dScreenSize, m_dPixelWidth2D are all set in UpdateCamera
 */
class DrawState {
private:
	OGLShader2    *m_opShader;  // Will create an instance of the shader after gl initialized
	OGLObjsCamera  m_camera;    // Camera
    ShadowBox     *m_opShadowBox; // Shadow box from action events
    std::vector<GLuint> m_agEnumTexture; // What textures to read in
	bool           m_bInShader;          // Are we in a shader? Used to see if we accidentally invoke a shader twice
    double         m_dPixelWidth2D;      // Based on number of pixels in screen. Not aspect ratio corrected
	double         m_dPixelWidth;        // Width of a pixel in camera coordinates at film plane. Not aspect ratio corrected (always uses x)
    double         m_dScreenSize;        // Width of selection area on film plane
	// These were originally stored in MeshViewer; store here now. Basically how much the
	// gui camera dials have been rotated. Stored here now so they can be zero'd out on a non-dial camera move
	double m_dLeftRightLast, m_dUpDownLast;
    
	/* Texture stuff, in DrawState_Texture.cpp */
	// Read in a single texture
    void ReadTexture( const string &in_fname, const int in_iDepth, const int in_iEnum );
	// Set up all the menu and stroke textures
    void SetupTextures();
    void SetupImages();
	
	// Store current state (zoom, at, from) and new state
	void SetCameraAnimationData( const OGLObjsCamera &in_camNew ) const;
	
	/// Zero out m_dSpinLast, m_dTiltLast, m_dPanLast on camera change
	void ZeroCameraControls( );

public:
	/* These are passed to StartShader. They may share fragment or vertex shaders. If you 
	 * add a new one, make sure to update StartShader with another case */
    typedef enum {
        BOX_SHADING,         // shadow box, gray scale no lighting
        LINE_SHADING,        // RGB warm-cool shading, no texture
		SURFACE_SHADING,     // Standard phong lighting, no texture
        STROKE_SHADING,      // Apply stroke texture to strokes
        FRAGMENT_SHADING,    // Apply fragment stroke texture to strokes
        LINE_STROKE_SHADING, // Apply line stroke texture to composite curve
        STRIPE_V_SHADING,    // v texturing with warm-cool shading
        STRIPE_BAND_SHADING, // banded texturing with warm-cool shading
        SHADOW_SHADING,      // Alpha-blended shadows
        SCREENSPACE_SHADING, // Shading for 2D tick marks
		SCREENSPACE_STROKE,  // For the 2D stroke as it's being drawn
		MENU_3D,             // 3D menus in the scene
    } ShaderType;
	
	/* Enumerated ids for each of the textures. If you add one of these, make sure
	 * to update s_astrNames in DrawState_Textures.cpp with the additional filename */
    typedef enum {
        STROKE = 0,
        MENU_STROKE,
        MENU_CAMERA,
        MENU_CURVE,
        ICON_DRAG,
        LINE_STROKE,
        FRAGMENT_STROKE,
        NUM_TEXTURES
    } TextureNames;
	
	/* Enumerated ids for each of the images. If you add one of these, make sure
	 * to update s_astrImageNames in DrawState_Textures.cpp with the additional filename */
    typedef enum {        
        INSTRUCTIONSOVERVIEW = 0,
        INSTRUCTIONS2D,
        INSTRUCTIONS2DCAMERA,
        INSTRUCTIONS2DMENU,
        INSTRUCTIONS2DTIPS,
        INSTRUCTIONS3D,
        INSTRUCTIONS3DDRAW,
        INSTRUCTIONS3DDRAWPLANE,
        INSTRUCTIONS3DDRAWINFLATION,
        INSTRUCTIONS3DDRAWEXTRUSION,
        INSTRUCTIONS3DCURVEMENU,
        INSTRUCTIONS3DTIPS,
        INSTRUCTIONSCAMERA,
        INSTRUCTIONSCAMERAMENU,
        INSTRUCTIONSSURFACE,
        INSTRUCTIONSSURFACEPOINTS,
        INSTRUCTIONSSURFACENORMALS,
        INSTRUCTIONSSURFACETIPS,
        NUM_IMAGES
    } ImageNames;
	
	/* Enumerated ids for the actual vertex shaders. If you add one, make sure to
	 * add a string for the filename to s_strVertexShaderNames in DrawState_Shader.cpp */
	typedef enum {
		POSITION_NORMAL = 0,
		POSITION_NORMAL_TEXTURE,
		POSITION_TEXTURE,
		POSITION_TEXTURE_2D,
		NUM_VERTEX
	} VertexShaderTypes;
	
	/* Enumerated ids for the actual fragment shaders. If you add one, make sure to
	 * add a string for the filename to s_strFragmentShaderNames in DrawState_Shader.cpp */
	typedef enum {
		COLOR_PASSTHROUGH = 0,
		WARM_COOL,
		GREY_SCALE,
		STRIPE_V,
		STRIPE_BAND,
		STROKE_TEXTURE,
		SHADOW,
		TEXTURE_ONLY,
		NUM_FRAGMENT
	} FragmentShaderTypes;

    typedef enum {
        SCREEN_STROKES = 0,
        TUBES,
        RIBBONS,
        WEBBING,
        POINTS_OR_LINES
    } DrawingStyle;
	
    
    UserInterface *m_opUI; // This one is gobally available to all calling methods. Set by meshviewer
	// Extracted from the camera in UpdateCamera
    R3Vec m_vecView, m_vecUp, m_vecRight;
	
    DrawingStyle m_drawingStyle;
    Instructions m_instructions;
    
	/**@name Initialization */
	//@{
	// Set shadow box from action events
	void SetShadowBox( ShadowBox *in_opShadowBox );
	/// Re-read param file - pass on to shadow box
	void UpdateParams();
	//@}
	
	/**@name Access */
	//@{
	/// Accessors for camera and shadow box
	const OGLObjsCamera &GetCamera() const { return m_camera; }
	///
	const ShadowBox &GetShadowBox() const { return *m_opShadowBox; }
	//@}
	
	/// Is the drawing plane close enough aligned to the view direction to use for drawing?
	bool ProjectToPlane() const;

	/// How big a pixel is in camera space (width / 2) * size of film plane in 3D
	double PixelWidth() const { return m_dPixelWidth; }
    
    // How big a pixel is in camera coordinates (width / 2)
    double PixelWidth2D() const { return m_dPixelWidth2D; }

	
	/**@name Draw controls from Gui */
	//@{
	/// Width of strip/ribbon/tube
	double StripWidth() const;
	/// Amount of bend in ribbon
	double StripHeight() const;
	/// For line drawing
	double LineWidth() const;
	/// For width of 2D curve
	double ScreenStripeWidth() const;
	//@}

	/**@name 3D selection size */
	//@{
	/// Size of selection circle in absolute terms
	double SelectionSize() const;
	/// Screen-space size of selection circle
	double ScreenSelectionSize() const;
	/// Absolute amount to space curve points by
	double CurveSpacingSize() const;
	/// Size of menu on screen/in camera coordinates
	double MenuSize2D() const;
	/// Size of menu in absolute terms; for drawing 3D axes
	double MenuSize3D() const;
	//@}

	/// Return either the shadow box drawing plane or the view plane if the draw plane is not visible
	R3Plane DrawingPlane() const;	
	
	
	/**@name Camera controls. Basically, calculate a new camera, then set up the camera animation, move the camera, update the shadow box */
	//@{
	/// Are we in the middle of a camera animation?
	bool IsCameraAnimation() const;
	/// Go to the next camera. Return true if done.
	bool NextCamera();
	/// From controls in upper left
    void LeftRight( const double in_d );
	/// From controls in upper left
    void UpDown( const double in_d );
	/// Center camera on the bounding box. View direction stays the same. Move the drawing plane as well, y/n
	void CenterCamera( const std::pair<R3Pt,R3Pt> &in_ptLLUR, const bool in_bCenterDrawingPlane );
	/// Rotate camera to look down the draw plane normal. Center on bounding box
	void CenterCameraOnDrawingPlane(  const std::pair<R3Pt,R3Pt> &in_ptLLUR  );
	/// Call when camera is changed to update size data and shadow box and m_dPixelWidth and m_dPixelSize and m_vec*
	void UpdateCamera();
	/// look, up
	void AlignCamera( const R3Matrix &in_mat );
	/// Rotate to set up and look
	void AlignCamera( const R3Vec &in_vecLook, const R3Vec &in_vecUp );
	/// look, use same up if possible
	void AlignCamera( const R3Vec &in_vec );
	/// Pin paper in the upper left. Rotation is around pin point, zoom is in and out from pin point
	void RotateScale( const R2Pt &in_ptLast, const R2Pt &in_ptNext );
	///
	void RotateLeftRight( const R2Pt &in_ptLast, const R2Pt &in_ptNext );
	///
	void RotateUpDown( const R2Pt &in_ptLast, const R2Pt &in_ptNext );
	///
	void RotateSpin( const R2Pt &in_ptLast, const R2Pt &in_ptNext );
	///
	void Trackball( const R2Pt &in_ptLast, const R2Pt &in_ptNext );
	///
	void Pan( const R2Pt &in_ptLast, const R2Pt &in_ptNext );
	/// Save the current camera
	void StartDollyZoom();
	/// 
	void DollyZoom( const R2Pt &in_ptLast, const R2Pt &in_ptNext );
	///
	void Zoom( const R2Pt &in_ptDown, const R2Pt &in_ptNext );
	///
	void SetZoom( const double in_dZoom );
	///
	void ResizeCamera( const int in_iW, const int in_iH );
	///x,y,z,X,Y,Z change to those axes
    void HandleKeystroke( const unsigned char in_c, const bool in_bShift, const bool in_bCtrl );
	/// Used to read the camera from the log file
    void ReadCamera( ifstream &in );
	//@}
    
	/**@name Shader stuff (in DrawState_Shader.cpp) */
	//@{
    ///	
    string GetBaseDirectory( const string &in_strSubDir ) const;
	/// Call this to set up the given shader. Don't forget to match with an EndShader call
	void StartShader( const ShaderType );
	/// Call this to set up the menu shader with the given menu texture. Don't forget to match with an EndShader call
	void StartMenuShader( const TextureNames, const bool in_bIs3D = false );
	/// End shader
	void EndShader();
	/// Read the shaders in. Initializes m_opShader, so has to be called at least once
    void ReadShaders();
	//@}
    
    /**@names images */
    //@{
    ///
    Fl_Shared_Image *GetImage( const ImageNames &in_im );
    //@}
		
    DrawState();
    ~DrawState() {}
};
    

extern DrawState g_drawState;

#endif
