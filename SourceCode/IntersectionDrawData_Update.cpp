/*
 *  IntersectionDrawData_Update.cpp
 *  SketchCurves
 *
 *  Created by Cindy Grimm on 7/2/12.
 *  Copyright 2012 Washington University in St. Louis. All rights reserved.
 *
 */


#include "IntersectionDrawData.h"
#include "DrawState.h"
#include "PointConstraint.h"
#include "CurveGroup.h"

static bool s_bTrace = false;

void IntersectionDrawData::UpdateColors( )
{
    const R4Pt_f fColPin( s_colPointConstraint[0], s_colPointConstraint[1], s_colPointConstraint[2], 1.0 );
    R4Pt_f crvCol;

    double dFromTop = 0.0;
    for ( int i = 0; i < m_aafColSpoke.size(); i++ ) {

        const int iIncr = m_aSpokeData[i].m_bReverseDir ? -1 : 1;
        const CurveDrawData &cdd = m_pin.GetCurveGroup( m_aSpokeData[i].m_iCurve ).GetCurveDrawData();
        
        if ( m_aSpokeData[i].NumPts() == 0 ) {
            continue;
        }
        
        const int iNotWeb = m_aSpokeData[i].NumPts() - m_aSpokeData[i].NumWeb();
        const int iWeb = m_aafColSpoke[i].size() - iNotWeb;
        for ( int j = 0; j < m_aafColSpoke[i].size(); j++ ) {
            if ( j < iNotWeb ) {
                dFromTop = 0.0;
                crvCol = cdd.GetColor( m_aSpokeData[i].m_iStart + iIncr * j );
                if ( s_bTrace ) cout << m_aSpokeData[i].m_iStart + iIncr * j << ", ";
            } else {
                dFromTop = (double) ( j - iNotWeb ) / WINmax(1.0, (double) iWeb );
                
                const double dIndex = dFromTop * m_aSpokeData[i].m_iPin + (1.0 - dFromTop) * m_aSpokeData[i].m_iMid;
                const int iIndex = floor( dIndex );
                const double dPerc = dIndex - iIndex;
                
                const R4Pt_f &crvColPrev = cdd.GetColor( iIndex );
                const R4Pt_f &crvColNext = cdd.GetColor( iIndex+1 );
                for ( int iC = 0; iC < 4; iC++ ) {
                    crvCol[iC] = (1.0 - dPerc) * crvColPrev[iC] + dPerc * crvColNext[iC];
                }
                if ( s_bTrace ) cout << iIndex << " " << dPerc << ", ";
            }
            if ( s_bTrace ) cout << dFromTop << " ";
            if ( dFromTop < 0.0 || dFromTop > 1.0 ) {
                cerr << "ERR: IntersectionDrawData::UpdateColor out of range " << dFromTop << "\n";
            }
            
            for ( int iC = 0; iC < 4; iC++ ) {
                m_aafColSpoke[i][j][iC] = dFromTop * fColPin[iC] + (1.0 - dFromTop) * crvCol[iC];
            }
        }
        if ( s_bTrace ) cout << "\n";
    }
}

void IntersectionDrawData::WorldStateUpdate( const bool in_bWidthSame, const bool in_bHeightSame, const bool in_bViewSame )
{
    if ( !in_bWidthSame ) {
        m_bUpdateWebbing = true;
        m_bUpdateShadow = true;
    }
    
    if ( !in_bHeightSame ) {
        m_bUpdateWebbing = true;
    }
    
    if ( !in_bViewSame ) {
        m_bUpdateShadow = true;
    }    
}

void IntersectionDrawData::SetShadow( const bool in_bShadow )
{
    if ( m_bCastShadow != in_bShadow ) {
        m_bUpdateShadow = true;
    }
    
    m_bCastShadow = in_bShadow;
}

/// Sets geometry
void IntersectionDrawData::GeometryChanged()
{
    m_bUpdateColor = true;    
    m_bUpdateSpokeData = true;
    m_bUpdateWebbing = true;
    m_bUpdateShadow = true;
}    

/// Call just before drawing to make sure everything is up to date
void IntersectionDrawData::Update( )
{
    WorldStateChanged();
    
    if ( m_bUpdateSpokeData && g_drawState.m_drawingStyle == DrawState::WEBBING ) {
        SetSpokeData( );
        m_bUpdateSpokeData = false;
    }
    
    if ( m_bUpdateWebbing && g_drawState.m_drawingStyle == DrawState::WEBBING ) {
        UpdateGeometry( );
        m_bUpdateColor = true;
        m_bUpdateWebbing = false;
    }
    
    if ( m_bUpdateColor ) {
        UpdateColors( );
        m_bUpdateColor = false;
    }
    
    if ( m_bUpdateShadow ) {
        UpdateShadow();
        m_bUpdateShadow = false;
    }
}