/*
 *  CurveGroup.h
 *  SketchCurves
 *
 *  Created by Cindy Grimm on 9/2/10.
 *  Copyright 2010 Washington University in St. Louis. All rights reserved.
 *
 * Changes from the original version:
 *  1) Use to keep just an array of curves and a composite curve. Switched to keeping a
 *     history list plus a list of curves, then moved the actual final curve into StrokeHistory.
 *  2) The blend/overstroke stuff use to happen in this curve. Now the determination of what to blend
 *     to has moved to CurveOverlap and the actual blending to StrokeHistory
 *  3) All of the AddZValue functions are new. They're here (rather than somewhere else) because
 *     the existing curve (which is needed for extrapolation/interpolation) is stored here.
 *  4) Rolling back a stroke is new. 
 *  5) Point/normal constraints are new
 *
 */

#ifndef _CURVE_GROUP_DEFS_H_
#define _CURVE_GROUP_DEFS_H_

#include "Curve.h"
#include "ScreenCurve.h"
#include "Shadowbox.h"
#include "DrawState.h"
#include "StrokeHistory.h"
#include "CurveRibbon.h"

class CurveOverlap;

/*
 * A curve and the history or sequence of actions that made it
 * The history (and most of the merge/join work) is done in StrokeHistory
 * The overlap determination is done in CurveOverlap
 * Actually editing the curve/calculations on the curve are in Curve
 * Most of the edits are simply passed on to StrokeHistory or the composite curve in StrokeHistory
 * The actual curve (ie, the final one that's being drawn) is in the last StrokeHistory instance,
 * in m_crvComposite (GetComposite)
 *
 * Note that you can't *set* the curve directly from here - you can only call functions that
 * in term call functions on StrokeHistory which in turn call functions on the actual curves. That
 * is because each action creates a new StrokeHistory instance, which takes the composite curve
 * from the last instance, stores any secondary data, and stores the new, edited curve in itself
 *
 * TODO: Handle drawing the construction strokes correctly. This would involve going back and editing/clipping
 * the saved stroke curves in the StrokeHistory stack.
 */
class CurveGroup {
private:
    // Unique id for hash table
    const int m_iHashId;
    
	// The composite curve is kept in the stroke history
	Array<StrokeHistory> m_aStrokeHistory;
    
    // The latest incarnation of the curve. Has curve normals. 
    CurveRibbon   m_crvRibbon, m_crvRibbonReflect;    
    CurveDrawData m_crvDraw, m_crvDrawReflect; // The draw data associated with the composite curve
    R3Plane       m_planeReflect;
    bool          m_bReflected;
    
    // Point and normal constraints. Sorted by t values on the curve
    Array<PointConstraint *> m_aopPins;
	
	// A left-over variable that was suppose to say whether or not to fade out strokes
	// Currently used to determine whether or not to draw the strokes in the StrokeHistory stack
    bool         m_bDoFade;

    /* Semaphore to prevent recursive constraint satisfaction. 
     * Should be ok, since the drags don't move existing point constraints
     */
    bool m_bInGeometryUpdate;
    
	/* Helper functions for AddZFunctions. In CurveGroup_ZValue.cpp */
	/// If the input curve is planar, see if you want to snap the drawing plane to the curve's plane
	bool BestPlaneForPlanarCurve( const Curve &in_crvComposite, R3Plane &out_plane ) const;

	/* Actually build the curve from the t values on the lines (after some filtering) */
	Curve AddZValue( const Array<R3Line> &in_aline, const Array<double> &in_adDist ) const;

	/// Simple case - project the curve onto the plane
	Curve AddZValue( const ScreenCurve &in_crvScreen, const R3Plane &in_plane ) const;
	/// Project the curve onto either the view plane or the draw plane
	Curve AddZValue( const ScreenCurve &in_crvScreen ) const;
	/// For merge/join. Extend the depth values by either linear extrapolation or to the draw plane
	Curve AddZValueExtend( const CurveOverlap &in_crvOverlap, const Curve &in_crvComposite, const ScreenCurve &in_crvScreen ) const;
	/// Oversketch for closed curves
	Curve AddZValueClosed( const CurveOverlap &in_crvOverlap, const Curve &in_crvComposite, const ScreenCurve &in_crvScreen ) const;
	/// Overskecth
	Curve AddZValueOversketch( const CurveOverlap &in_crvOverlap, const Curve &in_crvComposite, const ScreenCurve &in_crvScreen ) const;
	/// Interpolate z values
	Curve AddZValueCombine( const CurveOverlap &in_crvOverlapMe, const Curve &in_crvCompositeMe, const CurveOverlap &in_crvOverlapOther, const Curve &in_crv, const ScreenCurve &in_crvScreen ) const;
	
    /// Called when this curve changes to update other curves
    int SplitSegment( const double in_dT, const double in_dClose ); // adjust pins if neede
    void RemoveDanglingConstraints( const StrokeHistory::MergeType in_action );
    void FixTValuesOfConstraints( const StrokeHistory::MergeType in_action );
    void MovePointConstraints( const StrokeHistory::MergeType in_action );
    void PropagateEdit( const StrokeHistory::MergeType in_action );
    ///
    void SortPointConstraints();
    
	/// Get the first action of the last sequence of edits to this curve
    StrokeHistory &SetLastAction() ;
    
    ///
    void DragPointConstraint( const double in_dT, PointConstraint &io_pin, const bool in_bAddToStrokeHistory );
    
    ///
    void UpdateReflection();
    void GeometryChanged( const StrokeHistory::MergeType in_action );
    
    CurveGroup &operator=( const CurveGroup &in_crvGroup );
    CurveGroup( const CurveGroup & );
    
public:
	/**@name Accessors */
	//@{
    ///
    int HashId() const { return m_iHashId; }
    
    ///
    bool IsReflected() const { return m_bReflected; }
    ///
    bool IsHidden() const { return m_crvDraw.IsHidden(); }
	/// Actual current curve 
    const Curve &CompositeCurve() const { return m_crvRibbon; }
	/// Actual current curve 
    const CurveRibbon &GetCurveRibbon() const { return m_crvRibbon; }
	/// Actual current curve 
    const CurveDrawData &GetCurveDrawData() const { return m_crvDraw; }
	/// What was the last action in editing this curve?
	StrokeHistory::MergeType LastStrokeAction() const;
	/// Get the first action of the last sequence of edits to this curve
	const StrokeHistory &LastAction() const;
	/// How many strokes made up this curve
	int NumStrokes() const { return m_aStrokeHistory.num(); }
	/// The last 2D screen stroke (if any)
    const ScreenCurve &LastStroke2D() const ;
	/// The last screen stroke, promoted to 3D
    const Curve &LastStroke3D() const ;
	/// Where did the last stroke start?
	R3Pt StrokeStartPt() const;
	/// Where did it end?
	R3Pt StrokeEndPt() const;
	/// Where did the last selected region start? (For smoothing circle placement)
	R3Pt SelectRegionStartPt() const;
	/// And where did the last selected region end? (For smoothing circle placement)
	R3Pt SelectRegionEndPt() const;
	/// Walk back in the stroke history list, collecting blend strokes. In CurveGroup_AddStroke.cpp
	std::vector<ScreenCurve> GetBlendStack() const;
    ///
    const R3Plane &ReflectionPlane() const { return m_planeReflect; }
    ///
    int NumConstraints() const { return m_aopPins.num(); }
    ///
    int NumPointConstraints() const;
    ///
    const PointConstraint &GetConstraint( const int in_i ) const;
	//@}
	
	/**@name Editing the curve by adding strokes (CurveGroup_AddStroke.cpp) */
	//@{
	/// Last stroke was a blend (scratch back and forth). Add another StrokeHistory to the stack to reflect this
	void AddBlendToHistory( const ScreenCurve &in_crv );
	/// Same as above, but for when the last stroke exactly overlaps the current one
	void AddBlendOverstroke( const ScreenCurve &in_crv );
	/// Invoked from the stroke menu. The user explicitly says what action to do
	bool AddStrokeForced( const StrokeHistory::MergeType &in_action, const ScreenCurve &in_crv2D, const bool in_bAllowClosed = true );
	/// Usual add stroke
	bool AddStroke( const CurveOverlap &in_crvOverlap, const ScreenCurve &in_crv2D );
	/// Merge the last overstrokes together before overstroking
	bool AddMultistrokeOverstroke( const CurveOverlap &in_crvOverlap, const ScreenCurve &in_crv2D );
	/// Combines two curves; Stroke history stack and constraints are copied from the other one
	void Combine( const CurveOverlap &in_crvOverlapMe,  CurveGroup &in_crvGrp, const CurveOverlap &in_crvOverlapOther, const ScreenCurve &in_crv2D ); 
	/// Similar to Undo, except actually delete from stroke history
	StrokeHistory RollBackLastStroke( );
	//@}

	/**@name Editing or creating curves in other ways (CurveGroup_Edit.cpp) */
	//@{
    /// Create a new curve, starting with this one
    void NewCurve( const ScreenCurve &in_curveTemp );
	/// Create a new curve, snapping it closed if need be. Projected onto the current drawing surface
    void NewClosedCurve( const CurveOverlap &in_crvOverlap, const ScreenCurve &in_curveTemp );
	/// For initializing with other curves, eg, ILoveSketch
    void NewCompositeCurve( const ScreenCurve &in_curveTemp, const Curve &in_curve );
	/// For initializing with other curves, eg, ILoveSketch
    void NewCompositeCurve( const Curve &in_curve );
	/// Erase part of the curve, returning the shorter part (if any) and keeping the longer part here
	CurveGroup *Erase( const double in_dTStart, const double in_dTEnd );
	/// Call before starting drag to set up StrokeHistory correctly
	void StartTransform();
	/// Drag part of curve between in_dTBracket. 
    void Drag( const R3Plane &in_plane, const R3Vec &in_vec, const double in_dTDrag, const std::pair<double,double> &in_dTBracket );
	/// Transform entire curve. Can do as many transforms as wanted; stored in a single strokeHistory
    void Transform( const R4Matrix & in_mat, const bool in_bReset ); // Transform curve
	/// Smooth the curve between the t values
    void Smooth( const double in_dTStart, const double in_dTEnd, const int in_iNLoops );
	/// Repeat the last smooth
	void RepeatSmooth();
	/// Undo the last action. Returns the last action
	StrokeHistory UndoLastAction( );
    ///
    void FinishedUndo() { GeometryChanged( StrokeHistory::UNDO ); }
	/// Erase it all
    void Clear();
	//@}

	/**@name Setting draw state. */
	//@
	///
    void SetFade( const bool in_bFade ) { m_bDoFade = in_bFade; }
    /// Just set hidden state
    void SetHidden( const bool in_bHidden );
    /// Toggle hidden state
    void ToggleHide();
    /// Just set active state
    void SetActive( const bool in_bActive );
    /// Just set shadow state
    void SetShadow( const bool in_bShadow );
	/// Color may need to be changed (selected region is yellow)
	void SetSelectionRegion( const std::pair<double,double> &in_dTOnCrv );
	/// Sets colors
	void ClearSelectionRegion( );
    
	/// Call just before drawing to make sure everything is up to date
    void UpdateDrawData( );
    ///
    void ForceUpdateDrawData();
    //@}
    
	/**@name Point and normal constraints. In CurveGroup_Constraints.cpp. */
	//@
	/// Fit to the constraint, if need be, then add to pin
	void AddConstraint( PointConstraint *in_iopPin, const double in_dT, const bool in_bDoDrag = true );
    /// See if there's a normal constraint close to this t value
    PointConstraint *HasConstraint( const double in_dT );
    /// See if there's a normal constraint close to this t value
    const PointConstraint *HasConstraintNoEdit( const double in_dT ) const;
	/// Remove pin from list
	void RemoveConstraint( PointConstraint *in_iopPin );
    /// Fix just this point constraint. Sets t and index values after dragging curve
    void ReEstablishPointConstraint( PointConstraint &in_pin, const bool in_bDoGeomUpdate );
    /// Just normals have changed
    void ReEstablishNormalConstraints( );
    /// Delete all constraints on this curve 
    void RemoveCurveFromPointConstraints();
    ///
    PointConstraint *SetConstraint( const int in_i ) ;
    /// Check consistency of constraints
    void CheckConsistency() const;
	//@}
	
	/// Remove stroke history
    void Finalize();

	/// Create a reflection of this curve.
    void Reflect( const R3Plane &in_plane ) ;
    ///
    void Unreflect() { m_bReflected = false; }
    
	/// Draw strokes
    void DrawFragments( ) const;
	/// Craw last composite curve
    void DrawCurve(  ) const;
    /// Accumulate all of the tube geometry in a list of vertices and points   
    void TubeGeometry( std::vector<R3Pt> &io_aptVs, std::vector<R3Vec> &io_avecVNs, std::vector< R3Pt_i > &io_aptFs ) ;

        
    bool operator==( const CurveGroup &in_cg ) const { return in_cg.m_iHashId == m_iHashId; }
    bool operator!=( const CurveGroup &in_cg ) const { return in_cg.m_iHashId != m_iHashId; }
    
    CurveGroup( const int in_iId );
    ~CurveGroup();

    void Write( ofstream &out, const int in_iId ) const;
    void WriteSimple( ofstream &out ) const;
    void Read( ifstream &in );
    void ReadKaran( ifstream &in );
    void ReadSimple( ifstream &in );
    void ReadTangentNormal( ifstream &in );
    
    void PrintHistory( const bool in_bPrintPins ) const;
};

#endif
