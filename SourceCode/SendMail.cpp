//
//  SendMail.cpp
//  SketchCurves
//
//  Created by Cindy Grimm on 4/23/12.
//  Copyright (c) 2012 Washington University in St. Louis. All rights reserved.
//

#include "SendMail.H"
#include <curl/curl.h>


static CURL *curl = NULL;

void SendMail::UploadForm( const int in_iUId, const int in_iUploadCountId,
                           const string &in_strLogfile, const string &in_strCurvefile, const string &in_strSurvey ) 
{
    CURLcode res;
    
    struct curl_httppost *formpost=NULL;
    struct curl_httppost *lastptr=NULL;
    struct curl_slist *headerlist=NULL;
    static const char buf[] = "Expect:";
    
    /* Fill in the file upload field */ 
    curl_formadd(&formpost,
                 &lastptr,
                 CURLFORM_COPYNAME, "curve",
                 CURLFORM_FILE, in_strCurvefile.c_str(),
                 CURLFORM_END);
    
    curl_formadd(&formpost,
                 &lastptr,
                 CURLFORM_COPYNAME, "mouse",
                 CURLFORM_FILE, in_strLogfile.c_str(),
                 CURLFORM_END);
    
    /* Fill in the filename field */ 
    curl_formadd(&formpost,
                 &lastptr,
                 CURLFORM_COPYNAME, "survey",
                 CURLFORM_COPYCONTENTS, in_strSurvey.c_str(),
                 CURLFORM_END);
        
    curl_formadd(&formpost,
                 &lastptr,
                 CURLFORM_COPYNAME, "magic",
                 CURLFORM_COPYCONTENTS, "JustDrawItUISend",
                 CURLFORM_END);
    
    char strN1[1024];
    sprintf( strN1, "%d", in_iUId );
    curl_formadd(&formpost,
                 &lastptr,
                 CURLFORM_COPYNAME, "id",
                 CURLFORM_COPYCONTENTS, strN1,
                 CURLFORM_END);
    
    sprintf( strN1, "%d", in_iUploadCountId );
    curl_formadd(&formpost,
                 &lastptr,
                 CURLFORM_COPYNAME, "execid",
                 CURLFORM_COPYCONTENTS, strN1,
                 CURLFORM_END);
    
    
    /* Fill in the submit field too, even if this is rarely needed */ 
    curl_formadd(&formpost,
                 &lastptr,
                 CURLFORM_COPYNAME, "submit",
                 CURLFORM_COPYCONTENTS, "send",
                 CURLFORM_END);
    
    /* initalize custom header list (stating that Expect: 100-continue is not
     wanted */ 
    headerlist = curl_slist_append(headerlist, buf);
    if(curl) {
        /* what URL that receives this POST */ 
        curl_easy_setopt(curl, CURLOPT_URL, "http://www.cse.wustl.edu/~cmg/myform.cgi");
        //if ( (argc == 2) && (!strcmp(argv[1], "noexpectheader")) )
        /* only disable 100-continue header if explicitly requested */ 
            //curl_easy_setopt(curl, CURLOPT_HTTPHEADER, headerlist);
        curl_easy_setopt(curl, CURLOPT_HTTPPOST, formpost);
        res = curl_easy_perform(curl);
        
        /* then cleanup the formpost chain */ 
        curl_formfree(formpost);
        /* free slist */ 
        curl_slist_free_all (headerlist);
    }
}

SendMail::SendMail() 
{
    curl_global_init(CURL_GLOBAL_ALL);
    

    curl = curl_easy_init();
}

SendMail::~SendMail() 
{
    /* always cleanup */ 
    curl_easy_cleanup(curl);
    
}

SendMail g_forceInit;
