#ifdef WIN32
#include <FL/gl.h>
#include <GL/glu.h>
#else
# if defined(powerpc) || defined(__APPLE__)
#   include <OpenGL/gl.h>
#   include <OpenGL/glu.h>
# else
#   include <GL/gl.h>
#   include <GL/glu.h>
# endif
#endif

#include "UserInterface.h"
//char g_strDataHome[] = "/Users/cigrimm/SourceCode/Data/";
//char g_strDataHome[] = "/Users/cindygrimm/SketchCurves/Data/";
char g_strDataHome[] = "./Data/";


#define SETUNREMOVABLE(signs) ((signs) = ((signs) | 0x10))


#ifdef __APPLE__
#include "CoreFoundation/CoreFoundation.h"
#endif

#include "CurveGroupManager.h"
// ----------------------------------------------------------------------------

/*
 * Closing and exiting: This gets a bit complicated, because of the query
 *
 * Ways the program can be closed:
 *   User chooses quit from the menu
 *   They click the red close button on all windows
 *   They choose close from the finder bar
 *
 * In all cases, this close callback is called, either directly (from the quit
 * menu button) or indirectly.
 *
 * Case 1: The user has ok'd data to be sent, or no data is sent
 *  Send/don't send data, then exit here
 *
 * Case 2: We have to bring the ok to be sent dialog up. Don't exit here;
 *   instead, exit from the ok to send dialog
 *   In this case, the static variable s_bQuitting is set to true by the WriteProfile
 *     call, so the subsequent ActionEvents::SendData will quit after
 *     sending (or not sending) the data.
 *   Note: This is partly why the SendData function is called either on a cancel or on
 *         a Done/write, because it needs to know if we're quitting.
 */
void close_cb( Fl_Widget *, void*) 
{
    g_drawState.m_opUI->m_meshViewer->SetActionEvents().WriteProfile(true);
    
    g_drawState.m_opUI->m_meshViewer->SetActionEvents().QuerySend();
    
    exit(0);
}


bool ReadWriteFiles( ActionEvents &events, int argc, char * const argv[1] ) 
{
    if ( argc > 2 ) {
        cout << "Options for first two arguments are: \n";
        cout << "  s name   - read in simple file format, name is filename\n";
        cout << "  c name   - read in curve file format, name is file name\n";
        cout << "  p name   - read in patchmaker curve file format, name is file name\n";
        cout << "Options for second two arguments: \n";
        cout << "  S name   - write out in simple file format.\n";
        cout << "  C name   - write out in curve file format.\n";
        cout << "  T name   - write out curves as mesh file.\n";
        cout << "  M name   - generate surface and write out as stl file\n";
        cout << "Other options: \n";
        cout << "  N name   - Use curve network to add normals.\n";
        cout << "  P        - Generate patches.\n";
        cout << "  Q        - exit interface when done\n";
    }
    
    int iCurArg = 1;
    while ( iCurArg < argc ) {
        const char iCSwitch = argv[ iCurArg ][0];
        const string str = iCurArg+1 < argc ? string( argv[iCurArg+1] ) : string("");
        iCurArg += 2;
        switch ( iCSwitch ) {
            case 'c' : {
                cout << "Reading curves " << str << "\n";
                ifstream in( str.c_str(), ios::in );
                if ( in.good() ) {
                    events.Read( in );
                    in.close();
                } else {
                    cout << "ERR: File not found\n";
                }
                break;
            }
            case 's' : {
                cout << "Reading simple curves " << str << "\n";
                ifstream in( str.c_str(), ios::in );
                if ( in.good() ) {
                    events.ReadSimple( str.c_str() );
                } else {
                    cout << "ERR: File not found\n";
                }
                break;
            }
            case 'p' : {
                cout << "Reading patches " << str << "\n";
                ifstream in( str.c_str(), ios::in );
                if ( in.good() ) {
                    events.ReadPatchMaker( str );
                } else {
                    cout << "ERR: File not found\n";
                }
                break;
            }
            case 'C' : {
                cout << "Writing full curve file format " << str << "\n";
                ofstream out( str.c_str(), ios::out );
                if ( out.good() ) {
                    events.Write(out);
                    out.close();
                } else {
                    cout << "Unable to write file\n";
                }
                break;
            }
            case 'S' : {
                cout << "Writing simple " << str << "\n";
                ofstream out( str.c_str(), ios::out );
                if ( out.good() ) {
                    events.WriteSimple(out);
                    out.close();
                } else {
                    cout << "Unable to write file\n";
                }
                break;
            }
            case 'T' : {
                cout << "Writing meshes for curves " << str << "\n";
                events.WriteCurvesAsOffFile(str.c_str());
                break;
            }
            case 'M' : {
                cout << "Writing mesh for surface " << str << "\n";
                events.CalcSurface();
                events.WriteSTLSurface(str.c_str());
                break;
            }
            case 'N' : {
                cout << "Using curves " << str << " for normals\n";
                events.AddNormalsFromCurves(str);
                break;
            }
            case 'P' : {
                cout << "Making patches\n";
                events.MakePatchSurface();
                break;
            }
            case 'Q' : return true;
        }
    }
    return false;
}
            
int main (int argc, char * const argv[]) 
{

	// ----------------------------------------------------------------------------
	// This makes relative paths work in C++ in Xcode by changing directory to the Resources folder inside the .app bundle
#ifdef __APPLE__    
	CFBundleRef mainBundle = CFBundleGetMainBundle();
	CFURLRef resourcesURL = CFBundleCopyResourcesDirectoryURL(mainBundle);
	char path[PATH_MAX];
	if (!CFURLGetFileSystemRepresentation(resourcesURL, TRUE, (UInt8 *)path, PATH_MAX))
	{
		// error!
	}
	CFRelease(resourcesURL);
	
	chdir(path);
	std::cout << "Current Path: " << path << std::endl;
#endif
	g_paramFile.ReadParamFile();
	
    // For windows GetUserName()
    
	UserInterface *window =	new	UserInterface( );
	window->m_meshViewer->SetUI( window	);
    
    ActionEvents &events = window->m_meshViewer->SetActionEvents();
    if ( ReadWriteFiles( events, argc, argv ) ) {
        exit(0);
    }
    
    g_paramFile.LoadDebuggingWindow();
    
    /*
    window->m_meshViewer->ReadCurves( "/Users/cindygrimm/Sketching/JustDrawIt/Data/CatOutlineClosed.crv" );
    window->m_meshViewer->WriteSimple( "/Users/cindygrimm/Sketching/JustDrawIt/Data/CatOutlineClosed.txt" );
     */
    
    //window->m_meshViewer->StartPlayback( "/Users/cindygrimm/svn/Tower3.txt" );
    //window->m_meshViewer->StartPlayback( "/Users/cindygrimm/svn/SketchCurves/BadCases/Overstroke.txt" );
    //window->m_meshViewer->ReadCurves( "/Users/cindygrimm/svn/SketchCurves/Data/CatNeckWithNormals.crv" );
    //window->m_meshViewer->SetActionEvents().ReadSimple( "/Users/cmg/DropBox/SketchGrant/Data/Simple/DragonArm2f.crv" );
    
    
    window->m_meshViewer->SetActionEvents().SetupFilesAtStart();
    window->m_mainWindow->callback( close_cb ); 
    /*
    string str;
    char strN[1024];
    ActionEvents &events = window->m_meshViewer->SetActionEvents();
    for ( int i = 0; i < 9; i++ ) {
        sprintf(strN, "/Users/cindygrimm/Sketching/JustDrawIt/Data/Teacup%d.crv", i+1 );
        cout << "Reading " << strN << "\n";
        ifstream in( strN, ios::in );
        events.Read( in );
        in.close();
        
        sprintf(strN, "/Users/cindygrimm/Dropbox/SketchGrant/Data/Simple/Teacup%d.txt", i+1 );
        ofstream out( strN, ios::out );
        events.WriteSimple( out );
        out.close();
    }
     */
    
    
    /* Resize the window intelligently */
    const int iWTop = window->m_grpControlTop->w();
    const int iHTop = window->m_grpControlTop->h();
    const int iWLeft = window->m_grpControlSide->w();
    const int iHLeft = window->m_grpControlSide->h();
    if ( window->m_mainWindow->w() + window->m_mainWindow->x() > Fl::w() || window->m_mainWindow->h() + window->m_mainWindow->y() > Fl::h() ) {
        const int xChange = -WINmin( Fl::w() - window->m_mainWindow->x() - window->m_mainWindow->w(), 0);
        const int yChange = -WINmin( Fl::h() - window->m_mainWindow->y() - window->m_mainWindow->h(), 0);
        window->m_mainWindow->resize( window->m_mainWindow->x(), window->m_mainWindow->y(), window->m_mainWindow->w() - xChange, window->m_mainWindow->h() - yChange );
        window->m_grpControlTop->resize( window->m_grpControlTop->x(), window->m_grpControlTop->y(), iWTop, iHTop );
        window->m_grpControlSide->resize( window->m_grpControlSide->x(), window->m_grpControlSide->y(), iWLeft, iHLeft );
        const int iLeft = window->m_grpControlSide->x() + iWLeft + 4;
        const int iTop = window->m_grpControlTop->y() + iHTop + 4;
        window->m_meshViewer->resize( iLeft, iTop, window->m_mainWindow->w() - iLeft - 4, window->m_mainWindow->h() - iTop - 24 );
    }
    if ( window->m_windowConsent->w() + window->m_windowConsent->x() > Fl::w() || window->m_windowConsent->h() + window->m_windowConsent->y() > Fl::h() ) {
        window->m_windowConsent->resize( window->m_windowConsent->x(), window->m_windowConsent->y(), WINmin( window->m_windowConsent->w(), Fl::w() - window->m_windowConsent->x() ), WINmin( window->m_windowConsent->h(), Fl::h() - window->m_windowConsent->y() ) );
    }
	window->m_mainWindow->show();
    
    //window->m_meshViewer->ReadCurves( ( string(g_strDataHome) + "teapot.crv" ).c_str() );
    //window->m_meshViewer->ReadCurves( ( string(g_strDataHome) + "Box.crv" ).c_str() );
    //window->m_meshViewer->ReadCurves( ( string(g_strDataHome) + "Horse.crv" ).c_str() );
//    window->m_meshViewer->ReadCurves( ( g_strDataHome + string("roadster_curves_deg_1.txt") ).c_str() );
    
	return Fl::run();
}
