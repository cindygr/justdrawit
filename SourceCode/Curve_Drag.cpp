/*
 *  Curve_Drag.cpp
 *  SketchCurves
 *
 *  Created by Cindy Grimm on 5/19/11 (copied from Curve_Fit).
 *  Copyright 2011 Adobe. All rights reserved.
 *  Except Drag2DLaplacian, copyright 2010 Wash U
 *
 */

#include "Curve.h"
#include <utils/Rn_Matrix.H>
#include <fitting/FITTOOLS_Least_squares.H>
#include <utils/Crv_HermiteTC.H>

const bool s_bTrace = false;

static Array<R2Pt> &CalcMatrix( const Array<R3Pt> &in_apt, const R2Vec &in_vecDelta )
{
    // matC (6 x 4) is the coeff matrix for the system that solves for T in terms of v'
    //    v * T = v' for vertex and neighbors
    //
    //      s  a  t1
    // T = -a  s  t2
    //      0  0  1
    static CwMtx::RNMatrix matC(6,4), matCprime(4,6), matCinv(4,4), matRHS(4,6);
    static Array<R2Pt> matTDelta(6);
    
    const R2Pt ptX(1,0), ptY(0,1);
    for ( int i = 0; i < 3; i++ ) {
        const R2Pt ptTang( in_apt[i][1], -in_apt[i][0] );
        for ( int j = 0; j < 2; j++ ) {
            matC[i][j] = in_apt[i][j];
            matC[i][j+2] = ptX[j];
            matC[3+i][j] = ptTang[j];
            matC[3+i][2+j] = ptY[j];
        }
    }
	
    if ( RNIsZero( fabs( in_vecDelta[0] ) + fabs( in_vecDelta[1] ), 1e-5 ) ) {
        for ( int i = 0; i < 6; i++ ) {
            matTDelta[i][0] = 0.0;
            matTDelta[i][1] = 0.0;
        }
    } else {
		// Have C * T = delta
        // Solve (Ct C)^-1 T = Ct delta
        //   T = (Ct C)^-1 * Ct * delta
        matCprime.StoreTranspose(matC);
        matCinv.StoreInverse(matCprime*matC);
        matRHS = matCinv * matCprime;
		
        // matRHS * delta; note that delta is actually delta copied 3 times
        //     delta x
        //     delta y
        //        ...
        // What to put in each row in the matrix
        // x constraint: a * deltax + s * deltay
        // y constraint: -s * deltax + a * deltay
        for ( int i = 0; i < 6; i++ ) {
            matTDelta[i][0] = in_vecDelta[0] * matRHS[0][i] + in_vecDelta[1] * matRHS[1][i];
            matTDelta[i][1] = in_vecDelta[1] * matRHS[0][i] - in_vecDelta[0] * matRHS[1][i];
        }
    }
    
    return matTDelta;
}



/*
 * Laplacian of curve: -0.5 * pt[i-1] + pt[i] - 0.5 * pt[i+1]
 * Delta is Laplacian matrix * point locations (ie, difference of pt from it's neighbors)
 * This is basically Olga's code. 
 *
 * Cases: 
 *   Curve is not closed
 *     a) Drag point is in middle of curve. Pin both ends and the drag point (3 pins)
 *     b) Drag point is one end of selected region. Pin opposite end and drag point (2 pins)
 *   Curve is closed
 *     a) Entire curve selected. Pin point at TBracket and drag point (2 pins)
 *     b) Region of curve selected, drag point is in middle of region. Pin drag point and end points (3 pins)
 *     c) Region of curve selected, drag point is end point. Pin drag point and opposite end (2 pins)
 *
 * For closed curve, allow the indices to wrap.
 */
Array<R3Vec> Curve::Drag2DLaplacian( const R3Vec &in_vecDrag, const double in_dTDrag, 
									const std::pair<double,double> &in_dTBracket, const bool in_bIsClosed ) const
{
    // 2D Laplacian Surface Editing Solver
    //	matXY (n x 2) is the xy positions of the contour vertices, which will get updated 
    //	in_iNumV is the number of vertices (n)
    //	matL (n x n) is the Laplacian, matL = I - Inv(D)*A
    //	matDelta (n x 2) is the Laplacian coordinates
    //	in_aiAnchors is the indicies of the anchor points
    //	in_aiHandles is the indicies of the handle points
    //	in_aptTargets is the target locations for the handles
    
    // oLS is our sparse least squares system Ax = b
    //		oLS.Desired_sparse(i, aiRow) adds a row to the A matrix ( 2*(n+numAnchors+numTargets) x 2n )
    //		oLS.Rhs_sparse(i,j) accesses the b matrix ( 2*(n+numAnchors+numTargets) x 1 )
    //		oLS.Solution_sparse(i,j) accesses the x matrix (2n x 1)
    Array<R3Vec> avecOut(m_apt.num());
    avecOut.fill( R3Vec(0,0,0) );
    
    const int iStartV = FloorWrap( in_dTBracket.first );
    const int iEndV = WINmin( CeilWrap( in_dTBracket.second ), NumPts() - 1 );
    const int iDragV = FloorWrap( in_dTDrag );
    const double dDragVPerc = WINminmax( in_dTDrag - PercAlongWrap(iDragV), 0.0, 1.0 );

    if ( iStartV > iDragV || iDragV > iEndV || iStartV > NumPts() || iEndV - iStartV > NumPts() ) {
        cerr << "ERR: Curve::Drag2DLaplacian, bad start and end " << iStartV << " " << iDragV << " " << iEndV << "\n";
        return avecOut;
    }
    
    if ( s_bTrace ) {
        cerr << "Start and end " << iStartV << " " << iDragV << " " << iEndV << "\n";
        for ( int i = 0; i < NumPts(); i++ ) {
            cout << i << " " << m_apt[i] << "\n";
        }
    }
    
    const bool bAddPinStart = iDragV != iStartV;
    const bool bAddPinEnd = iDragV != iEndV;
    
    // Start at iStartV+1 and end at iEndV - 2
    const int iNVs = WINmin( iEndV - iStartV - 1, NumPts() - 2 );
	
    if ( s_bTrace ) {
        cout << "T values " << in_dTBracket.first << " " << in_dTDrag << " " << in_dTBracket.second << "  StartIndex ";
        cout << iStartV << " " << iDragV << " " << iEndV << " " << iNVs << " NPts " << NumPts() << "\n";
        cout << iStartV % NumPts() << " " << iDragV % NumPts() << " " << iEndV % NumPts() << "\n";
    }
    
    if ( iNVs < 3 ) {
        cerr << "WARNING: Curve::Drag2DLaplacian can't drag - region too small " << in_dTBracket.first << " " << in_dTBracket.second << "\n";
        return avecOut;
    }
    
    int iNPins = 1; // for drag
    if ( bAddPinStart ) {
        if ( iStartV == 0 && !in_bIsClosed ) {
            iNPins += 1;
        } else {
            iNPins += 2;
        }
    }
    
    if ( bAddPinEnd ) {
        if ( iEndV == NumPts() - 1 && !in_bIsClosed ) {
            iNPins += 1;
        } else {
            iNPins += 2;
        }
    }
    
    if ( s_bTrace ) cout << "NPins " << iNPins << "\n";
    
    FITTools_LS oLS( 2*(iNVs + iNPins), 2*(iNVs+2), 1);
    
    /*
     * Build Laplacian * points
     *   Laplacian is -0.5 * pt[i-1] + pt[i] - 0.5 * pt[i+1]
     */
    static Array<R3Pt> apt(3);    
    R2Vec vecDelta;

    const int iVIndex = iNVs + 2; 
    int iRowIndex = 0;
    for ( int i = 1; i < iNVs+1; i++ ) {
        
        const int iPtIndex = iStartV + i;
        apt[0] = m_apt.wrap( iPtIndex - 1 );
        apt[1] = m_apt.wrap( iPtIndex );
        apt[2] = m_apt.wrap( iPtIndex + 1 );
        for ( int j = 0; j < 2; j++ ) {
            vecDelta[j] = -0.5 * apt[0][j] + apt[1][j] - 0.5 * apt[2][j];
        }
        const Array<R2Pt> &aptDelta = CalcMatrix( apt, vecDelta );
        if ( ApproxEqual(apt[0], apt[1], 1e-6) ) {
            cerr << "Similar points1 " << i << "\n";
        }
        if ( ApproxEqual(apt[1], apt[2], 1e-6) ) {
            cerr << "Similar points2 " << i << "\n";
        }
        if ( ApproxEqual(apt[0], apt[2], 1e-6) ) {
            cerr << "Similar points3 " << i << "\n";
        }
		
        // L - TDelta, u constraint
        oLS.Desired(iRowIndex, i-1) = -0.5 - aptDelta[0][0];
        oLS.Desired(iRowIndex, i  ) =  1.0 - aptDelta[1][0];
        oLS.Desired(iRowIndex, i+1) = -0.5 - aptDelta[2][0];
        
        // 0 - TDelata
        oLS.Desired(iRowIndex, iVIndex+i-1) = - aptDelta[3][0];
        oLS.Desired(iRowIndex, iVIndex+i  ) = - aptDelta[4][0];
        oLS.Desired(iRowIndex, iVIndex+i+1) = - aptDelta[5][0];
        
        double dSum = 0.0;
        for ( int j = -1; j < 2; j++ ) {
            dSum += fabs( oLS.Desired(iRowIndex, i+j) );
            dSum += fabs( oLS.Desired(iRowIndex, iVIndex + i+j) );
        }
        if ( dSum < 1e-6 ) {
            cerr << "Bad row " << dSum << "\n";
            cerr << apt[0] << " " << apt[1] << " " << apt[2] << "\n";
        }
        iRowIndex++;

        // L - TDelta, v constraint
        oLS.Desired(iRowIndex, iVIndex+i-1) = -0.5 - aptDelta[3][1];
        oLS.Desired(iRowIndex, iVIndex+i  ) =  1.0 - aptDelta[4][1];
        oLS.Desired(iRowIndex, iVIndex+i+1) = -0.5 - aptDelta[5][1];
        
        // 0 - TDelata
        oLS.Desired(iRowIndex, i-1) = - aptDelta[0][1];
        oLS.Desired(iRowIndex, i  ) = - aptDelta[1][1];
        oLS.Desired(iRowIndex, i+1) = - aptDelta[2][1];
        iRowIndex++;        
    }
	
    const double dWeightAnchor = 1000.0;
    
    // Pin down ends and drag point
    const R3Pt ptDrag = (*this)( in_dTDrag > 1.0 ? (in_dTDrag - 1.0) : in_dTDrag ) + in_vecDrag;
    const R3Pt ptStart = bAddPinStart ? m_apt.wrap(iStartV) : ptDrag;
    const R3Pt ptEnd = bAddPinEnd ? m_apt.wrap(iEndV) : ptDrag;
    
    // Pin start value down (u and v)
    oLS.Desired(iRowIndex, 0) = dWeightAnchor;
    oLS.Rhs(iRowIndex, 0) = dWeightAnchor * ptStart[0];
    iRowIndex++;
    
    oLS.Desired(iRowIndex, iVIndex) = dWeightAnchor;
    oLS.Rhs(iRowIndex, 0) = dWeightAnchor * ptStart[1];
    iRowIndex++;

    /// Pin end value down (u and v)
    oLS.Desired(iRowIndex, iVIndex - 1) = dWeightAnchor;
    oLS.Rhs(iRowIndex, 0) = dWeightAnchor * ptEnd[0];
    iRowIndex++;
    
    oLS.Desired(iRowIndex, 2 * iVIndex - 1) = dWeightAnchor;
    oLS.Rhs(iRowIndex, 0) = dWeightAnchor * ptEnd[1];
    iRowIndex++;
    
    if ( bAddPinStart && bAddPinEnd ) {
        const int iDragIndex = iDragV - iStartV;
        oLS.Desired(iRowIndex+0, iDragIndex) = (1.0 - dDragVPerc) * dWeightAnchor;
        oLS.Desired(iRowIndex+1, iVIndex + iDragIndex) = (1.0 - dDragVPerc) * dWeightAnchor;
        
        oLS.Desired(iRowIndex+0, iDragIndex+1) = (dDragVPerc) * dWeightAnchor;
        oLS.Desired(iRowIndex+1, iVIndex + iDragIndex+1) = (dDragVPerc) * dWeightAnchor;
        
        oLS.Rhs(iRowIndex+0, 0) = dWeightAnchor * ptDrag[0];
        oLS.Rhs(iRowIndex+1, 0) = dWeightAnchor * ptDrag[1];
        
        iRowIndex += 2;
    }
    
    // More lightly pin down the next point if interior
    if ( bAddPinStart && ( iStartV != 0 || in_bIsClosed ) ) {
        // Pin next point down (u and v)
        oLS.Desired(iRowIndex, 1) = dWeightAnchor * 0.5;
        oLS.Rhs(iRowIndex, 0) = dWeightAnchor * 0.5 * m_apt.wrap( iStartV + 1 )[0];
        iRowIndex++;
        
        oLS.Desired(iRowIndex, iVIndex + 1) = dWeightAnchor * 0.5;
        oLS.Rhs(iRowIndex, 0) = dWeightAnchor * 0.5 * m_apt.wrap( iStartV + 1 )[1];
        iRowIndex++;
    }
    
    // More lightly pin down the next point if interior
    if ( bAddPinEnd && ( iEndV != NumPts() - 1 || in_bIsClosed ) ) {
        // Pin next point down (u and v)
        oLS.Desired(iRowIndex, iVIndex - 2) = dWeightAnchor * 0.5;
        oLS.Rhs(iRowIndex, 0) = dWeightAnchor * 0.5 * m_apt.wrap( iEndV - 1 )[0];
        iRowIndex++;
        
        oLS.Desired(iRowIndex, 2 * iVIndex - 2) = dWeightAnchor * 0.5;
        oLS.Rhs(iRowIndex, 0) = dWeightAnchor * 0.5 * m_apt.wrap( iEndV - 1 )[1];
        iRowIndex++;
    }
    
    if ( s_bTrace ) oLS.Print(cout);
    if ( !oLS.Solve() ) {
        oLS.Print(cout);
        ASSERT(FALSE);
    }
    
    if ( s_bTrace ) {
        cout << "End Points\n";
        cout << iStartV << " " << m_apt.wrap(iStartV) << oLS.Solution(0,0) << " " << oLS.Solution(iVIndex,0) << "\n";
        cout << iEndV << " " << m_apt.wrap(iEndV) << oLS.Solution(iVIndex-1,0) << " " << oLS.Solution(2*iVIndex-1,0) << "\n";
        const int iDragIndex = iDragV - iStartV;
        const int iDragIndexNext = WINmin( iDragIndex+1, iVIndex-1 );
        cout << iDragV << " " << ptDrag << (1.0 - dDragVPerc) * oLS.Solution(iDragIndex,0) + dDragVPerc * oLS.Solution(iDragIndexNext, 0) 
        << " " << (1.0 - dDragVPerc) * oLS.Solution(iVIndex + iDragIndex,0) + dDragVPerc * oLS.Solution(iVIndex + iDragIndexNext, 0) << "\n";
        cout << " Drag " << in_vecDrag << "\n";
        cout << "vecs\n";
    }
	
    for ( int i = 0; i < iNVs+2; i++ ) {
		const int iPtIndex = iStartV + i;
        avecOut.wrap( iPtIndex )[0] = ( oLS.Solution(i, 0) - m_apt.wrap( iPtIndex )[0] );
        avecOut.wrap( iPtIndex )[1] = ( oLS.Solution(iVIndex + i, 0) - m_apt.wrap( iPtIndex )[1] );
		
        if ( s_bTrace ) cout << iPtIndex << " " << iPtIndex % NumPts() << " " << avecOut.wrap(iPtIndex) << "    " << oLS.Solution(i, 0) << "\n";
    }
    if ( bAddPinStart ) {
        avecOut.wrap( iStartV ) = R3Vec(0,0,0);
    }
    if ( bAddPinEnd ) {
        avecOut.wrap( iEndV) = R3Vec(0,0,0);
    }
    
    return avecOut;
}


/*
 * Bare bones quadratic distance fall off
 */
Array<R3Vec> Curve::Drag2DQuadraticFalloff( const R3Vec &in_vecDrag, const double in_dTDrag, 
										   const std::pair<double,double> &in_dTBracket ) const
{
    Array<R3Vec> avecOut(m_apt.num());
    Array<double> adWeight( m_apt.num() );
    CRVHermite2 crvFalloff(3);
    crvFalloff.SetPt(0) = R2Pt(0.0, 0.0);
    crvFalloff.SetPt(1) = R2Pt(0.5, 1.0);
    crvFalloff.SetPt(2) = R2Pt(1.0, 0.0);
    crvFalloff.SetVec(0) = R2Vec(0.5, 0.0);
    crvFalloff.SetVec(1) = R2Vec(0.5, 0.0);
    crvFalloff.SetVec(2) = R2Vec(0.5, 0.0);
	
    if ( in_dTDrag > in_dTBracket.second || in_dTDrag < in_dTBracket.first ) {
		cerr << "ERR: Drag2DQuadraticFalloff t values are not good " << in_dTBracket.first << " " << in_dTDrag << " " << in_dTBracket.second << "\n";
    }
	
	const double dDist = g_drawState.SelectionSize();
	double dTDrag = in_dTDrag;
	if ( fabs( in_dTDrag - in_dTBracket.first ) * Length() < dDist ) {
		dTDrag = in_dTBracket.first;
	}
	
	if ( fabs( in_dTDrag - in_dTBracket.second ) * Length() < dDist ) {
		dTDrag = in_dTBracket.second;
	}
	
    for ( int i = 0; i < m_apt.num(); i++ ) {
		double dTAlong = PercAlong(i);
		if ( IsClosed() ) {
			if ( dTAlong < in_dTBracket.first ) {
				dTAlong += 1.0;
			} else if ( dTAlong > in_dTBracket.second ) {
				dTAlong -= 1.0;
			}
		}
        if ( dTAlong < in_dTBracket.first || dTAlong > in_dTBracket.second ) {
            adWeight[i] = 0.0;
        } else if ( dTAlong < dTDrag ) {
            if ( RNIsZero( dTDrag - in_dTBracket.first ) ) {
                adWeight[i] = 1.0;
            } else {
                const double dX = (dTAlong - in_dTBracket.first) / ( dTDrag - in_dTBracket.first );
                assert( dX >= 0.0 && dX <= 1.0 );
                adWeight[i] = crvFalloff( dX * 0.5 )[1];
            }
        } else {
            if ( RNIsZero( in_dTBracket.second - dTDrag ) ) {
                adWeight[i] = 1.0;
            } else {
                const double dX = (dTAlong - dTDrag) / ( in_dTBracket.second - dTDrag );
                assert( dX >= 0.0 && dX <= 1.0 );
                adWeight[i] = crvFalloff( 0.5 + dX * 0.5 )[1];
            }
        }
        assert( adWeight[i] >= 0.0 && adWeight[i] <= 1.0 );
        avecOut[i] = in_vecDrag * adWeight[i];
    }
    return avecOut;
}

/*
 * Bare bones cubic distance fall off. Swiped from Adobe Illustrator code (thanks Paul!)
 */
Array<R3Vec> Curve::Drag2DCubicFalloff( const R3Vec &in_vecDrag, const double in_dTDrag, 
									   const std::pair<double,double> &in_dTBracket ) const
{
    Array<R3Vec> avecOut(m_apt.num());
	
    if ( in_dTDrag > in_dTBracket.second || in_dTDrag < in_dTBracket.first ) {
		cerr << "ERR: Drag2DCubicFalloff t values are not good " << in_dTBracket.first << " " << in_dTDrag << " " << in_dTBracket.second << "\n";
    }
	
	const double dDist = g_drawState.SelectionSize();
	double dTDrag = in_dTDrag;
	if ( fabs( in_dTDrag - in_dTBracket.first ) * Length() < dDist ) {
		dTDrag = in_dTBracket.first;
	}
	
	if ( fabs( in_dTDrag - in_dTBracket.second ) * Length() < dDist ) {
		dTDrag = in_dTBracket.second;
	}
	
    for ( int i = 0; i < m_apt.num(); i++ ) {
		double dTAlong = PercAlong(i);
		if ( IsClosed() ) {
			if ( dTAlong < in_dTBracket.first ) {
				dTAlong += 1.0;
			} else if ( dTAlong > in_dTBracket.second ) {
				dTAlong -= 1.0;
			}
		}
		
		double dWeight = 0.0;
        if ( dTAlong >= in_dTBracket.first && dTAlong <= in_dTBracket.second ) {
			if ( dTAlong < dTDrag ) {
				if ( RNApproxEqual( dTDrag, in_dTBracket.first ) ) {
					dWeight = 1.0;
				} else {
					const double dX = (dTAlong - in_dTBracket.first) / ( dTDrag - in_dTBracket.first );
					assert( dX >= 0.0 && dX <= 1.0 );
					dWeight = dX * dX * (3.0 - 2.0 * dX);
				}
			} else {
				if ( RNApproxEqual( in_dTBracket.second, dTDrag ) ) {
					dWeight = 1.0;
				} else {
					const double dX = (dTAlong - dTDrag) / ( in_dTBracket.second - dTDrag );
					assert( dX >= 0.0 && dX <= 1.0 );
					dWeight = 1.0 - dX * dX * (3.0 - 2.0 * dX);
				}
			}
		}
        assert( dWeight >= 0.0 && dWeight <= 1.0 );
        avecOut[i] = in_vecDrag * dWeight;
    }
    return avecOut;
}


/*
 * Drag the entire curve
 */
void Curve::Drag( const R3Vec &in_vec ) 
{
    for ( int i = 0; i < m_apt.num(); i++ ) {
        m_apt[i] = m_apt[i] + in_vec;
    }
	
	m_bbox.first = m_bbox.first + in_vec;
	m_bbox.second = m_bbox.second + in_vec;
}

/*
 * Transform the entire curve
 */
void Curve::Transform( const R4Matrix &in_mat ) 
{
    for ( int i = 0; i < m_apt.num(); i++ ) {
        m_apt[i] = in_mat * m_apt[i];
    }
	
	m_bPercData = false;
    m_bTangentData = false;

	SetAllData();
}



/*
 * Probably should eventually make this 3D, but, we cheat a bit here
 * Project the curve onto the drawing plane
 * Calculate fall-offs on the projected curve
 * Use that to figure out 3D move vectors
 * Apply vectors to original curve
 *
 * To select across the end of a closed curve, set the second t value to be greater than 1.0
 */
void Curve::Drag( const R3Plane &in_plane, const R3Vec &in_vec, const double in_dTDrag, const std::pair<double,double> &in_dTBracket, const bool in_bUseLaplacian )
{
    Curve crvProject = ProjectAndRotate( in_plane );
    R3Matrix matRot;
    R3Matrix::MatrixVecToVec( matRot, in_plane.Normal(), R3Vec(0,0,1) );
    
    const bool bIsClosed = IsClosed();

    // Will correctly handle wrapped curve
    const int iPtIndex = Floor( in_dTDrag );
    const bool bIsPt = ( RNApproxEqual( in_dTDrag, PercAlong( iPtIndex ) ) );
    const R3Pt ptFinal = (*this)( in_dTDrag ) + in_vec;
    
    Array<R3Vec> avec;
    const R3Vec vecInPlane = matRot * in_vec;
    if ( in_bUseLaplacian ) {
        if ( s_bTrace ) {
            cout << "start " << crvProject.FloorWrap( in_dTBracket.first ) << " ";
            cout << "drag " << crvProject.ClosestIndexWrap(in_dTDrag ) << " ";
            cout << "end " << crvProject.CeilWrap( in_dTBracket.second ) << "\n";
        }
        
        if ( bIsClosed ) {
            crvProject.DeleteLastPoint();
        }
        avec = crvProject.Drag2DLaplacian( vecInPlane, in_dTDrag, in_dTBracket, bIsClosed );
    } else {
        avec = crvProject.Drag2DCubicFalloff( vecInPlane, in_dTDrag, in_dTBracket );
    }

    matRot = matRot.Transpose();
    
    for ( int i = 0; i < m_apt.num(); i++ ) {
        const R3Vec vecRotateBack = matRot * avec.wrap(i);
        m_apt[i] = m_apt[i] + vecRotateBack;
        if ( s_bTrace ) cout << i << " " << vecRotateBack << "   " << m_apt[i] << "\n";
    }
    
    // Make absolutely sure this point went where it was suppose to go
    if ( bIsPt ) {
        m_apt[iPtIndex] = ptFinal;
        if ( s_bTrace ) {
            cout << "Pinning " << iPtIndex << " pt " << m_apt[iPtIndex] << " " << ptFinal << "\n";
        }
    }
    
    if ( bIsClosed ) {
        const R3Pt ptAvg = Lerp( m_apt[0], m_apt.last(), 0.5 );
        m_apt[0] = ptAvg;
        m_apt.last() = ptAvg;
    }
	
    m_bPercData = false;
    m_bTangentData = false;

    SetAllData();

    for ( int i = 0; i < NumPts(); i++ ) {
        if ( ApproxEqual( m_apt[i], m_apt.wrap(i+2), 1e-6 ) ) {
            cerr << "Same pt " << i << "\n";
        }
    }
}




