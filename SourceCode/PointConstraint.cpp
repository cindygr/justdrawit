/*
 *  PointConstraint.cpp
 *  SketchCurves
 *
 *  Created by Cindy Grimm on 9/15/11.
 *  Copyright 2011 Washington University in St. Louis. All rights reserved.
 *
 */

#include <utils/Rn_Defs.h>
#include "PointConstraint.h"
#include "CurveGroup.h"
#include "ParamFile.h"
#include "CurveNetwork.h"
#include <fitting/FITTools_SVD.H>

static char s_astrType[4][50] = 
{
    "POINT_ONLY",
    "NORMAL_ONLY", 
    "POINT_NORMAL",
    "INVALID"
};

static char s_astrTypeAction[9][50] = 
{
    "NO_ACTION",
    "NEW_POINT",
    "NEW_NORMAL",
    "NEW_POINT_NORMAL",
    "ADDED_CURVEGROUP",
    "REMOVED_CURVEGROUP",
    "CHANGED_POINT",
    "CHANGED_NORMAL",
    "UNDO"
};

/* Save the current state then update the time stamp.
 * Don't do this if the time stamp hasn't changed, ie, we're in the middle of a drag */
void PointConstraint::TimeStamp() 
{ 
    for ( int i = m_aHistory.num() - 1; i >= 0; i-- ) {
        if ( m_aHistory[i].m_iTimeStamp == CurveNetwork::CurrentTime() ) {
            if ( m_aHistory[i].m_action == m_data.m_action ) {
                return;
            }
        } else { 
            break;
        }
    }

    m_aHistory += m_data; 
    m_data.m_iTimeStamp = CurveNetwork::CurrentTime(); 
    
}

std::string PointConstraint::GetTypeString( const ConstraintType in_type )
{
    if ( (int) in_type >= 0 && (int) in_type < 3 ) {
        return std::string( s_astrType[ (int) in_type ] );
    }
    
    return std::string( s_astrType[3] );
}

std::string PointConstraint::GetActionString( const ActionType in_type )
{
    return std::string( s_astrTypeAction[ WINminmax( (int) in_type, 0, 9 ) ] );
}

double PointConstraint::ConstraintData::GetTValue( const CurveGroup &in_grp ) const
{
    for ( int i = 0; i < NumCurves(); i++ ) {
        if ( *m_aci[i].m_opCG == in_grp ) {
            return m_aci[i].m_dT;
        }
    }
    
    cerr << "ERR: PointConstraint::GetTValue, no curve " << in_grp.HashId() << "\n";
    return -1.0;
}

int PointConstraint::ConstraintData::GetPtIndex( const CurveGroup &in_grp ) const
{
    for ( int i = 0; i < NumCurves(); i++ ) {
        if ( *m_aci[i].m_opCG == in_grp ) {
            return m_aci[i].m_iPtIndex;
        }
    }
    
    cerr << "ERR: PointConstraint::GetPtIndex, no curve " << in_grp.HashId() << "\n";
    return -1;
}

R3Pt PointConstraint::ConstraintData::GetPt( const CurveGroup &in_grp ) const
{
    for ( int i = 0; i < NumCurves(); i++ ) {
        if ( *m_aci[i].m_opCG == in_grp ) {
            return m_aci[i].m_opCG->CompositeCurve().Pt( m_aci[i].m_iPtIndex );
        }
    }
    
    cerr << "ERR: PointConstraint::GetPt, no curve " << in_grp.HashId() << "\n";
    return R3Pt(1e30, 1e30, 1e30 );
}

R3Pt PointConstraint::ConstraintData::GetPt( const int in_i ) const
{
    if ( in_i < 0 || in_i >= NumCurves() ) {
        cerr << "ERR: PointConstraint::ConstraintData::GetPt, bad index " << in_i << " " << NumCurves() << "\n";
        return R3Pt(0,0,0);
    }
    return m_aci[in_i].m_opCG->CompositeCurve()( m_aci[in_i].m_dT );
}

bool PointConstraint::HadCurveGroupPast( const CurveGroup &in_grp, const int in_iTimeStamp ) const
{
    if ( m_data.m_iTimeStamp < in_iTimeStamp ) {
        return m_data.HasCurveGroup( in_grp );
    }
    
    for ( int i = m_aHistory.num()-1; i >= 0; i-- ) {
        if ( m_aHistory[i].m_iTimeStamp < in_iTimeStamp ) {
            return m_aHistory[i].HasCurveGroup( in_grp );
        }
    }
    
    return false;
}

double PointConstraint::GetTValuePast( const CurveGroup &in_grp, const int in_iTimeStamp ) const
{
    if ( m_data.m_iTimeStamp < in_iTimeStamp ) {
        return m_data.GetTValue( in_grp );
    }
    
    for ( int i = m_aHistory.num()-1; i >= 0; i-- ) {
        if ( m_aHistory[i].m_iTimeStamp < in_iTimeStamp ) {
            return m_aHistory[i].GetTValue( in_grp );
        }
    }
    
    cerr << "ERR: PointConstraint::GetTValuePast, not valid time stamp " << in_iTimeStamp << "\n";
    return 0.0;
}

bool PointConstraint::IsHidden() const
{
    for ( int i = 0; i < NumCurves(); i++ ) {
        if ( !m_data.m_aci[i].m_opCG->IsHidden() ) {
            return false;
        }
    }
    
    return true;
}


bool PointConstraint::ConstraintData::HasCurveGroup( const CurveGroup &in_grp ) const
{
    for ( int i = 0; i < NumCurves(); i++ ) {
        if ( *(m_aci[i].m_opCG) == in_grp ) {
            return true;
        }
    }
    return false;
}

void PointConstraint::SetPointConstraint( const R3Pt &in_pt )
{
    if ( !ApproxEqual(in_pt, m_data.m_pt) || m_data.m_type != POINT_ONLY ) {
        TimeStamp();
    }
    
    m_data.m_pt = in_pt;
    m_data.m_type = POINT_ONLY;
    m_data.m_action = NEW_POINT;
    
    SetNormalConstraint();
    m_drawData.GeometryChanged();
}

///
void PointConstraint::SetNormalConstraint( const R3Vec &in_vec )
{
    if ( !ApproxEqual(in_vec, m_data.m_vec) || m_data.m_type != NORMAL_ONLY ) {
        TimeStamp();
    }
    
    m_data.m_vec = in_vec;
    m_data.m_type = NORMAL_ONLY;
    m_data.m_action = NEW_NORMAL;

    m_drawData.GeometryChanged();
}

void PointConstraint::SetNormalConstraint()
{
    if ( NumCurves() == 1 ) {
        m_data.m_vec = m_data.m_aci[0].m_opCG->GetCurveRibbon().Normal( m_data.m_aci[0].m_dT );
    } else if ( NumCurves() > 1 ) {
        Array<R3Pt> apt;
        const double dDist = g_drawState.SelectionSize() * 0.1;
        
        for ( int i = 0; i < NumCurves(); i++ ) {
            const Curve &crv = m_data.m_aci[i].m_opCG->CompositeCurve();
            const double dTCrv = m_data.m_aci[i].m_dT;
            
            const double dTStep = dDist / crv.Length();
            for ( int iS = -5; iS <= 5; iS++ ) {
                const double dS = dTCrv + dTStep * iS;
                if ( crv.IsClosed() || ( dS >= 0.0 && dS <= 1.0 ) ) {
                    apt += crv(dS);
                }
            }
        }
        if ( apt.num() > 3 ) {
            FITToolsSVDLapack oSVD(apt.num(), 3, false, true);
            for ( int i = 0; i < apt.num(); i++ ) {
                const R3Vec vec = apt[i] - m_data.m_pt;
                for ( int j = 0; j < 3; j++ ) {
                    oSVD.A(i,j) = vec[j];
                }
            }
            oSVD.Solve();
            for ( int j = 0; j < 3; j++ ) {
                m_data.m_vec[j] = oSVD.Vt(2, j);
            }	
            
        } else {
            const R3Vec vecTang1 = m_data.m_aci[0].m_opCG->CompositeCurve().Tangent( m_data.m_aci[0].m_dT );
            const R3Vec vecTang2 = m_data.m_aci[1].m_opCG->CompositeCurve().Tangent( m_data.m_aci[1].m_dT );
            m_data.m_vec = Cross( vecTang1, vecTang2 );
        }
        if ( RNIsZero( ::Length( m_data.m_vec ) ) ) {
            m_data.m_vec = m_data.m_aci[0].m_opCG->GetCurveRibbon().Normal( m_data.m_aci[0].m_dT );
        } else {
            m_data.m_vec.Normalize();
        }
    }

    int iReversed = 0, iNotReversed = 0;
    for ( int i = 0; i < NumCurves(); i++ ) {
        for ( int j = 0; j < GetCurveGroup(i).NumConstraints(); j++ ) {
            const PointConstraint &pin = GetCurveGroup(i).GetConstraint(j);
            if (pin.HashId() != HashId() && pin.IsNormal() ) {
                if ( Dot( pin.GetNormal(), GetNormal() ) < 0 ) {
                    iReversed++;
                } else {
                    iNotReversed++;
                }
            }
        }
    }
    if ( iReversed + iNotReversed == 0 ) {
        for ( int i = 0; i < NumCurves(); i++ ) {
            if ( Dot( GetNormal(), GetCurveGroup(i).GetCurveRibbon().Normal( GetTValue(i) ) ) < 0.0 ) {
                iReversed++;
            } else {
                iNotReversed++;
            }
        }
    }
    /*
    for ( int i = 0; i < NumCurves(); i++ ) {
        const R3Vec vecToCenter = m_data.GetPt(i) - m_data.m_pt;
        if ( Dot( m_data.m_vec, vecToCenter ) < 0.0 ) {
            iReversed++;
        }
    }
     */
    if ( iReversed >= iNotReversed ) {
        m_data.m_vec *= -1.0;
    }
    
}


/* Had a point (or normal) constraint and now want to add the one that's missing */
void PointConstraint::ConvertToPointAndNormalConstraint( const R3Pt &in_ptCenter )
{
    
    if ( IsNormal() && IsPoint() ) {
        return;
    } else if ( IsNormal() && !IsPoint() ) {
        R3Pt ptAvg(0,0,0);
        for ( int i = 0; i < NumCurves(); i++ ) {
            const R3Pt ptCrv = m_data.GetPt(i); 
            for ( int j = 0; j < 3; j++ ) {
                ptAvg[j] += ptCrv[j] / (double) NumCurves();
            }
        }
        if ( NumCurves() ) {
            m_data.m_pt = ptAvg;
        }
    } else if ( !IsNormal() && IsPoint() ) {
        SetNormalConstraint();
    } 
    
    for ( int i = 0; i < NumCurves(); i++ ) {
        m_data.m_aci[i].m_opCG->ReEstablishNormalConstraints();
    }    
    
    TimeStamp();
    m_data.m_type = POINT_AND_NORMAL;
    m_data.m_action = CHANGED_TYPE;
    
    m_drawData.GeometryChanged();
}

///
void PointConstraint::SetPointAndNormalConstraint( const R3Pt &in_pt, const R3Vec &in_vec )
{
    if ( !ApproxEqual(in_pt, m_data.m_pt) || !ApproxEqual(in_vec, m_data.m_vec) || m_data.m_type != POINT_AND_NORMAL ) {
        TimeStamp();
    }
    
    m_data.m_pt = in_pt;
    m_data.m_vec = in_vec;
    m_data.m_type = POINT_AND_NORMAL;
    m_data.m_action = NEW_POINT_NORMAL;
    
    m_drawData.GeometryChanged();
}

void PointConstraint::Merge( const PointConstraint &in_pin )
{
    for ( int i = 0; i < in_pin.NumCurves(); i++ ) {
        AddCurveGroup( in_pin.m_data.m_aci[i].m_opCG, in_pin.m_data.m_aci[i].m_dT, in_pin.m_data.m_aci[i].m_iPtIndex );
    }
    
    if ( !IsNormal() && in_pin.IsNormal() ) {
        m_data.m_vec = in_pin.m_data.m_vec;
    } else if ( !IsNormal() && !in_pin.IsNormal() ) {
        SetNormalConstraint();
    }
    
    m_drawData.GeometryChanged();
}

void PointConstraint::Write( ofstream &out, const Array<int> &in_aiMapId ) const
{
    out << "PointConstraintType " << GetTypeString( m_data.m_type ) << " " << GetPt() << GetNormal();
    out << "NCurves " << NumCurves() << "\n";
    for ( int i = 0; i < NumCurves(); i++ ) {
        out << in_aiMapId[ m_data.m_aci[i].m_opCG->HashId() ] << " " << m_data.m_aci[i].m_dT << " " << m_data.m_aci[i].m_iPtIndex << "\n";
    }
}

/* The constraints and the curves are written out at the same resolution so
 * the point corresponding to the curve index should be *exactly* the same/
 * The t value, however, may have changed because the distances will be slightly
 * different. So use the index value to get the new t value */
void PointConstraint::Read( ifstream &in, const Array< CurveGroup * > &in_aopCrv ) 
{
    std::string str;

    m_aHistory.need(0); // Erase history
    
    in >> str; ASSERT( !str.compare("PointConstraintType") );
    in >> str;
    m_data.m_type = INVALID;
    for ( int i = 0; i < 3; i++ ) {
        if ( !str.compare( s_astrType[i] ) ) {
            m_data.m_type = ( ConstraintType ) (i);
        }
    }
    
    ASSERT( in.good() );
    in >> m_data.m_pt;
    ASSERT( in.good() );
    in >> m_data.m_vec;
    ASSERT( in.good() );
    
    in >> str; ASSERT( !str.compare("NCurves") );
    
    int iN = 0;
    in >> iN;
    m_data.m_aci.need( 0 );
    int iId = 0, iIndex = 0;
    double dT = 0;
    for ( int i = 0; i < iN; i++ ) {
        in >> iId >> dT >> iIndex;
        in_aopCrv[iId]->AddConstraint(this, in_aopCrv[iId]->CompositeCurve().PercAlong(iIndex), false);
    }
    m_data.m_iTimeStamp = CurveNetwork::CurrentTime();
    
    m_drawData.GeometryChanged();    
}

PointConstraint::CurveInfo &PointConstraint::CurveInfo::operator=( const CurveInfo &in_ci )
{
    m_opCG = in_ci.m_opCG;
    m_dT = in_ci.m_dT;
    m_iPtIndex = in_ci.m_iPtIndex;
    
    return *this;
}

PointConstraint::ConstraintData &PointConstraint::ConstraintData::operator=( const ConstraintData &in_ci )
{
    m_type = in_ci.m_type;
    m_action = in_ci.m_action;
    m_pt = in_ci.m_pt;
    m_vec = in_ci.m_vec;
    m_aci = in_ci.m_aci;
    m_iTimeStamp = in_ci.m_iTimeStamp;
    
    return *this;
}

void PointConstraint::AddCurveGroup( CurveGroup *in_opCG, const int in_iPtIndex, const double in_dT )
{
    int iIndex = -1;
    for ( int i = 0; i < m_data.m_aci.num(); i++ ) {
        if ( *m_data.m_aci[i].m_opCG == *in_opCG ) {
            iIndex = i;
            break;
        }
    }
    
    CurveInfo ci;
    // Does the curve exist in the list?
    if ( iIndex != -1 ) {
        // If it does, is this a new t/pt value? 
        if ( RNApproxEqual( m_data.m_aci[iIndex].m_dT, in_dT ) && m_data.m_aci[iIndex].m_iPtIndex && in_iPtIndex ) {
            return;
        }
    }

    // Something changed, time stamp
    TimeStamp();

    if ( iIndex == -1 ) {
        m_data.m_aci.add(1);
        iIndex = m_data.m_aci.num() - 1;
    }
    
    m_data.m_action = ADDED_CURVEGROUP;
    m_data.m_aci[iIndex].m_opCG = in_opCG;
    m_data.m_aci[iIndex].m_dT = in_dT;
    m_data.m_aci[iIndex].m_iPtIndex = in_iPtIndex;
    
    // Keep the point valid if at all possible, even for normal constraints
    if ( !IsPoint() ) {
        m_data.m_pt = in_opCG->CompositeCurve().Pt( in_iPtIndex );
    }
    if ( !IsNormal() ) {
        SetNormalConstraint();
    }
    m_drawData.GeometryChanged();
}

/// Returns number of curves left
int PointConstraint::RemoveCurveGroup( CurveGroup *in_opCG )
{
    int iIndex = -1;
    for ( int i = 0; i < m_data.m_aci.num(); i++ ) {
        if ( *m_data.m_aci[i].m_opCG == *in_opCG ) {
            if ( iIndex != -1 ) {
                cerr << "ERR: PointConstraint::RemoveCurveGroup, duplicate curve group " << iIndex << " " << i << "\n";
            }
            iIndex = i;
        }
    }
    
    if ( iIndex == -1 ) {
        cerr << "ERR:PointConstraint::RemoveCurveGroup, could not find to remove " << in_opCG->HashId() << "\n";
        return m_data.m_aci.num();
    }

    TimeStamp();
    
    m_data.m_action = REMOVED_CURVEGROUP;
    m_data.m_aci.del(iIndex, 1);
    
    in_opCG->RemoveConstraint( this );

    if ( !IsNormal() ) {
        SetNormalConstraint();
    }
    
    m_drawData.GeometryChanged();
    
    return m_data.m_aci.num();
}

// Remove self from other curves
void PointConstraint::RemoveAllCurveGroups() 
{
    for ( int i = 0; i < m_data.m_aci.num(); i++ ) {
        m_data.m_aci[i].m_opCG->RemoveConstraint( this );
    }    
    m_data.m_aci.need(0);
    
    m_drawData.GeometryChanged();    
}
    
/**@name For finding and editing */
//@{
/// Returns 1e30 if not point constraint
bool PointConstraint::SelectPointConstraint( const R3Ray &in_ray, double &out_dDist, double &out_dZ ) const
{
    if ( !IsPoint() ) { 
        return false;
    }
    
    const R3Line line( in_ray.Pt(), UnitSafe( in_ray.Dir() ) );
    
    double dT;
    R3Pt ptOut;
    
    line.FindPtOnLine( GetPt(), ptOut, dT, out_dDist );
    
    if ( dT > 0.0 && out_dDist < g_drawState.SelectionSize() ) {
        out_dZ = g_drawState.GetCamera().CameraPt( GetPt() )[2];
        return true;
    }
    
    return false;
}

/// Returns 1e30 if not point constraint
bool PointConstraint::SelectNormalConstraint( const R3Ray &in_ray, double &out_dDist, double &out_dZ ) const
{
    if ( !IsNormal() ) { 
        return false;
    }
    if ( NumCurves() == 0 ) {
        cerr << "Err:  PointConstraint::SelectNormalConstraint, no curves\n";
        return false;
    }

	const double dScl = 4.0 * m_dSclNormal * g_drawState.StripWidth();
    
    const R3Pt ptCrv = m_data.m_aci[0].m_opCG->CompositeCurve()( m_data.m_aci[0].m_dT );
    const R3Pt pt = ptCrv + GetNormal() * dScl;
    
    const R3Line line( in_ray.Pt(), UnitSafe( in_ray.Dir() ) );

    double dT;
    R3Pt ptOut;
    
    line.FindPtOnLine( pt, ptOut, dT, out_dDist );
    
    if ( dT > 0.0 && out_dDist < g_drawState.SelectionSize() ) {
        out_dZ = g_drawState.GetCamera().ProjectedPt( ptOut )[2];
        return true;
    }

    return false;
}

void PointConstraint::DragPointConstraint( const R3Vec &in_vec, const bool in_bDelayed )
{
    if ( !IsPoint() ) {
        cerr << "ERR: PointConstraint::DragPointConstraint, editing point but not point constraint \n";
    }
    if ( RNIsZero( ::Length( in_vec ) ) ) {
        return;
    }
    
    TimeStamp();
    
    m_data.m_action = CHANGED_POINT;
    m_data.m_pt = GetPt() + in_vec;

    if ( in_bDelayed ) {
        m_bUpdated = false;
    } else {
        for ( int i = 0; i < m_data.m_aci.num(); i++ ) {
            m_data.m_aci[i].m_opCG->ReEstablishPointConstraint( *this, true );
        }
        if ( !IsNormal() ) {
            SetNormalConstraint();
        }
    }

    m_drawData.GeometryChanged();
}

void PointConstraint::EditNormal( const R3Vec &in_vecNewNorm, const bool in_bDelayed )
{
    if ( !IsNormal() ) {
        cerr << "ERR: PointConstraint::EditNormal, editing normal but not normal constraint \n";
    }
    
    if ( ApproxEqual( GetNormal(), in_vecNewNorm ) ) {
        return;
    }
    
    TimeStamp();
    
    m_data.m_action = CHANGED_NORMAL;
    m_data.m_vec = in_vecNewNorm;
    
    if ( in_bDelayed ) {
        m_bUpdated = false;
    } else {
        for ( int i = 0; i < m_data.m_aci.num(); i++ ) {
            m_data.m_aci[i].m_opCG->ReEstablishNormalConstraints( );
        }
    }
    
    m_drawData.GeometryChanged();
}

void PointConstraint::CurveMovedReprojectNormal()
{
    if ( NumCurves() != 1 ) {
        cerr << "ERR: PointConstraint::CurveMovedReprojectNormal, expected 1 curve was " << NumCurves() << "\n";
        return;
    }
    m_data.m_pt = m_data.GetPt( (int) 0);
    const R3Vec vecTang = m_data.m_aci[0].m_opCG->CompositeCurve().Tangent( m_data.m_aci[0].m_dT );
    if ( fabs( Dot( vecTang, m_data.m_vec ) ) < 0.95 ) {
        m_data.m_vec = UnitSafe( Rejection( vecTang, m_data.m_vec ) );
    }
    
    m_drawData.GeometryChanged();
}

void PointConstraint::DelayedUpdate()
{
    if ( m_bUpdated == false ) {
        ReEstablish();
        m_bUpdated = true;
        
        if ( !IsNormal() ) {
            SetNormalConstraint();
        }
        m_drawData.GeometryChanged();
    }
}

void PointConstraint::SelectedRegionChanged()
{
    m_drawData.SelectedRegionChanged();
}

void PointConstraint::PropagateUpdateFromCurves()
{
    for ( int i = 0; i < NumCurves(); i++ ) {
        if ( GetCurveGroup(i).GetCurveDrawData().NeedsColorUpdate() ) {
            m_drawData.SelectedRegionChanged();
        }
    }
}

/* Drag curves or set normals as appropriate. For use after an undo */
void PointConstraint::ReEstablish()  
{
    if ( IsPoint() ) {
        for ( int i = 0; i < m_data.m_aci.num(); i++ ) {
            m_data.m_aci[i].m_opCG->ReEstablishPointConstraint( *this, true );
        }
    }        
    if ( IsNormal() ) {
        for ( int i = 0; i < m_data.m_aci.num(); i++ ) {
            m_data.m_aci[i].m_opCG->ReEstablishNormalConstraints( );
        }
    }        
    m_bUpdated = true;
    
    m_drawData.GeometryChanged();
}

/* Rollback constraint to a previous time period. 
 * First remove any existing curve groups 
 *   If this was a restored from the graveyard constraint, this won't do anything
 * Second, find the history that was active at this time stamp and copy it into m_data
 * Third, remove any curves that no longer exist
 * Fourth, add pin back to groups
 *
 * 
 */
void PointConstraint::Rollback( const int in_iTimeStampLast, 
                                const Array< CurveGroup * > &in_aopActiveCurves,
                                const bool in_bChangePointNormal, 
                                const bool in_bDragCurves )
{
    if ( m_data.m_iTimeStamp < in_iTimeStampLast ) {
        return;
    }
    
    TimeStamp();
   
    int iRestore = -1;
    for ( int iH = m_aHistory.num()-1; iH >= 0; iH-- ) {
        if ( m_aHistory[iH].m_iTimeStamp < in_iTimeStampLast ) {
            iRestore = iH;
            break;
        }
    }
     
    if ( iRestore == -1 ) {
        cerr << "ERR: PointConstraint::Rollback, rollback time before last valid time " << in_iTimeStampLast << " " << m_data.m_iTimeStamp << "\n";    
        return;
    }
    
    const ConstraintData &cdRestore = m_aHistory[iRestore];
    const ConstraintData  cdCurrent = m_data;
    
    if ( in_bChangePointNormal ) {
        m_data.m_pt = cdRestore.m_pt;
        m_data.m_vec = cdRestore.m_vec;
        m_data.m_type = cdRestore.m_type;
    }
    
    Array<int> aiDel, aiAdd;
    /*
     * Figure out which curves to take out of the current list
     *  1) restored constraint does not have that curve
     *  2) curve is not active anymore
     *  3) not allowing dragging and curve would move a lot
     */
    const double dSelDist = g_drawState.SelectionSize() * 0.25;
    for ( int i = 0; i < cdCurrent.NumCurves(); i++ ) {
        CurveGroup *opCrvGrp = cdCurrent.m_aci[i].m_opCG;
        
        if ( !cdRestore.HasCurveGroup( *opCrvGrp ) ) {
            // Not in last time stamp - remove
            aiDel += i;
        } else if ( !in_aopActiveCurves.contains( opCrvGrp ) ) {

            // Not a valid curve - remove
            aiDel += i;
        } else if ( !in_bDragCurves && IsPoint() ) {
            // Check to see if keeping this curve would cause it to move a lot
            const R3Pt ptCrv = opCrvGrp->CompositeCurve()( cdRestore.GetTValue( *opCrvGrp ) );
            const R3Pt ptCrvNew = opCrvGrp->CompositeCurve()( cdCurrent.GetTValue( *opCrvGrp ) );
            if ( ::Length( ptCrvNew - m_data.m_pt ) > dSelDist ) {
                // Current t value takes point far away - try old one
                if ( ::Length( ptCrv - m_data.m_pt ) > dSelDist ) {
                    // Too far away - take it out
                    aiDel += i;
                } else {
                    // use old t values
                    m_data.m_aci[i].m_dT = cdRestore.GetTValue( *opCrvGrp );
                    m_data.m_aci[i].m_iPtIndex = cdRestore.GetPtIndex( *opCrvGrp );
                } 
            }
        }
    }
    /*
     * Now see if we have to add any curves
     *   1) Curve is in history, but not in current
     *   2) Curve is still valid
     *   3) not allowing dragging and curve would move a lot
     */
    for ( int i = 0; i < cdRestore.NumCurves(); i++ ) {
        CurveGroup *opCrvGrp = cdRestore.m_aci[i].m_opCG;
        if ( !cdCurrent.HasCurveGroup( *opCrvGrp ) ) {
            if ( in_aopActiveCurves.contains( opCrvGrp ) ) {
                // Curve is valid
                // Check to see if keeping this curve would cause it to move a lot
                const R3Pt ptCrv = cdRestore.GetPt( *opCrvGrp );
                const bool bIsClose = ( ::Length( ptCrv - m_data.m_pt ) < dSelDist );
                
                if ( in_bDragCurves || bIsClose || !IsPoint() ) {
                    aiAdd += i;
                }
            }
        }
    }

    m_data.m_action = UNDO;
    
    aiDel.reverse();
    for ( int j = 0; j < aiDel.num(); j++ ) {
        m_data.m_aci[ aiDel[j] ].m_opCG->RemoveConstraint( this);
        m_data.m_aci.del( aiDel[j], 1 );
    }
    
    /* Add in the old ones from the restore list */
    for ( int j = 0; j < aiAdd.num(); j++ ) {
        m_data.m_aci += cdRestore.m_aci[ aiAdd[j] ];
        m_data.m_aci.last().m_opCG->AddConstraint( this, cdRestore.GetTValue( *cdRestore.m_aci[ aiAdd[j] ].m_opCG ), false );
    }

    if ( !IsNormal() ) {
        SetNormalConstraint();
    }

    m_drawData.GeometryChanged();
}

void PointConstraint::CurveInfo::Print() const
{
    cout << "   Curve " << m_opCG->HashId() << " t " << m_dT << " id " << m_iPtIndex << "\n";
}

void PointConstraint::ConstraintData::Print() const
{
    cout << "Type: " << PointConstraint::GetTypeString( m_type );
    cout << " action: " << PointConstraint::GetActionString( m_action );
    cout << " time " << m_iTimeStamp;
    cout << " crvs " << m_aci.num() << "\n";
    
    if ( IsPoint() ) {
        cout << "  Point " << m_pt << "\n";
    }
    if ( IsNormal() ) {
        cout << "  Normal " << m_vec << "\n";
    }
    
    for ( int i = 0; i < m_aci.num(); i++ ) {
        m_aci[i].Print();
    }
}



void PointConstraint::Print() const
{
    cout << "Pt id " << m_iHashId << " ";
    m_data.Print();
}

void PointConstraint::PrintHistory() const
{
    Print();
    for ( int i = 0; i < m_aHistory.num(); i++ ) {
        cout << " " << i << ": " ;
        m_aHistory[i].Print();
    }
}

double PointConstraint::m_dSclNormal = 0.01;
double PointConstraint::m_dSclPoint = 0.01;
UTILSColor PointConstraint::m_colPtSelected = UTILSColor::YELLOW;
UTILSColor PointConstraint::m_colPtUnselected = UTILSColor::GREEN;
UTILSColor PointConstraint::m_colNormSelected = UTILSColor::YELLOW;
UTILSColor PointConstraint::m_colNormUnselected = UTILSColor::GREY;

void PointConstraint::ParamFileUpdated()
{
    m_dSclPoint = g_paramFile.GetDouble("ScalePointConstraints");
	m_colPtSelected = g_paramFile.GetColor( "ColorSelectedPointConstraint" );
	m_colPtUnselected = g_paramFile.GetColor( "ColorPointConstraints" );
	m_dSclNormal = g_paramFile.GetDouble("ScaleNormalConstraints");
	m_colNormSelected = g_paramFile.GetColor( "ColorSelectedNormalConstraint" );
	m_colNormUnselected = g_paramFile.GetColor( "ColorNormalConstraints" );
}

PointConstraint::ConstraintData::ConstraintData() 
: 
m_type( INVALID ), 
m_action(NO_ACTION),
m_pt(0,0,0), m_vec(0,0,0)
{ 
    m_iTimeStamp = CurveNetwork::CurrentTime(); 
}

static int s_iHashId = 1;

PointConstraint::PointConstraint( )
: m_drawData( *this ),
  m_iHashId( s_iHashId++ ), m_bUpdated(true)
{
    m_data.m_iTimeStamp = CurveNetwork::CurrentTime();
}

PointConstraint::~PointConstraint()
{
    for ( int i = 0; i < NumCurves(); i++ ) {
        m_data.m_aci[i].m_opCG->RemoveConstraint( this );
    }
    m_data.m_aci.clearcompletely();  
    m_aHistory.clearcompletely();
}

void PointConstraint::CheckConsistency() const
{
    cout << "Point constraint " << GetPt() << " " << HashId() << "\n";

    if ( m_data.m_aci.num() == 0 ) {
        cerr << "ERR: Point constraint with no curves\n";
    }
    
    if ( m_bUpdated == false ) {
        cerr << "ERR: PointConstraint::CheckConsistency, not updated\n";
    }

    for ( int i = 0; i < m_data.m_aci.num(); i++ ) {
        bool bHasPin = false;
        const CurveGroup &crvGrp = *m_data.m_aci[i].m_opCG;
        const Curve &crv = crvGrp.CompositeCurve();
        const double dT = m_data.m_aci[i].m_dT;
        const int iIndex = m_data.m_aci[i].m_iPtIndex;

        if ( crv.NumPts() == 0 ) {
            cerr << "ERR: PointConstraint::CheckConsistency " << HashId() << ", possibly bad curve " << crvGrp.HashId() << "\n";
        }
        for ( int j = 0; j < crvGrp.NumConstraints(); j++ ) {
            if ( crvGrp.GetConstraint(j) == *this ) {
                bHasPin = true;
            }
        }
        if ( bHasPin == false ) {
            cerr << "ERR: PointConstraint::CheckConsistency " << HashId() << ", curve does not have pin " << crvGrp.HashId() << "\n";
        }
        
        if ( iIndex < 0 || iIndex >= crv.NumPts() ) {
            cerr << "ERR: PointConstraint::CheckConsistency " << HashId() << ", curve " << crvGrp.HashId() << " does not have valid index " << iIndex << "\n";
        } else {
            const double dPerc = crvGrp.CompositeCurve().PercAlong( iIndex );
            if ( !RNApproxEqual( dPerc, dT ) ) {
                cerr << "ERR: PointConstraint::CheckConsistency " << HashId() << ", curve " << crvGrp.HashId() << " does not have valid t " << dPerc << " " << dT << "\n";
            }
            const R3Pt pt = crv(dT);
            if ( !ApproxEqual( pt, GetPt() ) ) {
                cerr << "ERR: PointConstraint::CheckConsistency " << HashId() << ", curve " << crvGrp.HashId() << " does not have matching pt " << pt << " " << GetPt() << " index " << iIndex << " " << crvGrp.CompositeCurve().Pt(iIndex) << "\n";
            }
        }
        
        if ( IsPoint() ) {
            const R3Pt pt = crv(dT);
            if ( !ApproxEqual( pt, GetPt() ) ) {
                cerr << "ERR: PointConstraint::CheckConsistency " << HashId() << ", curve " << crvGrp.HashId() << " does not have matching pt " << pt << " " << GetPt() << "\n";
            }
        }
        
        if ( IsNormal() && NumCurves() == 1 ) {
            const R3Vec vec = crvGrp.GetCurveRibbon().Normal(dT);
            const R3Vec vecTang = crv.Tangent(dT);
            const R3Vec vecReject = UnitSafe( Rejection( vecTang, GetNormal() ) );
            const double dDot = Dot( vec, vecReject );
            if ( dDot < 0.9 ) {
                cerr << "ERR: PointConstraint::CheckConsistency " << HashId() << ", curve " << crvGrp.HashId() << " does not have matching normal " << vec << " " << GetNormal() << "\n";
            }
        }
    }
}
