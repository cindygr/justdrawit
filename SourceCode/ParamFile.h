/*
 *  ParamFile.h
 *  SketchCurves
 *
 *  Created by Cindy Grimm on 2/20/2011
 *  Copyright 2011 Adobe Corp. All rights reserved.
 *
 */

#ifndef _PARAM_FILE_DEFS_H__
#define _PARAM_FILE_DEFS_H__

#include <utils/Utils_Color.H>
#include <map>
#include <vector>

/* Basically a giant hash table
 *
 * Format of text file (see ParamData.txt in ShaderData) is
 *    Name Integer/Double/Color value
 *
 * TODO: Eventually, this should set up defaults for every parameter in the program,
 * so that if ParamData.txt isn't found it still behaves ok.
 * 
 * ReadParamFile is called at initialization and whenever the button is pressed
 */
class ParamFile {
private:
    
    std::map<std::string, UTILSColor> m_aMapColors;
    std::map<std::string, double> m_aMapDoubles;
    std::map<std::string, int> m_aMapIntegers;
    
    std::vector< std::pair<std::string, bool> > m_aKeepText;
    
    void ParamsChanged();
    
public:
	// Get things back out of the hash table. Just does a cerr message if string not found...
    UTILSColor GetColor( const std::string &in_str ) ;
    double GetDouble( const std::string &in_str ) ;
    int GetInteger( const std::string &in_str ) ;
    
    // For interactively changing parameter values
    void LoadDebuggingWindow() ;
    void ChangedParamFile();
    void ChangedParamChoice();
    
	/// So that mesh viewer can grab the current parameter file and save it on a dump.
	std::string ParamFileName() const;
    void ReadParamFile();
    void WriteParamFile();
    
    ParamFile();
    ~ParamFile() {}
};

/// Global variables Bad, but in this case the easiest way to do it.
extern ParamFile g_paramFile;
#endif

