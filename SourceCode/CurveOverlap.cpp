/*
 *  CurveOverlap.cpp
 *  SketchCurves
 *
 *  Created by Cindy Grimm on 1/20/11.
 *  Copyright 2011 Adobe Corp.. All rights reserved.
 *
 */

#include "CurveOverlap.h"
#include "Curve.h"
#include "ScreenCurve.h"
#include "ParamFile.h"
#include "UserInterface.h"
#include <OpenGL/OGLObjs_Camera.H>

static char s_strCurveEnd[4][15] =
{ "No ends", "Start", "End", "Both" };

void CurveOverlap::PrintCurveEnd( const CurveEndType in_end )
{
    cout << s_strCurveEnd[ (int) (in_end+1) ];
}

bool CurveOverlap::s_bTraceJoin = false;
bool CurveOverlap::s_bTraceJoinDetails = false;
bool CurveOverlap::s_bTraceMerge = false;
bool CurveOverlap::s_bTraceOverstroke = false;
bool CurveOverlap::s_bTraceTree = false;
bool CurveOverlap::s_bTraceTs = false;
bool CurveOverlap::s_bTraceScore = false;
bool CurveOverlap::s_bTraceBlend = false;

/*
 * This mirrors TryMatch, but has a bit of a hack to see if the stroke forms a closed curve
 * Break the stroke into two pieces at the middle then check the loose ends
 * in_bForced will force the evaluation of all data (otherwise, this routine bails if
 * a closed curve is not likely because the ends are too far apart).
 *
 * Supports joins or merges at the ends
 */
bool CurveOverlap::TryClosedCurve( const ScreenCurve &in_stroke, const bool in_bForced )
{
	m_overlapType = NO_OVERLAP;
	m_bReverseStroke = false;
	
	const double dDistSel = g_drawState.ScreenSelectionSize();
	const double dLenCap = g_paramFile.GetDouble("JoinLengthCap") * dDistSel;

	// Don't bother if the ends aren't even close
	if ( ::Length( in_stroke(0.0) - in_stroke(1.0) ) > dLenCap && ! in_bForced ) {
		return false;
	}
	if ( s_bTraceTree ) cout << "TryClosedCurve: ends close enough to try \n";

	/* Split up the curve into two pieces, one of which goes in stroke, the other in projected */
	const int iNDiv = in_stroke.NumPts() / 2;
		
	m_crvStroke.Clear();
	for ( int i = 0; i < iNDiv; i++ ) {
		m_crvStroke.AddPoint( in_stroke.Pt(i), in_stroke.Time(i) );
	}

	m_crvProjected.Clear();
	for ( int i = iNDiv; i < in_stroke.NumPts(); i++ ) {
		m_crvProjected.AddPoint( in_stroke.Pt(i), in_stroke.Time(i) );
	}
	
	// Set up data that would normally be set up by the Geometry calls */
	m_adTsMap.resize(m_crvProjected.NumPts());
	for ( int i = 0; i < m_crvProjected.NumPts(); i++ ) {
		m_adTsMap[i] = m_crvProjected.PercAlong(i);
	}
	m_dDepthProj = m_dDepthMin = m_dDepthMax = 0.0;
	m_dRatioScreenLengthToCurveLength = 1.0;
	
	m_overlapType = BAD_STROKE;
	m_bCurveClosed = false;
	if ( ( m_crvStroke.NumPts() < 3 || m_crvProjected.NumPts() < 3 ) && ! in_bForced  ) {
		return false;
	}

	// Do they overlap/join? if not, return. Unless forced, in which case keep going
	if ( ProjectStrokeOnScreenCurve() == false && ! in_bForced  ) return false;

	// Set overlap and join data
    SetOverlapData( m_strokeEnd1, START_CURVE, 1.5 * dDistSel );	
    SetOverlapData( m_strokeEnd2, END_CURVE, 1.5 * dDistSel ); // Do this just so join ts is ok
	SetJoinData();
	
	// Convert t values
	ConvertOriginalCurveTValues();
    
	if ( s_bTraceTree == true ) {
		m_strokeEnd1.Print();
		m_strokeEnd2.Print();
		m_strokeJoinEnd1.Print();
		m_strokeJoinEnd2.Print();
	}

	if ( m_strokeEnd1.IsOverlapped() && m_strokeEnd1.IsMergeEnd() ) {
		if ( m_strokeEnd1.m_bStrokeReversed == false && m_strokeEnd1.m_crvEnd == END_CURVE ) {
			if ( s_bTraceTree == true ) cout << "Closed merge\n";
			m_overlapType = CLOSES;
		}
	} else if ( m_strokeJoinEnd1.IsJoined( false ) ) {
		if ( m_strokeJoinEnd1.m_bStrokeReversed == false && m_strokeJoinEnd1.m_crvEnd == END_CURVE ) {
			if ( s_bTraceTree == true ) cout << "Closed join\n";
			m_overlapType = CLOSES;
		}
	}
	if ( m_overlapType == CLOSES || in_bForced  ) {
		SetJoinInfo( END_CURVE, false );
		SetJoinTValues( );
		ConvertCurveJoinTValues( );
		
		return true;
	}
	return false;
}

/* Assumes t values have already been converted */
void CurveOverlap::SetDepthValues( const Curve &in_curve ) 
{
	m_dDepthMin = g_drawState.GetCamera().ProjectedPt( in_curve(m_dTsCurveFirstJoin.first) )[2];
	m_dDepthMax = m_dDepthMin;
	m_dDepthProj = 0.0;
	double dCount = 0.0;
	
	if ( m_dTsCurveFirstJoin.second - m_dTsCurveFirstJoin.first <= 1e-6 ) {
		m_dDepthProj = m_dDepthMin;
		return;
	}
	
	const double dTStep = ( m_dTsCurveFirstJoin.second - m_dTsCurveFirstJoin.first ) / 10.0;
	for ( double dT = m_dTsCurveFirstJoin.first; dT <= m_dTsCurveFirstJoin.second; dT += dTStep ) {
		const R3Pt ptProj = g_drawState.GetCamera().ProjectedPt( in_curve(dT) );
		m_dDepthMin = WINmin( m_dDepthMin, ptProj[2] );
		m_dDepthMax = WINmax( m_dDepthMax, ptProj[2] );
		m_dDepthProj += ptProj[2];
		dCount += 1.0;
	}
	
	if ( m_overlapType == OVERSTROKE || m_overlapType == OVERSTROKE_START || m_overlapType == CLOSES ) {
		const double dTStep = ( m_dTsCurveSecondJoin.second - m_dTsCurveSecondJoin.first ) / 10.0;
		if ( dTStep > 1e-6 ) {
			for ( double dT = m_dTsCurveSecondJoin.first; dT <= m_dTsCurveSecondJoin.second; dT += dTStep ) {
				const R3Pt ptProj = g_drawState.GetCamera().ProjectedPt( in_curve(dT) );
				m_dDepthMin = WINmin( m_dDepthMin, ptProj[2] );
				m_dDepthMax = WINmax( m_dDepthMax, ptProj[2] );
				m_dDepthProj += ptProj[2];
				dCount += 1.0;
			}		
		}
	}
	if ( !RNIsZero( dCount ) ) {
		m_dDepthProj /= dCount;
	}
}

/* 
 * Set up geometric data. If geometric data bad, bail.
 * Set up overlap and join data
 * See if we have a match/overlap/merge/etc
 * If so, convert the t values to the 3D curve, calculate depth info
 */
bool CurveOverlap::TryMatch( const Curve &in_curve, const ScreenCurve &in_stroke, const bool in_bAllowBackwardJoin )
{
	m_crvStroke = in_stroke;
	
	if ( SetGeometryData( in_curve ) == false ) {
		return false;
	}
	
	m_overlapType = DecisionTree( in_bAllowBackwardJoin );
	SetJoinTValues( );
	ConvertCurveJoinTValues( );
	
	SetDepthValues( in_curve );

	if ( m_overlapType <= 0 ) {
		return false;
	}
	
	/* Special case = if the join curve goes into the screen, bail */
	if ( m_overlapType == JOIN ) {
		const R3Plane &plane = g_drawState.DrawingPlane();
		const double dT = 0.5 * ( CurveMergeTsFirst().first + CurveMergeTsFirst().second );
		const R3Vec vecTang = in_curve.Tangent( dT );
		const double dDot = fabs( Dot( UnitSafe( plane.Normal() ), vecTang ) );
		if ( s_bTraceTree ) {
			cout << "Join angle with curve " << dDot << " ";
		}
		if ( dDot > 0.5 ) {
			if ( s_bTraceTree ) {
				cout << "Join going into the drawing plane, canceling\n";
			}
			m_overlapType = BAD_JOIN;
			return false;
		} else {
			if ( s_bTraceTree ) cout << "\n";
		}
	}
	return true;
}

/*
 * Compare two. Mostly uses scores
 */
bool CurveOverlap::IsBetterMatch( const CurveOverlap &in_oMatch, const double in_dZMin, const double in_dZMax ) const
{
	// Just in general, overstroke_start's should be unexpected
	if ( m_overlapType != OVERSTROKE_START && in_oMatch.m_overlapType == OVERSTROKE_START ) {
		return true;
	}	
	if ( m_overlapType == OVERSTROKE_START && in_oMatch.m_overlapType != OVERSTROKE_START ) {
		return false;
	}

	const double dScoreMe = Score(in_dZMin, in_dZMax);
	const double dScoreOther = in_oMatch.Score(in_dZMin, in_dZMax);
	if ( s_bTraceScore ) cout << "Not overstroke, checking scores " << dScoreMe << " " << dScoreOther << "\n";
	return dScoreMe < dScoreOther;
}

/*
 * Does the start of the stroke use a merge or a join? used to set score
 */
bool CurveOverlap::IsJoinStartStroke() const
{
	if ( m_bReverseStroke ) {
		if ( m_strokeEnd2.IsOverlapped() && m_strokeEnd2.IsMergeEnd() ) {
			return false;
		}
		if ( m_strokeJoinEnd2.m_dScore > M_PI ) {
			return false;
		}
	} else {
		if ( m_strokeEnd1.IsOverlapped() && m_strokeEnd1.IsMergeEnd() ) {
			return false;
		}
		if ( m_strokeJoinEnd1.m_dScore > M_PI ) {
			return false;
		}
	}
	return true;
}

/*
 * Does the end of the stroke use a merge or a join?used to set score
 */
bool CurveOverlap::IsJoinEndStroke() const
{
	if ( m_bReverseStroke ) {
		if ( m_strokeEnd1.IsOverlapped() && m_strokeEnd1.IsMergeEnd() ) {
			return false;
		}
		if ( m_strokeJoinEnd1.m_dScore > M_PI ) {
			return false;
		}
	} else {
		if ( m_strokeEnd2.IsOverlapped() && m_strokeEnd2.IsMergeEnd() ) {
			return false;
		}
		if ( m_strokeJoinEnd2.m_dScore > M_PI ) {
			return false;
		}
	}
	return true;
}

/*
 * Basically use the score for the merge or join of the ends.
 * Average if two ends
 */
double CurveOverlap::Score( ) const
{
	const OverlapInfo &oiE1 = ( ( m_strokeEnd == START_CURVE && m_bReverseStroke == false ) || ( m_strokeEnd == END_CURVE && m_bReverseStroke == true ) ) ? m_strokeEnd1 : m_strokeEnd2;
	const OverlapInfo &oiE2 = ( ( m_strokeEnd == START_CURVE && m_bReverseStroke == false ) || ( m_strokeEnd == END_CURVE && m_bReverseStroke == true ) ) ? m_strokeEnd2 : m_strokeEnd1;
	
	const JoinInfo &jiE1 = ( ( m_strokeEnd == START_CURVE && m_bReverseStroke == false ) || ( m_strokeEnd == END_CURVE && m_bReverseStroke == true ) ) ? m_strokeJoinEnd1 : m_strokeJoinEnd2;
	const JoinInfo &jiE2 = ( ( m_strokeEnd == START_CURVE && m_bReverseStroke == false ) || ( m_strokeEnd == END_CURVE && m_bReverseStroke == true ) ) ? m_strokeJoinEnd2 : m_strokeJoinEnd1;
	
	double dJoinScore = 0.0;
	if ( m_overlapType == MERGE ) {
		dJoinScore += oiE1.Score();
	} else if ( m_overlapType == JOIN ) {
		dJoinScore += jiE1.Score();
	} else if ( m_overlapType == OVERSTROKE || m_overlapType == OVERSTROKE_START || m_overlapType == BLEND ) {
		dJoinScore += 0.5 * (oiE1.Score() + oiE2.Score());
	} else if ( m_overlapType == CLOSES ) {
		if ( IsJoinStartStroke() ) {
			dJoinScore += 0.5 * jiE1.Score();
		} else {
			dJoinScore += 0.5 * oiE1.Score();
		}
		if ( IsJoinEndStroke() ) {
			dJoinScore += 0.5 * jiE2.Score();
		} else {
			dJoinScore += 0.5 * oiE2.Score();
		}
	} else {
		cerr << "ERR: CurveOverlap::Score, unknown type\n";
		PrintState();
		dJoinScore += 10.0;
	}
	return dJoinScore;
}

/*
 * Add z value to score. Essentially punishes things that are not parallel to the film plane
 */
double CurveOverlap::Score( const double in_dZMin, const double in_dZMax ) const
{
	double dZScore = 0.0;
	if ( !RNIsZero( in_dZMax - in_dZMin ) ) {
		dZScore += ( m_dDepthProj - in_dZMin ) / ( in_dZMax - in_dZMin );
		if ( m_overlapType == JOIN || m_overlapType == MERGE ) {
			dZScore = dZScore * 0.75 + 0.25 * ( m_dDepthMax - m_dDepthMin ) / ( in_dZMax - in_dZMin );
		}
	}
	
	const double dJoinScore = Score();

	if ( s_bTraceScore ) {
		cout << "ZScore " << dZScore << " " << dJoinScore << "\n";
	}
	
	if ( m_overlapType == JOIN ) {
		// Joins with big z differences are generally Bad 
		return 2.0 * dZScore + 0.75 * dJoinScore;
	}
	
	return 0.25 * dZScore + 0.75 * dJoinScore;
}

/*
 * Used by StrokeHistory to determine how much to smooth the join
 * Further apart, smooth more
 */
double CurveOverlap::DistanceApart( const CurveOverlap::CurveEndType in_endType ) const
{
	const OverlapInfo &oiE1 = ( ( m_strokeEnd == START_CURVE && m_bReverseStroke == false ) || ( m_strokeEnd == END_CURVE && m_bReverseStroke == true ) ) ? m_strokeEnd1 : m_strokeEnd2;
	const OverlapInfo &oiE2 = ( ( m_strokeEnd == START_CURVE && m_bReverseStroke == false ) || ( m_strokeEnd == END_CURVE && m_bReverseStroke == true ) ) ? m_strokeEnd2 : m_strokeEnd1;
	
	const JoinInfo &jiE1 = ( ( m_strokeEnd == START_CURVE && m_bReverseStroke == false ) || ( m_strokeEnd == END_CURVE && m_bReverseStroke == true ) ) ? m_strokeJoinEnd1 : m_strokeJoinEnd2;
	const JoinInfo &jiE2 = ( ( m_strokeEnd == START_CURVE && m_bReverseStroke == false ) || ( m_strokeEnd == END_CURVE && m_bReverseStroke == true ) ) ? m_strokeJoinEnd2 : m_strokeJoinEnd1;
	
	const double dSelDist = g_drawState.ScreenSelectionSize();
	if ( m_overlapType == MERGE ) {
		return oiE1.m_dAvgDist / dSelDist;
	} else if ( m_overlapType == JOIN ) {
		return jiE1.m_dDist / dSelDist;
	} else if ( m_overlapType == OVERSTROKE || m_overlapType == OVERSTROKE_START || m_overlapType == BLEND ) {
		if ( in_endType == START_CURVE ) {
			return oiE1.m_dAvgDist / dSelDist;
		} else {
			return oiE2.m_dAvgDist / dSelDist;
		}
	} else if ( m_overlapType == CLOSES ) {
		if ( IsJoinStartStroke() ) {
			return jiE1.m_dDist / dSelDist;
		} else {
			return oiE1.m_dAvgDist / dSelDist;
		}
		if ( IsJoinEndStroke() ) {
			return jiE2.m_dDist / dSelDist;
		} else {
			return oiE2.m_dAvgDist / dSelDist;
		}
	}
	return 0.0;
}

/*
 * Is the curve behind the draw plane?
 */
bool CurveOverlap::IsBehindDrawPlane( const Curve &in_crv ) const
{
	if ( g_drawState.m_opUI && !g_drawState.m_opUI->m_bShowDrawingPlane->value() ) {
		return false;
	}
	
	const ShadowBox &sb = g_drawState.GetShadowBox();
	const OGLObjsCamera &cam = g_drawState.GetCamera();
	
	R2Polygon poly(4);
	poly[0] = cam.CameraPt( sb.GetDrawingPlaneCenter() - sb.GetDrawingPlaneXVec() - sb.GetDrawingPlaneYVec() );
	poly[1] = cam.CameraPt( sb.GetDrawingPlaneCenter() + sb.GetDrawingPlaneXVec() - sb.GetDrawingPlaneYVec() );
	poly[2] = cam.CameraPt( sb.GetDrawingPlaneCenter() + sb.GetDrawingPlaneXVec() + sb.GetDrawingPlaneYVec() );
	poly[3] = cam.CameraPt( sb.GetDrawingPlaneCenter() - sb.GetDrawingPlaneXVec() + sb.GetDrawingPlaneYVec() );
	
	bool bIsBehind = false;
	
	const double dTCrv = 0.5 * ( CurveMergeTsFirst().first + CurveMergeTsFirst().second );
	const R3Pt ptCurveJoin = in_crv(dTCrv);
	const R3Pt ptOnPlane = sb.DrawingPlane().IntersectRay( R3Ray( cam.From(), ptCurveJoin - cam.From() ) );
	if ( ::Length( ptOnPlane - cam.From() ) < 0.99999 * ::Length( ptCurveJoin - cam.From() ) ) {
		if ( poly.Inside( cam.CameraPt(ptCurveJoin) ) ) {
			bIsBehind = true;
		}
	}
	
	if ( m_overlapType == CLOSES || m_overlapType == OVERSTROKE || m_overlapType == OVERSTROKE_START  || m_overlapType == BLEND ) {
		const double dTCrv2 = 0.5 * ( CurveMergeTsSecond().first + CurveMergeTsSecond().second );
		const R3Pt ptCurveJoin2 = in_crv(dTCrv2);
		const R3Pt ptOnPlane2 = sb.DrawingPlane().IntersectRay( R3Ray( cam.From(), ptCurveJoin2 - cam.From() ) );
		if ( ::Length( ptOnPlane2 - cam.From() ) < 0.99999 * ::Length( ptCurveJoin2 - cam.From() ) ) {
			if ( poly.Inside( cam.CameraPt(ptCurveJoin2) ) ) {
				bIsBehind = true;
			}
		}
	}
	return bIsBehind;
}


CurveOverlap & CurveOverlap::operator=( const CurveOverlap &in_crvOverlap )
{
	m_overlapType = in_crvOverlap.m_overlapType; 
	
	m_bCurveClosed = in_crvOverlap.m_bCurveClosed;
	m_crvStroke = in_crvOverlap.m_crvStroke;
	m_crvProjected = in_crvOverlap.m_crvProjected;
	m_adTsMap = in_crvOverlap.m_adTsMap;
	
	m_adDistToCurve = in_crvOverlap.m_adDistToCurve; 
	m_adTsOnCurve = in_crvOverlap.m_adTsOnCurve; 
	m_adTsOnOriginalCurve = in_crvOverlap.m_adTsOnOriginalCurve; 
	
	m_dTMin = in_crvOverlap.m_dTMin;
	m_dTMax = in_crvOverlap.m_dTMax;
	
	m_dDepthProj = in_crvOverlap.m_dDepthProj; 
	m_dDepthMin = in_crvOverlap.m_dDepthMin; 
	m_dDepthMax = in_crvOverlap.m_dDepthMax; 
	m_dRatioScreenLengthToCurveLength = in_crvOverlap.m_dRatioScreenLengthToCurveLength; 
	
	m_strokeEnd1 = in_crvOverlap.m_strokeEnd1;
	m_strokeEnd2 = in_crvOverlap.m_strokeEnd2;
	m_strokeJoinEnd1 = in_crvOverlap.m_strokeJoinEnd1;
	m_strokeJoinEnd2 = in_crvOverlap.m_strokeJoinEnd2;

	m_strokeEnd = in_crvOverlap.m_strokeEnd;
	m_curveEnd = in_crvOverlap.m_curveEnd;	
	m_bReverseStroke = in_crvOverlap.m_bReverseStroke; 	

	m_dTsStrokeFirstJoin = in_crvOverlap.m_dTsStrokeFirstJoin; // Bracketing t values for first join
	m_dTsCurveFirstJoin = in_crvOverlap.m_dTsCurveFirstJoin;  // Bracketing t values for first join
	m_dTsStrokeSecondJoin = in_crvOverlap.m_dTsStrokeSecondJoin; // Only needed if OVERSTROKE (second join)
	m_dTsCurveSecondJoin = in_crvOverlap.m_dTsCurveSecondJoin;  // Only needed if OVERSTROKE (second join) or MERGE_ERASE (part to erase of curve)
	
	return *this;
}

void CurveOverlap::PrintState() const
{
	cout << "Depth " << m_dDepthMin << " " << m_dDepthProj << " " << m_dDepthMax << "  ";
	switch ( m_overlapType ) {
		case BAD_STROKE : cout << " Bad stroke\n"; return;
		case BAD_CURVE : cout << " Bad curve\n"; return;
		case BAD_JOIN : cout << " Bad join\n"; return;
		case BAD_OVERLAP : cout << " Bad overlap\n"; break;
		case BAD_OVERHANG : cout << " Bad overhang\n"; break;
		case NO_OVERLAP : cout << " No overlap " << m_overlapType << "\n"; break;
		case JOIN : cout << " Join\n"; break;
		case BLEND : cout << " Blend\n"; break;
		case OVERSTROKE : cout << " Overstroke\n"; break;
		case OVERSTROKE_START : cout << " Overstroke start\n"; break;
		case MERGE : cout << " Merge \n"; break;
		case CLOSES : cout << " Closes: "; 
			if ( m_strokeEnd1.IsOverlapped() ) {
				cout << "Merge ";
			} else {
				cout << "Join ";
			}
			if ( m_strokeEnd2.IsOverlapped() ) {
				cout << "Merge ";
			} else {
				cout << "Join ";
			}
			cout << "\n"; break;
		default :
			assert(false);
	}
}
