/*
 *  CurveDrawData_Update.cpp
 *  SketchCurves
 *
 *  Created by Cindy Grimm on 9/27/11.
 *  Copyright 2011 Washington University in St. Louis. All rights reserved.
 *
 */

#include "CurveDrawData.h"
#include "DrawState.h"
#include "ParamFile.h"
#include "CurveRibbon.h"

const bool s_bTrace = false;

/*
 * Add in a color ramp around the given values
 */
void CurveDrawData::AddColorRamp( const double in_dLeft, const double in_dRight, const UTILSColor &in_col )
{
    const double dTDelta = g_drawState.SelectionSize() / GetCurve().Length();

    const double dStartRamp = GetCurve().IsClosed() ? in_dLeft - dTDelta : WINmax( in_dLeft - dTDelta, 0.0 );
    const double dEndRamp = GetCurve().IsClosed() ? in_dRight + dTDelta : WINmin( in_dRight + dTDelta, 1.0 );
    const int iStartRamp = GetCurve().FloorWrap( dStartRamp );
    const int iStart = GetCurve().FloorWrap( in_dLeft );
    const int iEnd = GetCurve().CeilWrap( in_dRight );
    const int iEndRamp = GetCurve().CeilWrap( dEndRamp );
    
    for ( int i = iStartRamp; i < iStart; i++ ) {
        const int iI = (i + GetCurve().NumPts()) % GetCurve().NumPts();
        
        const double dTAlong = GetCurve().PercAlongWrap(i);
        const double dPerc = WINminmax( ( dTAlong - dStartRamp ) / ( in_dLeft - dStartRamp ), 0.0, 1.0 );
        
        m_afCol[iI][0] = (float) ( m_afCol[iI][0] * (1.0 - dPerc) + in_col[0] * dPerc );
        m_afCol[iI][1] = (float) ( m_afCol[iI][1] * (1.0 - dPerc) + in_col[1] * dPerc );
        m_afCol[iI][2] = (float) ( m_afCol[iI][2] * (1.0 - dPerc) + in_col[2] * dPerc );
    }
    
    for ( int i = iStart; i < iEnd; i++ ) {
        const int iI = (i + GetCurve().NumPts()) % GetCurve().NumPts();

        m_afCol[iI][0] = (float) in_col[0];
        m_afCol[iI][1] = (float) in_col[1];
        m_afCol[iI][2] = (float) in_col[2];    }
    
    for ( int i = iEnd; i < iEndRamp; i++ ) {
        const int iI = (i + GetCurve().NumPts()) % GetCurve().NumPts();

        const double dTAlong = GetCurve().PercAlongWrap(i);
        const double dPerc = WINminmax( ( dEndRamp - dTAlong ) / ( dEndRamp - in_dRight ), 0.0, 1.0 );

        m_afCol[iI][0] = (float) ( m_afCol[iI][0] * (1.0 - dPerc) + in_col[0] * dPerc );
        m_afCol[iI][1] = (float) ( m_afCol[iI][1] * (1.0 - dPerc) + in_col[1] * dPerc );
        m_afCol[iI][2] = (float) ( m_afCol[iI][2] * (1.0 - dPerc) + in_col[2] * dPerc );
    }
}

/* Overall color is determined by whether the curve is active, inactive, or hidden
 * Transparency is determined by whether or not the curve is hidden
 * If the curve has a selection region, then that region is colored yellow
 * If the curve has a pinned region, then that region is colored blue
 */
void CurveDrawData::UpdateColors( const Array<double> &in_adPins ) 
{
    if ( s_bTrace ) cout << "Updating Colors\n";
    
    const UTILSColor colState = IsActive() ? s_colActive : ( IsHidden() ? s_colHidden : s_colInactive );    
	const double dTransp = IsHidden() ? s_dTransparencyHidden : 1.0;
    //const bool bIsClosed = GetCurve().IsClosed();
    
	m_afCol.resize( GetCurve().NumPts() );
    
    /* First, set all colors to be color state */
    for ( int i = 0; i < m_afCol.size(); i++ ) {
        m_afCol[i][0] = (float) colState[0];
        m_afCol[i][1] = (float) colState[1];
        m_afCol[i][2] = (float) colState[2];
		m_afCol[i][3] = (float) dTransp;
    }
    
    // Do we have a selection regin?
    if ( m_dTOnCrv.first != -1.0 && m_dTOnCrv.second != -1.0 ) {
        AddColorRamp( m_dTOnCrv.first, m_dTOnCrv.second, s_colSelect );
    }
    
    /** Comment back in if you want to color the pin points 
    for ( int i = 0; i < in_adPins.num(); i++ ) {
        const double dTDelta = 0.2 * g_drawState.SelectionSize() / GetCurve().Length();
        
        AddColorRamp( WINmax( 0.0, in_adPins[i] - dTDelta ), WINmin( 1.0, in_adPins[i] + dTDelta ), s_colPointConstraint );
    }
    */
    
    /** Comment back in if you want to color the closure points 
    if ( bIsClosed ) {
        AddColorRamp( 0.0, 0.0, s_colPointConstraint );
    }        
     */

    m_bUpdateColor = false;
}

/*
 * Add in a scale ramp around the given values
 */
void CurveDrawData::AddScaleRamp( const double in_dT )
{
    const double dTaperLength = s_dTaperPercentage * g_drawState.StripWidth();
    const double dMinScale = 0.05 * g_drawState.StripWidth();
    const double dTDelta = dTaperLength / GetCurve().Length();

    const double dStartRamp = WINmax( in_dT - dTDelta, 0.0 );
    const double dEndRamp = WINmin( in_dT + dTDelta, 1.0 );
    const int iStartRamp = GetCurve().Floor( dStartRamp );
    const int iMidRamp = RNIsZero( in_dT) ? GetCurve().Floor( in_dT ) : GetCurve().Ceil( in_dT );
    const int iEndRamp = GetCurve().Ceil( dEndRamp );
    
    for ( int i = iStartRamp; i < iMidRamp; i++ ) {
        const double dTAlong = GetCurve().PercAlong(i);
        const double dPerc = pow( WINminmax( ( dTAlong - dStartRamp ) / ( in_dT - dStartRamp), 0.0, 1.0 ), 2.0 );
        
        m_adScale[i] = (1.0 - dPerc) * m_adScale[i] + dPerc * dMinScale;
    }
    
    for ( int i = iMidRamp; i < iEndRamp; i++ ) {
        const double dTAlong = GetCurve().PercAlong(i);
        const double dPerc = pow( WINminmax( ( dEndRamp - dTAlong ) / ( dEndRamp - in_dT ), 0.0, 1.0 ), 2.0 );
        
        m_adScale[i] = (1.0 - dPerc) * m_adScale[i] + dPerc * dMinScale;
    }
}

/* Overall scale is set by a combination of the current camera zooma and the strip width in the gui
 * If the curve has a pinned region then that pinned region is scaled down
 * If the curve is closed then the ends are not taperd
 *   Otherwise, ends are tapered
 */
void CurveDrawData::UpdateScales( const Array<double> &in_adPins ) 
{
    if ( s_bTrace ) cout << "Updating Scales\n";
    
    m_dStripWidth = g_drawState.StripWidth();
    
    const double dWidth = 0.5 * m_dStripWidth;
    const bool bIsClosed = GetCurve().IsClosed();
    
	m_adScale.resize( GetCurve().NumPts() );
    
    /* First, set all scales to be color state */
    for ( int i = 0; i < m_adScale.size(); i++ ) {
        m_adScale[i] = dWidth;
    }
    
    for ( int i = 0; i < in_adPins.num(); i++ ) {
        AddScaleRamp( in_adPins[i] );
    }
    
    if ( !bIsClosed ) {
        AddScaleRamp( 0.0 );
        AddScaleRamp( 1.0 );
    }        
    
    m_bUpdateScale = false;
}


void CurveDrawData::SetShadow( const bool in_bShadow )
{
    if ( m_bCastShadow != in_bShadow ) {
        m_bUpdateShadow = true;
    }
    
    m_bCastShadow = in_bShadow;
}


// Determine if the camera has changed enough that we need to re-calc things; flag what
// needs to be re-calc'd (but don't do it yet)
void CurveDrawData::WorldStateUpdate( const bool in_bWidthSame, const bool in_bHeightSame, const bool in_bViewSame )
{
    if ( !in_bWidthSame ) {
        m_bUpdateView = true;
        m_bUpdateTube = true;
        m_bUpdateRibbon = true;
        m_bUpdateWebbing = true;
        m_bUpdateShadow = true;
    }
    
    if ( !in_bHeightSame ) {
        m_bUpdateRibbon = true;
        m_bUpdateWebbing = true;
    }

    if ( !in_bViewSame ) {
        m_bUpdateView = true;
        m_bUpdateShadow = true;
    }    
}

/* Update colors if selection region has changed */
void CurveDrawData::SetSelectionRegion( const std::pair<double,double> &in_dTOnCrv ) 
{
	const bool bUpdate = ( !RNApproxEqual( in_dTOnCrv.first, m_dTOnCrv.first ) || !RNApproxEqual( in_dTOnCrv.second, m_dTOnCrv.second ) );
	
	m_dTOnCrv = in_dTOnCrv;
    
	if ( bUpdate ) {
        m_bUpdateColor = true;
    }
}


/* Update colors if selection region has changed */
void CurveDrawData::ClearSelectionRegion(  ) 
{
	const bool bUpdate = ( !RNApproxEqual( -1.0, m_dTOnCrv.first ) || !RNApproxEqual( -1.0, m_dTOnCrv.second ) );
	
	m_dTOnCrv.first = -1.0;
	m_dTOnCrv.second = -1.0;
    
	if ( bUpdate ) {
        m_bUpdateColor = true;
    }
}

void CurveDrawData::GeometryChanged()
{
    if ( m_afCol.size() != GetCurve().NumPts() ) {
        m_bUpdateColor = true;
    }
    
    if ( m_adScale.size() != GetCurve().NumPts() ) {
        m_bUpdateScale = true;
    }
    
    m_bUpdateWebbingSplits = true;
    m_bUpdateView = true;
    m_bUpdateTube = true;
    m_bUpdateRibbon = true;
    m_bUpdateWebbing = true;
    m_bUpdateShadow = true;
}


/* Update everything */
void CurveDrawData::Update( const Array<double> &in_adPins ) 
{
    WorldStateChanged();
    
    if ( m_bUpdateColor ) {
        UpdateColors( in_adPins );
    }
    if ( m_bUpdateScale ) {
        UpdateScales( in_adPins );
        m_bUpdateView = true;
        m_bUpdateTube = true;
        m_bUpdateRibbon = true;
        m_bUpdateWebbing = true;
        m_bUpdateShadow = true;        
    }
    if ( m_bUpdateWebbingSplits && g_drawState.m_drawingStyle == DrawState::WEBBING ) {
        SetWebbingSplits( in_adPins ); // this needs to come after scales and before update geometry
    }
    
	UpdateGeometry( );
}


