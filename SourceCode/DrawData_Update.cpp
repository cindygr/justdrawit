/*
 *  DrawData_Update.cpp
 *  SketchCurves
 *
 *  Created by Cindy Grimm on 6/29/12.
 *  Copyright 2012 Washington University in St. Louis. All rights reserved.
 *
 */

#include "DrawData.h"
#include "DrawState.h"
#include "ParamFile.h"

const bool s_bTrace = false;


void DrawData::SetHidden( const bool in_bHidden )
{
    if ( m_bHidden != in_bHidden ) {
        m_bUpdateColor = true;
    }
    
    m_bHidden = in_bHidden;
}

void DrawData::ToggleHide(  )
{
    m_bUpdateColor = true;
    
    m_bHidden = !m_bHidden;
}

void DrawData::SetActive( const bool in_bActive )
{
    if ( m_bActive != in_bActive ) {
        m_bUpdateColor = true;
    }
    
    m_bActive = in_bActive;
}

void DrawData::SetShadow( const bool in_bShadow )
{
    m_bCastShadow = in_bShadow;
}


// Determine if the camera has changed enough that we need to re-calc things; flag what
// needs to be re-calc'd (but don't do it yet)
void DrawData::WorldStateChanged(  )
{
	const double dStripWidth = g_drawState.StripWidth();
	const double dStripHeight = g_drawState.StripHeight();
	
    const R3Vec vecViewPlane = g_drawState.m_vecView;
	const R3Vec vecViewUp = g_drawState.m_vecUp;
    
    const bool bViewSame = ApproxEqual( m_vecView, vecViewPlane, 0.0001 ) && ApproxEqual( m_vecUp, vecViewUp, 0.0001 );
    const bool bSizeWidthSame = fabs( m_dStripWidth - dStripWidth ) / ( m_dStripWidth + dStripWidth ) < 0.01;
    const bool bSizeHeightSame = fabs( m_dStripHeight - dStripHeight ) / ( m_dStripHeight + dStripHeight ) < 0.01;
    
    WorldStateUpdate( bSizeWidthSame, bSizeHeightSame, bViewSame );
    
    if ( !bSizeWidthSame ) {
        m_bUpdateScale = true;
        
        m_dStripWidth = g_drawState.StripWidth(); // force update of everything
        
        if ( s_bTrace ) cout << "Size changed\n";
    }
    
    if ( !bSizeHeightSame ) {
        
        m_dStripHeight = g_drawState.StripHeight(); 
        if ( s_bTrace ) cout << "Height changed\n";
    }
    
    if ( !bViewSame ) {
        
        m_vecView = g_drawState.m_vecView;
        m_vecUp = g_drawState.m_vecUp;
        if ( s_bTrace ) cout << "View changed\n";
    }    
}



