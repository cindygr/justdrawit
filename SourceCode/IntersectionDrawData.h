/*
 *  IntersectionDrawData.h
 *  SketchCurves
 *
 *  Created by Cindy Grimm on 6/30/12.
 *  Copyright 2012 Washington University in St. Louis. All rights reserved.
 *
 */

#ifndef _INTERSECTION_DRAW_DATA_H
#define _INTERSECTION_DRAW_DATA_H

#include "DrawData.h"
#include "CurveRibbon.h"
#include "CurveDrawData.h"

class PointConstraint;
/*
 * Calculate, and store, the geometry used to make the webbing rendering
 * of the curve junction
 *
 * Lazy evaluation; data only changes on one of the Update calls. The geometry
 * only changes if the curve itself (or its normals) changes. The color
 * may change based on state change (hidden to not, active to not)
 *
 * Basically a star-shaped grid that joins to the tubes on either side
 *
 * DrawData reads and stores the param data file information, so if that
 * file is re-read, need to update the local params here (and recalc the curves)
 */
class IntersectionDrawData : public DrawData
{
private:
    class SpokeData {
    public:
        int m_iCurve;
        int m_iStart, m_iMid, m_iPin;
        bool m_bReverseDir;
        double m_dTheta, m_dRadius;
        
        void InsureOrdered( const int in_iNPts );
        int NumPts() const;
        int NumWeb() const { return abs( m_iPin - m_iMid ); }
        
        SpokeData() {}
        ~SpokeData() {}
    };
        
    // The pin
    const PointConstraint &m_pin;
    
    // Where the spokes start and end
    std::vector< SpokeData > m_aSpokeData;
    
	/* Store colors, t values for texture mapping, and scale values.
     * Only updated when Update called
     * Changed by:
     *  State change (hidden/active): changes color
     *  Point constraints: Changes color and scale (pinches in at pin)
     *  Camera zoom factor: Changes scale
     *  Strip width in GUI: Changes scale
     *  Geometry changes: Changes t values
     */
    std::vector< std::vector<R4Pt_f> > m_aafColSpoke, m_aafColWebbing; 
	
	/* Geometry-specific data 
     * This is not kept up to date; only updated when drawing type is active
     * Changed by:
     *   Point constraints: Need to create/delete
     *   Camera orientation: Changes shadows
     *   Strip width in GUI: Changes scale for everyone
     *   Geometry changes: Changes all geometry for everyone
     */
    std::vector< std::vector< R3Pt > > m_aaptTopPolygon, m_aaptBotPolygon; // Webbing polygon
    std::vector< std::vector< R4Pt_i > > m_aafPolygonFaces; // Webbing polygon
    std::vector< std::vector< R3Vec > > m_aavecTopPolygonNorm, m_aavecBotPolygonNorm; // 2D view-facing strip
	std::vector< std::vector< R3Pt > > m_aaaptShadowPolygon[3]; // Shadow bulge

    std::vector< std::vector< std::vector< R3Pt > > > m_aaaptSpokes; // One for each spoke
    std::vector< std::vector< std::vector< R3Vec > > > m_aaavecSpokesNorm; // 2D view-facing strip

    bool m_bUpdateSpokeData, m_bUpdateWebbing, m_bUpdateShadow;
    
	// Just the colors
	void UpdateColors( );
	/// Camera/gui may have changed - update curve's screen space vectors. May update all if width/zoom has changed
    void WorldStateUpdate( const bool in_bWidthSame, const bool in_bHeightSame, const bool in_bViewSame );
	// The geometry
    int NumSpokes() { return m_aSpokeData.size(); }
    
    void UpdateShadow();
    pair<int,double> NumSamplesWeb() const;
    
    void SetSpokeData();
    void SetSpokeRadius();
    void SetSpoke( const int in_iS, const int in_iNSamples );
    void SetWebbing( const pair<int,double> in_iNSamples );
	void UpdateGeometry( );
    
    /* Just spit out geometry, some of which is stored in m_drawData */
    void DrawPointsSpokes( ) const;
	void DrawLineSpokes( ) const;
    void DrawPointsWebbing( ) const;
	void DrawLineWebbing( ) const;
    void DrawShadow(  const R3Plane &in_plane ) const;
    void DrawSpokes( ) const;
	void DrawWebbing( ) const;
	
    IntersectionDrawData &operator=( const IntersectionDrawData &in_data );
    IntersectionDrawData( const IntersectionDrawData & );
	
public:
	/**@name Might need to update */
	//@{
    /// Just set shadow state
    void SetShadow( const bool in_bShadow );
    ///
    void SelectedRegionChanged() { m_bUpdateColor = true; }
    /// Sets geometry
    void GeometryChanged();
    ///
    void ForceUpdate();
    
	/// Call just before drawing to make sure everything is up to date
    void Update();
	//@}
    
    
	/**@name Drawing routines. In CurveDrawData_Draw */
	//@{
	/// Draw with given style in g_drawState
    void Draw( ) const;
	/// Draw as a shadow (projected and black)
    void DrawShadow( ) const;
	//@}
    
	IntersectionDrawData( const PointConstraint & );
	~IntersectionDrawData() {}
};

#endif