/*
 *  CurveNetwork.cpp
 *  SketchCurves
 *
 *  Created by Cindy Grimm on 9/16/10.
 *  Copyright 2010 Washington University in St. Louis. All rights reserved.
 *
 */

#include "CurveNetwork.h"
#include "CurveGroupManager.h"
#include "PointConstraint.h"
#include "DrawState.h"
#include <OpenGL/OGLObjs_Camera.H>

const bool s_bTrace = false;

int CurveNetwork::m_iCurrentTime = 1;


/* Search through list to find curve group */
int CurveNetwork::FindIndexOfCurveGroup( const CurveGroup &in_grp ) const
{
    for ( int i = 0; i < NumCrvs(); i++ ) {
        if ( in_grp == GetCurveGroup(i) ) {
            return i;
        }
    }
    
    cerr << "ERR: CurveNetwork::IndexOfCurveGroup, crv group not found " << in_grp.HashId() << "\n";
    return 0;
}

/* Use the center of the bounding box, not the centroid */
const R3Pt CurveNetwork::Center() const
{
	const std::pair<R3Pt,R3Pt> bbox = BoundingBox();
	return Lerp( bbox.first, bbox.second, 0.5 );
}

/* Bounding box */
const std::pair<R3Pt,R3Pt> CurveNetwork::BoundingBox() const
{
	R3Pt ptLL(1e30,1e30,1e30), ptUR(-1e30,-1e30,-1e30);
	
	if ( NumCrvs() == 0 ) {
		return std::pair<R3Pt,R3Pt>( R3Pt(0,0,0), R3Pt(0,0,0) );
	}
	
	for ( int i = 0; i < NumCrvs(); i++ ) {
		const std::pair<R3Pt,R3Pt> bbox = GetCurve(i).BoundingBox();
		
		for ( int j = 0; j < 3; j++ ) {
			ptLL[j] = WINmin( ptLL[j], bbox.first[j] );
			ptUR[j] = WINmax( ptUR[j], bbox.second[j] );
		}
	}
	return std::pair<R3Pt,R3Pt>( ptLL, ptUR );
}

bool CurveNetwork::IsHidden( const int in_i ) const 
{
	if ( in_i < 0 || in_i >= NumCrvs() ) {
        cerr << "CurveNetwork::IsHidden, accessing invalid curve " << in_i << " " << NumCrvs() << "\n";
		return true;
	}
    
    return GetCurveGroup(in_i).IsHidden();
}

/* Should we draw the smoothing circles? */
bool CurveNetwork::IsLastActionSmooth(  ) const
{
	if ( m_iLastEdited < 0 || m_iLastEdited >= NumCrvs() ) {
		return false;
	}
	
	const CurveGroup &crvGrp = GetCurveGroup(m_iLastEdited);
	if ( crvGrp.NumStrokes() == 0 ) {
		return false;
	}
	
	return crvGrp.LastStrokeAction() == StrokeHistory::SMOOTH;
}

/* For setting curve color */
bool CurveNetwork::IsLastEditedCurve( const int in_i ) const 
{
	return m_iLastEdited == in_i;
}

const CurveGroup &CurveNetwork::GetLastEditedCurveGroup( ) const 
{ 
	if ( m_iLastEdited < 0 || m_iLastEdited >= NumCrvs() ) {
		cerr << "ERR: Last edited curve group, no valid curve!\n";
		return g_CrvGrpManager.InvalidCurveGroup();
	}
	return GetCurveGroup( m_iLastEdited ); 
}


/* Toggle if the curve is reflected or not. */
void CurveNetwork::ReflectCurve( const int in_i, const R3Plane &in_plane )
{
	if ( in_i < 0 || in_i >= NumCrvs() ) {
        cerr << "CurveNetwork::ReflectCurve, accessing invalid curve " << in_i << " " << NumCrvs() << "\n";
		return;
	}
	
    SetCurveGroup(in_i).Reflect( in_plane );
}

void CurveNetwork::UnreflectCurve( const int in_i )
{
	if ( in_i < 0 || in_i >= NumCrvs() ) {
        cerr << "CurveNetwork::UnreflectCurve, accessing invalid curve " << in_i << " " << NumCrvs() << "\n";
		return;
	}
	
    SetCurveGroup(in_i).Unreflect( );
}

/* Toggle hiding/showing the curve */
void CurveNetwork::ToggleHideCurve( const int in_i )
{
	if ( in_i < 0 || in_i >= NumCrvs() ) {
        cerr << "CurveNetwork::ToggleHideCurve, accessing invalid curve " << in_i << " " << NumCrvs() << "\n";
		return;
	}
	
    SetCurveGroup(in_i).ToggleHide();
	
	if ( !SetCurveGroup(in_i).IsHidden() ) {
		m_iLastEdited = in_i;
	}
}

/* Closest point search. Use z values if the curves are close. Returns distance of closest curve */
double CurveNetwork::ClosestCurve( const R3Ray &in_ray, const bool in_bSkipHidden, int &out_iWhich, double &out_dT ) const
{
	double dClosest = 1e30;
	out_iWhich = -1;
	out_dT = -1.0;
	
	double dDist, dT;
	double dZBest = 1e30;
	R3Pt ptOut;
	
	const OGLObjsCamera &cam = g_drawState.GetCamera();
	const double dSelDist = g_drawState.SelectionSize();
	for ( int i = 0; i < NumCrvs(); i++) {
		if ( in_bSkipHidden && IsHidden(i) ) {
			continue;
		}
		GetCurve(i).ClosestPoint( in_ray, dT, dDist );
		bool bIsBetter = out_iWhich == -1 ? true : false;
		double dZ = cam.ProjectedPt( GetCurve(i)( dT ) )[2];
		
		if ( dDist <= dSelDist && dClosest <= dSelDist ) {
			if ( dZ < dZBest ) {
				bIsBetter = true;
			}
		} else if ( dDist <= dSelDist && dClosest > dSelDist ) {
			bIsBetter = true;
		} else if ( dDist > dSelDist && dClosest <= dSelDist ) {
			bIsBetter = false;
		} else {
			bIsBetter = dDist < dClosest;
		}
		
		if ( bIsBetter ) {
			out_iWhich = i;
			dClosest = dDist;
			out_dT = dT;
			dZBest = dZ;
		}
	}   
	
	return dClosest;
}

/* Wipe out stroke histories */
void CurveNetwork::FinalizeAll()
{
	for ( int i = 0; i < NumCrvs(); i++ ) {
		SetCurveGroup(i).Finalize();
	}
	m_iCurveLastStroke = -1;
}


/* Update colors in CurveDataDraw */
void CurveNetwork::SetSelectionRegion( const int in_i, const std::pair<double,double> &in_adTs )
{
	if ( in_i < 0 || in_i >= NumCrvs() ) {
        cerr << "CurveNetwork::SetSelectionRegion, accessing invalid curve " << in_i << " " << NumCrvs() << "\n";
		return;
	}
	
    SetCurveGroup(in_i).SetSelectionRegion( in_adTs );
}

/* Update colors in CurveDataDraw */
void CurveNetwork::ClearSelectionRegion( const int in_i )
{
	if ( in_i < 0 || in_i >= NumCrvs() ) {
        cerr << "CurveNetwork::ClearSelectionRegion, accessing invalid curve " << in_i << " " << NumCrvs() << "\n";
		return;
	}
    
    SetCurveGroup(in_i).ClearSelectionRegion(  );
}

/* Update colors in CurveDataDraw */
void CurveNetwork::UpdateCurveState( const int in_iCursorOverCurve )
{
	for ( int i = 0; i < NumCrvs(); i++ ) {
        SetCurveGroup(i).SetActive( IsLastEditedCurve(i) || i == in_iCursorOverCurve );
	}
}

void CurveNetwork::ParamDataChanged()
{
    for ( int i = 0; i < NumCrvs(); i++ ) {
        SetCurveGroup(i).ForceUpdateDrawData();
    }
    for ( int i = 0; i < m_aopPins.num(); i++ ) {
        m_aopPins[i]->ForceUpdateDrawData();
    }
}

const bool CurveNetwork::s_bTraceUndo = false;
const bool CurveNetwork::s_bTraceErase = false;
const bool CurveNetwork::s_bTraceCombine = false;
const bool CurveNetwork::s_bTraceAddingConstraints = false;
const bool CurveNetwork::s_bTraceProcessStroke = false;
const bool CurveNetwork::s_bTracePropagate = false;
const bool CurveNetwork::s_bTraceDrag = false;


CurveNetwork::CurveNetwork()
	: m_iCurveLastStroke(-1)
{
	m_iSaveLastEdited = -1;
	m_iLastEdited = -1;
}

void CurveNetwork::Write( ofstream &out ) const
{
    out << "CurveNetwork " << NumCrvs() << "\n";
    int iMaxId = 0;
    for ( int i = 0; i < NumCrvs(); i++ ) {
        iMaxId = WINmax( iMaxId, GetCurveGroup(i).HashId() );
    }
    
    Array<int> aiMapIds( iMaxId +1 );
    aiMapIds.fill( -1 );
    for ( int i = 0; i < NumCrvs(); i++ ) {
        GetCurveGroup(i).Write(out, i);
        aiMapIds[ GetCurveGroup(i).HashId() ] = i;
    }
    out << "Constraints " << NumConstraints() << "\n";
    for ( int i = 0; i < NumConstraints(); i++ ) {
        GetConstraint(i).Write( out, aiMapIds );
    }
    
}

void CurveNetwork::WriteSimple( ofstream &out ) const
{
    out << "# Num curves\n";
    out << "# For each curve: Num points\n";
    out << "# For each point: index, t value, point, normal\n";
    out << "# Constraints\n";
    out << "# For each constraint: Num curves, point, normal\n";
    out << "# For each constrained curve: curve #, point index, t value\n";
    
    out << NumCrvs() << "\n";

    Array<int> aiMap( g_CrvGrpManager.NextId() + 1 );
    for ( int i = 0; i < NumCrvs(); i++ ) {
        GetCurveGroup(i).WriteSimple(out);
        aiMap[ GetCurveGroup(i).HashId() ] = i;
    }
    
    out << NumConstraints() << "\n";
    for ( int i = 0; i < NumConstraints(); i++ ) {
        out << GetConstraint(i).NumCurves() << " " << GetConstraint(i).GetPt() << " " << GetConstraint(i).GetNormal() << "\n";
        for ( int j = 0; j < GetConstraint(i).NumCurves(); j++ ) {
            out << " " << aiMap[ GetConstraint(i).GetCurveGroup(j).HashId() ] << " ";
            out << GetConstraint(i).GetPtIndex(j) << " ";
            out << GetConstraint(i).GetTValue(j) << "\n";
        }
    }
}

void CurveNetwork::WriteSimple( const string &in_str ) const
{
    ofstream out( in_str.c_str(), ios::out );
    if ( out.good() ) {
        WriteSimple( out );
    } else {
        cerr << "ERR: CurveNetwork::WriteSimple, unable to write " << in_str << "\n";
    }
}

void CurveNetwork::WriteMaya( const string &in_str ) const
{
    ofstream out( in_str.c_str(), ios::out );
    if ( !out.good() ) {
        cerr << "ERR: CurveNetwork::WriteMaya, File not good " << in_str << "\n";
        return;
    }
    
    out << "//Maya ASCII 2010 scene\n";
    out << "//Codeset: 1252\n";
    out << "requires maya \"2010\";\n";
    out << "requires \"stereoCamera\" \"10.0\";\n";
    out << "currentUnit -l centimeter -a degree -t film;\n";
    out << "fileInfo \"application\" \"maya\";\n";
    out << "fileInfo \"product\" \"Maya Unlimited 2010\";\n";
    out << "fileInfo \"version\" \"2010 x64\";\n";
    out << "fileInfo \"cutIdentifier\" \"200907280308-756013\";\n";
    out << "fileInfo \"osv\" \"Microsoft Windows Vista  (Build 7600)\\n\";\n";
    
    for ( int i = 0; i < NumCrvs(); i++ ) {
        const CurveRibbon &crv = GetCurveRibbon(i);
        if ( crv.NumPts() == 0 ) {
            continue;
        }
        
        out << "createNode transform -n \"curve" << (i+1) << "\"\n";
        out << "createNode nurbsCurve -n \"curveShape" << (i+1) << "\" -p \"curve" << (i+1) << "\"\n";
        out << "    setAttr -k off \".v\"\n";
        out << "    setAttr \".cc\" -type \"nurbsCurve\"\n"; 
        out << "    1 " << crv.NumPts()-1 << " 0 no 3\n";
        out << "    " << crv.NumPts() << " ";
        for ( int j = 0; j < crv.NumPts(); j++ ) {
            out << j << " ";
        }
        out << "\n";
        out << "    " << crv.NumPts();
        
        for ( int j = 0; j < crv.NumPts(); j++ ) {
            out << "\n  " << crv.Pt(j);
        }
        out << ";\n";        
    }    
    out.close();
}

void CurveNetwork::WriteAdobe( const string &in_str ) const
{
    ofstream out( in_str.c_str(), ios::out );
    if ( !out.good() ) {
        cerr << "ERR: CurveNetwork::WriteMaya, File not good " << in_str << "\n";
        return;
    }
    
    out.close();
}

void CurveNetwork::WriteCurveTubeGeometry( const string &in_str )
{
    string strNoEnd = in_str;
    if ( strNoEnd.find_last_of('.') != string::npos ) {
        strNoEnd.erase( in_str.find_last_of('.'), string::npos );
    }
    ofstream outOff( ( strNoEnd + ".off" ).c_str(), ios::out );
    ofstream outObj( ( strNoEnd + ".obj" ).c_str(), ios::out );
    ofstream outMtl( ( strNoEnd + ".mtl" ).c_str(), ios::out );
    if ( !outOff.good() ) {
        cerr << "ERR: ActionEvents::WriteCurvesAsOffFile, couldn't write file " << in_str << "\n";
        outOff.close();
        outObj.close();
        outMtl.close();
        return;
    }
    
    // First make sure the tube geometry exists
    const DrawState::DrawingStyle iSaveDrawState = g_drawState.m_drawingStyle;
    g_drawState.m_drawingStyle = DrawState::TUBES;
    
    UpdateDrawData();
    g_drawState.m_drawingStyle = iSaveDrawState;
    
    std::vector<R3Pt> aptVs;
    std::vector<R3Vec> avecVNs;
    std::vector< R3Pt_i > aptFs;
    std::vector<int> aiBreaks;
    
    // Collect points/faces
    string strFileName = strNoEnd;
    if ( strFileName.find_last_of('/') != string::npos ) {
        strFileName.erase( 0, strFileName.find_last_of('/') + 1 );
    }
    
    outObj << "mtllib " << strFileName << ".mtl\n\n";
    for ( int iC = 0; iC < NumCrvs(); iC++ ) {
        const int iNStart = aptVs.size();
        const int iNFStart = aptFs.size();
        SetCurveGroup(iC).TubeGeometry( aptVs, avecVNs, aptFs );
        outObj << "g Crv" << iC << "\n";
        outObj << "usemtl CrvMtl" << iC << "\n";
        for ( int i = iNStart; i < aptVs.size(); i++ ) {
            outObj << "v " << aptVs[i] << "\n";
        }
        for ( int i = iNStart; i < aptVs.size(); i++ ) {
            outObj << "vn " << avecVNs[i] << "\n";
        }
        for ( int i = iNFStart; i < aptFs.size(); i++ ) {
            outObj << "f ";
            for ( int j = 0; j < 3; j++ ) {
                const int iVI = aptFs[i][j] + 1;// - iNStart + 1;
                outObj << iVI << "//" << iVI << " ";
            }
            outObj << "\n";
        }
        outObj << "\n";
        
        
        outMtl << "newmtl CrvMtl" << iC << "\n";
        outMtl << "illum 2\n";
        outMtl << "Kd 0 1 1\n";
        outMtl << "Ka 0.1 0.1 0.1\n";
        outMtl << "Ks 0.75 0.75 0.75\n\n";
    }
    
    for ( int iP = 0; iP < NumConstraints(); iP++ ) {
        if ( m_aopPins[iP]->IsPoint() ) {
            
            const int iNStart = aptVs.size();
            const int iNFStart = aptFs.size();
            m_aopPins[iP]->SphereGeometry( aptVs, avecVNs, aptFs );
            outObj << "g Pin" << iP << "\n";
            outObj << "usemtl PinMtl" << iP << "\n";
            for ( int i = iNStart; i < aptVs.size(); i++ ) {
                outObj << "v " << aptVs[i] << "\n";
            }
            for ( int i = iNStart; i < aptVs.size(); i++ ) {
                outObj << "vn " << avecVNs[i] << "\n";
            }
            for ( int i = iNFStart; i < aptFs.size(); i++ ) {
                outObj << "f ";
                for ( int j = 0; j < 3; j++ ) {
                    const int iVI = aptFs[i][j] + 1;// - iNStart + 1;
                    outObj << iVI << "//" << iVI << " ";
                }
                outObj << "\n";
            }
            outObj << "\n";
            
            
            outMtl << "newmtl PinMtl" << iP << "\n";
            outMtl << "illum 2\n";
            outMtl << "Kd 1 0 0\n";
            outMtl << "Ka 0.1 0.1 0.1\n";
            outMtl << "Ks 0.75 0.75 0.75\n\n";
        }
    }

    outOff << "OFF\n";
    outOff << aptVs.size() << " " << aptFs.size() << " " << 3 * aptFs.size() / 2 << "\n";
    for ( int i = 0; i < aptVs.size(); i++ ) {
        outOff << aptVs[i] << "\n";
    }
    for ( int i = 0; i < aptFs.size(); i++ ) {
        outOff << "3 " << aptFs[i] << "\n";
    }
    
    outObj.close();
    outMtl.close();
    outOff.close();
    
}

void CurveNetwork::Read( const string &in_str )
{
    ifstream in( in_str.c_str(), ios::in );
    if ( in.good() ) {
        Read( in );
    } else {
        cerr << "ERR: CurveNetwork::Read, unable to read " << in_str << "\n";
    }
}    

void CurveNetwork::Read( ifstream &in )
{
    std::string str;
    in >> str; 
    if ( str.compare( "CurveNetwork" ) ) {
        cerr << "ERR: CurveNetwork::Read, not a curve network\n";
        return;
    }
    
	Clear();
	
    int iN;
    in >> iN;
    m_aopCrv.need(iN);
    for ( int i = 0; i < iN; i++ ) {
        m_aopCrv[i] = g_CrvGrpManager.CreateCurveGroup();
        
        m_aopCrv[i]->Read(in);
    }

    in >> str; ASSERT( !str.compare( "Constraints" ) );
    in >> iN;
    m_aopPins.need(iN);
    for ( int i = 0; i < iN; i++ ) {
        m_aopPins[i] = new PointConstraint;
        m_aopPins[i]->Read( in, m_aopCrv );
    }

    /*
    for ( int iC = 0; iC < NumCrvs(); iC++ ) {
        for ( int i = 0; i < GetCurveGroup(iC).NumConstraints(); i++ ) {
            if ( m_aopCrv[iC]->GetConstraint(i).IsPoint() ) {
                m_aopCrv[iC]->ReEstablishPointConstraint( m_aopCrv[iC]->GetConstraint(i) );
            }
        }
    }
     */
    
    CheckConsistency();
}

void CurveNetwork::ReadKaran( ifstream &in )
{
	Clear();
	
    char str[500];
    for ( int i = 0; i < 11; i++ ) {
        in.getline( str, 500 );
    }

    while ( !in.eof() ) {
        in.getline( str, 500 );
        cout << str << "\n";
        if ( std::string::npos != std::string( str ).find("rebuildCurve") ) {
            break;
        }
        
        m_aopCrv += g_CrvGrpManager.CreateCurveGroup();
        m_aopCrv.last()->ReadKaran(in);
        ASSERT( in.good() );
    }
    
    CenterAndScale();
    //Resample(); // rebuild resampled curves
}

void CurveNetwork::ReadSimple( ifstream &in )
{
	Clear();
    
    char str[1024];
    if ( in.peek() == '#' ) {
        in.getline( str, 1024, '\n' );
    }
    int iNCrvs;
    in >> iNCrvs;
    for ( int i = 0; i < iNCrvs; i++ ) {
        m_aopCrv += g_CrvGrpManager.CreateCurveGroup();
        m_aopCrv.last()->ReadSimple(in);
    }
    
    int iNConstraints = 0;
    in >> iNConstraints;
    if ( !in.good() || iNConstraints == 0 ) {
        return;
    }
    
    R3Pt pt;
    R3Vec vecNorm;
    int iCrvId, iPtIndex;
    double dTValue;
    for ( int i = 0; i < iNConstraints; i++ ) {
        in >> iNCrvs >> pt >> vecNorm;
        
        PointConstraint *opPin = new PointConstraint;
        opPin->SetPointAndNormalConstraint( pt, vecNorm );
        
        for ( int j = 0; j < iNCrvs; j++ ) {
            in >> iCrvId >> iPtIndex >> dTValue;
            if ( iCrvId >= 0 && iCrvId < m_aopCrv.num() ) {
                opPin->AddCurveGroup( m_aopCrv[iCrvId], iPtIndex, dTValue );
            }
        }
        m_aopPins += opPin;
    }
}

static int CurveExists( const CurveNetwork &in_crvNW, const Curve &in_crv, bool &out_bSwap )
{
    out_bSwap = false;
    
    for ( int i = 0; i < in_crvNW.NumCrvs(); i++ ) {
        const Curve &crv = in_crvNW.GetCurve(i);
        const double dLen1 = crv.Length();
        const double dLen2 = in_crv.Length();
        if ( fabs( dLen1 - dLen2 ) > 0.05 * WINmin( dLen1, dLen2 ) ) {
            continue;
        }
        
        const double dDistCenter = ::Length( crv.CenterPoint() - in_crv.CenterPoint() );
        if ( dDistCenter > in_crv.Length() * 0.1 ) {
            continue;
        }
        const double dLenEnd1 = WINmin( ::Length( in_crv(0.0) - crv(0.0) ), ::Length( in_crv(0.0) - crv(1.0) ) );
        const double dLenEnd2 = WINmin( ::Length( in_crv(1.0) - crv(0.0) ), ::Length( in_crv(1.0) - crv(1.0) ) );
        if ( dLenEnd1 > in_crv.Length() * 0.01 || dLenEnd2 > in_crv.Length() * 0.01 ) {
            continue;
        }
        if ( ::Length( in_crv(0.0) - crv(0.0) ) < ::Length( in_crv(1.0) - crv(0.0) ) ) {
            out_bSwap = false;
        } else {
            out_bSwap = true;
        }
        return i;
    }
    return -1;
}

void CurveNetwork::ReadPatchMaker( ifstream &in, Array< Array<int> > &out_aaiPatch, Array< Array<bool> > &out_aabReversePatch )
{
	Clear();
    
    char str[1024];
    in.getline( str, 1024, '\n' );
    int iNPatches = 0, iN, iNCurves;
    R3Pt pt;
    in >> str >> iNPatches; in >> str;
    Curve crv;
    bool bSwap;
    out_aaiPatch.need( iNPatches );
    out_aabReversePatch.need( iNPatches );
    Array<double> adEnd1, adEnd2;
    
    for ( int iP = 0; iP < iNPatches; iP++ ) {
        in >> str >> iN >> str >> iNCurves;

        out_aaiPatch[iP].need( iNCurves );
        out_aabReversePatch[iP].need( iNCurves );
        adEnd1.need( iNCurves );
        adEnd2.need( iNCurves );
        for ( int iC = 0; iC < iNCurves; iC++ ) {
            in >> str >> iN >> str;

            crv.ReadPatchMaker(in);
            const int iMatch = CurveExists( *this, crv, bSwap );
            if ( iMatch == -1 ) {
                m_aopCrv += g_CrvGrpManager.CreateCurveGroup(  );
                m_aopCrv.last()->NewCompositeCurve( crv );            
                out_aaiPatch[iP][iC] = m_aopCrv.num() - 1;
                out_aabReversePatch[iP][iC] = false;
                adEnd1[iC] = 0.0;
                adEnd2[iC] = 1.0;
            } else {
                out_aaiPatch[iP][iC] = iMatch;
                out_aabReversePatch[iP][iC] = bSwap;
                if ( bSwap == false ) {
                    adEnd1[iC] = 0.0;
                    adEnd2[iC] = 1.0;
                } else {
                    adEnd1[iC] = 1.0;
                    adEnd2[iC] = 0.0;
                }
            }
//            cout << crv(0.0) << "\n";
//            cout << crv(1.0);
            cout << GetCurve( out_aaiPatch[iP][iC] )( adEnd1[iC] ) << "\n";
            cout << GetCurve( out_aaiPatch[iP][iC] )( adEnd2[iC] );
        }
        cout << "\n";
        for ( int iC = 0; iC < iNCurves; iC++ ) {
            const R3Pt ptE1 = GetCurve( out_aaiPatch[iP][iC] )(adEnd2[iC]);
            const R3Pt ptE2 = GetCurve( out_aaiPatch[iP].wrap(iC+1) )(adEnd1.wrap(iC+1));
            if ( !ApproxEqual( ptE1, ptE2, 1e-6 ) ) {
                cerr << "ERR: CurveNetwork::ReadPatchMaker ends not equal " << ptE1 << " " << ptE2 << "\n";
            }

            NewSnapCurves( out_aaiPatch[iP][iC], adEnd2[iC], out_aaiPatch[iP].wrap(iC+1), adEnd1.wrap(iC+1) );
        }
    }
    for ( int iP = 0; iP < m_aopPins.num(); iP++ ) {
        if ( m_aopPins[iP]->NumCurves() > 2 ) {
            m_aopPins[iP]->ConvertToPointAndNormalConstraint( m_aopPins[iP]->GetPt() );
        }
    }
    for ( int iP = 0; iP < m_aopPins.num(); iP++ ) {
        if ( m_aopPins[iP]->NumCurves() <= 2 ) {
            m_aopPins[iP]->ConvertToPointAndNormalConstraint( m_aopPins[iP]->GetPt() );
        }
    }
}

void CurveNetwork::ReadTangentNormal( ifstream &in )
{
    char str[500];
    in.getline( str, 500 );
    ASSERT( str[0] == '#' );
    
    for ( int i = 0; i < NumCrvs(); i++ ) {
        m_aopCrv[i]->ReadTangentNormal(in);
    }        
}

/* Need to do with Maya curves */
void CurveNetwork::CenterAndScale()
{
    const std::pair<R3Pt,R3Pt> ptExtent = BoundingBox();
    const R3Pt ptCenter = Lerp( ptExtent.first, ptExtent.second, 0.5 );

    const double dLenDiag = ::Length( ptExtent.first - ptExtent.second );
    const double dScl = 1.0 / ( dLenDiag );

    const R4Matrix matTransScale = R4Matrix::Scaling( dScl, dScl, dScl, 1.0 ) * R4Matrix::Translation( R3Pt(0,0,0) - ptCenter );
    for ( int i = 0; i < NumCrvs(); i++ ) {
        SetCurveGroup(i).Transform( matTransScale, false );
    }

	
	m_iCurveLastStroke = m_iLastEdited = m_iSaveLastEdited = -1;
}

CurveNetwork &CurveNetwork::operator=( const CurveNetwork &in_crvNW )
{
    cerr << "ERR: Curve network being copied!\n";
    return *this;
}

CurveNetwork::~CurveNetwork()
{
    Clear();
}

#define MAX_LINE_LENGTH 20000
void CurveNetwork::ReadMaya( ifstream &fin )
{
	char line[MAX_LINE_LENGTH]={0};			//the actual line from the input file
	char tempLine[MAX_LINE_LENGTH]={0};		//copy of the above (for tokenizing)
	char lineData[MAX_LINE_LENGTH]={0};		//store individual tokens in here..one by one
	
	Clear();
	
	Curve crv;
	R3Pt pt;
	while (fin.getline(line, MAX_LINE_LENGTH)) {
		strcpy(tempLine, line);
		
		//split the line into tokens using '\t' or space as the delimiter
		char * pch;
		pch = strtok(tempLine,"\t ");
		if (pch != NULL) {
			//check if this is the beginning of the 'createNode nurbsCurve' 
			if (!strcmp(pch, "createNode"))	{
				pch = strtok (NULL, "\t ");
				
				if (!strcmp(pch, "nurbsCurve")) {					
					//read in this curve 
					//std::vector<Rep::MeshRepousseSolver::Mesh::Point> curve; 
					crv.Clear();
					
					//ignore first three lines
					for (unsigned int i=0;i<3;i++)
						fin.getline(line, MAX_LINE_LENGTH);

					//now read the # of samples
					int numSamples = 0;
					fin>>numSamples;
					double param = 0; //unused for now
					for (int i=0;i<numSamples;i++)
						fin>>param;
					
					//now read the # of samples (again)
					fin>>numSamples;
					for (int i=0;i<numSamples;i++){
						fin>>pt;
						//Rep::MeshRepousseSolver::Mesh::Point p(x,y,z);
						crv.AddPoint(pt);
					}
					crv.SetAllData();
					
					NewCompositeCurve(crv);
				} //if found a nurbsCurve
			} //if this was a createNode
		} // pch was not NULL
		
		//clear the lines
		for (int i=0;i<MAX_LINE_LENGTH;i++)
		{
			line[i]=tempLine[i]=lineData[i]=0;
		}
	}
	fin.close();
	
	CenterAndScale();
}

void CurveNetwork::AddNormalsFromCurves( const CurveNetwork &in_crvs )
{
    
}

