/*
 *  CurveNetwork_Aligning.cpp
 *  SketchCurves
 *
 *  Created by Cindy Grimm on 2/18/2011
 *  Copyright 2011 Adobe Corp. All rights reserved.
 *
 */

#include "CurveNetwork.h"
#include "DrawState.h"
#include "PointConstraint.h"
#include "UserInterface.h"

const bool s_bTrace = false;

void CurveNetwork::FixAllIntersectionNormals()
{
    const R3Pt ptCenter = Center();
    
    for ( int iP = 0; iP < m_aopPins.num(); iP++ ) {
        if ( !m_aopPins[iP]->IsNormal()  && m_aopPins[iP]->NumCurves() > 1 ) {
            m_aopPins[iP]->ConvertToPointAndNormalConstraint( ptCenter );
        }
    }
}


PointConstraint *
CurveNetwork::FindOrResurrectPin( const int in_iPin )
{
    PointConstraint *opConstraint = NULL;
    for ( int iP = 0; iP < m_aopPins.num(); iP++ ) {
        if ( m_aopPins[iP]->HashId() == in_iPin ) {
            opConstraint = m_aopPins[iP];
            return opConstraint;
        }
    }
    
    for ( int iP = 0; iP < m_aopPinGraveyard.num(); iP++ ) {
        if ( m_aopPinGraveyard[iP]->HashId() == in_iPin ) {
            opConstraint = m_aopPinGraveyard[iP];
            m_aopPins += opConstraint;
            m_aopPinGraveyard.del(iP,1);
            break;
        }
    }
    
    return opConstraint;
}

void CurveNetwork::RemoveEmptyConstraints()
{
    Array<int> aiKill;
    for ( int i = 0; i < NumConstraints(); i++ ) {
        if ( m_aopPins[i]->NumCurves() == 0 ) {
            aiKill += i;
        }
    }
    aiKill.reverse();

    // Count down
    for ( int i = 0; i < aiKill.num(); i++ ) {
        if ( s_bTraceErase ) {
            cout << "Removing pin\n";
            m_aopPins[ aiKill[i] ]->Print();
        }
        m_aopPinGraveyard += m_aopPins[  aiKill[i] ];
        m_aopPins.del( aiKill[i], 1 );
    }
}

/*
 * Undo
 * Input: Curve that we're undoing the erase on and 
 * pin ids that were on the curve at that time
 *
 * For each pin:
 *   1) Resurrect the pin if need be
 *   2) Rollback the pin to the erase time stamp
 *      2a) Only include curves that exist and that are still close to the constraint
 */
void CurveNetwork::RestoreConstraintsInEraseRegion( const Array<int> &in_aiPinIds, const int in_iTimeStamp )
{
    // First, resurrect any pin constraints that got deleted
    for ( int i = 0; i < in_aiPinIds.num(); i++ ) {
        PointConstraint *opPin = FindOrResurrectPin( in_aiPinIds[i] );
        if ( opPin == NULL ) {
            cerr << "ERR: CurveNetwork::RestoreConstraintsInEraseRegion. could not find pin " << in_aiPinIds[i] << "\n";
            continue;
        }
        
        if ( s_bTraceUndo ) {
            cout << "Restoring " << in_aiPinIds[i] << ", time " << in_iTimeStamp << "\n";
            opPin->PrintHistory();
        }
            
        opPin->Rollback( in_iTimeStamp, m_aopCrv, false, false );
        
        if ( opPin->NumCurves() == 0 || ( opPin->IsPoint() && opPin->NumCurves() < 2 ) ) {
            RemoveConstraint( opPin );
            if (s_bTraceUndo) cout << "Removed\n";
        }
        
        if ( s_bTraceUndo ) {
            cout << "Kept\n";
            opPin->PrintHistory();
        }
    }
}

/* Undo
 * Constraints in list are those that were on the original curve; put them back in
 *  and take them back out of the original curve
 *
 * Also rollback all of the constraints on the original curve
 * 
 */
void CurveNetwork::RestoreConstraintsCombine( CurveGroup &io_crvGroupOrig, CurveGroup &in_crvRestored, 
                                              const Array<int> &in_aiPinIds, const int in_iTimeStamp )
{
    for ( int i = 0; i < in_aiPinIds.num(); i++ ) {
        PointConstraint *opConstraint = FindOrResurrectPin( in_aiPinIds[i] );
        
        if ( opConstraint == NULL ) {
            cerr << "ERR: CurveNetwork::RestoreConstraintsCombine, could not find constraint " << in_aiPinIds[i] << "\n";
            continue;
        }
        
        if ( s_bTraceUndo ) {
            cout << "Moving pin: Before rollback " << in_aiPinIds[i] << ", timestamp " << in_iTimeStamp << "\n";
            opConstraint->PrintHistory();
        }
        opConstraint->Rollback( in_iTimeStamp, m_aopCrv, false, false );
        
        if ( !opConstraint->HasCurveGroup( in_crvRestored ) ) {
            cerr << "ERR: CurveNetwork::RestoreConstraintsCombine, could not find curve after rollback " << in_aiPinIds[i] << "\n";
        }
        
        if ( opConstraint->HasCurveGroup( io_crvGroupOrig ) ) {
            io_crvGroupOrig.RemoveConstraint( opConstraint );
            opConstraint->RemoveCurveGroup( &io_crvGroupOrig );
        }

        if ( s_bTraceUndo ) {
            cout << "After\n";
            opConstraint->PrintHistory();
        }
    }
    
    /* Do this second, after all of the added constraints have been removed */
    for ( int i = 0; i < io_crvGroupOrig.NumConstraints(); i++ ) {
        PointConstraint *opConstraint = FindOrResurrectPin( io_crvGroupOrig.GetConstraint(i).HashId() );
        
        if ( opConstraint == NULL ) {
            cerr << "ERR: CurveNetwork::RestoreConstraintsCombine, could not find constraint " << io_crvGroupOrig.GetConstraint(i).HashId() << "\n";
            continue;
        }
        if ( s_bTraceUndo ) {
            cout << "Restoring pin: Before rollback " << opConstraint->HashId() << ", timestamp " << in_iTimeStamp << "\n";
            opConstraint->PrintHistory();
        }
        opConstraint->Rollback( in_iTimeStamp, m_aopCrv, false, false );
        if ( s_bTraceUndo ) {
            cout << "After\n";
            opConstraint->PrintHistory();
        }
    }        
}

/* Undo a removed constraint. May need to resurrect from graveyard.  
 */
void CurveNetwork::AddConstraintToCurve( CurveGroup &in_crvGroup, const int in_iHashId, const int in_iTimeStamp )
{
    PointConstraint *opConstraint = FindOrResurrectPin( in_iHashId );
    
    if ( opConstraint == NULL ) {
        cerr << "ERR: CurveNetwork::AddConstraintToCurve, could not find constraint " << in_iHashId << "\n";
        return;
    }
        
    if ( s_bTraceUndo ) {
        cout << "Before\n";
        opConstraint->PrintHistory();
    }
    
    if ( opConstraint->HadCurveGroupPast( in_crvGroup, in_iTimeStamp ) ) {
        const double dT = opConstraint->GetTValuePast( in_crvGroup, in_iTimeStamp );
        opConstraint->AddCurveGroup( &in_crvGroup, in_crvGroup.CompositeCurve().ClosestIndex( dT ), dT );
    } else {
        cerr << "ERR: CurveNetwork::AddConstraintToCurve, constraint exists, but curve not in it " << in_iHashId << "\n";
    }
    
    if ( s_bTraceUndo ) {
        cout << "After\n";
        opConstraint->PrintHistory();
    }
}


/* Undo an added constraint. Check that the constraint exists, is active,
 * and still has the curve group in it before removing 
 */
void CurveNetwork::RemoveConstraintFromCurve( CurveGroup &in_crvGroup, const int in_iHashId )
{
    PointConstraint *opConstraint = NULL;
    for ( int i = 0; i < m_aopPins.num(); i++ ) {
        if ( m_aopPins[i]->HashId() == in_iHashId ) {
            opConstraint = m_aopPins[i];
        }
    }
    if ( opConstraint == NULL ) {
        cerr << "ERR: CurveNetwork::RemoveConstraintFromCurve, no existing constraint " << in_iHashId << "\n";
    } else {
        if ( s_bTraceUndo ) {
            cout << "Before\n";
            opConstraint->PrintHistory();
        }
        
        if ( !opConstraint->HasCurveGroup( in_crvGroup ) ) {
            cerr << "ERR: CurveNetwork::RemoveConstraintFromCurve, curve group not in constraint " << in_iHashId << "\n";
        } else {
            opConstraint->RemoveCurveGroup( &in_crvGroup );
        }
        if ( s_bTraceUndo ) {
            cout << "After\n";
            opConstraint->PrintHistory();
        }
        if ( !opConstraint->IsNormal() && opConstraint->NumCurves() == 1 ) {
            RemoveConstraint( opConstraint );
        } 
    }
}

/* Undo an added constraint. Check that the constraint exists, is active,
 * and still has the curve group in it before removing 
 */
void CurveNetwork::RollbackConstraintAction( const int in_iHashId, const int in_iTimeStamp )
{
    PointConstraint *opConstraint = NULL;
    for ( int i = 0; i < m_aopPins.num(); i++ ) {
        if ( m_aopPins[i]->HashId() == in_iHashId ) {
            opConstraint = m_aopPins[i];
        }
    }
    if ( opConstraint == NULL ) {
        cerr << "ERR: CurveNetwork::RollbackConstraintAction, no existing constraint " << in_iHashId << "\n";
    } else {
        if ( s_bTraceUndo ) {
            cout << "Before, id " << in_iHashId << ", time " << in_iTimeStamp << "\n";
            opConstraint->PrintHistory();
        }
        
        opConstraint->Rollback( in_iTimeStamp, m_aopCrv, true, true );
        
        if ( s_bTraceUndo ) {
            cout << "After\n";
            opConstraint->PrintHistory();
        }
    }   
}

/* The input pins may have changed; for each curve, see if it
 * has that constraint, and if it does, re-establish it
 *
 * Do this at the end so all constraints are put back in before updating
 *
 * Delay geom update until all constraints are updated
 */
void CurveNetwork::ReEstablishConstraints( const Array<int> &in_aiPinIds )
{
    for ( int i = 0; i < NumCrvs(); i++ ) {
        CurveGroup &crvGrp = SetCurveGroup(i);
        bool bChanged = false;
        for ( int iP = 0; iP < crvGrp.NumConstraints(); iP++ ) {
            if ( in_aiPinIds.contains( crvGrp.GetConstraint(iP).HashId() ) ) {
                if ( crvGrp.GetConstraint(iP).IsPoint() ) {
                    crvGrp.ReEstablishPointConstraint( *crvGrp.SetConstraint(iP), false );
                }
                bChanged = true;
            }
        }
        if ( bChanged == true ) {
            crvGrp.FinishedUndo();
        }
    }
}

/*
 * First, make sure there isn't a constraint right there already.
 * Find the normal on the curve at that t value and use it to set the normal of the constraint
 * Add the constraint to the curve
 *   Shouldn't change the curve, but t value may need to shift
 *   Curve is responsible for finding t and index value and adding self to point constraint
 *   Curve can then update itself (normals)
 * Add the constraint to our list
 */
PointConstraint *CurveNetwork::NewNormalConstraint( const int in_iCrv1, const double in_dCrv1 )
{
    if ( in_iCrv1 < 0 || in_iCrv1 >= NumCrvs() || in_dCrv1 < 0.0 || in_dCrv1 > 1.0 ) {
        cerr << "ERR CurveNetwork::NewNormalConstraint bad curve " << in_iCrv1 << " " << NumCrvs();
        cerr << " or t value " << in_dCrv1 << "\n";
        return NULL;
    }
    
    /* First check to see if this curve actually has a normal constraint right about here */
    CurveGroup &crvGrp = *m_aopCrv[ in_iCrv1 ];
    
    const double dSelDist = g_drawState.SelectionSize();
    for ( int i = 0; i < crvGrp.NumConstraints(); i++ ) {
        if ( crvGrp.GetConstraint(i).IsNormal() ) {
            const double dT = crvGrp.GetConstraint(i).GetTValue( crvGrp );
            const double dDist = fabs( in_dCrv1 - dT ) * crvGrp.CompositeCurve().Length();
            if ( dDist < 0.1 * dSelDist ) {
                cerr << "Warning: tried to add a normal constraint next to an existing one, not adding " << in_iCrv1 << " " << dT << " " << in_dCrv1 << "\n";
                for ( int j = 0; j < NumConstraints(); j++ ) {
                    if ( *m_aopPins[j] == crvGrp.GetConstraint(i) ) {
                        return m_aopPins[j];
                    }
                }
                cerr << "ERR: CurveNetwork::NewNormalConstraint, pins invalid\n";
                return NULL;
            }
        }
    }

    /* Create a new normal constraint with the curve's normal */
    PointConstraint *opPin = new PointConstraint;
    opPin->SetNormalConstraint( GetCurveRibbon( in_iCrv1 ).Normal( in_dCrv1 ) );
    crvGrp.AddConstraint( opPin, in_dCrv1, false );
    
    m_aopPins += opPin;

    return opPin;
}

/// Create a constraint and add it to the list and to the curve
PointConstraint *CurveNetwork::NewPinConstraint( const int in_iCrv1, const double in_dCrv1 )
{
    if ( in_iCrv1 < 0 || in_iCrv1 >= NumCrvs() || in_dCrv1 < 0.0 || in_dCrv1 > 1.0 ) {
        cerr << "ERR CurveNetwork::NewPinConstraint bad curve " << in_iCrv1 << " " << NumCrvs();
        cerr << " or t value " << in_dCrv1 << "\n";
        return NULL;
    }
    
    /* First check to see if this curve actually has a normal constraint right about here */
    CurveGroup &crvGrp = *m_aopCrv[ in_iCrv1 ];
    
    const double dSelDist = g_drawState.SelectionSize();
    for ( int i = 0; i < crvGrp.NumConstraints(); i++ ) {
        if ( crvGrp.GetConstraint(i).IsPoint() ) {
            const double dT = crvGrp.GetConstraint(i).GetTValue( crvGrp );
            const double dDist = fabs( in_dCrv1 - dT ) * crvGrp.CompositeCurve().Length();
            if ( dDist < 0.1 * dSelDist ) {
                cerr << "Warning: tried to add a point constraint next to an existing one, not adding " << in_iCrv1 << " " << dT << " " << in_dCrv1 << "\n";
                for ( int j = 0; j < NumConstraints(); j++ ) {
                    if ( *m_aopPins[j] == crvGrp.GetConstraint(i) ) {
                        return m_aopPins[j];
                    }
                }
                cerr << "ERR: CurveNetwork::NewPinConstraint, pins invalid\n";
                return NULL;
            }
        }
    }
    
    /* Create a new constraint with the curve's point. Do a drag in order to adjust the t values */
    PointConstraint *opPin = new PointConstraint;
    opPin->SetPointConstraint( GetCurveRibbon( in_iCrv1 )( in_dCrv1 ) );
    crvGrp.AddConstraint( opPin, in_dCrv1, true );
    
    // Add to master pin list
    m_aopPins += opPin;
    
    return opPin;
}

PointConstraint *CurveNetwork::NewSnapCurves( const int in_iCrv1, const double in_dCrv1, const int in_iCrv2, const double in_dCrv2 )
{
    if ( s_bTraceAddingConstraints ) {
        cout << "\nSnapping curves, start\n";
        m_aopCrv[in_iCrv1]->PrintHistory( false );
        m_aopCrv[in_iCrv2]->PrintHistory( false );
    }
    
    PointConstraint *opCheck = HasConstraint( in_iCrv1, in_dCrv1 );
    PointConstraint *opCheckOther = HasConstraint( in_iCrv2, in_dCrv2 );

    /* By default, the new curve will be dragged to the existing pin point, if a pin exists
     * Otherwise, Set to the mid point *unless* one of the pin points is an end point,
     * in which case, drag only the end point
     */
    const bool bIsEnd1 = RNIsZero( in_dCrv1 ) || RNApproxEqual( in_dCrv1, 1.0 ); 
    const bool bIsEnd2 = RNIsZero( in_dCrv2 ) || RNApproxEqual( in_dCrv2, 1.0 ); 
    const int iNPins1 = GetCurveGroup(in_iCrv1).NumPointConstraints();
    const int iNPins2 = GetCurveGroup(in_iCrv2).NumPointConstraints();

    if ( opCheck && opCheckOther ) {
        if ( s_bTraceAddingConstraints ) {
            cout << "Merging\n";
            opCheck->Print();
            opCheckOther->Print();
        }
        if ( *opCheck == *opCheckOther ) {
            return opCheck;
        }
        
        opCheck->Merge( *opCheckOther );
        RemoveConstraint( opCheckOther );
        
        if ( s_bTraceAddingConstraints ) {
            cout << "Merged\n";
            opCheck->Print();
        }
        return opCheck;
    } else if ( opCheck && !opCheckOther ) {
        m_aopCrv[in_iCrv2]->AddConstraint( opCheck, in_dCrv2 );
        if ( s_bTraceAddingConstraints ) {
            cout << "Adding\n";
            opCheck->Print();
        }
    } else if ( !opCheck && opCheckOther ) {
        if ( s_bTraceAddingConstraints ) {
            cout << "Adding\n";
            opCheckOther->Print();
        }
        m_aopCrv[in_iCrv1]->AddConstraint( opCheckOther, in_dCrv1 );
        opCheck = opCheckOther;
    } else {
        /// Only drag end 1
        if ( bIsEnd1 || ( iNPins1 == 0 && iNPins2 > 0 ) ) {
            if ( s_bTraceAddingConstraints ) cout << "End1 move, no drag\n";
            opCheck = NewPinConstraint( in_iCrv2, in_dCrv2 );
            // Will drag end 1
            m_aopCrv[in_iCrv1]->AddConstraint( opCheck, in_dCrv1 );
        } else if ( bIsEnd2 || ( iNPins2 == 0 && iNPins1 > 0 ) ) {
            if ( s_bTraceAddingConstraints ) cout << "End2 move, no drag\n";
            // Will drag end 2 only
            opCheck = NewPinConstraint( in_iCrv1, in_dCrv1 );
            m_aopCrv[in_iCrv2]->AddConstraint( opCheck, in_dCrv2, true );
        } else {
            if ( s_bTraceAddingConstraints ) cout << "Drag\n";
            // Add curve group in with current location
            opCheck = NewPinConstraint( in_iCrv1, in_dCrv1 );
            // Add second location in without drag
            m_aopCrv[in_iCrv2]->AddConstraint( opCheck, in_dCrv2, false );
            
            // Now drag both together. Increment the timer so the drag is separate from the add
            IncrementTime();
            const R3Pt ptMid = Lerp( GetCurve(in_iCrv1)(in_dCrv1), GetCurve(in_iCrv2)(in_dCrv2), 0.5 );
            opCheck->DragPointConstraint( ptMid - opCheck->GetPt(), false );
        }
    }
    
    PropagateEdits();
    
    if ( s_bTraceAddingConstraints ) {
        for ( int i = 0; i < m_aopCrv[in_iCrv1]->NumConstraints(); i++ ) {
            m_aopCrv[in_iCrv1]->GetConstraint(i).CheckConsistency();
        }
        for ( int i = 0; i < m_aopCrv[in_iCrv2]->NumConstraints(); i++ ) {
            m_aopCrv[in_iCrv2]->GetConstraint(i).CheckConsistency();
        }
        opCheck->CheckConsistency();
        m_aopCrv[in_iCrv1]->CheckConsistency();
        m_aopCrv[in_iCrv2]->CheckConsistency();
        
        cout << "Result\n";
        m_aopCrv[in_iCrv1]->PrintHistory( true );
        m_aopCrv[in_iCrv2]->PrintHistory( true );
        cout << "SNAPPING CURVES, end\n\n";
    }
    
    return opCheck;
}

/// Remove curves from constraint then delete the constraint
void CurveNetwork::RemoveConstraint( PointConstraint *io_pin )
{
    io_pin->RemoveAllCurveGroups();
    
    for ( int i = 0; i < NumConstraints(); i++ ) {
        if ( *m_aopPins[i] == *io_pin ) {
            m_aopPins.del(i, 1);
            m_aopPinGraveyard += io_pin;
            return;
        }
    }
    
    cerr << "ERR CurveNetwork::RemoveConstraint, not found " << io_pin->HashId() << "\n";
}

///
PointConstraint *CurveNetwork::GetClosestPointConstraint( const R3Ray &in_ray, double &out_dDist, double &out_dZ )
{
    //TODO: new pin constraint
    return NULL;
}

///
PointConstraint *CurveNetwork::GetClosestNormalConstraint( const R3Ray &in_ray, double &out_dDist, double &out_dZ )
{
    out_dDist = 1e30;
    out_dZ = 1e30;
    
	if ( g_drawState.m_opUI && !g_drawState.m_opUI->m_bShowCurveNormalConstraints->value() ) {
		return NULL;
	}

    const double dSelDist = g_drawState.SelectionSize();
    
	PointConstraint *opOut = NULL;
	double dDistBest = 1e30;
	double dZBest = 1e30;
	
	for ( int i = 0; i < NumConstraints(); i++ ) {
        if ( GetConstraint(i).IsHidden() || !GetConstraint(i).IsNormal() ) {
            continue;
        }
		
        double dDist, dZ;
        if ( GetConstraint(i).SelectNormalConstraint( in_ray, dDist, dZ ) ) {

			const double dDistDiff = fabs( dDist - dDistBest ) / dSelDist;
			const double dZDiff = fabs( dZBest - dZ ) / 0.01;
			
			if ( (dDist < dDistBest && dZ < dZBest ) || ( dDist < dDistBest && dZDiff < 0.01 ) || ( dDistDiff < 0.25 && dZ < dZDiff ) ) {
				opOut = m_aopPins[i];
				dDistBest = dDist;
				dZBest = dZ;
			}
		}
	}
    out_dZ = dZBest;

	return opOut;
}

PointConstraint *CurveNetwork::HasConstraint( const int in_iCurve, const double in_dT )
{
    if ( in_iCurve < 0 || in_iCurve >= NumCrvs() ) {
        cerr << "ERR: CurveNetwork::HasConstraint, bad curve id " << in_iCurve << " " << NumCrvs() << "\n";
        return NULL;
    }
    
    return m_aopCrv[in_iCurve]->HasConstraint( in_dT );
}

const PointConstraint *CurveNetwork::HasConstraintNoEdit( const int in_iCurve, const double in_dT ) const
{
    if ( in_iCurve < 0 || in_iCurve >= NumCrvs() ) {
        cerr << "ERR: CurveNetwork::HasConstraint, bad curve id " << in_iCurve << " " << NumCrvs() << "\n";
        return NULL;
    }
    
    return m_aopCrv[in_iCurve]->HasConstraintNoEdit( in_dT );
}


void CurveNetwork::CheckConsistency() const
{
    if ( s_bTrace ) cerr << "Checking consistency " << NumCrvs() << " " << NumConstraints() << "\n";
    
    for ( int i = 0; i < NumConstraints(); i++ ) {
        GetConstraint(i).CheckConsistency();
    }
    
    for ( int i = 0; i < NumCrvs(); i++ ) {
        GetCurveGroup(i).CheckConsistency();
    }
    
    for ( int i = 0; i < m_aopPinGraveyard.num(); i++ ) {
        if ( m_aopPins.contains( m_aopPinGraveyard[i] ) ) {
            cerr << "ERR: CurveNetwork::CheckConsistency, pin in both graveyard and pins " << m_aopPinGraveyard[i]->HashId() << "\n";
        }
    }
    for ( int i = 0; i < m_aopCrvGraveyard.num(); i++ ) {
        if ( m_aopCrv.contains( m_aopCrvGraveyard[i] ) ) {
            cerr << "ERR: CurveNetwork::CheckConsistency, curve in both graveyard and curves " << m_aopCrvGraveyard[i]->HashId() << "\n";
        }
    }
}

/* Make sure all delayed edits go through */
void CurveNetwork::PropagateEdits()
{
    if ( s_bTracePropagate ) cout << "CurveNetwork::Beginning propagate\n";
    
    bool bDone = false;
    int iCount = 10;
	Array<int> aiPins;
    while ( bDone == false && iCount-- > 0 ) {
        bDone = true;
		aiPins.need(0);
        for ( int i = 0; i < NumConstraints(); i++ ) {
            if ( GetConstraint(i).NeedsUpdating() ) {
                if ( s_bTracePropagate ) cout << "PE Pin " << GetConstraint(i).HashId() << "\n";
				aiPins += GetConstraint(i).HashId();
                bDone = false;
			}
        }
		
		if ( bDone == false ) {
			ReEstablishConstraints( aiPins );
		}

		// Flag as done
		for ( int i = 0; i < m_aopPins.num(); i++ ) {
            if ( GetConstraint(i).NeedsUpdating() ) {
				m_aopPins[ i ]->DelayedUpdate();
			}
		}
		
    }
    
    if ( s_bTracePropagate ) {
        for ( int i = 0; i < NumCrvs(); i++ ) {
            GetCurveGroup(i).PrintHistory(false);
        }
        cout << "CurveNetwork::Done propagate " << iCount << "\n";
    }

    if ( bDone == false ) {
        cerr << "ERR: CurveNetwork::PropagateEdits Circular edits\n";
    }
    
    CheckConsistency();
}

/*
std::pair<int,int> CurveNetwork::ClosestCurveNormalConstraint( const R3Ray &in_ray ) const
{
	if ( g_drawState.m_opUI && !g_drawState.m_opUI->m_bShowCurveNormalConstraints->value() ) {
		return std::pair<int,int>(-1,-1);
	}
	
	double dDistBest = 1e30;
	double dZBest = 1e30;
	std::pair<int,int> iBest = std::pair<int,int>(-1,-1);
	
	const double dSelDist = g_drawState.SelectionSize();
	const OGLObjsCamera &cam = g_drawState.GetCamera();
	
	double dDist = 1e30;
	for ( int i = 0; i < m_acrv.num(); i++ ) {
		if ( IsHideCurve(i) ) continue;
		
		const int iWhich = GetCurve(i).SelectNormalConstraint( in_ray, dDist );
		if ( iWhich != -1 && dDist < dSelDist ) {
			const R3Pt ptProj = cam.ProjectedPt( GetCurve(i).NormalConstraintPt(iWhich ) );
			const double dDistDiff = fabs( dDist - dDistBest ) / dSelDist;
			const double dZDiff = fabs( dZBest - ptProj[2] ) / 0.01;
			
			if ( (dDist < dDistBest && ptProj[2] < dZBest ) || ( dDist < dDistBest && dZDiff < 0.01 ) || ( dDistDiff < 0.25 && ptProj[2] < dZDiff ) ) {
				iBest = std::pair<int,int>( i, iWhich );
				dDistBest = dDist;
				dZBest = ptProj[2];
			}
		}
	}
	return iBest;
}

///
int CurveNetwork::AddNormalConstraint( const int in_iCrv, const R3Ray &in_ray )
{
	return m_acrv[in_iCrv].AddNormalConstraint( in_ray );
}

int CurveNetwork::AddNormalConstraint( const int in_iCrv, const double in_dT, const R3Vec &in_vec )
{
	return m_acrv[in_iCrv].AddNormalConstraint( in_dT, in_vec );
}

///
void CurveNetwork::RemoveNormalConstraint( const int in_iCrv, const int in_iWhich )
{
	m_acrv[in_iCrv].RemoveNormalConstraint( in_iWhich );
}

void CurveNetwork::EditNormalConstraint( const int in_iCrv, const int in_iWhich, const R3Vec &in_vec )
{
	m_acrv[in_iCrv].EditNormalConstraint( in_iWhich, in_vec );
}



std::pair<int,int> CurveNetwork::ClosestCurvePointConstraint( const R3Ray &in_ray ) const
{
	if ( g_drawState.m_opUI && !g_drawState.m_opUI->m_bShowCurvePointConstraints->value() ) {
		return std::pair<int,int>(-1,-1);
	}
	
	double dDistBest = 1e30;
	double dZBest = 1e30;
	std::pair<int,int> iBest = std::pair<int,int>(-1,-1);
	
	const double dSelDist = g_drawState.SelectionSize();
	const OGLObjsCamera &cam = g_drawState.GetCamera();
	
	double dDist = 1e30;
	for ( int i = 0; i < m_acrv.num(); i++ ) {
		if ( IsHideCurve(i) ) continue;
		
		const int iWhich = GetCurve(i).SelectPointConstraint( in_ray, dDist );
		if ( iWhich != -1 && dDist < dSelDist ) {
			const R3Pt ptProj = cam.ProjectedPt( GetCurve(i).PointConstraint(iWhich ) );
			const double dDistDiff = fabs( dDist - dDistBest ) / dSelDist;
			const double dZDiff = fabs( dZBest - ptProj[2] ) / 0.01;
			
			if ( (dDist < dDistBest && ptProj[2] < dZBest ) || ( dDist < dDistBest && dZDiff < 0.01 ) || ( dDistDiff < 0.25 && ptProj[2] < dZDiff ) ) {
				iBest = std::pair<int,int>( i, iWhich );
				dDistBest = dDist;
				dZBest = ptProj[2];
			}
		}
	}
	return iBest;
}

///
int CurveNetwork::AddPointConstraint( const int in_iCrv, const R3Ray &in_ray )
{
	return m_acrv[in_iCrv].AddPointConstraint( in_ray );
}

int CurveNetwork::AddPointConstraint( const int in_iCrv, const double in_dT )
{
	return m_acrv[in_iCrv].AddPointConstraint( in_dT );
}

///
void CurveNetwork::RemovePointConstraint( const int in_iCrv, const int in_iWhich )
{
	m_acrv[in_iCrv].RemovePointConstraint( in_iWhich );
}

void CurveNetwork::MovePointConstraint( const int in_iCrv, const int in_iWhich, const R3Vec &in_vec )
{
	m_acrv[in_iCrv].MovePointConstraint( in_iWhich, in_vec );
}

/// Drag a curve point and add a point constraint 
double CurveNetwork::SnapToPoint( const R3Pt &in_pt, const int in_iCrv, 
                                 const double in_dT, const std::pair<double,double> &in_dTBracket )
{
    if ( in_iCrv < 0 || in_iCrv >= m_acrv.num() ) {
        cerr << "ERR: CurveNetwork Snap together, no such curve " << in_iCrv << "\n";
    }
    const double dNewT = m_acrv[in_iCrv].SnapToPoint( in_pt, in_dT, in_dTBracket );
    
    UpdateReflected( in_iCrv );
    
    return dNewT;
}

/// Snap the two curves together and introduce a point constraint 
void CurveNetwork::SnapCurves( const int in_iCrv1, const double in_dCrv1, const int in_iCrv2, const double in_dCrv2 )
{
	R3Pt ptMiddle( Lerp( GetCurve(in_iCrv1)(in_dCrv1), GetCurve(in_iCrv2)(in_dCrv2), 0.5 ) );
	double dCrv1 = in_dCrv1, dCrv2 = in_dCrv1;
	
	double dDist;
	R3Pt ptClosest1, ptClosest2;
	for ( int i = 0; i < 3; i++ ) {
		dCrv1 = GetCurve( in_iCrv1 ).ClosestPointTValue( ptMiddle, dDist, ptClosest1 );
		dCrv2 = GetCurve( in_iCrv2 ).ClosestPointTValue( ptMiddle, dDist, ptClosest2 );
		ptMiddle = Lerp( ptClosest1, ptClosest2, 0.5 );
	}
	
	const int i1 = m_acrv[ in_iCrv1 ].AddPointConstraint( dCrv1 );
	const int i2 = m_acrv[ in_iCrv2 ].AddPointConstraint( dCrv2 );
	
	m_acrv[in_iCrv1].MovePointConstraint( i1, ptMiddle - GetCurve( in_iCrv1 ).PointConstraint(i1) );
	m_acrv[in_iCrv2].MovePointConstraint( i2, ptMiddle - GetCurve( in_iCrv2 ).PointConstraint(i2) );
	
	const R3Vec vecTang1 = GetCurve(in_iCrv1).Tangent(dCrv1);
	const R3Vec vecTang2 = GetCurve(in_iCrv2).Tangent(dCrv2);
	
	const double dDot = Dot( vecTang1, vecTang2 );
	if ( fabs( dDot < 0.8 ) ) {
		R3Vec vecNorm = UnitSafe( Cross( vecTang1, vecTang2 ) );
		if ( GetCurve(in_iCrv1).IsClosed() ) {
			if ( Dot( ptMiddle - GetCurve(in_iCrv1).CenterPoint(), vecNorm ) < 0.0 ) {
				vecNorm *= -1;
			}
		} else if ( GetCurve(in_iCrv2).IsClosed() ) {
			if ( Dot( ptMiddle - GetCurve(in_iCrv2).CenterPoint(), vecNorm ) < 0.0 ) {
				vecNorm *= -1;
			}
		} else {
			if ( Dot( ptMiddle - Center(), vecNorm ) < 0.0 ) {
				vecNorm *= -1;
			}
		}
			
		m_acrv[in_iCrv1].AddNormalConstraint( dCrv1, vecNorm );
		m_acrv[in_iCrv2].AddNormalConstraint( dCrv2, vecNorm );
	}
}

 */
