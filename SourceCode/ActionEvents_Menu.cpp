/*
 *  ActionEvents_Menu.cpp
 *  SketchCurves
 *
 *  Created by Cindy Grimm on 9/9/10.
 *  Copyright 2010 Washington University in St. Louis. All rights reserved.
 *
 */

#include <FL/GL.H>
#include <OpenGL/OGLDraw2D.H>
#include "ActionEvents.h"
#include "ParamFile.h"
#include "UserInterface.h"

const bool s_bTrace = false;

/*
 * Control the wedge size and the gap between the wedges and the size of the interior circle
 */
const int ciNumMenuItems = 8;
const double cdSizeWedge = 2.0 * M_PI / (double) ciNumMenuItems;
const double cdWedgeWidth = 0.7 * cdSizeWedge;
const double cdInteriorCirclePerc = 0.25;

char ActionEvents::s_astrMenu[ActionEvents::NUM_MENU][60] = 
{ "No menu",
	"Last stroke menu",
	"Last stroke combine",
	"Last stroke join closed",
	"Last stroke merge",
	"Last stroke blend",
	"Last stroke delete",
	"Last stroke new closed",
	"Last stroke overstroke",
	"Last stroke smooth",
	"Last stroke new curve",
	"Curve menu",
	"Curve snap",
	"Curve group",
	"Curve smooth",
	"Curve align",
	"Curve erase",
	"Curve undo",
	"Curve hide",
	"Curve normal",
	"Curve drag",
	"Camera menu",
	"Camera rotate up down",
	"Camera dolly zoom",
	"Camera scale rotate",
	"Camera align",
	"Camera center",
	"Camera pan",
	"Camera rotate left right",
	"Camera trackball",
	"Camera undo",
};

// Print menu state using 
void ActionEvents::PrintMenuState() const
{
	cout << "In menu " << s_astrMenu[ m_menuWhich ] << ": " << s_astrMenu[ m_menuSelection ] << "\n";
}

/*
 * Handle when the menu item is actually applied. In all cases, m_menuSelection has which menu item
 *  the mouse is currently over (this is set in SelectMenu or SelectMenuClosest below).
 * The rest of the state is in m_action, in_bWasTap, and the curve selection info (m_*CursorOverCurve*).
 *
 * Note that in_bWasTap is passed in - don't use m_bWasTap. This way logging playback will work correctly. There
 * are also a few menu invocations that force a tap or no tap.
 *
 * Cases:
 *   Mouse passes out of the desired menu option. m_action will be still be set to MENU, which is what
 *    it is set to on a tap that invokes a menu.
 *   There is a tap in the menu item. In this case in_bWasTap will be true, and m_action will be MENU_DRAG.
 *   There was a drag from the menu item. In this case m_action can be one of many things, depending on the menu item the drag started in:
 *        (m_action will be set in MouseDrag once the mosue has moved enough to not be a tap)
 *     MENU_DRAG : that menu item doesn't have a drag option
 *     CAMERA_* : Camera menu item
 *     MENU_SELECT_CURVE : Selecting a curve
 *     MENU_SELECT_CURVE_REGION : Selecting a curve region
 *
 * There are some menu options that require a curve, and some that may have a curve selection. Whether or not there
 * is a curve selection is determined by the OverCurve method. Whether or not there was suppose to be a curve selection for every menu but
 * the curve menu is determined by if MENU_SELECT_CURVE is the current action. It's best to use OverCurve in general,
 * since this actively checks if there is a valid selection.
 *
 * Most of the time the menu selection should be cleared once the menu option has been invoked. The only case this doesn't happen is
 * when there's an extended menu action (bringing up the arrows and picking one, starting a normal/point selection, dragging). 
 *
 * Also clears the currently selected curve data, if we know for sure that we're done with it. This will happen again in a mouse
 * up, but can't hurt to do here.
 *
 * So perhaps this method should be called DoMenuItemOrStartMenuItem...
 *
 * if in_bWasPassThrough == true then in_bWasTap == true
 */
void ActionEvents::DoMenuItem( const bool in_bWasTap, const bool in_bWasPassThrough )
{
    if ( s_bTrace ) cout << "Do menu " << s_astrMenu[ m_menuSelection ] << "\n";
    if ( m_outLogFile.good() ) {
        m_outLogFile << "DoMenu " << s_astrMenu[ m_menuWhich ] << " item " << s_astrMenu[ m_menuSelection ] << "\n";
    }
    
	bool bClearMenuSelection = true;
    switch ( m_menuSelection ) {
		case MENU_LAST_STROKE_COMBINE : 
			PushUndo( m_iCursorOverCurve, StrokeHistory::COMBINE );			
			if ( OverCurve() ) {
				// This last stroke option needs a second curve to combine with. The first will be whatever was the last edited curve, if any
				m_crvNetwork.ReProcessStroke( StrokeHistory::COMBINE, m_iCursorOverCurve );
			}
			break;
		case MENU_LAST_STROKE_MERGE_CLOSE : 
			PushUndo( m_iCursorOverCurve, StrokeHistory::CLOSE );			
			
			m_crvNetwork.ReProcessStroke( StrokeHistory::CLOSE, m_iCursorOverCurve );
			
			break;
		case MENU_LAST_STROKE_MERGE : 
			PushUndo( m_iCursorOverCurve, StrokeHistory::MERGE );			
			
			m_crvNetwork.ReProcessStroke( StrokeHistory::MERGE, m_iCursorOverCurve );
			break;
		case MENU_LAST_STROKE_BLEND : 
			PushUndo( m_iCursorOverCurve, StrokeHistory::BLEND_OVERSTROKE );			
			
			m_crvNetwork.ReProcessStroke( StrokeHistory::BLEND_OVERSTROKE, m_iCursorOverCurve );
			break;
		case MENU_LAST_STROKE_DELETE : 
			PushUndo( m_iCursorOverCurve, StrokeHistory::ERASE );			
			
			m_crvNetwork.ReProcessStroke( StrokeHistory::ERASE, m_iCursorOverCurve );
			break;
		case MENU_LAST_STROKE_NEW_CLOSED : 
			PushUndo( m_iCursorOverCurve, StrokeHistory::FIRST_STROKE_CLOSED );			
			
			m_crvNetwork.ReProcessStroke( StrokeHistory::FIRST_STROKE_CLOSED, m_iCursorOverCurve );
			break;
		case MENU_LAST_STROKE_OVERSTROKE : 
			PushUndo( m_iCursorOverCurve, StrokeHistory::OVERSKETCH );			
			
			m_crvNetwork.ReProcessStroke( StrokeHistory::OVERSKETCH, m_iCursorOverCurve );
			break;
		case MENU_LAST_STROKE_SMOOTH : 
			PushUndo( m_iCursorOverCurve, StrokeHistory::SMOOTH );			
			
			m_crvNetwork.RepeatLastSmooth();
			break;
		case MENU_LAST_STROKE_NEW_CURVE : 
			PushUndo( m_iCursorOverCurve, StrokeHistory::FIRST_STROKE );			
			
			m_crvNetwork.ReProcessStroke( StrokeHistory::FIRST_STROKE, m_iCursorOverCurve );
			break;
        case MENU_CURVE_SNAP :
			PushUndo( m_iCursorOverCurve, StrokeHistory::ADDED_CONSTRAINT );			
            
			// Tap or pass out: Add or remove a point constraint, depending on if one exists ( m_opCursorOverCurvePointConstraint == NULL )
			// Drag, no point constraint exists: Snap curves together
			// Drag, point constraint exists: Move it
			// If there is a drag then we switch edit modes and don't clear the menu selection
            
            // First see if we have a constraint here at all
            m_opCursorOverCurvePointConstraint = OverCurve() ? m_crvNetwork.HasConstraint( m_iCursorOverCurve, m_dCursorOverCurve ) : NULL;
            cout << "Setting PointConstraint II\n";

			if ( m_opCursorOverCurvePointConstraint == NULL ) {
				if ( in_bWasTap && OverCurve() ) {
					m_opCursorOverCurvePointConstraint = m_crvNetwork.NewPinConstraint( m_iCursorOverCurve, m_dCursorOverCurve );
					if ( s_bTrace ) cout << "Adding point constraint\n";
				} else if ( !in_bWasTap && OverCurve() ) {
					if ( s_bTrace ) cout << "Snapping curves together\n";
					SnapCurvesTogether();
				}
			} else if ( in_bWasTap && m_opCursorOverCurvePointConstraint ) {
                if ( m_opCursorOverCurvePointConstraint->IsPoint() ) {
                    m_crvNetwork.RemoveConstraint( m_opCursorOverCurvePointConstraint );
                    m_opCursorOverCurvePointConstraint = NULL;
                    if ( s_bTrace ) cout << "Removing point constraint\n";
                } else {
                    m_opCursorOverCurvePointConstraint->ConvertToPointAndNormalConstraint( m_crvNetwork.Center() );
                    m_opCursorOverCurvePointConstraint->ReEstablish();
                    if ( s_bTrace ) cout << "Converting to point constraint\n";
                }
			} else if ( in_bWasTap == false && m_opCursorOverCurvePointConstraint ) {
                if ( m_action == MENU_SELECT_CURVE ) {
					if ( s_bTrace ) cout << "Snapping curve to existing constraint\n";
					SnapCurvesTogether();
                } else {                    
                    m_action = EDIT_CURVE_POINT_CONSTRAINT;
                    if ( s_bTrace ) cout << "Editing point constraint\n";
                }
			}
			// If point constraints aren't visible, show them
			if ( !g_drawState.m_opUI->m_bShowCurvePointConstraints->value() ) {
				g_drawState.m_opUI->m_bShowCurvePointConstraints->value(1);
				LogGUI();
			}
			// If normal constraints aren't visible, bring them up
			if ( !g_drawState.m_opUI->m_bShowCurveNormalConstraints->value() ) {
				g_drawState.m_opUI->m_bShowCurveNormalConstraints->value(1);
				LogGUI();
			}
			
			ClearCurveSelected();			
			break;
		case MENU_CURVE_GROUP :
			/* TODO: Group curves
			 * Probably the right way to model this is after the CenterCamera code
			 * On a tap or pass through: add or remove the curve to the group
			 * On a drag, add all of the curves the mouse passes over to the group (see CenterCameraRegion code)
			 *
			 */
			
			break;			
        case MENU_CURVE_SMOOTH :
			PushUndo( m_iCursorOverCurve, StrokeHistory::SMOOTH );			

			// Pass through or tap, smooth entire curve, otherwise use selected region
			if ( in_bWasTap ) {
				m_crvNetwork.Smooth( m_iCursorOverCurve, 0.0, 1.0 );
			} else {
				m_crvNetwork.Smooth( m_iCursorOverCurve, StartRegion(), EndRegion() );
			}
			ClearCurveSelected();			
			break;
		case MENU_CURVE_ALIGN :
			// Align the drawing plane to the curve in some manner
			
			// Note - over curve should be true, since this should be invoked from the curve menu
			if ( in_bWasPassThrough == true && OverCurve() ) {
				// They passed out of the menu without tapping
				if ( s_bTrace ) cout << "Snapping drawing plane\n";
				SetDrawingPlaneBestFit();
			} else if ( in_bWasTap == true && OverCurve() ) {
				// They tapped - bring up the alignment arrows
				if ( s_bTrace ) cout << "Picking drawing plane\n";
				m_action = DRAWPLANE_ALIGN;
				CreateAxis( m_ptScreenLast, m_rayMenuDown );
				
			} else if ( m_action == MENU_SELECT_CURVE ) {
				// They dragged - use two points to set drawing plane
				if ( s_bTrace ) cout << "Drawing plane from two points\n";
				SetDrawingPlaneTwoPoints();
			}
			// If the drawing plane isn't visible, bring it up
			if ( !g_drawState.m_opUI->m_bShowDrawingPlane->value() && !g_drawState.m_opUI->m_bShowInflationSrf->value() ) {
				g_drawState.m_opUI->m_bShowDrawingPlane->value(1);
				LogGUI();
			}
            break;
        case MENU_CURVE_ERASE :
			PushUndo( m_iCursorOverCurve, StrokeHistory::ERASE );			
			
			// Pass through or tap, delete the entire curve
			if ( in_bWasTap ) {
				m_crvNetwork.Erase( m_iCursorOverCurve, 0.0, 1.0 );
			} else {
				m_crvNetwork.Erase( m_iCursorOverCurve, StartRegion(), EndRegion() );
			}
			ClearCurveSelected();			
			break;
			
		case MENU_CURVE_UNDO :
			PushUndo( m_iCursorOverCurve, StrokeHistory::UNDO );			

            if ( OverCurve() ) {
                m_crvNetwork.UndoLastAction( m_iCursorOverCurve );
            }
			ClearCurveSelected();			
			break;
			
        case MENU_CURVE_HIDE : 
			m_crvNetwork.ToggleHideCurve( m_iCursorOverCurve );
			ClearCurveSelected();			
			break;
		case MENU_CURVE_NORMAL : {
			PushUndo( m_iCursorOverCurve, StrokeHistory::CHANGED_CONSTRAINT );			

            bool bCreated = false;
            
            // Tap or moved outside - see if over existing constraint
            if ( in_bWasTap && OverCurve() ) {
                m_opCursorOverCurveNormalConstraint = m_crvNetwork.HasConstraint( m_iCursorOverCurve, m_dCursorOverCurve );
            }
            
            
			// Tap or pass through and no normal constraint, add one
			// Tap or pass through and normal constraint, delete it
			// Mouse drag, start editing (switch happens in MouseDrag)
            //  Which type of editing depends on how many curves in constraint
            
            // No matter what, if no normal constraint exists, create one
            if ( m_opCursorOverCurveNormalConstraint == NULL ) {
				if ( OverCurve() ) {
					m_opCursorOverCurveNormalConstraint = m_crvNetwork.NewNormalConstraint( m_iCursorOverCurve, m_dCursorOverCurve );
                    bCreated = true;
					if ( s_bTrace ) cout << "Adding normal\n";
				} else {
					cerr << "ERR: Menu curve normal, but no curve or normal constraint seelcted\n";	
                    break;
				}
            }
            
            // This can happen if they click near a point constraint
            if ( !m_opCursorOverCurveNormalConstraint->IsNormal() ) {
                m_opCursorOverCurveNormalConstraint->ConvertToPointAndNormalConstraint( m_crvNetwork.Center() );
                m_opCursorOverCurveNormalConstraint->ReEstablish();
                bCreated = true;
            }

            // if we had a tap and we didn't create a constraint, delete this one
            if ( bCreated == false && in_bWasTap ) {
				m_crvNetwork.RemoveConstraint( m_opCursorOverCurveNormalConstraint );
				m_opCursorOverCurveNormalConstraint = NULL;
                m_action = NO_ACTION;
				if ( s_bTrace ) cout << "Removing normal\n";
                break;
            } 
            
            // If we're doing a drag,  determine which kind
            if ( !in_bWasTap && m_opCursorOverCurveNormalConstraint ) {
                if ( m_opCursorOverCurveNormalConstraint->NumCurves() == 1 ) {
                    m_action = EDIT_CURVE_NORMAL;
                    CalcGeometryAtStartNormalConstraint();
					if ( s_bTrace ) cout << "Constrained normal rotation\n";
                } else {
                    // Free-form rotation
                    m_action = EDIT_INTERSECTION_NORMAL;
					if ( s_bTrace ) cout << "Free form normal rotation\n";
                }
            }

			// If normal constraints aren't visible, bring them up
			if ( !g_drawState.m_opUI->m_bShowCurveNormalConstraints->value() ) {
				g_drawState.m_opUI->m_bShowCurveNormalConstraints->value(1);
				LogGUI();
			}
			
			break;
        }
        case MENU_CURVE_DRAG : 

			// Tap selects the entire curve (set the region to be the empty set)
			// Otherwise, drag selection
			// Switches to dragging curve
			if ( !OverCurve() ) {
				cerr << "ERR: Menu item curve drag no curve\n";
				m_action = NO_ACTION;
			} else {
                PushUndo( m_iCursorOverCurve, StrokeHistory::TRANSFORM );			
				
				if ( in_bWasTap ) {
					m_dCursorOverCurveTs.first = m_dCursorOverCurveTs.second = m_dCursorOverCurve;
				}
                
				m_action = DRAG_CURVE;
			}
			break;
			
		case MENU_CAMERA : 			break;
			break;
			/// The camera action state switches happen in mouse down or moues drag
		case MENU_CAMERA_ROTATE_UP_DOWN :
		case MENU_CAMERA_SCALE_ROTATE : 
		case MENU_CAMERA_DOLLY_ZOOM : 
		case MENU_CAMERA_PAN : 
		case MENU_CAMERA_TRACKBALL : 
		case MENU_CAMERA_ROTATE_LEFT_RIGHT :
			break;
		case MENU_CAMERA_UNDO : {
			Undo(); 
			break;
		}
		case MENU_CAMERA_CENTER :
			// Passing out of the menu centers on all of the curves
			// Tapping centers the camera on the drawing plane
			// Dragging to a curve centers on that curve
			//   If the cursor returns to the menu after dragging, center on all the curves the mouse passed over
            if ( in_bWasPassThrough == true ) {
				if ( s_bTrace ) cout << "Centering camera: All\n";
				if ( m_crvNetwork.NumCrvs() ) {
					g_drawState.CenterCamera( m_crvNetwork.BoundingBox(), true );
				} else {
					const std::pair<R3Pt, R3Pt> ptLLUR( R3Pt(0,0,0), R3Pt(2,2,2) );
					g_drawState.CenterCamera( ptLLUR, true );
				}
			} else if ( in_bWasTap ) {
				if ( s_bTrace ) cout << "Centering camera: Drawing plane\n";
				g_drawState.CenterCameraOnDrawingPlane( m_crvNetwork.BoundingBox() );
				
            } else if ( ::Length( m_ptScreenLast - m_ptMenuDown ) < g_drawState.MenuSize2D() ) {
				if ( s_bTrace ) cout << "Centering camera: Lasso selection\n";
				CenterCameraRegion();
				
			} else if ( OverCurve() ) {
				if ( s_bTrace ) cout << "Centering camera: Over curve " << m_iCursorOverCurve << "\n";
				g_drawState.CenterCamera( m_crvNetwork.GetCurve(m_iCursorOverCurve).BoundingBox(), false );
			}
            break;
		case MENU_CAMERA_ALIGN :
			/* Align the camera to a particular view direction 
			 * pass through: align to closest of the canonical directions, or if we're in a drag, the drag region
			 * If drag, create a set of axes. Will either be x,y,z (no curve selected) or the Frenet frame of the curve. From
			 * there, the user picks an axis and this routine is called again, this time with action being CAMERA_ALIGN
			 */
			if ( in_bWasPassThrough == true ) {
				// Double click not over a curve
				if ( m_pushAction == DRAG_CURVE && OverCurve() ) {
					if ( IsSubset() ) {
						if ( s_bTrace ) cout << "Align to Drag region\n";
						const std::pair<R3Vec,R3Vec> avecBest = m_crvNetwork.GetCurveRibbon( m_iCursorOverCurve ).GetBestView( m_dCursorOverCurveTs );
						g_drawState.AlignCamera( avecBest.first, avecBest.second );
					} else {
						if ( s_bTrace ) cout << "Align to curve frame\n";
						g_drawState.AlignCamera( m_crvNetwork.GetCurveRibbon( m_iCursorOverCurve ).Frame( m_dCursorOverCurve ).second );
					}
				} else {
					if ( s_bTrace ) cout << "Align to best x,y,z\n";
					g_drawState.AlignCamera( R3Matrix::Identity() );
				}
			} else if ( m_action == MENU_DRAG || m_action == MENU_SELECT_CURVE ) {
				if ( s_bTrace ) cout << "Starting axes selection\n";
				m_action = CAMERA_ALIGN; // Make sure CreateAxis actually does so
				CreateAxis( m_ptScreenLast, m_rayLast );
				bClearMenuSelection = false;
				
			} else if ( m_action == CAMERA_ALIGN ) {
				if ( s_bTrace ) cout << "Align to Selected vector\n";
				if ( m_iAlignCamera != -1 ) {
					if ( s_bTrace ) cout << "Align to Selected vector\n";
					g_drawState.AlignCamera( m_vecAlignCameraLook, m_vecAlignCameraUp );
				}
			}
            break;
			
        default : break;
    }

	if ( bClearMenuSelection == true ) {
		m_menuWhich = NO_MENU_SELECTED;
		m_menuSelection = NO_MENU_SELECTED;
	}
}

/*
 * Pick a menu option. Either pick the middle option (simplest) or
 * figure out which wedge the option is over. Make sure inside of wedge
 */
ActionEvents::MenuSelection ActionEvents::SelectMenu( const R2Pt &in_pt ) const
{
    const double dMenuSize = g_drawState.MenuSize2D();
   
	// Center selection. A little bit off, because this may be an ellipse in camera space, but who cares
	if ( ::Length( in_pt - m_ptMenuDown ) < dMenuSize * cdInteriorCirclePerc ) {
		const ActionEvents::MenuSelection retMenu = (MenuSelection) ( m_menuWhich + 9 );
		if ( s_bTrace && retMenu != m_menuSelection ) cout << "Selecting " << s_astrMenu[ retMenu ] << "\n";;
		return retMenu;
	}
	
	// Counter act any aspect ratio scaling
	const R2Vec vecActual = R3Matrix::Scaling( 1.0, 1.0 / g_drawState.GetCamera().GetAspectRatio(), 1.0 ) * ( in_pt - m_ptMenuDown );

	// Puts rightmost side of top wedge at 0
	const double dAng = 2.0 * M_PI + atan2( vecActual[1], vecActual[0] ) - M_PI / 2.0 + 0.5 * cdSizeWedge;
	const double dWedge = dAng / cdSizeWedge; // which wedge
	const int iWedge = floor( dWedge );
	const double dWedgeLeftOver = (dWedge - iWedge); // amount left over
	const double dPercHalf = 0.5 * (1.0 - cdWedgeWidth / cdSizeWedge);
	
	//cout << "Wedge " << cdSizeWedge << " " << 2.0 * M_PI / 8.0 << "\n";
	//cout << dAng << " " << dWedge << " " << iWedge << " " << dWedge - iWedge << " " << dWedgeLeftOver << " " << dPercHalf << "\n";
	if ( dWedgeLeftOver < dPercHalf || dWedgeLeftOver > 1.0 - dPercHalf ) {
		return NO_MENU_SELECTED;
	}

	const ActionEvents::MenuSelection retMenu = (MenuSelection) ( m_menuWhich + 1 + iWedge % ciNumMenuItems );
	if ( s_bTrace && retMenu != m_menuSelection ) cout << "Selecting " << s_astrMenu[ retMenu ] << "\n";;
	return retMenu;
}

/*
 * Same as above, but leave out the wedge left over part
 */
ActionEvents::MenuSelection ActionEvents::SelectMenuClosest( const R2Pt &in_pt ) const
{
    const MenuSelection curSel = SelectMenu( in_pt );
    if ( curSel != NO_MENU_SELECTED ) {
        return curSel;
    }
    if ( m_menuSelection != NO_MENU_SELECTED ) {
        return m_menuSelection;
    }

	// Counter act any aspect ratio scaling
	const R2Vec vecActual = R3Matrix::Scaling( 1.0, 1.0 / g_drawState.GetCamera().GetAspectRatio(), 1.0 ) * ( in_pt - m_ptMenuDown );

	const double dAng = 2.0 * M_PI + atan2( vecActual[1], vecActual[0] ) - M_PI / 2.0 + 0.5 * cdSizeWedge;
	const double dWedge = dAng / cdSizeWedge;
	const int iWedge = WINminmax( (int) floor( dWedge ), 0, ciNumMenuItems - 1 );
	
	return (MenuSelection) ( m_menuWhich + 1 + iWedge % ciNumMenuItems );
}

/*
 * Draw the menu texture, the lines around the wedges, and the lines around the middle
 * Could probably have the actual menu texture have proper wedges and lines, instead of
 * doing here, but it wasn't worth arguing with photoshop
 */
void ActionEvents::DrawMenuCircle( const DrawState::TextureNames in_menu ) const
{
    const double dMenuSize = g_drawState.MenuSize2D();
	// Counteract aspect ratio scaling
	const R3Matrix matAdj = R3Matrix::Translation( m_ptMenuDown - R2Pt(0,0) ) * R3Matrix::Scaling( 1.0, g_drawState.GetCamera().GetAspectRatio(), 1.0 );
    
	// grey background of menu. Which is ugly, but 
	g_drawState.StartShader( DrawState::SHADOW_SHADING );
	OGLBegin2D( g_drawState.GetCamera().Width(), g_drawState.GetCamera().Height() );

	// The wedges
	const double dAngWedge = cdWedgeWidth * 0.5;
	const double dAngStep = dAngWedge / 8.0 - 1e-16;
	const double dMenuSizeShrunk = dMenuSize * 0.99;
	for ( int iC = 0; iC < ciNumMenuItems; iC++ ) {
		glBegin( GL_POLYGON );
		
		glColor4f( 0.8f, 0.8f, 0.8f, 1.0 );
		glTexCoord2f( 0.0f, 0.0f );	
		glVertex2dv( &( matAdj * R2Pt(0,0) )[0] );
		glNormal3f( 0.0, 0.0, 1.0 );
		
		const double dAng = M_PI / 2.0 + iC * cdSizeWedge;
				
		const R2Vec vec2( cos( dAng - dAngWedge), sin( dAng - dAngWedge) );
		glVertex2dv( &(matAdj * ( vec2 * dMenuSize * (cdInteriorCirclePerc * 1.01) ) )[0] );
		glVertex2dv( &(matAdj * ( vec2 * dMenuSizeShrunk ))[0] );
		for ( double dT = dAng - dAngWedge; dT <= dAng + dAngWedge + dAngStep * 0.5; dT += dAngStep ) {
			const R2Vec vec( cos( dT ), sin( dT ) );
			glVertex2dv( &(matAdj * ( vec * dMenuSizeShrunk ) )[0] );
		}
		
		const R2Vec vec( cos( dAng + dAngWedge), sin( dAng + dAngWedge) );
		glVertex2dv( &(matAdj * ( vec * dMenuSizeShrunk ) )[0] );
		glVertex2dv( &(matAdj * ( vec * dMenuSize * (cdInteriorCirclePerc * 1.01) ))[0] );
		glEnd();
	}
	// The circle in the middle
	const double dTDiv = 2.0 * M_PI / 31.0;
	glBegin( GL_POLYGON );
	for ( int i = 0; i < 31; i++ ) {
		const R2Vec vec( cos( dTDiv * i ), sin( dTDiv * i ) );
		glVertex2dv( &(matAdj * (vec * dMenuSizeShrunk * cdInteriorCirclePerc) )[0] );
	}
	glEnd();
	OGLEnd2D();
	
	g_drawState.EndShader();
	
	/* Draw the actual textured menu as a circle with the texture applied to it
	 * Texture has alpha value 0 where there's no text/drawing */
	g_drawState.StartMenuShader( in_menu );

	OGLBegin2D( g_drawState.GetCamera().Width(), g_drawState.GetCamera().Height() );
		
	glBegin( GL_TRIANGLE_FAN );
	
	glColor4f( 0.0f, 0.0f, 0.0f, 1.0 );
	glTexCoord2f( 0.5f, 0.5f );	
	glVertex2dv( &( matAdj * R2Pt(0,0) )[0] );
	
	for ( int i = 0; i < 32; i++ ) {
		const R2Vec vec( cos( dTDiv * i ), sin( dTDiv * i ) );
		glTexCoord2f( 0.5 + vec[0] * 0.5, 0.5 - vec[1] * 0.5 );	
		
		glVertex2dv( &(matAdj * (vec * (dMenuSize)))[0] );
	}
	glEnd();
	OGLEnd2D();

	g_drawState.EndShader();
	    
	glDisable( GL_DEPTH_TEST );
	/* Selection circles */
	g_drawState.StartShader( DrawState::SCREENSPACE_SHADING );
    
	glLineWidth( 3.0f );
    
	glEnable( GL_LINE_SMOOTH );
    glHint( GL_LINE_SMOOTH_HINT, GL_NICEST );
    
    if ( (m_menuSelection - m_menuWhich) == ciNumMenuItems + 1 ) {
		glColor4f(0.25f, 0.25f, 0.75f, 1.0f);
	} else {
		glColor4f(0.65f, 0.65f, 0.5f, 1.0f);
	}
	glTexCoord2f( 0.0f, 1.0f );	
    
	// The little circle in the middle
	OGLBegin2D( g_drawState.GetCamera().Width(), g_drawState.GetCamera().Height() );
	
	glBegin( GL_LINE_LOOP );	
	for ( int i = 0; i < 31; i++ ) {
		const R2Vec vec( cos( dTDiv * i ), sin( dTDiv * i ) );
		glVertex2dv( &(matAdj * (vec * dMenuSize * cdInteriorCirclePerc) )[0] );
	}
	glEnd();
    
	// The wedges
	for ( int iC = 0; iC < ciNumMenuItems; iC++ ) {
        if ( (m_menuSelection - m_menuWhich - 1) == iC ) {
			glColor4f(0.25f, 0.25f, 0.75f, 1.0f);
        } else {
            glColor4f(0.65f, 0.65f, 0.5f, 1.0f);
        }
		glBegin( GL_LINE_STRIP );
		const double dAng = M_PI / 2.0 + iC * cdSizeWedge;

		
		const R2Vec vec2( cos( dAng - dAngWedge), sin( dAng - dAngWedge) );
		glVertex2dv( &(matAdj * ( vec2 * dMenuSize * cdInteriorCirclePerc ) )[0] );
		glVertex2dv( &(matAdj * ( vec2 * dMenuSize ))[0] );
		for ( double dT = dAng - dAngWedge; dT <= dAng + dAngWedge + dAngStep * 0.5; dT += dAngStep ) {
			const R2Vec vec( cos( dT ), sin( dT ) );
			glVertex2dv( &(matAdj * ( vec * dMenuSize ) )[0] );
		}

		const R2Vec vec( cos( dAng + dAngWedge), sin( dAng + dAngWedge) );
		glVertex2dv( &(matAdj * ( vec * dMenuSize ) )[0] );
		glVertex2dv( &(matAdj * ( vec * dMenuSize * cdInteriorCirclePerc ))[0] );
		
		glEnd();
	}
	OGLEnd2D();
	
	g_drawState.EndShader();
}


/* If an active menu, then draw menu */
void ActionEvents::DrawMenu() const
{
	// Don't draw menu during snap, since it just obscures things
	if ( m_menuWhich == MENU_CURVE && m_menuSelection == MENU_CURVE_SNAP && m_action == MENU_SELECT_CURVE ) {
		return;
	}
	
    if ( m_action == MENU || m_action == MENU_SELECT_CURVE || m_action == MENU_DRAG ) {
	
		if ( m_menuWhich == MENU_LAST_STROKE ) {			
			DrawMenuCircle( DrawState::MENU_STROKE );
		} else if ( m_menuWhich == MENU_CAMERA ) {
			DrawMenuCircle( DrawState::MENU_CAMERA );
		} else if ( m_menuWhich == MENU_CURVE ) {
			DrawMenuCircle( DrawState::MENU_CURVE );		
		}
	}	
}    


