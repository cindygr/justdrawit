/*
 *  StrokeHistory.h
 *  SketchCurves
 *
 *  Created by Cindy Grimm on 1/20/11.
 *  Copyright 2011 Adobe Corp.. All rights reserved.
 *
 */

#ifndef _STROKE_HISTORY_DEFS_H_
#define _STROKE_HISTORY_DEFS_H_

#include <utils/Rn_Defs.H>
#include <utils/Crv_HermiteTC.H>
#include <utils/Rn_Line_seg.H>

#include "Curve.h"
#include "CurveOverlap.h"
#include "ScreenCurve.h"

/* Record how this stroke was added into the composite curve.
 * Record actions that edit the curve (drag, smooth, erase)
 *
 * This class does the actual joining of the 3D stroke to the composite curve
 *
 * Basic idea is to find a hermite curve that best joins the two
 * curves where they need to merged/joined. Then trim the original
 * and stroke curves as needed, and paste the sampled hermite curve in.
 *
 * The join curve is found by searching over possible join points. The possible join
 * regions are computed (and stored) in the CurveOverlap class. Scoring is basically the
 * same as for scoring joins/merges in CurveOverlap; this is just in 3D
 *
 * For Multiple stroke overstrokes: Take the strokes making up the
 * overstroke and merge/join them together. Then take this combined curve
 * and add it to the original curve using an overstroke.
 *
 * For an oversketch_start, store the current merged curve in m_crvIntermediate
 *
 * For blend curves, extend and blend to each other before applying. Blended curve
 * is stored in m_crvFinal
 *
 * The join regions are marked for smoothing and some amount of smoothing applied
 * based on how nice the join/merge is.
 *
 * 12/11 Added a complete history to support undo. 
 *  Changes due to constraints are now logged (adding, removing, changing)
 *    Store the pin hash id
 *  Store the curve and active pin ids for erase (optionally created curve, pins that
 *     apply to the curve and may be erased)
 *  Store the curve and active pin ids for the second curve for combine (which curve is
 *     being deleted and the pins that belong to it)
 *  Undo is stored as yet another action. Has a counter in it that says how far back
 *     down the stack to go, so that you can do multiple undos and redos
 */
class StrokeHistory
{
public:
	typedef enum {
		FIRST_STROKE = 0,
		FIRST_STROKE_CLOSED, // Made a closed curve
		BLEND_SCRATCH,       // Scratched back and forth
		BLEND_OVERSTROKE,    // New stroke exactly overlapping old stroke 
		MERGE,               // Continuing curve off of the end of the curve
		MERGE_REVERSE,       // Continuing curve off of the start of the curve
		JOIN,                // Continuing curve off of the end of the curve (gap)
		JOIN_REVERSE,        // Continuing curve off of the start of the curve (gap)
		CLOSE,               // Stroke continues off of end of curve and joins start of curve at end. Also overstroke over joint in closed curve
		OVERSKETCH,          // Replace part of curve
		OVERSKETCH_START,    // Also replace part of curve; more smoothing applied
		COMBINE,             // Two merges on two curves
		REFLECT,             // This curve was made by reflecting another one
		TRANSFORM,           // Transform some or all of a curve (rotate, scale, translate)
		SMOOTH,              // Smooth
		ERASE,               // Erase
        ADDED_CONSTRAINT,    // For undo. Save constraint and which constraint it was (hash id)
        REMOVED_CONSTRAINT,
        CHANGED_CONSTRAINT,
        UNDO,                // Store this as an action
		NO_ACTION
	} MergeType;
private:
	
	MergeType m_mergeType; // Action
    
    /* For undo - basically a counter that gets incremented every change (mouse down to mouse up) */
    int m_iTimeStamp;    
	
	/* Fit to the start/end of the stroke. Also used for end point selection (Stroke menu) */
	R3Line_seg m_segStroke;
	
	/* Merge, join, and overstroke need a 3D blend curve (two points). 
	 * For operations that have two joins this will be a four point curve, first join in first two
	 * points, second in second join
	 * Not really used for anything; just store it here in case. */
	CRVHermite3 m_crvJoin;
	
	/* Where the join curve meets the current and previous curve 
	 * Somewhat overloaded - used to pass back actual join points from FindJoinCurve
	 * (m_dTJoinPrev.first and m_dTJoinNext.first)
	 * Then set again in SmoothJoin to indicate region of join on composite curve
	 *  - used on a stroke menu smooth to determine what part of the curve to smooth
	 *  - for a smooth operation, stores the t values of the smooth so that it can be applied again
	 */
	std::pair<double, double> m_dTJoinPrev, m_dTJoinNext;
	
	/* The 3D curve that is the result of the operation. If this is the topmost element of the
     * stack than this curve should be the same as the curve drawn.
     * This curve is also used to reset the curve position for a partial drag.
     * Also used in undo. */
	Curve m_crvFinal;
    
	/* This curve is also used to reset the curve position for a partial drag.
     * Gets set in StartTransform. */
	Curve m_crvDragTemp;
    
	/* Save the 3D version of the stroke curve. This is the curve that gets drawn as a fragment. 
     * TODO: Clip this curve so that if it is too far from actual final curve, don't draw all of it */
	Curve m_crvStroke3D;
	
	/* Information for re-constructing the stroke. On a re-process stroke, CurveNetwork will grab
	 * this screen curve for the re-process. */
	ScreenCurve m_crvStroke2D; // original, unmodified
	R3Plane     m_planeDraw; // Drawplane or view plane used when drawing
    
    /* Undo information
     * Information on pin constraints/curve groups - which one was edited.
     * Used for which pin contraint was edited
     * Used for which curve was created/deleted on erase/combine */
    int m_iCurveHashIdOrig, m_iCurveHashIdNew, m_iUndoLevel;
    Array<int> m_aiPinHashId;
	
	// Set m_segStroke to be the end points of the stroke
	void SetSeg( const Curve &in_crvStroke );
	
	/* Search over the t value ranges to find the best join curve. Sets
	 * m_crvJoin and m_dTJoinPrev/Next. Returns the score of the join for use in smoothing later */
	CRVHermite3 FindJoinCurve( const Curve &in_crvComposite, const Curve &in_crvStroke, 
							   const std::pair<double,double> &in_adCrvRange, const std::pair<double,double> &in_adStrokeRange,
         					   const bool in_bJoin,
 							   double &out_dScore ) ;
	
	/* From the end of the curve to the start of the stroke (merge or join) */
	void SetJoinCurve( const Curve &in_crvComposite, const Curve &in_crvStroke, 
					   const std::pair<double,double> &in_adCrvRange, const std::pair<double,double> &in_adStrokeRange,
					   const bool in_bJoin,
					   const double in_dDistApart, 
                       Curve &out_crv );
	
	/* From the start of the curve to the end of the stroke (merge or join) */
	void SetJoinCurveReverse( const Curve &in_crvComposite, const Curve &in_crvStroke, 
					          const std::pair<double,double> &in_adCrvRange, const std::pair<double,double> &in_adStrokeRange,
							  const bool in_bJoin,
							  const double in_dDistApart,
                              Curve &out_crv );
	
	/* Resample the input 3D stroke curve, transfering the t values over */
	std::pair<double,double> ResampleStroke( const std::pair<double,double> &in_ad, const Curve &in_crvOrig, const Curve &in_crvNew ) const;
	
	/* Create a Curve from a CRVHermite3, using the default curve sampling rate */
	Curve ResampleJoinCurve( const CRVHermite3 &in_crv ) const;
	
	/* Apply smoothing to the join, amount based on score */
	void SmoothJoin( const CRVHermite3 &in_crv, const double in_dScore, const bool in_bFirst, Curve &io_crv ) ;
	
	/* This is the blend code. Project the point onto all of the curve fragments */
	static std::pair<R2Pt, R2Vec> Project( const R2Pt &in_pt, 
										   const std::vector< ScreenCurve > &in_acrv, 
										   const std::vector< std::pair<double,double> > &in_adTBlends,
										   const double in_dDist );
	/* Do one round of projecting the points of the curves onto the curves, then blending the curves together */
	static std::vector< ScreenCurve > ProjectAndBlendCurves( const std::vector< ScreenCurve > &in_acrv, const double in_dDist );
	
public:
	/**@name Access */
	//@{
	/// What action this was
    const MergeType &GetMergeType() const { return m_mergeType; }
	/// The 2D curve used to make the composite curve (if any)
	const ScreenCurve &Stroke2D() const { return m_crvStroke2D; }
	/// The 2D curve used to make the composite curve, promoted to 3D
	const Curve &Stroke3D() const { return m_crvStroke3D; }
	/// Intermediate result (drag, blend, multi-stroke)
	const Curve &Final() const { return m_crvFinal; }
	/// Are we one of the two blend types?
	bool IsBlend() const { return m_mergeType == BLEND_SCRATCH || m_mergeType == BLEND_OVERSTROKE; }
    /// Was this action one that actually made/has a stroke?
    bool WasStroke() const ;
    /// Was this a constraint action?
    bool WasConstraint() const;    
	
	/// Where does the stroke start in 3D space? (for the stroke menu circles)
	const R3Pt &StrokeStartPt() const { return m_segStroke.P1(); }
	/// Where does the stroke end in 3D space? (for the stroke menu)
	const R3Pt &StrokeEndPt() const { return m_segStroke.P2(); }
	/// Where does the smooth region start in 3D space? (for the smoothing circles)
    double SelectRegionStartT() const { return m_dTJoinPrev.first; }
	/// Where does the smooth region end in 3D space? (for the smoothing circles)
    double SelectRegionEndT() const { return m_dTJoinPrev.second; }
    /// Where does the overstroke start?
    double OverstrokeStart() const { return m_dTJoinPrev.first; }
    /// Where does the overstroke start?
    double OverstrokeEnd() const { return m_dTJoinNext.second; }
	/// Where draw plane was used to make this stroke or edit?
	const R3Plane &DrawPlane() const { return m_planeDraw; }
    /// Which curve (for erase, combine)?
    int CurveHashIdOrig() const { return m_iCurveHashIdOrig; }
    /// Which curve (for erase, combine)?
    int CurveHashIdNew() const { return m_iCurveHashIdNew; }
    /// Undo level
    int UndoLevel() const { return m_iUndoLevel; }
    const Array<int> &PinHashId() const { return m_aiPinHashId; }
    /// Time stamp of action
    int GetTimeStamp() const { return m_iTimeStamp; }
    //@}
		
	/**@name Operations */
	//@{
	/// This is destructive - it will delete whatever's in there. Sets merge type to FIRST_STROKE
	void StartNew( const Curve &in_crv );
	/// This is destructive - it will delete whatever's in there. Sets merge type to FIRST_STROKE_CLOSED
	void StartNewClosed( const CurveOverlap &in_overlap, const Curve &in_crv, Curve &out_crv );	
	/// Set the stroke curves, merge type, plane, t values, and seg. Doesn't do blend, just stores blend info
	void BlendOverstroke( const ScreenCurve &in_screenCurve );
	/// Set the stroke curves, merge type, plane, t values, and seg. Doesn't do blend, just stores blend info
	void BlendScratch( const ScreenCurve &in_screenCurve, const R3Plane &in_plane );
	/// Close the gap between the curve and the stroke. 
	void Join( const CurveOverlap &in_overlap, const Curve &in_crvComposite, const Curve &in_crvStroke, Curve &out_crv );
	/// Close the curve
	void Close( const CurveOverlap &in_overlap, const Curve &in_crvComposite, const Curve &in_crvStroke, Curve &out_crv );
	/// Merge this stroke onto the end
	void Merge( const CurveOverlap &in_overlap, const Curve &in_crvComposite, const Curve &in_crvStroke, Curve &out_crv );
	/// Overstroke, partial or full
	void Overstroke( const CurveOverlap &in_overlap, const Curve &in_crvComposite, const Curve &in_crvStroke, const bool in_bIsPartial, Curve &out_crv );
	/// Two merges to combine the curves
	void Combine( const CurveOverlap &in_overlapE1, const Curve &in_crvCompositeE1, 
                  const CurveOverlap &in_overlapE2, const Curve &in_crvCompositeE2, 
                  const Curve &in_crvJoin, 
                  const int in_iHashId2, 
                  const Array< PointConstraint * > &in_aopPins,
                  Curve &out_crv );
	/// Set m_crvComposite. Call before Drag or Transform is called
	void StartTransform( const Curve &in_crvComposite );
	/// Do drag
	void Drag( const R3Plane &in_plane, const R3Vec &in_vec, const double in_dTDrag, const std::pair<double, double> & in_dTBracket, Curve &out_crv );
	/// Do transform. in_bReset == true means set the curve to the saved composite curve first
	void Transform( const R4Matrix &in_mat, Curve &out_crv, const bool in_bReset );
	/// Erase part of the curve. May make two curves (second passed back in StrokeHistory)
	void Erase( const double in_dTStart, const double in_dTEnd, 
                const int in_iOrigHashId, const int in_iLHSHashId,
                const Array< int > &in_aiPins,
                Curve &io_crvOrig,                            
                StrokeHistory & out_shLHS,
                Curve         & out_crvLHS  );
	/// Smooth
	void Smooth( const double in_dTStart, const double in_dTEnd, const int in_iNLoops, Curve &io_crvComposite );
	/// And again, using saved smooth t values
	void RepeatSmooth( Curve &io_crvComposite );
    /// Mark that we did an undo. Store how far back we went, so can go back further if needed
    void Undo( const int in_iLevel, const Curve &in_crv );
	/// Just set the screen curve and the plane
	void SetConstructionHistory( const ScreenCurve &in_crvScreen, const R3Plane &in_plane );
	/// Do blend and return result. No data saved
	static ScreenCurve Blend( const std::vector< ScreenCurve > &in_acrvs, const double in_dDist );
	//@}

    /**@name Store constraint operations */
	//@{
    ///
    void AddedConstraint( const int in_iId, const Curve &in_crvNew );
    ///
    void RemovedConstraint( const int in_iId, const Curve &in_crvNew );
    ///
    void ChangedConstraint( const int in_iId, const Curve &in_crvNew );
    /// Use only for when the curve has been dragged because of pin constraints
    void CurveChanged( const Curve &in_crvNew ) { m_crvFinal = in_crvNew; }
    //@}
    
	StrokeHistory & operator=( const StrokeHistory &in_sh );
	StrokeHistory( const StrokeHistory &in_sh ) { *this = in_sh; }
	StrokeHistory() ;
	~StrokeHistory(){}

	void Print() const;
    void PrintHistory() const;

	static std::string GetMergeTypeString( const StrokeHistory::MergeType in_mt );
	void Write( ofstream &out ) const;
	void Read( ifstream &in );
};

#endif
