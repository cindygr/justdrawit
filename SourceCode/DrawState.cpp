/*
 *  DrawState.cpp
 *  SketchCurves
 *
 *  Created by Cindy Grimm on 2/24/2011
 *  Copyright 2011 Adobe corp. All rights reserved.
 *
 */


#include "DrawState.h"
#include <OpenGL/OGLShader.h>
#include "UserInterface.h"
#include "ParamFile.h"

DrawState g_drawState;

const bool s_bTrace = false;

// Shadow box passed in from action events
void DrawState::SetShadowBox( ShadowBox *in_opShadowBox ) 
{ 
	m_opShadowBox = in_opShadowBox; 
	m_opShadowBox->UpdateParams();
}


/* param file has been updated - re-read */
void DrawState::UpdateParams()
{
	if ( m_opShadowBox ) {
		m_opShadowBox->UpdateParams();
	}
}

/* Return either the drawing plane or the view plane if the drawing plane is not visible */
R3Plane DrawState::DrawingPlane() const
{
	const R3Plane plane( R3Pt(1,1,1), R3Vec(0,0,1) );
	if ( !m_opUI ) {
		return plane;
	}
	if ( !m_opShadowBox || !m_opUI->m_bShowDrawingPlane->value() ) {
		return R3Plane( m_camera.At(), m_camera.Look() );
	}
	return m_opShadowBox->DrawingPlane();
}

double DrawState::StripWidth() const
{
	if ( !m_opUI ) {
		return 1.0;
	}
	
	return m_opUI->m_dStripWidth->value() * m_dScreenSize;
}

double DrawState::StripHeight() const
{
	if ( !m_opUI ) {
		return 1.0;
	}
	
	return m_opUI->m_dStripHeight->value() * m_dScreenSize;
}

double DrawState::LineWidth() const
{
	if ( !m_opUI ) {
		return 1.0;
	}
	
	return m_opUI->m_dLineWidth->value();
}

double DrawState::ScreenStripeWidth() const
{
	if ( !m_opUI ) {
		return 0.1;
	}
	
	const double dPixelSize = 2.0 / GetCamera().Width();
	return m_opUI->m_dStripWidth->value() * dPixelSize * g_paramFile.GetDouble( "ScreenStripeWidthInPixels" );
}

double DrawState::SelectionSize() const
{
	if ( !m_opUI ) {
		return 1.0;
	}
	
	return m_opUI->m_dSelectionSize->value() * m_dScreenSize;
}

double DrawState::ScreenSelectionSize() const
{
	if ( !m_opUI ) {
		return 1.0;
	}
	
	const double dPixelSize = 2.0 / GetCamera().Width();
	return m_opUI->m_dSelectionSize->value() * dPixelSize * g_paramFile.GetDouble( "ScreenSelectWidthInPixels" );
}

double DrawState::CurveSpacingSize() const
{
	return g_paramFile.GetDouble( "CurveSpacingPercentage" ) * m_dScreenSize;
}

double DrawState::MenuSize2D() const
{
	if ( m_opUI ) {
		const double dPixelSize = 2.0 / GetCamera().Width();
		return 0.5 * g_paramFile.GetDouble("MenuSizeInPixels") * dPixelSize;
	}
	return 0.1;
	
}

double DrawState::MenuSize3D() const
{
	if ( m_opUI ) {
		const R3Plane plane( GetShadowBox().GetCenter(), m_vecView );
		const R3Ray ray1( GetCamera().From(), GetCamera().RayFromEye(R2Pt(-1.0,0)) );
		const R3Ray ray2( GetCamera().From(), GetCamera().RayFromEye(R2Pt(1.0,0)) );
		
		const double dWidthFilmPlane = ::Length( plane.IntersectRay( ray1 ) - plane.IntersectRay(ray2) );
		return 0.5 * MenuSize2D() * dWidthFilmPlane;
	}
	return 0.1;
	
}

bool DrawState::ProjectToPlane() const
{
	if ( m_opUI ) {
		if ( m_opUI->m_bShowDrawingPlane->value() ) {
			return true;
		} else {
			return false;
		}
	}
	return false;
}

string DrawState::GetBaseDirectory( const string &in_strSubDir ) const
{
    const string strTry1 = "./ShaderData/";
    const string strTry2 = string( "./" ) + in_strSubDir;
    
    if ( DirectoryExists( strTry2 ) ) {
        return strTry2;
    }
    if ( DirectoryExists( strTry1 ) ) {
        return strTry1;
    }
    cerr << "ERR: DrawState::GetBaseDirectory: " << " Couldn't find resource files (ShaderData)\n";
    
    return "./";
}


DrawState::DrawState()
: m_opUI(NULL),
  m_opShadowBox(NULL),
 m_opShader(NULL),
m_vecView(0,0,1), m_vecUp(0,1,0), m_vecRight(1,0,0),
m_bInShader(false),
m_dPixelWidth(0.01),
m_dPixelWidth2D(0.01),
m_drawingStyle( SCREEN_STROKES )
{    
	ZeroCameraControls();		
}

