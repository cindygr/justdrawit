/*
 *  ScreenCurve.h
 *  SketchCurves
 *
 *  Created by Cindy Grimm on 9/8/10.
 *  Copyright 2010 Washington University in St. Louis. All rights reserved.
 *
 */


#ifndef _SCREEN_CURV_DEFS_H_
#define _SCREEN_CURV_DEFS_H_

#include <utils/Rn_Defs.H>
#include <utils/R3_Plane.H>
#include <utils/Rn_Line_seg.H>
#include <utils/Rn_Sphere.H>
#include <utils/Utils_Color.H>
#include <boost/dynamic_bitset.hpp>

/*
 * Two-dimensional curve, arc-length parameterized. 
 * Originally supported gestures; that code is still here, but is no longer
 * used.
 * Has many of the functions that the Curve class has (eg closest point)
 */
class ScreenCurve {
public:
	// What gesture
    typedef enum {
        NO_GESTURE = 0,
        ERASE,  // Sine wave
        SQUIGGLE, // Back and forth over self
        LINE,   // Line
        CIRCLE,
        TAP
    } Gesture;
    
private:
	/// The raw points
    Array<R2Pt> m_apt;
	
	/// ith value has the length of the curve from the start to the ith point
	/// Used with m_dLength to give arc-length parameterization
	/// Lower left and upper right are the bounding box
	/// Start/end time records the time it took to make the curve. Not really
	/// used anymore, so I'm not sure if it's being set correctly
	/// All of these are always up to date if you use AddPoint to add points to the curve
    Array<double> m_adDistAlong;
	double m_dLength;
    R2Pt m_ptLowerLeft, m_ptUpperRight;
    double m_dStartTime, m_dEndTime;
    
	/// Derived data (legacy). Basically the probability of the curve being
	/// one of the gestures, and a list of possible gestures this curve might be
    double     m_dErrLine, m_dErrCircle, m_dErrSquiggle, m_dErrErase;
    R2Line_seg m_segGesture;
    R2Sphere   m_circGesture;
    Array<Gesture> m_aGesture;
    
	/// Legacy. These probably still work, but there's some data set up that has to 
	/// happen (see GetGesture) before you call them directly
    bool IsLine( const double in_dDist ) const;
    bool IsCircle( const double in_dDist ) const;
    bool IsSquiggle( const double in_dDist, const boost::dynamic_bitset<> &in_abCorners ) ;
    bool IsErase( const double in_dDist, const boost::dynamic_bitset<> &in_abCorners ) ;

public:
	/**@name Accessors */
	//@{
	///
    int NumPts() const { return m_apt.num(); }
	///
    const R2Pt &Pt( const int in_i ) const { return m_apt.clamp(in_i); }
    /// center point
    R2Pt CenterPoint() const { return Lerp( m_ptLowerLeft, m_ptUpperRight, 0.5 ); }
    
	/// in_dT in [0,1], returns a point on the curve
    R2Pt operator()( const double in_dT ) const;
	/// Normalized to be unit length. Uses segment direction as tangent
	R2Vec Tangent( const double in_dT ) const;
    /// Summed distance from the start of the curve to point i
	double DistAlong(const int in_iPt) const { return m_adDistAlong.clamp(in_iPt); }
	/// Arc-length distance. First point is at 0.0, last point is at 1.0
    double PercAlong(const int in_iPt) const { return RNIsZero( m_dLength ) ? 0.0 : m_adDistAlong[in_iPt] / m_dLength; }
	/// Summed length of the curve
    double Length() const { return m_dLength; }
	/// Only valid if time was passed in to AddPoint
    double Time(const int in_iPt) const { return m_dStartTime + PercAlong(in_iPt) * m_dEndTime; }
	/// Returns the segment (i, i+1) and percentage along that segment [0.0,1.0) for the given input t value in_dT in [0,1]
	std::pair<int,double> BinarySearch(const double in_dT) const;
	/// Returns the point index that is less than or equal to in_dT (in [0,.., NumPts() - 1])
	int Floor(const double in_dT) const;
	/// Returns the point index that is greater than or equal to in_dT (in [0,.., NumPts()])
	int Ceil(const double in_dT) const;
	/// Find the closest point on the curve. Returns a value in [0,1], and the distance from curve(t) to in_pt
    double ClosestPointTValue( const R2Pt &in_pt, double &out_dDist ) const;
	/// Is the last point the same as the first?
	bool IsClosed() const;
	/// Is it more horizontal than vertical?
    bool Horizontal() const;
	//@}

	/**@name Edit the curve */
	//@{
	///
    void AddPoint( const R2Pt &in_pt, const double in_dTime = -1.0 );
	///
    void Clear();
	/// Average with neighbors smoothing. End points will not move. Note: does not recalc m_adDistAlong, so DistAlong and PercAlong may be slighly off
	void Smooth(const int in_iLoops);
	/// Same as above, but only filter the ends of the curve
	void SmoothEnds(const int in_iLoops);
    ///
    void MakeUnitLength( const bool in_bRecenter ); // Scale around center until unit length
	//@}

	/**@name Copy and change the curve */
	//@{
	/// Copy (doesn't copy gesture information)
    const ScreenCurve &operator=( const ScreenCurve &in_crv );
	/// Adds new points between widely spaced ones to ensure spacing is at least in_d. Does not move original points. 
	ScreenCurve FillGaps( const double in_d ) const;
	/// Reverse the order
	ScreenCurve Reverse( ) const;
	//@}
	
	
	/**@name Information about the curve (gesture-related stuff) */
	//@{
	/// Curvature
	double Curvature( const int in_iPt ) const;
    /// Fit Circle version
    double CurvatureCircle( const int in_iPt ) const;
	/* Find all the corners. 
	 * in_dDist is used to cull out duplicate corners and for how big a section of the curve to consider
	 * angle is how big the angle has to be to consider it a corner
	 * Returns: a boolean array which is 1 for all points which are corners
	 */
    boost::dynamic_bitset<> Corners( const double in_dDist, const double in_dAng =  M_PI / 3.0 ) const;
	/// Is it a tap? Ie, small number of points and no travel
    bool IsTap(  ) const;
	/// Legacy. Try all gestures and return a sorted list of the possible ones
    const Array<Gesture> &GetGesture( const double in_dDist = 0.1 ) ;
	//@}
    
	    
    ScreenCurve( const ScreenCurve &in_crv ) { *this = in_crv; }
    ScreenCurve() { Clear(); }
    ~ScreenCurve() { }
	
	void Write( ofstream &out ) const;
	void Read( ifstream &in );

	/// Default draw is as a screen-aligned strip, stroke shading
	void Draw() const;

};

#endif