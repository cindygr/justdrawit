/*
 *  DrawData.cpp
 *  SketchCurves
 *
 *  Created by Cindy Grimm on 6/29/12.
 *  Copyright 2012 Washington University in St. Louis. All rights reserved.
 *
 */

#include "DrawData.h"
#include "DrawState.h"
#include "ParamFile.h"

/* Store all this stuff so we don't have to keep getting it back again */
UTILSColor DrawData::s_colSelect( UTILSColor::YELLOW );
UTILSColor DrawData::s_colActive( UTILSColor::RED );
UTILSColor DrawData::s_colInactive( UTILSColor::GREY );
UTILSColor DrawData::s_colHidden( UTILSColor::GREY );
UTILSColor DrawData::s_colPointConstraint( UTILSColor::BLUE );
UTILSColor DrawData::s_colBoundary( UTILSColor(0.0, 0.8, 0.9) );
R4Pt_f DrawData::s_colFragment( 0.0f, 0.0f, 0.0f, 1.0f );
double DrawData::s_dTransparencyHidden = 0.2;
int DrawData::s_iTubeDivs = 16;
int DrawData::s_iRibbonDivs = 4;
double DrawData::s_dTaperPercentage = 4.0;

/* Called when param file re-read */
void DrawData::ParamFileUpdated()
{
	s_colSelect = g_paramFile.GetColor("CurveColorSelectedRegion");
	s_colActive = g_paramFile.GetColor("CurveColorActive");
	s_colInactive = g_paramFile.GetColor("CurveColorInactive");
	s_colHidden = g_paramFile.GetColor("CurveColorHidden");
	s_colPointConstraint = g_paramFile.GetColor("ColorPointConstraints");
	s_colBoundary = g_paramFile.GetColor("IntersectionSrfCurveCol");
    s_dTaperPercentage = g_paramFile.GetDouble("TaperPercentage");
	
	const UTILSColor col = g_paramFile.GetColor("CurveColorFragment");
	for ( int i = 0; i < 3; i++ ) {
		s_colFragment[i] = (float) col[i];
	}
	s_colFragment[3] = 1.0f;
	
	s_dTransparencyHidden = g_paramFile.GetDouble("CurveTransparencyHidden");
}

DrawData &DrawData::operator=( const DrawData &in_data ) 
{
    cerr << "Warning: Should not be calling DrawData::operator=\n";
    
	m_bHidden = in_data.m_bHidden;
	m_bActive = in_data.m_bActive;
	m_bCastShadow = in_data.m_bCastShadow;
	m_bUpdateColor = in_data.m_bUpdateColor;
	m_bUpdateScale = in_data.m_bUpdateScale;
	m_dStripWidth = in_data.m_dStripWidth;
	m_dStripHeight = in_data.m_dStripHeight;
	m_vecView = in_data.m_vecView;
	m_vecUp = in_data.m_vecUp;
	
	return *this;
}

DrawData::DrawData( )
: m_bHidden( false ),
m_bActive( false ),
m_bCastShadow( true ),
m_dStripWidth(0.0),
m_dStripHeight(0.0),
m_vecView( 0,0,0 ),
m_vecUp(0,0,0)
{
    m_bUpdateColor = m_bUpdateScale = true;
}
