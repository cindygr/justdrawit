/*
 *  InflationSurface.h
 *  SketchCurves
 *
 *  Created by Cindy Grimm on 4/27/11.
 *  Copyright 2011 Adobe Corp. All rights reserved.
 *
 */

#ifndef _INFLATION_SURFACE_DEFS_H
#define _INFLATION_SURFACE_DEFS_H

#include "CurveNetwork.h"

/*
 * Make a temporary inflation surface to draw on from two "curves"
 */
class InflationSurface {
public:
	// Mode - are we editing the left curve, the right, or drawing on the surface?
	// State kept in ActionEvents
    typedef enum {
        NO_EDIT = -1,
        EDIT_LEFT_CURVE,
        EDIT_RIGHT_CURVE,
        DRAW_CURVE,
    } EditAction;
    
private:
	// Size of grid
    int m_iWidth, m_iHeight;
    
	// Array of points and normals
    Array< Array< R3Pt > > m_aaptGrid;
    Array< Array< R3Vec > > m_aavecGrid;
    
	// Do we have two valid curves?
    bool m_bValid;
    Curve m_crvLeft, m_crvRight;
    
	// Workhorse - interact ray with inflation/extrusion surface
    R3Pt IntersectSurface( const R2Pt &in_ptScreen ) const;
    
	// Build the 3D surface
    void BuildSurface();
	// Set the normals using cross products
    void SetNormals();
    
public:
	/// Do we have a valid surface?
    bool IsValid() const { return m_bValid; }
    
	/**@name Drawing/creating */
	//@{
	///  Create the left or right boundary curve
    void CreateBoundaryCurve( const CurveNetwork &in_crvNetwork, const ScreenCurve &in_crvScreen, const bool in_bDoLeft );
	/// Create (and return) a 3D curve from the screen curve
    Curve DrawOnSurface( const ScreenCurve &in_crvScreen ) const;
	/// Create a 3D surface by extruding the curve
    void AlignToCurve( const Curve &in_crv, const R3Vec &in_vecDir );
	/// Clear the current surface/curves
	void Clear();
	/// Set the resolution of the surface
    void SetSize( const int in_iW, const int in_iH );
	//@}
    
    /**@name Selecting elements (spheres at ends of curves, sphere on surface) */
	//@{
	/// Returns -1 if nothing selected.
    int SelectSurface( const R2Pt &in_ptScreen, double &out_dZ ) const;
    /// Translate value returned from SelectSurface into which element was clicked on
    bool IsSelectedLeftCurve( const int in_iSelected ) const { return in_iSelected == 0 || in_iSelected == 1; }
    /// Translate value returned from SelectSurface into which element was clicked on
    bool IsSelectedRightCurve( const int in_iSelected ) const { return in_iSelected == 2 || in_iSelected == 3; }
    /// Translate value returned from SelectSurface into which element was clicked on
    bool IsSelectedSurface( const int in_iSelected ) const { return in_iSelected == 4; }
	//@}
	
    InflationSurface();
    ~InflationSurface() {}
    
	/// in_iSelected is the value returned from SelectSurface, and edit action is what we're doing (both stored in ActionEvents)
    void Draw( const int in_iSelected, const EditAction in_editAction ) const;
};

#endif