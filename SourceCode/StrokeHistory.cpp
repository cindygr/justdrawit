/*
 *  StrokeHistory.cpp
 *  SketchCurves
 *
 *  Created by Cindy Grimm on 1/20/11.
 *  Copyright 2011 Adobe Corp.. All rights reserved.
 *
 */

#include "StrokeHistory.h"
#include "ParamFile.h"
#include "UserInterface.h"

const bool s_bTrace = false;
const bool s_bTraceBlend = false;
const bool s_bTraceBlendDetails = false;

void StrokeHistory::SetSeg( const Curve &in_crvStroke )
{
	m_segStroke = R3Line_seg( in_crvStroke(0.0), in_crvStroke(1.0) );
}

StrokeHistory::StrokeHistory() 
: m_mergeType(FIRST_STROKE),
m_crvJoin(2),
m_segStroke( R3Pt(-1e30, -1e30, -1e30), R3Pt(1e30, 1e30, 1e30) ),
m_iCurveHashIdOrig(-1),
m_iCurveHashIdNew(-1)
{ 
	m_dTJoinPrev.first = m_dTJoinNext.first = -1.0;
	m_dTJoinPrev.second = m_dTJoinNext.second = -1.0;
	m_planeDraw = g_drawState.DrawingPlane();
    
    m_iTimeStamp = CurveNetwork::CurrentTime();
    m_iUndoLevel = -1;
}

/*
CRVHermite2 crvHermite(1);
crvHermite.SetPt(0) = in_stroke.Pt(0);
crvHermite.SetPt(1) = m_ptEndCurve;
crvHermite.SetVec(0) = m_vecEnd2Curve * (dLen / 3.0 );
crvHermite.SetVec(1) = m_vecEnd1Screen * (dLen / 3.0 );
*/


void StrokeHistory::StartNew( const Curve &in_crv )
{
	m_mergeType = FIRST_STROKE;
	m_crvStroke3D = in_crv;
	m_crvFinal = in_crv;
	
	SetSeg( in_crv );
    m_iTimeStamp = CurveNetwork::CurrentTime();
}

void StrokeHistory::StartNewClosed( const CurveOverlap &in_overlap, const Curve &in_crvStroke3D, Curve &out_crv )
{
	m_mergeType = FIRST_STROKE_CLOSED;
	m_crvStroke3D = in_crvStroke3D;

	const Curve crvStroke = in_crvStroke3D.ResampleAbsoluteSize( g_drawState.CurveSpacingSize() );
	std::pair<double,double> adTsStroke = in_overlap.StrokeMergeTsFirst( );
	std::pair<double,double> adTsCurve = in_overlap.CurveMergeTsFirst( );
	
	// Account for the fact that the curves have been cut in half
	adTsStroke.first /= 2.0;
	adTsStroke.second /= 2.0;
	adTsCurve.first = 1.0 - (1.0 - adTsCurve.first) / 2.0;
	adTsCurve.second = 1.0 - (1.0 - adTsCurve.second) / 2.0;
	
	
	// Reverse search
	if ( s_bTrace ) cout << "Joining end\n";
	double dScore = 0.0;
	m_crvJoin = FindJoinCurve( in_crvStroke3D, crvStroke,
								adTsCurve,
								adTsStroke,
								in_overlap.IsJoinStartStroke(), dScore );	
	
	const Curve crvResample = ResampleJoinCurve( m_crvJoin );
	
	if ( s_bTrace ) {
		cout << m_crvJoin.GetPt(0) << in_crvStroke3D( m_dTJoinPrev.first ) << "\n";
		cout << m_crvJoin.GetPt(1) << crvStroke( m_dTJoinNext.first ) << "\n";
	}

	/* Second half of stroke first */
	out_crv.Clear();
	const int iNDiv = crvStroke.NumPts() / 2;
	out_crv.AddPoint( crvStroke.Pt(iNDiv) );
	for ( int i = iNDiv+1; i < crvStroke.NumPts(); i++ ) {
		if ( crvStroke.PercAlong(i) <= m_dTJoinPrev.first ) {
			out_crv.AddPoint( crvStroke.Pt(i) );
		}
	}
	
	/* Join to start of stroke */
	for ( int i = 1; i < crvResample.NumPts() - 1; i++ ) {
		out_crv.AddPoint( crvResample.Pt(i) );
	}

	/* Remainder of stroke */
	for ( int i = 0; i < iNDiv; i++ ) {
		if ( crvStroke.PercAlong(i) >= m_dTJoinNext.first ) {
			out_crv.AddPoint( crvStroke.Pt(i) );
		}
	}
	out_crv.AddPoint( crvStroke.Pt(iNDiv) );
	
	if ( s_bTrace ) {
		cout << "Ts crv " << m_dTJoinPrev.first << " " << m_dTJoinNext.second << "\n";
	}
	out_crv.SetAllData();
	SmoothJoin( m_crvJoin, dScore, true, out_crv );
	
	SetSeg( in_crvStroke3D );

    for ( int i = 0; i < out_crv.NumPts() - 2; i++ ) {
        if ( ApproxEqual( out_crv.Pt(i), out_crv.Pt(i+2) ) ) {
            cerr << "Same pt " << i << "\n";
        }
    }
    
    m_crvFinal = out_crv;

    m_iTimeStamp = CurveNetwork::CurrentTime();
}

std::pair<double,double> StrokeHistory::ResampleStroke( const std::pair<double,double> &in_ad, const Curve &in_crvOrig, const Curve &in_crvNew ) const
{
	const R3Pt pt1 = in_crvOrig( in_ad.first );
	const R3Pt pt2 = in_crvOrig( in_ad.second );
	
	const double dT1 = in_crvNew.ClosestPointTValue( pt1 );
	const double dT2 = in_crvNew.ClosestPointTValue( pt2 );
	
	return std::pair<double,double>( dT1, dT2 );
}

Curve StrokeHistory::ResampleJoinCurve( const CRVHermite3 &in_crv ) const
{
	const double dLenDefault = ::Length( in_crv.GetPt(1) - in_crv.GetPt(0) );
	const double dNSteps = 1e-6 + dLenDefault / ( 0.5 * g_drawState.CurveSpacingSize() );
	const double dTStep = 0.25 * WINmin( 0.25, 1.0 / dNSteps ); 
	
	Curve crvResampleHermite;
	for ( double dT = 0.0; dT < 1.0 + dTStep * 0.5; dT += dTStep ) {
		crvResampleHermite.AddPoint( in_crv(dT) );
	}
	return crvResampleHermite.ResampleAbsoluteSize( g_drawState.CurveSpacingSize() );
}

/* Score should range from 0 (angles are almost the same) to 1 (big angle difference */
void StrokeHistory::SmoothJoin( const CRVHermite3 &in_crv, const double in_dScore, const bool in_bFirst, Curve &io_crv ) 
{
	const double dScore = WINminmax( in_dScore, 0.0, 2.0 );
	const double dTSel = ( (1.0 + 2.0 * dScore ) * g_drawState.SelectionSize() ) / io_crv.Length();

	const double dTStart = io_crv.ClosestPointTValue( in_crv.GetPt(0) );
	const double dTEnd = io_crv.ClosestPointTValue( in_crv.GetPt(1) );
	
	std::pair<double,double> &dT = in_bFirst ?  m_dTJoinPrev : m_dTJoinNext;
    const R3Vec vecTangCrv = io_crv.Tangent( dTStart );
    const double dDot = Dot( vecTangCrv, in_crv.GetVec(0) );
    if ( io_crv.IsClosed() ) {
        if ( dDot > 0.0 ) {
            dT.first = dTStart - dTSel;
            dT.second = dTEnd + dTSel;
        } else {
            dT.first = dTEnd - dTSel;
            dT.second = dTStart + dTSel;
        }
        if ( dT.first > dT.second ) {
            if ( dT.first > 0.5 ) {
                dT.first -= 1.0;
            } else {
                dT.second += 1.0;
            }
        }
    } else {
        if ( dTStart < dTEnd ) {
            dT.first = dTStart - dTSel;
            dT.second = dTEnd + dTSel;
        } else {
            dT.first = dTEnd - dTSel;
            dT.second = dTStart + dTSel;
        }
        dT.first = WINmax( 0.0, dT.first );
        dT.second = WINmin( dT.second, 1.0 );
    }
    if ( s_bTraceBlendDetails ) cout << "Joins " << dT.first << " " << dT.second << "\n";
		
	const int iPerScore = 1 + (int) ( dScore * 40.0 );
	const int iLoopSmooth = iPerScore + (int) (g_paramFile.GetInteger("SmoothingMaximumLoops") * g_drawState.m_opUI->m_dSmoothing->value() );
	io_crv.SmoothPoints( dT.first, dT.second, iLoopSmooth );

	if ( s_bTrace ) {
		cout << "Score " << dScore << " bracket " << io_crv.Floor( dT.first ) << " " << dT.first << " " << io_crv.Ceil( dT.second ) << " " << dT.second << " smooth " << iPerScore << "\n";
	}
    m_iTimeStamp = CurveNetwork::CurrentTime();
}

static CRVHermite1 s_crvBlend;
inline void InitCrvBlend()
{
	if ( s_crvBlend.NumPts() == 0 ) {
		s_crvBlend.Add( R1Pt(0.0), R1Vec(0.01) );
		s_crvBlend.Add( R1Pt(1.0), R1Vec(0.01) );
	}
}

std::pair<R2Pt, R2Vec> StrokeHistory::Project( const R2Pt &in_pt, 
											   const std::vector< ScreenCurve > &in_acrv, 
											   const std::vector< std::pair<double,double> > &in_adTBlends, 
											   const double in_dDist )
{
	R2Pt ptOut, ptSearch = in_pt;
	R2Vec vecSearch;
	
	static std::vector< R2Pt > apt;
	static std::vector< R2Vec > avec;
	static std::vector< double > adWeight;
	InitCrvBlend();
	
	if ( in_acrv.size() == 0 ) {
		cerr << "ERR: StrokeHistory::Project, no curves\n";
		return std::pair<R2Pt, R2Vec>( in_pt, R2Vec(1,1) );
	}
	
	if ( s_bTraceBlendDetails ) cout << "Projecting " << in_pt << in_acrv.size() << " crvs\n";
	double dClosest = 1e30, dTClosest = 0.0,dDist = 0.0;
	int iClosest = 0;
	for ( int iLoop = 0; iLoop < 5; iLoop++ ) {
		apt.resize(0);
		avec.resize(0);
		adWeight.resize(0);
		iClosest = -1;
		dClosest = 1e30;
		const R2Pt ptSave = ptSearch;
		for ( int i = 0; i < in_acrv.size(); i++ ) {
			const double dT = in_acrv[i].ClosestPointTValue( in_pt, dDist );
			
			// in case doesn't come close to points
			if ( dDist < dClosest ) {
				iClosest = i;
				dClosest = dDist;
				dTClosest = dT;
			}

			if ( dT > 0.0 && dT < 1.0 && dDist < in_dDist ) {
				apt.push_back( in_acrv[i](dT) );
				avec.push_back( in_acrv[i].Tangent(dT) );
				double dWeight = 1.0;
				if ( dT < in_adTBlends[i].first ) {
					dWeight = s_crvBlend( dT / in_adTBlends[i].first )[0];
				} else if ( dT > in_adTBlends[i].second ) {
					dWeight = s_crvBlend( (1.0 - dT) / (1.0 - in_adTBlends[i].second) )[0];
				}
				adWeight.push_back( dWeight );
				if ( s_bTraceBlendDetails ) cout << " " << i << " dist " << dDist << " weight " << dWeight << "\n";
			}
		}
		// This is a last ditch in case there wasn't a seg the point projected onto
		if ( apt.size() == 0 ) {
			const R2Pt pt = in_acrv[iClosest]( dTClosest );
			
			ptSearch = pt;
			vecSearch = in_acrv[iClosest].Tangent( dTClosest );
			
			if ( s_bTraceBlendDetails ) cout << "Using closest " << iClosest << "\n";
		} else if ( apt.size() == 1 ) {
			ptSearch = apt[0];
			vecSearch = avec[0];
			if ( s_bTraceBlendDetails ) cout << "Using one\n";
		} else {
			ptSearch = R2Pt(0,0);
			double dSum = 0.0;
			for ( int i = 0; i < apt.size(); i++ ) {
				for ( int j = 0; j < 2; j++ ) {
					ptSearch[j] += apt[i][j] * adWeight[i];
				}
				dSum += adWeight[i];
			}
			if ( RNIsZero(dSum) ) {
				cerr << "ERR: StrokeHistory::Project, zero sum weight\n";
				dSum = 1.0;
			}
			for ( int j = 0; j < 2; j++ ) {
				ptSearch[j] /= dSum;
			}
		}
		
		if ( s_bTraceBlendDetails ) cout << ptSearch << "  " << vecSearch << "\n";

		if ( ApproxEqual( ptSearch, ptSave ) ) {
			break;
		}
	}
	if ( s_bTraceBlendDetails ) cout << "\n";
	return std::pair<R2Pt, R2Vec>( ptSearch, vecSearch );
}

/*
 * Take each curve and extend it out.
 * Project each curve on the summed blends
 * Move the curve towards the summed blended version
 */
std::vector< ScreenCurve > StrokeHistory::ProjectAndBlendCurves( const std::vector< ScreenCurve > &in_acrvs, const double in_dDist )
{
	std::vector< ScreenCurve > acrv( in_acrvs.size() );
	if ( in_acrvs.size() == 0 ) {
		cerr << "ERR: StrokeHistory::ProjectAndBlendCurves (static), no curves to blend!\n";
		return acrv;
	}
	
	std::vector< std::pair<double,double> > adTBlends( in_acrvs.size() );
	const int iLoopSmooth = 1 + (int) (g_paramFile.GetInteger("SmoothingMaximumLoops") * g_drawState.m_opUI->m_dSmoothing->value() );
	for ( int i = 0; i < in_acrvs.size(); i++ ) {
		acrv[i] = in_acrvs[i];
		acrv[i].Smooth(1);
        acrv[i].SmoothEnds(iLoopSmooth);

		adTBlends[i] = std::pair<double,double>(0.0,1.0);
	}
	const std::vector< ScreenCurve > acrvSaveSmoothed( acrv );
	
	/* Create blend curves to the start/end of the curves */
	std::vector< std::pair< CRVHermite2, CRVHermite2 > > acrvEnds( in_acrvs.size() );
	const double dProjDist = in_dDist * 1.5;
	const double dStepDist = in_dDist * 0.5;
	const double dVecDist = in_dDist * 1.0;
	for ( int iC = 0; iC < acrvEnds.size(); iC++ ) {
		if ( in_acrvs[iC].NumPts() < 3 ) {
			cerr << "ERR: StrokeHistory::ProjectAndBlendCurves (static), no points in curve " << iC << "\n";
			continue;
		}
		CRVHermite2 &crvStart = acrvEnds[iC].first;
		CRVHermite2 &crvEnd = acrvEnds[iC].second;
		const R2Pt ptE1 = in_acrvs[iC].Pt(0);
		const R2Vec vecE1 = UnitSafe( ptE1 - in_acrvs[iC].Pt(1) );
		const R2Pt ptE2 = in_acrvs[iC].Pt( in_acrvs[iC].NumPts() - 1 );
		const R2Vec vecE2 = UnitSafe( ptE2 - in_acrvs[iC].Pt(in_acrvs[iC].NumPts() - 2) );
		std::pair< R2Pt, R2Vec > ptProjE1(ptE1, vecE1);
		std::pair< R2Pt, R2Vec > ptProjE2(ptE2, vecE2);
		double dLenE1 = 0.0, dLenE2 = 0.0;
		for ( int iWalkOut = 0; iWalkOut < 6; iWalkOut++ ) {
			const R2Pt ptSaveE1 = ptProjE1.first;
			const R2Pt ptSaveE2 = ptProjE2.first;
			
			ptProjE1 = Project( ptProjE1.first + ptProjE1.second * dStepDist, acrv, adTBlends, dProjDist );
			ptProjE2 = Project( ptProjE2.first + ptProjE2.second * dStepDist, acrv, adTBlends, dProjDist );
			
			dLenE1 += ::Length( ptProjE1.first - ptSaveE1 );
			dLenE2 += ::Length( ptProjE2.first - ptSaveE2 );
			if ( Dot( vecE1, ptProjE1.second ) < 0.0 ) {
				ptProjE1.second = ptProjE1.second * -1.0;
			}
			if ( Dot( vecE2, ptProjE2.second ) < 0.0 ) {
				ptProjE2.second = ptProjE2.second * -1.0;
			}
		}
		/* If the stroke ends roughly at the beginning/end of the entire curve, make it point out
		 * instead of projecting (prevents sharp blends)
		 */
		if ( RNIsZero( dLenE1, 1e-6 ) ) { 
			ptProjE1.first = ptE1; 
		} else if ( dLenE1 < 2.0 * dStepDist ) {
			ptProjE1.first = ptE1 + vecE1 * (dLenE1 * 1.1);
		}
		if ( RNIsZero( dLenE2, 1e-6 ) ) { 
			ptProjE2.first = ptE2; 
		} else if ( dLenE2 < 2.0 * dStepDist ) {
			ptProjE2.first = ptE2 + vecE2 * (dLenE2 * 1.1);
		}
		crvStart.Add( ptProjE1.first, ptProjE1.second * dVecDist );
		crvStart.Add( ptE1, vecE1 * -dVecDist );
		crvEnd.Add( ptProjE2.first, ptProjE2.second * dVecDist );
		crvEnd.Add( ptE2, vecE2 * -dVecDist );
		if ( s_bTraceBlend ) {
			cout << "Start " << crvStart.GetPt(0) << crvStart.GetVec(0) << " pt " << crvStart.GetPt(1) << crvStart.GetVec(1) << "\n";
			cout << "End " << crvEnd.GetPt(0) << crvEnd.GetVec(0) << " pt " << crvEnd.GetPt(1) << crvEnd.GetVec(1) << "\n";
		}
	}
	
	/* Now add the hermite segments in */
	const int iNSteps = 12;
	const double dStep = 1.0 / (double) iNSteps;
	
	ScreenCurve crvTemp;
	for ( int iC = 0; iC < acrvEnds.size(); iC++ ) {
		
		crvTemp.Clear();
		if ( !ApproxEqual( acrvEnds[iC].first.GetPt(0), acrvEnds[iC].first.GetPt(1), 1e-6 ) ) {
			for ( int i = 0; i < iNSteps; i++ ) {
				const R2Pt pt = acrvEnds[iC].first( (i+1) * dStep );
				crvTemp.AddPoint( pt );
			}
		}
		for ( int i = 0; i < acrv[iC].NumPts(); i++ ) {
			crvTemp.AddPoint( acrv[iC].Pt(i) );
		}
		if ( !ApproxEqual( acrvEnds[iC].second.GetPt(0), acrvEnds[iC].second.GetPt(1), 1e-6 ) ) {
			for ( int i = iNSteps - 1; i >= 0; i-- ) {
				const R2Pt pt = acrvEnds[iC].second( (i+1) * dStep );
				crvTemp.AddPoint( pt );
			}
		}
        crvTemp.Smooth(iLoopSmooth);
		
		acrv[iC] = crvTemp;
		if ( !ApproxEqual( acrvEnds[iC].first.GetPt(0), acrvEnds[iC].first.GetPt(1), 1e-6 ) ) {
			adTBlends[iC].first = crvTemp.PercAlong(iNSteps);
		}
		if ( !ApproxEqual( acrvEnds[iC].second.GetPt(0), acrvEnds[iC].second.GetPt(1), 1e-6 ) ) {
			adTBlends[iC].second = crvTemp.PercAlong( crvTemp.NumPts() - iNSteps - 1 );
		}
		const double dMid = 0.5 * (adTBlends[iC].first + adTBlends[iC].second);
		adTBlends[iC].first = 0.25 * dMid + 0.75 * adTBlends[iC].first;
		adTBlends[iC].second = 0.25 * dMid + 0.75 * adTBlends[iC].second;
		if (s_bTraceBlend) cout << iC << " " << adTBlends[iC].first << " " << adTBlends[iC].second << "\n";
	}
	
	/* Now walk along the curve and add in points */
	double dScore = 0.0;
	int iNPts = 0;
	std::vector< ScreenCurve > acrvProject( acrvSaveSmoothed.size() );
	for ( int iC = 0; iC < acrvSaveSmoothed.size(); iC++ ) {
		if ( s_bTraceBlendDetails ) cout << iC << "\n";
		
		for ( int j = 0; j < acrvSaveSmoothed[iC].NumPts(); j++ ) {
			if ( s_bTraceBlendDetails ) cout << j << "\n";
			const R2Pt ptOrig = acrvSaveSmoothed[iC].Pt(j);
			const R2Pt ptProj = Project( ptOrig, acrv, adTBlends, dProjDist ).first;
			dScore += ::Length( ptOrig - ptProj );
			iNPts++;
			const double dT = acrvSaveSmoothed[iC].PercAlong(j);
			double dPerc = 0.25;
			if ( dT < 0.25 ) {
				dPerc = (1.0 - s_crvBlend( dT * 4.0 )[0]) * 0.5 + 0.25;
			} else if ( dT > 0.75 ) {
				dPerc = s_crvBlend( (dT-0.75) * 4.0 )[0] * 0.5 + 0.25;
			}
			acrvProject[iC].AddPoint( Lerp( ptOrig, ptProj, dPerc ) );
		}
        acrvProject[iC].Smooth(iLoopSmooth);
	}

	if ( s_bTraceBlend ) cout << "Score " << 100.0 * dScore / (double) iNPts << "\n";
	return acrvProject;
}

/*
 * Extend each screen curve out and add weights
 */
ScreenCurve StrokeHistory::Blend( const std::vector< ScreenCurve > &in_acrvs, const double in_dDist )
{
	ScreenCurve crv;
	if ( in_acrvs.size() == 0 ) {
		cerr << "ERR: StrokeHistory::Blend (static), no curves to blend!\n";
		return crv;
	}
	
	std::vector< ScreenCurve > acrvProject = ProjectAndBlendCurves( in_acrvs, in_dDist );
	for ( int iLoop = 0; iLoop < 3; iLoop++ ) {
		acrvProject = ProjectAndBlendCurves( acrvProject, in_dDist );
	}
		
	/* Now string them together */
	double dDist;
				
	/* Just in case didn't catch a fold, start with part of the first curve, not the whole thing */
	crv.Clear();
	for ( int i = 0; i < WINmin( 5, acrvProject[0].NumPts() ); i++ ) {
		crv.AddPoint( acrvProject[0].Pt(i) );
	}

	const double dDistAdd = in_dDist * 0.1;
	for ( int iC = 0; iC < acrvProject.size(); iC++ ) {
		const ScreenCurve &crvAdd = acrvProject[iC];
		if ( s_bTraceBlend ) cout << "Crv " << iC << "," << crvAdd.NumPts() << " ";
		int iLastAdd = 0;
		for ( int i = 0; i < crvAdd.NumPts(); i++ ) {
			/* First see if an endpoint of this curve lies inside the curve */
			const double dT = crv.ClosestPointTValue( crvAdd.Pt(i), dDist );
			if ( RNIsZero(dT) ) {
				// Shouldn't happen too often;
				const R2Pt ptLast = crv(0.0);
				if ( ::Length( ptLast - crvAdd.Pt(i) ) > dDistAdd ) { 
					crv = crv.Reverse();
					crv.AddPoint( crvAdd.Pt(i) );
					crv = crv.Reverse();
					if ( s_bTraceBlend ) cout << "Zero " << i << " ";
					iLastAdd = i;
				}
			} else if ( RNApproxEqual( dT, 1.0 ) ) {
				const R2Pt ptLast = crv(1.0);
				if ( ::Length( ptLast - crvAdd.Pt(i) ) > dDistAdd ) { 
					crv.AddPoint( crvAdd.Pt(i) );
					if ( s_bTraceBlend ) cout << i << " ";
					iLastAdd = i;
				}
			} else if ( dDist > in_dDist ) {
				int iBackTrack = i;
				const R2Pt ptLast = crv(1.0);
				for ( int j = i; j >= iLastAdd; j-- ) {
					if ( ::Length( ptLast - in_acrvs[iC].Pt(j) ) < dDistAdd ) {
						break;
					}
					iBackTrack = j;
				}
				if ( s_bTraceBlend ) cout << "Dist ";
				// Use the ORIGINAL curve here, because we have an accidental blend
				for ( int j = iBackTrack; j <= i; j++ ) {
					crv.AddPoint( in_acrvs[iC].Pt(j) );
					if ( s_bTraceBlend ) cout << j << " ";
				}
				if ( s_bTraceBlend ) cout << "EndDist ";
			}
		}
		if ( s_bTraceBlend ) cout << "\n";
	}
	const int iLoopSmooth = 5 + (int) (g_paramFile.GetInteger("SmoothingMaximumLoops") * g_drawState.m_opUI->m_dSmoothing->value() );
    crv.Smooth(iLoopSmooth);

	return crv;
}

void StrokeHistory::BlendScratch( const ScreenCurve &in_screenCurve, const R3Plane &in_plane )
{
	m_mergeType = BLEND_SCRATCH;
	m_dTJoinPrev = std::pair<double,double>(0.0,1.0);
	m_crvStroke2D = in_screenCurve;
	m_planeDraw = in_plane;
	
	m_crvStroke3D.Clear();
	const OGLObjsCamera &cam = g_drawState.GetCamera();
	for ( int i = 0; i < in_screenCurve.NumPts(); i++ ) {
		m_crvStroke3D.AddPoint( m_planeDraw.IntersectRay( R3Ray( cam.From(), cam.RayFromEye( in_screenCurve.Pt(i) ) ) ) );
	}
	m_crvStroke3D.SetAllData();
	SetSeg( m_crvStroke3D );
	
	m_crvFinal = m_crvStroke3D;
    m_iTimeStamp = CurveNetwork::CurrentTime();
}

void StrokeHistory::BlendOverstroke( const ScreenCurve &in_screenCurve )
{
	m_mergeType = BLEND_OVERSTROKE;
	m_dTJoinPrev = std::pair<double,double>(0.0,1.0);
	m_crvStroke2D = in_screenCurve;
	
	m_crvStroke3D.Clear();
	const OGLObjsCamera &cam = g_drawState.GetCamera();
	for ( int i = 0; i < in_screenCurve.NumPts(); i++ ) {
		m_crvStroke3D.AddPoint( m_planeDraw.IntersectRay( R3Ray( cam.From(), cam.RayFromEye( in_screenCurve.Pt(i) ) ) ) );
	}
	m_crvStroke3D.SetAllData();
	SetSeg( m_crvStroke3D );

	m_crvFinal = m_crvStroke3D;
    m_iTimeStamp = CurveNetwork::CurrentTime();
}

/*
 * Find the best curve that joins the two
 * io_dScore is the distance the curves are apart at their closest point
 */
CRVHermite3 StrokeHistory::FindJoinCurve( const Curve &in_crvComposite, const Curve &in_crvStroke, 
										  const std::pair<double,double> &in_adCrvRange, const std::pair<double,double> &in_adStrokeRange,
						                  const bool in_bJoin,
										  double &io_dScore ) 
{
	CRVHermite3 crvRet(2);
	
	// Walk back along the curve for awhile and see if there's a good blend place
	const double dDistSel = g_drawState.SelectionSize();
	const double dJoinStep = g_paramFile.GetDouble("SampleSizeJoinMergeSearchPerc");
	const double dTang = g_paramFile.GetDouble("TangentStepSizePerc");
	
	const double dTCrv = dJoinStep * dDistSel / in_crvComposite.Length();
	const double dTStroke = dJoinStep * dDistSel / in_crvStroke.Length();
	
	std::vector<double> adTCrv, adTStroke;
	std::vector<R3Pt> aptCrv, aptStroke;
	std::vector<R3Vec> avecCrv, avecStroke;
	
	for ( double dT = in_adCrvRange.first; dT <= in_adCrvRange.second; dT += dTCrv ) {
		const R3Vec vecEndCurve = in_crvComposite( dT + dTCrv * dTang ) - in_crvComposite( dT - dTCrv * dTang );
		if ( !RNIsZero( ::Length( vecEndCurve ) ) ) {
			adTCrv.push_back( dT );
			aptCrv.push_back( in_crvComposite( dT ) );
			avecCrv.push_back( UnitSafe( vecEndCurve ) );
		}
	}
	for ( double dT = in_adStrokeRange.first; dT <= in_adStrokeRange.second; dT += dTStroke ) {
		const R3Vec vecEndStroke = in_crvStroke( dT + dTStroke * dTang ) - in_crvStroke( dT - dTStroke * dTang );
		if ( !RNIsZero( ::Length( vecEndStroke ) ) ) {
			adTStroke.push_back( dT );
			aptStroke.push_back( in_crvStroke( dT ) );
			avecStroke.push_back( UnitSafe( vecEndStroke ) );
		}
	}
	// This is a general score of how far apart the two curves are
	const double dLenMid = in_bJoin ? WINminmax( (g_paramFile.GetDouble("JoinLengthCap") - io_dScore) / g_paramFile.GetDouble("JoinLengthCap"), 0.0, 1.0 ) : io_dScore;
	
	m_dTJoinPrev.first = 0.5 * ( in_adCrvRange.first + in_adCrvRange.second );
	m_dTJoinNext.first = 0.5 * ( in_adStrokeRange.first + in_adStrokeRange.second );
	crvRet.SetPt(0) = in_crvComposite(m_dTJoinPrev.first);
	crvRet.SetPt(1) = in_crvStroke(m_dTJoinNext.first);
	
	const double dLenDefault = ::Length( m_crvJoin.GetPt(1) - m_crvJoin.GetPt(0) );
	
	crvRet.SetVec(0) = in_crvComposite.Tangent(m_dTJoinPrev.first) * (dLenDefault * 0.5);
	crvRet.SetVec(1) = in_crvStroke.Tangent(m_dTJoinNext.first) * (dLenDefault * 0.5);
	
	double dBestAvgAng = M_PI;
	double dBestCross = 0.0;
    const double dPercAvg = g_paramFile.GetDouble("JoinAvgVsDiff");    
	if ( s_bTrace ) {
		if ( aptCrv.size() && aptStroke.size() ) {
			cout << "pts " << aptCrv.size() << " " << adTCrv[0] << " " << adTCrv.back() << " " << aptStroke.size() << " " << adTStroke[0] << " " << adTStroke.back() << "\n";
		} else {
			cout << "Merge, no points to check\n";
		}
	}
	io_dScore = 1.0;
	for ( int i = 0; i < aptCrv.size(); i++ ) {
		for ( int j = 0; j < aptStroke.size(); j++ ) {
			R3Vec vecJoin = aptStroke[j] - aptCrv[i];
			if ( RNIsZero( ::Length(vecJoin) ) ) {
				continue;
			}
			vecJoin.Normalize();
			const double dDotJoinCurve = Dot( vecJoin, avecCrv[i] );
			const double dDotJoinStroke = Dot( vecJoin, avecStroke[j] );
			const double dAngJoinCurve = DotToAngle( dDotJoinCurve );
			const double dAngJoinStroke = DotToAngle( dDotJoinStroke );
			
	 		// Look for the place where the angle change is split
			const double dAvgAngSplit = fabs( dAngJoinCurve - dAngJoinStroke );
			const double dAvgAngTotal = 0.5 * (dAngJoinCurve + dAngJoinStroke);
			const double dAvgAng = in_bJoin ? ( dPercAvg * dAvgAngTotal + (1.0 - dPercAvg) * dAvgAngSplit ) : ( 0.8 * dAvgAngTotal + 0.2 * dAvgAngSplit );
			const double dCross = Dot( Cross( vecJoin, avecCrv[i] ), Cross( vecJoin, avecStroke[i] ) );
			
			if ( dAvgAng < dBestAvgAng || ( dBestCross < 0.0 && dCross > 0.0 && dAvgAng < 1.1 * dBestAvgAng ) ) {
				dBestAvgAng = dAvgAng;
				dBestCross = dCross;
				m_dTJoinPrev.first = adTCrv[i];
				m_dTJoinNext.first = adTStroke[j];
				crvRet.SetPt(0) = aptCrv[i];
				crvRet.SetPt(1) = aptStroke[j];
				
				const double dLen = ::Length( crvRet.GetPt(1) - crvRet.GetPt(0) );
				
				io_dScore = WINminmax( fabs( dAngJoinCurve ) / (0.25 * M_PI) + fabs( dAngJoinStroke ) / (0.25 * M_PI), 0.0, 1.0 );
				io_dScore *= (1.0 + dLenMid) * io_dScore;

				if ( dCross > 0.0 ) {
					crvRet.SetVec(0) = avecCrv[i] * (dLen * 1.5);
					crvRet.SetVec(1) = avecStroke[j] * (dLen * 1.5);
				} else {
					crvRet.SetVec(0) = avecCrv[i] * (dLen * 0.5);
					crvRet.SetVec(1) = avecStroke[j] * (dLen * 0.5);
				}
				if ( s_bTrace == true ) cout << i << " " << j << " vec " << vecJoin << " angs " << dAngJoinCurve << " " << dAngJoinStroke << " " << dAvgAng << " " << dCross << " score " << io_dScore << "\n";
			}
		}
	}
	
	return crvRet;
}

void StrokeHistory::SetJoinCurve( const Curve &in_crvComposite, const Curve &in_crvStroke, 
								  const std::pair<double,double> &in_adCrvRange, 
								  const std::pair<double,double> &in_adStrokeRange,
								  const bool in_bJoin,
								  const double in_dDistApart,
                                  Curve &out_crv )
{
	double dScore = in_dDistApart;
	m_crvJoin = FindJoinCurve( in_crvComposite, in_crvStroke, in_adCrvRange, in_adStrokeRange, in_bJoin, dScore );
	
	out_crv.Clear();
	for ( int i = 0; i < in_crvComposite.NumPts(); i++ ) {
		if ( in_crvComposite.PercAlong(i) <= m_dTJoinPrev.first ) {
			out_crv.AddPoint( in_crvComposite.Pt(i) );
		}
	}
	
	const Curve crvResampleHermite = ResampleJoinCurve( m_crvJoin );
	for ( int i = 1; i < crvResampleHermite.NumPts() - 1; i++ ) {
		out_crv.AddPoint( crvResampleHermite.Pt(i) );
	}
	
	for ( int i = 0; i < in_crvStroke.NumPts(); i++ ) {
		if ( in_crvStroke.PercAlong(i) >= m_dTJoinNext.first ) {
			out_crv.AddPoint( in_crvStroke.Pt(i) );
		}
	}
	
	if ( s_bTrace ) {
		cout << "Ts " << m_dTJoinPrev.first << " " << m_dTJoinNext.first << "\n";
	}

	out_crv.SetAllData();
	SmoothJoin( m_crvJoin, dScore, true, out_crv );
	
}

void StrokeHistory::SetJoinCurveReverse( const Curve &in_crvComposite, const Curve &in_crvStroke, 
   								         const std::pair<double,double> &in_adCrvRange, 
								         const std::pair<double,double> &in_adStrokeRange,
										 const bool in_bJoin,
										 const double in_dDistApart,
                                         Curve &out_crv )
{
	double dScore = in_dDistApart;
	m_crvJoin = FindJoinCurve( in_crvStroke, in_crvComposite, in_adStrokeRange, in_adCrvRange, in_bJoin, dScore );

	out_crv.Clear();

	// Stroke first
	for ( int i = 0; i < in_crvStroke.NumPts(); i++ ) {
		if ( in_crvStroke.PercAlong(i) <= m_dTJoinPrev.first ) {
			out_crv.AddPoint( in_crvStroke.Pt(i) );
		}
	}
	
	// Now blend curve
	const Curve crvResampleHermite = ResampleJoinCurve( m_crvJoin );
	for ( int i = 1; i < crvResampleHermite.NumPts() - 1; i++ ) {
		out_crv.AddPoint( crvResampleHermite.Pt(i) );
	}

	// Now rest of composite curve
	for ( int i = 0; i < in_crvComposite.NumPts(); i++ ) {
		if ( in_crvComposite.PercAlong(i) >= m_dTJoinNext.first ) {
			out_crv.AddPoint( in_crvComposite.Pt(i) );
		}
	}
	
	out_crv.SetAllData();
	SmoothJoin( m_crvJoin, dScore, true, out_crv );
	
	if ( s_bTrace ) {
		cout << "Ts " << m_dTJoinPrev.first << " " << m_dTJoinPrev.second << "  ";
		cout << m_dTJoinNext.first << " " << m_dTJoinNext.second << "\n";
	}
}

/*
 * For a join, we know we're going from one end of the curve to one end of the stroke
 * So search distance is set by distance from end of curve.
 * Use 3D search criteria only, no 2D overlap info 
 */
void StrokeHistory::Join( const CurveOverlap &in_overlap, const Curve &in_crvComposite, const Curve &in_crvStroke, Curve &out_crv )
{
	m_crvStroke3D = in_crvStroke;

	const Curve crvStroke = in_crvStroke.ResampleAbsoluteSize( g_drawState.CurveSpacingSize() );

	// Walk back along the curve for awhile and see if there's a good blend place
	const double dScore = in_overlap.Score( );
	const double dDistSel = (1.0 + 6.0 * dScore) * g_drawState.SelectionSize();
	
	const double dJoinSearchCap = g_paramFile.GetDouble("JoinMergeMaximumSearchPerc");

	const double dTCrv = WINmin( dJoinSearchCap * in_crvComposite.Length(), dDistSel ) / in_crvComposite.Length();
	const double dTStroke = WINmin( dJoinSearchCap * crvStroke.Length(), dDistSel ) / crvStroke.Length();

	if ( in_overlap.StrokeEnd() == CurveOverlap::START_CURVE && in_overlap.CurveEnd() == CurveOverlap::END_CURVE ) {
        if ( s_bTrace ) cout << "Join case 1: end of curve to start of stroke\n";
		m_mergeType = JOIN;
		
		SetJoinCurve( in_crvComposite, crvStroke, 
					  std::pair<double,double>( WINmax(0.5, 1.0 - dTCrv), 1.0 ),
					  std::pair<double,double>( 0.0, WINmin(0.5, dTStroke) ), 
					  true, in_overlap.DistanceApart( CurveOverlap::START_CURVE ),
                      out_crv );
	} else if ( in_overlap.StrokeEnd() == CurveOverlap::END_CURVE && in_overlap.CurveEnd() == CurveOverlap::START_CURVE ) {
        if ( s_bTrace ) cout << "Join case 2: start of curve to end of stroke\n";
		m_mergeType = JOIN_REVERSE;
		
		SetJoinCurveReverse( in_crvComposite, crvStroke, 
							std::pair<double,double>( 0.0, WINmin(0.5, 0.0 + dTCrv) ),
							std::pair<double,double>( WINmax( 0.5, 1.0 - dTStroke ), 1.0 ), 
							true, in_overlap.DistanceApart( CurveOverlap::START_CURVE ),
                            out_crv );
	} else {
		cerr << "ERR: Join end labels don't make sense\n";
	}
	
	SetSeg( in_crvStroke );

    m_crvFinal = out_crv;
    m_iTimeStamp = CurveNetwork::CurrentTime();
}

/* Assumes the stroke has already been oriented with respect to the composite curve
 *    --- Tangents at contact point are in the same direction
 * Four cases: Start/end of stroke, Start/end of composite curve
 *
 * Join points: 
 *   On the composite curve: 
 *     Project the starting/ending point of the stroke onto the composite curve. 
 *     Walk a little ways in on the stroke and project again (this determines if we are going in
 *       the positive or negative t direction on the composite curve)
 *     Look up to 1.5 select distance back *before* the stroke joins
 *   On the stroke:
 *     Project the start/end point of the composite curve onto the stroke (probably should look for the overhang
 *     point). 
 *     Look up to 1.5 select distance *after* the start/end point
 *     
 */
void StrokeHistory::Merge( const CurveOverlap &in_overlap, const Curve &in_crvComposite, const Curve &in_crvStroke, Curve &out_crv )
{
	m_crvStroke3D = in_crvStroke;

	const Curve crvStroke = in_crvStroke.ResampleAbsoluteSize( g_drawState.CurveSpacingSize() );
	const std::pair<double,double> adTs = ResampleStroke( in_overlap.StrokeMergeTsFirst( in_crvStroke ), in_crvStroke, crvStroke );
	
	if ( in_overlap.StrokeEnd() == CurveOverlap::START_CURVE && in_overlap.CurveEnd() == CurveOverlap::END_CURVE ) {
        if ( s_bTrace ) cout << "Merge, case 1\n";
		m_mergeType = MERGE;
		
		SetJoinCurve( in_crvComposite, crvStroke, 
					  in_overlap.CurveMergeTsFirst(),
					  adTs,
					  false, in_overlap.DistanceApart(CurveOverlap::START_CURVE),
                      out_crv );
	} else if ( in_overlap.StrokeEnd() == CurveOverlap::END_CURVE && in_overlap.CurveEnd() == CurveOverlap::START_CURVE ) {
        if ( s_bTrace ) cout << "Merge, case 2\n";
		m_mergeType = MERGE_REVERSE;
		
		SetJoinCurveReverse( in_crvComposite, crvStroke, 
							 in_overlap.CurveMergeTsFirst(),
							 adTs,
							 false, in_overlap.DistanceApart( CurveOverlap::START_CURVE ),
                             out_crv);
	} else {
		cerr << "ERR: Merge end labels don't make sense\n";
	}

	SetSeg( in_crvStroke );
    m_crvFinal = out_crv;
    m_iTimeStamp = CurveNetwork::CurrentTime();
}

/* Assumes the stroke has already been oriented so that the start and stop ts are in order.
 * Stroke is oriented in same direction as curve
 */
void StrokeHistory::Overstroke( const CurveOverlap &in_overlap, const Curve &in_crvComposite, const Curve &in_crvStroke3D, const bool in_bIsPartial, Curve &out_crv )
{
	m_mergeType = in_bIsPartial ? OVERSKETCH_START : OVERSKETCH;
	m_crvStroke3D = in_crvStroke3D;

	const Curve crvStroke = in_crvStroke3D.ResampleAbsoluteSize( g_drawState.CurveSpacingSize() );
	const std::pair<double,double> adTsFirst = ResampleStroke( in_overlap.StrokeMergeTsFirst( in_crvStroke3D ), in_crvStroke3D, crvStroke );
	const std::pair<double,double> adTsSecond = ResampleStroke( in_overlap.StrokeMergeTsSecond( in_crvStroke3D ), in_crvStroke3D, crvStroke );
	
	// Reverse search
	if ( s_bTrace ) cout << "Stroke end to curve\n";
	double dScoreE2 = in_overlap.DistanceApart( CurveOverlap::END_CURVE );
	const CRVHermite3 crvE2 = FindJoinCurve( crvStroke, in_crvComposite, 
											 adTsSecond,
											 in_overlap.CurveMergeTsSecond(),
									          false, dScoreE2 );
	m_dTJoinPrev.second = m_dTJoinPrev.first;
	m_dTJoinNext.second = m_dTJoinNext.first;
	
	if ( s_bTrace ) cout << "Curve to stroke start\n";
	double dScoreE1 = in_overlap.DistanceApart( CurveOverlap::START_CURVE );
	const CRVHermite3 crvE1 = FindJoinCurve( in_crvComposite, crvStroke,  
											in_overlap.CurveMergeTsFirst(),
											adTsFirst,
											 false, dScoreE1 );
	
	const Curve crvResampleE1 = ResampleJoinCurve( crvE1 );
	const Curve crvResampleE2 = ResampleJoinCurve( crvE2 );

	// Save for later
	m_crvJoin = CRVHermite3(4);
	
	m_crvJoin.SetPt(0) = crvE1.GetPt(0);
	m_crvJoin.SetPt(1) = crvE1.GetPt(1);
	m_crvJoin.SetPt(2) = crvE2.GetPt(0);
	m_crvJoin.SetPt(3) = crvE2.GetPt(1);

	m_crvJoin.SetVec(0) = crvE1.GetVec(0);
	m_crvJoin.SetVec(1) = crvE1.GetVec(1);
	m_crvJoin.SetVec(2) = crvE2.GetVec(0);
	m_crvJoin.SetVec(3) = crvE2.GetVec(1);
	
	out_crv.Clear();
	/* First the original part of the curve */
	for ( int i = 0; i < in_crvComposite.NumPts(); i++ ) {
		if ( in_crvComposite.PercAlong(i) <= m_dTJoinPrev.first ) {
			out_crv.AddPoint( in_crvComposite.Pt(i) );
		} else {
			if ( s_bTrace ) cout << "Last point " << i << " ";
			break;
		}
	}

	/* Now the first join */
	for ( int i = 1; i < crvResampleE1.NumPts() - 1; i++ ) {
		out_crv.AddPoint( crvResampleE1.Pt(i) );
	}
	
	/* Now the overstroke */
	for ( int i = 0; i < crvStroke.NumPts(); i++ ) {
		if ( crvStroke.PercAlong(i) >= m_dTJoinNext.first && crvStroke.PercAlong(i) <= m_dTJoinPrev.second ) {
			out_crv.AddPoint( crvStroke.Pt(i) );
		}
	}

	/* Now the second join */
	for ( int i = 1; i < crvResampleE2.NumPts() - 1; i++ ) {
		out_crv.AddPoint( crvResampleE2.Pt(i) );
	}
	
	/* Now the remainder of the curve */
	if ( s_bTrace ) {
		cout << " added " << crvResampleE1.NumPts() + ( crvStroke.Ceil( m_dTJoinPrev.second) - crvStroke.Floor( m_dTJoinNext.first ) ) + crvResampleE2.NumPts() - 4 << " ";
		cout << " restart " << in_crvComposite.Floor(m_dTJoinNext.second) << "\n";
	}
	for ( int i = in_crvComposite.Floor(m_dTJoinNext.second); i < in_crvComposite.NumPts(); i++ ) {
		if ( in_crvComposite.PercAlong(i) >= m_dTJoinNext.second ) {
			out_crv.AddPoint( in_crvComposite.Pt(i) );
		}
	}
	if ( s_bTrace ) {
		cout << "Ts crv " << m_dTJoinPrev.first << " " << m_dTJoinNext.second << "  stroke ";
		cout << m_dTJoinPrev.second << " " << m_dTJoinNext.first << "\n";
	}
	out_crv.SetAllData();
	SmoothJoin( crvE1, dScoreE1, true, out_crv );
	SmoothJoin( crvE2, dScoreE2, false, out_crv );
	
	if ( in_bIsPartial ) {
		if ( in_overlap.ReverseStroke() ) {
			m_crvStroke3D = m_crvStroke3D.Reverse();
		} else {
			m_crvStroke3D = in_crvStroke3D;
		}
	}
	
	SetSeg( in_crvStroke3D );
    m_crvFinal = out_crv;
    
    m_iTimeStamp = CurveNetwork::CurrentTime();
}

/* Combine two curves into one with the Joining stroke.
 * Assumes the join stroke is oriented with respect to the two curves BUT the
 * direction of the second curve may need to be reversed. 
 * Assumes the t values for the join have already been clipped (don't overlap)
 * The curves will be joined in the order listed.
 * New curve will be:
 *   E1 Join E2
 *
 * Observation: If you split the join curve in two, then this is essentially two merges
 * followed by gluing the two back together.
 */
void StrokeHistory::Combine( const CurveOverlap &in_overlapE1, const Curve &in_crvCompositeE1, 
							 const CurveOverlap &in_overlapE2, const Curve &in_crvCompositeE2, 
							 const Curve &in_crvJoin, 
                             const int in_iHashId2,
                             const Array< PointConstraint * > &in_aopPins,
                             Curve &out_crv )
{
	m_mergeType = COMBINE;
	m_crvStroke3D = in_crvJoin;
    m_iCurveHashIdOrig = in_iHashId2;
    m_iCurveHashIdNew = -1;

	StrokeHistory sh1, sh2;
	const Curve crvJoinRev = in_crvJoin.Reverse();
	const Curve crvStroke = in_crvJoin.ResampleAbsoluteSize( g_drawState.CurveSpacingSize() );
    Curve crvMergeE1Join, crvMergeE2Join;

	sh1.Merge( in_overlapE1, in_crvCompositeE1, in_overlapE1.ReverseStroke() ? crvJoinRev : in_crvJoin, crvMergeE1Join );
	sh2.Merge( in_overlapE2, in_crvCompositeE2, in_overlapE2.ReverseStroke() ? crvJoinRev : in_crvJoin, crvMergeE2Join );
	m_crvJoin = CRVHermite3(4);
	m_crvJoin.SetPt(0) = sh1.m_crvJoin.GetPt(0); m_crvJoin.SetVec(0) = sh1.m_crvJoin.SetVec(0);
	m_crvJoin.SetPt(1) = sh1.m_crvJoin.GetPt(1); m_crvJoin.SetVec(1) = sh1.m_crvJoin.SetVec(1);
	m_crvJoin.SetPt(2) = sh2.m_crvJoin.GetPt(0); m_crvJoin.SetVec(2) = sh2.m_crvJoin.SetVec(0);
	m_crvJoin.SetPt(3) = sh2.m_crvJoin.GetPt(1); m_crvJoin.SetVec(3) = sh2.m_crvJoin.SetVec(1);
	
	/* Now find the point on the curve to splice the two back together. Only
	 * tricky part here is that the t values are going to be off */
	m_dTJoinPrev = in_overlapE1.StrokeMergeTsFirst( in_overlapE1.ReverseStroke() ? crvJoinRev : in_crvJoin );
	m_dTJoinNext = in_overlapE2.StrokeMergeTsFirst( in_overlapE2.ReverseStroke() ? crvJoinRev : in_crvJoin );
	
	const double dTE1 = ( in_overlapE1.CurveEnd() == CurveOverlap::END_CURVE ) ? m_dTJoinPrev.second : m_dTJoinPrev.first;
	const double dTE2 = ( in_overlapE2.CurveEnd() == CurveOverlap::END_CURVE ) ? m_dTJoinNext.second : m_dTJoinNext.first;

	double dBracketE1 = dTE1, dBracketE2 = dTE2;
	
	/* Now get the orientation of the two merged curves correct. */
	if ( in_overlapE1.CurveEnd() == CurveOverlap::START_CURVE ) {
		dBracketE1 = 1.0 - dBracketE1;
		crvMergeE1Join = crvMergeE1Join.Reverse();
		m_crvJoin.SetPt(0) = sh1.m_crvJoin.GetPt(1); m_crvJoin.SetVec(0) = sh1.m_crvJoin.SetVec(1);
		m_crvJoin.SetPt(1) = sh1.m_crvJoin.GetPt(0); m_crvJoin.SetVec(1) = sh1.m_crvJoin.SetVec(0);
	}
	if ( in_overlapE2.CurveEnd() == CurveOverlap::END_CURVE ) {
		dBracketE2 = 1.0 - dBracketE2;
		crvMergeE2Join = crvMergeE2Join.Reverse();
		m_crvJoin.SetPt(2) = sh2.m_crvJoin.GetPt(1); m_crvJoin.SetVec(2) = sh2.m_crvJoin.SetVec(1);
		m_crvJoin.SetPt(3) = sh2.m_crvJoin.GetPt(0); m_crvJoin.SetVec(3) = sh2.m_crvJoin.SetVec(0);
	}
	if ( s_bTrace ) cout << "Ts bracket " << dBracketE1 << " " << dBracketE2 << "\n";
	
	/* Now find the join point and just stick it together */
	const double dESplit = 0.5 * ( dBracketE1 + dBracketE2 );
	const R3Pt ptSplit = crvStroke(dESplit);
	 
	/* Merge should have just copied the join curve over - walk back (forward) from the end of each merged curve */
	double dDist = 0.0;
	R3Pt ptOut;
	const std::pair<int,double> iIndexE1Full = crvMergeE1Join.ClosestPointSegLocation( ptSplit, dDist, ptOut );
	const std::pair<int,double> iIndexE2Full = crvMergeE2Join.ClosestPointSegLocation( ptSplit, dDist, ptOut );
	const int iIndexE1 = iIndexE1Full.second < 0.5 ? iIndexE1Full.first : iIndexE1Full.first + 1;
	const int iIndexE2 = iIndexE2Full.second < 0.5 ? iIndexE2Full.first : iIndexE2Full.first + 1;

	if ( s_bTrace ) {
		cout << "Combine points E1 " << iIndexE1Full.first << "/" << crvMergeE1Join.NumPts() << " " << iIndexE1Full.second << "\n";
		cout << "Combine points E2 " << iIndexE2Full.first << "/" << crvMergeE2Join.NumPts() << " " << iIndexE2Full.second << "\n";
		cout << ptSplit << crvMergeE1Join.Pt(iIndexE1) << crvMergeE2Join.Pt(iIndexE2) << "\n";
	}
		
	out_crv.Clear();
	/* The first curve plus the first half of the join */
	for ( int i = 0; i < iIndexE1; i++ ) {
		out_crv.AddPoint( crvMergeE1Join.Pt(i) );
	}
	
	/* Now the second */
	for ( int i = iIndexE2; i < crvMergeE2Join.NumPts(); i++ ) {
		out_crv.AddPoint( crvMergeE2Join.Pt(i) );
	}
	
	out_crv.SetAllData();
	
	SetSeg( in_crvJoin );

    m_aiPinHashId.need( in_aopPins.num() );
    for ( int i = 0; i < in_aopPins.num(); i++ ) {
        m_aiPinHashId[i] = in_aopPins[i]->HashId();
    }
    
    m_crvFinal = out_crv;
    m_iTimeStamp = CurveNetwork::CurrentTime();
}

/* Assumes the stroke has already been oriented so that the start and stop ts are in order.
 * Stroke is oriented in same direction as curve, so start of stroke lies at the end of the curve
 * Put the curve back together in this order:
 *   Original curve (minus the start/end)
 *   Join curve 1
 *   Stroke (minus the start/end)
 *   Join curve 2
 */
void StrokeHistory::Close( const CurveOverlap &in_overlap, const Curve &in_crvComposite, const Curve &in_crvStroke3D, Curve &out_crv )
{
	m_mergeType = CLOSE;
	m_crvStroke3D = in_crvStroke3D;
	
    const bool bIsClosed = in_crvComposite.IsClosed();
    
	const Curve crvStroke = in_crvStroke3D.ResampleAbsoluteSize( g_drawState.CurveSpacingSize() );
	const std::pair<double,double> adTsFirst = ResampleStroke( in_overlap.StrokeMergeTsFirst( in_crvStroke3D ), in_crvStroke3D, crvStroke );
	const std::pair<double,double> adTsSecond = ResampleStroke( in_overlap.StrokeMergeTsSecond( in_crvStroke3D ), in_crvStroke3D, crvStroke );
	
	// Reverse search
	if ( s_bTrace ) cout << "Curve end to start of stroke\n";
	double dScoreE2 = in_overlap.DistanceApart( CurveOverlap::START_CURVE );
	const CRVHermite3 crvE2 = FindJoinCurve( in_crvComposite, crvStroke,
 											 in_overlap.CurveMergeTsFirst(),
											 adTsFirst,
											 in_overlap.IsJoinStartStroke(), dScoreE2 );
	m_dTJoinPrev.second = m_dTJoinPrev.first;
	m_dTJoinNext.second = m_dTJoinNext.first;
	const double dTOldEnd = m_dTJoinPrev.first;
	const double dTStrokeStart = m_dTJoinNext.first;
	
	if ( s_bTrace ) cout << "End of stroke to start of curve\n";
	double dScoreE1 = in_overlap.DistanceApart( CurveOverlap::END_CURVE );
	const CRVHermite3 crvE1 = FindJoinCurve( crvStroke, in_crvComposite,  
											 adTsSecond,
											 in_overlap.CurveMergeTsSecond(),
											 in_overlap.IsJoinEndStroke(), dScoreE1 );
	
	const double dTOldStart = m_dTJoinNext.first;
	const double dTStrokeEnd = m_dTJoinPrev.first;
	
	const Curve crvResampleE1 = ResampleJoinCurve( crvE1 );
	const Curve crvResampleE2 = ResampleJoinCurve( crvE2 );

	// Save for later
	m_crvJoin = CRVHermite3(4);
	
	m_crvJoin.SetPt(0) = crvE1.GetPt(0);
	m_crvJoin.SetPt(1) = crvE1.GetPt(1);
	m_crvJoin.SetPt(2) = crvE2.GetPt(0);
	m_crvJoin.SetPt(3) = crvE2.GetPt(1);
	
	m_crvJoin.SetVec(0) = crvE1.GetVec(0);
	m_crvJoin.SetVec(1) = crvE1.GetVec(1);
	m_crvJoin.SetVec(2) = crvE2.GetVec(0);
	m_crvJoin.SetVec(3) = crvE2.GetVec(1);
	
	if ( s_bTrace ) {
		cout << crvE2.GetPt(0) << in_crvComposite( dTOldEnd ) << "\n";
		cout << crvE2.GetPt(1) << crvStroke( dTStrokeStart ) << "\n";
		cout << crvE1.GetPt(0) << crvStroke( dTStrokeEnd ) << "\n";
		cout << crvE1.GetPt(1) << in_crvComposite( dTOldStart ) << "\n";
	}
	
	/* Composite curve first */
    const int iStartCrv = in_crvComposite.Ceil( dTOldStart );
    const int iEndCrv = in_crvComposite.CeilWrap( ( bIsClosed && dTOldStart > dTOldEnd ) ? dTOldEnd + 1.0 : dTOldEnd );

	out_crv.Clear();
	for ( int i = iStartCrv; i < iEndCrv; i++ ) {
        out_crv.AddPoint( in_crvComposite.Pt(i) );
	}

	/* Join to start of stroke */
	for ( int i = 1; i < crvResampleE2.NumPts() - 1; i++ ) {
		out_crv.AddPoint( crvResampleE2.Pt(i) );
	}

	/* Stroke */
	for ( int i = 0; i < crvStroke.NumPts(); i++ ) {
		if ( crvStroke.PercAlong(i) >= dTStrokeStart && crvStroke.PercAlong(i) <= dTStrokeEnd ) {
			out_crv.AddPoint( crvStroke.Pt(i) );
		}
	}
	
	/* Join to end of curve */
	for ( int i = 1; i < crvResampleE1.NumPts() - 1; i++ ) {
		out_crv.AddPoint( crvResampleE1.Pt(i) );
	}
	
	const R3Pt ptStart = out_crv.Pt(0);
	out_crv.AddPoint( ptStart );
	
	if ( s_bTrace ) {
		cout << "Ts crv " << m_dTJoinPrev.first << " " << m_dTJoinNext.second << "  stroke ";
		cout << m_dTJoinPrev.second << " " << m_dTJoinNext.first << "\n";
	}
	out_crv.SetAllData();

	SmoothJoin( crvE1, dScoreE1, true, out_crv );
	SmoothJoin( crvE2, dScoreE2, false, out_crv );
	
	SetSeg( in_crvStroke3D );
    
    for ( int i = 0; i < out_crv.NumPts() - 2; i++ ) {
        if ( ApproxEqual( out_crv.Pt(i), out_crv.Pt(i+2) ) ) {
            cerr << "Same pt " << i << "\n";
        }
    }

    m_crvFinal = out_crv;
    m_iTimeStamp = CurveNetwork::CurrentTime();
}

void StrokeHistory::StartTransform( const Curve &in_crvComposite )
{
	m_mergeType = TRANSFORM;	
	m_crvDragTemp = in_crvComposite;
    m_crvFinal = in_crvComposite;
    m_iTimeStamp = CurveNetwork::CurrentTime();
}

void StrokeHistory::Drag( const R3Plane &in_plane, const R3Vec &in_vec, const double in_dTDrag, const std::pair<double, double> & in_dTBracket, Curve &out_crv )
{
	out_crv = m_crvDragTemp;
	out_crv.Drag( in_plane, in_vec, in_dTDrag, in_dTBracket );
    m_crvFinal = out_crv;
}

void StrokeHistory::Transform( const R4Matrix &in_mat, Curve &out_crv, const bool in_bReset )
{
    if ( in_bReset ) {
        out_crv = m_crvDragTemp;
    } 
    
	out_crv.Transform( in_mat );
    m_crvFinal = out_crv;
}


/*
 * When we come out of here, this stroke history has
 *   Intermediate curve is the right half of the curve
 *   io_crvOrig is also the right half of the curve
 *   The left half of the curve is stored in out_crvLHS
 *   The stroke history for the LHS is returned as well
 */
void StrokeHistory::Erase( const double in_dTStart, const double in_dTEnd, 
                           const int in_iOrigHashId, const int in_iLHSHashId,
                           const Array< int > &in_aiPins,
                           Curve &io_crvOrig,                            
                           StrokeHistory & out_shLHS,
                           Curve         & out_crvLHS )
{
	m_mergeType = ERASE;
	m_dTJoinPrev.first = in_dTStart;
	m_dTJoinPrev.second = in_dTEnd;
    m_iCurveHashIdOrig = in_iOrigHashId;
    m_iCurveHashIdNew = in_iLHSHashId;
    
    out_shLHS = *this;

    /* Erase the curve from start to end. Store the longer piece in the original curve */
    Curve crvErase( io_crvOrig );
    Curve crvLHS = crvErase.Erase( in_dTStart, in_dTEnd );
    
    if ( crvErase.Length() > crvLHS.Length() ) {
        io_crvOrig = crvErase;
        out_crvLHS = crvLHS;
    } else {
        io_crvOrig = crvLHS;
        out_crvLHS = crvErase;
    }

    out_shLHS.m_crvFinal = out_crvLHS;
    out_shLHS.m_iCurveHashIdOrig = in_iOrigHashId;
    out_shLHS.m_iCurveHashIdNew = in_iLHSHashId;
    out_shLHS.m_iTimeStamp = CurveNetwork::CurrentTime();
    m_crvFinal = io_crvOrig;
    
    m_aiPinHashId = in_aiPins;
    out_shLHS.m_aiPinHashId = in_aiPins;

    m_iTimeStamp = CurveNetwork::CurrentTime();
}

void StrokeHistory::Smooth( const double in_dTStart, const double in_dTEnd, const int in_iNLoops, Curve &io_crvComposite )
{
    // first time through
    m_mergeType = SMOOTH;
    m_dTJoinNext.first = m_dTJoinPrev.first = in_dTStart;
    m_dTJoinNext.second = m_dTJoinPrev.second = in_dTEnd;

    m_segStroke = R3Line_seg( io_crvComposite(in_dTStart), io_crvComposite(in_dTEnd) );
	
	if ( s_bTrace ) cout << "Smoothing " << in_dTStart << " " << in_dTEnd << "\n";
    io_crvComposite.SmoothPoints( in_dTStart, in_dTEnd, in_iNLoops );
    
    m_crvFinal = io_crvComposite;
    m_iTimeStamp = CurveNetwork::CurrentTime();
}

void StrokeHistory::RepeatSmooth( Curve &io_crvComposite )
{	
	
	switch ( GetMergeType() ) {
		case SMOOTH :
			if ( s_bTrace ) cout << "Repeat smooth " << m_dTJoinNext.first << " " << m_dTJoinNext.second << "\n";
			
			io_crvComposite.SmoothPoints( m_dTJoinNext.first, m_dTJoinNext.second );
			break;
			
		case MERGE :
		case MERGE_REVERSE :
		case JOIN :
		case JOIN_REVERSE :
		case FIRST_STROKE_CLOSED :
			if ( s_bTrace ) cout << "Repeat smooth one end " << m_dTJoinPrev.first << " " << m_dTJoinPrev.second << "\n";
			
			io_crvComposite.SmoothPoints( m_dTJoinPrev.first, m_dTJoinPrev.second );
			break;
			
		case OVERSKETCH :
		case OVERSKETCH_START :
		case COMBINE :
		case CLOSE :
			if ( s_bTrace ) {
				cout << "Repeat smooth two ends " << m_dTJoinPrev.first << " " << m_dTJoinPrev.second << " ";
				cout << m_dTJoinNext.first << " " << m_dTJoinNext.second << "\n";
			}

			io_crvComposite.SmoothPoints( m_dTJoinPrev.first, m_dTJoinPrev.second );
			io_crvComposite.SmoothPoints( m_dTJoinNext.first, m_dTJoinNext.second );
			break;
			
		default :
			cerr << "ERR: Stroke history, being asked to repeat smooth but last action not suitable for smoothing\n";
			break;
	}	
    m_crvFinal = io_crvComposite;
}
	
void StrokeHistory::Undo( const int in_iLevel, const Curve &in_crv ) 
{
    m_mergeType = UNDO;
    m_iUndoLevel = in_iLevel;
    m_crvFinal = in_crv;
    m_iTimeStamp = CurveNetwork::CurrentTime();
}
    
bool StrokeHistory::WasStroke() const 
{
    return m_crvStroke2D.NumPts() ? true : false;
}

bool StrokeHistory::WasConstraint() const
{
    if ( m_mergeType == ADDED_CONSTRAINT || m_mergeType == REMOVED_CONSTRAINT || m_mergeType == CHANGED_CONSTRAINT ) {
        return true;
    }
    return false;
}

///
void StrokeHistory::AddedConstraint( const int in_iId, const Curve &in_crvNew )
{
    m_mergeType = ADDED_CONSTRAINT;
    m_crvFinal = in_crvNew;
    m_aiPinHashId.need(1);
    m_aiPinHashId[0] = in_iId;
    m_iTimeStamp = CurveNetwork::CurrentTime();
}
///
void StrokeHistory::RemovedConstraint( const int in_iId, const Curve &in_crvNew )
{
    m_mergeType = REMOVED_CONSTRAINT;
    m_crvFinal = in_crvNew;
    m_aiPinHashId.need(1);
    m_aiPinHashId[0] = in_iId;
    m_iTimeStamp = CurveNetwork::CurrentTime();
}

///
void StrokeHistory::ChangedConstraint( const int in_iId, const Curve &in_crvNew )
{
    m_mergeType = CHANGED_CONSTRAINT;
    m_crvFinal = in_crvNew;
    m_aiPinHashId.need(1);
    m_aiPinHashId[0] = in_iId;
    m_iTimeStamp = CurveNetwork::CurrentTime();
}


StrokeHistory & StrokeHistory::operator=( const StrokeHistory &in_sh )
{
	m_mergeType = in_sh.m_mergeType;
	m_segStroke = in_sh.m_segStroke;
	m_crvJoin = in_sh.m_crvJoin;
	m_dTJoinPrev = in_sh.m_dTJoinPrev;
	m_dTJoinNext = in_sh.m_dTJoinNext;
	m_crvFinal = in_sh.m_crvFinal;
	m_crvStroke3D = in_sh.m_crvStroke3D;
	
	m_crvStroke2D = in_sh.m_crvStroke2D;
	m_planeDraw = in_sh.m_planeDraw;
    m_iTimeStamp = in_sh.m_iTimeStamp;
    m_aiPinHashId = in_sh.m_aiPinHashId;
    m_iCurveHashIdOrig = in_sh.m_iCurveHashIdOrig;
    m_iCurveHashIdNew = in_sh.m_iCurveHashIdNew;
    
    m_iUndoLevel = in_sh.m_iUndoLevel;

	return *this;
}

void StrokeHistory::SetConstructionHistory( const ScreenCurve &in_crvScreen, const R3Plane &in_plane )
{
	m_crvStroke2D = in_crvScreen;
	m_planeDraw = in_plane;
}



static char s_str[ StrokeHistory::NO_ACTION ][30] = 
{
	"FIRST_STROKE",
	"FIRST_STROKE_CLOSED",
	"BLEND_SCRATCH",
	"BLEND_OVERSTROKE",
	"MERGE",
	"MERGE_REVERSE",
	"JOIN",
	"JOIN_REVERSE",
	"CLOSE",
	"OVERSKETCH",
	"OVERSKETCH_START",
	"COMBINE",
	"REFLECT",
	"TRANSFORM", 
	"SMOOTH",
	"ERASE",
	"ADDED_CONSTRAINT",
	"REMOVED_CONSTRAINT",
	"CHANGED_CONSTRAINT",
	"UNDO"
};
	
void StrokeHistory::Print() const
{
	cout << s_str[ m_mergeType ] << "\n";
}

void StrokeHistory::PrintHistory() const
{
	cout << s_str[ m_mergeType ] << ", time " << m_iTimeStamp << ": ";
    switch( m_mergeType ) {    
        case ERASE :
        case COMBINE : {
            cout << "CI " << m_iCurveHashIdOrig << " " << m_iCurveHashIdNew << " PI: ";
            for ( int i = 0; i < m_aiPinHashId.num(); i++ ) {
                cout << m_aiPinHashId[i] << " ";
            }
            cout << "\n";
            break;
        }
        case ADDED_CONSTRAINT :
        case REMOVED_CONSTRAINT :
        case CHANGED_CONSTRAINT : {
            cout <<  "PI: ";
            for ( int i = 0; i < m_aiPinHashId.num(); i++ ) {
                cout << m_aiPinHashId[i] << " ";
            }
            cout << "\n";
            break;
        }
        case UNDO :
            cout << "Undo " << m_iUndoLevel << "\n";
            break;
        default :
            cout << "\n";
    }
}

std::string StrokeHistory::GetMergeTypeString( const StrokeHistory::MergeType in_mt ) 
{
	return std::string( s_str[ in_mt ] );
}

void StrokeHistory::Write( ofstream &out ) const
{
	out << "StrokeHistory\n";
	out << "MergeType " << (int) m_mergeType << " ";
	out << m_dTJoinPrev.first << " " << m_dTJoinNext.first << " ";
	out << m_dTJoinPrev.second << " " << m_dTJoinNext.second << " ";
	out << " SegStroke ";
	m_segStroke.Write(out);
	out << "CrvJoin ";
	m_crvJoin.Write(out);
	out << "Final ";
	m_crvFinal.Write(out);
	out << "Stroke3D ";
	m_crvStroke3D.Write(out);
	out << "Plane ";
	m_planeDraw.Write(out);
	out << "Stroke2D ";
	m_crvStroke2D.Write(out);
    out << "CurveHashIDOrig " << m_iCurveHashIdOrig << "\n";
    out << "CurveHashIDNew " << m_iCurveHashIdNew << "\n";
    out << "PinHashID " << m_aiPinHashId.num() << "\n";
    for ( int i = 0; i < m_aiPinHashId.num(); i++ ) {
        out << m_aiPinHashId[i] << " ";
    }
    out << "\n";
    out << "UndoLevel " << m_iUndoLevel << "\n";
}

void StrokeHistory::Read( ifstream &in )
{
    std::string str;
    
    in >> str; ASSERT( !str.compare( "StrokeHistory" ) );
    in >> str; ASSERT( !str.compare( "MergeType" ) );
	int iTemp;
	in >> iTemp >> m_dTJoinPrev.first >> m_dTJoinNext.first >> m_dTJoinPrev.second >> m_dTJoinNext.second;
	m_mergeType = (MergeType) iTemp;
	
    in >> str; ASSERT( !str.compare( "SegStroke" ) );
	m_segStroke.Read(in);
	
    in >> str; ASSERT( !str.compare( "CrvJoin" ) );
	m_crvJoin.Read(in);
	
    in >> str; ASSERT( !str.compare( "Final" ) );
	m_crvFinal.Read(in);
	
    in >> str; ASSERT( !str.compare( "Stroke3D" ) );
	m_crvStroke3D.Read(in);
		
    in >> str; ASSERT( !str.compare( "Plane" ) );
	m_planeDraw.Read(in);
	
    in >> str; ASSERT( !str.compare( "Stroke2D" ) );
	m_crvStroke2D.Read(in);
	
    in >> str; ASSERT( !str.compare( "CurveHashIDOrig" ) );
	in >> m_iCurveHashIdOrig;
    
    in >> str; ASSERT( !str.compare( "CurveHashIDNew" ) );
	in >> m_iCurveHashIdNew;
    
    int iN;
    in >> str; ASSERT( !str.compare( "PinHashID" ) );
	in >> iN;
    m_aiPinHashId.need(iN);
    for ( int i = 0; i < iN; i++ ) {
        in >> m_aiPinHashId[i];
    }
    
    in >> str; ASSERT( !str.compare( "UndoLevel" ) );
	in >> m_iUndoLevel;
    
}
