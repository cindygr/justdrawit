/* -------------------------------------------------------------------------
 * DESCR   :	Return the appropriate coefficient given the new index
 * ------------------------------------------------------------------------- */
#include "StdAfx.H"
#include <utils/Crv_Knot_vec.H>
#include <utils/Crv_Alpha.H>

double
CRVAlpha::operator()(int in_i) const
{
   if (Order() == -1) {
      if (m_adOldVec[m_iIndex] <= m_adNewVec[in_i] && 
          m_adNewVec[in_i] < m_adOldVec[m_iIndex+1]) {
         return 1;
      } else {
         return 0;
      }
   }
   double coef1 = 0;
   double coef2 = 0;
   if (!RNApproxEqual(m_adOldVec[m_iIndex + Order()+1], m_adOldVec[m_iIndex])) {
      coef1 = ((m_adNewVec[in_i + Order()+1] - m_adOldVec[m_iIndex]) /
               (m_adOldVec[m_iIndex + Order()+1] - m_adOldVec[m_iIndex]));
   }

   if (!RNApproxEqual(m_adOldVec[m_iIndex + Order() + 2], m_adOldVec[m_iIndex+1])) {
      coef2 = ((m_adOldVec[m_iIndex + Order() + 2] - m_adNewVec[in_i + Order()+1]) /
               (m_adOldVec[m_iIndex + Order() + 2] - m_adOldVec[m_iIndex + 1]));
   }

   return coef1 * (*m_alphaIth)(in_i) + coef2 * (*m_alphaIthPlus1)(in_i);
}

/* -------------------------------------------------------------------------
 * DESCR   :	Make one order lower
 * ------------------------------------------------------------------------- */
CRVAlpha::CRVAlpha(
                   const CRVSpace &in_crvspace,       // One order less than calling function
                   const Array<double> &in_adOldVec,     // Old knot vector
                   const Array<double> &in_adNewVec,     // New knot vector
                   int   in_iIndex        // index into old knot vector
   ) : CRVSpace(in_crvspace),
      m_iIndex(in_iIndex),
      m_adOldVec(in_adOldVec),
      m_adNewVec(in_adNewVec)
{
   // Don't need anymore children
   if (Order() == -1) {
      m_alphaIth = NULL;
      m_alphaIthPlus1 = NULL;
      return;
   }

   // Reduce the dimension 1 and make children, one of which starts at index,
   // and one of which starts at index+1
   CRVSpace reduced(in_crvspace.Order()-1);
   m_alphaIth =      new CRVAlpha(reduced, m_adOldVec, m_adNewVec, m_iIndex);
   m_alphaIthPlus1 = new CRVAlpha(reduced, m_adOldVec, m_adNewVec, m_iIndex + 1);
}

/* -------------------------- Public Routines ----------------------------- */

/* -------------------------------------------------------------------------
 * DESCR   :	Find the right alpha (i old index) and evaluate at j new index
 * ------------------------------------------------------------------------- */
double
CRVNurb_alpha::operator()(int in_iOld,  // old index
                          int in_iNew   // new index
   )
{
   return (*m_aAlphas[in_iOld])(in_iNew);
}

/* -------------------------------------------------------------------------
 * DESCR   :	Make one Alpha for each old control point
 * ------------------------------------------------------------------------- */
CRVNurb_alpha::CRVNurb_alpha(
                             const CRVSpace &in_crvspace,
                             const Array<double> &in_adOldKnot,
                             const Array<double> &in_adNewKnot
                             ) 
    : CRVSpace(in_crvspace), m_adOldKnot(in_adOldKnot), m_adNewKnot(in_adNewKnot),
      m_aAlphas(m_adOldKnot.num() - Order() - 2)
{
   // Number of control points, given number of knots and Order

   for (int i = 0; i < m_aAlphas.num(); i++)
     m_aAlphas[i] = new CRVAlpha(in_crvspace, m_adOldKnot, m_adNewKnot, i);
}

CRVNurb_alpha::~CRVNurb_alpha()
{
   for (int i = 0; i < m_aAlphas.num(); i++) {
      delete m_aAlphas[i];
      m_aAlphas[i] = NULL;
   }
}


/* -------------------------------------------------------------------------
 * DESCR   :	Print out the alpha values
 * ------------------------------------------------------------------------- */
void
CRVNurb_alpha::Print()
const
{
   cout << "Alphas\n";
   for (int j = 0; j < m_aAlphas.num() + (m_adNewKnot.num() - m_adOldKnot.num()); j++) 
      for (int i = 0; i < m_aAlphas.num(); i++) {
         double d = (*m_aAlphas[i])(j);
         if (!RNIsZero(d))
            cout << "Old " << i << " new " << j << " " << d << "\n"; 
      }
   cout << "\n";
   cout.flush();
}



