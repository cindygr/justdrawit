#include "StdAfx.H"
#include <utils/Crv_Knot_vec.H>

CRVSpace::CRVSpace(ifstream &in, WINbool bIs_binary) 
{
    if (bIs_binary == TRUE) {
        int iN[1];
        in.read((char *) iN, sizeof(int) * 1);
        ASSERT (in.gcount() == sizeof(int) * 1);
        m_iOrder = iN[0];
    } else {
        in >> m_iOrder;
    }
    ASSERT(m_iOrder >=0);
}
