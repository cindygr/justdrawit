/* ------------------------------------------------------------
  A non uniform rational bspline knot vector
  Operations implemented:
      refinement
      sub curve
   ------------------------------------------------------------ */

#include "StdAfx.H"
#include <utils/Crv_Knot_vec.H>

/* --------------- protected Routines -------------- */

double CRVKnot_vec::EvalAllBasis( double          in_dT, 
                                  Array<int>    & out_aiIndx, 
                                  Array<double> & out_adValue ) const
{
   // Evaluate basis function/control point pairs
   int f_k = Floor_knot(in_dT);
   int c_k = Ceil_knot(in_dT);
   int s_i = WINmax(0, f_k - (Order()+1));
   int e_i = WINmin(c_k+1, Num_pts());

   out_aiIndx.need(0);
   out_adValue.need(0);

   double dBasisSum = 0;
   for ( int i = s_i; i < e_i; i++) {
      out_adValue += m_aBasis[i](in_dT);
      out_aiIndx += i;

      dBasisSum += out_adValue.last();
   }
   return dBasisSum;
}

double CRVKnot_vec::EvalAllBasisDeriv( double          in_dT, 
                                       Array<int>    & out_aiIndx, 
                                       Array<double> & out_adValue ) const
{
   // Evaluate basis function/control point pairs
   int f_k = Floor_knot(in_dT);
   int c_k = Ceil_knot(in_dT);
   int s_i = WINmax(0, f_k - (Order()+1));
   int e_i = WINmin(c_k+1, Num_pts());

   out_aiIndx.need(0);
   out_adValue.need(0);

   double dBasisSum = 0;
   for ( int i = s_i; i < e_i; i++) {
      out_adValue += m_aBasis[i].Derivative(in_dT);
      out_aiIndx += i;

      dBasisSum += out_adValue.last();
   }
   return dBasisSum;
}

WINbool
CRVKnot_vec::Add_knots(int in_iN, int in_iWhere)
{
   ASSERT(in_iN >= 0);
   if (in_iWhere < 0) return FALSE;
   if (in_iWhere > Num_pts()) return FALSE;

   CRVBasis::SetDefaultOrder(Order());

   double st = Knot(in_iWhere);
   double et = Knot(in_iWhere+1);

   int old_b = m_aBasis.add(in_iN);
   int old_k = m_adKnots.add(in_iN);
   
   int j = 0;
   int i;
   for ( i = old_k-1; i > in_iWhere && j < Num_knots(); i--,j++)
     m_adKnots[Num_knots() -1 - j] = Knot(i);
   
   j = 0;
   for (i = old_b-1; i > in_iWhere && j < Num_basis(); i--, j++)
     Basis(Num_basis()-1-j) = Basis(i);

   double div = in_iN + 1.0;
   if (div > 0) div = (et - st) / div;
   
   for (i = 0; i < in_iN+1; i++)
     m_adKnots[in_iWhere+i] = Knot(in_iWhere) + i * div;

   // Change all basis functiosn
   int s_b = WINmax(0, in_iWhere - Order() - 2);
   int e_b = WINmin(Num_basis(), in_iWhere + in_iN+1);
   for (i = s_b; i < e_b; i++)
     Basis(i).Change_knot(Knots(), i);
   
   return CheckKnotVec();
}

WINbool
CRVKnot_vec::Remove_knots(int in_iN, int in_iWhere)
{
   if (in_iWhere < 0) return FALSE;
   if (in_iWhere + in_iN > Num_knots()) return FALSE;
   
   int i;
   for ( i = in_iWhere; i < Num_knots() - in_iN; i++)
     m_adKnots[i] = Knot(i+in_iN);

   for (i = in_iWhere; i < Num_basis() - in_iN; i++)
     Basis(i) = Basis(i+in_iN);

   int s_b = WINmax(0, in_iWhere - Order() - 2);
   int e_b = WINmin(Num_basis(), in_iWhere + Order() + 1);
   for (i = s_b; i < e_b; i++)
     Basis(i).Change_knot(m_adKnots, i);
   
   m_adKnots.sub(in_iN);
   m_aBasis.sub(in_iN);

   return CheckKnotVec();
}

/* ------------------------------------------------------------
 * 	Reset all the knot values. Check that they\'re ascending
 * ------------------------------------------------------------ */
WINbool
CRVKnot_vec::Set_knots(const Array<double> & in_adNewKnots,
                       int                   in_iStart, 
                       int                   in_iEnd)
{
   int n_set = in_iEnd - in_iStart - 1;
   ASSERT(in_iStart > -2 && n_set > 0 && in_iEnd <= Num_knots());

   // Check boundaries of knot values
   ASSERT(in_iStart < 0 || in_adNewKnots[0] >= First_knot());
   ASSERT(in_iEnd >= Num_knots() || in_adNewKnots[n_set-1] <= Last_knot());

   // Ascending check
   int i;
   for ( i = 1; i < in_adNewKnots.num(); i++)
     ASSERT(in_adNewKnots[i] >= in_adNewKnots[i-1]);

   int dupl_count_l = 0, dupl_count_r = 0;
   for (i = in_iStart; i >= 0; i--) 
     if (RNApproxEqual(in_adNewKnots[0], Knot(i))) dupl_count_l++;
     else break;
   for (i = in_iEnd; i < Num_knots(); i++) 
     if (RNApproxEqual(in_adNewKnots[n_set-1], Knot(i))) 
       dupl_count_r++;
     else break;
   
   // Too many duplicate knots check
   if (RNApproxEqual(in_adNewKnots[0], in_adNewKnots[n_set-1])) {
      ASSERT((dupl_count_l + dupl_count_r) < Order()+2);
   }
   
   for (i = 1; i < n_set; i++)
     if (RNApproxEqual(in_adNewKnots[i], in_adNewKnots[0])) 
       dupl_count_l++;
     else break;
   for (i = n_set-2; i >= 0; i++)
     if (RNApproxEqual(in_adNewKnots[i], in_adNewKnots[n_set-1])) 
       dupl_count_r++;
     else break;
   
   // Too many duplicate knots check
   ASSERT(dupl_count_l < Order()+2);
   ASSERT(dupl_count_r < Order()+2);
   
   int dupl_count = 0;
   if (n_set > Order()+1) {
      for (int i = 0; i < n_set-1; i++)
        if (RNApproxEqual(in_adNewKnots[i+1], in_adNewKnots[i])) {
           dupl_count++;
           ASSERT(dupl_count < Order()+2);
        } else {
           dupl_count = 0;
        }
   }

   for (i = 0; i < n_set; i++) 
     m_adKnots[in_iStart+1 + i] = in_adNewKnots[i];
   
   // Change all basis functiosn
   int s_b = WINmax(0, in_iStart - Order() - 1);
   int e_b = WINmin(Num_basis(), in_iStart + n_set + 1);
   for (i = s_b; i < e_b; i++)
     Basis(i).Change_knot(m_adKnots, i);
   
   return CheckKnotVec();
}

/* -------------------------- Private Routines ---------------- */


/* ------------- Public Routines ----------------------------- */



/* ------------------------------------------------------------
 * DESCR   :	Copy the curve
 * ------------------------------------------------------------ */
CRVKnot_vec &
CRVKnot_vec::operator=(const CRVKnot_vec &in_crvknot)
{
   if (Order() != in_crvknot.Order()) {
      m_aBasis.need(0);
      m_aBasis.squeeze();
      m_iOrder = in_crvknot.Order();
      CRVBasis::SetDefaultOrder(m_iOrder);
   }

   m_adKnots = in_crvknot.m_adKnots;
   m_aBasis = in_crvknot.m_aBasis;

   CheckKnotVec();
   return *this;
}

/* ------------------------------------------------------------
 * DESCR   :	Compare the curve
 * ------------------------------------------------------------ */
WINbool
CRVKnot_vec::operator==(const CRVKnot_vec &in_crvknot) const
{
   if (Order() != in_crvknot.Order()) return FALSE;
   if (Num_pts() != in_crvknot.Num_pts()) return FALSE;

   for (int i = 0; i < Num_knots(); i++) {
      if (RNApproxEqual(Knot(i), in_crvknot.Knot(i)) == FALSE) return FALSE;
   }
   return TRUE;
}

/* ------------------------------------------------------------
 * DESCR   :	Make a curve with the given # of points
 * DETAILS :    dim is dim+1 to account for weights
 * ------------------------------------------------------------ */
CRVKnot_vec::CRVKnot_vec(const CRVSpace & in_crvspace,
                         int              in_iNPts) : CRVSpace(in_crvspace)
{
    m_adKnots.need(WINmax(0, in_iNPts) + Order()+2);
    
    for ( int i = 0; i < Num_knots(); i++)
        m_adKnots[i] = i - (Order() + 1);
    
    CRVBasis::SetDefaultOrder(Order());
    
    for (FORINT i = 0; i < Num_pts(); i++)
        m_aBasis += CRVBasis(Order(), Knots(), i);
}

CRVKnot_vec::CRVKnot_vec(ifstream &in, WINbool bIn_binary) : CRVSpace( 2 )
{
    if ( bIn_binary == TRUE ) {
        Read_binary( in );
    } else {
        ReadKnotVec( in );
    }
}

/* ------------------------------------------------------------
 * DESCR   :	Print the knot vector and the points
 * ------------------------------------------------------------ */
void
CRVKnot_vec::PrintKnotVec(WINbool in_bDoBasis)
const 
{
   cout << "Knots: ";
   for (int k = 0; k < Num_knots(); k++)
     cout << Knot(k) << " ";
   cout << "\n";

   if (in_bDoBasis == TRUE)
     for (int i = 0; i < Num_basis(); i++) 
       Basis(i).Print();
}

/* ------------------------------------------------------------
 * DESCR   :	Write out and read in
 * DETAILS
 *    order
 *    dimension (of points+1)
 *    num of points
 *    num of data allocd
 *    knot dimension
 *    knot (RNvec::write)
 *    pts 0..n (RNpt::write)
 * ------------------------------------------------------------ */
WINbool
CRVKnot_vec::ReadKnotVec(ifstream &in)
{
    WINbool out_bRes = TRUE;
    int _order = -1;
    
    in >> _order;
    
    if (_order < -1) out_bRes = FALSE;
    
    m_iOrder = _order;
    CRVBasis::SetDefaultOrder(Order());
    m_aBasis.need(0);
    m_aBasis.squeeze();
    
    int n_knots = 0;
    
    in >> n_knots;
    
    m_adKnots.need(n_knots);
    m_aBasis.need(Num_pts());
    
    int i;
    for ( i = 0; i < Num_knots(); i++)
        in >> m_adKnots[i];
    
    for (i = 0; i < Num_pts(); i++) {
        Basis(i).Change_knot(Knots(), i);
    }
    
    ASSERT(out_bRes && CheckKnotVec());
    return (out_bRes && CheckKnotVec()) ? TRUE : FALSE;
}

void
CRVKnot_vec::WriteKnotVec(ofstream &out)
const 
{
   int iSavePrec = out.precision(15);
   out << Order() << " " << Num_knots() << "\n";
   
   for (int k = 0; k < Num_knots(); k++)
     out << Knot(k) << " ";
   out << "\n";
   out.precision(iSavePrec);
}

WINbool
CRVKnot_vec::Read_binary(ifstream &in)
{
    in.read( (char *) &m_iOrder, sizeof(int) );

    m_adKnots.ReadBinary(in);
    return in.good();
}

void
CRVKnot_vec::Write_binary(ofstream &out) const 
{
    out.write( ( const char *) &m_iOrder, sizeof(int) );   

    m_adKnots.WriteBinary(out);
}

void CRVKnot_vec::Set_knots(double in_dStart, double in_dEnd,
                            int    in_iStart, int    in_iEnd)
{
   int n_p = (in_iEnd - in_iStart +1);
   if (n_p < 2) return;
   
   double div = (in_dEnd - in_dStart) / (n_p-1);
   Array<double> in_adNewKnots(n_p);
   for (int i = 0; i < n_p; i++)
     in_adNewKnots[i] = in_dStart + i * div;

   Set_knots(in_adNewKnots, in_iStart-1, in_iEnd+1);
}

CRVKnot_vec  CRVKnot_vec::Derivative() const
{
    CRVKnot_vec out_kv(*this);

    for (int i = 0; i < m_aBasis.num(); i++)
        out_kv.m_aBasis[i] = m_aBasis[i].Derivative();

    return out_kv;
}
