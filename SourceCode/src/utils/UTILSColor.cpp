#include <utils/Rn_Defs.H>
#include <sstream>
#include <utils/Utils_Color.H>

const double UTILSColor::sm_aadColors[ 8 ][ 3 ] = 
  { { 1, 0, 0 }, { 0, 1, 0 }, { 0, 0, 1 },
  { 1, 0, 1 }, { 1, 1, 0 }, { 1, 1, 1 }, 
  { 0.5, 0.5, 0.5 }, { 0, 0, 0 } };
 
/*
// pre-saved values derived from the dot-product of original
// S,M,L and R,G,B curves.  Used in DKL color model. Curves
// were scaled to be between 0 and 1 before dot-producting.
const R3Matrix UTILSColor::smlMatrix( R3Vec( 0.1138, 0.1764, 12.0757 ),
												  R3Vec( 0.1809, 0.3703, 0.5432 ),
												  R3Vec( 0.9417, 0.6901, 0.7359 ) );
									  R3Vec(6.2210835930306301, 4.0876314796493141, 0.34437630657572266),
									  R3Vec(59.142045381250888, 63.382369008388984, 11.408624346416826),
									  R3Vec(7.6006447153520753, 11.982233556238741, 39.062873117146054) );
*/

//------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------

/* based on Colorimetry for CRT displays - Golz 03

L		0.9417		0.6901		0.7359			R	
M   =	0.1809		0.3703		0.5432      *	G
S		0.1138		0.1764	 	12.0757			B

This results in L,M,S values that are greater than 1 hence the matrix has to be scaled. The resulting scaled matrix is: */

const R3Matrix UTILSColor::rgbtolmsMatrix( R3Vec(0.3977,    0.1653,    0.0092),
										   R3Vec(0.2915,    0.3384,    0.0143),
										   R3Vec(0.3108,    0.4963,    0.9765) );

/* based on //Capilla et al - 98

A		1		1		0			L	
T   =	1		-2		0		*	M
D		1		1		-1			S

This results in values outside the range [0 1].

For display purposes:
	- A = A_result * 0.5
	- T = (T_Result + 2.0) / 3.0
	- D = (D_Result + 1.0) / 3.0

We can incorporate the scale, but not the translation into a precomputed matrix. The row 1 is scaled 
by 0.5, rows 2 and 3 are scaled by 3. The resulting matrix is: */
const R3Matrix UTILSColor::lmstoatdMatrix( R3Vec(0.5, 1.0/3.0,  1.0/3.0),
										   R3Vec(0.5, -2.0/3.0, 1.0/3.0),
										   R3Vec(0,   0,        -1.0/3.0));
//------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------

void UTILSColor::Clip( ) 
{ 
    m_adC[0] = WINminmax( m_adC[0], 0.0, 1.0 );
    m_adC[1] = WINminmax( m_adC[1], 0.0, 1.0 );
    m_adC[2] = WINminmax( m_adC[2], 0.0, 1.0 );

}//UTILSColor::Clip


//------------------------------------------------------------------------------------------

WINbool UTILSColor::operator==( const UTILSColor & in_rcoDotColor ) const
{ 
    for ( int iI = 0; iI < 3; iI++ )
        if ( in_rcoDotColor[ iI ] != m_adC[ iI ] ) return FALSE;

    return TRUE;

}//UTILSColor::operator==

//------------------------------------------------------------------------------------------

WINbool UTILSColor::operator==( UTILSColor::Color _c ) const
{ 
    for ( int iI = 0; iI < 3; iI++ )
        if ( m_adC[ iI ] != sm_aadColors[ _c ][ iI ] ) return FALSE;
   
    return TRUE;

}//UTILSColor::operator==


//------------------------------------------------------------------------------------------

UTILSColor::UTILSColor( )
{ 
    for ( int iI = 0; iI < 3; iI++ )
        m_adC[ iI ] = sm_aadColors[ UTILSColor::GREY ][ iI ];

}//UTILSColor::UTILSColor

//------------------------------------------------------------------------------------------

UTILSColor::UTILSColor( double in_dR, double in_dG, double in_dB )
{ 
    m_adC[ 0 ] = in_dR;
    m_adC[ 1 ] = in_dG;
    m_adC[ 2 ] = in_dB;

}//UTILSColor::UTILSColor

//------------------------------------------------------------------------------------------

UTILSColor::UTILSColor( const UTILSColor & in_rcoDotColor )
{ 
    for ( int iI = 0; iI < 3; iI++ )
        m_adC[ iI ] = in_rcoDotColor[ iI ];

}//UTILSColor::UTILSColor

//------------------------------------------------------------------------------------------

UTILSColor & UTILSColor::operator=( const UTILSColor & in_rcoDotColor )
{ 
    for ( int iI = 0; iI < 3; iI++ )
        m_adC[ iI ] = in_rcoDotColor[ iI ];

    return *this;

}//UTILSColor::operator=


//------------------------------------------------------------------------------------------

void UTILSColor::Print( ostream & os ) const
{ 
    os << m_adC[0] << " " << m_adC[1] << " " << m_adC[2];
}//UTILSColor::Print

//------------------------------------------------------------------------------------------

void UTILSColor::Write( ofstream & io_rstrmOut ) const
{ 
    io_rstrmOut << m_adC[ 0 ] << " " << m_adC[ 1 ] << " " << m_adC[ 2 ] << "  ";

}//UTILSColor::Write

//------------------------------------------------------------------------------------------

WINbool UTILSColor::Read( ifstream & in_rstrmIn )
{ 
    in_rstrmIn >> m_adC[ 0 ] >> m_adC[ 1 ] >> m_adC[ 2 ];

    if ( in_rstrmIn.good() ) return TRUE;
    return FALSE;

}//UTILSColor::Read

//------------------------------------------------------------------------------------------

void UTILSColor::WriteBinary( ofstream & io_rstrmOut ) const
{ 
	io_rstrmOut.write( (const char *) &m_adC[0], sizeof(double) * 3 );

}//UTILSColor::Write

//------------------------------------------------------------------------------------------

WINbool UTILSColor::ReadBinary( ifstream & in_rstrmIn )
{ 
	in_rstrmIn.read( (char *) &m_adC[0], sizeof(double) * 3 );

    return in_rstrmIn.good();

}//UTILSColor::Read

//------------------------------------------------------------------------------------------

UTILSColor UTILSColor::Index( int iI )
{
    switch ( iI % BLACK ) 
    {
        case RED : 
            return RED;
        case GREEN : 
            return GREEN;
        case BLUE : 
            return BLUE;
        case PURPLE : 
            return PURPLE;
        case YELLOW : 
            return YELLOW;
        case WHITE : 
            return WHITE;
        case GREY :
            return GREY;

    }

    return BLACK;

}//UTILSColor::Index

//------------------------------------------------------------------------------------------

WINbool UTILSColor::WriteBinary( const Array<UTILSColor> & in_aoColors, 
                                 ofstream                & io_strmOut )
{
	in_aoColors.WriteBinary(io_strmOut);

    return io_strmOut.good();
}


//------------------------------------------------------------------------------------------

WINbool UTILSColor::ReadBinary( Array<UTILSColor> & io_aoColors, 
                                ifstream          & io_strmIn )
{
	io_aoColors.ReadBinary(io_strmIn);

    return io_strmIn.good();
}


//----------------------------------------------------	
void UTILSColor::ToCMY( double &out_dC, double &out_dM, double &out_dY ) const
{
	out_dC = 1.0 - m_adC[0];
	out_dM = 1.0 - m_adC[1];
	out_dY = 1.0 - m_adC[2];
}
///
//------------------------------------------------------------------------------------------
void UTILSColor::ToYIQ( double &out_dY, double &out_dI, double &out_dQ ) const
{
	out_dY = 0.299 * m_adC[0] + 0.587 * m_adC[1] + 0.114 * m_adC[2];
	out_dI = 0.596 * m_adC[0] - 0.275 * m_adC[1] - 0.321 * m_adC[2];
	out_dQ = 0.212 * m_adC[0] - 0.523 * m_adC[1] + 0.311 * m_adC[2];
}
///
//------------------------------------------------------------------------------------------
void UTILSColor::ToHSV( double &out_dH, double &out_dS, double &out_dV ) const
{
	const double dMax = WINmax( m_adC[0], WINmax( m_adC[1], m_adC[2] ) );
	const double dMin = WINmin( m_adC[0], WINmin( m_adC[1], m_adC[2] ) );
	const double dR = m_adC[0];
	const double dG = m_adC[1];
	const double dB = m_adC[2];

	out_dV = dMax;

	/* Calculate saturation. Saturation is zero if all RGB zero */
	out_dS = ( RNIsZero( dMax ) ) ? 0.0 : ( dMax - dMin ) / dMax;

	if ( RNIsZero( out_dS ) ) {
		out_dH = 0;
	} else {
		const double dDelta = dMax - dMin;
		if ( RNApproxEqual(dR, dMax ) ) {
			out_dH = ( dG - dB ) / dDelta;
		} else if ( RNApproxEqual(dG, dMax ) ) {
			out_dH = 2.0 + (dB - dR ) / dDelta; 
		} else if ( RNApproxEqual( dB, dMax ) ) {
			out_dH = 4.0 + ( dR - dG ) / dDelta;
		} else {
			out_dH = 0;
		}

		out_dH *= 60.0; // Convert to degrees
		if ( out_dH < 0 )
			out_dH += 360;
	}
}
///
//------------------------------------------------------------------------------------------
void UTILSColor::ToHLS( double &out_dH, double &out_dL, double &out_dS ) const
{
	const double dMax = WINmax( m_adC[0], WINmax( m_adC[1], m_adC[2] ) );
	const double dMin = WINmin( m_adC[0], WINmin( m_adC[1], m_adC[2] ) );
	const double dR = m_adC[0];
	const double dG = m_adC[1];
	const double dB = m_adC[2];

	out_dL = ( dMax + dMin ) / 2.0;

	/* Calculate saturation. Saturation is zero if all RGB zero */
	if ( RNIsZero( dMax - dMin ) ) {
		out_dS = 0;
		out_dH = 0;
	} else {
		const double dDelta = dMax - dMin;

		out_dS = ( out_dL <= 0.5 ) ? (dDelta / (dMax + dMin) ) : ( dDelta / ( 2.0 - dMax - dMin ) );

		if ( RNApproxEqual(dR, dMax ) ) {
			out_dH = ( dG - dB ) / dDelta;
		} else if ( RNApproxEqual(dG, dMax ) ) {
			out_dH = 2.0 + (dB - dR ) / dDelta; 
		} else if ( RNApproxEqual( dB, dMax ) ) {
			out_dH = 4.0 + ( dR - dG ) / dDelta;
		} else {
			out_dH = 0;
		}

		out_dH *= 60.0; // Convert to degrees
		if ( out_dH < 0 )
			out_dH += 360;
	}
}
///
//------------------------------------------------------------------------------------------
void UTILSColor::ToLUV( double &out_dL, double &out_dU, double &out_dV ) const
{
	double X, Y, Z;
	ToXYZ( X, Y, Z );
	XYZtoLUV( X, Y, Z, out_dL, out_dU, out_dV );
}
///
//------------------------------------------------------------------------------------------
void UTILSColor::ToXYZ( double &out_dX, double &out_dY, double &out_dZ ) const
{
	double dR = m_adC[0];
	double dG = m_adC[1];
	double dB = m_adC[2];

	if (dR > 0.04045) {
		dR = pow(((dR + 0.055) / 1.055), 2.4);
	} else {
		dR = dR / 12.92;
	}
	if (dG > 0.04045) {
		dG = pow(((dG + 0.055) / 1.055), 2.4);
	} else {
		dG = dG / 12.92;
	}
	if (dB > 0.04045) {
		dB = pow(((dB + 0.055) / 1.055), 2.4);
	} else {
		dB = dB / 12.92;
	}

	dR = dR * 100;
	dG = dG * 100;
	dB = dB * 100;
	
	/* Note: These calculations are for Observer=2 degrees, Illuminant=D65.  
	         Refer to the CIE XYZ color system. */
	out_dX = dR * 0.4124 + dG * 0.3576 + dB * 0.1805;
	out_dY = dR * 0.2126 + dG * 0.7152 + dB * 0.0722;
	out_dZ = dR * 0.0193 + dG * 0.1192 + dB * 0.9505;
}
///
//-----------------------------------------------------------------------------------------
void UTILSColor::ToLMS( double &out_dL, double &out_dM, double &out_dS ) const
{
	// Ax = B, where B = our SML coordinates
	R3Pt out_LMS = rgbtolmsMatrix * R3Pt(m_adC[0], m_adC[1], m_adC[2]);
	
	out_dL = out_LMS[0];
	out_dM = out_LMS[1];
	out_dS = out_LMS[2];
}
///
//------------------------------------------------------------------------------------------
void UTILSColor::ToATD( double & out_dA, double & out_dT, double & out_dD) const
{
	double tempL, tempM, tempS;
	ToLMS(tempL, tempM, tempS);
	LMStoATD(tempL, tempM, tempS, out_dA, out_dT, out_dD);
}
//------------------------------------------------------------------------------------------
void UTILSColor::FromYIQ( double in_dY, double in_dI, double  in_dQ) 
{
	R3Matrix mat( R3Vec( 0.299, 0.587, 0.114 ),
				  R3Vec( 0.596, - 0.275, - 0.321 ),
			      R3Vec( 0.212, - 0.523, + 0.311 ) );

   WINbool bSuc = FALSE;
	R3Matrix matInv = mat.Inverse( bSuc );

	R3Pt pt( in_dY, in_dI, in_dQ );
	R3Pt ptRGB = matInv * pt;

	m_adC[0] = ptRGB[0];
	m_adC[1] = ptRGB[1];
	m_adC[2] = ptRGB[2];
}
///
//------------------------------------------------------------------------------------------
void UTILSColor::FromHSV( double in_dH, double in_dS, double in_dV ) 
{
	if ( RNIsZero( in_dS ) ) {
		// Acromatic case
		m_adC[0] = m_adC[1] = m_adC[2] = in_dV;
	} else {
		in_dH = WINminmax( in_dH, 0.0, 359.9999999 );

		in_dH /= 60.0;
		const int iH = (int) floor( in_dH );
		const double dH = in_dH - iH;
		const double dP = in_dV * (1.0 - in_dS);
		const double dQ = in_dV * (1.0 - in_dS * dH);
		const double dT = in_dV * (1.0 - ( in_dS * ( 1.0 - dH ) ) );

		switch ( iH ) {
		case 0 : m_adC[0] = in_dV; m_adC[1] = dT; m_adC[2] = dP; break;
		case 1 : m_adC[0] = dQ; m_adC[1] = in_dV; m_adC[2] = dP; break;
		case 2 : m_adC[0] = dP; m_adC[1] = in_dV; m_adC[2] = dT; break;
		case 3 : m_adC[0] = dP; m_adC[1] = dQ; m_adC[2] = in_dV; break;
		case 4 : m_adC[0] = dT; m_adC[1] = dP; m_adC[2] = in_dV; break;
		case 5 : m_adC[0] = in_dV; m_adC[1] = dP; m_adC[2] = dQ; break;
        default : ASSERT(FALSE);
		}
	}
}
///
//------------------------------------------------------------------------------------------

static double UTILSColorValue( double in_dN1, double in_dN2, double in_dHue )
{
	if ( in_dHue > 360.0 )
		in_dHue -= 360.0;
	else if ( in_dHue < 0 )
		in_dHue += 360.0;

	if ( in_dHue < 60.0 ) 
		return in_dN1 + ( in_dN2 - in_dN1) * in_dHue / 60.0;
	else if ( in_dHue < 180.0 ) 
		return in_dN2;
	else if ( in_dHue < 240.0 ) 
		return in_dN1 + ( in_dN2 - in_dN1) * (240.0 - in_dHue) / 60.0;
	else
		return in_dN1;

	return in_dN1;
}

void UTILSColor::FromHLS( double in_dH, double in_dL, double in_dS ) 
{
	const double dM2 = (in_dL <= 0.5 ) ? ( in_dL * (in_dL + in_dS) ) : (in_dL + in_dS - in_dL * in_dS);
	const double dM1 = 2.0 * in_dL - dM2;

	if ( RNIsZero( in_dS ) ) {
		// Acromatic case
		m_adC[0] = m_adC[1] = m_adC[2] = in_dL;
	} else {
		m_adC[0] = UTILSColorValue( dM1, dM2, in_dH + 120.0 );
		m_adC[1] = UTILSColorValue( dM1, dM2, in_dH );
		m_adC[2] = UTILSColorValue( dM1, dM2, in_dH - 120.0 );
	}
}
///
//------------------------------------------------------------------------------------------
void UTILSColor::FromCMY( double in_dC, double in_dM, double in_dY ) 
{
	m_adC[0] = 1.0 - in_dC;
	m_adC[1] = 1.0 - in_dM;
	m_adC[2] = 1.0 - in_dY;
}
///
//------------------------------------------------------------------------------------------
void UTILSColor::FromLUV( double in_dL, double in_dU, double in_dV )
{
	double X, Y, Z;
	LUVtoXYZ(in_dL, in_dU, in_dV, X, Y, Z);
	FromXYZ(X, Y, Z);
}
///
//------------------------------------------------------------------------------------------
void UTILSColor::FromXYZ( double in_dX, double in_dY, double in_dZ )
{
	in_dX = in_dX / 100;
	in_dY = in_dY / 100;
	in_dZ = in_dZ / 100;

	/* Note: these calculations are for Observer=2 degrees, Illuminant=D65.  
	         Refer to the CIE XYZ color system.*/
	double dR = in_dX *  3.2406 + in_dY * -1.5372 + in_dZ * -0.4986;
	double dG = in_dX * -0.9689 + in_dY *  1.8758 + in_dZ *  0.0415;
	double dB = in_dX *  0.0557 + in_dY * -0.2040 + in_dZ *  1.0570;

	if ( dR > 0.0031308 ) {
		dR = 1.055 * pow( dR, ( 1 / 2.4 )) - 0.055;
	} else {
		dR = 12.92 * dR;
	}
	if ( dG > 0.0031308 ) {
		dG = 1.055 * pow( dG, ( 1 / 2.4 )) - 0.055;
	} else {               
		dG = 12.92 * dG;
	}
	if ( dB > 0.0031308 ) {
		dB = 1.055 * pow( dB, ( 1 / 2.4 )) - 0.055;
	} else {
		dB = 12.92 * dB;
	}

	m_adC[0] = dR;
	m_adC[1] = dG;
	m_adC[2] = dB;
	Clip();
}
///
//------------------------------------------------------------------------------------------
void UTILSColor::FromLMS( double in_dL, double in_dM, double in_dS  )
{
    const R3Pt ptRHS( in_dL, in_dM, in_dS );
	WINbool bSuc = FALSE;
    const R3Matrix matInv = rgbtolmsMatrix.Inverse( bSuc );

	VERIFY( bSuc == TRUE );

    const R3Pt pt = matInv * ptRHS;
	m_adC[0] = pt[0];
	m_adC[1] = pt[1];
	m_adC[2] = pt[2];

}
//------------------------------------------------------------------------------------------
void UTILSColor::FromATD( double in_dA, double in_dT, double in_dD  )
{
	double tempL, tempM, tempS;
	ATDtoLMS(in_dA, in_dT, in_dD, tempL, tempM, tempS);
	FromLMS(tempL, tempM, tempS);

	// get rid of weird round offs:
	const double dDelta = 0.00001;
	WINbool eq = RNApproxEqual( m_adC[0], 0, dDelta );
	if ( eq ) m_adC[0] = 0;
	eq = RNApproxEqual( m_adC[1], 0, dDelta );
	if ( eq ) m_adC[1] = 0;
	eq = RNApproxEqual( m_adC[2], 0, dDelta );
	if ( eq ) m_adC[2] = 0;
}
//------------------------------------------------------------------------------------------
void UTILSColor::XYZtoLUV( double in_dX, double in_dY, double in_dZ, 
						   double &out_dL, double &out_dU, double &out_dV) const
{
	double var_U = ( 4.0 * in_dX ) / ( in_dX + ( 15 * in_dY ) + ( 3 * in_dZ ) );
	double var_V = ( 9.0 * in_dY ) / ( in_dX + ( 15.0 * in_dY ) + ( 3 * in_dZ ) );

	double var_Y = in_dY / 100;
	if ( var_Y > 0.008856 ) {
		var_Y = pow(var_Y , ( 1.0/3.0));
	} else {
		var_Y = ( 7.787 * var_Y ) + ( 16.0 / 116.0 );
	}
	

	double ref_X =	 95.047;		 //Observer= 2∞, Illuminant= D65
	double ref_Y = 100.000;
	double ref_Z = 108.883;

	double ref_U = ( 4 * ref_X ) / ( ref_X + ( 15 * ref_Y ) + ( 3 * ref_Z ) );
	double ref_V = ( 9 * ref_Y ) / ( ref_X + ( 15 * ref_Y ) + ( 3 * ref_Z ) );

	out_dL = ( 116 * var_Y ) - 16;
	out_dU = 13 * out_dL * ( var_U - ref_U );
	out_dV = 13 * out_dL * ( var_V - ref_V );
}
///
//------------------------------------------------------------------------------------------
void UTILSColor::LUVtoXYZ( double in_dL, double in_dU, double in_dV, 
						   double &out_dX, double &out_dY, double &out_dZ) const
{
	double var_Y = ( in_dL + 16.0 ) / 116.0;
	if ( pow(var_Y, 3) > 0.008856 ) {
		var_Y = pow(var_Y, 3);
	} else {
		var_Y = ( var_Y - 16 / 116 ) / 7.787;
	}
	double ref_X =  95.047;      //Observer= 2∞, Illuminant= D65
	double ref_Y = 100.000;
	double ref_Z = 108.883;

	double ref_U = ( 4 * ref_X ) / ( ref_X + ( 15 * ref_Y ) + ( 3 * ref_Z ) );
	double ref_V = ( 9 * ref_Y ) / ( ref_X + ( 15 * ref_Y ) + ( 3 * ref_Z ) );

	double var_U = in_dU / ( 13 * in_dL ) + ref_U;
	double var_V = in_dV / ( 13 * in_dL ) + ref_V;

	out_dY = var_Y * 100;
	out_dX =  - ( 9 * out_dY * var_U ) / ( ( var_U - 4 ) * var_V  - var_U * var_V );
	out_dZ = ( 9 * out_dY - ( 15 * var_V * out_dY ) - ( var_V * out_dX ) ) / ( 3 * var_V );
}
///
//-------------------------------------------------------------------------------------
void UTILSColor::LMStoATD( const double in_dL, const double in_dM, const double in_dS, double &out_dA, double &out_dT, double &out_dD  ) const
{
	R3Pt out_ATD = lmstoatdMatrix * R3Pt(in_dL, in_dM, in_dS);

	out_dA = out_ATD[0];				//see comments for lmstoatdMatrix above
	out_dT = out_ATD[1] + (2.0/3.0);
	out_dD = out_ATD[2] + (1.0/3.0);
	
	//out_dA = out_ATD[0] * 0.5;					
	//out_dT = (out_ATD[1] + 2.0) / 3.0;
	//out_dD = (out_ATD[2] + 1.0) / 3.0;
}
///
//-------------------------------------------------------------------------------------
void UTILSColor::ATDtoLMS( const double in_dA, const double in_dT, const double in_dD, double &out_dL, double &out_dM, double &out_dS  ) const
{

	double raw_A, raw_T, raw_D;
	raw_A = in_dA;
	raw_T = in_dT - (2.0/3.0);
	raw_D = in_dD - (1.0/3.0);

	/*raw_A = in_dA / 0.5;
	raw_T = (in_dT * 3.0) - 2.0;
	raw_D = (in_dD * 3.0) - 1.0;*/

	const R3Pt ptRHS( raw_A, raw_T, raw_D );
	WINbool bSuc = FALSE;
    const R3Matrix matInv = lmstoatdMatrix.Inverse( bSuc );

	VERIFY( bSuc == TRUE );

    const R3Pt pt = matInv * ptRHS;
	out_dL = pt[0];
	out_dM = pt[1];
	out_dS = pt[2];
}
//--------------------------------------------------------------------------------------
ostream & operator<<( ostream & os, const UTILSColor & c )
{
    c.Print(os);
    return os;
}

///
//--------------------------------------------------------------------------------------
UTILSColor UTILSColor::Ramp( const double in_dValue, const double in_dMin, const double in_dMax, const RampType in_type )
{
    if( in_dMax <= in_dMin ) {
        return UTILSColor::GREY;
    }
    
    if (in_dValue < in_dMin) return UTILSColor(0,0,0);
    if (in_dValue > in_dMax) return UTILSColor(0.9,0.9,0.9);
	
    double dVal = (in_dValue - in_dMin) / (in_dMax - in_dMin);
	
    switch(in_type) {
            // Color
        case DECREASING_RGB:
            dVal = 1.0 - dVal;
        case INCREASING_RGB : {
            const int iSeg = WINminmax( (int) ( 4.0 * dVal ), 0, 3 );
            const double dFracT = WINminmax( 4.0 * dVal - iSeg, 0.0, 1.0 );
            switch( iSeg ) {
                case 0: return UTILSColor(0.0,    dFracT,       1.0);
                case 1: return UTILSColor(0.0,    1.0,          1.0 - dFracT);
                case 2: return UTILSColor(dFracT, 1.0,          0.0);
                case 3: return UTILSColor(1.0,    1.0 - dFracT, 0.0);
                    //case 4: return UTILSColor(1.0,    0.0,          dFracT);
                default: return UTILSColor::WHITE;
            }
        }
            
            // Grayscale
        case DECREASING_GREY:
            dVal = 1.0 - dVal;
        case INCREASING_GREY:
            dVal = 0.05 + (1.0 - 0.1) * dVal;
            return UTILSColor( dVal, dVal, dVal );
            
        case CONSTANT_LUMINANCE:
            const int iSeg = WINminmax( (int) ( 4.0 * dVal ), 0, 3 );
            const double dFracT = WINminmax( 4.0 * dVal - iSeg, 0.0, 1.0 );
            switch( iSeg ) {
                case 0: return UTILSColor(0.0,    dFracT,       1.0);
                case 1: return UTILSColor(0.0,    1.0,          1.0 - dFracT);
                case 2: return UTILSColor(dFracT, 1.0,          0.0);
                case 3: return UTILSColor(1.0,    1.0 - dFracT, 0.0);
                    //case 4: return UTILSColor(1.0,    0.0,          dFracT);
                default: return UTILSColor::WHITE;
            }
    }
    return UTILSColor::GREY;
}

///
//--------------------------------------------------------------------------------------
UTILSColor UTILSColor::RampMiddle( const double in_dValue, const double in_dMin, const double in_dMiddle, const double in_dMax, const RampType in_type )
{
    if( in_dMax <= in_dMin ) {
        return UTILSColor::GREY;
    }
    
    double dValNeg = (in_dValue - in_dMin) / (in_dMiddle - in_dMin);
    double dValPos = (in_dValue - in_dMiddle) / (in_dMax - in_dMiddle);
    double dVal = ( in_dValue < in_dMiddle ) ? dValNeg : dValPos;
    
    return Ramp( dVal, 0.0, 1.0, in_type );
}

///
UTILSColor UTILSColor::LogRamp( const double in_dValue, const double in_dMin, const double in_dMax, const RampType in_type )
{
    const double dShift = 1.0 - in_dMin;
    if ( in_dMax < in_dMin )
        return UTILSColor::GREY;
    if ( in_dValue < in_dMin || in_dValue > in_dMax )
        return Ramp( in_dValue, in_dMin, in_dMax, in_type );

    return Ramp( log( in_dValue + dShift ), log( in_dMin + dShift ), log( in_dMax + dShift ), in_type );
}

///
UTILSColor UTILSColor::LogRampMiddle( const double in_dValue, const double in_dMin, const double in_dMax, const RampType in_type )
{
    const double dMid = ( in_dMin + in_dMax ) * 0.5;
    const double dShift = - dMid; // bring to zero

    if ( in_dMax < in_dMin )
        return UTILSColor::GREY;
    if ( in_dValue < in_dMin || in_dValue > in_dMax )
        return Ramp( in_dValue, in_dMin, in_dMax, in_type );

    const double dVal = in_dValue + dShift;
    const double dEnd = log( 1.0 + (in_dMax - dMid) );

    if ( dVal < 0.0 ) 
        return Ramp( -log( 1.0 + fabs( dVal ) ), -dEnd, dEnd, in_type );

    return Ramp( log( 1.0 + dVal ), -dEnd, dEnd, in_type );
    
}

