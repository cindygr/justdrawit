/* Copyright 1995, Brown Computer Graphics Group.  All Rights Reserved. */

/* -------------------------------------------------------------------------
The basis functions for  non uniform rational bsplines
   ------------------------------------------------------------------------- */

#include "StdAfx.H"
#include <utils/Crv_Knot_vec.H>

/* -------------------------- protected Routines --------------------------- */

/* -------------------------- Private Routines ----------------------------- */

/* -------------------------------------------------------------------------
 * DESCR   :	Recursively calculate the coeficients
 *   b_i^k(t) = ((t - t_i) b_i^{k-1}(t))         / (t_{i+k-1} - t_i) + 
                ((t_{i+k} - t) b_{i+1}^{k-1}(t)) / (t_{i+k} - t_{i+1})
 * ------------------------------------------------------------------------- */
void
CRVBasis::Calc_coefs(const CRVBasis &in_ith, 
                     const CRVBasis &in_ithPlus1)
{
   RNPolynomial zero_poly(Order()+2);

   const double denom1 = m_adKnots[Order()+1] - m_adKnots[0];
   const double denom2 = m_adKnots[Order()+2] - m_adKnots[1];

   if (RNIsZero(denom1) && RNIsZero(denom2)) {
      for (int seg_i = 0; seg_i < Order()+2; seg_i++) 
        for (int poly_i = 0; poly_i < Order()+1; poly_i++) 
          Poly(seg_i)[poly_i] = 0;
      return;
   }
   
   if (!RNIsZero(denom1)) {
      const RNPolynomial &ith_poly = in_ith.Poly(0);
      RNPolynomial &poly = Poly(0);      
      int poly_i;
      for (poly_i = 0; poly_i < Order()+1; poly_i++) {
         poly[poly_i+1] = ith_poly[poly_i] / denom1;
      }

      for (poly_i = 0; poly_i < Order()+1; poly_i++) {
         poly[poly_i] += - m_adKnots[0] * ith_poly[poly_i] / denom1;
      }
   } else {
      for (int poly_i = 0; poly_i < Order()+1; poly_i++) 
         Poly(0)[poly_i+1] = 0;
   }

   const double d1 = (RNIsZero(denom1)) ? 1.0 : denom1;
   const double d2 = (RNIsZero(denom2)) ? 1.0 : denom2;
   for (int seg_i = 1; seg_i < Order()+1; seg_i++) {
       const RNPolynomial &iPolySeg = in_ith.Poly(seg_i);
       const RNPolynomial &iPolyPlus1Seg = in_ithPlus1.Poly(seg_i-1);
       WINbool bIsZero1 = (RNIsZero(denom1) == TRUE ) ? TRUE : FALSE;
       WINbool bIsZero2 = (RNIsZero(denom2) == TRUE ) ? TRUE : FALSE;
       RNPolynomial ith_poly = iPolySeg;
       if ( bIsZero1 ) ith_poly = zero_poly;
       RNPolynomial ithPlus1_poly = iPolyPlus1Seg;
       if ( bIsZero2 ) ithPlus1_poly = zero_poly;

      RNPolynomial &poly = Poly(seg_i);      
      int poly_i;
      for (poly_i = 0; poly_i < Order()+1; poly_i++) 
         poly[poly_i+1] = (ith_poly[poly_i] / d1 - 
                           ithPlus1_poly[poly_i] / d2);

      for (poly_i = 0; poly_i < Order()+1; poly_i++) 
         poly[poly_i] += (- m_adKnots[0] * ith_poly[poly_i] / d1
                          + m_adKnots[Order()+2] * ithPlus1_poly[poly_i] / d2);
   }

   if (!RNIsZero(denom2)) {
      const RNPolynomial &ithPlus1_poly = in_ithPlus1.Poly(Order());
      RNPolynomial &poly = Poly(Order()+1);      
      int poly_i;
      for (poly_i = 0; poly_i < Order()+1; poly_i++) 
         poly[poly_i+1] = - ithPlus1_poly[poly_i] / denom2;

      for (poly_i = 0; poly_i < Order()+1; poly_i++) 
         poly[poly_i] += m_adKnots[Order()+2] * ithPlus1_poly[poly_i] / denom2;
   } else {
      for (int poly_i = 0; poly_i < Order()+1; poly_i++) 
         Poly(Order()+1)[poly_i+1] = 0;
   }
}


/* -------------------------------------------------------------------------
 * DESCR   :	Multiply by coefs
 * ------------------------------------------------------------------------- */
double
CRVBasis::operator()(double in_dT)
const
{
   // Only valid on interval
   if (in_dT <= Start_t() ) return 0;
   if (in_dT >= End_t() ) return 0;

   // Figure out which segment
   int seg_i = (in_dT < 0.5 * (Start_t() + End_t())) ? 0 : Num_knots()-2;
   for (int i = 0; i < Num_knots()-1; i++) {
      if (in_dT >= Knot(i) - RNEpsilon_d && in_dT <= Knot(i+1) + RNEpsilon_d
          && !RNApproxEqual(Knot(i), Knot(i+1))) {
         seg_i = i;
         break;
      }
   }
   const double dEval = Segment(seg_i)(in_dT);
   
   return WINminmax( dEval, 0.0, 1.0 );
}

/* -------------------------------------------------------------------------
 * DESCR   :	Multiply by coefs
 * ------------------------------------------------------------------------- */
double
CRVBasis::Derivative(double in_dT)
const
{
   if (Order() < 0) {
      cerr << "Taking derivative of C^-1 curve\n";
      return 0;
   }
   
   // Only valid on inteval
   if (in_dT < Start_t() - RNEpsilon_d) return 0;
   if (in_dT > End_t() + RNEpsilon_d) return 0;

   // Figure out which segment
   int seg_i = -1;
   for (int i = 0; i < Num_knots()-1; i++)
      if (in_dT >= Knot(i) - RNEpsilon_d && in_dT <= Knot(i+1) + RNEpsilon_d) {
         seg_i = i;
         break;
      }
   
   // Just in case - since we have no segment
   if (seg_i == -1) return 0;
   
   if (Order() == 0)
      if (RNIsZero(in_dT - Knot(seg_i))) {
         cerr << "Taking derivative of C^0 curve at knot\n";
      }

   // Take derivative
   return Segment(seg_i).Differentiate()(in_dT);
}

/* -------------------------------------------------------------------------
 * DESCR   :	Multiply by coefs
 * ------------------------------------------------------------------------- */
double
CRVBasis::SecondDerivative(double in_dT)
const
{
   if (Order() < 1) {
      cerr << "Taking second derivative of C^0 curve\n";
      return 0;
   }
   
   // Only valid on inteval
   if (in_dT < Start_t() - RNEpsilon_d) return 0;
   if (in_dT > End_t() + RNEpsilon_d) return 0;

   // Figure out which segment
   int seg_i = -1;
   for (int i = 0; i < Num_knots()-1; i++)
      if (in_dT >= Knot(i) - RNEpsilon_d && in_dT <= Knot(i+1) + RNEpsilon_d) {
         seg_i = i;
         break;
      }
   
   // Just in case - since we have no segment
   if (seg_i == -1) return 0;
   
   if (Order() <= 1)
      if (RNIsZero(in_dT - Knot(seg_i))) {
         cerr << "Taking second derivative of C^1 curve at knot\n";
      }

   // Take derivative
   return Segment(seg_i).Differentiate().Differentiate()(in_dT);
}

void CRVBasis::AllEval( const double in_dT, 
                        double &out_dEval, double &out_dDeriv, double &out_dSecondDeriv ) const
{
    out_dEval = out_dDeriv = out_dSecondDeriv = 0.0;

   // Only valid on interval
   if (in_dT <= Start_t() ) return;
   if (in_dT >= End_t() ) return;

   // Figure out which segment
   int seg_i = (in_dT < 0.5 * (Start_t() + End_t())) ? 0 : Num_knots()-2;
   for (int i = 0; i < Num_knots()-1; i++) {
      if (in_dT >= Knot(i) - RNEpsilon_d && in_dT <= Knot(i+1) + RNEpsilon_d
          && !RNApproxEqual(Knot(i), Knot(i+1))) {
         seg_i = i;
         break;
      }
   }
   if ( seg_i == -1 )
       return;

   out_dEval = Segment(seg_i)(in_dT);
   const RNPolynomial poly = Segment( seg_i ).Differentiate();
   out_dDeriv = poly( in_dT );
   out_dSecondDeriv = poly.Differentiate()( in_dT );
   
   out_dEval = WINminmax( out_dEval, 0.0, 1.0 );
}

void CRVBasis::AllEval( const double in_dT, 
                        double &out_dEval, double &out_dDeriv ) const
{
    out_dEval = out_dDeriv = 0.0;

   // Only valid on interval
   if (in_dT <= Start_t() ) return;
   if (in_dT >= End_t() ) return;

   // Figure out which segment
   int seg_i = (in_dT < 0.5 * (Start_t() + End_t())) ? 0 : Num_knots()-2;
   for (int i = 0; i < Num_knots()-1; i++) {
      if (in_dT >= Knot(i) - RNEpsilon_d && in_dT <= Knot(i+1) + RNEpsilon_d
          && !RNApproxEqual(Knot(i), Knot(i+1))) {
         seg_i = i;
         break;
      }
   }
   if ( seg_i == -1 )
       return;

   out_dEval = Segment(seg_i)(in_dT);
   const RNPolynomial poly = Segment( seg_i ).Differentiate();
   out_dDeriv = poly( in_dT );
   
   out_dEval = WINminmax( out_dEval, 0.0, 1.0 );
}


CRVBasis CRVBasis::Derivative() const
{
    CRVBasis out_basis( *this );

    for (int i = 0; i < out_basis.m_aPolys.num(); i++)
        out_basis.m_aPolys[i] = m_aPolys[i].Differentiate();

    return out_basis;
}


double 
CRVBasis::Integrate(double in_dT0, double in_dT1) const
{
   // Only valid on inteval
   if (in_dT1 < Start_t() + RNEpsilon_d) return 0;
   if (in_dT0 > End_t() + RNEpsilon_d) return 0;

   // Figure out which segment(s)
   double out_dSum = 0;
   for (int i = 0; i < Order()+2; i++) {
      // Check if segment contained
      double k1 = Knot(i) + RNEpsilon_d;
      double k2 = Knot(i+1) + RNEpsilon_d;

      if (in_dT0 <= k1) {
         if (in_dT1 > k2) {
            out_dSum += Segment(i).Integrate(k1, k2);
         } else if (in_dT1 <= k2 && in_dT1 > k1) {
            out_dSum += Segment(i).Integrate(k1, in_dT1);
         }
      } else if (in_dT0 >= k1 && in_dT0 < k2) {
         if (in_dT1 > k2) {
            out_dSum += Segment(i).Integrate(in_dT0, k2);
         } else if (in_dT1 <= k2) {
            out_dSum += Segment(i).Integrate(in_dT0, in_dT1);
         }
      }
   }

   return out_dSum;
}

/* -------------------------------------------------------------------------
 * DESCR   :	Set the knot vector to m_adKnots[start_i] .. m_adKnots[start_i+order+1]
 * ------------------------------------------------------------------------- */
void
CRVBasis::Change_knot(const Array<double> &in_adKnots,
                      int in_iStart)
{
   int i;
   for ( i = 0; i < m_adKnots.num(); i++)
     m_adKnots[i] = in_adKnots[in_iStart + i];

   Array<double> knot_ith(Num_knots()-1), knot_ithPlus1(Num_knots()-1);
   
   // If Order == -1, then coefs are just one
   if (Order() == -1) {
      Poly(0)[0] = 1;
      return;
   }
   
   // Set the vectors to i..order and i+1..order+1
   for (i = 0; i < knot_ith.num(); i++) {
      knot_ith[i] = Knot(i);
      knot_ithPlus1[i] = Knot(i+1);
   }

   // Make basis functions of 1 less order
   CRVBasis ith(Order()-1, knot_ith, 0);
   CRVBasis ithPlus1(Order()-1, knot_ithPlus1, 0);

   // Zero out our coefs vec 
   RNPolynomial zero_poly(Order()+2);
   for (i = 0; i < Order()+2; i++) 
      Poly(i) = zero_poly;

   Calc_coefs(ith, ithPlus1);
}

/* -------------------------------------------------------------------------
 * DESCR   :	Compare knot values
 * ------------------------------------------------------------------------- */
WINbool CRVBasis::operator==(const CRVBasis &in_crvBasis) const
{
    if ( m_adKnots.num() != in_crvBasis.m_adKnots.num() )
        return FALSE;

    for ( int i = 0; i < m_adKnots.num(); i++ ) 
        if ( !RNApproxEqual( m_adKnots[i], in_crvBasis.m_adKnots[i] ) )
            return FALSE;

    return TRUE;
}

/* -------------------------------------------------------------------------
 * DESCR   :	Copy a nurb's basis function
 * ------------------------------------------------------------------------- */
CRVBasis &
CRVBasis::operator=(const CRVBasis &in_crvbasis)
{
   m_iOrder = in_crvbasis.Order();
   m_adKnots = in_crvbasis.m_adKnots;

   m_aPolys = in_crvbasis.m_aPolys;
   
   return *this;
}

int CRVBasis::m_iDefaultConstructOrder = 0;
CRVBasis::CRVBasis() : 
    CRVSpace(m_iDefaultConstructOrder), 
    m_adKnots(m_iDefaultConstructOrder+3), 
    m_aPolys(Order()+2)
{
   for (int i = 0; i < Order()+2; i++) 
     m_aPolys[i] = RNPolynomial(Order()+2);
}

CRVBasis::CRVBasis(const CRVBasis &in_crvbasis) 
: CRVSpace(in_crvbasis), m_adKnots(in_crvbasis.m_adKnots)
{
   m_aPolys = in_crvbasis.m_aPolys;
}

/* -------------------------------------------------------------------------
 * DESCR   :	Make a nurbs basis function with the given knot vector
 * ------------------------------------------------------------------------- */
CRVBasis::CRVBasis(int in_iOrder,
                   const Array<double> &in_adKnot,
                   int in_iStartArrayIndex
   ) : CRVSpace(in_iOrder), m_adKnots(in_iOrder+3), m_aPolys(Order()+2)
{
   for (int i = 0; i < Order()+2; i++)
     m_aPolys[i] = RNPolynomial(Order()+2);

   // Make basis functions of one degree less and recursively calculate
   // coefficients
   Change_knot(in_adKnot, in_iStartArrayIndex);
}

/* -------------------------------------------------------------------------
 * DESCR   :	Make a basis function with support from left_end to right_end,
 *              with uniform spacing.
 * ------------------------------------------------------------------------- */
CRVBasis::CRVBasis(int in_iOrder,
                   double in_dLeft_end,
                   double in_dRight_end
   ) : CRVSpace(in_iOrder), m_adKnots(in_iOrder+3), m_aPolys(Order()+2)
{
   double div = (in_dRight_end - in_dLeft_end) / (double) (Order()+2);
   int i;
   for (i = 0; i < Order()+3; i++)
     m_adKnots[i] = in_dLeft_end + (double) i * div;

   for (i = 0; i < Order()+2; i++) 
      m_aPolys[i] = RNPolynomial(Order()+2);

   // Make basis functions of one degree less and recursively calculate
   // coefficients
   Change_knot(m_adKnots, 0);
}


/* -------------------------------------------------------------------------
 * DESCR   :	Print out knot vector
 * ------------------------------------------------------------------------- */
void
CRVBasis::Print()
const
{
   cout << "Knot dim " << m_adKnots.num() << " knot values ";
   int i;
   for ( i = 0; i < m_adKnots.num(); i++)
      cout << m_adKnots[i] << " ";

   cout << "\n";

   cout << "Coef values \n";
   for (i = 0; i < Order()+2; i++) 
      Poly(i).Print();

   cout << "\n";
    
}

void CRVBasis::Write( ofstream &out ) const
{
    out << m_adKnots.num() << " " << m_iOrder << "\n";
    for ( int i = 0; i < m_adKnots.num(); i++ )
        out << m_adKnots[i] << " ";
    out << "\n";
}

void CRVBasis::WriteBinary( ofstream &out ) const
{
    const int iN = m_adKnots.num();
    out.write( (const char *) &iN, sizeof(int) );
    out.write( (const char *) &m_iOrder, sizeof(int) );
    out.write( (const char *) & m_adKnots[0], sizeof( double ) * m_adKnots.num() );
}

void CRVBasis::Read( ifstream &in ) 
{
    int iN;
    in >> iN >> m_iOrder;

    Array<double> adK(iN);
    for ( int i = 0; i < adK.num(); i++ )
        in >> adK[i];

    (*this) = CRVBasis( m_iOrder, adK, 0 );
}

void CRVBasis::ReadBinary( ifstream &in ) 
{
    int iN;
    in.read( (char *) &iN, sizeof(int) );
    in.read( (char *) &m_iOrder, sizeof(int) );

    Array<double> adK(iN);
    in.read( (char *) &adK[0], sizeof( double ) * iN );

    (*this) = CRVBasis( m_iOrder, adK, 0 );
}
