#include "StdAfx.H"
#include <utils/Crv_Nurb_sampledTC.H>

template<>
WINbool CRVNurb_sampledTC<R3Pt, R3Vec, R3Line_seg  >::test()
{
   CRVNub_sampled3TC crv1(CRVSpace(2), 6);

   crv1.Pt(0) = R3Pt (0,0,0);
   crv1.Pt(1) = R3Pt (1,1,0);
   crv1.Pt(2) = R3Pt (2,2,0);
   crv1.Pt(3) = R3Pt (2,3,0);
   crv1.Pt(4) = R3Pt (1,4,0);
   crv1.Pt(5) = R3Pt (0,5,0);

   crv1.Resample(0.2, 0.2);

   ofstream outT("f:\\facedata\\debug\\cindy\\test_crv.txt", ios::out);
   Array<R3Pt > pts;
   Array<double> ts;
   crv1.Samples(pts);
   int i;
   for (i = 0; i < pts.num(); i++) {
      double t = 0;
      double dist = ::Length(pts[i] - crv1.Closest(pts[i], t));
      ts += t;
      ASSERT(RNIsZero(dist));
      pts[i].Write( outT );
      outT << "\n";
   }
   outT.close();
   for (i = 1; i < ts.num(); i++)
       ASSERT(ts[i] > ts[i-1]);

   CRVNub_sampled3TC crv2(crv1);

   crv2.Samples(pts, 1.0, 2.0);
   ts.clearcompletely();
   for (i = 0; i < pts.num(); i++) {
      double t = 0;
      double dist = ::Length(pts[i] - crv1.Closest(pts[i], t));
      ASSERT(t >= 1.0 && t <= 2.0);
      ts += t;
      ASSERT(RNIsZero(dist));
   }
   for (i = 1; i < ts.num(); i++)
       ASSERT(ts[i] > ts[i-1]);
   

   return TRUE;
}


template<>
WINbool CRVNurb_sampledTC<R2Pt, R2Vec, R2Line_seg >::test()
{
   CRVNub_sampled2TC crv1(CRVSpace(2), 6);

   crv1.Pt(0) = R2Pt (0,0);
   crv1.Pt(1) = R2Pt (1,1);
   crv1.Pt(2) = R2Pt (2,2);
   crv1.Pt(3) = R2Pt (2,3);
   crv1.Pt(4) = R2Pt (1,4);
   crv1.Pt(5) = R2Pt (0,5);

   crv1.Resample(0.2, 0.2);

   Array<R2Pt > pts;
   Array<double> ts;
   crv1.Samples(pts);
   int i;
   for ( i = 0; i < pts.num(); i++) {
      double t = 0;
      double dist = ::Length(pts[i] - crv1.Closest(pts[i], t));
      ts += t;
      ASSERT(RNIsZero(dist));
   }
   for (i = 1; i < ts.num(); i++)
       ASSERT(ts[i] > ts[i-1]);

   CRVNub_sampled2TC crv2(crv1);

   crv2.Samples(pts, 1.0, 2.0);
   ts.clearcompletely();
   for (i = 0; i < pts.num(); i++) {
      double t = 0;
      double dist = ::Length(pts[i] - crv1.Closest(pts[i], t));
      ASSERT(t >= 1.0 && t <= 2.0);
      ts += t;
      ASSERT(RNIsZero(dist));
   }
   for (i = 1; i < ts.num(); i++)
       ASSERT(ts[i] > ts[i-1]);
   

   return TRUE;
}



