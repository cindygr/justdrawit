/* Copyright 1995, Brown Computer Graphics Group.  
  All Rights Reserved. */

/* ------------------------------------------------------------
  A non uniform rational bspline
  Operations implemented:
      refinement
      sub curve
   ------------------------------------------------------------ */

#include "StdAfx.H"
#include <utils/Crv_NurbTC.H>

template<>
WINbool CRVNurbTC<R3Pt, R3Pt, R3Vec >::Test()
{
   CRVNub3TC crv1(CRVSpace(2), 6), crv2(CRVSpace(3), 10);

   crv1.Pt(0) = R3Pt (0,0,0);
   crv1.Pt(1) = R3Pt (1,1,0);
   crv1.Pt(2) = R3Pt (2,2,0);
   crv1.Pt(3) = R3Pt (2,3,0);
   crv1.Pt(4) = R3Pt (1,4,0);
   crv1.Pt(5) = R3Pt (0,5,0);

   CRVNub3TC crv3(crv1);
   VERIFY(crv3 == crv1);
   
   VERIFY(crv1.InDim() == 3);
   VERIFY(crv1.OutDim() == 3);

   VERIFY(crv1.Start_t() == 0.0);
   VERIFY(crv1.End_t() == 3.0);

   R3Pt out_pt = crv1(1.5);
   VERIFY(RNApproxEqual(out_pt[0], 1.9583333333333333333));
   VERIFY(RNApproxEqual(out_pt[1], 2.5));
   VERIFY(RNApproxEqual(out_pt[2], 0));

   R3Vec v1, v2, v3;
   crv1.Frame(0.5, v1, v2,v3);

   RNEpsilon_d = 1e-10;
   double t;
   for (t = 0.0; t < 3.0; t += 0.1) {
      int seg = crv1.Floor_knot(t);
      R3Pt p1 = crv1(t);
      R3Pt p2 = crv1.Perc(t / 3.0);
      R3Pt p3 = crv1.PercSegment(seg, t - seg);
      VERIFY(ApproxEqual(p1, p2));
      VERIFY(ApproxEqual(p1, p3));
   }

   Array<double> ts = crv1.Even_spacing(30);
   double len = crv1.Length() / 30.0;
   int i;
   for ( i = 1; i < ts.num(); i++) {
       VERIFY(ts[i] > ts[i-1]);
       R3Vec vec = crv1(ts[i]) - crv1(ts[i-1]);
       double d = ::Length(vec);
       ASSERT(d > 0.95 * len && d < 1.05 * len);
   }

   R3Vec out_vec = crv1.Derivative(1.5);
   VERIFY(RNApproxEqual(out_vec[0], 0));
   VERIFY(RNApproxEqual(out_vec[1], 1.0));
   VERIFY(RNApproxEqual(out_vec[2], 0.0));

   crv1.Add_points(3, 3);
   crv1.Pt(3) = R3Pt (2,2,0);
   crv1.Pt(4) = R3Pt (2,2,0);
   crv1.Pt(5) = R3Pt (2,2,0);
   R3Pt pt = crv1(0.5);
   VERIFY(ApproxEqual(pt, crv1.Pt(3)));

   crv2 = crv1;
   crv1.Add_points(1, 0);
   crv1.Add_points(1, crv1.Num_pts());

   CRVNub3TC crv4 = crv1.Sub_crv(0.1, 2.5);
   crv1.Print();
   crv4.Print();
   VERIFY(crv4.Start_t() == 0.1);
   VERIFY(RNApproxEqual(crv4.End_t(), 2.5));
   for (t = 0.1; t < 2.5; t += 0.1) {
       VERIFY(ApproxEqual(crv4(t), crv1(t)));
   }

   crv1.Remove_points(1, 0);
   crv1.Remove_points(1, crv1.Num_pts()-1);
   for (i = 0; i < crv1.Num_pts(); i++)
     VERIFY(ApproxEqual(crv1.Pt(i), crv2.Pt(i)));

   t = crv1.Closest_t(R3Pt (3, 2, 0));
   VERIFY(ApproxEqual(crv1(t), crv1.Pt(3)));
   VERIFY(ApproxEqual(crv1.Closest_pt(R3Pt (3, 2, 0)), crv1.Pt(3)));

   RNEpsilon_d = 1e-15;
   ofstream out("RNtest.txt", ios::out);
   crv1.Write(out);
   out.close();
   ifstream in("RNtest.txt", ios::in);
   VERIFY(crv2.Read(in));
   in.close();
   VERIFY(crv1 == crv2);

   in.open("RNtest.txt", ios::in);
   CRVNub3TC crv6(in, (WINbool) FALSE);
   in.close();
   VERIFY(crv1 == crv6);

   crv2.Print();

   WINWriteBinary("RNtest.bin", out);
   crv1.Write_binary(out);
   out.close();
   WINReadBinary("RNtest.bin", in);
   VERIFY(crv3.Read_binary(in));
   in.close();
   VERIFY(crv1 == crv3);

   WINReadBinary("RNtest.bin", in);
   CRVNub3TC crv5(in, (WINbool) TRUE);
   in.close();
   VERIFY(crv1 == crv5);

   crv5 = crv1.Derivative();
   return FALSE;
}

template<>
WINbool CRVNurbTC<R2Pt, R2Pt, R2Vec >::Test()
{
   CRVNub2TC crv1(CRVSpace(2), 6), crv2(CRVSpace(3), 10);

   crv1.Pt(0) = R2Pt (0,0);
   crv1.Pt(1) = R2Pt (1,1);
   crv1.Pt(2) = R2Pt (2,2);
   crv1.Pt(3) = R2Pt (2,3);
   crv1.Pt(4) = R2Pt (1,4);
   crv1.Pt(5) = R2Pt (0,5);

   CRVNub2TC crv3(crv1);
   VERIFY(crv3 == crv1);
   
   VERIFY(crv1.InDim() == 2);
   VERIFY(crv1.OutDim() == 2);

   VERIFY(crv1.Start_t() == 0.0);
   VERIFY(crv1.End_t() == 3.0);

   R2Pt out_pt = crv1(1.5);
   VERIFY(RNApproxEqual(out_pt[0], 1.9583333333333333333));
   VERIFY(RNApproxEqual(out_pt[1], 2.5));

   RNEpsilon_d = 1e-10;
   double t;
   for ( t = 0.0; t < 3.0; t += 0.1) {
      int seg = crv1.Floor_knot(t);
      R2Pt p1 = crv1(t);
      R2Pt p2 = crv1.Perc(t / 3.0);
      R2Pt p3 = crv1.PercSegment(seg, t - seg);
      VERIFY(ApproxEqual(p1, p2));
      VERIFY(ApproxEqual(p1, p3));
   }

   Array<double> ts = crv1.Even_spacing(30);
   double len = crv1.Length() / 30.0;
   int i;
   for ( i = 1; i < ts.num(); i++) {
      VERIFY(ts[i] > ts[i-1]);
      R2Vec vec = crv1(ts[i]) - crv1(ts[i-1]);
      double d = ::Length(vec);
      VERIFY((d > 0.95 * len && d < 1.05 * len) ? TRUE : FALSE);
   }

   R2Vec out_vec = crv1.Derivative(1.5);
   VERIFY(RNApproxEqual(out_vec[0], 0));
   VERIFY(RNApproxEqual(out_vec[1], 1.0));

   crv1.Add_points(3, 3);
   crv1.Pt(3) = R2Pt (2,2);
   crv1.Pt(4) = R2Pt (2,2);
   crv1.Pt(5) = R2Pt (2,2);
   R2Pt pt = crv1(0.5);
   VERIFY(ApproxEqual(pt, crv1.Pt(3)));

   crv2 = crv1;
   crv1.Add_points(1, 0);
   crv1.Add_points(1, crv1.Num_pts());
   CRVNub2TC crv4 = crv1.Sub_crv(0.1, 2.5);
   crv1.Print();
   crv4.Print();
   VERIFY(crv4.Start_t() == 0.1);
   VERIFY(RNApproxEqual(crv4.End_t(), 2.5));
   for (t = 0.1; t < 2.5; t += 0.1) {
      VERIFY(ApproxEqual(crv4(t), crv1(t)));
   }

   crv1.Remove_points(1, 0);
   crv1.Remove_points(1, crv1.Num_pts()-1);
   for (i = 0; i < crv1.Num_pts(); i++)
     VERIFY(ApproxEqual(crv1.Pt(i), crv2.Pt(i)));

   t = crv1.Closest_t(R2Pt (3, 2));
   VERIFY(ApproxEqual(crv1(t), crv1.Pt(3)));
   VERIFY(ApproxEqual(crv1.Closest_pt(R2Pt (3, 2)), crv1.Pt(3)));

   RNEpsilon_d = 1e-15;
   ofstream out("RNtest.txt", ios::out);
   crv1.Write(out);
   out.close();
   ifstream in("RNtest.txt", ios::in);
   VERIFY(crv2.Read(in));
   in.close();
   VERIFY(crv1 == crv2);

   in.open("RNtest.txt", ios::in);
   CRVNub2TC crv6(in, (WINbool) FALSE);
   in.close();
   VERIFY(crv1 == crv6);

   crv2.Print();

   WINWriteBinary("RNtest.bin", out);
   crv1.Write_binary(out);
   out.close();
   WINReadBinary("RNtest.bin", in);
   CRVNub2TC crv5(in, (WINbool) TRUE);
   in.close();
   VERIFY(crv1 == crv5);

   WINReadBinary("RNtest.bin", in);
   VERIFY(crv3.Read_binary(in));
   in.close();
   VERIFY(crv3 == crv5);
   crv5 = crv1.Derivative();

   return FALSE;
}

template<>
WINbool CRVNurbTC<R1Pt, R1Pt, R1Vec >::Test()
{
   CRVNub1TC crv1(CRVSpace(2), 6), crv2(CRVSpace(3), 10);

   crv1.Pt(0) = R1Pt (0);
   crv1.Pt(1) = R1Pt (1);
   crv1.Pt(2) = R1Pt (2);
   crv1.Pt(3) = R1Pt (2);
   crv1.Pt(4) = R1Pt (1);
   crv1.Pt(5) = R1Pt (0);

   CRVNub1TC crv3(crv1);
   VERIFY(crv3 == crv1);
   
   VERIFY(crv1.InDim() == 1);
   VERIFY(crv1.OutDim() == 1);

   VERIFY(crv1.Start_t() == 0.0);
   VERIFY(crv1.End_t() == 3.0);

   R1Pt out_pt = crv1(1.5);
   VERIFY(RNApproxEqual(out_pt[0], 1.9583333333333333333));

   RNEpsilon_d = 1e-10;
   double t;
   for ( t = 0.0; t < 3.0; t += 0.1) {
      int seg = crv1.Floor_knot(t);
      R1Pt p1 = crv1(t);
      R1Pt p2 = crv1.Perc(t / 3.0);
      R1Pt p3 = crv1.PercSegment(seg, t - seg);
      VERIFY(ApproxEqual(p1, p2));
      VERIFY(ApproxEqual(p1, p3));
   }

   Array<double> ts = crv1.Even_spacing(30);
   double len = crv1.Length() / 30.0;
   int i;
   for ( i = 1; i < ts.num(); i++) {
      VERIFY(ts[i] > ts[i-1]);
      R1Vec vec = crv1(ts[i]) - crv1(ts[i-1]);
      double d = ::Length(vec);
      VERIFY(d > 0.95 * len && d < 1.05 * len);
   }

   R1Vec out_vec = crv1.Derivative(1.5);
   VERIFY(RNApproxEqual(out_vec[0], 0));

   crv1.Add_points(3, 3);
   crv1.Pt(3) = R1Pt (2);
   crv1.Pt(4) = R1Pt (2);
   crv1.Pt(5) = R1Pt (2);
   R1Pt pt = crv1(0.5);
   VERIFY(ApproxEqual(pt, crv1.Pt(3)));

   crv2 = crv1;
   crv1.Add_points(1, 0);
   crv1.Add_points(1, crv1.Num_pts());

   CRVNub1TC crv4 = crv1.Sub_crv(0.1, 2.5);
   crv1.Print();
   crv4.Print();
   VERIFY(crv4.Start_t() == 0.1);
   VERIFY(RNApproxEqual(crv4.End_t(), 2.5));
   for (t = 0.1; t < 2.5; t += 0.1) {
      VERIFY(ApproxEqual(crv4(t), crv1(t)));
   }

   crv1.Remove_points(1, 0);
   crv1.Remove_points(1, crv1.Num_pts()-1);
   for (i = 0; i < crv1.Num_pts(); i++)
     VERIFY(ApproxEqual(crv1.Pt(i), crv2.Pt(i)));

   t = crv1.Closest_t(R1Pt (3));
   VERIFY(ApproxEqual(crv1(t), crv1.Pt(3)));
   VERIFY(ApproxEqual(crv1.Closest_pt(R1Pt (3)), crv1.Pt(3)));

   RNEpsilon_d = 1e-15;
   ofstream out("RNtest.txt", ios::out);
   crv1.Write(out);
   out.close();
   ifstream in("RNtest.txt", ios::in);
   VERIFY(crv2.Read(in));
   in.close();
   VERIFY(crv1 == crv2);

   in.open("RNtest.txt", ios::in);
   CRVNub1TC crv6(in, (WINbool) FALSE);
   in.close();
   VERIFY(crv1 == crv6);

   crv2.Print();

   WINWriteBinary("RNtest.bin", out);
   crv1.Write_binary(out);
   out.close();
   WINReadBinary("RNtest.bin", in);
   CRVNub1TC crv5(in, (WINbool) TRUE);
   in.close();
   VERIFY(crv1 == crv5);

   WINReadBinary("RNtest.bin", in);
   VERIFY(crv3.Read_binary(in));
   in.close();
   VERIFY(crv3 == crv5);
   crv5 = crv1.Derivative();

   return FALSE;
}


void CRVTestCrvs()
{

	CRVNub2TC crv( CRVSpace(2), 6);

	crv.Test();    

	CRVNub3TC crv3( CRVSpace(2), 6);

	crv3.Test();    

     CRVNub1TC crv1( CRVSpace(2), 6);

     crv1.Test();    

}

