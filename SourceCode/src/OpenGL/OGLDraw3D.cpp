#include <OpenGL/OGLRoutines.H>
#include <OpenGL/OGLDraw3D.H>
#include <utils/Rn_Sphere.H>

static int s_iDivsTheta = 0, s_iDivsPhi = 0;
static GLuint s_iId = 0;

static void MakeSphere( const int in_iDivsTheta, const int in_iDivsPhi )
{
    const int iThetaDiv = WINmax( in_iDivsTheta, 4 );
    const int iPhiDiv = WINmax( in_iDivsPhi, 3 );
    const double dThetaDiv = 2.0 * M_PI / (double) iThetaDiv;
    const double dPhiDiv = M_PI / (double) iPhiDiv;
	
	glNewList( s_iId, GL_COMPILE );
	
    glBegin( GL_TRIANGLE_FAN );
    glNormal3d( 0,1,0 );
    glVertex3d( 0,1,0);
    const double dPhiTopCos = cos( M_PI / 2.0 + dPhiDiv );
    const double dPhiTopSin = sin( M_PI / 2.0 + dPhiDiv );
    for (int iT = 0; iT < iThetaDiv + 1; iT++ ) {
        glNormal3d( -cos( -iT * dThetaDiv ) * dPhiTopCos, dPhiTopSin, -sin( -iT * dThetaDiv ) * dPhiTopCos );
        glVertex3d( -cos( -iT * dThetaDiv ) * dPhiTopCos, dPhiTopSin, -sin( -iT * dThetaDiv ) * dPhiTopCos );
    }
    glEnd( ); //GL_TRIANGLE_FAN
	
    glBegin( GL_TRIANGLE_FAN );
    glNormal3d( 0,-1,0 );
    glVertex3d( 0,-1,0);
    for (FORINT iT = 0; iT < iThetaDiv + 1; iT++ ) {
        glNormal3d( cos( iT * dThetaDiv ) * dPhiTopCos, -dPhiTopSin, sin( iT * dThetaDiv ) * dPhiTopCos );
        glVertex3d( cos( iT * dThetaDiv ) * dPhiTopCos, -dPhiTopSin, sin( iT * dThetaDiv ) * dPhiTopCos );
    }
    glEnd( ); //GL_TRIANGLE_FAN
	
    for ( int iP = 0; iP < iPhiDiv - 2; iP++ ) {
        const double dPhiCos1 = cos( M_PI / 2.0 + (iP+1) * dPhiDiv );
        const double dPhiSin1 = sin( M_PI / 2.0 + (iP+1) * dPhiDiv );
        const double dPhiCos2 = cos( M_PI / 2.0 + (iP+2) * dPhiDiv );
        const double dPhiSin2 = sin( M_PI / 2.0 + (iP+2) * dPhiDiv );
        glBegin( GL_TRIANGLE_STRIP );
        for (int iT = 0; iT < iThetaDiv + 1; iT++ ) {
            glNormal3d( cos( iT * dThetaDiv ) * dPhiCos1, -dPhiSin1, sin( iT * dThetaDiv ) * dPhiCos1 );
            glVertex3d( cos( iT * dThetaDiv ) * dPhiCos1, -dPhiSin1, sin( iT * dThetaDiv ) * dPhiCos1 );
            glNormal3d( cos( iT * dThetaDiv ) * dPhiCos2, -dPhiSin2, sin( iT * dThetaDiv ) * dPhiCos2 );
            glVertex3d( cos( iT * dThetaDiv ) * dPhiCos2, -dPhiSin2, sin( iT * dThetaDiv ) * dPhiCos2 );
        }
        glEnd( ); //GL_TRIANGLE_STRIP
    }
	
    glEndList();
}


static void MakeCone( const int in_iDivsTheta )
{
    const int iThetaDiv = WINmax( in_iDivsTheta, 4 );
    const double dThetaDiv = 2.0 * M_PI / (double) iThetaDiv;
	
	glNewList( s_iId+1, GL_COMPILE );
	
    glBegin( GL_TRIANGLE_FAN );
    glNormal3d( 0,1,0 );
    glVertex3d( 0,0.5,0);
    for (int iT = 0; iT < iThetaDiv + 1; iT++ ) {
        glNormal3d( -cos( -iT * dThetaDiv ), -0.5, -sin( -iT * dThetaDiv ) );
        glVertex3d( -cos( -iT * dThetaDiv ), -0.5, -sin( -iT * dThetaDiv ) );
    }
    glEnd( ); //GL_TRIANGLE_FAN
	
    glBegin( GL_TRIANGLE_FAN );
    glNormal3d( 0,-1,0 );
    glVertex3d( 0,-0.5,0);
    for (FORINT iT = 0; iT < iThetaDiv + 1; iT++ ) {
        glNormal3d( cos( iT * dThetaDiv ), -0.5, sin( iT * dThetaDiv ) );
        glVertex3d( cos( iT * dThetaDiv ), -0.5, sin( iT * dThetaDiv ) );
    }
    glEnd( ); //GL_TRIANGLE_FAN
	
    glEndList();
}

static void MakeCylinder( const int in_iDivsTheta )
{
	glNewList( s_iId+2, GL_COMPILE );
	
    const int iThetaDiv = WINmax( in_iDivsTheta, 4 );
    const double dThetaDiv = 2.0 * M_PI / (double) iThetaDiv;
	
    glBegin( GL_TRIANGLE_FAN );
    glNormal3d( 0,1,0 );
    glVertex3d( 0,1,0);
    for (int iT = 0; iT < iThetaDiv + 1; iT++ ) {
        glNormal3d( -cos( -iT * dThetaDiv ), 1.0, -sin( -iT * dThetaDiv ) );
        glVertex3d( -cos( -iT * dThetaDiv ), 1.0, -sin( -iT * dThetaDiv ) );
    }
    glEnd( ); //GL_TRIANGLE_FAN
	
    glBegin( GL_TRIANGLE_FAN );
    glNormal3d( 0,-1,0 );
    glVertex3d( 0,0,0);
    for (FORINT iT = 0; iT < iThetaDiv + 1; iT++ ) {
        glNormal3d( cos( iT * dThetaDiv ), 0.0, sin( iT * dThetaDiv ) );
        glVertex3d( cos( iT * dThetaDiv ), 0.0, sin( iT * dThetaDiv ) );
    }
    glEnd( ); //GL_TRIANGLE_FAN
	
    glBegin( GL_TRIANGLE_STRIP );
    for (FORINT iT = 0; iT < iThetaDiv + 1; iT++ ) {
        glNormal3d( cos( -iT * dThetaDiv ), 1.0, sin( -iT * dThetaDiv ) );
        glVertex3d( cos( -iT * dThetaDiv ), 1.0, sin( -iT * dThetaDiv ) );
        glNormal3d( cos( -iT * dThetaDiv ), 0.0, sin( -iT * dThetaDiv ) );
        glVertex3d( cos( -iT * dThetaDiv ), 0.0, sin( -iT * dThetaDiv ) );
    }
    glEnd( ); //GL_TRIANGLE_STRIP
	
    glEndList();
}

void OGLDrawSetDivisions( const int in_iDivsTheta, const int in_iDivsPhi )
{
	if ( s_iDivsTheta == in_iDivsTheta && s_iDivsPhi == in_iDivsPhi ) {
		return;
	}
	s_iDivsTheta = in_iDivsTheta;
	s_iDivsPhi = in_iDivsPhi;
	
	if ( s_iId == 0 ) {
		s_iId = glGenLists( 3 );
	}
	
	MakeSphere( in_iDivsTheta, in_iDivsPhi );
	MakeCylinder( in_iDivsTheta );
	MakeCone( in_iDivsTheta );
}


void OGLDrawSphere( const R3Sphere &in_sphere )
{
	if ( s_iId == 0 ) {
		OGLDrawSetDivisions(32, 16);
	}
	
    glPushMatrix();
	
    const R3Pt &ptMove = in_sphere.Center();
	
    glTranslated( ptMove[0], ptMove[1], ptMove[2] );
    glScaled( in_sphere.Radius(), in_sphere.Radius(), in_sphere.Radius() );
	
    glCallList( s_iId );
	
    glPopMatrix();
}

void OGLDrawCone( const R3Pt &in_ptCenter, const double in_dHeight, const double in_dRad )
{
	if ( s_iId == 0 ) {
		OGLDrawSetDivisions(32, 16);
	}
	
    glPushMatrix();

    glTranslated( in_ptCenter[0], in_ptCenter[1], in_ptCenter[2] );
    glScaled( in_dRad, in_dHeight, in_dRad );

    glCallList( s_iId+1 );

    glPopMatrix();
}


void OGLDrawCylinder( const R3Pt &in_ptCenter, const double in_dHeight, const double in_dRad )
{
	if ( s_iId == 0 ) {
		OGLDrawSetDivisions(32, 16);
	}
	

    glPushMatrix();

    glTranslated( in_ptCenter[0], in_ptCenter[1], in_ptCenter[2] );
    glScaled( in_dRad, in_dHeight, in_dRad );

    glCallList( s_iId+2 );

    glPopMatrix();
}

void OGLDrawArrow( const R3Pt & in_ptCenter, 
                  const double  in_dHeight, 
                  const double  in_dRad )
{
    OGLDrawCylinder( in_ptCenter, in_dHeight * 0.9, in_dRad * 0.4 );
    glPushMatrix();
    glTranslated( 0, in_dHeight, 0 );
    OGLDrawCone( in_ptCenter, in_dHeight * 0.2, in_dRad );
    glPopMatrix();

}

void OGLDrawSceptre( const R3Pt & in_ptCenter, 
                     const double  in_dHeight, 
                     const double  in_dRad )
{
    R3Sphere sphere( in_ptCenter, in_dRad );

    OGLDrawCylinder( in_ptCenter, in_dHeight, in_dRad * 0.5 );
    glPushMatrix();
    glTranslated( 0, in_dHeight, 0 );
    OGLDrawSphere( sphere );
    glPopMatrix();

}

