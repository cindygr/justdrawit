#include <OpenGL/OGLRoutines.H>
#include <OpenGL/OGLDraw2D.H>
#include <utils/Rn_Polygon.H>



void OGLDrawCursor( const R2Pt &in_pt, const UTILSColor &in_col, const double in_dSize,  const int in_iWidth, const int in_iHeight  )
{
    glPushAttrib( GL_ENABLE_BIT );
    OGLBegin2D( in_iWidth, in_iHeight );
    glColor3f( (GLfloat) in_col[0], (GLfloat) in_col[1], (GLfloat) in_col[2] );
    glLineWidth(2.0f);

    const double dSize = in_dSize;
    glBegin( GL_POLYGON );
    glVertex2d( in_pt[0], in_pt[1] );
    glVertex2d( in_pt[0] + 2.0 * dSize, in_pt[1] );
    glVertex2d( in_pt[0] + 1.5 * dSize, in_pt[1] - dSize );
    glVertex2d( in_pt[0] + 3 * dSize, in_pt[1] - 2.0 * dSize );
    glVertex2d( in_pt[0] + 2.0 * dSize, in_pt[1] - 3.0 * dSize );
    glVertex2d( in_pt[0] + dSize, in_pt[1] - 1.5 * dSize );
    glVertex2d( in_pt[0], in_pt[1] - 2.0 * dSize );
    glVertex2d( in_pt[0], in_pt[1] );
    glEnd();

    glColor3f( (GLfloat) (in_col[0] * 0.8f), (GLfloat) (in_col[1] * 0.8f), (GLfloat) (in_col[2] * 0.8f) );
    glBegin( GL_LINE_STRIP );
    glVertex2d( in_pt[0], in_pt[1] );
    glVertex2d( in_pt[0] + 2.0 * dSize, in_pt[1] );
    glVertex2d( in_pt[0] + 1.5 * dSize, in_pt[1] - dSize );
    glVertex2d( in_pt[0] + 3 * dSize, in_pt[1] - 2.0 * dSize );
    glVertex2d( in_pt[0] + 2.0 * dSize, in_pt[1] - 3.0 * dSize );
    glVertex2d( in_pt[0] + dSize, in_pt[1] - 1.5 * dSize );
    glVertex2d( in_pt[0], in_pt[1] - 2.0 * dSize );
    glVertex2d( in_pt[0], in_pt[1] );
    glEnd();

    OGLEnd2D();
    glPopAttrib();
}

void OGLDrawCircle( const R2Pt &in_pt, const double in_dR )
{
#ifndef __PROGRAMMABLE_PIPELINE__
    if ( RNIsZero( in_dR ) )
        return;

    const double dDiv = WINminmax( ( M_PI / 180.0 ) * in_dR, M_PI / 4.0, M_PI / 10000.0 );

    glPushMatrix();
    glTranslated( in_pt[0], in_pt[1], 0 );

    glBegin( GL_LINE_LOOP );
    for ( double dTheta = 0.0; dTheta < 2.0 * M_PI; dTheta += dDiv ) {
        glVertex2d( in_dR * cos( dTheta ), in_dR * sin( dTheta ) );
    }
    glEnd();

    glPopMatrix();
#endif

}

void OGLDrawPolygon( const R2Polygon &in_poly )
{
    glBegin( GL_LINE_LOOP );
    for ( int i = 0; i < in_poly.Num_pts(); i++ ) {
        glVertex2d( in_poly[i][0], in_poly[i][1] );
    }
    glEnd();
}

void OGLBegin2D(  const int in_iWidth, const int in_iHeight )
{
    ASSERT(in_iWidth > 1 && in_iHeight > 1);
    ::glViewport(0, 0, in_iWidth, in_iHeight);

    glMatrixMode( GL_PROJECTION );
    glPushMatrix();
    glLoadIdentity();

    glMatrixMode( GL_MODELVIEW );
    glPushMatrix();
    glLoadIdentity();

    glPushAttrib( GL_LIGHTING );
    glDisable( GL_LIGHTING );
}

void OGLEnd2D( )
{
    glMatrixMode( GL_PROJECTION );
    glPopMatrix();

    glMatrixMode( GL_MODELVIEW );
    glPopMatrix();

    glPopAttrib( );
}

void OGLDrawSquare(   const R2Pt &in_ptLL, const R2Pt &in_ptUR )
{
    glBegin( GL_LINE_LOOP );
    glVertex3d( in_ptLL[0], in_ptLL[1], 0.0 );
    glVertex3d( in_ptUR[0], in_ptLL[1], 0.0 );
    glVertex3d( in_ptUR[0], in_ptUR[1], 0.0 );
    glVertex3d( in_ptLL[0], in_ptUR[1], 0.0 );
    glEnd();
}
