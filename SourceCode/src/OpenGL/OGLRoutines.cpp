/*
 * Some utility routines for creating opengl windows.
 */
#include <WINSystemDefines.H>

#include <OpenGL/OGLRoutines.H>

/*
#include <OpenGL/OGLObjs_Camera.H>
#include <utils/Utils_RawImage.H>
void OGLReset() 
{
    glEnable(GL_DEPTH_TEST);
    if ( g_toggleAntialiasingOGL == TRUE ) {
        glEnable(GL_POINT_SMOOTH);
        glEnable(GL_BLEND);
        glBlendFunc(GL_SRC_ALPHA,GL_ONE_MINUS_SRC_ALPHA);
    } else {
        glDisable(GL_POINT_SMOOTH);
        glDisable(GL_BLEND);
    }

    if ( g_toggleNiceRenderingOGL == TRUE ) {
        glHint(GL_LINE_SMOOTH_HINT,            GL_NICEST);
        glHint(GL_POINT_SMOOTH_HINT,           GL_NICEST);
        glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
        glHint(GL_FOG_HINT,                    GL_NICEST);
        glEnable(GL_NORMALIZE);
    } else {
        glHint(GL_LINE_SMOOTH_HINT,            GL_FASTEST);
        glHint(GL_POINT_SMOOTH_HINT,           GL_FASTEST);
        glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_FASTEST);
        glHint(GL_FOG_HINT,                    GL_FASTEST);
        glDisable(GL_NORMALIZE);
    }
}

void OGLSetSize( int in_iWidth, int in_iHeight )
{
    if ( 0 >= in_iWidth || 0 >= in_iHeight ) {
        return;
    }

    glViewport(0, 0, in_iWidth, in_iHeight);
}

void OGLSetView( const R3Pt  & in_ptFrom, 
                 const R3Pt  & in_ptAt,
                 const R3Vec & in_vecUp,
                 GLdouble      in_dZoom,
                 GLdouble      in_dHither,
                 GLdouble      in_dYonder )
{
    int ai[4];
    glGetIntegerv( GL_VIEWPORT, ai );

    glMatrixMode(GL_PROJECTION);

    OGLObjsCamera oCam;
    oCam.SetFromAtUp( in_ptFrom, in_ptAt, in_vecUp );
    oCam.SetZoom( in_dZoom );
    oCam.SetNearFar( in_dHither, in_dYonder );
    oCam.SetSize( ai[0], ai[1] );
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();

    oCam.SetOpenGLCamera();
}
*/
void OGLSetLights( const UTILSColor   & in_colBackground )
{
    GLfloat vecLight0[] = { 0.5f, -0.3f, 1.0f, 0.0f };
    GLfloat vecLight1[] = { -0.3f, 0.6f, 1.0f, 0.0f };
    GLfloat vecLight2[] = { 0.3f, -0.6f, -1.0f, 0.0f };
    GLfloat vecLight3[] = { -0.5f, 0.3f, -1.0f, 0.0f };
    
    GLfloat mat_specular[] = { 0.1f, 0.1f, 0.1f, 1.0f };
    GLfloat mat_shininess[] = { 0.01f };
    GLfloat mat_diffuse[] = { 0.8f, 0.8f, 0.8f, 1.0f };
    GLfloat spot_color[] = { 0.5f, 0.5f, 0.5f, 1.0f };
    GLfloat light_color[] = { 0.8f, 0.8f, 0.8f, 1.0f };
    GLfloat ambient_color[] = { 0.05f, 0.05f, 0.05f, 1.0 };
    GLfloat black[] = { 0.0f, 0.0f, 0.0f, 0.0f };
    
    glClearColor( (GLclampf) in_colBackground[0], (GLclampf) in_colBackground[1], (GLclampf) in_colBackground[2], 0.0 );
        
    glMaterialfv( GL_FRONT, GL_SPECULAR, mat_specular );
    glMaterialfv( GL_FRONT, GL_SHININESS, mat_shininess);
    glMaterialfv( GL_FRONT, GL_AMBIENT_AND_DIFFUSE, mat_diffuse);
    

    glLightfv(GL_LIGHT0, GL_POSITION, vecLight0);
    glLightfv(GL_LIGHT1, GL_POSITION, vecLight1);
    glLightfv(GL_LIGHT2, GL_POSITION, vecLight2);
    glLightfv(GL_LIGHT3, GL_POSITION, vecLight3);
    
    glLightfv( GL_LIGHT0, GL_AMBIENT, ambient_color );
    glLightfv( GL_LIGHT1, GL_AMBIENT, black );
    glLightfv( GL_LIGHT2, GL_AMBIENT, black );
    glLightfv( GL_LIGHT3, GL_AMBIENT, black );
    
    glLightfv( GL_LIGHT0, GL_DIFFUSE, light_color );
    glLightfv( GL_LIGHT1, GL_DIFFUSE, light_color );
    glLightfv( GL_LIGHT2, GL_DIFFUSE, light_color );
    glLightfv( GL_LIGHT3, GL_DIFFUSE, light_color );
    
    glLightfv( GL_LIGHT0, GL_SPECULAR, spot_color );
    glLightfv( GL_LIGHT1, GL_SPECULAR, spot_color );
    glLightfv( GL_LIGHT2, GL_SPECULAR, spot_color );
    glLightfv( GL_LIGHT3, GL_SPECULAR, spot_color );
    
    glEnable(GL_LIGHTING);
    glEnable(GL_LIGHT0);
    glEnable(GL_LIGHT1);
    glEnable(GL_LIGHT2);
    glEnable(GL_LIGHT3);
    glEnable(GL_DEPTH_TEST);
}

/*
int OGLInitWindow( int in_iWidth, int in_iHeight,
                   const char * in_strName )
{
#ifndef WIN32
    static int singleBufferAttributes[] = {
        GLX_DRAWABLE_TYPE, GLX_WINDOW_BIT, 
        GLX_RENDER_TYPE,   GLX_RGBA_BIT,
        GLX_RED_SIZE,      1,             // Single buffer 
        GLX_GREEN_SIZE,    1,
        GLX_BLUE_SIZE,     1,
        None 
    };
    
    static Display              *s_oDisplay = NULL;
    static Pixmap                xPixmap;
    static XVisualInfo          *vInfo = NULL;
    static GLXFBConfig          *fbConfigs;
    static GLXContext            context;
    static GLXPixmap             glxPixmap;
    static int                   numReturned;
    
    // open a connection to the X server 
    if ( s_oDisplay == NULL ) {
        s_oDisplay = glXGetCurrentDisplay();
        
        if ( s_oDisplay == NULL ) {
            s_oDisplay = XOpenDisplay( NULL );
            if ( s_oDisplay == NULL ) {
                cerr << "Unable to open a connection to the X server 0\n";
                return -1;
            }
        }
    }
    // Request a single framebuffer configuration 
    fbConfigs = glXChooseFBConfig( s_oDisplay, DefaultScreen( s_oDisplay ),
                                   singleBufferAttributes, &numReturned );
        
    // Create an X colormap and window 
    vInfo = glXGetVisualFromFBConfig( s_oDisplay, fbConfigs[0] );
    
    // Create a GLX context for OpenGL rendering 
    context = glXCreateNewContext( s_oDisplay, fbConfigs[0], GLX_RGBA_TYPE, NULL, True );
    
    // Make a new pixmap, freeing oldone if necessary 
    xPixmap = XCreatePixmap( s_oDisplay, RootWindow( s_oDisplay, vInfo->screen ), 
                             in_iWidth, in_iHeight, vInfo->depth );
    
    
    // Create a GLX pixmap to associate the frame buffer configuration with the X window 
    glxPixmap = glXCreatePixmap( s_oDisplay, fbConfigs[0], xPixmap, NULL );
    
    // Bind the GLX context to the window 
    glXMakeCurrent( s_oDisplay, glxPixmap, context );

    OGLSetSize( in_iWidth, in_iHeight );
#endif
    return TRUE;
}

void OGLGetImage( UTILSRawImage &out_oImage )
{
    int ai[4], iWidth, iHeight, iN;
    glGetIntegerv( GL_VIEWPORT, ai );
    
    iWidth = ai[2];
    iHeight = ai[3];
    iN = iWidth * iHeight;

    out_oImage.SetSize( iWidth, iHeight, FALSE );
    GLfloat *afPixels = new GLfloat [ 3 * iN ];
    
    ::glReadPixels( 0, 0, iWidth, iHeight, GL_RGB, GL_FLOAT, (GLvoid *) afPixels );

    UTILSColor col;
    for (int i = 0; i < iN; i++) {
        for (int j = 0; j < 3; j++ ) {
            col[j] = afPixels[ i * 3 + j ];
        }
        
        int iX = i % iHeight;
        int iY = iHeight - i / iHeight - 1;
        out_oImage.Set( iX, iY, col );
    }

    delete [] afPixels;
}

void OGLDrawImage( const UTILSRawImage &in_image )
{
	glRasterPos2f(0,0);
	glDrawPixels(in_image.Width(), in_image.Height(), GL_RGB, GL_DOUBLE, (GLvoid *) in_image.Data());
}
*/

void OGLSetMaterial( GLenum face, GLenum pname, const UTILSColor &in_col, const double in_dAlpha )
{
    static GLclampf col[4];
    col[0] = (GLclampf) in_col[0];
    col[1] = (GLclampf) in_col[1];
    col[2] = (GLclampf) in_col[2];
    col[3] = (GLclampf) in_dAlpha;

    glMaterialfv( face, pname, col );
}

void OGLSetColor( const UTILSColor &in_col, const double in_dAlpha )
{
    static GLclampf col[4];
    col[0] = (GLclampf) in_col[0];
    col[1] = (GLclampf) in_col[1];
    col[2] = (GLclampf) in_col[2];
    col[3] = (GLclampf) in_dAlpha;

    glColor4fv( col );
}


void OGLSetLight( GLenum light, GLenum pname, const UTILSColor &in_col )
{
    static GLclampf col[3];
    col[0] = (GLclampf) in_col[0];
    col[1] = (GLclampf) in_col[1];
    col[2] = (GLclampf) in_col[2];

    glLightfv( light, pname, col );
}


extern void OGLOffsetLook( const double in_dPerc )
{
    glMatrixMode( GL_MODELVIEW );
    R4Matrix mat;
    WINbool bSuc;
    glGetDoublev( GL_MODELVIEW_MATRIX, &mat(0,0) );

    R4Matrix matInv = mat.Inverse( bSuc );
    ASSERT( bSuc );
    const R3Vec vecLook = ( matInv * R3Vec(0,0,1) ) * in_dPerc;

    glPushMatrix();

    glTranslated( vecLook[0], vecLook[1], vecLook[2] );
}

extern void OGLEndOffsetLook()
{
    glPopMatrix();
}


