#include <GL/glew.h>
#include <OpenGL/OGLShader.h>


#ifdef __PROGRAMMABLE_PIPELINE__
CGcontext *contextSingleton = NULL;

#define LOC() __FILE__, __LINE__

static void ShaderError(const char *pFile, int pLine, const char *pText, ...)
{
  char error[2048];
  va_list valist;
  va_start(valist, pText);
  vsprintf(error, pText, valist);
  va_end(valist);
  fprintf(stderr, "ERROR: %s(%i): %s\n", pFile, pLine, error);
  const char *listing = cgGetLastListing(*contextSingleton);
  if (listing)
	  fprintf(stderr, "%s\n", listing);
} 

static void ShaderWarning(const char *pFile, int pLine, const char *pText, ...)
{
  char error[2048];
  va_list valist;
  va_start(valist, pText);
  vsprintf(error, pText, valist);
  va_end(valist);
  fprintf(stderr, "Warning : %s(%i): %s\n", pFile, pLine, error);
  	const char *listing = cgGetLastListing(*contextSingleton);
    if (listing)
		fprintf(stderr, "%s\n", listing);
} 

void CgErrorCallback() {
	fprintf(stderr, "\n**Cg Error:\n-----------\n%s\n", cgGetErrorString(cgGetError()));
	const char *listing = cgGetLastListing(*contextSingleton);
    if (listing)
		fprintf(stderr, "%s\n", listing);
	fprintf(stderr, "-----------\n\n");
	fflush(stderr);
}

void OGLShader2::CheckForCgError(const char *situation)
{
	CGerror error;
	const char *string = cgGetLastErrorString(&error);
	
	if (error != CG_NO_ERROR) {
		printf("OGLShader2: %s: %s\n",
			   situation, string);
		if (error == CG_COMPILER_ERROR) {
			printf("%s\n", cgGetLastListing(context));
		}
		exit(1);
	}
}


void OGLShader2::Init()
{
	if (glewInit()!=GLEW_OK || !GLEW_VERSION_1_1) {
		fprintf(stderr, "Sketch curves: failed to initialize GLEW, OpenGL 1.1 required.\n" );    
		exit(1);
	}
}

const char * OGLShader2::GetTechnique(int pIndex) {
    if(namesList.size() == 0) {
        return NULL;
    }
    return namesList[pIndex];
};

OGLShader2::OGLShader2() 
{
	Init();

	context = cgCreateContext();
	CheckForCgError( "Creating context" );
	contextSingleton = &context;

	cgGLSetDebugMode(CG_FALSE);
	cgSetParameterSettingMode(context, CG_DEFERRED_PARAMETER_SETTING);

	/* Register a callback error function */
	cgSetErrorCallback(CgErrorCallback);
	cgGLRegisterStates(context);
	
	/* profiles for individual program files - get best */
	m_CgVertexProfile = cgGLGetLatestProfile(CG_GL_VERTEX);
	cgGLSetOptimalOptions(m_CgVertexProfile);
	CheckForCgError("selecting vertex profile");
	
	m_CgFragmentProfile = cgGLGetLatestProfile(CG_GL_FRAGMENT);
	cgGLSetOptimalOptions(m_CgFragmentProfile);
	CheckForCgError("selecting fragment profile");

	m_iActiveVP = -1;
	m_iActiveFP = -1;
	
	numTechniques = 0;
	loaded = false;
	complete = false;
	firstPass = true;
}

bool OGLShader2::Validate() {
	bool validTech = false;
	technique = cgGetFirstTechnique(effect);
    while (technique) {
		if (cgValidateTechnique(technique) == CG_FALSE) {
			ShaderWarning(LOC(), "Technique %s did not validate.  Skipping.\n", cgGetTechniqueName(technique));
		}
		else {
			fprintf(stderr, "Valid technique(%i): %s.\n", numTechniques, cgGetTechniqueName(technique));
			numTechniques++;
			validTech = true;
			techniqueList.push_back(technique);
			namesList.push_back(cgGetTechniqueName(technique));
		}
        technique = cgGetNextTechnique(technique);
    }

	if(!validTech) {
		ShaderError(LOC(), "No valid techniques");
		return false;
	}

	CGparameter p = cgGetFirstEffectParameter(effect);
	while(p) {
		printf("'%s':\nBase:\t\t%s\nType:\t\t%s\nSemantic:\t%s\nArraySize:\t%i\nRows:\t\t%i\nCols:\t\t%i\n------\n", cgGetParameterName(p), cgGetTypeString(cgGetParameterBaseType(p)), cgGetTypeString(cgGetParameterType(p)), cgGetParameterSemantic(p), cgGetArraySize(p, 0), cgGetParameterRows(p), cgGetParameterColumns(p));
		p = cgGetNextParameter(p);
	}
	loaded = true;
	return true;
}


void OGLShader2::SetNumberVertexShaders( const int in_iN )
{
	m_aCgVertexProgram.resize( in_iN, NULL );
}

void OGLShader2::LoadVertexShader( const int in_iWhich, const char *in_strFileName, const char *in_strFuncName )
{
	m_aCgVertexProgram[in_iWhich] =
    cgCreateProgramFromFile(
							context,              /* Cg runtime context */
							CG_SOURCE,            /* Program in human-readable form */
							in_strFileName,       /* Name of file containing program */
							m_CgVertexProfile,    /* Profile: OpenGL ARB vertex program */
							in_strFuncName,       /* Entry function name */
							NULL);                /* No extra compiler options */
	CheckForCgError("creating vertex program from file");
	cgGLLoadProgram(m_aCgVertexProgram[in_iWhich]);
	CheckForCgError("loading vertex program");
}

void OGLShader2::SetNumberFragmentShaders( const int in_iN )
{
	m_aCgFragmentProgram.resize( in_iN, NULL );
}

void OGLShader2::LoadFragmentShader( const int in_iWhich, const char *in_strFileName, const char *in_strFuncName )
{
	m_aCgFragmentProgram[in_iWhich] =
    cgCreateProgramFromFile(
							context,              /* Cg runtime context */
							CG_SOURCE,            /* Program in human-readable form */
							in_strFileName,       /* Name of file containing program */
							m_CgFragmentProfile,    /* Profile: OpenGL ARB vertex program */
							in_strFuncName,       /* Entry function name */
							NULL);                /* No extra compiler options */
	CheckForCgError("creating fragment program from file");
	cgGLLoadProgram(m_aCgFragmentProgram[in_iWhich]);
	CheckForCgError("loading fragment program");
}

void OGLShader2::BindVertexProgram( const int in_iWhich )
{
	cgGLBindProgram(m_aCgVertexProgram[in_iWhich]);
	CheckForCgError("binding vertex program");
	
	cgGLEnableProfile(m_CgVertexProfile);
	CheckForCgError("enabling vertex profile");

	ASSERT( m_iActiveVP == -1 );
	m_iActiveVP = in_iWhich;
}

void OGLShader2::BindFragmentProgram( const int in_iWhich )
{
	cgGLBindProgram(m_aCgFragmentProgram[in_iWhich]);
	CheckForCgError("binding fragment program");
	
	cgGLEnableProfile(m_CgFragmentProfile);
	CheckForCgError("enabling fragment profile");

	ASSERT( m_iActiveFP == -1 );
	m_iActiveFP = in_iWhich;
}

void OGLShader2::DisablePrograms()
{
	cgGLDisableProfile(m_CgVertexProfile);
	CheckForCgError("disabling vertex profile");
	
	cgGLDisableProfile(m_CgFragmentProfile);
	CheckForCgError("disabling fragment profile");
	
	m_iActiveVP = -1;
	m_iActiveFP = -1;
}

void OGLShader2::SetOGLMatrix4x4VP( const char * in_strName, const R4Matrix &pMat)
{
	CGparameter param = cgGetNamedParameter( m_aCgVertexProgram[ m_iActiveVP ], in_strName);
	CheckForCgError( ( string("could not get Matrix VP ") + string( in_strName ) + string(" parameter") ).c_str() );
	cgSetMatrixParameterdc( param, &pMat(0,0) );
}

void OGLShader2::SetColorFP( const char * in_strName, const UTILSColor &in_col )
{
	CGparameter param = cgGetNamedParameter( m_aCgFragmentProgram[ m_iActiveFP ], in_strName);
	CheckForCgError( ( string("could not get Color FP ") + string( in_strName ) + string(" parameter") ).c_str() );
	cgSetParameter3dv( param, in_col.Data() );	
}

void OGLShader2::SetPointFP( const char * in_strName, const R3Pt &in_pt )
{
	CGparameter param = cgGetNamedParameter( m_aCgFragmentProgram[ m_iActiveFP ], in_strName);
	CheckForCgError( ( string("could not get Point FP ") + string( in_strName ) + string(" parameter") ).c_str() );
	cgSetParameter3dv( param, &in_pt[0] );	
}

void OGLShader2::SetVectorFP( const char * in_strName, const R3Vec &in_vec )
{
	CGparameter param = cgGetNamedParameter( m_aCgFragmentProgram[ m_iActiveFP ], in_strName);
	CheckForCgError( ( string("could not get Vector FP ") + string( in_strName ) + string(" parameter") ).c_str() );
	cgSetParameter3dv( param, &in_vec[0] );	
}

void OGLShader2::SetTextureIdFP( const char * in_strName, const GLuint in_iTexId )
{
	CGparameter param = cgGetNamedParameter( m_aCgFragmentProgram[ m_iActiveFP ], in_strName);
	CheckForCgError( ( string("could not get Texture Id FP ") + string( in_strName ) + string(" parameter") ).c_str() );
	cgGLSetTextureParameter( param, in_iTexId );	
}


bool OGLShader2::LoadShaderFromFile(const char * pFileName) {

	fprintf(stderr, "Creating shader file");

	if(loaded) {
		ShaderError(LOC(), "Already loaded shader, please create new OGLShader object");
		return false;
	}

	if(pFileName == NULL) {
		ShaderError(LOC(), "Bad Shader File");
		return false;
	}

	effect = cgCreateEffectFromFile(context, pFileName, NULL);
	if(!effect) {
		ShaderError(LOC(), "Couldn't create effect file: '%s'", pFileName);
		return false;
	}

	Validate();
	loaded = true;
	return true;
}

bool OGLShader2::SetTechnique(const char *pTech) {
	if(!loaded) {
		ShaderError(LOC(), "Shader not loaded!");
		return false;
	}

	technique = cgGetFirstTechnique(effect);
	while(technique && strcmp(pTech, cgGetTechniqueName(technique))) {
		technique = cgGetNextTechnique(technique);
	}

	//Technique set
	pass = cgGetFirstPass(technique);

	return true;
}


bool OGLShader2::StartPass() {
	if(!loaded) {
		ShaderError(LOC(), "Shader not loaded!");
		return false;
	}
	if(firstPass) {
		pass = cgGetFirstPass(technique);
	}
	cgSetPassState(pass);
	return true;
}

bool OGLShader2::EndPass() {
	if(!loaded) {
		ShaderError(LOC(), "Shader not loaded!");
		return false;
	}
	cgResetPassState(pass);
	pass = cgGetNextPass(pass);
	if(!pass) {
		complete = true;
	}
	return true;
}

void OGLShader2::Release() {
	cgDestroyContext(context);
	cgDestroyEffect(effect);
}


// Parameter Settings
bool OGLShader2::SetMatrix4x4BySemantic(const char * pSemantic, float pMat[4][4]) {

	return true;
}

bool OGLShader2::SetMatrix4x4BySemantic(const char * pSemantic, float *pMat) {return true;};

// Necessary to flip!
bool OGLShader2::SetOGLMatrix4x4BySemantic(const char * pSemantic, const R4Matrix &pMat) {
	
	CGparameter p = cgGetFirstEffectParameter(effect);
	while(p) {
		p = cgGetNextParameter(p);
	}

	CGparameter m = cgGetEffectParameterBySemantic(effect, pSemantic);
	cgSetMatrixParameterdr(m, &pMat(0,0));
	return true;
}

bool OGLShader2::SetOGLMatrix4x4ArrayByName(const char * pName, R4Matrix *pMat, int pNum) {
	CGparameter p = cgGetFirstEffectParameter(effect);
	while(p) {
		if(!strcmp(cgGetParameterName(p), pName)) {
			break;
		}
		p = cgGetNextParameter(p);
	}
	if(!p) {
		ShaderError(LOC(), "Unable to find array with name %s");
		return false;
	}

	int ArraySize = cgGetArraySize(p, 0);
	
	if(pNum > ArraySize) {
		ShaderError(LOC(), "Out of bounds array element");
		return false;
	}
  
	for(int i = 0; i < pNum; i++) {
		CGparameter element = cgGetArrayParameter(p, i);
		cgSetMatrixParameterdr(element, &pMat[i](0,0));
     }
	return true;
}

bool OGLShader2::SetOGLMatrix4x4ArrayElementByName(const char * pName, const R4Matrix &pMat, int pElement) {
	CGparameter p = cgGetFirstEffectParameter(effect);
	while(p) {
		if(!strcmp(cgGetParameterName(p), pName)) {
			break;
		}
		p = cgGetNextParameter(p);
	}
	if(!p) {
		ShaderError(LOC(), "Unable to find array with name %s");
		return false;
	}

	int ArraySize = cgGetArraySize(p, 0);
	
	if(pElement > ArraySize) {
		ShaderError(LOC(), "Out of bounds array element");
		return false;
	}
  
	CGparameter element = cgGetArrayParameter(p, pElement);
	cgSetMatrixParameterdr(element, &pMat(0,0));

	return true;
}


//Uh, fix this...
//bool OGLShader2::SetMatrix4x4ArrayByName(const char * pSemantic, R4Matrix *pMat, int pNum);

// NOT necessary to flip
bool OGLShader2::SetMatrix4x4BySemantic(const char * pSemantic, R4Matrix pMat) {return true;};

bool OGLShader2::SetMatrix3x3BySemantic(const char * pSemantic, float pMat[3][3]) {return true;};
bool OGLShader2::SetMatrix3x3BySemantic(const char * pSemantic, float *pMat) {return true;};

bool OGLShader2::SetFloat4BySemantic(const char * pSemantic, const UTILSColor pColor) {
	float v[4] = {(float)pColor[0], (float)pColor[1], (float)pColor[2], 1.0f};
	CGparameter m = cgGetEffectParameterBySemantic(effect, pSemantic);
	cgSetParameter4fv(m, &v[0]);
	return true;
};

bool OGLShader2::SetFloat4BySemantic(const char * pSemantic, float pVec[4]) {
	CGparameter p = cgGetEffectParameterBySemantic(effect, pSemantic);
	cgSetParameter4fv(p, &pVec[0]);
	return true;
};

bool OGLShader2::SetFloat4ByName(const char * pName , R4Pt pVec )
{
	CGparameter p = cgGetFirstEffectParameter(effect);
	while(p){
		if(!strcmp(cgGetParameterName(p),pName)){
			break;
		}
		p = cgGetNextParameter(p);
	}
	if( !p ){
		ShaderError(LOC(),"parameter with %s name does not exist \n",pName);
		return false;
	}
	cgSetParameter4dv(p , &pVec[0] );
	return true ;
}

bool OGLShader2::SetDouble3BySemantic(const char * pSemantic, const double *in_ad)
{
	CGparameter p = cgGetEffectParameterBySemantic(effect, pSemantic);
	cgSetParameter3dv(p, in_ad);
	return true;
}



bool OGLShader2::SetFloat3BySemantic(const char * pSemantic, float pVec[3]) {
	CGparameter p = cgGetEffectParameterBySemantic(effect, pSemantic);
	cgSetParameter3fv(p, &pVec[0]);
	return true;
};
bool OGLShader2::SetFloat3BySemantic(const char * pSemantic, R3Pt pVec) {
	CGparameter p = cgGetEffectParameterBySemantic(effect, pSemantic);
	cgSetParameter3dv(p, &pVec[0]);
	return true;
};
bool OGLShader2::SetFloat2BySemantic(const char * pSemantic, float pVec[2]) {
	CGparameter p = cgGetEffectParameterBySemantic(effect, pSemantic);
	cgSetParameter2fv(p, &pVec[0]);
	return true;
};

bool OGLShader2::SetFloat2ByName(const char * pName , const R2Pt &pVec ){

	CGparameter p = cgGetFirstEffectParameter(effect);
	while(p){
		if(!strcmp(cgGetParameterName(p),pName)){
			break;
		}
		p = cgGetNextParameter(p);
	}
		
	if( !p ){
		ShaderError(LOC(),"parameter with %s name does not exist \n",pName);
		return false;
	}	

	cgSetParameter2dv(p, &pVec[0]);
	return true ;
}


bool OGLShader2::SetFloat2BySemantic(const char * pSemantic, const R2Pt_f &pVec) {
	CGparameter p = cgGetEffectParameterBySemantic(effect, pSemantic);
	cgSetParameter2fv(p, &pVec[0]);
	return true;
};

bool OGLShader2::SetFloat3ByName(const char * pName , R3Pt pVec )
{
	CGparameter p = cgGetFirstEffectParameter(effect);
	while(p){
	if(!strcmp(cgGetParameterName(p),pName)){
			break;
		}
		p = cgGetNextParameter(p);
	}
	if( !p ){
		ShaderError(LOC(),"parameter with %s name does not exist \n",pName);
		return false;
	}
	cgSetParameter3dv(p , &pVec[0] );
	return true ;
}



bool OGLShader2::SetTextureBySemantic(const char * pSemantic, GLuint pID) 
{
	WINbool bFound = FALSE;
	for ( unsigned int i = 0; i < m_aTexIds.size(); i++ ) {
		if ( m_aTexIds[i].first == pID )
			bFound = TRUE;
	}
	if ( bFound == FALSE ) {
		m_aTexIds.push_back( pair<GLuint, CGparameter>( pID, cgGetEffectParameterBySemantic( effect, pSemantic) ) );
	    cgGLSetTextureParameter( m_aTexIds.back().second, pID );
	}

	return true;
}

void OGLShader2::EnableTextureMaps()
{
	for ( unsigned int i = 0; i < m_aTexIds.size(); i++ )
		cgGLEnableTextureParameter( m_aTexIds[i].second );
}

void OGLShader2::DisableTextureMaps()
{
	for ( unsigned int i = 0; i < m_aTexIds.size(); i++ )
		cgGLDisableTextureParameter( m_aTexIds[i].second );

    m_aTexIds.resize(0);
}


bool OGLShader2::SetFloatBySemantic(const char * pSemantic, float pValue) {
	CGparameter p = cgGetEffectParameterBySemantic(effect, pSemantic);
	cgSetParameter1d(p, pValue);
	return true;
};

bool OGLShader2::SetFloatByName(const char * pName , double pValue )
{
	CGparameter p = cgGetFirstEffectParameter(effect);
	while(p){
		if(!strcmp(cgGetParameterName(p),pName)){
			break;
		}
		p = cgGetNextParameter(p);
	}
		
	if( !p ){
		ShaderError(LOC(),"parameter with %s name does not exist \n",pName);
		return false;
	}	

	cgSetParameter1d( p , pValue );
	return true ;

}	


bool OGLShader2::SetBoolBySemantic(const char * pSemantic, bool pValue) {
	return SetFloatBySemantic(pSemantic, pValue);
};
bool OGLShader2::SetIntBySemantic(const char * pSemantic, int pValue) {
	CGparameter p = cgGetEffectParameterBySemantic(effect, pSemantic);
	cgSetParameter1i(p, pValue);
	return true;
};


bool OGLShader2::SetIntArrayElementByName(const char * pName , int pValue , int pElement ){
	CGparameter p = cgGetFirstEffectParameter(effect);
	while(p) {
		if(!strcmp(cgGetParameterName(p), pName)) {
			break;
		}
		p = cgGetNextParameter(p);
	}
	if(!p) {
		ShaderError(LOC(), "Unable to find array with name %s");
		return false;
	}

	int ArraySize = cgGetArraySize(p, 0);
	
	if(pElement > ArraySize) {
		ShaderError(LOC(), "Out of bounds array element");
		return false;
	}
  
	CGparameter element = cgGetArrayParameter(p, pElement);
	cgSetParameter1i(element, pValue);

	return true;
}

bool OGLShader2::SetIntArrayByName(const char * pName , int* pArray , int pNum ){
	
	CGparameter p = cgGetFirstEffectParameter(effect);
	while(p) {
		if(!strcmp(cgGetParameterName(p), pName)) {
			break;
		}
		p = cgGetNextParameter(p);
	}
	if(!p) {
		ShaderError(LOC(), "Unable to find array with name %s", pName);
		return false;
	}
	int ArraySize = cgGetArraySize(p, 0);
	
	if(pNum > ArraySize) {
		ShaderError(LOC(), "Out of bounds array element");
		return false;
	}
	for(int i = 0; i < pNum; i++) {
		CGparameter element = cgGetArrayParameter(p, i);
		cgSetParameter1i(element, pArray[i]);
     }

	return true;
}

bool OGLShader2::SetFloatArrayByName(const char * pName, double *pArray, int pNum) {
	//CGparameter p = cgGetEffectParameterBySemantic(effect, pSemantic);
	//cgGLSetParameterArray1d(p, 0, pNum, pArray);

	CGparameter p = cgGetFirstEffectParameter(effect);
	while(p) {
		if(!strcmp(cgGetParameterName(p), pName)) {
			break;
		}
		p = cgGetNextParameter(p);
	}
	if(!p) {
		ShaderError(LOC(), "Unable to find array with name %s");
		return false;
	}



	int ArraySize = cgGetArraySize(p, 0);
	
	if(pNum > 
		ArraySize) {
		ShaderError(LOC(), "Out of bounds array element");
		return false;
	}


  
	for(int i = 0; i < pNum; i++) {
		CGparameter element = cgGetArrayParameter(p, i);
		cgSetParameter1d(element, pArray[i]);
     }

	return true;
}

bool OGLShader2::SetFloatArrayElementByName(const char *pName, double pArray, int pElement ) {
	CGparameter p = cgGetFirstEffectParameter(effect);
	while(p) {
		if(!strcmp(cgGetParameterName(p), pName)) {
			break;
		}
		p = cgGetNextParameter(p);
	}
	if(!p) {
		ShaderError(LOC(), "Unable to find array with name %s");
		return false;
	}

	int ArraySize = cgGetArraySize(p, 0);
	
	if(pElement > ArraySize) {
		ShaderError(LOC(), "Out of bounds array element");
		return false;
	}
  
	CGparameter element = cgGetArrayParameter(p, pElement);
	cgSetParameter1d(element, pArray);

	return true;
}


bool OGLShader2:: SetBoolArrayByName(const char * pName , bool* pArray , int pNum )
{
	CGparameter p = cgGetFirstEffectParameter(effect);
	while(p) {
		if(!strcmp(cgGetParameterName(p), pName)) {
			break;
		}
		p = cgGetNextParameter(p);
	}
	if(!p) {
		ShaderError(LOC(), "Unable to find array with name %s");
		return false;
	}

	int ArraySize = cgGetArraySize(p, 0);
	
	if(pNum > ArraySize) {
		ShaderError(LOC(), "Out of bounds array element");
		return false;
	}
  
	for(int i = 0; i < pNum; i++) {
		CGparameter element = cgGetArrayParameter(p, i);
		cgSetParameter1d(element, double(pArray[i]));
     }

	return true;
}




bool OGLShader2::SetFloat2ArrayByName(const char * pName, double *pArray, int pNum) {
	CGparameter p = cgGetFirstEffectParameter(effect);
	while(p) {
		if(!strcmp(cgGetParameterName(p), pName)) {
			break;
		}
		p = cgGetNextParameter(p);
	}
	if(!p) {
		ShaderError(LOC(), "Unable to find array with name %s");
		return false;
	}

	int ArraySize = cgGetArraySize(p, 0);
	
	if(pNum > ArraySize) {
		ShaderError(LOC(), "Out of bounds array element");
		return false;
	}
  
	for(int i = 0; i < pNum; i++) {
		CGparameter element = cgGetArrayParameter(p, i);
		//printf("Typerific:%s\n", cgGetTypeString(cgGetParameterType(element)));
		cgSetParameter2dv(element, &pArray[i*2]);
     }

	return true;
}
bool OGLShader2::SetFloat2ArrayElementByName(const char * pName, R2Pt pPt, int pElement) {
	CGparameter p = cgGetFirstEffectParameter(effect);
	while(p) {
		if(!strcmp(cgGetParameterName(p), pName)) {
			break;
		}
		p = cgGetNextParameter(p);
	}
	if(!p) {
		ShaderError(LOC(), "Unable to find array with name %s");
		return false;
	}

	int ArraySize = cgGetArraySize(p, 0);
	
	if(pElement > ArraySize) {
		ShaderError(LOC(), "Out of bounds array element");
		return false;
	}
  
	CGparameter element = cgGetArrayParameter(p, pElement);
	//printf("Typerific:%s\n", cgGetTypeString(cgGetParameterType(element)));
	double test[2] = {pPt[0], pPt[1]};
	cgSetParameter2dv(element, test);

	return true;
}

bool OGLShader2::SetFloat3ArrayByName(const char * pName, double *pArray, int pNum) {
	CGparameter p = cgGetFirstEffectParameter(effect);
	while(p) {
		if(!strcmp(cgGetParameterName(p), pName)) {
			break;
		}
		p = cgGetNextParameter(p);
	}
	if(!p) {
		ShaderError(LOC(), "Unable to find array with name %s");
		return false;
	}

	int ArraySize = cgGetArraySize(p, 0);
	
	if(pNum > ArraySize) {
		ShaderError(LOC(), "Out of bounds array element");
		return false;
	}
  
	for(int i = 0; i < pNum; i++) {
		CGparameter element = cgGetArrayParameter(p, i);
		//printf("Typerific:%s\n", cgGetTypeString(cgGetParameterType(element)));
		cgSetParameter3dv(element, &pArray[i*3]);
     }

	return true;
}

bool OGLShader2::SetFloat4ArrayByName(const char * pName, double *pArray, int pNum) {
	CGparameter p = cgGetFirstEffectParameter(effect);
	while(p) {
		if(!strcmp(cgGetParameterName(p), pName)) {
			break;
		}
		p = cgGetNextParameter(p);
	}
	if(!p) {
		ShaderError(LOC(), "Unable to find array with name %s");
		return false;
	}

	int ArraySize = cgGetArraySize(p, 0);
	
	if(pNum > ArraySize) {
		ShaderError(LOC(), "Out of bounds array element");
		return false;
	}
  
	for(int i = 0; i < pNum; i++) {
		CGparameter element = cgGetArrayParameter(p, i);
		//printf("Typerific:%s\n", cgGetTypeString(cgGetParameterType(element)));
		cgSetParameter4dv(element, &pArray[i*4]);
     }

	return true;
}

bool OGLShader2::SetInt4ArrayByName(const char * pName , int* pArray , int pNum ){
	CGparameter p = cgGetFirstEffectParameter(effect);
	while(p) {
		if(!strcmp(cgGetParameterName(p), pName)) {
			break;
		}
		p = cgGetNextParameter(p);
	}
	if(!p) {
		ShaderError(LOC(), "Unable to find array with name %s");
		return false;
	}

	int ArraySize = cgGetArraySize(p, 0);
	
	if(pNum > ArraySize) {
		ShaderError(LOC(), "Out of bounds array element");
		return false;
	}
  
	for(int i = 0; i < pNum; i++) {
		CGparameter element = cgGetArrayParameter(p, i);
		//printf("Typerific:%s\n", cgGetTypeString(cgGetParameterType(element)));
		cgSetParameter4iv(element, &pArray[i*4]);
     }

	return true;
}

bool OGLShader2::SetFloat4ArrayElementByName(const char * pName, R4Pt in_pt , int pElement ) {
	CGparameter p = cgGetFirstEffectParameter(effect);

	while(p) {
		if(!strcmp(cgGetParameterName(p), pName)) {
			break;
		}
		p = cgGetNextParameter(p);
	}

	if(!p) {
		ShaderError(LOC(), "Unable to find array with name %s");
		return false;
	}
	int ArraySize = cgGetArraySize(p, 0);

	if(pElement > ArraySize) {
		ShaderError(LOC(), "Out of bounds array element");
		return false;
	}
	CGparameter element = cgGetArrayParameter(p, pElement);

	double pt[4] = { in_pt[0], in_pt[1], in_pt[2], in_pt[3]};
	cgSetParameter4dv(element, pt);

	return true;

}

bool OGLShader2::SetFloat3ArrayElementByName(const char * pName, R3Pt pPt, int pElement) {
	CGparameter p = cgGetFirstEffectParameter(effect);
	while(p) {
		if(!strcmp(cgGetParameterName(p), pName)) {
			break;
		}
		p = cgGetNextParameter(p);
	}
	if(!p) {
		ShaderError(LOC(), "Unable to find array with name %s");
		return false;
	}

	int ArraySize = cgGetArraySize(p, 0);
	
	if(pElement > ArraySize) {
		ShaderError(LOC(), "Out of bounds array element");
		return false;
	}
  
	CGparameter element = cgGetArrayParameter(p, pElement);
	//printf("Typerific:%s\n", cgGetTypeString(cgGetParameterType(element)));
	double test[3] = {pPt[0], pPt[1], pPt[2]};
	cgSetParameter3dv(element, test);

	return true;
}

bool OGLShader2::SetParameterVariabilityByName(const char *pSemantic, CGenum pVar) {
	CGparameter p = cgGetNamedEffectParameter(effect, pSemantic);
	cgSetParameterVariability(p, pVar);
	return true;
}

OGLShader2::~OGLShader2() {

}
#endif