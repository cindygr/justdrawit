#include <WINSystemDefines.H>
#include <OpenGL/OGLRoutines_State.H>

#if defined(powerpc) || defined(__APPLE__)
#include <OpenGL/glext.h>
#include <GLUT/glut.h>
#else
#include <GL/glExt.h>
#include <GL/glut.h>
#endif

//#include <FL/tube.h>  /* the OpenGL Tubing and Extrusion library by
//                         Linas Vepstas */

OGLRoutines_State g_OGLState;

/*** OPENGL INITIALIZATION AND CHECKS ***/

/* supportsOneDotTwo - returns true if supports OpenGL 1.2 or higher. */
static bool
supportsOneDotTwo(void)
{
  const char *version;
  int major, minor;

  version = (char *) glGetString(GL_VERSION);
  if (sscanf(version, "%d.%d", &major, &minor) == 2) {
      return ( major > 1 || minor >= 2 ) ? TRUE : FALSE;
  }
  return FALSE;            /* OpenGL version string malformed! */
}

#ifdef _WIN32
# define GET_PROC_ADDRESS(x) wglGetProcAddress(x)
#else
# ifdef macintosh
#  ifndef GL_MAC_GET_PROC_ADDRESS_NV
#   define GL_MAC_GET_PROC_ADDRESS_NV 0x84FC
#  endif
#  define GET_PROC_ADDRESS(x) macGetProcAddress(x)
void *(*macGetProcAddress)(char *string);
# else
#  define GET_PROC_ADDRESS(x) glXGetProcAddress(x)
# endif
#endif


/* initExtensions - Check if required extensions exist. */
bool OGLRoutines_State::CheckShadowMap( const bool in_bForceEnvCombine )
{
    if ( in_bForceEnvCombine == TRUE ) {
        printf("Forcing use of GL_EXT_texture_env_combine.\n");

        if ( m_bHasTextureEnvCombine == FALSE ) {
            printf("I require the GL_EXT_texture_env_combine OpenGL extension to work\n");
            return FALSE;
        }
    } else {
        if ( m_bHasRegisterCombiners == FALSE && m_bHasTextureEnvCombine) {
            printf("I require one of the following OpenGL extensions to work:\n"
             "  GL_EXT_texture_env_combine\n"
             "  GL_NV_register_combiners\n");
            return FALSE;
        }
    }

    if ( m_bHasDepthTexture && m_bHasRegisterCombiners && m_bHasSeparateSpecularColor) {
        printf("Hardware shadow mapping support.\n");
        m_bHasShadowMapSupport = 1;
    }

    SetPolygonOffset( m_fSlopeScale );

    return TRUE;
}

void OGLRoutines_State::SetPolygonOffset( const float in_f )
{
    GLfloat scale, bias;

    m_fSlopeScale = in_f;

    if ( m_bHasShadowMapSupport == TRUE ) {
        if ( m_ihwDepthMapPrecision == GL_UNSIGNED_SHORT) {
            scale = m_fSlopeScale;
            bias = c_iDepthBias16 * m_iDepthScale16;
        } else {
            scale = m_fSlopeScale;
            bias = c_iDepthBias24 * m_iDepthScale24;
        }
    } else {
        if ( m_iDepthMapPrecision == GL_UNSIGNED_SHORT) {
            scale = m_fSlopeScale;
            bias = c_iDepthBias16 * m_iDepthScale16;
        } else {
            scale = m_fSlopeScale;
            bias = c_iDepthBias8 * m_iDepthScale8;
        }
    }
    glPolygonOffset(scale, bias);
}

/* initExtensions - Check if required extensions exist. */
void
OGLRoutines_State::InitExtensions()
{
    printf("Checking extensions\n");
    
    m_bHasOpenGL12 = supportsOneDotTwo();

    m_bHasARB_multitexture      = glutExtensionSupported("GL_ARB_multitexture") ? TRUE : FALSE;
    m_bHasEXT_bgra              = glutExtensionSupported("GL_EXT_bgra") ? TRUE : FALSE;
    m_bHasRegisterCombiners     = glutExtensionSupported("GL_NV_register_combiners") ? TRUE : FALSE;
    m_bHasTextureEnvCombine     = glutExtensionSupported("GL_EXT_texture_env_combine") ? TRUE : FALSE;
    m_bHasDepthTexture          = glutExtensionSupported("GL_SGIX_depth_texture") ? TRUE : FALSE;
    m_bHasTextureBorderClamp    = glutExtensionSupported("GL_ARB_texture_border_clamp") ? TRUE : FALSE;
    m_bHasTextureEdgeClamp      = glutExtensionSupported("GL_EXT_texture_edge_clamp") ? TRUE : FALSE;
    m_bHasSeparateSpecularColor = glutExtensionSupported("GL_EXT_separate_specular_color") ? TRUE : FALSE;
    m_bHasTextureRectangle      = glutExtensionSupported("GL_NV_texture_rectangle") ? TRUE : FALSE;

    if ( m_bHasTextureRectangle == TRUE ) {
#if defined(powerpc) || defined(__APPLE__)
        glGetIntegerv(GL_MAX_RECTANGLE_TEXTURE_SIZE_EXT, &m_iMaxRectangleTextureSize);
#else
        glGetIntegerv(GL_MAX_RECTANGLE_TEXTURE_SIZE_NV, &m_iMaxRectangleTextureSize);
#endif
    } else {
        m_iMaxRectangleTextureSize = 512;
    }

}


void OGLRoutines_State::InitDepth()
{
    glGetIntegerv(GL_DEPTH_BITS, &m_iDepthBits);
    printf("depth buffer precision = %d\n", m_iDepthBits);
    if (m_iDepthBits >= 24) {
        m_ihwDepthMapPrecision = GL_UNSIGNED_INT;
#if defined(powerpc) || defined(__APPLE__)
        m_ihwDepthMapInternalFormat = GL_DEPTH_COMPONENT24_ARB;
#else
        m_ihwDepthMapInternalFormat = GL_DEPTH_COMPONENT24_SGIX;
#endif
    }

    if ( m_bHasRegisterCombiners == TRUE ) {
        m_iDepthMapFormat = GL_LUMINANCE_ALPHA;
        m_iDepthMapInternalFormat = GL_LUMINANCE8_ALPHA8;
        m_iDepthMapPrecision = GL_UNSIGNED_SHORT;
    }

    if ( m_iDepthBits < 24) {
        m_iDepthScale24 = 1;
    } else {
        m_iDepthScale24 = 1 << (m_iDepthBits - 24);
    }
    if (m_iDepthBits < 16) {
        m_iDepthScale16 = 1;
    } else {
        m_iDepthScale16 = 1 << (m_iDepthBits - 16);
    }
    if (m_iDepthBits < 8) {
        m_iDepthScale8 = 1;
    } else {
        m_iDepthScale8 = 1 << (m_iDepthBits - 8);
    }

}

void OGLRoutines_State::Init()
{
    InitExtensions();
    InitDepth();

    glEnable(GL_NORMALIZE);
}

OGLRoutines_State::OGLRoutines_State()
:
m_ihwDepthMapPrecision( GL_UNSIGNED_SHORT ),
#if defined(powerpc) || defined(__APPLE__)
m_ihwDepthMapInternalFormat( GL_DEPTH_COMPONENT16_ARB ),
#else
m_ihwDepthMapInternalFormat( GL_DEPTH_COMPONENT16_SGIX ),
#endif
m_ihwDepthMapFiltering( GL_LINEAR ),
m_iDepthMapPrecision( GL_UNSIGNED_BYTE ),
m_iDepthMapFormat( GL_LUMINANCE ),
m_iDepthMapInternalFormat( GL_INTENSITY8 ),
m_fSlopeScale(1.1f),
c_iDepthBias8(2),
c_iDepthBias16(6),
c_iDepthBias24(8)
{
}

