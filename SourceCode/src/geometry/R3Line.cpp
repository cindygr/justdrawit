#include "StdAfx.H"
#include <utils/Rn_Line.H>

/* ------------------------------------------------------------------------- */

WINbool R3Line::Intersect( const R3Line &in_line, R3Pt &out_pt ) const
{
    double dS, dT;
    return Intersect( in_line, out_pt, dS, dT );
}

WINbool R3Line::Intersect( const R3Line &in_line, R3Pt &out_pt, double &out_dS, double &out_dT ) const
{
    const R3Pt &p1 = Pt();
    const R3Pt &p3 = in_line.Pt();
    const R3Pt &p4 = in_line.Pt() + in_line.Vec();
    const R3Vec v13 = p1 - p3;
    const R3Vec v43 = p4 - p3;
    
    out_dS = 0.0;
    out_dT = 0.0;
    out_pt = Lerp( p1, p3, 0.5 );

    if ( RNIsZero( Length( in_line.Vec() ) ) && RNIsZero( Length( Vec() ) ) ) {
        return FALSE;
    }
    
    if ( RNIsZero( Length( in_line.Vec() ) ) ) {
        out_dS = Dot( in_line.Pt() - Pt(), UnitSafe( Vec() ) );
        const R3Pt ptOnMe = Pt() + Vec() * out_dS;
        out_pt = Lerp( in_line.Pt(), ptOnMe, 0.5 );
        return FALSE;
    }
    
    if ( RNIsZero( Length( Vec() ) ) ) {
        out_dT = Dot( Pt() - in_line.Pt(), UnitSafe( in_line.Vec() ) );
        const R3Pt ptOnLine = in_line.Pt() + in_line.Vec() * out_dT;
        out_pt = Lerp( Pt(), ptOnLine, 0.5 );
        return FALSE;
    }
    
    const R3Vec &v21 = Vec();
        
    const double d1343 = Dot( v13, v43 ); //p13.x * p43.x + p13.y * p43.y + p13.z * p43.z;
    const double d4321 = Dot( v43, v21 ); //p43.x * p21.x + p43.y * p21.y + p43.z * p21.z;
    const double d1321 = Dot( v13, v21 ); //p13.x * p21.x + p13.y * p21.y + p13.z * p21.z;
    const double d4343 = Dot( v43, v43 ); //p43.x * p43.x + p43.y * p43.y + p43.z * p43.z;
    const double d2121 = Dot( v21, v21 ); // p21.x * p21.x + p21.y * p21.y + p21.z * p21.z;
    
    const double denom = d2121 * d4343 - d4321 * d4321;
    if ( RNIsZero( denom, 1e-14 ) ) { // parallel
        out_dS = Dot( in_line.Pt() - Pt(), UnitSafe( Vec() ) );
        const R3Pt ptOnMe = Pt() + Vec() * out_dS;
        out_pt = Lerp( in_line.Pt(), ptOnMe, 0.5 );
        return FALSE;
    }
    const double numer = d1343 * d4321 - d1321 * d4343;
    
    out_dS = numer / denom;
    out_dT = (d1343 + d4321 * out_dS) / d4343;

    const R3Pt ptOnMe = Pt() + Vec() * out_dS;
    const R3Pt ptOnLine = in_line.Pt() + in_line.Vec() * out_dT;

    out_pt = Lerp( ptOnMe, ptOnLine, 0.5 );
    
    if ( fabs(denom) > 1e-6 ) {
        const double d1 =  Dot( out_pt - ptOnMe, Vec() );
        const double d2 = Dot( out_pt - ptOnLine, in_line.Vec() );
        ASSERT( RNIsZero( d1, 1e-12 ) );
        ASSERT( RNIsZero( d2, 1e-12 ) );
    } else {
        const double d1 =  Dot( out_pt - ptOnMe, Vec() );
        const double d2 = Dot( out_pt - ptOnLine, in_line.Vec() );
        ASSERT( RNIsZero( d1, 1e-6 ) );
        ASSERT( RNIsZero( d2, 1e-6 ) );
    }

    return ApproxEqual( ptOnMe, ptOnLine );
}

/* -------------------------------------------------------------------------
 * DESCR   :	Return a perpendicular line
 * ------------------------------------------------------------------------- */
R3Line
R3Line::Perpendicular(const R3Pt &in_pt) const 
{
    R3Vec vec_opp(Vec());
    if (vec_opp[1] == 1.0) vec_opp[0] = 1;
    else                   vec_opp[1] = 1;
    R3Pt pt_random(Pt() + vec_opp);
    
    return R3Line(in_pt, Project(pt_random) - pt_random);
}

/* -------------------------------------------------------------------------
 * DESCR   :	Return a parallel line
 * ------------------------------------------------------------------------- */
R3Line
R3Line::Parallel(const R3Pt &in_pt) const 
{
    return R3Line(in_pt, Vec());
}

/* -------------------------------------------------------------------------
 * DESCR   :	Print this 
 * ------------------------------------------------------------------------- */
void R3Line::Print() const
{
    cout << "RNline" << Dim() << " point " << Pt() << " vec " << Vec() << "\n";
}

void R3Line::Write(ofstream &out) const
{
    out << Pt() << " " << Vec() << "\n";
}

WINbool R3Line::Read(ifstream &in)
{
    WINbool bRes1 = m_pt.Read(in);
    WINbool bRes2 = m_vec.Read(in);
    return (bRes1 && bRes2) ? TRUE : FALSE;
}

WINbool R3Line::Test()
{
   R3Pt pt1d(0,0.1,0), pt2d(2,0,0);
   R3Vec vecd(2, 0, 0);
   R3Line line1d, line3d;
   R3Line line2d(pt1d, vecd);

   line1d = line2d;
   VERIFY(line2d.Dim() == 3);
   VERIFY(line1d == line2d);
   VERIFY(Length(line1d.Vec()) == 1.0);

   pt2d = line1d(0.2);
   cerr << "Point " << pt2d << "\n";
   VERIFY(pt2d[0] == 0.2 && pt2d[1] == 0.1 && pt2d[2] == 0);
   VERIFY(IsParallel(line1d, line2d));

   double out_t_on_line = -1, out_d_to_line = -1;
   vecd = R3Vec(0, 0.1, 0.2);
   pt1d = pt2d + vecd;
   VERIFY(line1d.FindPtOnLine(pt1d, pt2d, out_t_on_line, out_d_to_line) == FALSE);
   cerr << "t " << out_t_on_line << " " << pt1d << " " << pt2d << "\n";
   cerr << line1d(0.2) << "\n";
   VERIFY(pt2d == line1d(0.2));
   ASSERT(out_t_on_line == 0.2);
   VERIFY(out_d_to_line == Length(vecd));

   line1d.Print();
   ofstream out("RNtest.txt", ios::out);
   line1d.Write(out);
   out.close();
   ifstream in("RNtest.txt", ios::in);
   line3d.Read(in);
   VERIFY(line1d == line3d);

   R3Pt pt1(0,0.1,0), pt2(1,0,0);
   R3Vec vec(0, 0.2, 0.2);
   R3Line line1, line2(pt1, pt2), line3(pt1, vec);
   R3Line line4(line1);

   line1 = line2;
   VERIFY(line1 == line2);
   line1 = line2.Perpendicular(pt2);
   VERIFY(IsPerpendicular(line1, line2));

   line1 = line2.Parallel(pt2);
   VERIFY(IsParallel(line1, line2));

   VERIFY(IsPerpendicular(line2, line3) == FALSE);
   VERIFY(IsParallel(line2, line3) == FALSE);

   line2.SetPt(line1.Pt());
   line2.SetVec(line1.Vec() * 3.0);
   VERIFY(line1 == line2);
   VERIFY(!(line1 == line3));

   return TRUE;
}

