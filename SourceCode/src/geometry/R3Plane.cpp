#include "StdAfx.H"
#include <utils/R3_Plane.H>
#include <utils/Rn_Line.H>
#include <utils/Rn_Line_seg.H>

R3Matrix R3Plane::ToXYZ() const
{
    R3Matrix mat;
    R3Matrix::MatrixVecToVec( mat, UnitSafe( Normal() ), R3Vec(0,1,0) );
    return mat;
}

void R3Plane::Normalize()
{
    const double dLen = sqrt( m_dA * m_dA + m_dB * m_dB + m_dC * m_dC );

    ASSERT( !RNIsZero( dLen ) );

    m_dA /= dLen;
    m_dB /= dLen;
    m_dC /= dLen;
    m_dD /= dLen;
}

R3Pt R3Plane::PtOnPlane() const
{
    if ( RNIsZero( m_dD ) ) {
        return R3Pt(0,0,0);
    }

    if ( !RNIsZero( m_dA ) ) {
        return R3Pt( -m_dD/m_dA, 0.0, 0.0 );
    }
    if ( !RNIsZero( m_dB ) ) {
        return R3Pt( 0.0, -m_dD/m_dB, 0.0 );
    }
    if ( !RNIsZero( m_dC ) ) {
        return R3Pt( 0.0, 0.0, -m_dD/m_dC );
    }

    ASSERT(FALSE);
    return R3Pt(0,0,0);
}

void R3Plane::TangentPlane( R3Vec &out_vecDs, R3Vec &out_vecDt ) const
{
    const R3Vec vecNorm = Normal();
    if ( RNIsZero( vecNorm[0] ) ) {
        out_vecDs[0] = 0;
        out_vecDs[1] = -vecNorm[2];
        out_vecDs[2] = vecNorm[1];
    } else if ( RNIsZero( vecNorm[1] ) ) {
        out_vecDs[1] = 0;
        out_vecDs[0] = -vecNorm[2];
        out_vecDs[2] = vecNorm[0];
    } else if ( RNIsZero( vecNorm[2] ) ) {
        out_vecDs[2] = 0;
        out_vecDs[1] = -vecNorm[2];
        out_vecDs[2] = vecNorm[1];
    } else {
        out_vecDs[0] = 0;
        out_vecDs[1] = -vecNorm[2];
        out_vecDs[2] = vecNorm[1];
    }

    out_vecDt = UnitSafe( Cross( vecNorm, out_vecDs ) );
    out_vecDs = UnitSafe( Cross( out_vecDt, vecNorm ) );
}

// d * dA * dA + d * dB * dB + d * dC * dC + m_dD = 0
// d( dA * dA + dB * dB + dC * dC) = -dD
double R3Plane::DistOnNormal() const 
{
    const double dLen = sqrt( m_dA * m_dA + m_dB * m_dB + m_dC * m_dC );

    ASSERT( !RNIsZero( dLen ) );

    return -m_dD / dLen;
}

///
R3Pt R3Plane::ProjectOnPlane( const R3Pt &in_pt ) const
{
    const R3Vec vecNorm = Normal();
    const double dRHS = -m_dD - m_dA * in_pt[0] - m_dB * in_pt[1] - m_dC * in_pt[2];
    const double dLHS = Dot( vecNorm, vecNorm );
    
    if ( RNIsZero( dLHS, 1e-12 ) ) {
        return in_pt;
    }
    const double dScl = dRHS / dLHS;
    const R3Pt ptRet = in_pt + Normal() * dScl;
    //const double dCheck = Evaluate( ptRet );
    return ptRet;
        /*
    const R3Pt pt = in_pt + Normal();
    const double dDistToPlane = DistOnNormal();
    const R3Vec vecNormal = Normal();

    double s1 = Dot( (pt - R3Pt(0,0,0)), vecNormal) - dDistToPlane;
    double s2 = Dot( (in_pt - R3Pt(0,0,0)), vecNormal) - dDistToPlane;

    if ( RNIsZero( s1 - s2 ) )
        return in_pt;

    return pt + (in_pt - pt) * (s1 / ( s1 - s2 ) ); 
         */
}

///
double R3Plane::DistToPlane( const R3Pt &in_pt ) const
{
    return fabs( Evaluate( in_pt ) / Length( Normal() ) );
}

///
WINbool R3Plane::IntersectRay( const R3Ray &in_ray, 
                               double &out_dT, 
                               R3Pt   &out_pt ) const
{
    const R3Pt &in_ptRay = in_ray.Pt();
    const R3Vec &in_vecRay = in_ray.Dir();
    const double dDenom = m_dA * in_vecRay[0] + m_dB * in_vecRay[1] + m_dC * in_vecRay[2];

    if ( RNIsZero( dDenom ) ) {
        if ( IsPtOnPlane( in_ptRay ) ) {
            out_dT = 0.5;
            out_pt = in_ptRay + in_vecRay * out_dT;
            return TRUE;
        } else {
            out_dT = 0.0;
            out_pt = in_ptRay;
            return FALSE;
        }
    }

    out_dT = - ( m_dD + m_dA * in_ptRay[0] + m_dB * in_ptRay[1] + m_dC * in_ptRay[2] ) / dDenom;
    out_pt = in_ptRay + in_vecRay * out_dT;

    return (out_dT >= 0.0) ? TRUE : FALSE;
}

R3Pt R3Plane::IntersectRay( const R3Ray &in_ray ) const
{
    double dT;
    R3Pt ptOut;
    
    IntersectRay( in_ray, dT, ptOut );
    return ptOut;
}

///
WINbool R3Plane::Intersect( const R3Line_seg &in_seg, double &out_dT, R3Pt &out_pt ) const
{
    const R3Pt pt = in_seg.P1();
    const R3Vec vec = in_seg.P2() - in_seg.P1();

    const double dDenom = m_dA * vec[0] + m_dB * vec[1] + m_dC * vec[2];

    if ( RNIsZero( dDenom ) ) {
        if ( IsPtOnPlane( in_seg.P1() ) ) {
            out_dT = 0.5;
            out_pt = in_seg(out_dT);
            return TRUE;
        } else {
            out_dT = 0.0;
            out_pt = in_seg.P1();
            return FALSE;
        }
    }

    out_dT = - ( m_dD + m_dA * pt[0] + m_dB * pt[1] + m_dC * pt[2] ) / dDenom;
    out_pt = in_seg( out_dT );

    return (out_dT >= 0.0 && out_dT <= 1.0) ? TRUE : FALSE;
}

/// a (px + t vx) + b (py + t vy) + c  (pz + t vz) + d = 0
/// t (a vx + b vy + c vz ) = - ( d + a px + b py + c pz )
WINbool R3Plane::Intersect( const R3Line &in_line, double &out_dT, R3Pt &out_pt ) const
{
    const R3Pt pt = in_line.Pt();
    const R3Vec vec = in_line.Vec();

    const double dDenom = m_dA * vec[0] + m_dB * vec[1] + m_dC * vec[2];

    if ( RNIsZero( dDenom ) ) {
        if ( IsPtOnPlane( in_line.Pt() ) ) {
            out_dT = 0.5;
            out_pt = in_line(out_dT);
            return TRUE;
        } else {
            out_dT = 0.0;
            out_pt = in_line.Pt();
            return FALSE;
        }
    }

    out_dT = - ( m_dD + m_dA * pt[0] + m_dB * pt[1] + m_dC * pt[2] ) / dDenom;
    out_pt = in_line( out_dT );

    return TRUE;
}

//@}

R3Plane R3Plane::Transform( const R4Matrix &in_mat ) const
{
    WINbool bBool;
    const R4Matrix mat = in_mat.Inverse( bBool ).Transpose();
    const R4Pt pt = mat * R4Pt( m_dA, m_dB, m_dC, m_dD );

    ASSERT ( bBool == TRUE );
    return R3Plane( pt );
}

void R3Plane::Flip()
{
    m_dA = -1.0 * m_dA;
    m_dB = -1.0 * m_dB;
    m_dC = -1.0 * m_dC;
    m_dD = -1.0 * m_dD;
}
///
WINbool R3Plane::operator==( const R3Plane &in_p ) const
{
    if ( !RNApproxEqual( in_p.m_dA, m_dA ) )
        return FALSE;
    if ( !RNApproxEqual( in_p.m_dB, m_dB ) )
        return FALSE;
    if ( !RNApproxEqual( in_p.m_dC, m_dC ) )
        return FALSE;
    if ( !RNApproxEqual( in_p.m_dD, m_dD ) )
        return FALSE;

    return TRUE;
}

///
R3Plane & R3Plane::operator=( const R3Plane &in_p )
{
    m_dA = in_p.m_dA;
    m_dB = in_p.m_dB;
    m_dC = in_p.m_dC;
    m_dD = in_p.m_dD;

    return *this;
}


///
R3Plane::R3Plane( const R3Plane &in_p )
{
    (*this) = in_p;
}

///
R3Plane::R3Plane( const R3Pt &in_ptOnPlane, const R3Vec &in_vecNormal )
{
    ASSERT( !RNIsZero( Length( in_vecNormal ) ) );

    const R3Vec vec = UnitSafe( in_vecNormal );

    m_dA = vec[0];
    m_dB = vec[1];
    m_dC = vec[2];

    m_dD = - (m_dA * in_ptOnPlane[0] + m_dB * in_ptOnPlane[1] + m_dC * in_ptOnPlane[2] );

}

R3Plane::R3Plane( const R3Pt &in_pt1, const R3Pt &in_pt2, const R3Pt &in_pt3 )
{
    const R3Vec vecNorm = Cross( in_pt2 - in_pt1, in_pt3 - in_pt1 );

    ASSERT( !RNIsZero( Length( vecNorm ) ) );

    const R3Vec vec = UnitSafe( vecNorm );

    m_dA = vec[0];
    m_dB = vec[1];
    m_dC = vec[2];

    m_dD = - (m_dA * in_pt1[0] + m_dB * in_pt1[1] + m_dC * in_pt1[2] );
}

///
R3Plane::R3Plane( const double in_dDist, const R3Vec &in_vecNormal )
{
    const R3Pt pt = R3Pt(0,0,0) + in_vecNormal * in_dDist;

    ASSERT( !RNIsZero( Length( in_vecNormal ) ) );

    const R3Vec vec = UnitSafe( in_vecNormal );

    m_dA = vec[0];
    m_dB = vec[1];
    m_dC = vec[2];

    m_dD = - (m_dA * pt[0] + m_dB * pt[1] + m_dC * pt[2] );
}

R3Plane::R3Plane( const R4Pt &in_ptABCD )
{
    //Force normal to be unit length
    const double dLen = sqrt( in_ptABCD[0] * in_ptABCD[0] + in_ptABCD[1] * in_ptABCD[1] + in_ptABCD[2] * in_ptABCD[2] );

    ASSERT( !RNIsZero(dLen) );
    m_dA = in_ptABCD[0] / dLen;
    m_dB = in_ptABCD[1] / dLen;
    m_dC = in_ptABCD[2] / dLen;
    m_dD = in_ptABCD[3] / dLen;
}

//@}

/**@name Constructors, comparitors */
//@{
///
void R3Plane::Write(ofstream &out) const
{
    out << m_dA << " " << m_dB << " " << m_dC << " " << m_dD << "\n";
}
///
void R3Plane::Read(ifstream &in)
{
    in >> m_dA >> m_dB >> m_dC >> m_dD;
}

WINbool R3Plane::Test()
{
    R3Plane pl1( R4Pt( 0.3, 1.0, 3.2, 0.6 ) );
    R3Plane pl2( R3Pt( 0.3, 1.0, 3.2 ), R3Vec( 0.7, 0.2, 0.8 ) );
    R3Plane pl3( 0.2, R3Vec( 0.7, 0.2, 0.8 ) );
    R3Plane pl4( pl1 );

    ASSERT( RNApproxEqual( Length( pl1.Normal() ), 1.0 ) );
    ASSERT( RNApproxEqual( Length( pl2.Normal() ), 1.0 ) );
    ASSERT( RNApproxEqual( Length( pl3.Normal() ), 1.0 ) );
    ASSERT( RNApproxEqual( Length( pl4.Normal() ), 1.0 ) );

    ASSERT( RNApproxEqual( pl3.DistOnNormal(), 0.2 ) );

    ASSERT( pl1.IsPtOnPlane( pl1.PtOnPlane() ) == TRUE );
    ASSERT( pl2.IsPtOnPlane( pl2.PtOnPlane() ) == TRUE );
    ASSERT( pl3.IsPtOnPlane( pl3.PtOnPlane() ) == TRUE );
    ASSERT( pl4.IsPtOnPlane( pl4.PtOnPlane() ) == TRUE );

    ASSERT( pl4 == pl1 );

    return TRUE;
}

