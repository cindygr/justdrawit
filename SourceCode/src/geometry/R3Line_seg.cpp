#include "StdAfx.H"

#include <utils/Rn_Line_seg.H>

/* -------------------------------------------------------------------------
 * DESCR   :	Construct a line segment perpendicular to the current line
 * 		segment with one point on the line segment, the other at
 * 		the given point
 * ------------------------------------------------------------------------- */
R3Line_seg 
R3Line_seg::Perpendicular(const R3Pt &in_p) const
{
    R3Line l1(P1(), P2());
    R3Line perp = l1.Perpendicular(in_p);

    return R3Line_seg(in_p, in_p + perp.Vec());
}

R3Line_seg 
R3Line_seg::Parallel(const R3Pt &in_p) const
{
    return R3Line_seg(in_p, in_p + (P2() - P1()));
}

WINbool
R3Line_seg::FindPtOnSeg( const R3Pt & in_p, 
						 R3Pt       & out_ptClosest, 
						 double     & out_t_on_seg, 
						 double     & out_d_to_seg) const
{
	const R3Vec vec = (P2() - P1());
	const double dLen = ::Length(vec);
	
    if ( RNIsZero( dLen ) ) {
        out_t_on_seg = 0.5;
        out_ptClosest = P1();
        out_d_to_seg = ::Length( in_p - P1() );
        return (RNIsZero( out_d_to_seg )) ? TRUE : FALSE;
    }
	out_t_on_seg = Dot( in_p - P1(), vec * (1.0 / dLen) );

	if ( out_t_on_seg <= 0.0 ) {
		out_ptClosest = P1();
		out_t_on_seg = 0.0;
		out_d_to_seg = ::Length( P1() - in_p );
	} else if ( out_t_on_seg >= dLen ) {
		out_ptClosest = P2();
		out_t_on_seg = 1.0;
		out_d_to_seg = ::Length( P2() - in_p );
	} else {
		out_t_on_seg = out_t_on_seg / dLen;
		out_ptClosest = P1() + vec * out_t_on_seg;
		out_d_to_seg = ::Length( out_ptClosest - in_p );
	}
	
	return RNIsZero( out_d_to_seg );
}


WINbool R3Line_seg::Intersect( const R3Vec & in_vecPlaneNormal,
                               const double  in_dDistAlong,
                               double      & out_dAlongSeg,
                               R3Pt        & out_pt ) const
{
    WINbool bRes = RNIntersectPlaneSegment( in_vecPlaneNormal, in_dDistAlong,
                                            (*this),
                                            out_pt );

    out_dAlongSeg = Projected_dist_on_seg( out_pt );

    return (bRes == FALSE || IsPtOnSeg( out_pt ) == FALSE ) ? FALSE : TRUE;
}

WINbool R3Line_seg::Intersect( const R3Line_seg & in_lineSeg, 
                               R3Pt             & out_pt,
                               double           & out_s, 
                               double           & out_t) const
{
	if ( ApproxEqual( P1(), P2() ) ) {
		double dDist = 0.0;
		out_s = 0.5;
		return in_lineSeg.FindPtOnSeg( P1(), out_pt, out_t, dDist );
	}
	if ( ApproxEqual( in_lineSeg.P1(), in_lineSeg.P2() ) ) {
		double dDist = 0.0;
		out_t = 0.5;
		return FindPtOnSeg( in_lineSeg.P1(), out_pt, out_s, dDist );
	}
    const R3Line line1( P1(), P2() );
    const R3Line line2( in_lineSeg.P1(), in_lineSeg.P2() );

    const WINbool bRes = line1.Intersect( line2, out_pt, out_s, out_t );
    
    const double dLen1 = ::Length( P2() - P1() );
    const double dLen2 = ::Length( in_lineSeg.P2() - in_lineSeg.P1() );

    if ( !RNIsZero( dLen1 ) ) {
        out_s /= dLen1;
    }
    if ( !RNIsZero( dLen1 ) ) {
        out_t /= dLen2;
    }
    
    if ( out_s > 0.0 && out_s < 1.0 && out_t > 0.0 && out_t < 1.0 ) {
        return bRes;
    }
    
    R3Pt aptTry[6];
    int iTry = 4;
    aptTry[0] = R3Pt( LengthSq( P1() - in_lineSeg.P1() ), 0.0, 0.0 );
    aptTry[1] = R3Pt( LengthSq( P1() - in_lineSeg.P2() ), 0.0, 1.0 );
    aptTry[2] = R3Pt( LengthSq( P2() - in_lineSeg.P1() ), 1.0, 0.0 );
    aptTry[3] = R3Pt( LengthSq( P2() - in_lineSeg.P2() ), 1.0, 1.0 );
    
    if ( out_s > 0.0 && out_s < 1.0 ) {
        const R3Pt pt = Lerp( P1(), P2(), out_s );
        aptTry[4] = R3Pt( LengthSq( pt - in_lineSeg.P1() ), out_s, 0.0 );
        aptTry[5] = R3Pt( LengthSq( pt - in_lineSeg.P2() ), out_s, 1.0 );
        iTry = 6;
    }
    if ( out_t > 0.0 && out_t < 1.0 ) {
        const R3Pt pt = Lerp( in_lineSeg.P1(), in_lineSeg.P2(), out_t );
        aptTry[4] = R3Pt( LengthSq( pt - P1() ), 0.0, out_t );
        aptTry[5] = R3Pt( LengthSq( pt - P2() ), 1.0, out_t );
        iTry = 6;
    }
    
    int iClosest = 0;
    for ( int i = 1; i < iTry; i++ ) {
        if ( aptTry[i][0] < aptTry[ iClosest ][0] ) {
            iClosest = i;
        }
    }
    out_s = aptTry[iClosest][1];
    out_t = aptTry[iClosest][2];

    const R3Pt ptOnMe = P1() + (P2() - P1()) * WINminmax( out_s, 0.0, 1.0 );
    const R3Pt ptOnLine = in_lineSeg.P1() + (in_lineSeg.P2() - in_lineSeg.P1()) * WINminmax( out_t, 0.0, 1.0 );
    
    out_pt = Lerp( ptOnMe, ptOnLine, 0.5 );
    
    return ::Length( ptOnMe - ptOnLine ) < RNEpsilon_d;
}


/* -------------------------------------------------------------------------
 * DESCR   :	Print this jeune fijs
 * ------------------------------------------------------------------------- */
void R3Line_seg::Print() const 
{
    cout << "RNline_seg " << Dim() << " R3Pts: " << P1() << P2() << "\n";
}

void R3Line_seg::Write(ofstream &out) const 
{
    out << P1() << " " << P2() << "\n";
}

WINbool R3Line_seg::Read(ifstream &in) 
{
   WINbool bRes1 = P1().Read(in);
   WINbool bRes2 = P2().Read(in);
   return (bRes1 && bRes2) ? TRUE : FALSE;
}

WINbool R3Line_seg::Test()
{
   R3Pt pt1d(0,0.1,0), pt2d(2,0,0);
   R3Vec vecd(2, 0, 0);
   R3Line_seg line1d, line3d;
   R3Line_seg line2d(pt1d, pt1d + vecd);

   line1d = line2d;
   VERIFY(line2d.Dim() == 3);
   VERIFY(line1d == line2d);
   VERIFY(( ::Length(line1d.P2() - line1d.P1()) ) == ( ::Length(vecd) ));
   VERIFY(line1d.Length() == ( ::Length(vecd)) );

   pt2d = line1d(0.2);
   VERIFY(pt2d[0] == 0.4 && pt2d[1] == 0.1 && pt2d[2] == 0);
   VERIFY(IsParallelSeg(line1d, line2d));

   double out_t_on_line = -1, out_d_to_line = -1;
   vecd = R3Vec(0, 0.1, 0.2);
   pt1d = pt2d + vecd;
   VERIFY(line1d.FindPtOnSeg(pt1d, pt2d, out_t_on_line, out_d_to_line) == FALSE);
   VERIFY(pt2d == line1d(0.2));
   ASSERT(out_t_on_line == 0.2);
   VERIFY(out_d_to_line == ( ::Length(vecd) ));

   pt2d = line1d(1.2);
   pt1d = pt2d;
   VERIFY(line1d.FindPtOnSeg(pt1d, pt2d, out_t_on_line, out_d_to_line) == FALSE);
   VERIFY(pt2d == line1d.P2());
   ASSERT(out_t_on_line == 1.0);
   VERIFY(RNApproxEqual(out_d_to_line, ( ::Length(line1d(1.2) - line1d.P2()))));

   line1d.Print();
   ofstream out("RNtest.txt", ios::out);
   line1d.Write(out);
   out.close();
   ifstream in("RNtest.txt", ios::in);
   line3d.Read(in);
   VERIFY(line1d == line3d);

   R3Pt pt1(0,0.1,0), pt2(1,0,0);
   R3Vec vec(0, 0.2, 0.2);
   R3Line_seg line1, line2(pt1, pt2), line3(pt1, pt1 + vec);

   line1 = line2;
   VERIFY(line1 == line2);
   line1 = line2.Perpendicular(pt2);
   VERIFY(IsPerpendicularSeg(line1, line2));

   line1 = line2.Parallel(pt2);
   VERIFY(IsParallelSeg(line1, line2));

   VERIFY(IsPerpendicularSeg(line2, line3) == FALSE);
   VERIFY(IsParallelSeg(line2, line3) == FALSE);

   line2.P1() = line1.P1();
   line2.P2() = line1.P2();
   VERIFY(line1 == line2);
   VERIFY(!(line1 == line3));

   return TRUE;
}
