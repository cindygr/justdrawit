/*
 *  RnMatrix.cpp
 *
 *  Copyright 2008 Washington Univ in St. Louis. All rights reserved.
 *
 */

#include <utils/Rn_Defs.H>
#include <utils/Rn_Matrix.H>
#include <f2c.h>
#include <clapack.h>

namespace CwMtx
{

// stores inverse of mat in this
// (note: this requires LAPACK)
template <>
void CWTMatrix<double>::StoreInverse( const CWTMatrix<double> &in_mat, const bool do_not_check_ident )
{
	// NOTE: it is assumed that this has correct dimensions,
	// i.e. CWMatrix(mat.m_ccol, mat.m_crow)
	ASSERT( m_ccol == m_crow );
	//let LAPACK do it!
	integer size = m_ccol;
	//I'd rather use T's for flexibility, but LAPACK requires
	//floating-point numbers, so I use an array of doubles:
	double * matrix = new double[size*size];
	integer info;
	//store the matrix in column-major order
	for( int iC=0; iC<size; iC++ )
		for( int iR=0; iR<size; iR++ )
			matrix[iC*size+iR] = static_cast<double>(in_mat[iR][iC]);
	
	double * work = new double[size*size];
	integer * index = new integer[size];
	//decompose
	dgetrf_(&size,&size,matrix,&size,index,&info);
	//invert
	dgetri_(&size,matrix,&size,index,work,&size,&info);
	//now extract the inverted matrix in column-major order
	for( int iC=0; iC<size; iC++ )
		for( int iR=0; iR<size; iR++ )
			(*this)[iR][iC] = static_cast<double>(matrix[iC*size+iR]);
	delete []matrix;
	delete []work;
	delete []index;

    //check if the inverse is really an inverse
    if( !do_not_check_ident ){
        CWTMatrix<double> matCheck = in_mat * (*this);
        for ( unsigned int iR = 0; iR < m_crow; iR++ ) {
            for ( unsigned int iC = 0; iC < m_ccol; iC++ ) {
                if ( iR == iC ) {
                    ASSERT( RNApproxEqual( matCheck[iR][iC], 1.0, 1e-5 ) );
                }
                else{
                    ASSERT( RNApproxEqual( matCheck[iR][iC], 0.0, 1e-5 ) );
                }
            }
        }
    }
}

// stores inverse of mat in this
// (note: this requires LAPACK)
template <>
void CWTMatrix<float>::StoreInverse( const CWTMatrix<float> &in_mat, const bool do_not_check_ident )
{
	// NOTE: it is assumed that this has correct dimensions,
	// i.e. CWMatrix(mat.m_ccol, mat.m_crow)
	ASSERT( m_ccol == m_crow );
	//let LAPACK do it!
	integer size = m_ccol;
	//I'd rather use T's for flexibility, but LAPACK requires
	//floating-point numbers, so I use an array of doubles:
	float * matrix = new float[size*size];
	integer info;
	//store the matrix in column-major order
	for( int iC=0; iC<size; iC++ )
		for( int iR=0; iR<size; iR++ )
			matrix[iC*size+iR] = static_cast<float>(in_mat[iR][iC]);
	
	float * work = new float[size*size];
	integer * index = new integer[size];
	//decompose
	sgetrf_(&size,&size,matrix,&size,index,&info);
	//invert
	sgetri_(&size,matrix,&size,index,work,&size,&info);
	//now extract the inverted matrix in column-major order
	for( int iC=0; iC<size; iC++ )
		for( int iR=0; iR<size; iR++ )
			(*this)[iR][iC] = static_cast<float>(matrix[iC*size+iR]);
	delete []matrix;
	delete []work;
	delete []index;
	
	//check if the inverse is really an inverse
	if( !do_not_check_ident ){
		CWTMatrix<float> matCheck = in_mat * (*this);
		for ( unsigned int iR = 0; iR < m_crow; iR++ ) {
			for ( unsigned int iC = 0; iC < m_ccol; iC++ ) {
				if ( iR == iC ) {
					ASSERT( RNApproxEqual( matCheck[iR][iC], 1.0f, 1e-5f ) );
				}
				else{
					ASSERT( RNApproxEqual( matCheck[iR][iC], 0.0f, 1e-5f ) );
				}
			}
		}
	}
}
	
};