#include "StdAfx.H"
#include <utils/Rn_Sphere.H>

void R3Sphere::Print() const 
{
    cout << "Sphere dim " << Dim() << " radius " << Radius() << "  ";
    cout << "Center: "<< Center() << "\n";
}

void R3Sphere::Write(ofstream &out) const 
{
    out << Center() << " " << Radius() << "\n";
}

WINbool R3Sphere::Read(ifstream &in) 
{
    WINbool bRes = Center().Read(in);
    in >> Radius();
    return bRes;
}

int R3Sphere::Intersect( const R3Line_seg & in_lseg, 
                         R3Pt             & out_p1, 
                         R3Pt             & out_p2) const
{
    Array<double> adT;
    Array<R3Pt> apt;
    IntersectRay( R3Ray( out_p1, out_p2 - out_p1 ), adT );
    
    if (adT.num() == 0 ) {
        return 0;
    }
    int iRet = 0;
    if ( adT[0] <= 1.0 ) {
        iRet++;
        out_p1 = apt[0];
    }
    if ( adT.num() == 1 ) {
        return iRet;
    }
    if ( adT[1] <= 1.0 ) {
        iRet++;
        if ( iRet == 1 ) {
            out_p1 = apt[1];
        } else {
            out_p2 = apt[1];
        }
    }
    return iRet;
}


Array<R3Pt> R3Sphere::IntersectRay( const R3Ray &in_ray, Array<double> &out_adT ) const
{
    Array<R3Pt> aptOut;

    const R3Vec &vec = in_ray.Dir();
    const R3Vec pt = in_ray.Pt() - m_ptCenter;

    const double dA = pow( vec[0], 2 ) + pow( vec[1], 2 ) + pow( vec[2], 2 );
    const double dB = 2.0 * ( vec[0] * pt[0] + vec[1] * pt[1] + vec[2] * pt[2] );
    const double dC = pow( pt[0], 2 ) + pow( pt[1], 2 ) + pow( pt[2], 2 ) - pow( m_dRadius, 2.0 );

    const double dDet = pow( dB, 2 ) - 4.0 * dA * dC;

    if ( dDet <= 0.0 ) {
        return aptOut;
    } else if ( RNIsZero( dDet ) ) {
        out_adT += -dB / (2.0 * dA);
    } else {
        out_adT += (-dB - sqrt(dDet)) / (2.0 * dA);
        out_adT += (-dB + sqrt(dDet)) / (2.0 * dA);
    }

    for ( int i = 0; i < out_adT.num(); i++ ) {
        aptOut += in_ray( out_adT[i] );

        //ASSERT( On( aptOut[i] ) );
    }

    return aptOut;
}

WINbool R3Sphere::Test()
{
   R3Sphere s1(1.0), s2(R3Pt(1,1,1), 2), s3(s1);

   VERIFY(s1 == s3);
   
   VERIFY(s1.Area() == (4.0 / 3.0) * M_PI);
   VERIFY(s1.SurfaceArea() == 4.0 * M_PI);
   
   s1 = s2;
   VERIFY(s1 == s2);
   

   s1 = R3Sphere(1.0);
   s3 = R3Sphere(3.0);
   s2 = R3Sphere (R3Pt(0.1, 0,0), 2.0);

   VERIFY(s1.Dim() == 3);
   VERIFY(s1.Radius() == 1.0);
   VERIFY(s1.Center() == R3Pt(0,0,0));

   R3Pt pt(0.1, 0.0, 0.2);
   VERIFY(s1.Inside(pt) == TRUE);
   VERIFY(s2.Inside(pt) == TRUE);
   pt[0] = 1.0;
   pt[1] = 0;
   pt[2] = 0;
   VERIFY(s1.On(pt) == TRUE);
   VERIFY(s2.On(pt) == FALSE);
   
   pt[0] = 3.0;
   VERIFY(s1.Inside(pt) == FALSE);

   s3 = s2;
   VERIFY(s3 == s2);
   VERIFY(!(s3 == s1));

   s3.Print();
   ofstream out("RNtest.txt", ios::out);
   s3.Write(out);
   out.close();
   ifstream in("RNtest.txt", ios::in);
   s1.Read(in);
   in.close();
   VERIFY(s3 == s1);

   return TRUE;
}
