/* Copyright 1991, Brown Computer Graphics Group.  All Rights Reserved. */

/* -------------------------------------------------------------------------
   Manipulations of a point in euclidean space
   ------------------------------------------------------------------------- */

#include "StdAfx.H"

#include <utils/Rn_Line_seg.H>


/* -------------------------- protected Routines --------------------------- */

/* -------------------------- Private Routines ----------------------------- */

/* -------------------------- public routines  ----------------------------- */

template<class Point, class Vector>
class RNLine_segTC {
public:
    
    static WINbool FindPtOnSeg( const Point & m_pt1,
                                const Point & m_pt2,
                                const Point & in_p, 
                                Point       & out_ptClosest, 
                                double      & out_t_on_seg, 
                                double      & out_d_to_seg,
                                const WINbool       out_bRes) ;
};

#ifndef RN_EPSILON
#define RN_EPSILON 1e-15
#endif

template<class Point, class Vector>
inline
WINbool 
RNLine_segTC<Point, Vector>::FindPtOnSeg( const Point & m_pt1,
                                          const Point & m_pt2,
                                          const Point & in_p, 
                                          Point       & out_ptClosest, 
                                          double      & out_t_on_seg, 
                                          double      & out_d_to_seg,
                                          const WINbool  in_bRes) 
{
    WINbool out_bRes = in_bRes;

    double len = Length( m_pt1 - m_pt2 );
    
    if ( RNIsZero(len) ) {
        if ( in_p == m_pt1 ) out_t_on_seg = 0;
        else                 out_t_on_seg = -1; /// will be clipped to 0
    } else {
        out_t_on_seg = out_t_on_seg / len;
    }
    
    if (out_t_on_seg < RN_EPSILON) {
        out_t_on_seg = 0.0;
        out_bRes = FALSE;
    }
    
    if (out_t_on_seg > 1.0 + RN_EPSILON) {
        out_t_on_seg = 1.0;
        out_bRes = FALSE;
    }
    
    out_ptClosest = m_pt1 + (m_pt2 - m_pt1) * (out_t_on_seg);
    out_d_to_seg = Length(out_ptClosest - in_p);
    
    return out_bRes;
}

