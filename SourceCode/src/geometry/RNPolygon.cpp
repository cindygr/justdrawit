
/* Copyright 1991, Brown Computer Graphics Group.  All Rights Reserved. */

/* -------------------------------------------------------------------------
   Manipulations of a point in euclidean space
   ------------------------------------------------------------------------- */

#include "StdAfx.H"


#include <utils/Rn_Polygon.H>


/* -------------------------- protected Routines --------------------------- */

/* -------------------------- Private Routines ----------------------------- */

/* -------------------------- public routines  ----------------------------- */

/* -------------------------------------------------------------------------
 * DESCR   :	See if a point lies on the polygon\'s boundary
 * ------------------------------------------------------------------------- */

Array<short> m_abInside_edge, m_abOutside_edge; // for closest_pt

template<class Point, class Vector, class Line_seg>
class RNPolygonTC {
public:
    static WINbool IsBoundary(const Point &in_pt,
                           const Array<Point> &in_apts);
    static WINbool Compare(const Array<Point> &in_apts1,
                        const Array<Point> &in_apts2);
    static Point Closest_pt(const Array<Point> &in_apts,
                            const Point &in_pt);
    
    static WINbool Inside(const Array<Point> &in_apts,
                       const Point &in_pt) ;
    
    static Point Project(const Array<Point> &in_apts,
                         const Point &in_pt) ;

    static R3Vec Normal(const Array<Point> &in_apts) ;
};

template<class Point, class Vector, class Line_seg>
inline
R3Vec RNPolygonTC<Point, Vector, Line_seg>::Normal(const Array<Point> &in_apts)
{
    R3Vec v1, v2, n;

    RNZero(v1);
    RNZero(v2);
    RNZero(n);
   
   if (in_apts.num() == 0) return n;

   for (int i = 0; i < in_apts.num(); i++) {
      Vector vec1 = UnitSafe((in_apts.wrap(i) - in_apts.wrap(i-1)));
      Vector vec2 = UnitSafe((in_apts.wrap(i+1) - in_apts.wrap(i)));
      for (int j = 0; j < vec1.Dim(); j++) {
         v1[j] = vec1[j];
         v2[j] = vec2[j];
      }
      n += Cross(v1, v2);
   }
   
   return UnitSafe(n);
}


// project the point onto the plane
template<class Point, class Vector, class Line_seg>
inline
Point RNPolygonTC<Point, Vector, Line_seg>::Project(const Array<Point> &in_apts,
                                                  const Point &in_pt) 
{
    R3Pt pt(0,0,0), polypt(0,0,0);
    
    int i;
    for (i = 0; i < in_pt.Dim(); i++) {
        pt[i] = in_pt[i];
        polypt[i] = in_apts[0][i];
    }
    
    R3Vec norm = Normal( in_apts );
    
    /* The shortest distance between p and this face will be along the
       direction of the face normal.  So find the normal projection of p onto
       the plane of this face. */
    
    R3Vec to_p = pt - polypt;
    R3Pt p_proj = pt - norm * Dot( to_p, norm );
    
    Point out_pt;
    for (i = 0; i < in_pt.Dim(); i++)
        out_pt[i] = p_proj[i];
    
    return out_pt;
}

// Sets m_bInside_edge for closest_pt
template<class Point, class Vector, class Line_seg>
inline
WINbool RNPolygonTC<Point, Vector, Line_seg>::Inside(const Array<Point> &in_apts,
                                        const Point &in_pt) 
{
    double dot;
    Vector e, v;
    R3Vec e3, v3, normal(0,0,1), cross;
    
    for (int iCorn = 0; iCorn < in_apts.num(); iCorn++) {
        e = in_apts.wrap(iCorn + 1) - in_apts.wrap(iCorn + 0);
        v = in_apts.wrap(iCorn + 2) - in_apts.wrap(iCorn + 1);
        
        const double dDot = Dot( UnitSafe( e ), UnitSafe( v ) );
        if ( RNApproxEqual( fabs( dDot ), 1.0, 1e-6 ) )
            continue;

        e3 = R3Vec (e[0], e[1], (e.Dim() == 2 ) ? 0 : e[2]);
        v3 = R3Vec (v[0], v[1], (e.Dim() == 2 ) ? 0 : e[2]);
        normal = Cross(e3, v3);
        if ( ! RNIsZero( LengthSq(normal ) ) ) {
            normal = Unit(normal);
            break;
        }
    }
    
    WINbool out_bRes = TRUE;
    
    m_abInside_edge.need(in_apts.num());
    for (int i = 0; i < in_apts.num(); i++) {
        e = in_apts.wrap(i+1) - in_apts.wrap(i);
        v = in_pt - in_apts.wrap(i);
        
        e3 = R3Vec (e[0], e[1], (e.Dim() == 2 ) ? 0 : e[2]);
        v3 = R3Vec (v[0], v[1], (e.Dim() == 2 ) ? 0 : e[2]);
        
        cross = Cross(e3, v3);
        dot = Dot(cross, normal);
        if (dot < -RNEpsilon_d) {
            m_abInside_edge[i] = FALSE;
            out_bRes = FALSE;
        } else {
            m_abInside_edge[i] = TRUE;
        }
    }
    return out_bRes;
}


/* -------------------------------------------------------------------------
 * DESCR   :	Return the closest point to the polygon
   Philip says:
   Returns the point on this face that is closest to p.
   If the projection of p onto the plane of this face lies inside the face,
   then closest_e and closest_v are NULL (even if the projection is actually
   on the "inside part" of an edge or vertex).
   If the projection of p lies outside this face, it is snapped to an edge or
   vertex of the face.  If this snapping occurs then closest_e or closest_v
   are set, otherwise they are NULL. 
   The return conventions for closest_e and closest_v could be more sensible.
   There should probably be another parameter whose return value indicates if
   the projection of p was inside the face or not.  Then this routine could
   set closest_e and closest_v in the case of the projection being inside and
   actually within some epsilon of an edge or vertex. 
 * ------------------------------------------------------------------------- */
template<class Point, class Vector, class Line_seg>
inline
Point 
RNPolygonTC<Point, Vector, Line_seg>::Closest_pt(const Array<Point> &in_apts, 
                                                 const Point &in_pt)
{
   int num_outside, i1, i2;
   double len, d;
   
   m_abOutside_edge.need(in_apts.num());
   m_abInside_edge.need(in_apts.num());

   Vector to_p_proj, e;
   Point p_proj = Project(in_apts, in_pt);
   Point p_edge;
   
   /* If p_proj is inside the face, return it.  If not, we have an array
      indicating whether p_proj is inside each edge. */
   // Also sets inside_edge
   if (Inside(in_apts, p_proj) == TRUE) {
      return p_proj;
   }

   /* Count the number of edges p_proj is outside. */
   num_outside = 0;
   for (int i = 0; i < in_apts.num(); i++) 
      if (m_abInside_edge[i] == FALSE)
        m_abOutside_edge[num_outside++] = i;

   /* If p_proj is outside only one edge, then the closest point is the
      projection of p_proj onto that edge. */
   if (num_outside == 1) {
      i1  = m_abOutside_edge[0];
      i2  = i1 + 1;
      e   = in_apts.wrap(i2) - in_apts.wrap(i1);
      len = Length(e);
      e = UnitSafe(e);

      to_p_proj = p_proj - in_apts.wrap(i1);
      d         = Dot(to_p_proj, e);
      
      if (d < 0.0) {
         p_edge         = in_apts.wrap(i1);
      }
      else if (d > len) {
         p_edge         = in_apts.wrap(i2);
      }
      else {
         p_edge         = in_apts.wrap(i1) + e * d;
      }
      return p_edge;
   }

   /* Here, p_proj is outside two edges.  In this case, the closest point is
      the vertex between those edges. */
   else {
      int i_close;
      if (m_abOutside_edge[1] == in_apts.num()-1 && m_abOutside_edge[0] == 0)
         i_close     = 0;
      else
         i_close     = m_abOutside_edge[1];
      return in_apts.wrap(i_close);
   }
}

/* -------------------------------------------------------------------------
 * DESCR   :	Two polygons are the same if their vertices are the same
 * 		(modulo a rotation)
 * ------------------------------------------------------------------------- */
template<class Point, class Vector, class Line_seg>
inline
WINbool RNPolygonTC<Point, Vector, Line_seg>::Compare(const Array<Point> &in_apts1,
                                                      const Array<Point> &in_apts2)
{
    if (in_apts1.num() != in_apts2.num())
        return FALSE;
   
    for (int i = 0; i < in_apts1.num(); i++) {
        WINbool matches = TRUE;
        for (int j = 0; j < in_apts2.num(); j++)
            if (ApproxEqual( in_apts1.wrap(i+j), in_apts2.wrap(j) ) == FALSE) {
                matches = FALSE;
                break; 
            }
        if (matches == TRUE)
            return TRUE;
    }
    return FALSE;
}


template<class Point, class Vector, class Line_seg>
inline
WINbool RNPolygonTC<Point, Vector, Line_seg>::IsBoundary(const Point &in_pt,
                                                         const Array<Point> &in_apts)
{
  int i;
  for (i = 0; i < in_apts.num(); i++)
    if (ApproxEqual(in_pt, in_apts[i]))
      return TRUE;
  
  Line_seg seg;
  for (i = 0; i < in_apts.num(); i++) {
    seg.P1() = in_apts.wrap(i);
    seg.P2() = in_apts.wrap(i+1);
    if (seg.IsPtOnSeg(in_pt)) 
      return TRUE;
  }
  return FALSE;
}

/* -------------------------------------------------------------------------
 * DESCR   :	See if a point lies on the polygon\'s boundary
 * ------------------------------------------------------------------------- */
WINbool R2Polygon::IsBoundary(const R2Pt &in_pt) const 
{
  return RNPolygonTC<R2Pt, R2Vec, R2Line_seg>::IsBoundary(in_pt, m_apts);
}

WINbool R3Polygon::IsBoundary(const R3Pt &in_pt) const 
{
  return RNPolygonTC<R3Pt, R3Vec, R3Line_seg>::IsBoundary(in_pt, m_apts);
}

WINbool R2Polygon::operator==( const R2Polygon &in_poly ) const
{
    return RNPolygonTC< R2Pt, R2Vec, R2Line_seg >::Compare( m_apts, in_poly.m_apts );
}

WINbool R3Polygon::operator==( const R3Polygon &in_poly ) const
{
    return RNPolygonTC< R3Pt, R3Vec, R3Line_seg >::Compare( m_apts, in_poly.m_apts );
}

WINbool R2Polygon::Inside(const R2Pt &in_pt) const
{
    return RNPolygonTC< R2Pt, R2Vec, R2Line_seg >::Inside( m_apts, in_pt );
}

WINbool R3Polygon::Inside(const R3Pt &in_pt) const
{
    return RNPolygonTC< R3Pt, R3Vec, R3Line_seg >::Inside( m_apts, in_pt );
}

R2Pt R2Polygon::Closest_pt(const R2Pt &in_pt) const
{
    
    if ( Inside( in_pt ) ) 
        return in_pt;

    return Lerp( ProjectBoundary( in_pt ), Centroid(), 1e-12 );
}

R2Pt R2Polygon::ProjectBoundary(const R2Pt &in_pt) const
{
    ASSERT( Num_pts() );

    Array<double> adDist;
    Array<R2Pt>   apt;
    double dDistToSeg = 0;
    double dDistOnSeg = 0;
    R2Pt pt;
    for (int i = 0; i < Num_pts(); i++) {
        Edge(i).FindPtOnSeg( in_pt, pt, dDistOnSeg, dDistToSeg );
        adDist += dDistToSeg;
        apt += pt;
    }
    apt.sort( adDist );

    return Lerp( apt[0], Centroid(), 1e-12 );
}

R3Pt R3Polygon::Closest_pt(const R3Pt &in_pt) const
{
    return RNPolygonTC< R3Pt, R3Vec, R3Line_seg >::Closest_pt( m_apts, in_pt );
}


R2Pt R2Polygon::Project(const R2Pt &in_pt) const
{
    return RNPolygonTC< R2Pt, R2Vec, R2Line_seg >::Project( m_apts, in_pt );
}

R3Pt R3Polygon::Project(const R3Pt &in_pt) const
{
    return RNPolygonTC< R3Pt, R3Vec, R3Line_seg >::Project( m_apts, in_pt );
}

R3Vec R2Polygon::Normal() const
{
    return RNPolygonTC< R2Pt, R2Vec, R2Line_seg >::Normal( m_apts );
}

R3Vec R3Polygon::Normal() const
{
    return RNPolygonTC< R3Pt, R3Vec, R3Line_seg >::Normal( m_apts );
}

