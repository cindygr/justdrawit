#include "StdAfx.H"
#include <utils/Rn_Sphere.H>


R2Pt R2Sphere::Closest( const R2Pt  & in_p1 ) const
{
    if ( ApproxEqual( in_p1, m_ptCenter ) )
        return m_ptCenter + R2Vec(0,1) * m_dRadius;

    const R2Vec vecAtOrig = in_p1 - m_ptCenter;

    return m_ptCenter + UnitSafe( vecAtOrig ) * (Radius() * (1.0 - RNEpsilon_d));
}

/// Generate evenly spaced points, number determined by io_apt.num
void R2Sphere::Points( Array<R2Pt> &io_apt, const double in_dTS, const double in_dTE ) const
{
    if ( io_apt.num() == 0 )
        io_apt.need( 10 );

    const double dDiv = (in_dTE - in_dTS) / io_apt.num();

    for (int i = 0; i < io_apt.num(); i++) {
        const R2Pt pt( cos( in_dTS + dDiv * i ), sin( in_dTS + dDiv * i ) );
        io_apt[i] = R2Pt( m_ptCenter[0] + m_dRadius * pt[0], m_ptCenter[1] + m_dRadius * pt[1] );
    }
}



/* -------------------------------------------------------------------------
 * DESCR   :	Intersect the line segment with the line
  >     x -> (2 c - 2 b m + 2 d m + 
 
                                     2
>         Sqrt[(-2 c + 2 b m - 2 d m)  - 
 
                    2    2    2            2    2               2
>           4 (1 + m ) (b  + c  - 2 b d + d  - r )]) / (2 (1 + m ))}}
 
 * ------------------------------------------------------------------------- */
int R2Sphere::Intersect(const R2Line   &in_line, 
                        R2Pt &out_p1, 
                        R2Pt &out_p2) const
{
    double b = in_line.Intercept();
    double m = in_line.Slope();
    double r = Radius();
    double c = Center()[0];
    double d = Center()[1];
    
   if (in_line.Vertical() == TRUE) {
       R2Pt e_pt;
       e_pt[0] = b;
       e_pt[1] = Center()[1];
       
       double len = Length(Center() - e_pt);
       if (len < Radius()) {
           out_p1[0] = b;
           out_p2[0] = b;
           out_p1[1] = Y_pos(out_p1[0]);
           out_p2[1] = Y_neg(out_p2[0]);
           return 2;
       } 
       if (RNIsZero(len - Radius())) {
           out_p1 = e_pt;
           return 1;
       } 
       return 0;
   }
   double radical = ((2 * (-c + m * (b - d))) * (2 * (-c + m * (b - d))) -
                     4 * (1 + m*m) * (b*b + c*c - 2 * b*d + d*d - r*r));
   
   if (radical < 0)
       return 0;
   
   if (RNIsZero(radical * 0.001)) {
       out_p1[0] = (2 * (c - m * (b - d))) / (2 * (1 + m * m));
       out_p1[1] = in_line.Y(out_p1[0]);
       return 1;
   }
   radical = sqrt(radical);
   
   out_p1[0] = (2 * (c - m * (b - d)) + radical) / (2 * (1 + m * m));
   out_p2[0] = (2 * (c - m * (b - d)) - radical) / (2 * (1 + m * m));
   out_p1[1] = in_line.Y(out_p1[0]);
   out_p2[1] = in_line.Y(out_p2[0]);
   
    if ( ApproxEqual( out_p1, out_p2 ) )
        return 1;
   return 2;
}

int
R2Sphere::Intersect(const R2Line_seg &in_seg,
                    R2Pt &out_p1,
                    R2Pt &out_p2) const
{
    out_p1 = in_seg.P1();
    out_p2 = in_seg.P2();

    const double dRad1 = Length(in_seg.P1() - Center() );
    const double dRad2 = Length(in_seg.P2() - Center() );

    // Both inside
    if ( dRad1 < Radius() && dRad2 < Radius() )
        return 0;

    // Ths prevents problems in the line intersect, in case the segment is zero length
    if ( ApproxEqual( in_seg.P1(), in_seg.P2() ) ) {
        return 0;
    }
    
    
    const R2Line line(in_seg.P1(), in_seg.P2());    
    const int num = Intersect(line, out_p1, out_p2);
    
    if (num == 0) {
        return 0;
    }
    const R2Vec vecSeg = in_seg.P2() - in_seg.P1();
    const R2Vec vecToPt1 = out_p1 - in_seg.P1();
    
    const bool bFound1 = ( Dot( vecSeg, vecToPt1 ) > 0 && LengthSq( vecToPt1 ) < LengthSq( vecSeg ) ) ? true : false;
    if ( num == 1 ) {
        if ( bFound1 ) 
            return 1;
        else           
            return 0;
    }
    
    const R2Vec vecToPt2 = out_p2 - in_seg.P1();
    const bool bFound2 = ( Dot( vecSeg, vecToPt2 ) > 0 && LengthSq( vecToPt2 ) < LengthSq( vecSeg ) ) ? true : false;
    if ( bFound1 && bFound2 )
        return 2;
    
    if ( bFound1 && !bFound2 ) {
        return 1;
    }

    if ( !bFound1 && bFound2 ) {
        out_p1 = out_p2;
        return 1;
    }
    
    return 0;
}

int R2Sphere::Intersect( const R2Sphere  & in_circ, 
                         R2Pt             & out_p1, 
                         R2Pt             & out_p2) const
{
    const double dLenCenters = Length( in_circ.Center() - Center() );
    if ( RNIsZero( dLenCenters, 1e-16 ) )
        return 0;
    
    if ( RNIsZero( Radius() ) ) {
        if ( in_circ.On( Center() ) ) {
            out_p1 = Center();
            return 1;
        }
    }
    if ( RNIsZero( in_circ.Radius() ) ) {
        if ( On( in_circ.Center() ) ) {
            out_p1 = in_circ.Center();
            return 1;
        }
    }
    if ( in_circ.Radius() + Radius() < dLenCenters ) {
        return 0;
    }
    const double dR0 = Radius();
    const double dR1 = in_circ.Radius();
    const R2Pt &pt0 = Center();
    const R2Pt &pt1 = in_circ.Center();

    if ( dLenCenters < fabs( dR0 - dR1 ) ) {
        return 0;
    }
    const double dRSq0 = pow( dR0, 2 );
    const double dRSq1 = pow( dR1, 2 );
    const double dLSq = pow( dLenCenters, 2 );
    
    // Actual intersection
    const double dMidPtLen1 = ( dRSq0 - dRSq1 + dLSq ) / (2.0 * dLenCenters);
    //const double dMidPtLen2 = ( dRSq1 - dRSq0 + dLSq ) / (2.0 * dLenCenters);
    //ASSERT( RNApproxEqual( dMidPtLen1 + dMidPtLen2, dLenCenters, 1e-6 ) );
    const double dH1 = sqrt( dRSq0 - pow(dMidPtLen1, 2) );
    //const double dH2 = sqrt( dRSq1 - pow(dMidPtLen2, 2) );
    
    const R2Pt ptMid1 = Lerp( pt0, pt1, (dMidPtLen1 / dLenCenters) );
    //const R2Pt ptMid2 = Lerp( pt1, pt0, (dMidPtLen2 / dLenCenters) );
    
    const R2Vec vecMid = pt1 - pt0;
    const R2Vec vPerpMid = R2Vec( vecMid[1], -vecMid[0] );
    out_p1 = ptMid1 + vPerpMid * (dH1 / dLenCenters);
    out_p2 = ptMid1 - vPerpMid * (dH1 / dLenCenters);
    
    ASSERT( RNIsZero( LengthSq( out_p1 - Center() ) - pow( Radius(), 2 ), 1e-6 ) );
    ASSERT( RNIsZero( LengthSq( out_p2 - Center() ) - pow( Radius(), 2 ), 1e-6 ) );
    ASSERT( RNIsZero( LengthSq( out_p1 - in_circ.Center() ) - pow( in_circ.Radius(), 2 ), 1e-6 ) );
    ASSERT( RNIsZero( LengthSq( out_p2 - in_circ.Center() ) - pow( in_circ.Radius(), 2 ), 1e-6 ) );
    
    return 2;
}


R2Polygon
R2Sphere::Intersect(const R2Polygon &in_poly ) const
{
    Array<R2Pt> apt;
    R2Pt ptOut1, ptOut2, ptCenter(0,0);
    for ( int i = 0; i < in_poly.Num_pts(); i++ ) {
        if ( Inside( in_poly[i] ) ) {
            apt += in_poly[i];
        }
        const R2Line_seg seg( in_poly[i], in_poly.PtWrap(i+1) );
        
        const int iN = Intersect( seg, ptOut1, ptOut2 );
        if ( iN == 1 ) {
            apt += ptOut1;
        }
        if ( iN == 2 ) {
            apt += ptOut1;
            apt += ptOut2;
        }
    }
    R2Polygon polyRet( apt.num() );

    for ( FORINT i = 0; i < apt.num(); i++ )
        polyRet[i] = apt[i];

    return polyRet;
    
}


WINbool
R2Sphere::Overlaps(const R2Polygon &in_poly ) const
{
    R2Pt p1, p2;
    if ( in_poly.Num_pts() == 0 ) {
        return FALSE;
    } else if ( in_poly.Num_pts() == 1 ) {
        return Inside( in_poly[0] );
    } else if ( in_poly.Num_pts() == 2 ) {
        return Intersect( R2Line_seg( in_poly[0], in_poly[1] ), p1, p2 ) == 0 ? FALSE : TRUE;
    }

    for ( int i = 0; i < in_poly.Num_pts(); i++ ) {
        if ( Inside( in_poly[i] ) )
            return TRUE;
    }

    for ( FORINT i = 0; i < in_poly.Num_pts(); i++ ) {
        const R2Line_seg seg = in_poly.Edge( i );
        if ( Intersect( seg, p1, p2 ) )
            return TRUE;
    }

    if ( in_poly.Inside( Center() ) )
        return TRUE;

    return FALSE;
    
}

/* -------------------------------------------------------------------------
 * DESCR   :	The positive/negative x given y

 >> y given x:
                         2       2    2    2            2
           2 d + Sqrt[4 d  - 4 (c  + d  - r  - 2 c x + x )]
>    {y -> ------------------------------------------------}}
                                  2

 * ------------------------------------------------------------------------- */
double R2Sphere::X_pos(double in_dY)
const
{
   const double c = Center()[0];
   const double d = Center()[1];
   const double r = Radius();

   return (2 * c + sqrt(4 * c*c - 
                        4 * (d*d + c*c - r*r - 2*d*in_dY + in_dY*in_dY))) / 2;
}

double R2Sphere::X_neg( double in_dY)
const
{
   const double c = Center()[0];
   const double d = Center()[1];
   const double r = Radius();

   return (2 * c - sqrt(4 * c*c - 4*(d*d + c*c - r*r - 2*d*in_dY + in_dY*in_dY))) / 2;
}

double R2Sphere::Y_pos(double in_dX)
const
{
   const double c = Center()[0];
   const double d = Center()[1];
   const double r = Radius();

   return (2 * d + sqrt(4 * d*d - 4*(c*c + d*d - r*r - 2*c*in_dX + in_dX*in_dX))) / 2;
}

double R2Sphere::Y_neg(double in_dX)
const
{
   const double c = Center()[0];
   const double d = Center()[1];
   const double r = Radius();

   return (2 * d - sqrt(4 * d*d - 4*(c*c + d*d - r*r - 2*c*in_dX + in_dX*in_dX))) / 2;
}

R2Sphere::R2Sphere(const R2Pt &in_ptP1, const R2Pt &in_ptP2, const R2Pt &in_ptP3 )
{
    const double dA = in_ptP2[0] - in_ptP1[0];
    const double dB = in_ptP2[1] - in_ptP1[1];
    const double dC = in_ptP3[0] - in_ptP1[0];
    const double dD = in_ptP3[1] - in_ptP1[1];
    const double dE = dA * ( in_ptP1[0] + in_ptP2[0] ) + dB * ( in_ptP1[1] + in_ptP2[1] ); 
    const double dF = dC * ( in_ptP1[0] + in_ptP3[0] ) + dD * ( in_ptP1[1] + in_ptP3[1] ); 
    const double dG = 2.0 * ( dA * ( in_ptP3[1] - in_ptP2[1] ) - dB * ( in_ptP3[0] - in_ptP2[0] ) ); 
    
    if ( RNIsZero( dG ) ) {
        // Colinear;
        m_ptCenter = in_ptP1;
        m_dRadius = 0.0;
        return;
    } 
    m_ptCenter = R2Pt( ( dD * dE - dB * dF ) / dG, ( dA * dF - dC * dE ) / dG );
    const double dRadius1 = Length( in_ptP1 - m_ptCenter );
    const double dRadius2 = Length( in_ptP2 - m_ptCenter );
    const double dRadius3 = Length( in_ptP3 - m_ptCenter );
    m_dRadius = (dRadius1 + dRadius2 + dRadius3) / 3.0;

    ASSERT( RNIsZero( m_dRadius - Length(in_ptP1 - m_ptCenter), 1e-12 ) );
    ASSERT( RNIsZero( m_dRadius - Length(in_ptP2 - m_ptCenter), 1e-12 ) );
    ASSERT( RNIsZero( m_dRadius - Length(in_ptP3 - m_ptCenter), 1e-12 ) );
}

void R2Sphere::Print() const 
{
    cout << "Sphere dim " << Dim() << " radius " << Radius() << "  ";
    cout << "Center: " << Center() << "\n";

}

void R2Sphere::Write(ofstream &out) const 
{
    out << Center() << " " << Radius() << "\n";
}

WINbool R2Sphere::Read(ifstream &in) 
{
    WINbool bRes = Center().Read( in );
    in >> Radius();
    return (bRes == TRUE && in.good()) ? TRUE : FALSE;
}

void R2Sphere::WriteBinary(ofstream &out) const 
{
    Center().WriteBinary(out);
    out.write( ( const char * ) &m_dRadius, sizeof( double ) );

}

void R2Sphere::ReadBinary(ifstream &in) 
{
    Center().ReadBinary(in);
    in.read( ( char * ) &m_dRadius, sizeof( double ) );
}


WINbool R2Sphere::Test()
{
   R2Sphere s1(1.0), s2(R2Pt(1,1), 2), s3(s1);

   VERIFY(s1 == s3);
   
   VERIFY(s1.X_pos(0.0) == 1.0);
   VERIFY(s1.Y_pos(0.2) == sqrt(1.0 - 0.2 * 0.2));
   VERIFY(s1.X_pos(0.1) == - s1.X_neg(0.1));
   VERIFY(s1.Y_pos(0.1) == - s1.Y_neg(0.1));

   VERIFY(s1.Area() == M_PI);
   VERIFY(s1.SurfaceArea() == 2.0 * M_PI);
   
   s1 = s2;
   VERIFY(s1 == s2);
   
   R2Line line(R2Pt(1,1), R2Vec(0.5, 0.5));
   R2Line line2(R2Pt(1,3), R2Vec(1,0));
   R2Line linev1(1.0), linev2(3.0);
   R2Line line3(R2Pt(1,3.5), R2Vec(1,0.001));
   R2Pt p1, p2;
   VERIFY(s1.Intersect(line, p1, p2) == 2);
   VERIFY(RNApproxEqual(p1[0], 1.0 + 2.0 * sqrt(0.5)));
   VERIFY(RNApproxEqual(p1[1], 1.0 + 2.0 * sqrt(0.5)));
   VERIFY(RNApproxEqual(p2[0], 1.0 - 2.0 * sqrt(0.5)));
   VERIFY(RNApproxEqual(p2[1], 1.0 - 2.0 * sqrt(0.5)));

   VERIFY(s1.Intersect(linev1, p1, p2) == 2);
   VERIFY(RNApproxEqual(p1[0], 1.0));
   VERIFY(RNApproxEqual(p1[1], 3.0));
   VERIFY(RNApproxEqual(p2[0], 1.0));
   VERIFY(RNApproxEqual(p2[1], -1.0));

   VERIFY(s1.Intersect(linev2, p1, p2) == 1);
   VERIFY(RNApproxEqual(p1[0], 3.0));
   VERIFY(RNApproxEqual(p1[1], 1.0));

   VERIFY(s1.Intersect(line2, p1, p2) == 1);
   VERIFY(ApproxEqual(line2.Pt(), p1));

   VERIFY(s1.Intersect(line3, p1, p2) == 0);


   R2Line_seg seg(R2Pt(-2,-2), R2Pt(4, 4));
   R2Line_seg seg2(R2Pt(0.9,3), R2Pt(1.1,3));
   R2Line_seg seg3(R2Pt(-0.5,1), R2Pt(0.5,1));
   VERIFY(s1.Intersect(seg, p1, p2) == 2);
   VERIFY(RNApproxEqual(p1[0], 1.0 + 2.0 * sqrt(0.5)));
   VERIFY(RNApproxEqual(p1[1], 1.0 + 2.0 * sqrt(0.5)));
   VERIFY(RNApproxEqual(p2[0], 1.0 - 2.0 * sqrt(0.5)));
   VERIFY(RNApproxEqual(p2[1], 1.0 - 2.0 * sqrt(0.5)));

   VERIFY(s1.Intersect(seg2, p1, p2) == 1);
   VERIFY(ApproxEqual(R2Pt(1,3), p1));

   VERIFY(s1.Intersect(seg3, p1, p2) == 0);

   s1 = R2Sphere(1.0);
   s3 = R2Sphere(3.0);
   s2 = R2Sphere(R2Pt(0.1, 0), 2.0);

   VERIFY(s1.Dim() == 2);
   VERIFY(s1.Radius() == 1.0);
   VERIFY(s1.Center() == R2Pt(0,0));

   R2Pt pt(0.1, 0.0);
   VERIFY(s1.Inside(pt) == TRUE);
   VERIFY(s2.Inside(pt) == TRUE);
   pt[0] = 1.0;
   pt[1] = 0;
   pt[2] = 0;
   VERIFY(s1.On(pt) == TRUE);
   VERIFY(s2.On(pt) == FALSE);
   
   pt[0] = 3.0;
   VERIFY(s1.Inside(pt) == FALSE);

   s3 = s2;
   VERIFY(s3 == s2);
   VERIFY(!(s3 == s1));

   s3.Print();
   ofstream out("RNtest.txt", ios::out);
   s3.Write(out);
   out.close();
   ifstream in("RNtest.txt", ios::in);
   s1.Read(in);
   in.close();
   VERIFY(s3 == s1);

    R2Sphere circ( R2Pt(0,0), 0.5 );
    
    // Segment entirely inside
    seg = R2Line_seg( R2Pt(0,0), R2Pt( 0.2, 0.2 ) );
    ASSERT( circ.Intersect(seg, p1, p2 ) == 0);
    
    // Zero length segment on the boundary
    seg = R2Line_seg( R2Pt(0.5,0), R2Pt( 0.5, 0 ) );
    ASSERT( circ.Intersect(seg, p1, p2 ) == 0);
    
    // Zero length segment inside
    seg = R2Line_seg( R2Pt(0.25,0), R2Pt( 0.25, 0 ) );
    ASSERT( circ.Intersect(seg, p1, p2 ) == 0);
    
    // Zero length segment outside
    seg = R2Line_seg( R2Pt(0.75,0), R2Pt( 0.75, 0 ) );
    ASSERT( circ.Intersect(seg, p1, p2 ) == 0);
    
    // Segment touching edge from inside
    seg = R2Line_seg( R2Pt(0.0,0), R2Pt( 0.5, 0 ) );
    ASSERT( circ.Intersect(seg, p1, p2 ) == 0);
    
    // Segment touching edge from outside
    seg = R2Line_seg( R2Pt(1.0,0), R2Pt( 0.5, 0 ) );
    ASSERT( circ.Intersect(seg, p1, p2 ) == 0);
    
    // Line tangent to edge, segment touching (vertical)
    seg = R2Line_seg( R2Pt(0.5,-1), R2Pt( 0.5, 1 ) );
    ASSERT( circ.Intersect(seg, p1, p2 ) == 1);
    ASSERT( ApproxEqual( p1, R2Pt(0.5, 0.0) ) );
    
    // Line tangent to edge, segment touching (horizontal)
    seg = R2Line_seg( R2Pt(-1,0.5), R2Pt( 1, 0.5 ) );
    ASSERT( circ.Intersect(seg, p1, p2 ) == 1);
    ASSERT( ApproxEqual( p1, R2Pt(0.0, 0.5) ) );
    
    // Line tangent to edge, segment touching (angle)
    const R2Pt ptOnCirc( cos(0.3) * 0.5, sin(0.3) * 0.5);
    const R2Vec vecTang( -sin(0.3), cos(0.3) );
    ASSERT( circ.On(ptOnCirc) );
    
    seg = R2Line_seg( ptOnCirc + vecTang, ptOnCirc - vecTang );
    ASSERT( circ.Intersect(seg, p1, p2 ) == 1);
    ASSERT( ApproxEqual( p1, ptOnCirc ) );
    
    // Line tangent to edge, segment NOT touching (vertical)
    seg = R2Line_seg( R2Pt(0.5,2), R2Pt( 0.5, 1 ) );
    ASSERT( circ.Intersect(seg, p1, p2 ) == 0);
    
    // Line tangent to edge, segment NOT touching (horizontal)
    seg = R2Line_seg( R2Pt(2,0.5), R2Pt( 1, 0.5 ) );
    ASSERT( circ.Intersect(seg, p1, p2 ) == 0);
    
    // Line tangent to edge, segment NOT touching (angle)
    seg = R2Line_seg( ptOnCirc + vecTang, ptOnCirc + vecTang * 2.0 );
    ASSERT( circ.Intersect(seg, p1, p2 ) == 0);
    
    // Line does not cross circle
    seg = R2Line_seg( R2Pt(1,0), R2Pt( 0, 1 ) );
    ASSERT( circ.Intersect(seg, p1, p2 ) == 0);
    
    // Line crosses circle, segment outside
    seg = R2Line_seg( R2Pt(-1.0,0), R2Pt( -0.7, 0.01 ) );
    ASSERT( circ.Intersect(seg, p1, p2 ) == 0);
    
    seg = R2Line_seg( R2Pt(1.0,0), R2Pt( 0.7, 0.01 ) );
    ASSERT( circ.Intersect(seg, p1, p2 ) == 0);
    
    // Line crosses circle, one end point touching
    seg = R2Line_seg( R2Pt(cos(1.3) * 0.5,sin(1.3) * 0.5), R2Pt( 0,0 ) );
    ASSERT( circ.Intersect(seg, p1, p2 ) == 0);
    seg = R2Line_seg( R2Pt( 0,0 ), R2Pt(cos(1.3) * 0.5,sin(1.3) * 0.5) );
    ASSERT( circ.Intersect(seg, p1, p2 ) == 0);
    
    // Line crosses circle, segment inside
    seg = R2Line_seg( R2Pt(0.3,0.1), R2Pt( -0.2, 0.01 ) );
    ASSERT( circ.Intersect(seg, p1, p2 ) == 0);
    
    // Line crosses circle, one intersection
    seg = R2Line_seg( R2Pt(0.01,0.002), R2Pt( 0.75, 0.9 ) );
    ASSERT( circ.Intersect(seg, p1, p2 ) == 1);
    ASSERT( circ.On(p1) );
    ASSERT( seg.IsPtOnSeg( p1 ) );
    
    seg = R2Line_seg( R2Pt(0.01,0.002), R2Pt( -0.75, -0.9 ) );
    ASSERT( circ.Intersect(seg, p1, p2 ) == 1);
    ASSERT( circ.On(p1) );
    ASSERT( seg.IsPtOnSeg( p1 ) );
    
    // Line crosses circle, two intersections
    seg = R2Line_seg( R2Pt( -0.7, -0.9 ), R2Pt( 0.75, 0.9 ) );
    ASSERT( circ.Intersect(seg, p1, p2 ) == 2);
    ASSERT( circ.On(p1) );
    ASSERT( circ.On(p2) );    
    ASSERT( !ApproxEqual( p1, p2 ) );
    ASSERT( seg.IsPtOnSeg( p1 ) );
    ASSERT( seg.IsPtOnSeg( p2 ) );
    return TRUE;
}
