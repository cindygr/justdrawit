#include "StdAfx.H"
#include <utils/Rn_Ellipse.H>

R3Matrix R3Ellipse::RotateToNormal() const
{
    const R3Matrix mat = R3Matrix::Rotation( 2, -m_dZRot ) * 
                         R3Matrix::Rotation( 1, -m_dYRot ) *
                         R3Matrix::Rotation( 0, -m_dXRot );
    return mat;
}

R3Matrix R3Ellipse::RotateFromNormal() const
{
    const R3Matrix mat = R3Matrix::Rotation( 0, m_dXRot ) * 
                         R3Matrix::Rotation( 1, m_dYRot ) *
                         R3Matrix::Rotation( 2, m_dZRot );

    return mat;
}

R4Matrix R3Ellipse::ToNormal() const
{
    const R4Matrix matRot = RotateToNormal();
    const R4Matrix mat = matRot *
        R4Matrix::Translation( R3Pt(0,0,0) - m_ptCenter );

    return mat;
}


R4Matrix R3Ellipse::FromNormal() const
{
    const R4Matrix matRot = RotateFromNormal();
    const R4Matrix mat =  R4Matrix::Translation( m_ptCenter - R3Pt(0,0,0) ) * 
                          matRot;

    return mat;
}



/// Generate evenly spaced points, number determined by io_apt.num
void R3Ellipse::Points( Array<R3Pt> &io_apt, const double in_dTAlongXAxis ) const
{
    if ( io_apt.num() == 0 )
        io_apt.need( 10 );

    const R4Matrix mat = FromNormal();
    const double dDiv = (2.0 * M_PI) / (io_apt.num() - 1.0);
    const double dPhi = M_PI * in_dTAlongXAxis - M_PI / 2.0;
    const double dSin = sin( dPhi );
    const double dCos = cos( dPhi );
    for (int i = 0; i < io_apt.num(); i++) {
        const R3Pt pt( m_dA * cos( dDiv * i ) * dCos, 
                       m_dB * sin( dDiv * i ) * dCos, 
                       m_dC * dSin );
        io_apt[i] = mat * pt;
    }
}

/// Generate evenly spaced points, number determined by io_apt.num
void R3Ellipse::PointsAndNormals( Array<R3Pt> &io_apt, 
                                  Array<R3Vec> &io_avec,
                                  const double in_dTAlongXAxis ) const
{
    if ( io_apt.num() == 0 )
        io_apt.need( 10 );

    io_avec.need( io_apt.num() );
    const R4Matrix mat = FromNormal();
    const R4Matrix matRot = RotateFromNormal();
    const double dDiv = (2.0 * M_PI) / (io_apt.num() - 1.0);
    const double dPhi = M_PI * in_dTAlongXAxis - M_PI / 2.0;
    const double dSin = sin( dPhi );
    const double dCos = cos( dPhi );
    for (int i = 0; i < io_apt.num(); i++) {
        const R3Pt pt( m_dA * cos( dDiv * i ) * dCos, 
                       m_dB * sin( dDiv * i ) * dCos, 
                       m_dC * dSin );
        const R3Vec vec( 2.0 * pt[0] / (m_dA * m_dA),
                         2.0 * pt[1] / (m_dB * m_dB),
                         2.0 * pt[2] / (m_dC * m_dC) ); 
    
        io_apt[i] = mat * pt;
        io_avec[i] = matRot * vec;
    }
}

R3Vec R3Ellipse::XAxis() const
{
    return RotateFromNormal() * R3Vec(m_dA, 0, 0);
}

R3Vec R3Ellipse::YAxis() const
{
    return RotateFromNormal() * R3Vec( 0, m_dB, 0);
}

R3Vec R3Ellipse::ZAxis() const
{
    return RotateFromNormal() * R3Vec(0, 0, m_dC);
}

void R3Ellipse::Print() const 
{
    cout << "Ellipse dim " << Dim() << " radius " << m_dA << " " << m_dB << " " << m_dC <<
            " theta " << m_dXRot << " " << m_dYRot << " " << m_dZRot << "  ";
    cout << "Center: " << Center() << "\n";

}

void R3Ellipse::Write(ofstream &out) const 
{
    out << Center() << " " << m_dA << " " << m_dB << " " << " " << m_dC << " " << m_dXRot << " " << m_dYRot << " " << m_dZRot << "\n";
}

WINbool R3Ellipse::Read(ifstream &in) 
{
    WINbool bRes = Center().Read( in );
    in >> m_dA >>  m_dB >> m_dC >> m_dXRot >> m_dYRot >> m_dZRot;
    return (bRes == TRUE && in.good()) ? TRUE : FALSE;
}


WINbool R3Ellipse::Test()
{
    R3Ellipse e1;
    e1.Center() = R3Pt( 0.1, 0.5, 0.4 );
    e1.XRadius() = 2.0;
    e1.YRadius() = 1.0;
    e1.ZRadius() = 1.0;
    e1.XRotation() = M_PI / 4.0;

    const R4Matrix mat = e1.ToNormal();
    const R3Pt ptCenter = mat * e1.Center();
    ASSERT( ApproxEqual( ptCenter, R3Pt(0,0,0) ) );
    const R3Pt ptMajor = mat * (e1.Center() + R3Vec(0.0, 0.1, -0.1));
    ASSERT( ApproxEqual( ptMajor, R3Pt( 0.0, 0.1 * sqrt(2.0), 0.0) ) );
    const R3Pt ptMinor = mat * (e1.Center() + R3Vec( 0.0, 0.1, 0.1));
    ASSERT( ApproxEqual( ptMinor, R3Pt(0.0, 0.0, 0.1 * sqrt(2.0)) ) );

   R3Ellipse s1( R3Pt(1.0,1,1) ), s2(R3Pt(1,1,1)), s3(s1);

   VERIFY(s1 == s3);

   s1 = s2;
   VERIFY(s1 == s2);
    s2 = R3Ellipse(R3Pt(0.1, 0, 0));

   VERIFY(s1.Dim() == 3);
   VERIFY(s1.Center() == R3Pt(1,1,1));

   R3Pt pt(0.9, 1.0, 1.0);
   VERIFY(s1.Inside(pt) == TRUE);
   VERIFY(s2.Inside(pt) == FALSE);
   pt[0] = 1.1;
   pt[1] = 0.0;
   pt[2] = 0.0;
   VERIFY(s2.On(pt) == TRUE);
   VERIFY(s1.On(pt) == FALSE);
   
   pt[0] = 3.0;
   VERIFY(s1.Inside(pt) == FALSE);

   s3 = s2;
   VERIFY(s3 == s2);
   VERIFY(!(s3 == s1));

   s3.Print();
   ofstream out("RNtest.txt", ios::out);
   s3.Write(out);
   out.close();
   ifstream in("RNtest.txt", ios::in);
   s1.Read(in);
   in.close();
   VERIFY(s3 == s1);
   return TRUE;
}
