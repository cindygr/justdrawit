/* Copyright 1991, Brown Computer Graphics Group.  All Rights Reserved. */

/* -------------------------------------------------------------------------
   Manipulations of spheres in euclidean space
   ------------------------------------------------------------------------- */

#include "StdAfx.H"
#include <utils/Rn_Sphere.H>


/* -------------------------- protected Routines --------------------------- */

/* -------------------------- Private Routines ----------------------------- */

/* -------------------------- public routines  ----------------------------- */

/* -------------------------------------------------------------------------
 * DESCR   :	Intersect the tangent vector with the sphere, returning the
 * 		first point encountered
//template <class Point, class CoVector, class Coord>
//template <class Coord>
int 
RNsphere<Point3TC<double>, double>::
intersect(const RNtangent_vec<Point3TC<double>, CoVector3TC<double>, double> &in_tvec,
          Point &out_p1, Point &out_p2) const 
{
   Vector3TC<double> vec = UnitSafe(in_tvec.Dir_vec());

   Vector3TC<double> EO = vec.Base_pt() - Center();
   double v = Dot(EO, V);

   double disc = Radius() * Radius() - (Dot(EO, EO) - v * v);
   if (disc < 0)
     return 0;
   
   double d = sqrt(disc);
   out_p1 = vec.Base_pt() + vec * (v - d);
   if (Length(in_tvec.Dir_vec()) > (v - d + 2.0 * Radius())) {
      out_p2 = vec.Base_pt() + vec * (v-d + 2.0 * Radius());
      return 2;
   }
   return 1;
}

 * ------------------------------------------------------------------------- */

