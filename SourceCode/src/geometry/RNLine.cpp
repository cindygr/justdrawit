/* Copyright 1991, Brown Computer Graphics Group.  All Rights Reserved. */

/* -------------------------------------------------------------------------
   Manipulations of a point in euclidean space
   ------------------------------------------------------------------------- */

#include "StdAfx.H"

#include <utils/Rn_Line.H>

/* -------------------------- protected Routines --------------------------- */

/* -------------------------- Private Routines ----------------------------- */

/* -------------------------- public routines  ----------------------------- */


/* -------------------------------------------------------------------------
 * DESCR   :	Is the point on the line?
 * DESCR   :	Find the t value on the line closest to the point
 * DETAILS :    R2Pt lies on plane  P dot v + d = 0
 * ------------------------------------------------------------------------- */

template<class Point, class Vector>
class RNLineTC {
public:
static WINbool FindPtOnLine( const Point   & m_pt,
                             const Vector  & m_vec,
                             const Point   & in_pt,
                             Point         & out_ptClosest,
                             double        & out_t_on_line,
                             double        & out_d_to_line);
};

template<class Point, class Vector>
inline
WINbool RNLineTC<Point, Vector>::FindPtOnLine(const Point   & m_pt,
                                              const Vector  & m_vec,
                                              const Point   & in_pt,
                                              Point         & out_ptClosest,
                                              double        & out_t_on_line,
                                              double        & out_d_to_line)
{
    // Move start of pt-vector to origin, then project onto the vector

    out_t_on_line = Dot( in_pt - m_pt, m_vec );
   
    out_ptClosest = m_pt + m_vec * out_t_on_line;
    
    out_d_to_line = Length(out_ptClosest - in_pt);

    return RNIsZero(out_d_to_line);
}

WINbool R2Line::FindPtOnLine( const R2Pt  & in_pt,
                              R2Pt        & out_ptClosest,
                              double      & out_t_on_line,
                              double      & out_d_to_line) const
{
    return RNLineTC<R2Pt, R2Vec>::FindPtOnLine(m_pt, m_vec,
                                               in_pt,
                                               out_ptClosest,
                                               out_t_on_line,
                                               out_d_to_line);
}

WINbool R3Line::FindPtOnLine( const R3Pt  & in_pt,
                              R3Pt        & out_ptClosest,
                              double      & out_t_on_line,
                              double      & out_d_to_line) const
{
    return RNLineTC<R3Pt, R3Vec>::FindPtOnLine(m_pt, m_vec,
                                               in_pt,
                                               out_ptClosest,
                                               out_t_on_line,
                                               out_d_to_line);
}
