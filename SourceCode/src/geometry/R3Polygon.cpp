#include "StdAfx.H"
#include <utils/Rn_Polygon.H>
#include <utils/R3_Plane.H>


///
WINbool R3Polygon::IntersectSegment( const R3Line_seg &in_seg, R3Pt &out_pt ) const
{
    double dS, dT;
    if ( RNIsZero( Length( Normal() ) ) ) {
        // We have a point or a line segment
        R3Line_seg seg( m_apts[0], m_apts[1] );
        for ( int i = 0; i < m_apts.num(); i++ ) {
            for ( int j = i+1; j < m_apts.num(); j++ ) {
                if ( Length( m_apts[j] - m_apts[i] ) > seg.Length() ) {
                    seg = R3Line_seg( m_apts[j], m_apts[i] );
                }
            }
        }
        const WINbool bRet = seg.Intersect( in_seg, out_pt, dS, dT );
        if ( bRet == true ) {
            cout << out_pt << dS << " " << dT << "\n";
            cout << seg(dS) << "\n";
            cout << in_seg(dT) << "\n";
        }
        return bRet;
    }
        
    
    const R3Plane plane( m_apts[0], Normal() );

    if ( plane.Intersect( in_seg, dT, out_pt ) == FALSE )
        return FALSE;

    if ( dT < 0.0 || dT > 1.0 )
        return FALSE;

    int iZ = 0;
    for ( int i = 0; i < 3; i++ )
        if ( fabs( plane.Normal()[i] ) > fabs( plane.Normal()[iZ] ) )
            iZ = i;

    const int iX = (iZ+1) % 3, iY = (iZ+2) % 3;

    R2Polygon poly( Num_pts() );
    for ( FORINT i = 0; i < poly.Num_pts(); i++ ) {
        poly[i][0] = (*this)[i][iX];
        poly[i][1] = (*this)[i][iY];
    }
    const R2Pt pt( out_pt[iX], out_pt[iY] );

    const WINbool bRes = poly.InsideQuick( pt );

    return bRes;
}

/* -------------------------------------------------------------------------
 * DESCR   :	Return the centroid of the polygon
 * ------------------------------------------------------------------------- */
R3Pt R3Polygon::Centroid() const
{
    R3Pt center(0,0,0);
    
    for (int i = 0; i < Num_pts(); i++)
        for (int d = 0; d < center.Dim(); d++)
            center[d] += (*this)[i][d];
    
    if (Num_pts() > 0) {
        for (int d = 0; d < center.Dim(); d++)
            center[d] = center[d] / (double) Num_pts();
    } else {
        for (int d = 0; d < center.Dim(); d++) center[d] = 1e30f;
    }

    return center;
}


/* -------------------------------------------------------------------------
 * DESCR   :	Print this 
 * ------------------------------------------------------------------------- */
void R3Polygon::Print() const
{
    cout << "Polygon " << Dim() << " with " << Num_pts() << " vertices\n";
    for (int i = 0; i < Num_pts(); i++) {
        cout << (*this)[i] << " ";
    }
    cout << "\n";
}



/* -------------------------------------------------------------------------
 * DESCR   :	read & write
 * ------------------------------------------------------------------------- */
void R3Polygon::WriteBinary(ofstream &out) const 
{
    const int iN = Num_pts();
    out.write( ( const char * ) &iN, sizeof( int ) );

    for (int i = 0; i < Num_pts(); i++) {
        m_apts[i].WriteBinary( out );
    }
}

void R3Polygon::Write(ofstream &out) const 
{
    out <<  " " << Num_pts() << "\n";
    for (int i = 0; i < Num_pts(); i++) {
        out << (*this)[i] << " ";
    }
    out << "\n";
}

WINbool R3Polygon::Read(ifstream &in)
{
    int n;
    in >> n;
    m_apts.need(n);
    
    WINbool bRes = (n >= 0) ? TRUE : FALSE;
    for (int i = 0; i < Num_pts(); i++) {
        bRes = ( bRes == TRUE && m_apts[i].Read( in ) == TRUE ) ? TRUE : FALSE;
    }
    return bRes;
}

WINbool R3Polygon::ReadBinary(ifstream &in)
{
    int iN;
    in.read( ( char * ) &iN, sizeof( int ) );
    m_apts.need(iN);
    
    for (int i = 0; i < Num_pts(); i++) {
        m_apts[i].ReadBinary(in);
    }
    return in.good();
}

double R3Polygon::LBDistSq( const R3Pt &in_pt ) const
{
	double d = 0.0;

    R3Pt ptMin(1e30, 1e30, 1e30), ptMax( -1e30, -1e30, -1e30 );
    for (int i = 0; i < m_apts.num(); i++ ) {
        for (int iD = 0; iD < 3; iD++ ) {
            ptMin[iD] = WINmin( ptMin[iD], m_apts[i][iD] );
            ptMax[iD] = WINmax( ptMax[iD], m_apts[i][iD] );
        }
    }
      
    const R3Vec vecMax = in_pt - ptMax;
    const R3Vec vecMin = ptMin - in_pt;

    for ( FORINT i = 0; i < 3; i++ ) {
        d = WINmax( d, vecMax[i] );
        d = WINmax( d, vecMin[i] );
    }
    return d * d;
}

double R3Polygon::Area() const
{
    if ( Num_pts() == 3 ) {
        const R3Vec vecCross = Cross( m_apts[1] - m_apts[0], m_apts[2] - m_apts[0] );

        return Length( vecCross ) * 0.5;
    } else if ( Num_pts() > 3 ) {
        const R3Pt ptCentroid = Centroid();
        double dArea = 0.0;
        for ( int iS = 0; iS < Num_pts(); iS++ ) {
            const R3Vec vecCross = Cross( m_apts[iS] - ptCentroid, PtWrap(iS+1) - ptCentroid );

            dArea += Length( vecCross ) * 0.5;
        }
        return dArea;
    }
    return 0.0;
}

// two bad cases:
// - v2==0 or v3==0 (two points of triangle are same) -> ok
// - v23*v23==v22*v33 (area zero but neither v2 nor v3 zero) -> ouch
WINbool R3Polygon::ProjectTri( const R3Pt  & in_pt, 
                               double      & out_dDistSq, 
                               R3Pt        & out_ptPlane, 
                               R3Pt        & out_ptTri, 
                               Array<double> & out_adBary ) const
{
    WINbool bRet = FALSE;
    out_ptTri = R3Pt(0,0,0);

	const double p1x=m_apts[0][0], p1y=m_apts[0][1], p1z=m_apts[0][2];
	const double v2x=m_apts[1][0]-p1x, v2y=m_apts[1][1]-p1y, v2z=m_apts[1][2]-p1z;
	const double v3x=m_apts[2][0]-p1x, v3y=m_apts[2][1]-p1y, v3z=m_apts[2][2]-p1z;
	const double vpp1x=in_pt[0]-p1x, vpp1y=in_pt[1]-p1y, vpp1z=in_pt[2]-p1z;
	double v22=v2x*v2x+v2y*v2y+v2z*v2z;
	double v33=v3x*v3x+v3y*v3y+v3z*v3z;
	const double v23=v2x*v3x+v2y*v3y+v2z*v3z;
	const double v2pp1=v2x*vpp1x+v2y*vpp1y+v2z*vpp1z;
	const double v3pp1=v3x*vpp1x+v3y*vpp1y+v3z*vpp1z;
	if ( RNIsZero(v22) ) v22=1.0;	// recover if v2==0
	if ( RNIsZero(v33) ) v33=1.0;	// recover if v3==0
	double a2,a3;
	const double denom=(v33-v23*v23/v22);
	if ( RNIsZero(denom) ) {
		a2=a3=1.0/3.0;	// recover if v23*v23==v22*v33
	} else {
		a3=(v3pp1-v23/v22*v2pp1)/denom;
		a2=(v2pp1-a3*v23)/v22;
	}
    out_adBary.need(3);

	const double a1=1-a2-a3;
	out_adBary[0] = a1;
    out_adBary[1] = a2;
    out_adBary[2] = a3;

	// Point clp=interp(p1,p2,p3,a1,a2);
	out_ptPlane[0] = p1x+a2*v2x+a3*v3x;
	out_ptPlane[1] = p1y+a2*v2y+a3*v3y;
	out_ptPlane[2] = p1z+a2*v2z+a3*v3z;

    Array<double> adBary(3);
	if (a1<0 || a2<0 || a3<0) {
	    // projection lies outside triangle, so more work is needed.
	    double mind2 = 1e30;
	    for (int i = 0; i < 3; i++) {
		    if ( out_adBary[(i+2)%3] >= 0 ) continue;
		    // project proj onto segment pf[(i+0)%3]--pf[(i+1)%3]
		    const R3Vec vvi = m_apts.wrap((i+1)%3) - m_apts[i];
		    const R3Vec vppi = out_ptPlane - m_apts[i];

		    double d12sq = LengthSq(vvi);
		    double don12 = Dot(vvi,vppi);
		    if (don12 <= 0) {
			    double d2 = LengthSq( m_apts[i] - out_ptPlane );
			    if ( d2 >= mind2) continue;
			    mind2 = d2; 
                adBary[i] = 1.0;
                adBary.wrap(i+1) = adBary.wrap(i+2) = 0;
		    } else if ( don12 >= d12sq ) {
			    double d2 = LengthSq(m_apts.wrap(i+1) - out_ptPlane);
			    if (d2 >= mind2) continue;
			    mind2 = d2; 
                adBary.wrap(i+1) = 1.0;
                adBary[i] = adBary.wrap(i+2) = 0;
		    } else {
			    double a = don12 / d12sq;
			    adBary[i]=1.0 - a; 
                adBary.wrap(i+1) = a; 
                adBary.wrap(i+2) = 0;
			    break;
		    }
	    }
        out_adBary = adBary;
        for ( int j = 0; j < 3; j++ )
            for ( int i = 0; i < 3; i++ )
                out_ptTri[j] += adBary[i] * m_apts[i][j];
    
    } else {
        bRet = TRUE;
        out_ptTri = out_ptPlane;
    }

    out_dDistSq = LengthSq( out_ptTri - in_pt );
    return bRet;
}

double R3Polygon::DistSq( const R3Pt &in_pt ) const
{
    Array<double> adBary;
    R3Pt ptPlane, ptTri;
    double dDistSq = 0;

    ProjectTri( in_pt, dDistSq, ptPlane, ptTri, adBary );

    return dDistSq;
}

WINbool R3Polygon::IntersectHyperplane(const R3Pt & in_pt, const R3Vec & in_vec, R3Polygon &out_polyClip ) const
{
	ASSERT( m_apts.num() >= 3 );

	static Array<double> sa(m_apts.num()); 
    sa.need( m_apts.num() );

	int nin = 0;
	for (int i = 0; i < m_apts.num(); i++) {
		sa[i] = Dot( m_apts[i] - in_pt, in_vec);
		if ( sa[i] >= 0) nin++;
	}

    if ( nin == m_apts.num() ) {
        out_polyClip = *this;
        return FALSE;
    }

	if ( nin == 0 ) { 
        out_polyClip.Clear();
        return TRUE; 
    }

    out_polyClip.Clear();
	for (int vc = 0; vc < m_apts.num(); vc++) {
		const int inc = sa[vc] >= 0.0;
		const int inp = sa.wrap(vc-1) >= 0.0;

        if (inp + inc == 1) {
			out_polyClip += Lerp( m_apts.wrap(vc-1), m_apts[vc], sa[vc] / (sa[vc] - sa.wrap(vc-1)) );
        }
        if (inc) {
			out_polyClip += m_apts[vc];
        }
	}
	return TRUE;
}


WINbool R3Polygon::Test()
{
   R3Polygon q3(3);
   R3Pt q_pt3(0,0,0);

   R3Polygon p1(5);
   R3Polygon p2(2), p3(1);

   double phi = M_PI - (2.0 * M_PI) / (double) 5.0;
   double neg_1 = -1;
   
   RNZero(p1[0]);

   int i;
   for (i = 1; i < 5; i++) {
      neg_1 = neg_1 * -1;
      p1[i][0] = p1[i-1][0] + neg_1 * cos(phi * i);
      p1[i][1] = p1[i-1][1] + neg_1 * sin(phi * i);
      p1[i][2] = 0;
   }

   VERIFY(p1.Valid() == TRUE);
   p2 = p1;
   VERIFY(p1 == p2);

   R3Pt bdry_pt(0,0,0);
   VERIFY(p1.IsBoundary(bdry_pt));
   bdry_pt[0] = 0.5;
   bdry_pt[1] = 1.0;
   VERIFY(p1.IsBoundary(bdry_pt) == FALSE);

   R3Vec norm = p1.Normal();
   VERIFY(RNApproxEqual(norm[0], 0));
   VERIFY(RNApproxEqual(norm[1], 0));
   VERIFY(RNApproxEqual(norm[2], -1));
   
   R3Pt center = p1.Centroid();
   VERIFY(RNApproxEqual(center[0], 0.5));
   VERIFY(RNApproxEqual(center[2], 0.0));

   p2.Print();
   ofstream out("RNtest.txt", ios::out);
   out.precision(16);
   p1.Write(out);
   out.close();
   ifstream in("RNtest.txt", ios::in);

   p3.Read(in);
   in.close();
   VERIFY(p1 == p3);

   p1 = R3Polygon(5);
   R2Polygon unit = R2Polygon::Unit_polygon(5);

   for ( i = 0; i < unit.Num_pts(); i++) {
      p1[i][0] = unit[i][0];
      p1[i][1] = unit[i][1];
      p1[i][2] = 0;
   }
   
   p2 = R3Polygon(2);
   p3 = R3Polygon(p1);

   VERIFY(p3 == p1);

   VERIFY(p1.Valid() == TRUE);
   p2 = p1;
   VERIFY(p1 == p2);

   VERIFY(p1.IsBoundary(p1.Edge(2)(0.2)));
   bdry_pt = Lerp( p1[0], Lerp(p1[1], p1[2], 0.5), 0.5);
   VERIFY(p1.Inside(bdry_pt) == TRUE);

   bdry_pt += R3Vec(-1.0, -2.2f, 0.0);
   VERIFY(p1.IsBoundary(bdry_pt) == FALSE);
   VERIFY(p1.Inside(bdry_pt) == FALSE);
   
   R3Pt closest_pt = p1.Closest_pt(bdry_pt);
   VERIFY(p1.Inside(closest_pt));

   R3Pt proj_pt;
   R3Line_seg seg = p1.Edge(0);
   proj_pt = seg.Project(bdry_pt);
   VERIFY(ApproxEqual(closest_pt, proj_pt));

   bdry_pt = R3Pt(0.01, 0.03, 0.05);
   closest_pt = p1.Closest_pt(bdry_pt);
   VERIFY(Length(closest_pt - bdry_pt) < 0.051);
   return TRUE;
}

