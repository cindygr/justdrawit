#include "StdAfx.H"
#include <utils/Rn_Defs.H>
#include <utils/Rn_Line_seg.H>

double RNEpsilon_d = 1e-15;
float RNEpsilon_f = 1e-6f;

WINbool 
RNIntersectPlaneSegment( const R3Vec       & in_vecPlaneNormal,
                         double              in_dDistAlong,
                         const R3Line_seg  & in_line_seg, 
                         R3Pt              & out_pt)
{
    const R3Pt ptOrig(0,0,0);
    double s1 = Dot( (in_line_seg.P1() - ptOrig), in_vecPlaneNormal) - in_dDistAlong;
    double s2 = Dot( (in_line_seg.P2() - ptOrig), in_vecPlaneNormal) - in_dDistAlong;

    if ( (s1 < 0 && s2 < 0) || (s1 > 0 && s2 > 0) ) 
        return FALSE;

    // what to do when segment lies in plane?  Return mid point
    if (RNIsZero(s1) && RNIsZero(s2)) {
        out_pt = in_line_seg(0.5);
        return TRUE;
    }
    if (RNIsZero( s2 - s1 )) return FALSE;

    out_pt = in_line_seg( s1 / (s1 - s2) );
    return TRUE;
}



R3Pt 
RNClosestPointToPlane( const R3Vec & in_vecPlaneNormal,
                       double        in_dDistAlong,
                       const R3Pt  & in_pt )
{
    const R3Pt pt2 = in_pt + in_vecPlaneNormal;
    double s1 = Dot( (pt2 - R3Pt(0,0,0)), in_vecPlaneNormal) - in_dDistAlong;
    double s2 = Dot( (in_pt - R3Pt(0,0,0)), in_vecPlaneNormal) - in_dDistAlong;

    if ( RNIsZero( s1 - s2 ) )
        return in_pt;

    return pt2 + (in_pt - pt2) * (s1 / ( s1 - s2 ) ); 
}

WINbool 
RNIntersectRayWithPlane( const R3Vec &in_vecPlaneNormal,
                         double       in_dDistAlong,
                         const R3Pt  &in_ptRay,
                         const R3Vec &in_vecRay,
                         R3Pt        &out_ptIntersect )
{
    if ( RNIsZero( Dot( in_vecRay, in_vecPlaneNormal ) ) )
        return FALSE;

    const R3Pt ptOrig(0,0,0);
    double s1 = Dot( (in_ptRay - ptOrig), in_vecPlaneNormal) - in_dDistAlong;
    double s2 = Dot( ((in_ptRay + in_vecRay) - ptOrig), in_vecPlaneNormal) - in_dDistAlong;

    if ( s1 > 0 ) 
        return FALSE;

    // what to do when segment lies in plane?  Return mid point
    if (RNIsZero(s1) && RNIsZero(s2)) {
        out_ptIntersect = in_ptRay + in_vecRay * 0.5;
        return TRUE;
    }
    if (RNIsZero( s2 - s1 )) return FALSE;

    out_ptIntersect = in_ptRay + in_vecRay * ( s1 / (s1 - s2) );
    return TRUE;

}

void
Curvature( const R3Pt  & in_pt, 
           const R3Vec & in_vecDs, 
           const R3Vec & in_vecDt, 
           const R3Vec & in_vecDss, 
           const R3Vec & in_vecDtt, 
           const R3Vec & in_vecDst,
           double &out_dArea, double &out_dCrv1, double &out_dCrv2, R2Vec &out_vec1, R2Vec &out_vec2, R3Vec &out_vecMin, R3Vec &out_vecMax, R3Vec &out_vecNorm )
{
    const double dE = Dot( in_vecDs, in_vecDs );
    const double dF = Dot( in_vecDs, in_vecDt );
    const double dG = Dot( in_vecDt, in_vecDt );

    out_vecNorm = UnitSafe( Cross( in_vecDs, in_vecDt ) );

    const double de = Dot( in_vecDss, out_vecNorm );
    const double df = Dot( in_vecDst, out_vecNorm );
    const double dg = Dot( in_vecDtt, out_vecNorm );

    out_dArea = sqrt( dE * dG - dF * dF );

    const double dA = (dF * dg - dG * df);
    const double dB = (dE * dg - dG * de);
    const double dC = (dE * df - dF * de);

    const double dRoot = dB * dB - 4.0 * dA * dC;
    if ( dRoot < 0 ) {
        ASSERT(FALSE);
    } 

    if ( RNIsZero( dRoot ) ) {
        out_dCrv1 = out_dCrv2 = 0.0;
        out_vec1 = R2Vec( 1.0, 0.0 );
        out_vec2 = R2Vec( 0.0, 1.0 );

        out_vecMax = in_vecDs;
        out_vecMin = in_vecDt;
        return;
    }

    const double dL1 = ( -dB + sqrt(dRoot) ) / (2.0 * dA);
    const double dL2 = ( -dB - sqrt(dRoot) ) / (2.0 * dA);

    out_dCrv1 = (de + df * dL1) / (dE + dF * dL1);
    out_dCrv2 = (de + df * dL2) / (dE + dF * dL2);

    const double dAng1 = atan( dL1 );
    const double dAng2 = atan( dL2 );

    const double dC1 = cos( dAng1 );
    const double dC2 = cos( dAng2 );
    const double dS1 = sin( dAng1 );
    const double dS2 = sin( dAng2 );
    out_vec1 = R2Vec( dC1, dS1 );
    out_vec2 = R2Vec( dC2, dS2 );

    out_vecMax = in_vecDs * dC1 + in_vecDt * dS1;
    out_vecMin = in_vecDs * dC2 + in_vecDt * dS2;

    if ( fabs( out_dCrv1 ) > fabs( out_dCrv2 ) ) {
        const R2Vec vec1 = out_vec2;
        out_vec2 = out_vec1;
        out_vec1 = vec1;

        const R3Vec vecMax = out_vecMin;
        out_vecMin = out_vecMax;
        out_vecMax = vecMax;

        double dCrv1 = out_dCrv2;
        out_dCrv2 = out_dCrv1;
        out_dCrv1 = dCrv1;
    }
    //
    ASSERT( RNIsZero( Dot( UnitSafe( out_vecMax ), UnitSafe( out_vecMin ) ), 1e-6 ) );
}

