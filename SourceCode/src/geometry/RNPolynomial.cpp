
/* Copyright 1991, Brown Computer Graphics Group.  All Rights Reserved. */

/* -------------------------------------------------------------------------
   Manipulations of a point in euclidean space
   ------------------------------------------------------------------------- */

#include "StdAfx.H"
#include <utils/Rn_Polynomial.H>

/* -------------------------- protected Routines --------------------------- */

void RNPolynomial::Compress() // remove excess degrees
{
    ASSERT( Dim() == 1 );
    int iHighest = 0;
    for ( int i = 0; i < num(); i++ ) {
        if ( ! RNIsZero( (*this)[i], 1e-12 ) )
            iHighest = i;
    }

    m_iDegree = iHighest;
    (*this).sub( num() - iHighest - 1 );
}

/* -------------------------- Private Routines ----------------------------- */


/* -------------------------- public routines  ----------------------------- */

/* -------------------------------------------------------------------------
 * DESCR   :	Evaluate a polynomial
 * ------------------------------------------------------------------------- */
double 
RNPolynomial::operator()(double in_t)
const
{
    ASSERT( Dim() == 1 );

    if ( Num_coefs() == 0 )
        return 0.0;

    double out_sum = (*this)[ Num_coefs() - 1 ];
    
    for (int i = Num_coefs() - 2; i >= 0; i--) {
        out_sum = out_sum * in_t + (*this)[i];
    }
    return out_sum;
}

double 
RNPolynomial::operator()(double in_s, double in_t)
const
{
    ASSERT( Dim() == 2 );
    double out_sum = 0;
    Array<double> adSProd( Degree()+1 ), adTProd( Degree()+1 );

    adSProd[0] = 1.0;
    adTProd[0] = 1.0;
    for ( int iI = 1; iI < Degree()+1; iI++ ) {
        adSProd[iI] = adSProd[iI-1] * in_s;
        adTProd[iI] = adTProd[iI-1] * in_t;
    }

    for ( int iT = 0; iT < Degree()+1; iT++ ) {
        for ( int iS = 0; iS < Degree()+1; iS++ ) {
            out_sum += Coef(iS, iT) * adSProd[iS] * adTProd[iT];
        }
    }

    return out_sum;
}


/* -------------------------------------------------------------------------
 * DESCR   :	Add two polynomials
 * ------------------------------------------------------------------------- */
RNPolynomial 
RNPolynomial::operator+(const RNPolynomial &in_poly)
const
{
   RNPolynomial add(WINmax(in_poly.Num_coefs(), Num_coefs()));

   int i = 0;
   for (i = 0; i < WINmin(in_poly.Num_coefs(), Num_coefs()); i++)
      add[i] = in_poly[i] + (*this)[i];

   if (in_poly.Num_coefs() > Num_coefs())
      for (i = Num_coefs(); i < in_poly.Num_coefs(); i++)
         add[i] = in_poly[i];

   if (in_poly.Num_coefs() < Num_coefs())
      for (i = in_poly.Num_coefs(); i < Num_coefs(); i++)
         add[i] = (*this)[i];

   return add;
}

/* -------------------------------------------------------------------------
 * DESCR   :	Subtract two polynomials
 * ------------------------------------------------------------------------- */
RNPolynomial 
RNPolynomial::operator-(
   const RNPolynomial &in_poly
   )
const
{
   RNPolynomial add(WINmax(in_poly.Num_coefs(), Num_coefs()));

   int i = 0;
   for (i = 0; i < WINmin(in_poly.Num_coefs(), Num_coefs()); i++)
      add[i] = (*this)[i] - in_poly[i];

   if (in_poly.Num_coefs() > Num_coefs())
      for (i = Num_coefs(); i < in_poly.Num_coefs(); i++)
         add[i] = -in_poly[i];

   if (in_poly.Num_coefs() < Num_coefs())
      for (i = in_poly.Num_coefs(); i < Num_coefs(); i++)
         add[i] = (*this)[i];

   return add;
}


/* -------------------------------------------------------------------------
 * DESCR   :	Multiply two polynomials
 * ------------------------------------------------------------------------- */
RNPolynomial 
RNPolynomial::operator*(const RNPolynomial &in_poly)
const
{
    RNPolynomial mult(in_poly.Num_coefs() + this->Num_coefs());

    int i = 0;
    for (i = 0; i < Num_coefs(); i++)
        for (int j = 0; j < in_poly.Num_coefs(); j++)
            mult[i + j] += (*this)[i] * in_poly[j];

    mult.Compress();
    return mult;
}

/* -------------------------------------------------------------------------
 * DESCR   :	Divide two polynomials
 * ------------------------------------------------------------------------- */
RNPolynomial 
RNPolynomial::Divide(const RNPolynomial &in_polyDenom, RNPolynomial &out_polyRemainder ) const 
{
    ASSERT( Dim() == 1 && in_polyDenom.Dim() == 1 );

    RNPolynomial rem( *this );

    RNPolynomial div( WINmax( rem.Degree() - in_polyDenom.Degree() + 1, 0 ) );

    for ( int i = 0; i < div.Num_coefs(); i++) {
        const int iDegLeft = rem.Degree() - in_polyDenom.Degree() + 1;
        RNPolynomial multDenom( iDegLeft );

        for ( int j = 0; j < multDenom.Num_coefs(); j++ )
            multDenom[j] = 0.0;
        multDenom[ multDenom.Num_coefs() - 1 ] = rem[ rem.Num_coefs() - 1 ] / in_polyDenom[ in_polyDenom.Num_coefs() - 1 ];
        div[ div.Num_coefs() - i - 1 ] = rem[ rem.Num_coefs() - 1 ] / in_polyDenom[ in_polyDenom.Num_coefs() - 1 ];

        const RNPolynomial sub = in_polyDenom * multDenom;

        rem = rem - sub;
        rem[ rem.Num_coefs() - 1 ] = 0.0;
        rem.sub(1);
        rem.m_iDegree--;
    }

    out_polyRemainder = rem;

    return div;
}


/* -------------------------------------------------------------------------
 * DESCR   :	Scale the polynomial
 * ------------------------------------------------------------------------- */
RNPolynomial 
RNPolynomial::operator*(double in_dScl)
const
{
   RNPolynomial out_polyScaled(Num_coefs());

   int i = 0;
   for (i = 0; i < Num_coefs(); i++)
     out_polyScaled[i] = in_dScl * (*this)[i];

   return out_polyScaled;
}

/* -------------------------------------------------------------------------
 * DESCR   :	Scale the polynomial
 * ------------------------------------------------------------------------- */
RNPolynomial 
RNPolynomial::operator-()
const
{
   RNPolynomial out_polyNegated(Num_coefs());

   int i = 0;
   for (i = 0; i < Num_coefs(); i++)
     out_polyNegated[i] = -1.0 * (*this)[i];

   return out_polyNegated;
}

/* -------------------------------------------------------------------------
 * DESCR   :	See if two polynomials are equal
 * ------------------------------------------------------------------------- */
WINbool
RNPolynomial::operator==(const RNPolynomial &in_poly)
const
{
    int iMinDim = WINmin(in_poly.Degree(), Degree()) + 1;
    
    int i = 0;
    for (i = 0; i < iMinDim; i++)
        if (!RNIsZero((*this)[i] - in_poly[i])) return FALSE;
    
    for (i = iMinDim; i < in_poly.Num_coefs(); i++)
        if (!RNIsZero(in_poly[i])) return FALSE;
    
    for (i = iMinDim; i < Num_coefs(); i++)
        if (!RNIsZero((*this)[i])) return FALSE;
    
   return TRUE;
}


/* -------------------------------------------------------------------------
 * DESCR   :	Set two polynomials equal
 * ------------------------------------------------------------------------- */
RNPolynomial &
RNPolynomial::operator=(const RNPolynomial &in_poly)
{
    m_iDim = in_poly.Dim();
    m_iDegree = in_poly.Degree();

    this->Array<double>::operator=(in_poly);
    
    return *this;
}


/* -------------------------------------------------------------------------
 * DESCR   :	Integrate the polynomial
 * ------------------------------------------------------------------------- */
RNPolynomial 
RNPolynomial::Integrate(double in_dConst)
const
{
   RNPolynomial integr(Num_coefs()+1);
   integr[0] = in_dConst;
   
   for (int i = 1; i < integr.Num_coefs(); i++)
     integr[i] = (*this)[i-1] / (double) i;

   return integr;
}

double
RNPolynomial::Integrate(double in_dT0, double in_dT1)
const
{
   RNPolynomial integr(Integrate(0));

   double sum = integr(in_dT1);

   for (int i = 0; i < integr.Num_coefs(); i++)
      integr[i] = - integr[i];

   return sum + integr(in_dT0);
}

/* -------------------------------------------------------------------------
 * DESCR   :	Differentiate the polynomial
 * ------------------------------------------------------------------------- */
RNPolynomial
RNPolynomial::Differentiate()
const
{
   if (Num_coefs() == 0) return RNPolynomial(0);

   RNPolynomial dif(Num_coefs()-1);

   for (int i = 0; i < dif.Num_coefs(); i++)
      dif[i] = (double) (i+1) * (*this)[i+1];

   return dif;
}

/* -------------------------------------------------------------------------
 * DESCR   :	Substitute one polynomial into another
 * ------------------------------------------------------------------------- */
RNPolynomial
 RNPolynomial::operator()(const RNPolynomial &in_poly)
const
{
   RNPolynomial res((in_poly.Num_coefs() - 1) * (Num_coefs() - 1) + 1);

   RNPolynomial xn(1);
   xn[0] = 1.0;
   for (int i = 0; i < Num_coefs(); i++) {
      res = res + xn * (*this)[i];
      if (i < Num_coefs()-1) xn = xn * in_poly;
   }
   return res;
}

/* -------------------------------------------------------------------------
 * DESCR   :	Print this
 * ------------------------------------------------------------------------- */
void
RNPolynomial::Print()
const
{
    cout << "Polynomial of Dim " << Dim() << " degree " << Degree() << "\n";
    if ( Dim() == 1 ) {
        for (int i = Num_coefs()-1; i > 0; i--)
            cout << (*this)[i] << " x" << i << " + ";
        cout << (*this)[0]  << "\n";
    } else {
        for ( int iS = 0; iS < Degree() + 1; iS++ ) {
            for ( int iT = 0; iT < Degree() + 1; iT++ ) {
                cout << (*this).Coef(iS, iT) << "s^" << iS << "t^" << iT << " ";
            }
        }
        cout << "\n";
                
    }
}

void
RNPolynomial::Write(ofstream &out)
const
{
   out << Num_coefs() << " "; 
   for (int i = 0; i < Num_coefs(); i++) out << (*this)[i] << " ";
   out << "\n";
}

WINbool
RNPolynomial::Read(ifstream &in)
{
   int iN = 0;
   in >> iN;
   if (iN < 0) return FALSE;
   need(iN);

   for (int i = 0; i < Num_coefs(); i++) in >> (*this)[i];

   return TRUE;
}

/* -------------------------------------------------------------------------
 * DESCR   :	Initialize with another polynomial
 * ------------------------------------------------------------------------- */
RNPolynomial::RNPolynomial(const RNPolynomial &in_poly) 
{
   *this = in_poly;
}


/* -------------------------------------------------------------------------
 * DESCR   :	Initialize with 1 variable
 * ------------------------------------------------------------------------- */
RNPolynomial::RNPolynomial(const Array<double> &in_ad ) 
: Array<double>( in_ad ),
  m_iDim(1),
  m_iDegree( in_ad.num()-1 )
{
}

/* -------------------------------------------------------------------------
 * DESCR   :	Initialize from scratch
 * ------------------------------------------------------------------------- */
RNPolynomial::RNPolynomial(int in_iNum_coefs) 
{
    need( in_iNum_coefs );

    m_iDim = 1;
    m_iDegree = in_iNum_coefs-1;
    for (int i = 0; i < in_iNum_coefs; i++)
        (*this)[i] = 0;
}

RNPolynomial::RNPolynomial(int in_iDim, int in_iDegree) 
    : Array<double>( (in_iDegree + 1) * (in_iDegree + 1) )
{
    ASSERT(in_iDim == 2);

    m_iDim = in_iDim;
    m_iDegree = in_iDegree;

    for (int i = 0; i < num(); i++)
        (*this)[i] = 0;
}
// Zero coefficients
RNPolynomial::RNPolynomial() 
{
    m_iDim = 1;
    m_iDegree = 0;
}

/* -------------------------------------------------------------------------
 * DESCR   :	Destroy
 * ------------------------------------------------------------------------- */
RNPolynomial::~RNPolynomial()
{
}

/* -------------------------------------------------------------------------
 * DESCR   :	Test
 * ------------------------------------------------------------------------- */
WINbool RNPolynomial::Test()
{
   RNPolynomial p1(3), p2(2), p3(4);
   p1[0] = 3.0;
   p1[1] = 2.0;
   p1[2] = 1.0;

   p2[0] = -1.0;
   p2[1] = 1.0;

   p3[0] = 3.0;
   p3[1] = 2.0;
   p3[2] = 1.0;
   p3[3] = 0.0;

   p3 = p1;
   VERIFY(p3 == p1);
   
   p3 = p1 + p2;
   double t = 0.1;
   for (t = 0.1; t < 1.0; t += 0.1) {
      VERIFY(RNApproxEqual(p3(t), p1(t) + p2(t), 1e-12));
   }

   p3 = p1 - p2;
   for (t = 0.1; t < 1.0; t += 0.1) {
      VERIFY(RNApproxEqual(p3(t), p1(t) - p2(t)));
   }

   p3 = p1 * p2;
   for (t = 0.1; t < 1.0; t += 0.1) {
      VERIFY(RNApproxEqual(p3(t), p1(t) * p2(t)));
   }

   p3 = p1(p2);
   for (t = 0.1; t < 1.0; t += 0.1) {
      VERIFY(RNApproxEqual(p3(t), p1(p2(t))));
   }

   p3 = p1 * 3.0;
   for (t = 0.1; t < 1.0; t += 0.1) {
      VERIFY(RNApproxEqual(p3(t), p1(t) * 3.0, 1e-12));
   }

   p3 = p1.Differentiate();
   VERIFY(p3.Num_coefs() == 2);
   VERIFY(p3[1] == p1[2] * 2.0);
   VERIFY(p3[0] == p1[1]);

   p3.Print();
   ofstream out("RNtest.txt", ios::out);
   p3.Write(out);
   out.close();
   ifstream in("RNtest.txt", ios::in);
   VERIFY(p3.Read(in));
   in.close();

   for ( int i = 0; i < 5; i++ ) {
       RNPolynomial polyNum(4), polyDenom(3);
       polyNum[0] = -1.0 + ( rand() % 10 ) / 5.0;
       polyNum[1] = -1.0 + ( rand() % 11 ) / 4.0;
       polyNum[2] = -1.0 + ( rand() % 12 ) / 3.0;
       polyNum[3] = -1.0 + ( rand() % 13 ) / 6.0;
       polyDenom[0] = -0.5 + ( rand() % 10 ) / 9.0;
       polyDenom[1] = -0.5 + ( rand() % 10 ) / 9.0;
       polyDenom[2] = -0.5 + ( rand() % 10 ) / 9.0;
       RNPolynomial polyDiv, polyRem;
       polyDiv = polyNum.Divide( polyDenom, polyRem );
       RNPolynomial polyCheck = polyDiv * polyDenom + polyRem;

       for ( int iC = 0; iC < polyCheck.Degree(); iC++ ) {
           VERIFY( RNApproxEqual( polyCheck[iC], polyNum[iC] ) );
       }
   }

   for ( FORINT i = 0; i < 5; i++ ) {
       RNPolynomial polyNum(5), polyDenom(3);
       polyNum[0] = -1.0 + ( rand() % 10 ) / 5.0;
       polyNum[1] = -1.0 + ( rand() % 11 ) / 4.0;
       polyNum[2] = -1.0 + ( rand() % 12 ) / 3.0;
       polyNum[3] = -1.0 + ( rand() % 13 ) / 6.0;
       polyNum[4] = -1.1 + ( rand() % 9 ) / 4.0;
       polyDenom[0] = -0.5 + ( rand() % 10 ) / 9.0;
       polyDenom[1] = -0.5 + ( rand() % 10 ) / 9.0;
       polyDenom[2] = -0.5 + ( rand() % 10 ) / 9.0;
       RNPolynomial polyDiv, polyRem;
       polyDiv = polyNum.Divide( polyDenom, polyRem );
       RNPolynomial polyCheck = polyDiv * polyDenom + polyRem;

       for ( int iC = 0; iC < polyCheck.Degree(); iC++ ) {
           VERIFY( RNApproxEqual( polyCheck[iC], polyNum[iC] ) );
       }
   }
   return TRUE;
}
