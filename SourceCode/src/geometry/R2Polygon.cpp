#include "StdAfx.H"
#include <utils/Rn_Polygon.H>


double R2Polygon::SampleEvenly( Array<R2Pt> &out_aptSamples ) const
{
    double dLenSum = 0.0;
    Array<double> adLen( Num_pts() );
    for ( int i = 0; i < Num_pts(); i++ ) {
        adLen[i] = Length( PtWrap(i+1) - (*this)[i] );
        dLenSum += adLen[i];
    }

    if ( out_aptSamples.num() == 0 ) out_aptSamples.need(16);

    const double dPerSeg = dLenSum / (double) out_aptSamples.num();

    int iSeg = 0;
    double dSumNext = 0.0, dSumLeft = adLen[0], dLenAccum = 0;
    // Sample uniformlly along the boundary
    for ( FORINT i = 0; i < out_aptSamples.num(); i++ ) {
        while ( dSumNext > dLenAccum ) {
            const double dLeft = dSumNext - dLenAccum;
            if ( dLeft < dSumLeft ) {
                dSumLeft = dSumLeft - dLeft;
                dLenAccum += dLeft;
            } else {
                iSeg++;
                dLenAccum += dSumLeft;
                dSumLeft = adLen[iSeg];
            }
        }
        const double dT = (adLen[iSeg] - dSumLeft) / adLen[iSeg];
        out_aptSamples[i] = Edge(iSeg)( dT );
        dSumNext += dPerSeg;
    }
    
    return dLenSum;
}

Array<R2Pt> R2Polygon::Intersect( const R2Line_seg &in_seg ) const
{
    Array<R2Pt> apt;
    R2Pt pt;
    double dS, dT;

    for ( int i = 0; i < Num_pts(); i++ ) {
        if ( Edge( i ).Intersect( in_seg, pt, dS, dT ) ) {
            if ( ! ApproxEqual( pt, Pts()[i] ) )
                apt += pt;
        }
    }
    return apt;
}

WINbool R2Polygon::IsConcave(const int in_iPt) const
{
    if ( m_apts.num() <= 3 ) return FALSE;

    R2Polygon oPoly( m_apts.num() - 1 );

    int iIndx = 0;
    for (int j = 0; j < m_apts.num(); j++) {
        if ( j != in_iPt )
            oPoly[iIndx++] = m_apts[j];
    }
    
    if ( oPoly.Inside( m_apts[in_iPt] ) == TRUE )
        return TRUE;

    return FALSE;
}

/* -------------------------------------------------------------------------
 * DESCR   :	Calculate the area of a polygon by adding up areas of wedges
 * ------------------------------------------------------------------------- */
double R2Polygon::Area() const
{
    if ( m_apts.num() == 3 ) {
        const R2Vec v1 = m_apts[1] - m_apts[0];
        const R2Vec v2 = m_apts[2] - m_apts[0];
        const double dCross = v1[0] * v2[1] - v1[1] * v2[0];
        return 0.5 * fabs( dCross );
    }

    double dArea = 0;
    const R2Pt ptCentroid = Centroid();

    Array<R2Vec> avecEdge( m_apts.num() );
    for (int i = 0; i < m_apts.num(); i++) {
        avecEdge[i][0] = m_apts[i][0] - ptCentroid[0];
        avecEdge[i][1] = m_apts[i][1] - ptCentroid[1];
    }
    
    for ( FORINT i = 0; i < m_apts.num(); i++ ) {
        const R2Vec &v1 = avecEdge[i];
        const R2Vec &v2 = avecEdge.wrap(i+1);
        const double dCross = v1[0] * v2[1] - v1[1] * v2[0];
        dArea += fabs( dCross ) * 0.5;
    }

    return dArea;
}

/* -------------------------------------------------------------------------
 * DESCR   :	Is the polygon convex?
 *             Using stupid check - see if any points lie within the polygon
 * ------------------------------------------------------------------------- */
WINbool R2Polygon::IsConvex() const
{
    if ( m_apts.num() <= 3 ) return TRUE;

    R2Polygon oPoly( m_apts.num() - 1 );

    for (int i = 0; i < m_apts.num(); i++) {
        int iIndx = 0;
        for (int j = 0; j < m_apts.num(); j++) {
            if ( j != i )
                oPoly[iIndx++] = m_apts[j];
        }
        
        if ( oPoly.Inside( m_apts[i] ) == TRUE )
            return FALSE;
    }
    return TRUE;
}

/* -------------------------------------------------------------------------
 * DESCR   :	Make a unit polygon with the given number of sides centered at
 *              the origin
 * ------------------------------------------------------------------------- */
R2Polygon
R2Polygon::Unit_polygon(int num_sides)
{
   R2Polygon unit(num_sides);

   const double cdPi = 3.1415926535897932384626433832795028842;
   double phi = cdPi - (2.0 * cdPi) / (double) num_sides;
   double neg_1 = -1;

   unit[0][0] = 0;
   unit[0][1] = 0;
   int i;
   for (i = 1; i < num_sides; i++) {
      neg_1 = neg_1 * -1;
      unit[i][0] = unit[i-1][0] + neg_1 * (double) cos( phi * i );
      unit[i][1] = unit[i-1][1] + neg_1 * (double) sin(phi * i);
   }

   R2Pt center_point = unit.Centroid();
   R2Vec center(center_point[0], center_point[1]);
   // Move to origin
   for (i = 0; i < num_sides; i++) {
     unit[i] = unit[i] - center;
   }
   
   return unit;
}


/* -------------------------------------------------------------------------
 * DESCR   :	Return the centroid of the polygon
 * ------------------------------------------------------------------------- */
R2Pt R2Polygon::Centroid() const
{
    R2Pt center = R2Pt(0, 0);
    
    for (int i = 0; i < Num_pts(); i++)
        for (int d = 0; d < center.Dim(); d++)
            center[d] += (*this)[i][d];
    
    if (Num_pts() > 0) {
        for (int d = 0; d < center.Dim(); d++)
            center[d] = center[d] / (double) Num_pts();
    } else {
        for (int d = 0; d < center.Dim(); d++) center[d] = 1e30f;
    }

    return center;
}


/* -------------------------------------------------------------------------
 * DESCR   :	Print this 
 * ------------------------------------------------------------------------- */
void R2Polygon::Print() const
{
    cout << "Polygon " << Dim() << " with " << Num_pts() << " vertices\n";
    for (int i = 0; i < Num_pts(); i++) {
        cout << (*this)[i];
    }
    cout << "\n";
}



/* -------------------------------------------------------------------------
 * DESCR   :	read & write
 * ------------------------------------------------------------------------- */
void R2Polygon::Write(ofstream &out) const 
{
    out <<  " " << Num_pts() << "\n";
    for (int i = 0; i < Num_pts(); i++) {
        out << (*this)[i] << " ";
    }
    out << "\n";
}

void R2Polygon::WriteBinary(ofstream &out) const 
{
    const int iN = Num_pts();
    out.write( (const char *) &iN, sizeof(int) );
    for (int i = 0; i < Num_pts(); i++) {
        out.write( (const char *) &m_apts[i][0], sizeof(double) * m_apts[i].Dim() );
    }
}

WINbool R2Polygon::Read(ifstream &in)
{
    int n;
    in >> n;
    m_apts.need(n);
    
    WINbool bRes = (n >= 0) ? TRUE : FALSE;
    for (int i = 0; i < Num_pts(); i++) {
        bRes = ( bRes && m_apts[i].Read( in ) ) ? TRUE : FALSE;
    }
    return bRes;
}

WINbool R2Polygon::ReadBinary(ifstream &in)
{
    int n;
    in.read( (char *) &n, sizeof(int) );
    m_apts.need(n);
    
    WINbool bRes = (n >= 0) ? TRUE : FALSE;
    for (int i = 0; i < Num_pts(); i++) {
        in.read( (char *) &m_apts[i][0], m_apts[i].Dim() * sizeof(double) );
        const int lRead = in.gcount();
        bRes = ( bRes && lRead == m_apts[i].Dim() * sizeof(double) ) ? TRUE : FALSE;
    }
    return bRes;
}

/* -------------------------------------------------------------------------
 * DESCR   :	Polygon intersection classes and routines
 * ------------------------------------------------------------------------- */

class Vert {
public:
    int index;
    int e1, e2, e3, e4;
    WINbool inside;
    R2Pt pt;
    void set( int e1,  int e2,  WINbool inside, const R2Pt &p);
    inline Vert() {}
    inline virtual ~Vert() {}
};

class Edge {
public:
    int index;
    int v1, v2;
    WINbool tested;
    R2Line_seg seg;
    int polygon;
    
    void set( int v1,  int v2,  WINbool tested,
              const R2Pt &p1, const R2Pt &p2,  int p = -1);
    inline Edge() {}
    inline virtual ~Edge() {}
};

static Array<Vert > v_list;
static Array<Edge > e_list;

static int iN1 = 0;
static int iN2 = 0;
static int __v_count = 0;
static int __e_count = 0;
static Array<int> __poly_verts;

/* -------------------------- protected Routines --------------------------- */

/* -------------------------- Private Routines ----------------------------- */

/* -------------------------------------------------------------------------
 * DESCR   :	Create verst/edges
 * ------------------------------------------------------------------------- */

void Vert::set( int in_iE1,  int in_iE2,  WINbool in_bInside, const R2Pt &in_pt)
{
    pt[0] = in_pt[0];
    pt[1] = in_pt[1];
    e1 = in_iE1;
    e2 = in_iE2;
    e3 = -1;
    e4 = -1;
    inside = in_bInside;
    index = __v_count++;
}

void Edge::set( int in_iV1,  int in_iV2,  WINbool in_bTested, 
                const R2Pt &in_p1, 
                const R2Pt &in_p2, 
                int in_iPolygon)
{
    seg.P1()[0] = in_p1[0];
    seg.P1()[1] = in_p1[1];
    seg.P2()[0] = in_p2[0];
    seg.P2()[1] = in_p2[1];
    
    index = __e_count++;
    v1 = in_iV1;
    v2 = in_iV2;
    tested = in_bTested;
    polygon = in_iPolygon;
}


/* -------------------------------------------------------------------------
 * DESCR   :	Initialize the lists with the polygon points
 * ------------------------------------------------------------------------- */
WINbool initialize_lists(const R2Polygon &in_p1,
                      const R2Polygon &in_p2)
{
   WINbool out_bIntersect = FALSE;
   iN1 = in_p1.Num_pts();
   iN2 = in_p2.Num_pts();

   // This should be big enough...
   int size = (iN1 + 2) * (iN2 + 2);
   v_list.need(size);
   e_list.need(size);
   
   __v_count = 0;
   __e_count = 0;

   int i;
   for (i = 0; i < iN1; i++) {
      v_list[i].set(i,  (i-1+iN1) % iN1,
                    in_p2.Inside(in_p1[i]),
                    in_p1[i]);
      
      if (v_list[i].inside == TRUE)
        out_bIntersect = TRUE;
   }
   for (i = 0; i < iN1; i++) 
     e_list[i].set(i,   (i+1) % iN1, 
                   FALSE, 
                   v_list[i].pt, 
                   v_list[(i+1) % iN1].pt, 
                   0);
   
   for (i = 0; i < iN2; i++) {
      v_list[i + iN1].set(i + iN1,   ((i - 1 + iN2) % iN2 + iN1),
                          in_p1.Inside(in_p2[i]), 
                          in_p2[i]);
      if (v_list[i + iN1].inside == TRUE)
        out_bIntersect = TRUE;
   }
   for (i = 0; i < iN2; i++) 
     e_list[i + iN1].set(i + iN1,   (i+1) % iN2 + iN1, 
                         FALSE, 
                         v_list[i + iN1].pt, 
                         v_list[(i+1) % iN2 + iN1].pt, 
                         1);
/*
   for (int j = 0; j < __v_count; j++)
      cout << "V " << j << " " << v_list[j].e1 << " " << v_list[j].e2 << " " <<
         v_list[j].e3 << " " << v_list[j].e4 << " inside " 
                     << v_list[j].inside << "\n";


   for (j = 0; j < __e_count; j++)
      cout << "E " << j << " " << e_list[j].v1 << " " << e_list[j].v2 << "\n";
      */
   return out_bIntersect;
}

/* -------------------------------------------------------------------------
 * DESCR   :	Find an untested edge. Return -1 if no edge
 * ------------------------------------------------------------------------- */
static int untested_edge()
{
   for (int i = 0; i < __e_count; i++)
     if (e_list[i].tested == FALSE)
       return i;
   
   return -1;
}

/* -------------------------------------------------------------------------
 * DESCR   :	Replace the edges (we have an intersection)
 *                                v1
 *                                |
 *                                | e1
 *                           e2   |    new e2 
 *                       w1-------nv------------w2
 *                                |
 *                                | new e1
 *                                |
 *                                v2
 * ------------------------------------------------------------------------- */
static void replace_edge(Edge &e1, Edge &e2, const R2Pt &new_pt)
{
    /*    new_pt.print(); */
    /*    cout << "Replace " << e1.index << " " << e2.index << "\n"; */
    int new_v = __v_count, new_e = __e_count;
    
    v_list[new_v].set(e1.index, e2.index, TRUE, new_pt);
    e_list[new_e].set(new_v, e1.v2, e1.tested, new_pt, v_list[e1.v2].pt);
    v_list[new_v].e3 = new_e;
    Vert &v2 = v_list[e1.v2];
    if      (v2.e1 == e1.index) v2.e1 = new_e;
    else if (v2.e2 == e1.index) v2.e2 = new_e;
    else if (v2.e3 == e1.index) v2.e3 = new_e;
    else if (v2.e4 == e1.index) v2.e4 = new_e;
    
    new_e = __e_count;
    e_list[new_e].set(new_v, e2.v2, e2.tested, new_pt, v_list[e2.v2].pt);
    v_list[new_v].e4 = new_e;
    Vert &w2 = v_list[e2.v2];
    if      (w2.e1 == e2.index) w2.e1 = new_e;
    else if (w2.e2 == e2.index) w2.e2 = new_e;
    else if (w2.e3 == e2.index) w2.e3 = new_e;
    else if (w2.e4 == e2.index) w2.e4 = new_e;
    
    e1.v2 = new_v;
    e2.v2 = new_v;
    e1.seg.P2() = new_pt;
    e2.seg.P2() = new_pt;

/*   
   for (int j = 0; j < V_count(); j++)
      cout << "V " << j << " " << v_list[j].e1 << " " << v_list[j].e2 << " " <<
         v_list[j].e3 << " " << v_list[j].e4 << " inside " 
                     << v_list[j].inside << "\n";


   for (j = 0; j < E_count(); j++)
      cout << "E " << j << " " << e_list[j].v1 << " " << e_list[j].v2 << "\n";

   e1.seg.print();
   e2.seg.print();
   e_list[new_e-1].seg.print();
   e_list[new_e].seg.print();
   */
}

/* -------------------------------------------------------------------------
 * DESCR   :	Split an edge (we have an end point in a segment)
 *                                v1
 *                                |
 *                                | split_e1
 *                                |    
 *                               /v------w2 (other edge attatched to v)
 *                        edge  / |
 *                             /  | split_e2 
 *                            w1  |  (new edge)
 *                                v2
 * ------------------------------------------------------------------------- */
static
void split_edge(Edge &split, 
                const R2Pt &inter_pt, 
                const Edge &edge)
{
/*    cout << "Splitting " << split.index << " " << edge.index << "\n"; */
   // v gets two new edges
   // v is marked as inside
   // split.v2 is changed to v
   // a new edge is made from v to split.v2

   const int new_e = __e_count;
   const int v = (v_list[edge.v1].pt == inter_pt) ? edge.v1 : edge.v2;

   WINbool split_in = FALSE, edge_in = FALSE;
   if (v_list[v].e1 == split.index) split_in = TRUE;
   if (v_list[v].e2 == split.index) split_in = TRUE;
   if (v_list[v].e3 == split.index) split_in = TRUE;
   if (v_list[v].e4 == split.index) split_in = TRUE;

   if (v_list[v].e1 == edge.index) edge_in = TRUE;
   if (v_list[v].e2 == edge.index) edge_in = TRUE;
   if (v_list[v].e3 == edge.index) edge_in = TRUE;
   if (v_list[v].e4 == edge.index) edge_in = TRUE;

   if (edge_in == TRUE && split_in == TRUE)
      return;

   v_list[v].inside = TRUE;

   e_list[new_e].set(v, split.v2, split.tested, 
                            inter_pt, v_list[split.v2].pt);
   v_list[v].e3 = new_e;
   Vert &v2 = v_list[split.v2];
   if      (v2.e1 == split.index) v2.e1 = new_e;
   else if (v2.e2 == split.index) v2.e2 = new_e;
   else if (v2.e3 == split.index) v2.e3 = new_e;
   else if (v2.e4 == split.index) v2.e4 = new_e;

   v_list[v].e4 = split.index;

   split.v2 = v;
   split.seg.P2() = inter_pt;
   
/*
   for (int j = 0; j < V_count(); j++)
      cout << "V " << j << " " << v_list[j].e1 << " " << v_list[j].e2 << " " <<
         v_list[j].e3 << " " << v_list[j].e4 << " inside " 
                     << v_list[j].inside << "\n";


   for (j = 0; j < E_count(); j++)
      cout << "E " << j << " " << e_list[j].v1 << " " << e_list[j].v2 << "\n";
*/
}

/* -------------------------------------------------------------------------
 * DESCR   :	Find the v on the other side of the edge
 * ------------------------------------------------------------------------- */
static int v_other(const Vert &v, const Edge &e) 
{
   if (e.v1 == v.index)
       return e.v2;
   return e.v1;
}

/* -------------------------------------------------------------------------
 * DESCR   :	Find the next v that\'s inside
 * ------------------------------------------------------------------------- */
static int find_next_v( int prev, int curr)
{
   int other_v;

   other_v = v_other(v_list[curr], e_list[v_list[curr].e1]);
   if (other_v != prev && v_list[other_v].inside == TRUE)
      return other_v;
   other_v = v_other(v_list[curr], e_list[v_list[curr].e2]);
   if (other_v != prev && v_list[other_v].inside == TRUE)
      return other_v;

   ASSERT (v_list[curr].e3 != -1); // find next v couldn't find

   other_v = v_other(v_list[curr], e_list[v_list[curr].e3]);
   if (other_v != prev && v_list[other_v].inside == TRUE)
      return other_v;

   other_v = v_other(v_list[curr], e_list[v_list[curr].e4]);
   if (other_v != prev && v_list[other_v].inside == TRUE)
      return other_v;

   ASSERT(FALSE); // find next v couldn't find 2

   int j;
   for ( j = 0; j < __v_count; j++)
      cout << "V " << j << " " << v_list[j].e1 << " " << v_list[j].e2 << " " <<
         v_list[j].e3 << " " << v_list[j].e4 << " inside " 
                     << (int) v_list[j].inside << "\n";

   
   for (j = 0; j < __e_count; j++)
      cout << "E " << j << " " << e_list[j].v1 << " " << e_list[j].v2 << "\n";

   cout.flush();

   return -1;
}

/* -------------------------------------------------------------------------
 * DESCR   :	Reconstruct the polygon by following edges
 * ------------------------------------------------------------------------- */
static WINbool reconstruct_polygon(WINbool intersected)
{
   int inside_v = -1;
/*
   for (int j = 0; j < __v_count; j++)
      cout << "V " << j << " " << v_list[j].e1 << " " << v_list[j].e2 << " " <<
         v_list[j].e3 << " " << v_list[j].e4 << " inside " 
                     << v_list[j].inside << "\n";

   for (j = 0; j < __e_count; j++)
      cout << "E " << j << " " << e_list[j].v1 << " " << e_list[j].v2 << "\n";
   cout.flush();
*/
   // Find first vertex inside a polygon
   for (int i = 0; i < __v_count; i++)
     if (v_list[i].inside == TRUE) {
        inside_v = i;
        break;
     }
   
   // No intersection
   if (intersected == FALSE && inside_v == -1) {
      __poly_verts.need(0);
      return TRUE;
   }

   // One polygon inside the other
   if (intersected == FALSE) {
      if (inside_v < iN1) {
         __poly_verts.need(iN1);
         for (int i = 0; i < __poly_verts.num(); i++) {
            __poly_verts[i] = i;
            if (v_list[i].inside == FALSE) // Ooops - we were outside
              __poly_verts.need(0);
         }
      } else {
         int num_verts = __v_count - iN1; // Number of vertices of other polygon
         __poly_verts.need(num_verts);
         for (int i = 0; i < num_verts; i++) {
            __poly_verts[i] = iN1 + i;
            if (v_list[i + iN1].inside == FALSE)  // Ooops - we were outside
               __poly_verts.need(0);
         }
      }
      return TRUE;
   }
   int next_v = -1, prev_v = -1, first_v = inside_v;
   __poly_verts.need(0);
   while (next_v != first_v) {
      __poly_verts += inside_v;
      next_v = find_next_v(prev_v, inside_v);
      if (next_v == -1) {
         __poly_verts.need(0);
         return FALSE; 
      }
      prev_v = inside_v;
      inside_v = next_v;
   }
   return TRUE;
}


/* -------------------------------------------------------------------------
 * DESCR   :	Make an RNpolygon_2D from poly_verts
 * DETAILS :    Destroys the data structures when it\'s done
 * ------------------------------------------------------------------------- */
R2Polygon R2Polygon::Make_polygon() const
{
    R2Polygon out_poly(__poly_verts.num());
    for (int i = 0; i < out_poly.Num_pts(); i++) {
        out_poly[i][0] = v_list[__poly_verts[i]].pt[0];
        out_poly[i][1] = v_list[__poly_verts[i]].pt[1];
    }
    __poly_verts.squeeze();
    v_list.squeeze();
    e_list.squeeze();
    
    return out_poly;
}

/* -------------------------- public routines  ----------------------------- */


/* -------------------------------------------------------------------------
 * DESCR   :	See if a point is inside the polygon by doing the inside
 * 		out test. 
 * DETAILS :    Only works in 2D
 * ------------------------------------------------------------------------- */
WINbool
R2Polygon::Inside_winding(const R2Pt &in_pt) const
{
   if (Num_pts() < 2)
     return FALSE;

   R2Pt ptClipMax(-1e30f, -1e30f), ptClipMin(1e30f, 1e30f);

   int i;
   for ( i = 0; i < Num_pts(); i++) {
       ptClipMax[0] = WINmax(ptClipMax[0], m_apts[i][0]);
       ptClipMax[1] = WINmax(ptClipMax[1], m_apts[i][1]);
       ptClipMin[0] = WINmin(ptClipMin[0], m_apts[i][0]);
       ptClipMin[1] = WINmin(ptClipMin[1], m_apts[i][1]);
      
       if (ApproxEqual(in_pt, m_apts[i]))
        return TRUE;
   }
   if ( in_pt[0] < ptClipMin[0] - RNEpsilon_d ) return FALSE;
   if ( in_pt[0] > ptClipMax[0] + RNEpsilon_d ) return FALSE;
   if ( in_pt[1] < ptClipMin[1] - RNEpsilon_d ) return FALSE;
   if ( in_pt[1] > ptClipMax[1] + RNEpsilon_d ) return FALSE;

   ptClipMax[1] += 1.0;
   ptClipMax[0] += 3.0;

   WINbool through_vertex = FALSE;

   R2Line_seg to_inf(in_pt, ptClipMax);
   int iCountChange = 0;
   do {
      through_vertex = FALSE;
      for (int i = 0; i < Num_pts(); i++)
        if (to_inf.IsPtOnSeg(m_apts[i]) == TRUE) {
           through_vertex = TRUE;
           break;
        }
      if (through_vertex == TRUE) {
         to_inf.P2()[0] +=  1e-6 * ( (double) rand() / (double) RAND_MAX );
         to_inf.P2()[1] +=  1e-6 * ( (double) rand() / (double) RAND_MAX );
      }
      
      iCountChange++;
   } while(through_vertex == TRUE && iCountChange < 20);
   
   int num_inters = 0;
   R2Line_seg edge;
   R2Pt pt_inter;
   double t_on_line, d_to_line;
   for (i = 0; i < Num_pts(); i++) {
      edge = Edge(i);
      if (edge.IsPtOnSeg(in_pt) == TRUE)
        return TRUE;
      if (edge.Intersect(to_inf, pt_inter, t_on_line, d_to_line))
         num_inters++;
   }
   if (num_inters%2 == 0)
     return FALSE;
   return TRUE;
}

/* -------------------------------------------------------------------------
 * DESCR   :	Intersect with each edge segment, take closest
 * ------------------------------------------------------------------------- */
WINbool R2Polygon::IntersectRay( const R2Pt  & in_pt, 
                                 const R2Vec & in_vec,
                                 Array<R2Pt> & out_apt ) const
{
    const double dLen = Length( Centroid() - in_pt );
    R2Pt ptMin(1e30, 1e30), ptMax(-1e30, -1e30);

    for ( int i = 0; i < Num_pts(); i++ ) {
        ptMin[0] = WINmin( ptMin[0], m_apts[i][0] );
        ptMin[1] = WINmin( ptMin[1], m_apts[i][1] );
        ptMax[0] = WINmax( ptMax[0], m_apts[i][0] );
        ptMax[1] = WINmax( ptMax[1], m_apts[i][1] );
    }

    const double dSize = Length( ptMax - ptMin );
    const R2Line_seg oSeg( in_pt, in_pt + UnitSafe( in_vec ) * (dLen + dSize * 1.4) );

    double        dS, dT;
    R2Pt          ptIntersect;

    out_apt.need(0);

    Array<double> adLen;
    for (FORINT i = 0; i < Num_pts(); i++) {
        if ( oSeg.Intersect( Edge(i), ptIntersect, dS, dT ) == TRUE ) {
            out_apt += ptIntersect;
            adLen += Length( ptIntersect - in_pt );
        }
    }
    out_apt.sort( adLen );

    if ( out_apt.num() > 0 )
        return TRUE;

    return FALSE;
}

WINbool R2Polygon::IntersectRay( const R2Pt &in_pt, const R2Vec &in_vec, 
                                 Array<R2Pt> &out_apt, Array<double> &out_adTRay,
                                 Array<int> &out_aiSide, Array<double> &out_adSide ) const
{
    const double dLen = Length( Centroid() - in_pt );
    R2Pt ptMin(1e30, 1e30), ptMax(-1e30, -1e30);

    for ( int i = 0; i < Num_pts(); i++ ) {
        ptMin[0] = WINmin( ptMin[0], m_apts[i][0] );
        ptMin[1] = WINmin( ptMin[1], m_apts[i][1] );
        ptMax[0] = WINmax( ptMax[0], m_apts[i][0] );
        ptMax[1] = WINmax( ptMax[1], m_apts[i][1] );
    }

    const double dSize = Length( ptMax - ptMin );
    const R2Line_seg oSeg( in_pt, in_pt + UnitSafe( in_vec ) * (dLen + dSize * 1.4) );

    double        dS, dT;
    R2Pt          ptIntersect;

    Array<R2Pt> apt;
    Array<double> adLen, adTRay, adSide;
    Array<int> aiOrder, aiSide;
    for (FORINT i = 0; i < Num_pts(); i++) {
        if ( oSeg.Intersect( Edge(i), ptIntersect, dS, dT ) == TRUE ) {
            aiOrder += out_apt.num();
            apt += ptIntersect;
            aiSide += i;
            adTRay += dS;
            adSide += dT;
            adLen += Length( ptIntersect - in_pt );
        }
    }

    out_apt.need(apt.num());
    out_aiSide.need(apt.num());
    out_adTRay.need(apt.num());
    out_adSide.need(apt.num());

    aiOrder.sort( adLen );
    for ( FORINT i = 0; i < aiOrder.num(); i++ ) {
        out_apt[i] = apt[ aiOrder[i] ];
        out_adTRay[i] = adTRay[ aiOrder[i] ];
        out_aiSide[i] = aiSide[ aiOrder[i] ];
        out_adSide[i] = adSide[ aiOrder[i] ];
    }

    if ( out_apt.num() > 0 )
        return TRUE;

    return FALSE;
}

/// 
WINbool R2BoxedPolygon::IntersectRayQuick( const R2Pt &in_pt, const R2Vec &in_vec, 
                                           Array<R2Pt> &out_apt, Array<double> &out_adTRay,
                                           Array<int> &out_aiSide, Array<double> &out_adSide ) const
{
    const double dLen = Length( in_pt - Centroid() );
    const R2Pt ptEnd = in_pt + UnitSafe( in_vec ) * (dLen + sqrt(2.0) * WINmax( Width() / 2.0, Height() / 2.0 ) );

    if ( ptEnd[0] < WMin() && in_pt[0] < WMin() )
        return FALSE;
    if ( ptEnd[0] > WMax() && in_pt[0] > WMax() )
        return FALSE;
    if ( ptEnd[1] < WMin() && in_pt[1] < WMin() )
        return FALSE;
    if ( ptEnd[1] > WMax() && in_pt[1] > WMax() )
        return FALSE;

    return IntersectRayQuick( in_pt, in_vec, out_apt, out_adTRay, out_aiSide, out_adSide );
}

/* -------------------------------------------------------------------------
 * DESCR   :	First check if inside box, then do convex dot product check
 * ------------------------------------------------------------------------- */
WINbool R2BoxedPolygon::Inside_quick(const R2Pt &in_pt) const
{
    if ( IsOutsideBox(in_pt[0], in_pt[1]) == TRUE ) return FALSE;

    const double dNorm = ( m_bClockwise ? 1 : -1 );
    
    for (int i = 0; i < m_apts.num(); i++) {
        const R2Pt &p2 = m_apts.wrap(i+1);
        const R2Pt &p1 = m_apts[i];
        const R2Vec &e_last = p2 - p1;
        const R2Vec &e = in_pt - p2;
        
        if ( ApproxEqual( in_pt, p1 ) ) 
            return TRUE;
        // Cross[2]
        if ( (e_last[0] * e[1] - e_last[1] * e[0] ) * dNorm < -RNEpsilon_d) {
            return FALSE;
        }
    }
    return TRUE;
}


/* -------------------------------------------------------------------------
 * DESCR   :	Intersect two polygons. Have three cases: one polygon is
 * 		entirely within another, they're seperate, or they overlap.
 * 		First see if any of the corners of the first polygon lie in
 * 		the second polygon. If none of the corners do, then the
 * 		intersection is empty.
 * ------------------------------------------------------------------------- */
R2Polygon
R2Polygon::Intersect(const R2Polygon &in_poly) const
{
   if (in_poly.Num_pts() == 0 || Num_pts() == 0)
       return R2Polygon(0);
   
   WINbool intersected = FALSE, cut = FALSE;
   int  untested = -1;
   WINbool vertex_inside = initialize_lists(*this, in_poly);
   
   for (int i = 0; i < in_poly.Num_pts(); i++) {
      R2Line pedge = in_poly.Line(i);
      for (int j = 0; j < Num_pts(); j++) {
         R2Line bedge = Line(j);
         if (bedge == pedge) {
            cerr << "identical edges " << i << " " << j << "\n";
            in_poly.Print();
            Print();
         }
      }
   }
         
   double s, t;
   int last_e1 = -1, last_e2 = -2;
   while ((untested = untested_edge()) != -1) {
      cut = FALSE;
      for (int i = 0; i < __e_count; i++) {
         if (e_list[i].tested == FALSE && 
             (e_list[i].polygon == -1 ||
              e_list[i].polygon != e_list[untested].polygon) ) {
            // See if they\'re already adjacent edges
            int v1 = e_list[i].v1, v2 = e_list[i].v2;
            int w1 = e_list[untested].v1, w2 = e_list[untested].v2;
            if (v1 == w1 || v1 == w2 || v2 == w1 || v2 == w2)
               continue;
            R2Pt inter;
            WINbool bIntersectResult =
              e_list[i].seg.Intersect(e_list[untested].seg, inter, s, t);

            if (bIntersectResult == TRUE) {
               if (e_list[untested].index == last_e1 
                   && e_list[i].index == last_e2)
                  continue;
               WINbool s_edge = FALSE, t_edge = FALSE;
               if (RNIsZero(1.0 - s) || RNIsZero(s)) 
                  s_edge = TRUE;
               if (RNIsZero(1.0 - t) || RNIsZero(t)) 
                  t_edge = TRUE;
               if (s_edge == TRUE && t_edge == FALSE) {
                  last_e1 = e_list[untested].index;
                  last_e2 = e_list[i].index;
                  split_edge(e_list[untested], inter, e_list[i]);
                  continue;
               }
               if (t_edge == TRUE && s_edge == FALSE) {
                  last_e1 = e_list[untested].index;
                  last_e2 = e_list[i].index;
                  split_edge(e_list[i], inter, e_list[untested]);
                  continue;
               }
               if (s_edge == TRUE || t_edge == TRUE) 
                  continue;
               cut = TRUE;
               intersected = TRUE;
               last_e1 = e_list[untested].index;
               last_e2 = e_list[i].index;
               replace_edge(e_list[i], e_list[untested], inter);
               break;
            }
         }
      }
      if (cut == FALSE)
         e_list[untested].tested = TRUE;
   }
   if (vertex_inside == FALSE && __v_count == (Num_pts() + in_poly.Num_pts()+1))
      return R2Polygon(0);

   if (reconstruct_polygon(intersected) == FALSE) {
      Print();
      in_poly.Print();
      cout.flush();
   }
   // Make a new polygon from data, deleting data
   return Make_polygon();
}

void R2Polygon::ScanConvert( Array<R2Pt_i> &out_aiptIndex ) const
{
    Array< ScanLine > aiScan;
    ScanConvert( aiScan );

    out_aiptIndex.need(0);
    for ( int i = 0; i < aiScan.num(); i++ ) {
        for ( int j = aiScan[i].m_iX1; j <= aiScan[i].m_iX2; j++ ) {
            out_aiptIndex += R2Pt_i( j, aiScan[i].m_iY );
        }
    }
}

class EdgeData {
public:
    int    m_iEdge, m_iYEnd, m_iV1, m_iV2;
    double m_dXIntersect, m_dXIncr;
    double m_dPerc, m_dPercIncr;
    double m_dX0, m_dY0, m_dX1, m_dY1;
};

// A pixel is put in the list if a line passes through it or it's
// contained in the polygon. A pixel occupies +- 0.5 of the point x,y. 
// If this is a boundary pixel the bdry is set to true
void R2Polygon::ScanConvert( Array<ScanLine> &out_aiScan ) const
{
    out_aiScan.need(0);

    if ( m_apts.num() < 3 )
        return;

    Array<int>     aiYSort;
    Array<double>  adYSort;
    double         dYMin = 1e30, dYMax = -1e30;

    for ( int i = 0; i < m_apts.num(); i++ ) {
        const double dY1 = m_apts[i][1];
        const double dY2 = PtWrap(i+1)[1];
        dYMin = WINmin( dY1, dYMin );
        dYMax = WINmax( dY1, dYMax );
        // Add any non-horizontal line
        if ( !RNApproxEqual( dY1, dY2 ) ) {
            aiYSort += i;
            adYSort += WINmin( dY1, dY2 );
        }
    }

    // Non-existant polygon
    if ( aiYSort.num() < 2 ) {
        return;
    }

    // If highest point of polygon is above the center of pixel (y + 0.5)
    // If lowest point of polygon is below the center of pixel
    // Polygon only covers pixel if edge crosses y = 0.5/middle of pixel
    aiYSort.sort( adYSort );
    const int iYFloorMin = (int) dYMin;
    const int iYFloorMax = (int) dYMax;
    const int iYMin = ( dYMin - iYFloorMin ) < 0.5 ? iYFloorMin : iYFloorMin + 1;
    const int iYMax = ( dYMax - iYFloorMax ) < 0.5 ? iYFloorMax - 1 : iYFloorMax;

    int iYCur = 0;
    Array<EdgeData> aoEdgeData;

    // Loop through all vertical lines
    for ( int iY = iYMin; iY <= iYMax; iY++ ) {
        // Remove any edges whose max y values are less than y + 0.5
        for ( int iE = 0; iE < aoEdgeData.num(); iE++ ) {
            if ( aoEdgeData[iE].m_iYEnd < iY ) {
                aoEdgeData.del( iE, 1 );
                iE--;
            }
        }
        // Increment x intersect and percentage
        for ( FORINT iE = 0; iE < aoEdgeData.num(); iE++ ) {
            aoEdgeData[iE].m_dXIntersect += aoEdgeData[iE].m_dXIncr;
            aoEdgeData[iE].m_dPerc += aoEdgeData[iE].m_dPercIncr;
        }

        // Add in any new edges
        // Since we're going from top to bottom, y value has to be
        // in the top half of this pixel, or the bottom half of the previous
        // pixel
        while ( iYCur < aiYSort.num() && adYSort[ iYCur ] < iY + 0.5 ) {
            EdgeData oED;

            const int iEdge = aiYSort[iYCur];
            oED.m_iEdge = iEdge;

            // Segment end points
            const R2Pt &p1 = m_apts[ iEdge ];
            const R2Pt &p2 = PtWrap( iEdge+1 );

            // Start and stop points
            if ( p1[1] > p2[1] ) {
                oED.m_iV1 = (iEdge+1) % m_apts.num();
                oED.m_iV2 = iEdge;
                oED.m_dX0 = p2[0];    oED.m_dY0 = p2[1];
                oED.m_dX1 = p1[0];    oED.m_dY1 = p1[1];
            } else {
                oED.m_iV1 = iEdge;
                oED.m_iV2 = (iEdge+1) % m_apts.num();
                oED.m_dX0 = p1[0];    oED.m_dY0 = p1[1];
                oED.m_dX1 = p2[0];    oED.m_dY1 = p2[1];
            }

            // If edge starts and ends between two pixels, skip it
            // But don't skip the last edge
            if ( oED.m_dY1 < iY + 0.5 && !RNApproxEqual( oED.m_dY1, dYMax ) ) {
                iYCur++;
                continue;
            }
            // Percentages at middle of start/stop pixels
               // Will use this segment for oED.m_iYEnd - iY + 1 pixels
            oED.m_iYEnd = (int) oED.m_dY1;
            oED.m_iYEnd = ( oED.m_dY1 - oED.m_iYEnd ) < 0.5 ? oED.m_iYEnd - 1 : oED.m_iYEnd;

            // Ratio of center of pixel y over entire y length of seg
            // Start at middle of pixel (iY + 0.5)
            // End at middle of pixel (iYEnd + 0.5)
            const double dTStart = ( (iY+0.5) - oED.m_dY0 ) / ( oED.m_dY1 - oED.m_dY0 );
            const double dTEnd = ( (oED.m_iYEnd+0.5) - oED.m_dY0 ) / ( oED.m_dY1 - oED.m_dY0 );
            if ( oED.m_iYEnd - iY < 1 ) {
                // Won't be used anyways (one pixel high line)
                oED.m_dPercIncr = 0.0;
                oED.m_dXIncr = 0.0;
            } else {
                oED.m_dPercIncr = ( dTEnd - dTStart ) / (double) (oED.m_iYEnd - iY);
                oED.m_dXIncr = oED.m_dPercIncr * ( oED.m_dX1 - oED.m_dX0 );
            }
            oED.m_dPerc = dTStart;
            oED.m_dXIntersect = oED.m_dX0 + dTStart * ( oED.m_dX1 - oED.m_dX0 ); 

            aoEdgeData += oED;
            iYCur++;
        }

        // Sort edges in x
        Array<int> aiXSort( aoEdgeData.num() );
        Array<double> adXSort( aoEdgeData.num() );
        for (int i = 0; i < aoEdgeData.num(); i++) {
            aiXSort[i] = i;
            // Clip x values to min max of real edge
            const double dXMin = WINmin( aoEdgeData[i].m_dX0, aoEdgeData[i].m_dX1 );
            const double dXMax = WINmax( aoEdgeData[i].m_dX0, aoEdgeData[i].m_dX1 );
            if ( aoEdgeData[i].m_dXIntersect > dXMax || aoEdgeData[i].m_dXIntersect < dXMin )
                adXSort[i] = WINminmax( aoEdgeData[i].m_dXIntersect, dXMin, dXMax );
            else
                adXSort[i] = aoEdgeData[i].m_dXIntersect;
        }
        aiXSort.sort( adXSort );

        // Write out pixel spans
        for ( FORINT i = 0; i < aiXSort.num()-1; i +=2 ) {
            out_aiScan.add(1);
            ScanLine &oSL = out_aiScan.last();

            oSL.m_iY = iY;

            const EdgeData &oED1 = aoEdgeData[ aiXSort[i] ];
            const EdgeData &oED2 = aoEdgeData[ aiXSort[i+1] ];

            oSL.m_iE1V1 = oED1.m_iV1;
            oSL.m_iE1V2 = oED1.m_iV2;
            oSL.m_iE2V1 = oED2.m_iV1;
            oSL.m_iE2V2 = oED2.m_iV2;

            oSL.m_dE1Perc = oED1.m_dPerc;
            oSL.m_dE2Perc = oED2.m_dPerc;
            const int iXFloorMin = (int) adXSort[i];
            const int iXFloorMax = (int) adXSort[i+1];
            oSL.m_iX1 = ( adXSort[i] - iXFloorMin ) < 0.5 ? iXFloorMin : iXFloorMin + 1;
            oSL.m_iX2 = ( adXSort[i+1] - iXFloorMax ) < 0.5 ? iXFloorMax - 1 : iXFloorMax;
            if ( oSL.m_iX1 > oSL.m_iX2 ) {
                // Very skinny polygon in x - pick the pixel that's closer
                //oSL.m_iX1 = oSL.m_iX2 = (int) ( 0.5 * (adXSort[i] + adXSort[i+1]) + 0.5 );
            }
        }
    }
}


WINbool R2Polygon::Test()    
{
   R2Polygon q(R2Polygon::Unit_polygon(3));
   R2Pt q_pt(0,0);
   R2Pt q_ptres = q.Closest_pt(q_pt);

   R2Polygon q3(3);
   R2Pt q_pt3(0,0);
   R2Pt q_ptres3 = q3.Closest_pt(q_pt3);

   R2Polygon p1(5);
   R2Polygon p2(2), p3(1);

   double phi = M_PI - (2.0 * M_PI) / (double) 5.0;
   double neg_1 = -1;
   
   RNZero(p1[0]);

   for (int i = 1; i < 5; i++) {
      neg_1 = neg_1 * -1;
      p1[i][0] = p1[i-1][0] + neg_1 * cos(phi * i);
      p1[i][1] = p1[i-1][1] + neg_1 * sin(phi * i);
   }

   VERIFY(p1.Valid() == TRUE);
   p2 = p1;
   VERIFY(p1 == p2);

   R2Pt bdry_pt(0,0);
   VERIFY(p1.IsBoundary(bdry_pt));
   bdry_pt[0] = 0.5;
   bdry_pt[1] = 1.0;
   VERIFY(p1.IsBoundary(bdry_pt) == FALSE);

   R3Vec norm = p1.Normal();
   VERIFY(RNApproxEqual(norm[0], 0));
   VERIFY(RNApproxEqual(norm[1], 0));
   VERIFY(RNApproxEqual(norm[2], -1));
   
   R2Pt center = p1.Centroid();
   VERIFY(RNApproxEqual(center[0], 0.5));
   VERIFY(RNApproxEqual(center[2], 0.0));

   p2.Print();
   ofstream out("RNtest.txt", ios::out);
   out.precision(16);
   p1.Write(out);
   out.close();
   ifstream in("RNtest.txt", ios::in);
   p3.Read(in);
   in.close();
   VERIFY(p1 == p3);

   p1 = R2Polygon(4);
   p1 = R2Polygon::Unit_polygon(5);
   
   p2 = R2Polygon(2);
   p3 = R2Polygon(p1);

   VERIFY(p3 == p1);

   VERIFY(p1.Valid() == TRUE);
   p2 = p1;
   VERIFY(p1 == p2);

   VERIFY(p1.IsBoundary(p1.Edge(2)(0.2)));
   bdry_pt = Lerp( p1[0], Lerp(p1[1], p1[2], 0.5), 0.5);
   VERIFY(p1.Inside_winding(bdry_pt) == TRUE);
   VERIFY(p1.Inside(bdry_pt) == TRUE);

   bdry_pt[0] = 0.5;
   bdry_pt[1] = 1.0;
   VERIFY(p1.IsBoundary(bdry_pt) == FALSE);
   VERIFY(p1.Inside_winding(bdry_pt) == FALSE);
   VERIFY(p1.Inside(bdry_pt) == FALSE);
   
   bdry_pt[0] = 0.75;
   bdry_pt[1] = 1.0;
   R2Pt closest_pt = p1.Closest_pt(bdry_pt);

   R2Pt proj_pt;
   R2Line_seg seg = p1.Edge(2);
   proj_pt = seg.Project(bdry_pt);
   VERIFY(ApproxEqual(closest_pt, proj_pt));

   return TRUE;
}

WINbool R2Polygon::InsideQuick( const R2Pt &in_pt ) const
{
    const R2Vec v1( m_apts[2] - m_apts[1] );
    const R2Vec v2( m_apts[1] - m_apts[0] );

    const double dZ = v2[0] * v1[1] - v2[1] * v1[0];
    const double dNorm = ( dZ > 0.0 ) ? 1 : -1 ;
    
    for (int i = 0; i < m_apts.num(); i++) {
        const R2Pt &p2 = m_apts.wrap(i+1);
        const R2Pt &p1 = m_apts[i];
        const R2Vec &e_last = p2 - p1;
        const R2Vec &e = in_pt - p2;
        
        if ( ApproxEqual( in_pt, p1 ) ) 
            return TRUE;
        // Cross[2]
        if ( (e_last[0] * e[1] - e_last[1] * e[0] ) * dNorm < -RNEpsilon_d) {
            return FALSE;
        }
    }

    return TRUE;
}

WINbool R2Polygon::Overlaps( const R2Polygon &in_poly ) const
{
    for ( int i = 0; i < in_poly.Num_pts(); i++ )
        if ( Inside( in_poly[i] ) == TRUE )
            return TRUE;

    for ( FORINT i = 0; i < Num_pts(); i++ )
        if ( in_poly.Inside( m_apts[i] ) == TRUE )
            return TRUE;

    R2Pt ptOut;
    double dS, dT;
    for ( FORINT i = 0; i < in_poly.Num_pts(); i++ ) {
        const R2Line_seg seg = in_poly.Edge(i);
        for ( int j = 0; j < Num_pts(); j++ ) {
            if ( Edge(j).Intersect( seg, ptOut, dS, dT ) == TRUE )
                return TRUE;
        }
    }
            
    return FALSE;
}

R2Polygon R2Polygon::IntersectConvex( const R2Polygon &in_poly ) const
{
    R2Polygon poly;

    poly.Clear();
    if ( in_poly.Num_pts() < 3 || Num_pts() < 3 )
        return poly;

    Array<R2Pt> aptInter;
    double dS, dT;
    R2Pt ptOut, ptCenter(0,0);
    for ( int i = 0; i < m_apts.num(); i++ ) {
        const R2Line_seg seg = Edge( i );
        for ( int j = 0; j < in_poly.Num_pts(); j++ ) {
            const R2Line_seg segOth = in_poly.Edge( j );

            if ( seg.Intersect( segOth, ptOut, dS, dT ) ) {
                aptInter += ptOut;
                ptCenter[0] += ptOut[0];
                ptCenter[1] += ptOut[1];
            }
        }
    }
    if ( aptInter.num() == 0 ) {
        WINbool bInside = TRUE;
        for ( int i = 0; i < m_apts.num(); i++ ) {
            if ( in_poly.InsideQuick( m_apts[i] ) == FALSE ) {
                bInside = FALSE;
                break;
            }
        }
        if ( bInside == TRUE )
            return *this;

        bInside = TRUE;
        for ( FORINT i = 0; i < in_poly.Num_pts(); i++ ) {
            if ( InsideQuick( in_poly[i] ) == FALSE ) {
                bInside = FALSE;
                break;
            }
        }
        if ( bInside == TRUE )
            return in_poly;

        return poly;
    }

    for ( FORINT i = 0; i < m_apts.num(); i++ ) {
        if ( in_poly.InsideQuick( m_apts[i] ) ) {
            aptInter += m_apts[i];
            ptCenter[0] += m_apts[i][0];
            ptCenter[1] += m_apts[i][1];
        }
    }
    for ( FORINT i = 0; i < in_poly.Num_pts(); i++ ) {
        if ( InsideQuick( in_poly[i] ) ) {
            aptInter += in_poly[i];
            ptCenter[0] += in_poly[i][0];
            ptCenter[1] += in_poly[i][1];
        }
    }

    if ( aptInter.num() == 2 )
        ASSERT( FALSE );

    Array<double> adDot( aptInter.num() );
    ptCenter[0] /= (double) aptInter.num(); 
    ptCenter[1] /= (double) aptInter.num();

    for ( FORINT i = 0; i < aptInter.num(); i++ ) {
        adDot[i] = atan2( aptInter[i][1] - ptCenter[1], aptInter[i][0] - ptCenter[0] );
    }

    aptInter.sort( adDot );

    for ( FORINT i = 0; i < adDot.num(); i++ ) {
        if ( ApproxEqual( aptInter[i], aptInter.wrap(i+1) ) ) {
            adDot.del(i, 1);
            aptInter.del(i, 1);
            i--;
        }
    }
    R2Polygon polyRet( aptInter.num() );

    polyRet.m_apts = aptInter;

    return polyRet;
}

R2BoxedPolygon 
R2BoxedPolygon::IntersectConvex(const R2BoxedPolygon &in_polygon) const
{
    R2BoxedPolygon poly(0);

    if ( in_polygon.WMin() > WMax() ) return poly;
    if ( in_polygon.WMax() < WMin() ) return poly;
    if ( in_polygon.HMin() > HMax() ) return poly;
    if ( in_polygon.HMax() < HMin() ) return poly;

    poly = R2Polygon::IntersectConvex( in_polygon );
    poly.Set_dim();

    return poly;
}

WINbool 
R2BoxedPolygon::Overlaps(const R2BoxedPolygon &in_polygon) const
{
    if ( in_polygon.WMin() > WMax() ) return FALSE;
    if ( in_polygon.WMax() < WMin() ) return FALSE;
    if ( in_polygon.HMin() > HMax() ) return FALSE;
    if ( in_polygon.HMax() < HMin() ) return FALSE;

    return R2Polygon::Overlaps( in_polygon );
}

R2Polygon R2Polygon::Blend( const Array<R2Polygon> & in_apoly, 
                            const Array<R2Pt>      & in_aptCenterMe, 
                            const Array<double>    & in_adWeights )
{
    Array<double> adAngle;
    Array<R2Pt_i> aiptFromWhich;
    
    for ( int i = 0; i < in_apoly.num(); i++ ) {
        for ( int j = 0; j < in_apoly[i].Num_pts(); j++ ) {
            const R2Vec vec = in_apoly[i][j] - in_aptCenterMe[i];
            if ( RNIsZero( ::Length( vec ) ) ) {
                continue;
            }
            const double dAng = atan2( vec[1], vec[0] );
            bool bFound = false;
            for ( int k = 0; k < adAngle.num(); k++ ) {
                if ( fabs( adAngle[k] - dAng ) < 1e-6 ) { 
                    bFound = true;
                    break;
                }
            }
            if ( bFound == false ) {
                adAngle += dAng;
                aiptFromWhich += R2Pt_i( i, j );
            }
        }
    }
    aiptFromWhich.sort( adAngle );
    
    R2Polygon polyOut( aiptFromWhich.num() );
    
    Array<R2Pt> aptFind;
    for ( int i = 0; i < adAngle.num(); i++ ) {
        R2Pt ptBlend(0.0, 0.0);
        double dBlend = 0.0;
        for ( int j = 0; j < in_apoly.num(); j++ ) {
            if ( aiptFromWhich[i][0] == j ) {
                aptFind.need(1);
                aptFind[0] = in_apoly[ aiptFromWhich[i][0] ][ aiptFromWhich[i][1] ];
            } else {
                in_apoly[j].IntersectRay( in_aptCenterMe[j], R2Vec( cos( adAngle[i] ), sin( adAngle[i] ) ), aptFind );
            }
            if ( aptFind.num() ) {
                ptBlend[0] += aptFind[0][0] * in_adWeights[j];
                ptBlend[1] += aptFind[0][1] * in_adWeights[j];
                dBlend += in_adWeights[j];
            }
        }
        if ( !RNIsZero( dBlend ) ) {
            ptBlend[0] /= dBlend;
            ptBlend[1] /= dBlend;
        }
        polyOut[i] = ptBlend;
    }
    
    return polyOut;

}


R2Polygon R2Polygon::Morph( const R2Polygon &in_poly, const R2Pt &in_ptCenterMe, const R2Pt &in_ptCenterPoly, const double in_dT ) const
{
    Array<R2Polygon> apoly(2);
    Array<R2Pt> aptCenter(2);
    Array<double> adTs(2);
    apoly[0] = *this;
    apoly[1] = in_poly;
    aptCenter[0] = in_ptCenterMe;
    aptCenter[1] = in_ptCenterPoly;
    adTs[0] = (1.0 - in_dT);
    adTs[1] = in_dT;
               
    return Blend( apoly, aptCenter, adTs );
}
