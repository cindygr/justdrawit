#include "StdAfx.H"
#include <utils/Rn_Ellipse.H>
#include <utils/Rn_Polygon.H>

R2Vec R2Ellipse::XAxis() const
{
    const R2Vec vec = R3Matrix::Rotation2D( m_dTheta ) * R2Vec(m_dA, 0.0);
    return vec;
}

///
R2Vec R2Ellipse::YAxis() const
{
    const R2Vec vec = R3Matrix::Rotation2D( m_dTheta ) * R2Vec(0.0, m_dB);
    return vec;
}

R3Matrix R2Ellipse::ToNormal() const
{
    const R3Matrix mat = R3Matrix::Rotation2D( m_dTheta ) * R3Matrix::Translation( R2Pt(0,0) - m_ptCenter );

    return mat;
}

R3Matrix R2Ellipse::FromNormal() const
{
    const R3Matrix mat =  R3Matrix::Translation( m_ptCenter - R2Pt(0,0) ) * R3Matrix::Rotation2D( -m_dTheta );

    return mat;
}

// Point where (-y,x) dot (q - q') = 0
R2Pt R2Ellipse::ClosestPt( const R2Pt &in_pt ) const 
{
    const R2Pt ptMap = ToNormal() * in_pt;

    // <-b sin theta, a cos theta, ptMap -  a cos theta, b sin theta >
    // -b sin(t) * (p[0] - a cos(t) ) + a cos(t) * (p[1] - b sin(t)) = 0
    // a b cos(t) sin(t) - a b cos(t) sin(t) - b sin(t) p[0] + a cos(t) p[1] = 0
    // -b p[0] sin(t) = - a cos(t) p[1]
    // tan(t) =(b p[0]) / (a p[1]) 

    const double dTheta = atan2( m_dA * ptMap[1], m_dB * ptMap[0] );
    const R2Pt ptBack( m_dA * cos( dTheta ), m_dB * sin( dTheta ) );

    return FromNormal() * ptBack;
}

R2Pt R2Ellipse::operator()( const double dT ) const 
{
    const double dA2 = pow(m_dA, 2);
    const double dB2 = pow(m_dB, 2);
    const R2Pt pt( cos( dT ), sin( dT ) );
    const double dL = dA2 * dB2 / ( dB2 * pow( pt[0], 2 ) + dA2 * pow( pt[1], 2 ) ); 
    const R2Pt ptRet( sqrt( dL ) * cos( dT + m_dTheta ) + m_ptCenter[0],
                      sqrt( dL ) * sin( dT + m_dTheta ) + m_ptCenter[1] );
    return ptRet;
}

/// Generate evenly spaced points, number determined by io_apt.num
void R2Ellipse::Points( Array<R2Pt> &io_apt, const double in_dTS, const double in_dTE ) const
{
    if ( io_apt.num() == 0 )
        io_apt.need( 10 );

    const double dDiv = (in_dTE - in_dTS) / io_apt.num();

    for (int i = 0; i < io_apt.num(); i++) {
        const double dT = in_dTS + dDiv * i;
        io_apt[i] = (*this)(dT);
    }
}


WINbool R2Ellipse::Overlap( const R2Ellipse &in_ell, const int in_iDiv, const double in_dTS, const double in_dTE ) const
{
    if ( in_ell.Inside( Center() ) )
        return TRUE;
    if ( Inside( in_ell.Center() ) )
        return TRUE;

    const double dLenMid = Length( Center() - in_ell.Center() );
    if ( dLenMid > WINmax( in_ell.XRadius(), in_ell.YRadius() ) + WINmax( XRadius(), YRadius() ) )
        return FALSE;

    Array<R2Pt> apt(in_iDiv);
    Points( apt, in_dTS, in_dTE );
    if ( RNIsZero( in_dTS ) && RNApproxEqual( in_dTE, 2.0 * M_PI ) ) {
        const R2Pt ptFirst = apt[0];
        apt += ptFirst;
    }

    for ( int i = 0; i < apt.num(); i++ )
        if ( in_ell.Inside( apt[i] ) )
            return TRUE;

    R2Line_seg segC, segA, segB;
    R2Pt ptIntersect, ptIntersect2;

    for ( FORINT i = 0; i < apt.num() - 1; i++ ) {
        const R2Vec vec( 2.0 * apt[i][0] + m_adCoef[1] / m_adCoef[0] * apt[i][1] + m_adCoef[3] / m_adCoef[0],
                         2.0 * m_adCoef[2] / m_adCoef[0] * apt[i][1] + m_adCoef[1] / m_adCoef[0] * apt[i][0] + m_adCoef[4] / m_adCoef[0] );
        const R2Vec vecNext( 2.0 * m_adCoef[0] * apt[i+1][0] + m_adCoef[1] * apt[i+1][1] + m_adCoef[3],
                             2.0 * m_adCoef[2] * apt[i+1][1] + m_adCoef[1] * apt[i+1][0] + m_adCoef[4] );
        const R2Line lineRim( apt[i], R2Vec( -vec[1], vec[0] ) );
        const R2Line lineRimNext( apt[i+1], R2Vec( -vecNext[1], vecNext[0] ) );
        VERIFY( lineRim.Intersect( lineRimNext, ptIntersect ) );


        segC = R2Line_seg( apt[i], apt[i+1] );
        segA = R2Line_seg( apt[i], ptIntersect );
        segB = R2Line_seg( ptIntersect, apt[i+1] );

        // Intersection is inside this ellipse
        if ( in_ell.Intersect( segC, ptIntersect, ptIntersect2 ) )
            return TRUE;

        // See if segment crosses ellipse
        if ( in_ell.Intersect( segA, ptIntersect, ptIntersect2 ) ||
             in_ell.Intersect( segB, ptIntersect, ptIntersect2 ) ) {

            // Close enough
            if ( in_dTE - in_dTS < 0.01 )
                return FALSE;

            const double dDiv = (in_dTE - in_dTS) / (double) in_iDiv;
            if ( Overlap( in_ell, 4, in_dTS + i * dDiv, in_dTS + (i+1) * dDiv ) )
                return TRUE;
        }
    }

    return FALSE;
}

WINbool R2Ellipse::Overlap( const R2Polygon &in_poly ) const
{
    if ( Inside( in_poly.Centroid() ) )
        return TRUE;

    for ( int i = 0; i < in_poly.Num_pts(); i++ ) {
        if ( Inside( in_poly[i] ) )
            return TRUE;
    }

    R2Pt pt1, pt2;
    for ( FORINT i = 0; i < in_poly.Num_pts(); i++ ) {
        if ( Intersect( in_poly.Edge(i), pt1, pt2 ) )
            return TRUE;
    }

    return FALSE;

}

///
WINbool R2Ellipse::Overlap( const R2Ellipse &in_ell ) const
{
    if ( in_ell.Inside( Center() ) )
        return TRUE;
    if ( Inside( in_ell.Center() ) )
        return TRUE;

    /// This function is numerically unstable.
    /// I don't know how to fix it, yet
    ASSERT(FALSE);

    // Coefficients of polynomial of intersection
    // R(y) = u0 + u1 y + u2 y^2 + u3 y^3 + u4 y^4
    double adMe[6], adOth[6];
    for ( int i = 0; i < 6; i++ ) {
        adMe[i] = m_adCoef[i];// / m_adCoef[i];
        adOth[i] = in_ell.m_adCoef[i];// / in_ell.m_adCoef[0];
    }

    const double v0  = adMe[0] * adOth[1] - adOth[0] * adMe[1];
    const double v1  = adMe[0] * adOth[2] - adOth[0] * adMe[2];
    const double v2  = adMe[0] * adOth[3] - adOth[0] * adMe[3];
    const double v3  = adMe[0] * adOth[4] - adOth[0] * adMe[4];
    const double v4  = adMe[0] * adOth[5] - adOth[0] * adMe[5];
    const double v5  = adMe[1] * adOth[2] - adOth[1] * adMe[2];
    const double v6  = adMe[1] * adOth[4] - adOth[1] * adMe[4];
    const double v7  = adMe[1] * adOth[5] - adOth[1] * adMe[5];
    const double v8  = adMe[2] * adOth[3] - adOth[2] * adMe[3];
    const double v9  = adMe[3] * adOth[4] - adOth[3] * adMe[4];
    const double v10 = adMe[3] * adOth[5] - adOth[3] * adMe[5];

    Array<double> adPoly(5);
    adPoly[0] = v2 * v10 - pow(v4,2);
    adPoly[1] = v0 * v10 + v2 * (v7 + v9) - 2.0 * v3 * v4;
    adPoly[2] = v0 * (v7 + v9) + v2 * (v6 - v8) - pow(v3, 2) - 2.0 * v1 * v4;
    adPoly[3] = v0 * (v6 - v8) + v2 * v5 - 2.0 * v1 * v3;
    adPoly[4] = v0 * v5 - pow(v1,2);

    Array< RNPolynomial > apoly(2);
    apoly[0] = RNPolynomial( adPoly );
    apoly[1] = apoly[0].Differentiate();
    RNPolynomial polyRem;
    for ( FORINT i = 2; i < 5; i++ ) {
        const RNPolynomial polyDiv = apoly[i-2].Divide( apoly[i-1], polyRem );

        apoly += -polyRem;
    }
    int iS = 0, iE = 0;

    const double dYMin = WINmin( Center()[1] - XRadius() - YRadius(), in_ell.Center()[1] - in_ell.XRadius() - in_ell.YRadius() );
    const double dYMax = WINmax( Center()[1] + XRadius() + YRadius(), in_ell.Center()[1] + in_ell.XRadius() + in_ell.YRadius() );

    //for ( FORINT i = 0; i < apoly.num(); i++ )
    //    TRACE("%f %f\n", apoly[i]( dYMin ), apoly[i]( dYMax ) );
    //TRACE("\n");
    int iSignS = apoly[0]( dYMin ) < 0 ? -1 : 1;
    int iSignE = apoly[1]( dYMax ) < 0 ? -1 : 1;
    for ( FORINT i = 1; i < apoly.num(); i++ ) {
        const int iSignSi = apoly[i]( dYMin ) < 0 ? -1 : 1;
        if ( iSignS != iSignSi ) {
            iS++;
            iSignS = iSignSi;
        }

        if ( i + 1 < apoly.num() ) {
            const int iSignEi = apoly[i+1]( dYMax ) < 0 ? -1 : 1;
            if ( iSignE != iSignEi ) {
                iE++;
                iSignE = iSignEi;
            }
        }
    }
    const int iNRoot = iS - iE;

    if ( iNRoot > 0 )
        return TRUE;

    return FALSE;
}

///
int R2Ellipse::Intersect( const R2Line &in_line, R2Pt &out_pt1, R2Pt &out_pt2 ) const
{
    const double da = m_adCoef[0];
    const double db = m_adCoef[1];
    const double dc = m_adCoef[2];
    const double dd = m_adCoef[3];
    const double de = m_adCoef[4];
    const double df = m_adCoef[5];
    double dA, dB, dC;
    in_line.Implicit( dA, dB, dC );

    if ( RNIsZero( dA ) ) {
        const double dSqrt = pow(dB,2) * ( pow(db * dC - dB * dd,2) - 4.0 * da *
                                           ( dc * pow(dC,2) + dB * ( - (dC * de) + dB * df ) ) );
        const double dDenom = 2.0 * da * pow(dB,2);
        if ( dSqrt < 0.0 || RNIsZero( dDenom ) ) {
            out_pt1 = R2Pt( 1e30, 1e30 );
            out_pt2 = R2Pt( 1e30, 1e30 );

            return 0;
        }
        out_pt1[0] = -( -(db * dB * dC) + pow(dB,2) * dd + sqrt(dSqrt) ) / dDenom;
        out_pt1[1] = - dC / dB;
        out_pt2[0] = (db * dB * dC - pow(dB,2) * dd + sqrt(dSqrt)) / dDenom;
        out_pt2[1] = - dC / dB;

        if ( ::ApproxEqual( out_pt1, out_pt2, 1e-16 ) )
            return 1;
        return 2;
    }

    const double dT1 = -( dA * db * dB) + da * pow(dB, 2) + pow(dA,2) * dc;

    const double dSqrt = pow(2.0 * da * dB * dC - dA * (db * dC + dB * dd) + pow(dA,2) * de, 2)-
                         4.0 * dT1 *
                         (da * pow(dC,2) + dA * (-(dC * dd) + dA * df) );
    const double dDenom = 2.0 * dA * dT1;
    if ( dSqrt < 0 || RNIsZero( dT1 ) ) {
        out_pt1 = R2Pt( 1e30, 1e30 );
        out_pt2 = R2Pt( 1e30, 1e30 );

        return 0;
    } else {
        out_pt1[0] = (dA * dB * (db * dC - dB * dd) + pow( dA, 2 ) * (-2.0 * dc * dC + dB * de) -
                    dB * sqrt(dSqrt)) / dDenom;
        out_pt1[1] = (dA * db * dC - 2.0 * da * dB * dC + dA * dB * dd - pow(dA,2) * de +
                    sqrt(dSqrt) ) / (2.0 * dT1);

        out_pt2[0] = (dA * dB * (db * dC - dB * dd) + pow( dA, 2 ) * (-2.0 * dc * dC + dB * de) +
                    dB * sqrt(dSqrt)) / dDenom;
        out_pt2[1] = -(2.0 * da * dB * dC - dA * (db * dC + dB * dd) + pow(dA,2) * de +
                    sqrt(dSqrt) ) / (2.0 * dT1);
    }

    if ( ::ApproxEqual( out_pt1, out_pt2, 1e-12 ) )
        return 1;

    return 2;
}

int R2Ellipse::Intersect( const R2Line_seg &in_line, R2Pt &out_pt1, R2Pt &out_pt2 ) const
{
    const R2Vec vec = in_line.P2() - in_line.P1();
	const double dLenVec = LengthSq( vec );
	if ( RNIsZero( dLenVec ) ) {
		return 0;
	}

    const R2Line line( in_line.P1(), in_line.P2() );

    const int iInter = Intersect(line, out_pt1, out_pt2);
    if ( iInter == 0 )
        return 0;

    const double dT1 = Dot( out_pt1 - line.Pt(), vec ) / (dLenVec);
    const double dT2 = Dot( out_pt2 - line.Pt(), vec ) / (dLenVec);

	if ( fabs( dT1 - 0.5 ) < 0.5 && fabs( dT2 - 0.5 ) < 0.5 )
        return 2;

    if ( fabs( dT1 - 0.5 ) < 0.5 )
        return 1;

    if ( fabs( dT2 - 0.5 ) < 0.5 ) {
        out_pt1 = out_pt2;
        return 1;
    }

    return 0;
}

WINbool R2Ellipse::XFromY( const double in_dY, double &out_dX1, double &out_dX2 ) const
{
    const double dSq = pow( m_adCoef[3] + m_adCoef[1] * in_dY, 2) -
                       4.0 * m_adCoef[0] * ( m_adCoef[5] + m_adCoef[4] * in_dY + m_adCoef[2] * pow(in_dY,2));
    if ( dSq < 0.0 ) {
        return FALSE;
    } else {
        out_dX1 = (-m_adCoef[3] - m_adCoef[1] * in_dY - sqrt(dSq) ) / (2.0 * m_adCoef[0]);
        out_dX2 = (-m_adCoef[3] - m_adCoef[1] * in_dY + sqrt(dSq) ) / (2.0 * m_adCoef[0]);
    }
    return TRUE;
}

///
WINbool R2Ellipse::YFromX( const double in_dX, double &out_dY1, double &out_dY2 ) const
{
    const double dSq = pow( m_adCoef[4] + m_adCoef[1] * in_dX, 2) -
                       4.0 * m_adCoef[2] * ( m_adCoef[5] + m_adCoef[3] * in_dX + m_adCoef[0] * pow(in_dX,2));
    if ( dSq < 0.0 ) {
        return FALSE;
    } else {
        out_dY1 = (-m_adCoef[4] - m_adCoef[1] * in_dX - sqrt(dSq) ) / (2.0 * m_adCoef[2]);
        out_dY2 = (-m_adCoef[4] - m_adCoef[1] * in_dX + sqrt(dSq) ) / (2.0 * m_adCoef[2]);
    }
    return TRUE;
}

WINbool R2Ellipse::IsEllipse() const
{
    const double dDet = pow( m_adCoef[1], 2 ) - 4.0 * m_adCoef[0] * m_adCoef[2];
    if ( dDet > 0.0 )
        return FALSE;

    double dY1, dY2;

    if ( YFromX( m_ptCenter[0], dY1, dY2 ) == FALSE )
        return FALSE;

    return TRUE;
}

// (x - x0)^2
R2Ellipse::R2Ellipse( const double in_dA, 
                      const double in_dB, 
                      const double in_dC, 
                      const double in_dD, 
                      const double in_dE, 
                      const double in_dF)
{
    m_adCoef[0] = in_dA;
    m_adCoef[1] = in_dB;
    m_adCoef[2] = in_dC;
    m_adCoef[3] = in_dD;
    m_adCoef[4] = in_dE;
    m_adCoef[5] = in_dF;

    const double dDet = pow( m_adCoef[1], 2 ) - 4.0 * m_adCoef[0] * m_adCoef[2];

    if ( dDet > 0.0 ) 
         TRACE("Bad ellipse\n");
    else
        SetCenterTheta();
}


void R2Ellipse::SetCenterTheta()
{
    m_ptCenter = R2Pt(0,0);
    m_dTheta = 0.0;
    m_dA = 1.0;
    m_dB = 1.0;

    double dDet = 0.0;
    const R2Vec tmp_u(m_adCoef[0], m_adCoef[1] * 0.5);
    const R2Vec tmp_v(m_adCoef[1] * 0.5, m_adCoef[2]);
    const R2Matrix matAB(tmp_u,tmp_v);

    const R2Matrix matInv = matAB.Inverse( dDet );

    m_ptCenter = matInv * R2Pt( -m_adCoef[3] / 2.0, -m_adCoef[4] / 2.0 );

    const double dCenter = (*this)( m_ptCenter );
    // Only solutions are complex
    if ( dCenter > 0.0 ) {
        m_dA = m_dB = 1e30;
        
        return;
    }

    //const R3Vec tmp_u0(m_adCoef[0], m_adCoef[1] * 0.5, m_adCoef[3] * 0.5);
    //const R3Vec tmp_u1(m_adCoef[1] * 0.5, m_adCoef[2], m_adCoef[4] * 0.5);
    //const R3Vec tmp_u2(m_adCoef[3]*0.5, m_adCoef[4] * 0.5, m_adCoef[5] * 0.5);
    //const R3Matrix matRSRInv(tmp_u0, tmp_u1, tmp_u2);

    if ( RNIsZero( m_adCoef[1], 1e-12 ) ) {
        m_dTheta = 0.0;

    } else {
        m_dTheta = -( -M_PI / 2.0 - 0.5 * atan2( m_adCoef[1], m_adCoef[0] - m_adCoef[2] ) );
    }

    R2Pt pt1, pt2, pt3, pt4;
    const R2Line line1( m_ptCenter, R2Vec( cos( m_dTheta ), sin( m_dTheta ) ) );
    const R2Line line2( m_ptCenter, R2Vec( -sin( m_dTheta ), cos( m_dTheta ) ) );
    if ( Intersect( line1, pt1, pt2 ) && Intersect( line2, pt3, pt4 ) ) {
        m_dA = Length( pt1 - m_ptCenter );
        m_dB = Length( pt3 - m_ptCenter );
        return;
    }

    const double dC = cos( m_dTheta );
    const double dS = sin( m_dTheta );
    const double dC2 = pow( dC, 2 );
    const double dS2 = pow( dS, 2 );
    const double dCS = dC * dS;

    const double dAu = m_adCoef[3] * dC + m_adCoef[4] * dS;
    const double dAv = m_adCoef[4] * dC + m_adCoef[3] * dS;

    const double dAuu = m_adCoef[0] * dC2 +
                        m_adCoef[2] * dS2 +
                        m_adCoef[1] * dCS;

    const double dAvv = m_adCoef[2] * dC2 +
                        m_adCoef[0] * dS2 -
                        m_adCoef[1] * dCS;

    //const double duCenter = - dAu / (2.0 * dAuu);
    //const double dvCenter = - dAv / (2.0 * dAvv);

    const double dwCenter = m_adCoef[5] - pow(dAu, 2) / (4.0 * dAuu) - pow(dAv, 2) / (4.0 * dAvv);

    const double dRu = -dwCenter / dAuu;
    const double dRv = -dwCenter / dAvv;

    m_dA = sqrt( fabs( dRu ) );
    m_dB = sqrt( fabs( dRv ) );
}

void R2Ellipse::SetImplicit()
{
    const double dC = cos( m_dTheta );
    const double dS = sin( m_dTheta );
    const double dA2 = pow( m_dA, 2 );
    const double dB2 = pow( m_dB, 2 );
    const double dX0 = m_ptCenter[0];
    const double dY0 = m_ptCenter[1];

    m_adCoef[0] = dB2 * pow(dC,2) + dA2 * pow(dS,2);
    m_adCoef[1] = 2.0 * dB2 * dC * dS -
                  2.0 * dA2 * dC * dS;
    m_adCoef[2] = dB2 * pow(dS,2) + dA2 * pow(dC,2);

    m_adCoef[3] = 2.0 * dB2 * ( -pow(dC,2) * dX0 - dY0 * dC * dS ) +
                  2.0 * dA2 * ( -pow(dS,2) * dX0 + dY0 * dC * dS );
    m_adCoef[4] = 2.0 * dB2 * ( -pow(dS,2) * dY0 - dX0 * dC * dS ) +
                  2.0 * dA2 * ( -pow(dC,2) * dY0 + dX0 * dC * dS );

    m_adCoef[5] = pow(dC,2) * (dB2 * pow(dX0,2) + dA2 * pow(dY0,2)) +
                  pow(dS,2) * (dB2 * pow(dY0,2) + dA2 * pow(dX0,2)) + 
                  sin( m_dTheta * 2.0 ) * ( dB2 * dX0 * dY0 - dA2 * dX0 * dY0 ) -
                  dA2 * dB2;

    /*
    double dSum = 0.0;
    for ( int i = 0; i < 6; i++ )
        dSum += m_adCoef[i];
    
    if ( !RNIsZero(dSum) ) {
        for ( int i = 0; i < 6; i++ )
            m_adCoef[i] /= dSum;
    }
     */
    
    ASSERT( pow( m_adCoef[1], 2 ) - 4.0 * m_adCoef[0] * m_adCoef[2] < 0.0 );
}

/* -------------------------------------------------------------------------
 * DESCR   :	Make a Ellipse from a Ellipse
 * (x - x0)/ w
 * ------------------------------------------------------------------------- */
R2Ellipse::R2Ellipse(const R2Pt &in_ptCenter, const double in_dA, const double in_dB, const double in_dRot )
    : m_dA(in_dA), m_dB( in_dB ), m_dTheta(in_dRot), m_ptCenter(in_ptCenter)
{
    const int iDiv = (int) (m_dTheta / (2.0 * M_PI));

    m_dTheta = m_dTheta - iDiv * 2.0 * M_PI;
    SetImplicit();
}

R2Ellipse::R2Ellipse(const double in_dA, const double in_dB) : m_dA(in_dA), m_dB( in_dB )
{
    RNZero(m_ptCenter);
    m_dTheta = 0;

    SetImplicit();
}

void R2Ellipse::Print() const 
{
    cout << "Ellipse dim " << Dim() << " radius " << m_dA << " " << m_dB << " theta " << m_dTheta << "  ";
    cout << "Center: " << Center() << "\n";

}

void R2Ellipse::Write(ofstream &out) const 
{
    out << Center() << " " << m_dA << " " << m_dB << " " << m_dTheta << "\n";

    for ( int i = 0; i < 6; i++ )
        out << m_adCoef[i] << " ";
    out << "\n";
}

void R2Ellipse::WriteBinary(ofstream &out) const 
{
    out.write( (const char *) &m_ptCenter[0], 2 * sizeof(double) );
    out.write( (const char *) &m_dA, sizeof(double) );
    out.write( (const char *) &m_dB, sizeof(double) );
    out.write( (const char *) &m_dTheta, sizeof(double) );
    out.write( (const char *) &m_adCoef[0], 6 * sizeof(double) );
}

WINbool R2Ellipse::Read(ifstream &in) 
{
    WINbool bRes = m_ptCenter.Read( in );
    in >> m_dA >>  m_dB >> m_dTheta;
    for ( int i = 0; i < 6; i++ )
        in >> m_adCoef[i];

    return (bRes == TRUE && in.good()) ? TRUE : FALSE;
}

WINbool R2Ellipse::ReadBinary(ifstream &in) 
{
    in.read( (char *) &m_ptCenter[0], 2 * sizeof(double) );
    in.read( (char *) &m_dA, sizeof(double) );
    in.read( (char *) &m_dB, sizeof(double) );
    in.read( (char *) &m_dTheta, sizeof(double) );
    in.read( (char *) &m_adCoef[0], 6 * sizeof(double) );

    return (in.good()) ? TRUE : FALSE;
}


WINbool R2Ellipse::Test()
{
    R2Ellipse ell( R2Pt(0,0), 1.0, 2.0, 0.0 );
    Array<R2Pt> apt(16);
    ell.Points(apt);
    for ( int i = 0; i < apt.num(); i++ ) {
        ASSERT( RNIsZero( ell( apt[i] ), 1e-6 ) );
    }
    ASSERT( ell( R2Pt(0,0) ) < 0.0 );
    ASSERT( ell( R2Pt(10,10) ) > 0.0 );

    R2Line line( R2Pt(0,0), R2Vec(0.1, 0.2) );

    ell = R2Ellipse( R2Pt(0.1, 0.3), 1.1, 2.3, 0.25 );
    R2Pt pt1, pt2;
    VERIFY( ell.Intersect(line, pt1, pt2) );
    ASSERT( RNIsZero( ell( pt1 ), 1e-12 ) );
    ASSERT( RNIsZero( ell( pt2 ), 1e-12 ) );
    ASSERT( line.IsPtOnLine( pt1 ) );
    ASSERT( line.IsPtOnLine( pt2 ) );

    line = R2Line( R2Pt(10, 10), R2Vec( 1.0, 0.01 ) );
    VERIFY( ell.Intersect(line, pt1, pt2) == FALSE );

    R2Ellipse ellFar( R2Pt(10.0, 10.0), 0.5, 0.75, 0.0 );
    R2Ellipse ellClose( R2Pt(0.1, 0.2), 1.0, 2.0, M_PI );
    ASSERT( ellFar.Overlap( ell ) == FALSE );
    ASSERT( ellClose.Overlap( ell ) == TRUE );

    R2Ellipse ellIm( ell.Coefs(0), ell.Coefs(1), ell.Coefs(2), ell.Coefs(3), ell.Coefs(4), ell.Coefs(5) );

    ASSERT( ApproxEqual( ell, ellIm, 1e-12 ) );

    // Check with no center shift, rotation
    for ( int iI = 0; iI < 10; iI++ ) {
        ell = R2Ellipse( R2Pt( 0.0, 0.0), 0.1 + ( rand() % 13 ) * 0.15, 0.01 + ( rand() % 6 ) * 0.5, 0.0 );
        ellIm = R2Ellipse( ell.Coefs(0), ell.Coefs(1), ell.Coefs(2), ell.Coefs(3), ell.Coefs(4), ell.Coefs(5) );
        ASSERT( ApproxEqual( ell, ellIm, 1e-12 ) );

        ell.Points( apt );
        for ( FORINT i = 0; i < apt.num(); i++ ) {
            ASSERT( RNIsZero( ell( apt[i] ), 1e-12 ) );
            ASSERT( RNIsZero( pow( apt[i][0] - ell.Center()[0], 2 ) / pow( ell.XRadius(), 2 ) +
                                  pow( apt[i][1] - ell.Center()[1], 2 ) / pow( ell.YRadius(), 2 ) - 1.0, 1e-12 ) );
        }
    }

    // Check with no rotation
    for ( FORINT iI = 0; iI < 10; iI++ ) {
        ell = R2Ellipse( R2Pt( -0.5 + ( rand() % 10 ) * 0.15, -0.4 + ( rand() % 12 ) * 0.25), 0.1 + ( rand() % 13 ) * 0.15, 0.01 + ( rand() % 6 ) * 0.5, 0.0 );
        ellIm = R2Ellipse( ell.Coefs(0), ell.Coefs(1), ell.Coefs(2), ell.Coefs(3), ell.Coefs(4), ell.Coefs(5) );
        ASSERT( ApproxEqual( ell, ellIm, 1e-12 ) );

        ellIm.Points( apt );
        for ( FORINT i = 0; i < apt.num(); i++ ) {
            ASSERT( RNIsZero( ell( apt[i] ), 1e-12 ) );
            ASSERT( RNIsZero( pow( apt[i][0] - ell.Center()[0], 2 ) / pow( ell.XRadius(), 2 ) +
							  pow( apt[i][1] - ell.Center()[1], 2 ) / pow( ell.YRadius(), 2 ) - 1.0, 1e-12 ) );
        }
    }

    // Check with all
    for ( FORINT iI = 0; iI < 10; iI++ ) {
        ell = R2Ellipse( R2Pt( 5.5 + ( rand() % 10 ) * 0.15, -0.4 + ( rand() % 12 ) * 0.25), 0.01 + ( rand() % 13 ) * 0.015, 0.01 + ( rand() % 6 ) * 0.05, -M_PI + 4.0 * M_PI * (rand() %100) * 0.1 );
        ellIm = R2Ellipse( ell.Coefs(0), ell.Coefs(1), ell.Coefs(2), ell.Coefs(3), ell.Coefs(4), ell.Coefs(5) );
        ASSERT( ApproxEqual( ell, ellIm, 1e-12 ) );

        const R2Matrix mat = R2Matrix::Rotation( ell.Rotation() );

        ellIm.Points( apt );
        for ( FORINT i = 0; i < apt.num(); i++ ) {


            const R2Vec vec = mat * ( apt[i] - ell.Center() );
            ASSERT( RNIsZero( ell( apt[i] ), 1e-11 ) );
            ASSERT( RNIsZero( pow( vec[0], 2 ) / pow( ell.XRadius(), 2 ) +
						      pow( vec[1], 2 ) / pow( ell.YRadius(), 2 ) - 1.0, 1e-10 ) );
        }
    }

    R2Ellipse ellBack( R2Pt(5.08543, 0.105919), 
                       Length( R2Pt( 5.08543, 0.105919 ) - R2Pt( 5.08007, 0.107153 ) ),
                       Length( R2Pt( 5.08543, 0.105919 ) - R2Pt( 5.08847, 0.103296 ) ), 1.82 );
    for ( FORINT iI = 0; iI < 6; iI++ ) 
        TRACE("%f ", ellBack.Coefs(iI) / ellBack.Coefs(0) );
    TRACE("\n");
    for ( FORINT iI = 0; iI < 10; iI++ ) {
        /*
        ell = R2Ellipse( 0.0626 * (rand() % 100) * 0.01, 
                         0.0656 * (rand() % 100) * 0.01,
                         0.24 * (rand() % 100) * 0.01,
                        -0.64 * (rand() % 100) * 0.01,
                        -0.38 * (rand() % 100) * 0.01,
                         1.65 * (rand() % 100) * 0.01 );
                         */
        ell = R2Ellipse( 0.0626109380924, 
                         0.065649523,
                         0.24366,
                        -0.6437601,
                        -0.385472,
                         1.65731022);
        for ( int j = 0; j < 6; j++ )
            TRACE("%f ", ell.Coefs(j) / ell.Coefs(0));
        TRACE("\n");


        const R2Matrix mat = R2Matrix::Rotation( ell.Rotation() );

        ell.Points( apt );
        for ( FORINT i = 0; i < apt.num(); i++ ) {
            const R2Vec vec = mat * ( apt[i] - ell.Center() );
            ASSERT( RNIsZero( ell( apt[i] ), 1e-12 ) );
            ASSERT( RNIsZero( pow( vec[0], 2 ) / pow( ell.XRadius(), 2 ) +
							  pow( vec[1], 2 ) / pow( ell.YRadius(), 2 ) - 1.0, 1e-12 ) );
        }
    }

    R2Ellipse e1( R2Pt( 0.1, 0.5 ), 2.0, 1.0, M_PI / 4.0 );

    const R3Matrix matTo = e1.ToNormal();
    const R3Matrix matFrom = e1.FromNormal();

    const R2Pt ptCenter = matTo * e1.Center();
    ASSERT( ::ApproxEqual( ptCenter, R2Pt(0,0) ) );

    const R2Pt ptMajor = matFrom * R2Pt( 2.0, 0.0);
    ASSERT( RNIsZero( e1(ptMajor) ) );
    const R2Pt ptMinor = matFrom * R2Pt( 0.0, 1.0);
    ASSERT( RNIsZero( e1(ptMinor), 1e-13 ) );

    apt.need(4);
    e1 = R2Ellipse(  R2Pt( 0.1, 0.5 ), 2.0, 1.0, 0.0 );
    e1.Points( apt );
    ASSERT( ::ApproxEqual( apt[0], R2Pt(2.0 + 0.1, 0.0 + 0.5) ) );
    ASSERT( ::ApproxEqual( apt[1], R2Pt(0.0 + 0.1, 1.0 + 0.5) ) );
    ASSERT( ::ApproxEqual( apt[2], R2Pt(-2.0 + 0.1, 0.0 + 0.5) ) );
    ASSERT( ::ApproxEqual( apt[3], R2Pt(0.0 + 0.1, -1.0 + 0.5) ) );

    apt.need(10);
    e1.Points( apt );
    for (FORINT i = 0; i < apt.num(); i++)
        ASSERT( RNIsZero( e1(apt[i]), 1e-13 ) );

   R2Ellipse s1(1.0), s2(R2Pt(1,1), 1.0, 1.0), s3(s1);

   VERIFY(s1 == s3);

   VERIFY(s1.Area() == M_PI);
   
   s1 = s2;
   VERIFY(s1 == s2);
   s1 = R2Ellipse(1.0, 1.0);
   s3 = R2Ellipse(3.0, 1.0);
   s2 = R2Ellipse(R2Pt(0.1, 0), 3.0, 1.0);

   VERIFY(s1.Dim() == 2);
   VERIFY(s1.XRadius() == 1.0);
   VERIFY(s1.YRadius() == 1.0);
   VERIFY(s1.Center() == R2Pt(0,0));

   R2Pt pt(0.1, 0.0);
   VERIFY(s1.Inside(pt) == TRUE);
   VERIFY(s2.Inside(pt) == TRUE);
   pt[0] = 1.0;
   pt[1] = 0;
   pt[2] = 0;
   VERIFY(s1.On(pt) == TRUE);
   VERIFY(s2.On(pt) == FALSE);
   
   pt[0] = 3.0;
   VERIFY(s1.Inside(pt) == FALSE);

   s3 = s2;
   VERIFY(s3 == s2);
   VERIFY(!(s3 == s1));

   s3.Print();
   ofstream out("RNtest.txt", ios::out);
   s3.Write(out);
   out.close();
   ifstream in("RNtest.txt", ios::in);
   s1.Read(in);
   in.close();
   VERIFY(s3 == s1);
   return TRUE;
}
