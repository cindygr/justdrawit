/* Copyright 1996, Microsoft.  All Rights Reserved. */


/* -------------------------------------------------------------------------
 *
 *               Solve Ax = b
 *
 * ------------------------------------------------------------------------- */
#include "StdAfx.H"

#include <fitting/FITTools_Least_squares.H>
#include <f2c.h>
#include <clapack.h>

/* -----------------------  Private Methods  ------------------------------- */

/* ----------------------- Protected Methods ------------------------------- */


/* -----------------------  Public Methods   ------------------------------- */

// solves overdetermined or underdetermined real linear systems
//  involving an M-by-N matrix A, or its transpose, using a QR or LQ
//  factorization of A.  It is assumed that A has full rank.
//   NOTE: X is column-row ordering (Fortran style)
static long int CallDgels( integer in_lNRows,
                           integer in_lNCols,
                           Array<double>  &in_adA,  // A input in column row format
                           integer in_lNColsB,
                           Array<double>  &in_adB,  // rhs B input in column row format
                           Array<double>  &out_adX  // return the result (each column is a solution)
              )
{
    
    integer lWork = WINmin( in_lNRows, in_lNCols ) + WINmax( WINmax((integer)1, in_lNRows), 
                                                             WINmax(in_lNCols, in_lNColsB) );

    Array<double> adWork( lWork );
    char cTrans = 'N';
    
    integer lInfo, lLdb = WINmax( in_lNRows, in_lNCols);

    double *adB = NULL;
    // B is too small, copy to X before doing dgels
    if ( in_lNRows < in_lNCols ) {
        ASSERT( out_adX.num() >= in_lNColsB * in_lNCols );
        ASSERT( in_adB.num() >= in_lNColsB * in_lNRows );

        for ( int i = 0; i < in_lNColsB; i++ ) {
            for ( int j = 0; j < in_lNRows; j++ ) {
                out_adX[ (int) (i * in_lNCols) + j ] = in_adB[ (int) (i * in_lNRows) + j ];
            }
            memcpy( &out_adX[ (int) (i * in_lNCols) ], 
                    &in_adB[ (int) (i * in_lNRows) ], 
                    (int) in_lNRows * sizeof(double) );
        }
        adB = &out_adX[0];
    } else {
        adB = &in_adB[0];
    }

    dgels_( &cTrans, 
           &in_lNRows, &in_lNCols, &in_lNColsB, 
           &in_adA[0], &in_lNRows, 
           adB, &lLdb, 
           &adWork[0], &lWork, &lInfo );

    // Result was in B. Copy to X.
    if ( in_lNRows >= in_lNCols ) {
        for ( int i = 0; i < in_lNColsB; i++ ) {
            memcpy( &out_adX[(int) (i * in_lNCols)], 
                    &in_adB[(int) (i * in_lNRows)], 
                    in_lNCols * sizeof(double) );
        }
    }

    return lInfo;
}

WINbool
FITTools_LS::Solve()
{
    int iRes = 0;
    WINbool bRes = TRUE;
    iRes = CallDgels( m_iNDesired, m_iNVariables,
                     m_adDesiredData, m_iNDimension,
                     m_adRhsData,
                     m_adSolutionData);

    if ( iRes != 0 ) {
       bRes = FALSE;
       cerr << m_iNDesired << " " << m_iNVariables << " " << iRes << "\n";
    }

    for (int i = 0; i < m_iNDimension; i++) {
        for (int c = 0; c < m_iNVariables; c++) {
            //TRACE("%f %f\n", lsCopy.Solution_utils(c, i), Solution(c, i));
            if ( ! ( Solution(c, i) > -1e20 && Solution(c, i) < 1e20 ) )
                bRes = FALSE;
        }
    }

    return bRes;
}

/* -------------------------------------------------------------------------
 * DESCR   :	Create the matrices that will Ae needed
 * ------------------------------------------------------------------------- */
FITTools_LS::FITTools_LS( int in_iNDesired, // number of rows/constraints
                          int in_iNVariables, // number of variables
                          int in_iNDimension //  number of cols in solution
                           ) 
{
    Resize( in_iNDesired, in_iNVariables, in_iNDimension );
}

void FITTools_LS::Clear()
{
    m_adDesiredData.memfill(0);
    m_adSolutionData.memfill(0);
    m_adRhsData.memfill(0);
}

void FITTools_LS::Resize( const int in_iNDesired, const int in_iNVariables, const int in_iDimension )
{
    ASSERT( in_iNDesired > 0 );
    ASSERT( in_iNVariables > 0 );
    ASSERT( in_iDimension > 0 );

    m_iNDesired = in_iNDesired;
    m_iNVariables = in_iNVariables;
    m_iNDimension = in_iDimension;

	m_adDesiredData.need( in_iNDesired * m_iNVariables );
	m_adSolutionData.need( in_iNVariables * in_iDimension );
	m_adRhsData.need( in_iNDesired * in_iDimension );

    Clear(  );
}

/* -------------------------------------------------------------------------
 * DESCR   :	Zap everything
 * ------------------------------------------------------------------------- */
FITTools_LS::~FITTools_LS()
{
}


void FITTools_LS::Print(ostream &out )
{
    Array<double> adSum( N_variables() );
    Array<double> adSumAbs( N_variables() );

    for (int iV = 0; iV < N_variables(); iV++) {
        adSum[iV] = 0;
        adSumAbs[iV] = 0;
    }

    out << "Desired " << N_desired() << " " << N_variables() << "\n";
    for (int iD = 0; iD < N_desired(); iD++) {
        double dSum = 0;
        for (int iV = 0; iV < N_variables(); iV++) {
            dSum += Desired(iD, iV);
            adSum[iV] += Desired(iD, iV);
            adSumAbs[iV] += fabs( Desired(iD, iV) );
            if (!RNIsZero( Desired(iD, iV) ) ) {
                out << iV << " " << Desired(iD, iV) << "  ";
            }
        }

        out << " = ";
        for (int iS = 0; iS < N_dimension(); iS++) {
            out << Rhs(iD, iS) << " ";
        }
        out << "  Sum " << dSum << "\n";
    }
    for ( FORINT iV = 0; iV < N_variables(); iV++) {
        out << adSum[iV] << " ";
    }
    out << "\n";
    for ( FORINT iV = 0; iV < N_variables(); iV++) {
        out << adSumAbs[iV] << " ";
    }
    out << "\n\n";
}

void  FITTools_LS::Check( const double dMinA, const double dMaxA, const double dMinR, const double dMaxR )
{
    Array<double> adSum( N_variables() );
    Array<double> adSumAbs( N_variables() );

    for (int iV = 0; iV < N_variables(); iV++) {
        adSum[iV] = 0;
        adSumAbs[iV] = 0;
    }

    for (int iD = 0; iD < N_desired(); iD++) {
        double dSum = 0;
        for (int iV = 0; iV < N_variables(); iV++) {
            dSum += Desired(iD, iV);
            adSum[iV] += Desired(iD, iV);
            adSumAbs[iV] += fabs( Desired(iD, iV) );
            ASSERT ( Desired(iD, iV) >= dMinA && Desired(iD, iV ) <= dMaxA ) ;
        }

        for (int iS = 0; iS < N_dimension(); iS++) {
            ASSERT (  Rhs(iD, iS) >= dMinR && Rhs(iD, iS) <= dMaxR ) ;
        }
        if ( RNIsZero( dSum ) )
            ASSERT(FALSE);
    }
    for ( FORINT iV = 0; iV < N_variables(); iV++) {
        if ( RNIsZero( adSumAbs[iV]) )
            ASSERT(FALSE);
    }
}



void FITTools_LS::Test()
{
    FITTools_LS oLS(2,2,1);

    oLS.Desired(0,0) = 1.0;
    oLS.Desired(0,1) = 0.0;
    oLS.Desired(1,0) = 0.0;
    oLS.Desired(1,1) = 1.0;

    oLS.Rhs(0, 0) = 3.0;
    oLS.Rhs(1, 0) = 4.0;

    VERIFY( oLS.Solve() == TRUE );

    cout << "(should be 3.0) " << oLS.Solution(0,0 ) << "\n";
    cout << "(should be 4.0) " << oLS.Solution(1,0 ) << "\n\n";

    ASSERT( RNApproxEqual(3.0, oLS.Solution(0,0 ), 1e-4 ) );
    ASSERT( RNApproxEqual(4.0, oLS.Solution(1,0 ), 1e-4 ) );

    FITTools_LS oLS2(3,2,1);

    oLS2.Desired(0,0) = 1.0;
    oLS2.Desired(0,1) = 0.0;
    oLS2.Desired(1,0) = 0.0;
    oLS2.Desired(1,1) = 1.0;
    oLS2.Desired(2,0) = 0.8;
    oLS2.Desired(2,1) = 0.2;

    oLS2.Rhs(0, 0) = 3.0;
    oLS2.Rhs(1, 0) = 4.0;
    oLS2.Rhs(2, 0) = 2.0;

    VERIFY( oLS2.Solve() == TRUE );

    cout << "(should be 2.42857) " << oLS2.Solution(0,0 ) << "\n";
    cout << "(should be 3.85714) " << oLS2.Solution(1,0 ) << "\n\n";

    ASSERT( RNApproxEqual(2.42857, oLS2.Solution(0,0 ), 1e-4 ) );
    ASSERT( RNApproxEqual(3.85714, oLS2.Solution(1,0 ), 1e-4 ) );

    FITTools_LS oLS3(3,2,2);

    oLS3.Desired(0,0) = 1.0;
    oLS3.Desired(0,1) = 0.0;
    oLS3.Desired(1,0) = 0.0;
    oLS3.Desired(1,1) = 1.0;
    oLS3.Desired(2,0) = 0.8;
    oLS3.Desired(2,1) = 0.2;

    oLS3.Rhs(0, 0) = 6.0;
    oLS3.Rhs(1, 0) = 8.0;
    oLS3.Rhs(2, 0) = 4.0;

    oLS3.Rhs(0, 1) = 3.0;
    oLS3.Rhs(1, 1) = 4.0;
    oLS3.Rhs(2, 1) = 2.0;

    VERIFY( oLS3.Solve() == TRUE );
    
    cout << "(should be 4.85714) " << oLS3.Solution(0,0 ) << "\n";
    cout << "(should be 7.71429) " << oLS3.Solution(1,0 ) << "\n";
    cout << "(should be 2.42857) " << oLS3.Solution(0,1 ) << "\n";
    cout << "(should be 3.85714) " << oLS3.Solution(1,1 ) << "\n\n";

    ASSERT( RNApproxEqual( 4.85714,  oLS3.Solution(0,0 ), 1e-4 ) );
    ASSERT( RNApproxEqual( 7.71429,  oLS3.Solution(1,0 ), 1e-4 ) );
    ASSERT( RNApproxEqual( 2.42857,  oLS3.Solution(0,1 ), 1e-4 ) );
    ASSERT( RNApproxEqual( 3.85714,  oLS3.Solution(1,1 ), 1e-4 ) );

    cerr << "Over constrained\n";
    FITTools_LS oLS4(2,3,2);

    oLS4.Desired(0,0) = 1.0;
    oLS4.Desired(0,1) = 0.0;
    oLS4.Desired(0,2) = 0.8;
    oLS4.Desired(1,0) = 0.0;
    oLS4.Desired(1,1) = 1.0;
    oLS4.Desired(1,2) = 0.2;

    oLS4.Rhs(0, 0) = 3.0;
    oLS4.Rhs(1, 0) = 4.0;
    oLS4.Rhs(0, 1) = 6.0;
    oLS4.Rhs(1, 1) = 8.0;

    VERIFY( oLS4.Solve() == TRUE );

    cout << "(should be 1.47619) " << oLS4.Solution(0,0 ) << "\n";
    cout << "(should be 3.61905) " << oLS4.Solution(1,0 ) << "\n";
    cout << "(should be 1.90476) " << oLS4.Solution(2,0 ) << "\n";
    cout << "(should be 2.95238) " << oLS4.Solution(0,1 ) << "\n";
    cout << "(should be 7.2381) " << oLS4.Solution(1,1 ) << "\n";
    cout << "(should be 3.80952) " << oLS4.Solution(2,1 ) << "\n";

    ASSERT( RNApproxEqual( 1.47619,  oLS4.Solution(0,0 ), 1e-4 ) );
    ASSERT( RNApproxEqual( 3.61905,  oLS4.Solution(1,0 ), 1e-4 ) );
    ASSERT( RNApproxEqual( 1.90476,  oLS4.Solution(2,0 ), 1e-4 ) );
    ASSERT( RNApproxEqual( 2.95238,  oLS4.Solution(0,1 ), 1e-4 ) );
    ASSERT( RNApproxEqual( 7.2381,  oLS4.Solution(1,1 ), 1e-4 ) );
    ASSERT( RNApproxEqual( 3.80952,  oLS4.Solution(2,1 ), 1e-4 ) );
}
