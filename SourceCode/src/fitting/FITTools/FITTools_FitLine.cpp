// FITTools_FitLine.cpp: implementation of the FITTools_FitLine class.
//
//////////////////////////////////////////////////////////////////////

#include "StdAfx.H"
#include <fitting/FITTools_FitLine.H>

//////////////////////////////////////////////////////////////////////
// Output
//////////////////////////////////////////////////////////////////////

R2Line     FITTools_FitLine::Line2D( )     const
{
    return R2Line( m_seg2D.P1(), m_seg2D.P2() );
}
///
const R2Line_seg &FITTools_FitLine::Line_seg2D( ) const
{
    return m_seg2D;
}
///
R3Line     FITTools_FitLine::Line3D( )     const
{
    return R3Line( m_seg3D.P1(), m_seg3D.P2() );
}
///
const R3Line_seg &FITTools_FitLine::Line_seg3D( ) const
{
    return m_seg3D;
}

void FITTools_FitLine::Fit( const Array<R2Pt> &in_apt ) 
{
    if ( in_apt.num() < 1 ) {
        m_seg2D.P1() = m_seg2D.P2() = R2Pt(0,0);
        ASSERT(FALSE);
        return;
    }

    Resize( in_apt.num(), 2, 1 );

    if ( in_apt.num() == 1 ) {
        m_dPercError = 0.0;
        m_seg2D.P1() = m_seg2D.P2() = in_apt[0];
        TRACE("Too few points\n");

    } else if ( in_apt.num() == 2 ) {
        m_dPercError = 0.0;
        m_seg2D.P1() = in_apt[0];
        m_seg2D.P2() = in_apt[1];

    } else {

        for ( int i = 0; i < in_apt.num(); i++ ) {
            Desired(i, 0) = in_apt[i][0];
            Desired(i, 1) = in_apt[i][1];
            Rhs(i, 0) = -1.0;
        }
        Solve();

        const double dA = Solution(0, 0);
        const double dB = Solution(1, 0);
        const double dC = 1.0;

        if ( RNIsZero( dA ) && RNIsZero( dB ) ) {
            TRACE("Bad line\n");
            m_seg2D.P1() = m_seg2D.P2() = in_apt[0];
            m_dPercError = 1e30;
            return;
        }

        const R2Line line( dA, dB, dC );

        double dTMin = 1e30, dTMax = -1e30, dTOnLine = 0.0, dTToLine = 0.0;
        R2Pt ptClose;
        m_dPercError = 0.0;
        for ( FORINT i = 0; i < in_apt.num(); i++ ) {
            line.FindPtOnLine( in_apt[i], ptClose, dTOnLine, dTToLine );
            if ( dTOnLine < dTMin ) {
                m_seg2D.P1() = ptClose;
                dTMin = dTOnLine;
            }
            if ( dTOnLine > dTMax ) {
                m_seg2D.P2() = ptClose;
                dTMax = dTOnLine;
            }
            m_dPercError += dTToLine;
        }
        m_dPercError /= (double) in_apt.num();
    }
}

void FITTools_FitLine::Fit( const Array<R3Pt> &in_apt ) 
{
    static Array<double> adWeights;
	
	if ( adWeights.num() < in_apt.num() ) {
		adWeights.need( in_apt.num() );		
		adWeights.fill(1.0);
	}
    
    Fit( in_apt, adWeights );
}

void FITTools_FitLine::Fit( const Array<R3Pt> &in_apt, const Array<double> &in_adWeights ) 
{
    if ( in_apt.num() < 1 ) {
        m_seg3D.P1() = m_seg3D.P2() = R3Pt(0,0,0);
        ASSERT(FALSE);
        return;
    }

    Resize( in_apt.num(), 2, 3 );

    if ( in_apt.num() == 1 ) {
        m_dPercError = 0.0;
        m_seg3D.P1() = m_seg3D.P2() = in_apt[0];
        TRACE("Too few points\n");

    } else if ( in_apt.num() == 2 ) {
        m_dPercError = 0.0;
        m_seg3D.P1() = in_apt[0];
        m_seg3D.P2() = in_apt[1];

    } else {
		const R3Pt ptOther = ApproxEqual(in_apt[0], in_apt.last() ) ? in_apt[0] + R3Vec(1,1,1) : in_apt.last();
        R3Line line( in_apt[0], ptOther );
        double dFurthest = LengthSq( in_apt[0] - in_apt[1] );
		const int iNTries = 10;
		const int iStep = WINmax( 1, in_apt.num() / iNTries );
        for ( int i = 0; i < in_apt.num(); i += iStep ) {
            for ( int j = i+1; j < in_apt.num(); j += iStep ) {
                const double dLen = LengthSq( in_apt[i] - in_apt[j] );
                if ( dLen > dFurthest ) {
                    line = R3Line( in_apt[i], in_apt[j] );
                    dFurthest = dLen;
                }
            }
        }
        double dTMin = 1e30, dTMax = -1e30, dTOnLine = 0.0, dTToLine = 0.0;
        R3Pt ptClose;
        for ( int iLoop = 0; iLoop < 3; iLoop++ ) {
            Clear();
            for ( int i = 0; i < in_apt.num(); i++ ) {
                const double dT = line.Projected_dist_on_line( in_apt[i] );
                Desired( i, 0 ) = 1.0 * in_adWeights[i];
                Desired( i, 1 ) = dT * in_adWeights[i];
                for ( int j = 0; j < 3; j++ ) {
                    Rhs( i, j ) = in_apt[i][j] * in_adWeights[i];
                }
            }
            VERIFY( Solve() );
            line.SetPt( R3Pt( Solution(0,0), Solution(0,1), Solution(0,2) ) );
            line.SetVec( R3Vec( Solution(1,0), Solution(1,1), Solution(1,2) ) );
        }
        m_dPercError = 0.0;
        for ( FORINT i = 0; i < in_apt.num(); i++ ) {
            line.FindPtOnLine( in_apt[i], ptClose, dTOnLine, dTToLine );
            if ( dTOnLine < dTMin ) {
                m_seg3D.P1() = ptClose;
                dTMin = dTOnLine;
            }
            if ( dTOnLine > dTMax ) {
                m_seg3D.P2() = ptClose;
                dTMax = dTOnLine;
            }
            m_dPercError += dTToLine;
        }
        m_dPercError /= (double) in_apt.num();
    }
}

//////////////////////////////////////////////////////////////////////
// Construction
//////////////////////////////////////////////////////////////////////
/// 2D line
FITTools_FitLine::FITTools_FitLine( const Array<R2Pt> &in_apt ) 
: FITTools_LS( WINmax(1, in_apt.num() ), 2, 1 )
{
    Fit( in_apt );
}

/// 3D line
FITTools_FitLine::FITTools_FitLine( const Array<R3Pt> &in_apt )
: FITTools_LS( WINmax( in_apt.num(), 1), 4, 1 )
{
    ASSERT(FALSE);
    Fit( in_apt );
}
WINbool FITTools_FitLine::Test()
{
    Array<R2Pt> apt;

    apt += R2Pt(0.0, 1.0);

    FITTools_FitLine f1(apt);   
    ASSERT( ApproxEqual( f1.Line_seg2D().P1(), apt[0] ) );

    apt += R2Pt( 0.2, 1.5 );
    FITTools_FitLine f2(apt);   
    ASSERT( ApproxEqual( f2.Line_seg2D().P1(), apt[0] ) );
    ASSERT( ApproxEqual( f2.Line_seg2D().P2(), apt[1] ) );

    apt += R2Pt( 0.41, 2.0 );
    FITTools_FitLine f3(apt);   
    ASSERT( ApproxEqual( f3.Line_seg2D().P1(), apt[0] ) );
    ASSERT( ApproxEqual( f3.Line_seg2D().P2(), apt[2] ) );
    ASSERT( f3.Error() < 0.1 );

    const R2Line_seg seg( apt[0], apt[2] );
    apt.clearcompletely();

    const double dDiv = 2147483647.0;
    for (double dT = 0; dT <= 1.0; dT += 0.11) {
        double dr1 = (double) rand() / dDiv;
        double dr2 = (double) rand() / dDiv;
        apt += seg( dT ) + R2Vec(dr1 * 0.01, dr2 * 0.01);
    }

    FITTools_FitLine f4(apt);   
    ASSERT( Length( f4.Line_seg2D().P1() - apt[0] ) < 0.05 );
    ASSERT( Length( f4.Line_seg2D().P2() - apt.last() ) < 0.05 );
    ASSERT( f4.Error() < 0.1 );

    return TRUE;
}
