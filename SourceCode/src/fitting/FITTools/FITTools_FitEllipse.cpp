// FITTools_FitEllipse.cpp: implementation of the FITTools_FitEllipse class.
//
//////////////////////////////////////////////////////////////////////

#include <fitting/FITTools_FitEllipse.H>
#include <fitting/FITTools_SVD.H>
#include <f2c.h>
#include <clapack.h>

R2Vec FITTools_FitEllipse::FindAxis( const Array<R2Pt> &in_apt ) const
{
    char jobu = 'S';
    char jobv = 'S';
    integer m = in_apt.num();
    integer n = 2;
    integer lda = m;
    integer lwork = 6 + in_apt.num();
    integer linfo = 0;
    double *work = new double [lwork];
    double *a = new double [m * n];
    double *s = new double [n];
    double *u = new double [m * m];
    double *vt = new double[m * m];

    const double dX = m_ellipse.Center()[0];
    const double dY = m_ellipse.Center()[1];
    for ( int i = 0; i < m; i++ ) {
        a[i] = in_apt[i][0] - dX;
        a[i+m] = in_apt[i][1] - dY;
    }

    dgesvd_( &jobu, &jobv, &m, &n, a, &lda, s, u, &lda, vt, &n, work, &lwork, &linfo );

    const R2Vec v1(vt[0], vt[1]);
    const R2Vec v2(vt[2], vt[3]);

    ASSERT( linfo == 0 );

    delete [] work;
    delete [] a;
    delete [] s;
    delete [] u;
    delete [] vt;
    return v1;
}

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

FITTools_FitEllipse::FITTools_FitEllipse( const Array<R2Pt> &in_apt )
: FITTools_LS( WINmax(1, in_apt.num()), 2, 1 )
{
    m_dPercErr = 1e30;
    if ( in_apt.num() < 1 ) {
        return;
    } else if ( in_apt.num() == 1 ) {
        m_ellipse = R2Ellipse( in_apt[0], 1e-16, 1e-16 );
        return;
    } else if ( in_apt.num() == 2 ) {
        m_ellipse = R2Ellipse( Lerp( in_apt[0], in_apt[1], 0.5 ),
                               0.5  * Length( in_apt[1] - in_apt[0] ),
                               0.25 * Length( in_apt[1] - in_apt[0] ),
                               -acos( Dot( UnitSafe( in_apt[1] - in_apt[0] ), R2Vec(1,0) ) ) );
        m_dPercErr = 1.0;
        return;
    }

    R2Pt ptCenter(0,0);
    for (int i = 0; i < in_apt.num(); i++) {
        ptCenter[0] += in_apt[i][0];
        ptCenter[1] += in_apt[i][1];
    }
    ptCenter = R2Pt( ptCenter[0] / (double) in_apt.num(), ptCenter[1] / (double) in_apt.num() );

    const R2Vec vec = FindAxis( in_apt );

    double dTheta = 0.0;
    if ( vec[0] < 0 ) {
        dTheta = -acos( Dot( vec, R2Vec(-1,0) ) );
    } else {
        dTheta = -acos( Dot( vec, R2Vec(1,0) ) );
    }

    m_ellipse = R2Ellipse( ptCenter, 1.0, 1.0, dTheta );

    const R3Matrix mat = m_ellipse.ToNormal();
    Array<R2Pt> apt( in_apt.num() );
    for ( FORINT i = 0; i < in_apt.num(); i++) {
        const R2Pt pt = mat * in_apt[i];
        apt[i] = pt;
        Desired( i, 0 ) = (pt[0] * pt[0]);
        Desired( i, 1 ) = (pt[1] * pt[1]);
        Rhs( i, 0 ) = 1;
    }

    Solve();

    const double dAPrime = WINmax( Solution(0,0), 1e-15 );
    const double dBPrime = WINmax( Solution(1,0), 1e-15 );


    m_ellipse = R2Ellipse( ptCenter, dAPrime, dBPrime, dTheta );

    m_dPercErr = 0.0;
    const double dA = m_ellipse.XRadius() * m_ellipse.XRadius();
    const double dB = m_ellipse.YRadius() * m_ellipse.YRadius();
    
    for ( FORINT i = 0; i < apt.num(); i++ ) {
        m_dPercErr += (apt[i][0] * apt[i][0]) / dA + (apt[i][1] * apt[i][1]) / dB - 1.0;
    }

    m_dPercErr /= (double) apt.num();
}

R2Ellipse FITTools_FitEllipse::FitSVD( const Array<R2Pt> &in_apt )
{
    R2Ellipse ell;
    if ( in_apt.num() < 1 ) {
        return ell;
    } else if ( in_apt.num() == 1 ) {
        ell = R2Ellipse( in_apt[0], 1e-16, 1e-16 );
        return ell;
    } else if ( in_apt.num() == 2 ) {
        ell = R2Ellipse( Lerp( in_apt[0], in_apt[1], 0.5 ),
                         0.5  * Length( in_apt[1] - in_apt[0] ),
                         0.25 * Length( in_apt[1] - in_apt[0] ),
                        -acos( Dot( UnitSafe( in_apt[1] - in_apt[0] ), R2Vec(1,0) ) ) );
        return ell;
    }

    static FITToolsSVDLapack s_svd( in_apt.num(), 2, FALSE, TRUE );
    s_svd.Resize( in_apt.num(), 2 );

    R2Pt ptCenter(0,0);
    for (int i = 0; i < in_apt.num(); i++) {
        ptCenter[0] += in_apt[i][0];
        ptCenter[1] += in_apt[i][1];
    }
    ptCenter = R2Pt( ptCenter[0] / (double) in_apt.num(), ptCenter[1] / (double) in_apt.num() );

    for ( FORINT i = 0; i < in_apt.num(); i ++ ) {
        s_svd.A(i,0) = in_apt[i][0] - ptCenter[0];
        s_svd.A(i,1) = in_apt[i][1] - ptCenter[1];
    }
    s_svd.Solve();

    const R2Vec vecU1( s_svd.Vt( 0, 0), s_svd.Vt( 0, 1) );
    const R2Vec vecU2( s_svd.Vt( 1, 0), s_svd.Vt( 1, 1) );

    const double dAng = atan2( vecU1[1], vecU1[0] );

    static FITTools_LS s_oLS( in_apt.num(), 2, 1 );
    s_oLS.Resize( in_apt.num(), 2, 1 );

    ell = R2Ellipse( ptCenter, 1.0, 1.0, dAng );
    const R3Matrix mat = ell.ToNormal();
    double dLen = 0.0;
    for ( FORINT i = 0; i < in_apt.num(); i++) {
        const R2Pt pt = mat * in_apt[i];
        s_oLS.Desired( i, 0 ) = (pt[0] * pt[0]);
        s_oLS.Desired( i, 1 ) = (pt[1] * pt[1]);
        s_oLS.Rhs( i, 0 ) = 1;
        dLen += sqrt( pow( pt[0], 2 ) + pow( pt[1], 2 ) );
    }

    if ( s_oLS.Solve() == FALSE ) {
        // If it's a circe w/evenly distributed points, may not have answer
        dLen = dLen / (double) in_apt.num();
        ell = R2Ellipse( ptCenter, dLen, dLen, dAng );
    } else {
        const double dEigen1 = s_oLS.Solution(0,0);
        const double dEigen2 = s_oLS.Solution(1,0);
        const double dAPrime = 1.0 / sqrt( WINmax( dEigen1, 1e-14 ) );
        const double dBPrime = 1.0 / sqrt( WINmax( dEigen2, 1e-14 ) );

        const double dRatio1 = dAPrime / dBPrime;
        const double dRatio2 = dBPrime / dAPrime;

        if ( RNIsZero( dRatio1, 1e-12 ) ) {
            ell = R2Ellipse( ptCenter, dAPrime, dBPrime * 0.001, dAng );
        } else if ( RNIsZero( dRatio2, 1e-12 ) ) {
            ell = R2Ellipse( ptCenter, dAPrime * 0.001, dBPrime, dAng );
        } else {
            ell = R2Ellipse( ptCenter, dAPrime, dBPrime, dAng );
        }
    }
    return ell;
}

WINbool FITTools_FitEllipse::Test()
{
    Array<R2Pt> apt;

    ASSERT( R2Ellipse::Test() );
    ASSERT( R3Ellipse::Test() );

    FITTools_FitEllipse e1( apt );
    ASSERT( e1.Error() == 1e30 );

    apt += R2Pt(0.1, 0.2);
    FITTools_FitEllipse e2( apt );
    ASSERT( ApproxEqual( e2.Ellipse().Center(), apt[0] ) );

    apt += R2Pt(0.2, 0.4);
    FITTools_FitEllipse e3( apt );
    ASSERT( ApproxEqual( e3.Ellipse().Center(), Lerp( apt[0], apt[1], 0.5 ) ) );
    const R2Pt ptRot0 = e3.Ellipse().ToNormal() * apt[0];
    const R2Pt ptRot1 = e3.Ellipse().ToNormal() * apt[1];
    ASSERT( RNIsZero( e3.Ellipse()(apt[0]) ) );
    ASSERT( RNIsZero( e3.Ellipse()(apt[1]) ) );

    R2Ellipse ellipse( R2Pt( 0.5, 0.1 ), 0.5, 4.0, M_PI / 5.0 );

    apt.need( 20 );
    ellipse.Points( apt );
    
    FITTools_FitEllipse e4( apt );

    ASSERT( ApproxEqual( ellipse.Center(), e4.Ellipse().Center() ) );
    ASSERT( RNIsZero( e4.Error() ) );

    return TRUE;
}


R2Ellipse FITTools_FitEllipse::FitImplicit( const Array<R2Pt> &in_apt )
{
    Array<double> adD( in_apt.num() * 6 );

    R2Vec vecShift(0,0), vecMax(-1e30,-1e30), vecMin(1e30, 1e30);
    for ( int i = 0; i < in_apt.num(); i++ ) {
        for ( int j = 0; j < 2; j++ ) {
            vecShift[j] += in_apt[i][j];
            vecMax[j] = WINmax( vecMax[j], in_apt[i][j] );
            vecMin[j] = WINmin( vecMin[j], in_apt[i][j] );
        }
    }

    vecShift[0] /= (double) in_apt.num();
    vecShift[1] /= (double) in_apt.num();
    const double dSclX = (vecMax[0] - vecMin[0]) / 2.0;
    const double dSclY = (vecMax[1] - vecMin[1]) / 2.0;

    // First Create S Matrix = D_Transpose * D

    // Initialize D values
    int iIndx = 0;
    for ( FORINT i = 0; i < in_apt.num(); i++, iIndx += 6 ) {
        const R2Pt pt( ( in_apt[i][0] - vecShift[0] ) / dSclX, ( in_apt[i][1] - vecShift[1] ) / dSclY );

        adD[ iIndx + 0 ] = pt[0] * pt[0];
        adD[ iIndx + 1 ] = pt[0] * pt[1];
        adD[ iIndx + 2 ] = pt[1] * pt[1];
        adD[ iIndx + 3 ] = pt[0];
        adD[ iIndx + 4 ] = pt[1];
        adD[ iIndx + 5 ] = 1.0;
    }

    // Create S Matrix = D_Tranpose * D
    static double adS[36];
    for ( FORINT i = 0; i < 6; i++) {
        for (int j = 0; j < 6; j++) {
             // Compute correct index for column major indexing
             const int nIndex = j * 6 + i;
             adS[nIndex] = 0.0;

             for (int k = 0; k < in_apt.num(); k++)
                adS[nIndex] += adD[k * 6 + i] * adD[k * 6 + j];
        }
    }
    R3Matrix A, B, C, D( R3Vec(0,0,-2), R3Vec(0,1,0), R3Vec(-2,0,0) );

/*
  % Break into blocks
  tmpA = S(1:3,1:3); 
  tmpB = S(1:3,4:6); 
  tmpC = S(4:6,4:6); 
  tmpD = C(1:3,1:3);
  tmpE = inv(tmpC)*tmpB';
  [evec_x, eval_x] = eig(inv(tmpD) * (tmpA - tmpB*tmpE));
  
  % Find the positive (as det(tmpD) < 0) eigenvalue
  I = find(real(diag(eval_x)) < 1e-8 & ~isinf(diag(eval_x)));
  
  % Extract eigenvector corresponding to negative eigenvalue
  A = real(evec_x(:,I));
  
  % Recover the bottom half...
  evec_y = -tmpE * A;
  A = [A; evec_y];
  */

    for ( FORINT i = 0; i < 3; i++ ) {
        for ( int j = 0; j < 3; j++ ) {
            A(i,j) = adS[ j * 6 + i ];
            B(i,j) = adS[ (j+3) * 6 + i ];
            C(i,j) = adS[ (j+3) * 6 + i + 3];
        }
    }

    WINbool bSuc;
    const R3Matrix E = C.Inverse( bSuc ) * B.Transpose();
    VERIFY( bSuc == TRUE );
    const R3Matrix matFinal = D.Inverse( bSuc ) * ( A - B * E );
    VERIFY( bSuc == TRUE );

	assert(false);
    R3Vec vec;

	/* Not putting complexnumbers in
	 R3VectorTC<C2Pt> vec1, vec2, vec3;
	 C2Pt cptL1, cptL2, cptL3;
    matFinal.Eigen( cptL1, cptL2, cptL3, vec1, vec2, vec3 );

    if ( cptL1.real() < 1e-8 && cptL1.real() < 1e30 ) 
        vec = R3Vec( vec1[0].real(), vec1[1].real(), vec1[2].real() );
    else if ( cptL2.real() < 1e-8 && cptL2.real() < 1e30 )
        vec = R3Vec( vec2[0].real(), vec2[1].real(), vec2[2].real() );
    else if ( cptL3.real() < 1e-8 && cptL3.real() < 1e30 )
        vec = R3Vec( vec3[0].real(), vec3[1].real(), vec3[2].real() );
    else
        ASSERT(FALSE);

	 */
    const R3Vec vecY = E * (-vec);

    //A(1)*sy*sy,   ...
    //  A(2)*sx*sy,   ...
    //  A(3)*sx*sx,   ...

    //  -2*A(1)*sy*sy*mx - A(2)*sx*sy*my + A(4)*sx*sy*sy,   ...
    //  -2*A(3)*sx*sx*my - A(2)*sx*sy*mx + A(5)*sx*sx*sy,   ...

    //  A(1)*sy*sy*mx*mx + A(2)*sx*sy*mx*my + A(3)*sx*sx*my*my   ...
    //  - A(4)*sx*sy*sy*mx - A(5)*sx*sx*sy*my   ...
    //  + A(6)*sx*sx*sy*sy   ...
    const R2Ellipse ellRet( vec[0] * dSclY * dSclY,
                            vec[1] * dSclX * dSclY,
                            vec[2] * dSclX * dSclX,

                            -2.0 * vec[0] * dSclY * dSclY * vecShift[0] - vec[1] * dSclX * dSclY * vecShift[1] + vecY[0] * dSclX * dSclY * dSclY,
                            -2.0 * vec[2] * dSclX * dSclX * vecShift[1] - vec[1] * dSclX * dSclY * vecShift[0] + vecY[1] * dSclY * dSclX * dSclX,

                            vec[0] * dSclY * dSclY * vecShift[0] * vecShift[0] + 
                            vec[1] * dSclX * dSclY * vecShift[0] * vecShift[1] +
                            vec[2] * dSclX * dSclX * vecShift[1] * vecShift[1] - 
                            vecY[0] * dSclX * dSclY * dSclY * vecShift[0] -
                            vecY[1] * dSclX * dSclX * dSclY * vecShift[1] +
                            vecY[2] * dSclX * dSclX * dSclY * dSclY );


    VERIFY( ellRet.IsEllipse() );
    return ellRet;
}

R2Ellipse FITTools_FitEllipse::FitImplicitDirect( const Array<R2Pt> &in_apt, const WINbool in_bUseTangents )
{
    if ( in_apt.num() < 4 && in_bUseTangents == FALSE )
        return FitImplicitCircle( in_apt );

    FITTools_LS oLS( in_apt.num() + 1 + (in_bUseTangents ? 2 * in_apt.num() : 0), 6, 1 );

    for ( int iRow = 0; iRow < in_apt.num(); iRow++ ) {
        oLS.Desired( iRow, 0 ) = in_apt[iRow][0] * in_apt[iRow][0];
        oLS.Desired( iRow, 1 ) = in_apt[iRow][0] * in_apt[iRow][1];
        oLS.Desired( iRow, 2 ) = in_apt[iRow][1] * in_apt[iRow][1];
        oLS.Desired( iRow, 3 ) = in_apt[iRow][0];
        oLS.Desired( iRow, 4 ) = in_apt[iRow][1];
        oLS.Desired( iRow, 5 ) = 1.0;
    }
    if ( in_bUseTangents == TRUE ) {
        for ( int iRow = 0; iRow < in_apt.num(); iRow++ ) {
            const R2Pt ptPrev = Lerp( in_apt[iRow], in_apt.wrap(iRow-1), 0.15 );
            oLS.Desired( in_apt.num() + 2 * iRow + 0, 0 ) = ptPrev[0] * ptPrev[0];
            oLS.Desired( in_apt.num() + 2 * iRow + 0, 1 ) = ptPrev[0] * ptPrev[1];
            oLS.Desired( in_apt.num() + 2 * iRow + 0, 2 ) = ptPrev[1] * ptPrev[1];
            oLS.Desired( in_apt.num() + 2 * iRow + 0, 3 ) = ptPrev[0];
            oLS.Desired( in_apt.num() + 2 * iRow + 0, 4 ) = ptPrev[1];
            oLS.Desired( in_apt.num() + 2 * iRow + 0, 5 ) = 1.0;

            const R2Pt ptNext = Lerp( in_apt[iRow], in_apt.wrap(iRow+1), 0.15 );
            oLS.Desired( in_apt.num() + 2 * iRow + 1, 0 ) = ptNext[0] * ptNext[0];
            oLS.Desired( in_apt.num() + 2 * iRow + 1, 1 ) = ptNext[0] * ptNext[1];
            oLS.Desired( in_apt.num() + 2 * iRow + 1, 2 ) = ptNext[1] * ptNext[1];
            oLS.Desired( in_apt.num() + 2 * iRow + 1, 3 ) = ptNext[0];
            oLS.Desired( in_apt.num() + 2 * iRow + 1, 4 ) = ptNext[1];
            oLS.Desired( in_apt.num() + 2 * iRow + 1, 5 ) = 1.0;

        }
    }
    for ( int i = 0; i < 6; i++ )
        oLS.Desired( oLS.N_desired() - 1, i ) = 1.0;
    oLS.Rhs(  oLS.N_desired() - 1, 0 ) = 1.0;

    VERIFY( oLS.Solve() );
    const double dSign = ( oLS.Solution(0,0) < 0.0 ) ? -1.0 : 1.0;
    const R2Ellipse ell( dSign * oLS.Solution(0,0), 
                         dSign * oLS.Solution(1,0), 
                         dSign * oLS.Solution(2,0), 
                         dSign * oLS.Solution(3,0), 
                         dSign * oLS.Solution(4,0), 
                         dSign * oLS.Solution(5,0) );

    return ell;
}

R2Ellipse FITTools_FitEllipse::FitImplicitNoCenter( const Array<R2Pt> &in_apt )
{
    if ( in_apt.num() < 4 )
        return FitImplicitCircle( in_apt );
    
    R2Ellipse ellGuess = FitImplicit( in_apt );
    R2Ellipse ellCur = R2Ellipse( R2Pt(0,0), ellGuess.XRadius(), ellGuess.YRadius(), ellGuess.GetTheta( ) );
    R2Ellipse ellDa( ellGuess ), ellDb( ellGuess ), ellDTheta( ellGuess );
    
    const double dAEps = 1e-4, dBEps = 1e-4, dThetaEps = 1e-4;
    
    double dLastErr = 0.0, dCurErr = 0.0;
    for ( int i = 0; i < in_apt.num(); i++ ) {
        dLastErr += fabs( ellGuess( in_apt[i] ) );
        dCurErr += fabs( ellCur( in_apt[i] ) );
    }
    do {
        ellDa = R2Ellipse( R2Pt(0,0), ellCur.XRadius() + dAEps, ellCur.YRadius(), ellCur.GetTheta() );
        ellDb = R2Ellipse( R2Pt(0,0), ellCur.XRadius(), ellCur.YRadius() + dBEps, ellCur.GetTheta() );
        ellDTheta = R2Ellipse( R2Pt(0,0), ellCur.XRadius(), ellCur.YRadius(), ellCur.GetTheta() + dThetaEps );
        
        double dDa = 0.0, dDb = 0.0, dDTheta = 0.0;
        for ( int i = 0; i < in_apt.num(); i++ ) {
            const double dCur = fabs( ellCur( in_apt[i] ) );
            dDa += ( fabs( ellDa( in_apt[i] ) ) - dCur ) / dAEps;
            dDb += ( fabs( ellDb( in_apt[i] ) ) - dCur ) / dBEps;
            dDTheta += ( fabs( ellDTheta( in_apt[i] ) ) - dCur ) / dThetaEps;
        }
        const R3Vec vecDParam = UnitSafe( R3Vec( dDa, dDb, dDTheta ) );
        double dEps = 0.1, dGuessErr = 1e30;
        while ( dEps > 1e-12 && dCurErr < dGuessErr ) {
            ellGuess = R2Ellipse( R2Pt(0,0), 
                                  ellCur.XRadius() - vecDParam[0] * dEps,
                                  ellCur.YRadius() - vecDParam[1] * dEps,
                                  ellCur.GetTheta() - vecDParam[2] * dEps );
            dGuessErr = 0.0;
            for ( int i = 0; i < in_apt.num(); i++ ) {
                dGuessErr += fabs( ellGuess( in_apt[i] ) );
            }
            dEps *= 0.1;
        }               
        if ( dGuessErr < dCurErr ) {
            dLastErr = dCurErr;
            dCurErr = dGuessErr;
            ellCur = ellGuess;
        } else {
            break;
        }
    } while ( dLastErr - dCurErr > 1e-6 );
            
    return ellCur;
}

R2Ellipse FITTools_FitEllipse::FitImplicitCircle( const Array<R2Pt> &in_apt )
{
    FITTools_LS oLS( in_apt.num() + 1, 4, 1 );

    for ( int iRow = 0; iRow < in_apt.num(); iRow++ ) {
        oLS.Desired( iRow, 0 ) = pow( in_apt[iRow][0], 2 ) + pow( in_apt[iRow][1], 2);
        oLS.Desired( iRow, 1 ) = in_apt[iRow][0];
        oLS.Desired( iRow, 2 ) = in_apt[iRow][1];
        oLS.Desired( iRow, 3 ) = 1.0;
    }
    for ( int i = 0; i < 4; i++ )
        oLS.Desired( in_apt.num(), i ) = 1.0;
    oLS.Rhs( in_apt.num(), 0 ) = 1.0;

    VERIFY( oLS.Solve() );

    double dSum = 0.0;
    for ( FORINT i  = 0; i < in_apt.num(); i++ ) {
        dSum += oLS.Solution(0,0) * ( pow(in_apt[i][0],2) + pow( in_apt[i][1], 2 ) ) +
                oLS.Solution(1,0) * in_apt[i][0] +                
                oLS.Solution(2,0) * in_apt[i][1] +                
                oLS.Solution(3,0);
    }
    // A x^2 + dB1 x + ? + A y^2 + dB2 + ? + C == 0
    // x^2 + 2 ? x + ?^2
    const double dB1 = ( oLS.Solution(1,0) / oLS.Solution(0,0) ) / 2.0;
    const double dB2 = ( oLS.Solution(2,0) / oLS.Solution(0,0) ) / 2.0;
    const double dC = oLS.Solution(3,0) / oLS.Solution(0,0);
    const double dR = dC - pow( dB1,2 ) - pow( dB2, 2 );

    dSum = 0.0;
    for ( FORINT i  = 0; i < in_apt.num(); i++ ) {
        dSum += pow(in_apt[i][0] + dB1,2) + pow( in_apt[i][1] + dB2, 2 ) + dR;
    }

    const R2Ellipse ell( R2Pt( - dB1, - dB2 ),
                         sqrt( -dR ), sqrt( -dR ), 0.0);

    return ell;
}
