/*
 *  FITTools_SVD.cpp
 *  SketchCurves
 *
 *  Created by Cindy Grimm on 1/12/11.
 *  Copyright 2011 Adobe Corp.. All rights reserved.
 *
 */


#include <fitting/FITTools_SVD.H>
#include <f2c.h>
#include <clapack.h>


bool FITToolsSVDLapack::Solve()
{
    m_adD.need( WINmax( m_iNRows, m_iNCols ) + 1);
    m_adE.need( WINmax( m_iNRows, m_iNCols ) + 1 );
    m_adTauP.need( WINmax( m_iNRows, m_iNCols ) + 1 );
    m_adTauQ.need( WINmax( m_iNRows, m_iNCols ) + 1 );
    m_adWork.need( (m_iNRows + m_iNCols) * 16 + 1 );
    iWork = (m_iNRows + m_iNCols) * 16 + 1;
    iInfo = 0;
	
    m_adD.last() = 0.0;
    m_adE.last() = 0.0;
    m_adTauQ.last() = 0.0;
    m_adTauP.last() = 0.0;
	
	iWork = -1;
	dgebrd_( &m_iNRows, &m_iNCols, (double *) &m_adA[0], &m_iNRows, (double *) &m_adD[0], (double *) &m_adE[0], (double *) &m_adTauQ[0], (double *) &m_adTauP[0], (double *) &m_adWork[0], &iWork, &iInfo );
	if ( iInfo == 0 ) {
		iWork = (int) m_adWork[0];
		m_adWork.need( iWork + 1 );
		m_adWork.last() = 0.0;
		dgebrd_( &m_iNRows, &m_iNCols, (double *) &m_adA[0], &m_iNRows, (double *) &m_adD[0], (double *) &m_adE[0], (double *) &m_adTauQ[0], (double *) &m_adTauP[0], (double *) &m_adWork[0], &iWork, &iInfo );
	} 

    ASSERT( m_adD.last() == 0.0 );
    ASSERT( m_adE.last() == 0.0 );
    ASSERT( m_adTauQ.last() == 0.0 );
    ASSERT( m_adTauP.last() == 0.0 );
    ASSERT( m_adWork.last() == 0.0 );
    if( iInfo != 0 ) {
        cerr << "Error calc'ing QBPt " << iInfo << "\n";
        return FALSE;
    }
	
    double adU[1];
    double *aopdU = adU;
    if ( m_bCalcU == TRUE ) {
        m_adU.need( m_iNRows * m_iNCols + 1);
        m_adU.last() = 0.0;
        memcpy( m_adU, m_adA, m_iNRows * m_iNCols * sizeof(double) );
        char cWhichVec = 'Q';
		dorgbr_( &cWhichVec, &m_iNRows, &m_iNCols, &m_iNCols, (double *) &m_adU[0], &m_iNRows, (double *) &m_adTauQ[0], (double *) &m_adWork[0], &iWork, &iInfo );
		aopdU = (double *) &m_adU[0];
		
        ASSERT( m_adWork.last() == 0.0 );
        ASSERT( m_adU.last() == 0.0 );
        if( iInfo != 0 ) {
            cerr << "Error calc'ing U " << iInfo << "\n";
            return FALSE;
        }
    }
	
    double adVt[1];
    double *aopdVt = adVt;
    if ( m_bCalcVt == TRUE ) {
        m_adVt.need( m_iNRows * m_iNCols + 1);
        m_adVt.last() = 0.0;
        char cWhichVec = 'P';
        memcpy( m_adVt, m_adA, m_iNRows * m_iNCols * sizeof(double) );
		dorgbr_( &cWhichVec, &m_iNCols, &m_iNCols, &m_iNRows, (double *) &m_adVt[0], &m_iNRows, (double *) &m_adTauP[0], (double *) &m_adWork[0], &iWork, &iInfo );
		aopdVt = (double *) &m_adVt[0];
		
        ASSERT( m_adWork.last() == 0.0 );
        ASSERT( m_adVt.last() == 0.0 );
        if( iInfo != 0 ) {
            cerr << "Error calc'ing U " << iInfo << "\n";
            return FALSE;
        }
    }
	
    integer iNRowsU = m_bCalcU ? m_iNRows : 0;
    integer iNColsVt = m_bCalcVt ? m_iNCols : 0;
    integer iNo = 0, iOne = 1;
    double adC[1];
    adC[0] = 0.0;
    char cUpperOrLower = 'U';
	dbdsqr_( &cUpperOrLower, &m_iNCols, &iNColsVt, &iNRowsU, &iNo, (double *) &m_adD[0], (double *) &m_adE[0], aopdVt, &m_iNRows, aopdU, &m_iNRows, adC, &iOne, (double *) &m_adWork[0], &iInfo );
	
    ASSERT( adC[0] == 0.0 );
	
    if( iInfo != 0 ) {
        cerr << "Error calc'ing SVD of B " << iInfo << "\n";
        return FALSE;
    }
    return TRUE;
}
