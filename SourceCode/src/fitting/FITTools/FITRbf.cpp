// RadBasisFunc.cpp: implementation of the RadBasisFunc class.
//
//////////////////////////////////////////////////////////////////////

#include "StdAfx.H"
#include <fitting/Fit_RBF.H>
#include <utils/Utils_GeomArray.H>


int FITRbfTC_SortFc( const void *in_op1, const void *in_op2 )

{
    const double *d1 = (double *) (in_op1);
    const double *d2 = (double *) (in_op2);

    return (*d1 < *d2) ? -1 : 1;
}

static void RBF_ForceCompile()
{
  int i1, i2;
  FITRbfTC_SortFc( &i1, &i2 );
}


