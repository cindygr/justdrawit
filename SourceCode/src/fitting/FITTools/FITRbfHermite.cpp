/*
 *  FITRbfHermite.cpp
 *  FITRbf
 *
 *  Created by Michelle Vaughan on 10/12/10.
 *  Copyright 2010 Washington Univ in St. Louis. All rights reserved.
 *
 */

#include "StdAfx.H"
#include <fitting/Fit_RBFHermite.H>
#include <iostream>


double FITRbfHermite::kernelFunc( const R3Pt &in_pt, const int &index ) const
{
	//Kernel Func ki(d) = (||d||)^3
	double len,lenCub;
	R3Vec diff;
	
	diff = in_pt - m_aptLocs[index];
	len = sqrt(pow(diff[0],2) + pow(diff[1],2) + pow(diff[2],2));
	lenCub = len*len*len;
	
	return lenCub;
}


//Equivalent to HRBFImplicitSolveMixed
void FITRbfHermite::Set( const Array< R3Pt > &in_aptLocsOnly, 
		 const Array< std::pair<R3Pt,R3Vec> > &in_aptNorms, 
		 const Array< std::pair<R3Pt,R3Vec> > &in_aptTangents )
{
	int nPts = in_aptLocsOnly.num();
	int nVecs = in_aptNorms.num();
	int nTangents = in_aptTangents.num();
	nPtsOnly = nPts;
	nPtsNorms = nVecs;
	nPtsTangents = nTangents;
	
	int nVecsX = nPts + nVecs + nTangents;
	int nVecsY = nVecsX + nVecs;
	int nVecsZ = nVecsY + nVecs;
	
	int nTangNorms = nVecsZ + nVecs;
	
	int nB = nTangNorms + nTangents;
	int nA = nB+1;
	
	const int matSize = nA+3;
	FITTools_LS oLS( matSize, matSize, 1 );
	
	m_aptLocs.need( nVecsX );
	// concatenate ptsOnly, ptsNorms, and ptsTangent
	for (int i=0; i<nVecsX; i++) {
		if (i<nPts) {
			m_aptLocs[i] = in_aptLocsOnly[i];
		}
		else if (i<nPts+nVecs) {
			m_aptLocs[i] = in_aptNorms[i-nPts].first;
		}
		else {
			m_aptLocs[i] = in_aptTangents[i-nVecs-nPts].first;
		}
	}

	R3Vec diff; 
	double len, lenCub;
	
	// Point constraints
	for (int k=0; k<nVecsX; k++) {
		// Upper left square, symmetric matrix
		// point kernel for point constraint rows
		for (int m=k+1; m<nVecsX; m++) {
			// Shift each kernel phi by m_aptLocs[k]
			// positional constraint
			diff = m_aptLocs[k] - m_aptLocs[m];
			lenCub = pow(sqrt(pow(diff[0],2) + pow(diff[1],2) + pow(diff[2],2)),3);
			
			oLS.Desired(k,m) = lenCub; //alpha
			oLS.Desired(m,k) = lenCub; //alpha, symmetric
		}
		
		// f( pt(k) ) for the normal kernels for the point constraints
		for (int m=0; m<nVecs; m++) {
			// Shift each kernel phi by m_aptLocs[k]
			// positional constraint
			diff = m_aptLocs[k] - in_aptNorms[m].first;
			len = sqrt(pow(diff[0],2) + pow(diff[1],2) + pow(diff[2],2));
			
			oLS.Desired(k,nVecsX+m) = -3 * diff[0] * len; //beta
			oLS.Desired(k,nVecsY+m) = -3 * diff[1] * len;
			oLS.Desired(k,nVecsZ+m) = -3 * diff[2] * len;
		}
		
		// f( pt(k) ) for the tangent kernels for the point constraints
		for (int m=0; m<nTangents; m++) {
			// Shift each kernel phi by m_aptLocs[k]
			// positional constraint
			diff = m_aptLocs[k] - in_aptTangents[m].first;
			len = sqrt(pow(diff[0],2) + pow(diff[1],2) + pow(diff[2],2));
			
			oLS.Desired(k,nTangNorms+m) = -3 * (diff[0]+diff[1]+diff[2]) * len; //all beta are the same
		}
		
		oLS.Desired(k,nB) = 1;	//b
		oLS.Desired(k,nA) = (m_aptLocs[k])[0];	//a dot x
		oLS.Desired(k,nA+1) = (m_aptLocs[k])[1];
		oLS.Desired(k,nA+2) = (m_aptLocs[k])[2];
		
		// sum of <alpha,x> + beta = 0 (<alpha,x> terms)
		oLS.Desired(nA,k) = (m_aptLocs[k])[0];
		oLS.Desired(nA+1,k) = (m_aptLocs[k])[1];
		oLS.Desired(nA+2,k) = (m_aptLocs[k])[2];
	}
	
	// Normal Constraints
	for (int k=0; k<nVecs; k++) {
		// f'(x) for the point derivative (lower left)
		for (int m=0; m<nVecsX; m++) {
			// Shift each kernel phi by ptsNorms[k]
			// positional constraint
			diff = in_aptNorms[k].first - m_aptLocs[m];
			len = sqrt(pow(diff[0],2) + pow(diff[1],2) + pow(diff[2],2));
			
			oLS.Desired(nVecsX+k,m) = 3 * diff[0] * len; //beta
			oLS.Desired(nVecsY+k,m) = 3 * diff[1] * len;
			oLS.Desired(nVecsZ+k,m) = 3 * diff[2] * len;
		}
		
		// f''(x) the Hessian (right bottom half) for the normals
		for (int m=0; m<nVecs; m++) {
			if (k != m) {
				diff = in_aptNorms[k].first - in_aptNorms[m].first;
				len = sqrt(pow(diff[0],2) + pow(diff[1],2) + pow(diff[2],2));
				ASSERT(len>1e-6);
				
				oLS.Desired(nVecsX+k,nVecsX+m) = -3 * pow(diff[0],2) / len - 3 * len; //beta, hessian
				oLS.Desired(nVecsY+k,nVecsY+m) = -3 * pow(diff[1],2) / len - 3 * len;
				oLS.Desired(nVecsZ+k,nVecsZ+m) = -3 * pow(diff[2],2) / len - 3 * len;
				
				oLS.Desired(nVecsX+k,nVecsY+m) = -3 * diff[0] * diff[1] / len;
				oLS.Desired(nVecsY+k,nVecsX+m) = -3 * diff[1] * diff[0] / len;
				
				oLS.Desired(nVecsY+k,nVecsZ+m) = -3 * diff[1] * diff[2] / len;
				oLS.Desired(nVecsZ+k,nVecsY+m) = -3 * diff[2] * diff[1] / len;
				
				oLS.Desired(nVecsX+k,nVecsZ+m) = -3 * diff[2] * diff[0] / len;
				oLS.Desired(nVecsZ+k,nVecsX+m) = -3 * diff[0] * diff[2] / len;
			}
		}
		
		// f''(x), the Hessian (right bottom half) for the tangents
		for (int m=0; m<nTangents; m++) {
			diff = in_aptNorms[k].first - in_aptTangents[m].first;
			len = sqrt(pow(diff[0],2) + pow(diff[1],2) + pow(diff[2],2));
			ASSERT(len>1e-6);
			
			oLS.Desired(nVecsX+k,nTangNorms+m) = -3 * ( pow(diff[0],2) + diff[0]*diff[1] + diff[0]*diff[2] ) / len - 3 * len; // beta, hessian
			oLS.Desired(nVecsY+k,nTangNorms+m) = -3 * ( pow(diff[1],2) + diff[1]*diff[0] + diff[1]*diff[2] ) / len - 3 * len; // beta, hessian
			oLS.Desired(nVecsZ+k,nTangNorms+m) = -3 * ( pow(diff[2],2) + diff[2]*diff[0] + diff[2]*diff[1] ) / len - 3 * len; // beta, hessian
		}
		
		//deriv of x y z
		oLS.Desired(nVecsX+k,nA) = 1;
		oLS.Desired(nVecsY+k,nA+1) = 1;
		oLS.Desired(nVecsZ+k,nA+2) = 1;
		
		//Set equal to norm
		oLS.Rhs(nVecsX+k,0) = (in_aptNorms[k].second)[0];
		oLS.Rhs(nVecsY+k,0) = (in_aptNorms[k].second)[1];
		oLS.Rhs(nVecsZ+k,0) = (in_aptNorms[k].second)[2];
	}
	
	// Tangent Constraints
	//double derivTemp[3][matSize];
	Array< Array<double> > derivTemp;
	derivTemp.need(3);
	for (int i=0; i<derivTemp.num(); i++) {
		derivTemp[i].need(matSize);
	}
	for (int k=0; k<nTangents; k++) {
		//f'(x) for the point derivative (lower left) dotted with the tangent vector
		for (int m=0; m<nVecsX; m++) {
			//Shift each kernel phi by ptsTangents[k]
			//positional constraint
			diff = in_aptTangents[k].first - m_aptLocs[m];
			len = sqrt(pow(diff[0],2) + pow(diff[1],2) + pow(diff[2],2));
			
			//Sum of Tx grad f/x, Ty grad f/y, Tz grad f/z
			derivTemp[0][m] = 3 * diff[0] * len; //beta
			derivTemp[1][m] = 3 * diff[1] * len;
			derivTemp[2][m] = 3 * diff[2] * len;
		}
		
		//f''(x), the Hessian (right bottom half) for the normals dotted with the tangent vector
		for (int m=0; m<nVecs; m++) {
			diff = in_aptTangents[k].first - in_aptNorms[m].first;
			len = sqrt(pow(diff[0],2) + pow(diff[1],2) + pow(diff[2],2));
			ASSERT(len>1e-6);
			
			derivTemp[0][nVecsX+m] = -3 * pow(diff[0],2) / len - 3 * len; //beta, hessian
			derivTemp[1][nVecsY+m] = -3 * pow(diff[1],2) / len - 3 * len;
			derivTemp[2][nVecsZ+m] = -3 * pow(diff[2],2) / len - 3 * len;
			
			derivTemp[0][nVecsY+m] = -3 * diff[0] * diff[1] / len;
			derivTemp[1][nVecsX+m] = -3 * diff[1] * diff[0] / len;
			
			derivTemp[1][nVecsZ+m] = -3 * diff[1] * diff[2] / len;
			derivTemp[2][nVecsY+m] = -3 * diff[2] * diff[1] / len;
		
			derivTemp[0][nVecsZ+m] = -3 * diff[2] * diff[0] / len;
			derivTemp[2][nVecsX+m] = -3 * diff[0] * diff[2] / len;
		}
		
		//f''(x), the Hessian (right bottom half) for the tangents dotted with the tangent vector
		for (int m=0; m<nTangents; m++) {
			if (k != m) {
				diff = in_aptTangents[k].first - in_aptTangents[m].first;
				len = sqrt(pow(diff[0],2) + pow(diff[1],2) + pow(diff[2],2));
				ASSERT(len>1e-6);
				
				derivTemp[0][nTangNorms+m] = -3 * ( pow(diff[0],2) + diff[0]*diff[1] + diff[0]*diff[2] ) / len - 3 * len; //beta, hessian
				derivTemp[1][nTangNorms+m] = -3 * ( pow(diff[1],2) + diff[1]*diff[0] + diff[1]*diff[2] ) / len - 3 * len; //beta, hessian
				derivTemp[2][nTangNorms+m] = -3 * ( pow(diff[2],2) + diff[2]*diff[0] + diff[2]*diff[1] ) / len - 3 * len; //beta, hessian
			}
			else {
				derivTemp[0][nTangNorms+m]=derivTemp[0][nTangNorms+m-1];
				derivTemp[1][nTangNorms+m]=derivTemp[1][nTangNorms+m-1];
				derivTemp[2][nTangNorms+m]=derivTemp[2][nTangNorms+m-1];
			}

		}
		
		//deriv of x y z
		derivTemp[0][nA] = 1;
		derivTemp[1][nA+1] = 1;
		derivTemp[2][nA+2] = 1;
		derivTemp[0][nB] = 0;
		derivTemp[1][nB] = 0;
		derivTemp[2][nB] = 0;
		
		//Now take dot product
		for (int i=0; i<matSize; i++) {
			oLS.Desired(nTangNorms+k,i) = derivTemp[0][i]*(in_aptTangents[k].second)[0] + 
								 derivTemp[1][i]*(in_aptTangents[k].second)[1] + 
								 derivTemp[2][i]*(in_aptTangents[k].second)[2];
		}
	}

	// sum of alpha = 0
	for (int i=0; i<nVecsX; i++) {
		oLS.Desired(nB,i) = 1;
	}
	
	//sum of <alpha,x> + beta = 0 (beta coefficients must be 1)
	for (int i=nVecsX; i<nVecsY; i++) {
		oLS.Desired(nA,i) = 1;
	}
	for (int i=nVecsY; i<nVecsZ; i++) {
		oLS.Desired(nA+1,i) = 1;
	}
	for (int i=nVecsZ; i<nTangNorms; i++) {
		oLS.Desired(nA+2,i)=1;
	}
	for (int i=nTangNorms; i<nB; i++) {
		oLS.Desired(nA,i) = 1;
		oLS.Desired(nA+1,i) = 1;
		oLS.Desired(nA+2,i) = 1;
	}
	
	//ofstream out1, out2;
	//out1.open("clapack.txt");
	//out2.open("weights.txt");
	//oLS.Print(out1,false);
	
	// Solve
	m_aptLambda.need(matSize);
	VERIFY( oLS.Solve() );
	for (int i=0; i<matSize; i++) {
		//out2 << oLS.Solution(i,0) << "\n";
		m_aptLambda[i] = oLS.Solution(i,0);
	}

	//out1.close();
	//out2.close();
}

static FITRbfHermite * s_hrbf;
void FITRbfHermite::SetThis()
{
	s_hrbf = this;
}

double FITRbfHermite::DistFunc(const R3Pt &in_pt)
{
	return s_hrbf->Eval(in_pt);
}

/// Equiv to HRBFImplicitMixed
double FITRbfHermite::Eval( const R3Pt &in_pt )
{
	const int nPts = NCenters();
	Array< R3Vec > diff(nPts);
	Array< double > kernelEval(m_aptLambda.num());
	Array< double > len(nPts);

	
	for (int i=0; i<nPts; i++) {
		diff[i] = in_pt - m_aptLocs[i];
		len[i] = sqrt( pow(diff[i][0],2) + pow(diff[i][1],2) + pow(diff[i][2],2) );
		kernelEval[i] = kernelFunc(in_pt,i);
		//kernelEval[i] = len[i]*len[i]*len[i];
	}
	
	
	// Sum of beta*diffs
	Array< double > lenVecs(nPtsNorms);
	for (int i=0; i<nPtsNorms; i++) {
		lenVecs[i] = -3.0 * len[nPtsOnly+i];
	}
	for (int i=0; i<nPtsNorms; i++) {
		kernelEval[nPts+i] = diff[nPtsOnly+i][0] * lenVecs[i];
		kernelEval[nPts+nPtsNorms+i] = diff[nPtsOnly+i][1] * lenVecs[i];
		kernelEval[nPts+2*nPtsNorms+i] = diff[nPtsOnly+i][2] * lenVecs[i];
	}
	
	// Sum of tangent beta * diffs
	// Same as normal constraint except betas are all the same
	for (int i=0; i<nPtsTangents; i++) {
		kernelEval[nPts+3*nPtsNorms+i] = (diff[nPtsOnly+nPtsNorms+i][0]+diff[nPtsOnly+nPtsNorms+i][1]+diff[nPtsOnly+nPtsNorms+i][2]) * (-3*len[nPtsOnly+nPtsNorms+i]);
	}
	
	// <b, xyz> and a
	kernelEval[nPts+3*nPtsNorms+nPtsTangents] = 1;
	kernelEval[nPts+3*nPtsNorms+nPtsTangents+1] = in_pt[0];
	kernelEval[nPts+3*nPtsNorms+nPtsTangents+2] = in_pt[1];
	kernelEval[nPts+3*nPtsNorms+nPtsTangents+3] = in_pt[2];
	
	// Sum of alpha * pts
	double val = 0;
	for (int i=0; i<m_aptLambda.num(); i++) {
		//cout << m_aptLambda[i] << "\n";
		val += m_aptLambda[i] * kernelEval[i];
	}
	
    return val;
}





/// Equiv to HRBFImplicitMixedGradient
R3Vec FITRbfHermite::Gradient( const R3Pt &in_pt ) const
{
	R3Vec grad(0,0,0);
	int nPts = NCenters();
	Array< R3Vec > diff(nPts);
	Array< R3Vec > kernelEvalXYZ(m_aptLambda.num());//(m_aptLambda.num())(3);
	Array< double > len(nPts);
	Array< double > lenScaled(nPts);
	
	//Point derivatives
	for (int i=0; i<nPts; i++) {
		diff[i] = in_pt - m_aptLocs[i];
		
		len[i] = sqrt(pow(diff[i][0],2) + pow(diff[i][1],2) + pow(diff[i][2],2));
		lenScaled[i] = 3 * len[i];
		kernelEvalXYZ[i][0] = diff[i][0] * lenScaled[i];
		kernelEvalXYZ[i][1] = diff[i][1] * lenScaled[i];
		kernelEvalXYZ[i][2] = diff[i][2] * lenScaled[i];
	}
	
	double xD, yD, zD, lD;
	//Sum of beta * diffs
	for (int k=0; k<nPtsNorms; k++) {
		xD = diff[nPtsOnly+k][0];
		yD = diff[nPtsOnly+k][1];
		zD = diff[nPtsOnly+k][2];
		lD = len[nPtsOnly+k];
		
		if (lD>0) {
			kernelEvalXYZ[nPts+k][0] = -3*xD*xD/lD - 3*lD;
			kernelEvalXYZ[nPts+k][1] = -3*xD*yD/lD;
			kernelEvalXYZ[nPts+k][2] = -3*xD*zD/lD;
			
			kernelEvalXYZ[nPts+nPtsNorms+k][0] = -3*yD*xD/lD;
			kernelEvalXYZ[nPts+nPtsNorms+k][1] = -3*yD*yD/lD - 3*lD;
			kernelEvalXYZ[nPts+nPtsNorms+k][2] = -3*yD*zD/lD;
			
			kernelEvalXYZ[nPts+2*nPtsNorms+k][0] = -3*zD*xD/lD;
			kernelEvalXYZ[nPts+2*nPtsNorms+k][1] = -3*zD*yD/lD;
			kernelEvalXYZ[nPts+2*nPtsNorms+k][2] = -3*zD*zD/lD - 3*lD;
		}
	}
	
	//Sum of tangent beta*diffs
	for (int k=0; k<nPtsTangents; k++) {
		xD = diff[nPtsOnly+nPtsNorms+k][0];
		yD = diff[nPtsOnly+nPtsNorms+k][1];
		zD = diff[nPtsOnly+nPtsNorms+k][2];
		lD = len[nPtsOnly+nPtsNorms+k];
		
		if (lD>0) {
			kernelEvalXYZ[nPts+3*nPtsNorms+k][0] = -3*xD*(xD+yD+zD)/lD - 3*lD;
			kernelEvalXYZ[nPts+3*nPtsNorms+k][1] = -3*yD*(xD+yD+zD)/lD - 3*lD;
			kernelEvalXYZ[nPts+3*nPtsNorms+k][2] = -3*zD*(xD+yD+zD)/lD - 3*lD;
		}
	}
	
	//<b,xyz> and a
	kernelEvalXYZ[nPts+3*nPtsNorms+nPtsTangents+1][0] = 1;
	kernelEvalXYZ[nPts+3*nPtsNorms+nPtsTangents+2][1] = 1;
	kernelEvalXYZ[nPts+3*nPtsNorms+nPtsTangents+3][2] = 1;
	
	//Sum of alpha * pts
	for (int i=0; i<m_aptLambda.num(); i++) {
		grad[0] += m_aptLambda[i] * kernelEvalXYZ[i][0];
		grad[1] += m_aptLambda[i] * kernelEvalXYZ[i][1];
		grad[2] += m_aptLambda[i] * kernelEvalXYZ[i][2];
	}
	
	return grad;
}

FITRbfHermite::FITRbfHermite()
{ 

}
FITRbfHermite::~FITRbfHermite()
{
	
}