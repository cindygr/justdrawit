/*
 *  CurveRibbon.h
 *  SketchCurves
 *
 *  Created by Cindy Grimm on 9/20/11.
 *  Copyright 2011 Washington University in St. Louis. All rights reserved.
 *
 */

#ifndef _CURVE_RIBBON_DEFS_H_
#define _CURVE_RIBBON_DEFS_H_

#include "Curve.h"

/*
 * A curve plus normal information
 *   Pulled out from curve class 9/11. Computing normals
 *   is expensive and the normals are not really used anywhere
 *   except for drawing. 
 *
 *   So now keep just one copy for each group curve that has 
 *   normals.
 */
class CurveRibbon : public Curve {
private:
    // Derived normals
    Array<R3Vec> m_avecNormal;
        
    /// For propagating normal constraints around the curve; could be from best guess normals or normal constraints
    void PropagateNormals( const Array< R3Vec > &in_avecNorm, const Array< int > &in_aiPts );
    /// Fix flips; make sure pins are still set in the right direction
    void FixNormalFlips( const Array< int > &in_aiPts );

    void SetNormalsSimple();
    
public:
	/**@name Accessors */
	//@{
	/// The calculated normal
	const R3Vec &GetNormalAtPoint( const int in_i ) const { return m_avecNormal[IndexWrap(in_i)]; };
	/// The calculated normal
	const R3Vec &GetNormalAtPointClamp( const int in_i ) const { return m_avecNormal[IndexClamp(in_i)]; };
	/// Evaluate at the t value in_dT in [0,1] (linear interpolation)
	R3Vec Normal( const double in_dT ) const;
	/// tangent, normal, bi-normal in_dT in [0,1] (linear interpolation)
	std::pair<R3Pt, R3Matrix> Frame( const double in_dT ) const;
    //@}
    
    /**@name Constructors/editors */
    //@{
    /// Set normals for the curve. Expensive.    
    void SetNormalData( const Array< R3Vec > &in_avecNorm, const Array< int > &in_aiPts );
	/// Average the normals, keeping end points the same
    void SmoothNormals( const double in_dTStart = 0.0, const double in_dTEnd = 1.0, const int in_iNLoops = 1 );
    ///
    void Clear();
    ///
    CurveRibbon &operator=( const CurveRibbon &in_crv );
    /// Copy curve then set normal data to something simple
    //void SetToCurveNoNormals( const Curve &in_crv );
    /// Copy curve data
    void SetToCurve( const Curve &in_crv );
    ///
    CurveRibbon( const CurveRibbon &in_crv ) { *this = in_crv; }
    //@}

	/**@name Properties of the curve */
	//@{
	/// Average the normals along the curve
    R3Vec AverageNormal() const;
	/// Given a segment of the curve, return a good look and up vector
    std::pair<R3Vec,R3Vec> GetBestView( const std::pair<double,double> &in_dTs ) const;
    //@}
    
    CurveRibbon( ) { }
    ~CurveRibbon() {}
    
    void Write( ofstream &out ) const;
    void WriteSimple( ofstream &out ) const;
    void Read( ifstream &in );
    void ReadTangentNormal( ifstream &in );
};

#endif
