/*
 *  DrawState.cpp
 *  SketchCurves
 *
 *  Created by Cindy Grimm on 2/24/2011
 *  Copyright 2011 Adobe corp. All rights reserved.
 *
 */


#include "DrawState.h"
#include <OpenGL/OGLShader.h>
#include "UserInterface.h"

#include <FL/Fl.H>
#include <FL/Fl_Shared_Image.H>
#include <FL/Fl_PNG_Image.H>

const bool s_bTrace = false;

/* Read in a single texture with the given file name, depth (3 for rgb, 4 if there's an
 * alpha channel). Stores the texture enum in the in_iEnum slot */
static void ReadImage( Fl_Shared_Image *&img, const string &strFName, const int in_iDepth )
{
	// Read in the texture into memory
	img = Fl_Shared_Image::get(strFName.c_str());		// load texturemap
	if ( ! img ) {
		cerr << "Unknown image format or file not found " << strFName << "\n";
		return;
	} else {
		cout << "Image loaded " << strFName << " size " << img->w() << " " << img->h() << " " << img->d() << " " << img->count() << " " << img->ld() << " " << img->w() * img->h() << "\n";
	}
	if ( img->d() != in_iDepth ) {
		cerr << "Image depth is " << img->d() << " expected " << in_iDepth << "\n";
	}
}


/* Read in a single texture with the given file name, depth (3 for rgb, 4 if there's an
 * alpha channel). Stores the texture enum in the in_iEnum slot */
void DrawState::ReadTexture( const string &in_fname, const int in_iDepth, const int in_iEnum )
{
	// Read in the texture into memory
	Fl_Shared_Image *img = NULL;
    ReadImage( img, in_fname, in_iDepth );

	// Bind this texture to this id in opengl
	glBindTexture( GL_TEXTURE_2D, m_agEnumTexture[in_iEnum]);
	
	if ( glGetError() ) {
		cerr << "ERR: Read texture, GLError after bind " << glGetError() << "\n";
	}
	
	/* Load stroke texture with texture params right. */
	glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
	glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
	
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	if ( in_iDepth == 3 ) {
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, img->w(), img->h(), 0,
					 GL_RGB, GL_UNSIGNED_BYTE, img->data()[0]);
	} else {
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, img->w(), img->h(), 0,
					 GL_RGBA, GL_UNSIGNED_BYTE, img->data()[0]);
	}
	
	img->release();		
	
	if ( glGetError() ) {
		cerr << "ERR: GLError after tex image " << glGetError() << "\n";
	}
}

/* Actual file names of textures */
static char s_astrNames[DrawState::NUM_TEXTURES][30] = 
{
	"Stroke.png",
	"StrokeMenu.png",
	"CameraMenu.png",
	"CurveMenu.png",
	"DragIcon.png",
	"LineStroke.png",
	"FragmentStroke.png"
};

/* For each texture, read it in if they aren't already read in. Files are expected to be in ./ShaderData.png */
void DrawState::SetupTextures()
{
	if ( m_agEnumTexture.size() == 0 ) {
		fl_register_images();                       // initialize image lib
        
        m_agEnumTexture.resize( NUM_TEXTURES );
		glGenTextures(NUM_TEXTURES, &m_agEnumTexture[0]);
		
        const string strBase = GetBaseDirectory( "Textures/" );
        for ( int i = 0; i < NUM_TEXTURES; i++ ) {
            ReadTexture( strBase + std::string( s_astrNames[i] ), 4, i );
        }
	}
	glEnable( GL_TEXTURE_2D );
}

/* For each image, read it in if they aren't already read in. Files are expected to be in ./ShaderData.png */
static Fl_Shared_Image *s_aopImages[DrawState::NUM_IMAGES];

/* Actual file names of images */
static char s_astrImageNames[DrawState::NUM_IMAGES][50] = 
{
    "InstructionsOverview.png",
    "Instructions2D.png",
    "Instructions2DCamera.png",
    "Instructions2DMenu.png",
    "Instructions2DTips.png",
    "Instructions3D.png",
    "Instructions3DDraw.png",
    "Instructions3DDrawPlane.png",
    "Instructions3DInflation.png",
    "Instructions3DExtrusion.png",
    "Instructions3DCurve.png",
    "Instructions3DTips.png",
    "InstructionsCamera.png",
    "InstructionsCameraMenu.png",
    "InstructionsSurface.png",
    "InstructionsSurfacePoints.png",
    "InstructionsSurfaceNormals.png",
    "InstructionsSurfaceTips.png"
};


void DrawState::SetupImages()
{
    static bool sbRead = false;
	if ( sbRead == false ) {
		fl_register_images();                       // initialize image lib
        
        const string strBase = GetBaseDirectory( "Instructions/" );
        for ( int i = 0; i < NUM_IMAGES; i++ ) {
            ReadImage( s_aopImages[i], strBase + std::string( s_astrImageNames[i] ), 3 );
        }
        
        sbRead = true;
	}
}

Fl_Shared_Image *DrawState::GetImage( const ImageNames &in_im )
{
    SetupImages();
    
    return s_aopImages[(int) in_im];
}


