/*
 *  DrawData.h
 *  SketchCurves
 *
 *  Created by Cindy Grimm on 6/29/12.
 *  Copyright 2012 Washington University in St. Louis. All rights reserved.
 *
 */

#ifndef _DRAW_DATA_H
#define _DRAW_DATA_H

#include <vector>
#include <utils/Rn_Defs.H>
#include <utils/R3_Plane.H>
#include <utils/Utils_Color.H>

/*
 * Load up draw state; base class for CurveDrawData
 *
 * Lazy evaluation; data only changes on one of the Update calls. 
 *
 * This class reads and stores the param data file information, so if that
 * file is re-read, need to update the local params here (and recalc the curves)
 */
class DrawData
{
public:
	// Affects the color and transparency of the curve
	// Hidden are transparent and white
	// Active is slightly reddish
	// Inactive is yellow
	// Boundary (for inflation surface) is green
	typedef enum {
		HIDDEN = 0,
		ACTIVE,
		INACTIVE,
        BOUNDARY
	} CurveState;
	
protected:
	// Param data stuff, stored statically
	static UTILSColor s_colSelect, s_colActive, s_colInactive, s_colHidden, s_colPointConstraint, s_colBoundary;
	static double     s_dTransparencyHidden, s_dTaperPercentage;
	static R4Pt_f     s_colFragment;
	static int        s_iTubeDivs, s_iRibbonDivs;
	
	// Which state we are at the moment
	bool m_bHidden, m_bActive, m_bCastShadow;

	// Do the colors or the scales need to change?
    bool m_bUpdateColor, m_bUpdateScale;

	// What strip width we're using (in case changed in gui or by camera zoom)
	double m_dStripWidth, m_dStripHeight;
	R3Vec m_vecView, m_vecUp; // Store relevent camera information
	
	/// Make sure WorldStateChanged is called on an update
    /// This virtual method will get called by WorldStateChanged
    virtual void WorldStateUpdate( const bool in_bWidthSame, const bool in_bHeightSame, const bool in_bViewSame ) = 0;
    void WorldStateChanged( );
    

    
    DrawData &operator=( const DrawData &in_data );
    DrawData( const DrawData & );
	
public:
	/**@name Access for the draw stuff that is not handled here */
	//@{
    ///
    bool IsHidden() const { return m_bHidden; }
    ///
    bool IsActive() const { return m_bActive; }
    ///
    bool IsShadow() const { return m_bCastShadow; }
	/// Number of divisions around tube
	int TubeDivs() const { return s_iTubeDivs; }
	//@}
	
	/// Call when param file is re-read
	static void ParamFileUpdated();
	
	/**@name Might need to update */
	//@{
    /// Just set hidden state
    void SetHidden( const bool in_bHidden );
    /// Toggle hidden state
    void ToggleHide();
    /// Just set active state
    void SetActive( const bool in_bActive );
    ///
    virtual void ForceUpdate() { m_bUpdateColor = m_bUpdateScale = true; }
    ///
    virtual void SetShadow( const bool in_bShadow );
    /// Sets geometry
    virtual void GeometryChanged() = 0;
    /// Need to change scale
    void AddedOrRemovedConstraint() { m_bUpdateScale = true; m_bUpdateColor = true; }
	//@}
    
    
	/**@name Drawing routines. In CurveDrawData_Draw */
	//@{
	/// Draw with given style in g_drawState
    virtual void Draw( ) const = 0;
	/// Draw as a shadow (projected and black)
    virtual void DrawShadow( ) const = 0;
	//@}
    
	DrawData( );
	~DrawData() {}
};

#endif