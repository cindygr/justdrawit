/*
 *  DrawState_Shader.cpp
 *  SketchCurves
 *
 *  Created by Cindy Grimm on 2/24/2011
 *  Copyright 2011 Adobe corp. All rights reserved.
 *
 */


#include "DrawState.h"
#include <OpenGL/OGLShader.h>
#include "UserInterface.h"
#include "ParamFile.h"

const bool s_bTrace = false;

/*
 * Set up open gl state and load the data for the given shader
 *
 * Sets up a vertex program and a fragment program
 */
void DrawState::StartShader( const ShaderType in_shType )
{
	glPushAttrib( GL_DEPTH_BUFFER_BIT | GL_TEXTURE_BIT | GL_LIGHTING_BIT | GL_COLOR_BUFFER_BIT | GL_POLYGON_BIT | GL_POINT_BIT | GL_LINE_BIT );
    
	glEnable( GL_DEPTH_TEST );
	glEnable( GL_LIGHTING );
    glEnable( GL_BLEND );
	glEnable( GL_POINT_SMOOTH );
	glEnable( GL_LINE_SMOOTH );
    glHint( GL_POINT_SMOOTH_HINT, GL_NICEST );
    glHint( GL_LINE_SMOOTH_HINT, GL_NICEST );
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glShadeModel( GL_SMOOTH );

	glPointSize( (GLfloat) g_drawState.LineWidth() );
	glLineWidth( (GLfloat) g_drawState.LineWidth() );
	
	/// In case someone forgets an end shader call
	if ( m_bInShader == true ) {
		cerr << "ERR: Start shader, already in shader\n";
	}
	m_bInShader = true;

	/// If the opengl state is messed up things may not look right even if the 
	if ( glGetError() ) {
		cerr << "ERR: DrawState Shader : GLError before shader " << glGetError() << "\n";
	}
	
    R4Matrix mat(R4Matrix::Identity());
    switch ( in_shType ) {
        case BOX_SHADING :
            m_opShader->BindVertexProgram(POSITION_NORMAL);
            m_opShader->BindFragmentProgram(COLOR_PASSTHROUGH);
            break;
        case LINE_SHADING : {
            m_opShader->BindVertexProgram(POSITION_NORMAL);
            m_opShader->BindFragmentProgram(WARM_COOL);
			m_opShader->SetVectorFP( "viewDir", m_vecView * -1.0 );
            break;
		}
        case SURFACE_SHADING :
            m_opShader->BindVertexProgram(POSITION_NORMAL);
            m_opShader->BindFragmentProgram(GREY_SCALE);
			m_opShader->SetVectorFP( "viewDir", UnitSafe(R3Vec(0.2, 0.2, 0.5)) );
            break;
        case STROKE_SHADING :
			SetupTextures();
            m_opShader->BindVertexProgram(POSITION_NORMAL_TEXTURE);
            m_opShader->BindFragmentProgram(STROKE_TEXTURE);
			glBindTexture(GL_TEXTURE_2D, m_agEnumTexture[STROKE] );
			m_opShader->SetTextureIdFP( "strokeTex", m_agEnumTexture[STROKE] );
			break;
        case FRAGMENT_SHADING :
			SetupTextures();
            m_opShader->BindVertexProgram(POSITION_NORMAL_TEXTURE);
            m_opShader->BindFragmentProgram(STROKE_TEXTURE);
			glBindTexture(GL_TEXTURE_2D, m_agEnumTexture[FRAGMENT_STROKE] );
			m_opShader->SetTextureIdFP( "strokeTex", m_agEnumTexture[FRAGMENT_STROKE] );
			break;
        case LINE_STROKE_SHADING :
			SetupTextures();
            m_opShader->BindVertexProgram(POSITION_NORMAL_TEXTURE);
            m_opShader->BindFragmentProgram(STROKE_TEXTURE);
			glBindTexture(GL_TEXTURE_2D, m_agEnumTexture[LINE_STROKE] );
			m_opShader->SetTextureIdFP( "strokeTex", m_agEnumTexture[LINE_STROKE] );
			break;
		case SCREENSPACE_STROKE :
			SetupTextures();
            m_opShader->BindVertexProgram(POSITION_TEXTURE);
            m_opShader->BindFragmentProgram(STROKE_TEXTURE);
			glBindTexture(GL_TEXTURE_2D, m_agEnumTexture[STROKE] );
			m_opShader->SetTextureIdFP( "strokeTex", m_agEnumTexture[STROKE] );
            break;
        case STRIPE_V_SHADING :
            m_opShader->BindVertexProgram(POSITION_NORMAL_TEXTURE);
            m_opShader->BindFragmentProgram(STRIPE_V);
			m_opShader->SetVectorFP( "viewDir", m_vecView * -1.0 );
            break;
        case STRIPE_BAND_SHADING :
            m_opShader->BindVertexProgram(POSITION_NORMAL_TEXTURE);
            m_opShader->BindFragmentProgram(STRIPE_BAND);
			m_opShader->SetVectorFP( "viewDir", m_vecView * -1.0 );
            break;
        case SHADOW_SHADING :
            m_opShader->BindVertexProgram(POSITION_TEXTURE);
            m_opShader->BindFragmentProgram(SHADOW);
            break;
        case SCREENSPACE_SHADING :
            m_opShader->BindVertexProgram(POSITION_TEXTURE_2D);
            m_opShader->BindFragmentProgram(SHADOW);
            break;
        case MENU_3D :
            m_opShader->BindVertexProgram(POSITION_TEXTURE);
            m_opShader->BindFragmentProgram(TEXTURE_ONLY);
            break;
        default : ASSERT(FALSE);
    }    
	if ( glGetError() ) {
		cerr << "ERR: DrawState Shader : GLError after shader " << glGetError() << "\n";
	}
	
}

/* Same as above, but for 2D menu textures */
void DrawState::StartMenuShader( const TextureNames in_tex, const bool in_bIs3D )
{
	glPushAttrib( GL_DEPTH_BUFFER_BIT | GL_TEXTURE_BIT | GL_LIGHTING_BIT | GL_COLOR_BUFFER_BIT | GL_POLYGON_BIT | GL_POINT_BIT | GL_LINE_BIT );
    
	glDisable( GL_DEPTH_TEST );
	glEnable( GL_LIGHTING );
    glEnable( GL_TEXTURE_2D );
    glEnable( GL_BLEND );
	glEnable( GL_POINT_SMOOTH );
	glEnable( GL_LINE_SMOOTH );
    glHint( GL_POINT_SMOOTH_HINT, GL_NICEST );
    glHint( GL_LINE_SMOOTH_HINT, GL_NICEST );
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glShadeModel( GL_SMOOTH );

	if ( m_bInShader == true ) {
		cerr << "ERR: Start shader, already in shader\n";
	}
	m_bInShader = true;
	
	SetupTextures();

    if ( in_bIs3D ) {
        m_opShader->BindVertexProgram(POSITION_TEXTURE);
        m_opShader->BindFragmentProgram(TEXTURE_ONLY);
    } else {
        m_opShader->BindVertexProgram(POSITION_TEXTURE_2D);
        m_opShader->BindFragmentProgram(TEXTURE_ONLY);
    }
    glBindTexture( GL_TEXTURE_2D, m_agEnumTexture[ (int) in_tex]);
    m_opShader->SetTextureIdFP( "menuTex", m_agEnumTexture[ (int) in_tex] );
}

/* Close out shader and pop attributes */
void DrawState::EndShader()
{
	m_opShader->DisablePrograms();
	
	glPopAttrib();
	
	if ( m_bInShader == false ) {
		cerr << "ERR: End shader, not in shader\n";
	}
	m_bInShader = false;
	
}

// Should match VertexShaderTypes enumerated types, although the names are different.
/// These are the actual .cg file names
static char s_strVertexShaderNames[DrawState::NUM_VERTEX][30] = 
{
	"VertexSimple",
	"VertexTexture",
	"VertexTextureOnly",
	"Vertex2D"
};

// Should match FragmentShaderTypes enumerated types
/// These are the actual .cg file names
static char s_strFragmentShaderNames[DrawState::NUM_FRAGMENT][30] = 
{
	"Box_fragment",
	"WarmCool_fragment",
	"GrayScale_fragment",
	"Stripe_fragment",
	"Stripe_tube_fragment",
	"Stroke_fragment",
	"Shadow_fragment",
	"Menu_fragment"
};
	

/* Re-read shader files. Files are expected to be in ./ShaderData.cg */
void DrawState::ReadShaders()
{
    if ( m_opShader ) {
        delete m_opShader;
    }
    
    const string strBase = GetBaseDirectory( "Shaders/" );

	cerr << "DrawState::ReadShaders: Loading shaders, directory " << strBase << "\n";

    m_opShader = new OGLShader2;
	m_opShader->SetNumberVertexShaders(NUM_VERTEX);
	m_opShader->SetNumberFragmentShaders(NUM_FRAGMENT);

	for ( int i = 0; i < NUM_VERTEX; i++ ) {
		m_opShader->LoadVertexShader( i, ( strBase + std::string(s_strVertexShaderNames[i]) + std::string(".cg") ).c_str(), s_strVertexShaderNames[i] );
	}
	for ( int i = 0; i < NUM_FRAGMENT; i++ ) {
		m_opShader->LoadFragmentShader( i, ( strBase + std::string(s_strFragmentShaderNames[i]) + std::string(".cg") ).c_str(), s_strFragmentShaderNames[i] );
	}
}
