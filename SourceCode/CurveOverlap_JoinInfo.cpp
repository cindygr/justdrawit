/*
 *  CurveOverlap_JoinInfo.cpp
 *  SketchCurves
 *
 *  Created by Cindy Grimm on 3/9/11.
 *  Copyright 2011 Adobe Corp.. All rights reserved.
 *
 */


/*
 *  CurveOverlap_OverlapInfo.cpp
 *  SketchCurves
 *
 *  Created by Cindy Grimm on 2/4/11.
 *  Copyright 2011 Washington University in St. Louis. All rights reserved.
 *
 */

#include "CurveOverlap.h"
#include "DrawState.h"
#include "ParamFile.h"

void CurveOverlap::JoinInfo::ZeroOut()
{
    m_dCrvT = m_dStrokeT = -1.0;
	m_ptCrv = m_ptStroke = R2Pt(1e30, 1e30);
	m_vecJoin = m_vecCrv = m_vecStroke = R2Vec(1e30, 1e30);
	m_dDist = 1e30;
	m_dAngCrv = 1e30;
	m_dAngStroke = 1e30;
	m_dScore = 1e30;

    m_crvEnd = m_strokeEnd = NO_ENDS;
    m_bStrokeReversed = false;
}

void CurveOverlap::JoinInfo::Print() const
{
    cout << "Join Info: Ts " << m_dCrvT << " " << m_dStrokeT;
    cout << " VecJoin: " << m_vecJoin;
    cout << " Dist " << m_dDist;
    cout << " Ang " << m_dAngCrv << " " << m_dAngStroke;
	cout << " Score " << m_dScore;
    cout << " reversed " << ( m_bStrokeReversed ? " t " : "f " );
    cout << " Crv end: "; 
    PrintCurveEnd( m_crvEnd );
    cout << " Stroke end: ";
    PrintCurveEnd( m_strokeEnd );
	if ( IsJoined( true ) ) {
		cout << " Join\n";
	} else {
		cout << " NO join\n";
	}
}

bool CurveOverlap::JoinInfo::IsJoined( const bool in_bPrintReason ) const
{	
	const double dDistSel = g_drawState.ScreenSelectionSize();
	const double dLenCap = g_paramFile.GetDouble("JoinLengthCap") * dDistSel;
	if ( m_dDist > dLenCap || RNIsZero( m_dDist ) ) {
		if ( s_bTraceJoin && in_bPrintReason ) cout << " join len failed " << m_dDist << " " << dLenCap << " ";
		return false;
	}

	const double dAngDiffCap = g_paramFile.GetDouble("JoinAngleDiffCap");
	const double dAngTotCap = g_paramFile.GetDouble("JoinAngleTotCap");
	
	const double dDotJoinCurve = Dot( UnitSafe( m_vecJoin ), m_vecCrv );
	const double dDotJoinStroke = Dot( UnitSafe( m_vecJoin ), m_vecStroke );
	const double dAngJoinCurve = DotToAngle( dDotJoinCurve );
	const double dAngJoinStroke = DotToAngle( dDotJoinStroke );
	
	// Look for the place where the angle change is split
	const double dAvgAngSplit = fabs( dAngJoinCurve - dAngJoinStroke );
	const double dAvgAngTotal = 0.5 * (dAngJoinCurve + dAngJoinStroke);
	
	if ( dAvgAngSplit > dAngDiffCap ) {
		if ( s_bTraceJoin && in_bPrintReason ) cout << " join angDiff failed " << dAvgAngSplit << " " << dAngDiffCap << " ";
		return false;
	}
	
	if ( dAvgAngTotal > dAngTotCap ) {
		if ( s_bTraceJoin && in_bPrintReason ) cout << " join angTot failed " << dAvgAngTotal << " " << dAngTotCap << " ";
		return false;
	}
	return true;
}

bool CurveOverlap::JoinInfo::IsBetter( const JoinInfo &in_info, const bool in_bPrintReason ) const
{
	if ( IsJoined() && !in_info.IsJoined() ) {
		if ( s_bTraceJoin && in_bPrintReason ) cout << " Better joined E1\n";
		return true;
	}
	if ( !IsJoined() && in_info.IsJoined() ) {
		if ( s_bTraceJoin && in_bPrintReason ) cout << " Better joined E2\n";
		return false;
	}
	
	const double dDistDiff = fabs( m_dDist - in_info.m_dDist );
	const double dDistAvg = 0.5 * ( m_dDist + in_info.m_dDist );
	if ( dDistDiff > 0.25 * dDistAvg ) {
		if ( s_bTraceJoin && in_bPrintReason ) cout << " Better distance " << dDistDiff << " " << dDistAvg << "\n";
		return m_dDist < dDistAvg;
	}
	if ( m_dAngCrv > M_PI / 2.0 && in_info.m_dAngCrv < M_PI / 2.0 ) {
		if ( s_bTraceJoin && in_bPrintReason ) cout << " Better angle E1 " << m_dAngCrv << " " << in_info.m_dAngCrv << "\n";
		return true;
	}
	if ( m_dAngCrv < M_PI / 2.0 && in_info.m_dAngCrv > M_PI / 2.0 ) {
		if ( s_bTraceJoin && in_bPrintReason ) cout << " Better angle E2 " << m_dAngCrv << " " << in_info.m_dAngCrv << "\n";
		return false;
	}
	return m_dScore < in_info.m_dScore;
}

/*
 * Factors:
 *   Total amount of angle change from one curve to the next
 *   If can balance angle differences
 *   Length: too close is bad, as is too far
 */
double CurveOverlap::JoinInfo::Score() const
{
	const double dLenCap = g_paramFile.GetDouble("JoinLengthCap") * g_drawState.ScreenSelectionSize();
	const double dAngDiffCap = g_paramFile.GetDouble("JoinAngleDiffCap");
	const double dAngTotCap = 1.5 * g_paramFile.GetDouble("JoinAngleTotCap");

	const double dDotJoinCurve = Dot( UnitSafe( m_vecJoin ), m_vecCrv );
	const double dDotJoinStroke = Dot( UnitSafe( m_vecJoin ), m_vecStroke );
	const double dAngJoinCurve = DotToAngle( dDotJoinCurve );
	const double dAngJoinStroke = DotToAngle( dDotJoinStroke );
	
	// Look for the place where the angle change is split
	const double dAvgAngSplit = fabs( dAngJoinCurve - dAngJoinStroke );
	const double dAvgAngTotal = 0.5 * (dAngJoinCurve + dAngJoinStroke);
	
	const double dDistScore = m_dDist / dLenCap;
	const double dAngScore = 0.25 * dAvgAngSplit / dAngDiffCap + 0.75 * dAvgAngTotal / dAngTotCap;
	
	if ( dDistScore > 0.75 ) {
		const double dZeroToOne = ( dDistScore - 0.75 ) / 0.25;
		return dAngScore * (1.0 + dZeroToOne);
	} else if ( dDistScore < 0.25 ) {
		const double dZeroToOne = dDistScore / 0.25;
		return dAngScore * (1.0 + dZeroToOne);
	}
	
	return dAngScore;
}

void CurveOverlap::JoinInfo::SetScore( ) 
{
    const double dPercAvg = g_paramFile.GetDouble("JoinAvgVsDiff");    
	
	m_dDist = ::Length( m_ptCrv - m_ptStroke );
	const double dDotJoinCurve = Dot( m_vecJoin, m_vecCrv );
	const double dDotJoinStroke = Dot( m_vecJoin, m_vecStroke );
	m_dAngCrv = DotToAngle( dDotJoinCurve );
	m_dAngStroke = DotToAngle( dDotJoinStroke );
	
	// Look for the place where the angle change is split
	const double dAvgAngSplit = fabs( m_dAngCrv - m_dAngStroke );
	const double dAvgAngTotal = 0.5 * (m_dAngCrv + m_dAngStroke);
	
	m_dScore = dPercAvg * dAvgAngTotal + (1.0 - dPercAvg) * dAvgAngSplit;
}


/*
 * Similar to the join routine in StrokeHistory
 *
 * Walk along the ends specified for each curve (Selection distance width)
 * Look for the best place to join the two curves
 *    Minimize the angle difference between the tangents and the join line
 *    Plus a bit of minimizing the actual angle between the tangents
 *    Don't let the join go backwards
 * Returns the points/tangents/t values of the joins
 */
void
CurveOverlap::FindBestJoin( JoinInfo &out_info,
						    const CurveEndType &in_curveEndType1, const CurveEndType &in_curveEndType2,
						    const bool in_bReverseCrv2 ) const 
{
	const double dDistSel = g_drawState.ScreenSelectionSize();
	const double dJoinStep = g_paramFile.GetDouble("SampleSizeJoinMergeSearchPerc");
	const double dJoinSearch = g_paramFile.GetDouble("JoinSearchRegionPerc");
	const double dJoinSearchCap = g_paramFile.GetDouble("JoinMergeMaximumSearchPerc");
	const double dTang = g_paramFile.GetDouble("TangentStepSizePerc");
	
	const double dTSearch1 = WINmin( dJoinSearchCap, dJoinSearch * dDistSel / m_crvProjected.Length() );
	const double dTSearch2 = WINmin( dJoinSearchCap, dJoinSearch * dDistSel / m_crvStroke.Length() );
	const double dTCrv1 = WINmin( dJoinStep * dDistSel / m_crvProjected.Length(), dTSearch1 / 4.0 );
	const double dTCrv2 = WINmin( dJoinStep * dDistSel / m_crvStroke.Length(), dTSearch2 / 4.0 );
	
	std::vector<double> adTCrv1, adTCrv2;
	std::vector<R2Pt> aptCrv1, aptCrv2;
	std::vector<R2Vec> avecCrv1, avecCrv2;
	
	/* Generate potential join points by sampling the ends of the two curves
	 * Only take points that have a tangent defined
	 */
	const double dTStart1 = (in_curveEndType1 == START_CURVE) ? 0.0 : WINmax( 0.5, 1.0 - dTSearch1 );
	const double dTEnd1 = (in_curveEndType1 == START_CURVE) ? WINmin( 0.5, 0.0 + dTSearch1 ) : 1.0;
	for ( double dT = WINmin( dTStart1, dTEnd1); dT <= WINmax( dTStart1, dTEnd1 ); dT += dTCrv1 ) {
		const R2Vec vecEndCurve = m_crvProjected( WINmin(1.0, dT + dTCrv1 * dTang ) ) - m_crvProjected( WINmax( 0.0, dT - dTCrv1 * dTang ) );
		if ( !RNIsZero( ::Length( vecEndCurve ) ) ) {
			adTCrv1.push_back( dT );
			aptCrv1.push_back( m_crvProjected( dT ) );
			avecCrv1.push_back( UnitSafe( vecEndCurve ) );
		}
	}
	const double dTStart2 = (in_curveEndType2 == START_CURVE) ? 0.0 : WINmax( 0.5, 1.0 - dTSearch2 );
	const double dTEnd2 = (in_curveEndType2 == START_CURVE) ? WINmin( 0.5, 0.0 + dTSearch2 ) : 1.0;
	for ( double dT = WINmin( dTStart2, dTEnd2); dT <= WINmax( dTStart2, dTEnd2 ); dT += dTCrv2 ) {
		const R2Vec vecEndCurve = m_crvStroke( WINmin(1.0, dT + dTCrv2 * dTang ) ) - m_crvStroke( WINmax( 0.0, dT - dTCrv2 * dTang ) );
		if ( !RNIsZero( ::Length( vecEndCurve ) ) ) {
			adTCrv2.push_back( dT );
			aptCrv2.push_back( m_crvStroke( dT ) );
			avecCrv2.push_back( UnitSafe( vecEndCurve ) );
		}
	}
	
    if ( s_bTraceJoinDetails ) {
		cout << "\nFindBestJoin: ";
		PrintCurveEnd(in_curveEndType1);
		cout << " ";
		PrintCurveEnd(in_curveEndType2);
	}
	
	out_info.ZeroOut();
	if ( adTCrv1.size() == 0 || adTCrv2.size() == 0 ) {
		if ( s_bTraceJoinDetails ) cout << "nFindBestJoin: No t values at ends\n";
		return;
	}
	
	/* Try all nm combinations */
	JoinInfo infoCurrent;
	
	if ( s_bTraceJoinDetails ) cout << "Pts " << aptCrv1.size() << " " << adTCrv1[0] << " " << adTCrv1.back() << " Pts " << aptCrv2.size() << " " << adTCrv2[0] << " " << adTCrv2.back() << "\n";
	for ( int i = 0; i < aptCrv1.size(); i++ ) {
		for ( int j = 0; j < aptCrv2.size(); j++ ) {
			infoCurrent.m_vecJoin = aptCrv2[j] - aptCrv1[i];
			const double dLenJoin = ::Length(infoCurrent.m_vecJoin);
			if ( RNIsZero( dLenJoin ) ) {
				continue;
			}

			infoCurrent.m_dCrvT = adTCrv1[i];
			infoCurrent.m_dStrokeT = adTCrv2[j];
			infoCurrent.m_ptCrv = aptCrv1[i];
			infoCurrent.m_ptStroke = aptCrv2[j];
			infoCurrent.m_vecCrv = avecCrv1[i];
			infoCurrent.m_vecStroke = in_bReverseCrv2 ? -avecCrv2[j] : avecCrv2[j];
			infoCurrent.m_vecJoin.Normalize();
			// Make it go from the end of the stroke to the start of the curve
			if ( in_curveEndType1 == START_CURVE ) {
				infoCurrent.m_vecJoin *= -1.0;
			}
			infoCurrent.SetScore();
			
			
			if ( infoCurrent.IsBetter( out_info, s_bTraceJoinDetails ) ) {
				out_info = infoCurrent;
				
				if ( s_bTraceJoinDetails == true ) {
					cout << " " << i << " " << j << " ";
					out_info.Print();
				}
			}
		}
	}
}


/*
 * See if the end of the stroke can be joined to the curve.
 * Try both orientations of the stroke.
 * There are only four ways the stroke can join to the curve:
 *   End of curve to start of stroke
 *   End of stroke to start of curve
 *   End of curve to end of stroke, stroke direction reversed
 *   Start of stroke to start of curve, stroke direction reversed
 */
void CurveOverlap::SetJoinData( ) 
{
	JoinInfo infoEndStart, infoStartEnd, infoEndStartReversed, infoStartEndReversed;
	
    if ( m_bCurveClosed ) {
        m_strokeJoinEnd1.ZeroOut();
        m_strokeJoinEnd2.ZeroOut();
        return;
    }
    
	FindBestJoin( infoEndStart, END_CURVE, START_CURVE, false );
	FindBestJoin( infoStartEndReversed, START_CURVE, START_CURVE, true );
	if ( infoEndStart.IsBetter( infoStartEndReversed ) ) {
		m_strokeJoinEnd1 = infoEndStart;
		m_strokeJoinEnd1.m_crvEnd = END_CURVE;
		m_strokeJoinEnd1.m_strokeEnd = START_CURVE;
		m_strokeJoinEnd1.m_bStrokeReversed = false;
	} else {
		m_strokeJoinEnd1 = infoStartEndReversed;
		m_strokeJoinEnd1.m_crvEnd = START_CURVE;
		m_strokeJoinEnd1.m_strokeEnd = END_CURVE;
		m_strokeJoinEnd1.m_bStrokeReversed = true;
	}

	FindBestJoin( infoStartEnd, START_CURVE, END_CURVE, false );
	FindBestJoin( infoEndStartReversed, END_CURVE, END_CURVE, true );
	if ( infoStartEnd.IsBetter( infoEndStartReversed ) ) {
		m_strokeJoinEnd2 = infoStartEnd;
		m_strokeJoinEnd2.m_crvEnd = START_CURVE;
		m_strokeJoinEnd2.m_strokeEnd = END_CURVE;
		m_strokeJoinEnd2.m_bStrokeReversed = false;
	} else {
		m_strokeJoinEnd2 = infoEndStartReversed;
		m_strokeJoinEnd2.m_crvEnd = END_CURVE;
		m_strokeJoinEnd2.m_strokeEnd = START_CURVE;
		m_strokeJoinEnd2.m_bStrokeReversed = true;
	}
}
