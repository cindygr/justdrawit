/*
 *  ParamFile.cpp
 *  SketchCurves
 *
 *  Created by Cindy Grimm on 2/20/2011
 *  Copyright 2011 Adobe Corp. All rights reserved.
 *
 */

#include "ParamFile.h"
#include "CurveDrawData.h"
#include "DrawState.h"
#include "PointConstraint.h"
#include "UserInterface.h"

// Get stuff out of the hash tables
UTILSColor ParamFile::GetColor( const std::string &in_str ) 
{
    const std::map<std::string, UTILSColor>::iterator itr = m_aMapColors.find( in_str );
    if ( itr == m_aMapColors.end() ) {
        cerr << "ERR: ParamFile, color not found " << in_str << "\n";
        return UTILSColor::GREY;
    }
    return itr->second;
}

double ParamFile::GetDouble( const std::string &in_str ) 
{
    const std::map<std::string, double>::iterator itr = m_aMapDoubles.find( in_str );
    if ( itr == m_aMapDoubles.end( ) ) {
        cerr << "ERR: ParamFile, double not found " << in_str << "\n";
        return 1.0;
    }
    return itr->second;
}


int ParamFile::GetInteger( const std::string &in_str ) 
{
    const std::map<std::string, int>::iterator itr = m_aMapIntegers.find( in_str );
    if ( itr == m_aMapIntegers.end( ) ) {
        cerr << "ERR: ParamFile, Integer not found " << in_str << "\n";
        return 1.0;
    }
    return itr->second;
}

void ParamFile::LoadDebuggingWindow() 
{
    Fl_Choice *opChoice = g_drawState.m_opUI->m_choiceParam;
    
    for ( map<std::string, UTILSColor>::iterator it = m_aMapColors.begin() ; it != m_aMapColors.end(); it++ ) {
        opChoice->add( it->first.c_str() );
    }
    for ( map<std::string, double>::iterator it = m_aMapDoubles.begin() ; it != m_aMapDoubles.end(); it++ ) {
        opChoice->add( it->first.c_str() );
    }
    for ( map<std::string, int>::iterator it = m_aMapIntegers.begin() ; it != m_aMapIntegers.end(); it++ ) {
        opChoice->add( it->first.c_str() );
    }
}

void ParamFile::ChangedParamFile()
{
    const Fl_Choice *opChoice = g_drawState.m_opUI->m_choiceParam;
    
    const char *strSearch = opChoice->text();
    const int iWhich = opChoice->value();
    
    if ( iWhich < m_aMapColors.size() ) {
        const std::map<std::string, UTILSColor>::iterator itr = m_aMapColors.find( strSearch );
        if ( itr != m_aMapColors.end() ) {
            itr->second = UTILSColor( g_drawState.m_opUI->m_dParamOne->value(), 
                                      g_drawState.m_opUI->m_dParamTwo->value(),
                                      g_drawState.m_opUI->m_dParamThree->value() );
        }
    } else if ( iWhich - m_aMapColors.size() < m_aMapDoubles.size() ) {
        const std::map<std::string, double>::iterator itr = m_aMapDoubles.find( strSearch );
        if ( itr != m_aMapDoubles.end() ) {
            itr->second = g_drawState.m_opUI->m_dParamOne->value();
        }
    } else if ( iWhich - m_aMapColors.size() - m_aMapDoubles.size() < m_aMapIntegers.size() ) {
        const std::map<std::string, int>::iterator itr = m_aMapIntegers.find( strSearch );
        if ( itr != m_aMapIntegers.end() ) {
            itr->second = (int) (g_drawState.m_opUI->m_dParamOne->value());
        }
    }
    
    ParamsChanged();
    
    WriteParamFile();
}

void ParamFile::ChangedParamChoice()
{
    const Fl_Choice *opChoice = g_drawState.m_opUI->m_choiceParam;
    
    const char *strSearch = opChoice->text();
    const int iWhich = opChoice->value();
    
    Fl_Slider *opSld1 = g_drawState.m_opUI->m_dParamOne;
    Fl_Slider *opSld2 = g_drawState.m_opUI->m_dParamTwo;
    Fl_Slider *opSld3 = g_drawState.m_opUI->m_dParamThree;
    Fl_Float_Input *opMin = g_drawState.m_opUI->m_dMinForValueSlider;
    Fl_Float_Input *opMax = g_drawState.m_opUI->m_dMaxForValueSlider;

    static char strMax[256];
    if ( iWhich < m_aMapColors.size() ) {
        const std::map<std::string, UTILSColor>::iterator itr = m_aMapColors.find( strSearch );
        if ( itr != m_aMapColors.end() ) {
            opSld1->bounds(0.0, 1.0);
            opSld1->step(0.05);
            opMin->value( "0.0" );
            opMax->value( "1.0" );
            opSld1->value( itr->second[0] );
            opSld2->value( itr->second[1] );
            opSld3->value( itr->second[2] );
        }
    } else if ( iWhich - m_aMapColors.size() < m_aMapDoubles.size() ) {
        const std::map<std::string, double>::iterator itr = m_aMapDoubles.find( strSearch );
        if ( itr != m_aMapDoubles.end() ) {
            opSld1->bounds( 0.0, WINmax( 0.1, itr->second * 10.0 ) );
            opSld1->step( WINmax( 0.001, itr->second * 0.05 ) );
            opSld1->value( itr->second );
            opMin->value( "0.0" );
            sprintf( strMax, "%f", WINmax( 0.1, itr->second * 10.0 ) );
            opMax->value( strMax );
        }
    } else if ( iWhich - m_aMapColors.size() - m_aMapDoubles.size() < m_aMapIntegers.size() ) {
        const std::map<std::string, int>::iterator itr = m_aMapIntegers.find( strSearch );
        if ( itr != m_aMapIntegers.end() ) {
            opSld1->bounds( 0.0, WINmax( 0.1, itr->second * 10.0 ) );
            opSld1->step( WINmax( 0.001, itr->second * 0.05 ) );
            opSld1->value( itr->second );
            opMin->value( "0.0" );
            sprintf( strMax, "%d", WINmax( 10, itr->second * 10 ) );
            opMax->value( strMax );
        }
    }
}
        
// Default location for the param file is in the ShaderData directory
std::string ParamFile::ParamFileName() const
{
    const string strName = g_drawState.GetBaseDirectory( "Shaders/" ) + "ParamData.txt";

    return strName;
}

void ParamFile::ParamsChanged()
{
	CurveDrawData::ParamFileUpdated();
    PointConstraint::ParamFileUpdated();
	g_drawState.UpdateParams();
}

// Read and stuff into the hash tables
void ParamFile::ReadParamFile()
{
	char strRead[1024];

	// Use ParamFileName() function so MeshViewer::Dump has the correct file name
    const string strParam = ParamFileName();
    ifstream in( strParam.c_str(), ios::in );
    if ( !in.good() ) {
        cerr << "ERR: Unable to open param file, using internal defaults\n";
    } else {
        cerr << "Reading param file " << strParam << "\n";
        m_aMapColors.clear();
        m_aMapDoubles.clear();
        m_aMapIntegers.clear();
        m_aKeepText.clear();
        
        std::string strName, strType;
        UTILSColor col;
        double d = 0.0;
		int iI = 0;
        
        while ( !in.eof() ) {
            in >> strName;
            if ( !in.good() ) break;
            
            if ( strName[0] == '#' ) {
                in.getline( strRead, 1024 );
                m_aKeepText.push_back( std::pair< std::string, bool >( strName + string(strRead), false ) );
            } else {
                in >> strType;
                if (!strType.compare("Color")) {
                    col.Read(in);
                    m_aMapColors.insert( std::pair<std::string, UTILSColor>(strName, col) );
                    
                } else if (!strType.compare("Double")) {
                    in >> d;
                    m_aMapDoubles.insert( std::pair<std::string, double>(strName, d) );
                } else if (!strType.compare("Integer")) {
                    in >> iI;
                    m_aMapIntegers.insert( std::pair<std::string, int>(strName, iI) );
                } else {
                    cerr << "ERR: Read params unknown type " << strName << " " << strType << "\n";
                    break;
                }
                m_aKeepText.push_back( std::pair< std::string, bool >( strName + " " + strType, true ) );
            }
        }
    }
	
	in.close();
	
    ParamsChanged();
}

void ParamFile::WriteParamFile()
{
    string strParam = ParamFileName();
    const size_t iEnd = strParam.find_last_of( ".txt" );
    if ( iEnd != string::npos ) {
        strParam.erase( iEnd - 3, string::npos );
    }
    strParam = strParam + string( ".txt" );
    
    string strName, strType;
    
    ofstream out( strParam.c_str(), ios::out );
    if ( !out.good() ) {
        cerr << "ERR: Unable to open param file for writing\n";
    } else {
        for ( int i = 0; i < m_aKeepText.size(); i++ ) {
            if ( m_aKeepText[i].second == false ) {
                out << m_aKeepText[i].first << "\n";
            } else {
                const string strKey = m_aKeepText[i].first;
                const size_t strSpace = strKey.find( " " );
                strName = strKey.substr(0, strSpace);
                strType = strKey.substr(strSpace+1, string::npos);
                
                out << strName << " " << strType << " ";
                if ( !strType.compare("Color") ) {
                    out << GetColor(strName) << "\n";
                } else if ( !strType.compare("Double") ) {
                    out << GetDouble(strName) << "\n";
                } else if ( !strType.compare("Integer") ) {
                    out << GetInteger(strName) << "\n";
                }
            }
        }
    }        
    out.close();
}

ParamFile::ParamFile()
{
}

ParamFile g_paramFile;
