/*
 *  CurveOverlap_Blend.cpp
 *  SketchCurves
 *
 *  Created by Cindy Grimm on 5/10/11.
 *  Copyright 2011 __MyCompanyName__. All rights reserved.
 *
 */


#include "CurveOverlap.h"
#include "Curve.h"
#include "ScreenCurve.h"
#include "ParamFile.h"

/* Split the curve into pieces at the corners */
static std::vector<ScreenCurve> Split( const boost::dynamic_bitset<> &in_abCorners, const ScreenCurve &in_crv )
{
	int iPrev = 0;
	int iNext = in_abCorners.find_first();
	if ( iNext == -1 ) iNext = in_crv.NumPts();
	
	std::vector<ScreenCurve> acrvOut;
	ScreenCurve crv;
	while ( iPrev != -1 ) {
		crv.Clear();
		for ( int i = iPrev; i < iNext; i++ ) {
			crv.AddPoint( in_crv.Pt(i) );
		}
		if ( !crv.IsTap() ) {
			acrvOut.push_back(crv);
		}
		iPrev = in_abCorners.find_next( iPrev );
		iNext = in_abCorners.find_next( iNext );
		if ( iNext == -1 ) iNext = in_crv.NumPts();
	}		
	
	return acrvOut;
}

/* Split the curve into pieces at the corners 
 * First, find all possible corners
 * Second, see if the segments actually overlay each other (may not
 * in the case of a legitimate corner
 * Third, split them up along the fold backs
 *
 * This method is static because none of the data is being stored. But it's put here in CurveOverlap
 * because this class handles 2D stroke processing
 */
std::vector<ScreenCurve> CurveOverlap::SplitAtFoldbacks( const ScreenCurve &in_crv ) 
{
	const ScreenCurve crv2D = in_crv.FillGaps( g_drawState.ScreenSelectionSize() * 0.4 );
	
	boost::dynamic_bitset<> abCorners = crv2D.Corners( g_drawState.ScreenSelectionSize() * 0.75 );
	
	std::vector<ScreenCurve> acrvSplit = Split( abCorners, crv2D );
	
	/* For each segment see if it overlaps the end of the previous one. If it does, then
	 * we keep this break. If it doesn't, merge this one with the next one */
	int iSplitToCheck = 1;
	std::vector<double> adTs, adDists, adTsTemp, adDistsTemp;
	const double dDist = g_paramFile.GetDouble( "BlendPerc" ) * g_drawState.ScreenSelectionSize();
	while ( iSplitToCheck < acrvSplit.size() ) {
		ScreenCurve crvSplit = acrvSplit[iSplitToCheck];
		ScreenCurve crvLeft = acrvSplit[iSplitToCheck-1];
		crvSplit.Smooth(1); crvSplit.SmoothEnds(1); 
		crvLeft.Smooth(1); 
        crvLeft.SmoothEnds(3);
		
		adTs.resize( crvSplit.NumPts() );
		adDists.resize( crvSplit.NumPts() );
		for ( int i = 0; i < crvSplit.NumPts(); i++ ) {
			adTs[i] = crvLeft.ClosestPointTValue( crvSplit.Pt(i), adDists[i] );
		}
		
		// Do a little filtering for noise
		for ( int iLoop = 0; iLoop < 2; iLoop++ ) {
			adTsTemp = adTs;
			adDistsTemp = adDists;
			for ( int i = 1; i < adTs.size() - 1; i++ ) {
				if ( RNIsZero( adTs[i+1], 1e-6 ) ) {
					break;
				}
				adTs[i] = 0.5 * adTsTemp[i] + 0.25 * ( adTsTemp[i-1] + adTsTemp[i+1] );
				adDists[i] = 0.5 * adDistsTemp[i] + 0.25 * ( adDistsTemp[i-1] + adDistsTemp[i+1] );
			}
		}
		
		if ( s_bTraceBlend == true ) {
			cout << "Ts: ";
			for ( int i = 0; i < adTs.size(); i++ ) {
				cout << adTs[i] << " " ;
			}
			cout << "\nDists: " << dDist << "  ";
			for ( int i = 0; i < adDists.size(); i++ ) {
				cout << adDists[i] << " ";
			}
			cout << "\n";
		}
		
		bool bOverlap = true;
		for ( int i = 0; i < adTs.size() - 1; i++ ) {
			if ( adTs[i+1] > adTs[i] ) {
				if ( s_bTraceBlend ) cout << "Ts\n";
				bOverlap = false;
				break;
			}
			if ( RNIsZero( adTs[i+1], 1e-6 ) ) {
				break;
			}
			if ( adDists[i] > dDist ) {
				if ( s_bTraceBlend ) cout << "Dists\n";
				bOverlap = false;
				break;
			}
		}
		if ( bOverlap == false ) {
			for ( int i = 0; i < crvSplit.NumPts(); i++ ) {
				acrvSplit[iSplitToCheck-1].AddPoint( crvSplit.Pt(i) );
			}
			acrvSplit.erase( acrvSplit.begin() + iSplitToCheck );
		} else {
			iSplitToCheck++;
		}
	}
	return acrvSplit;
}
