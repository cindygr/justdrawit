/*
 *  CurveNetwork.h
 *  SketchCurves
 *
 *  Created by Cindy Grimm on 9/16/10.
 *  Copyright 2010 Washington University in St. Louis. All rights reserved.
 *
 */

#include "CurveGroup.h"
#include "CurveOverlap.h"
#include <time.h>

#ifndef _CURVE_NETWORK_DEFS_H
#define _CURVE_NETWORK_DEFS_H

/*
 * 9/11 - Moved to curve group pointers to avoid allocating and copying
 *      - Pulled out point/normal constraints as individual entities
 *      - Store them here
 *      - Moved reflection to curve group
 *      - Moved state parameters (hidden, not hidden) to CurveGroup
 * 12/11
 *      - Undo: Added time stamp for actions. At the start of any action that
 *        changes the pins or the curves, increment the time stamp. Every action is
 *        tagged with what the action was, any data needed to re-create it (eg, active pin hash ids)
 *        and the time stamp. Undo is just another action; so action stacks will have undo listed
 *        in them in order to do re-do
 *      - Undo: Added curve and point graveyard so they can be resurrected if needed
 */
class CurveNetwork {
public:
    /// For debugging
    static const bool s_bTraceUndo, s_bTraceErase, s_bTraceCombine, s_bTracePropagate,
                      s_bTraceAddingConstraints, s_bTraceProcessStroke, s_bTraceDrag;
    
private:
    /* Time stamp. Incremented on every action */
    static int m_iCurrentTime;
    
	/* List of all currently active curves.
     * Also keep around all of the ones that get "deleted" in case
     * they need to be resurrected on an undo */
    Array< CurveGroup * > m_aopCrv, m_aopCrvGraveyard;
    
    /* List of all point and normal constraints.
     * Also keep around all of the ones that get "deleted" in case
     * they need to be resurrected on an undo */
    Array<PointConstraint *> m_aopPins, m_aopPinGraveyard;
    
	/*
	 * Things to keep track of:
	 *   m_iCurveLastStroke:
	       Which curve was last edited with a join, overstroke start, merge, or new (no close)
	 *        This curve should be checked first to see if the latest stroke continues the last one
	 *        Cleared by a drag, erase, or smooth of this curve
	 *   m_iLastEdited, m_iSaveLastEdited:
	 *     Which curve was last touched or changed by any action
	 *     If the last action was a stroke then m_iSaveLastEdited is set to the last edited curve
	 *     Otherwise is set to -1
	 */
	int m_iCurveLastStroke;     // Which curve had the last stroke (if any)
	int m_iLastEdited;
    int m_iSaveLastEdited;      // Save this in case they change their mind
	
	/* This is a little ugly, but... Basically, temporary storage
	 * for checking all possible overlap types. There are two extra
	 * special ones - just the next stroke and a closed stroke */
	static boost::dynamic_bitset<> s_abValid;
	static Array<CurveOverlap> s_acrvOverlap;

	// Indices of special cases above (CurveNetwork_Stroke.cpp)
	int IndexOverlapLastStroke() const { return m_aopCrv.num(); }
	int IndexOverlapClosedStroke() const { return m_aopCrv.num() + 1; }
	
	/* Stroke stuff. (CurveNetwork_Stroke.cpp) */
	// Determine if this is a scratch-back and forth stroke. If so, return the smoothed stroke
	StrokeHistory::MergeType IsBlend( const ScreenCurve &in_crv, ScreenCurve &out_crvScreen ) const;
	// Try all possible curves 
	void DoAllCompares( const ScreenCurve &in_crv ) const;
	// Search for possible combine
	int BestMerge( const CurveOverlap::CurveEndType in_endOfStroke ) const;
	// Search for best action
	int BestAction(  ) const;
	
	// Explicitly create a new curve. These are called internally
    void NewCurve( const ScreenCurve &in_crv2D );
    void NewClosedCurve( const CurveOverlap &in_overlap, const ScreenCurve &in_crv2D );
	void CombineCurves( const ScreenCurve &in_crv2D, const int in_iE1, const int in_iE2 );
	
    /// Look for the constraint with this id and resurrect if need be
    PointConstraint *FindOrResurrectPin( const int in_iPinHashId );
    /// Look for the curve with this id and resurrect if need be
    CurveGroup *FindOrResurrectCurve( const int in_iCurveHashId );
    /// Remove any constraints that no longer have enough curves
    void RemoveEmptyConstraints();
    /// Undo: Restore any constraints that might have been attached to this curve
    void RestoreConstraintsInEraseRegion( const Array<int> &in_aiPinIds, const int in_iTimeStamp );
    /// Undo: Move constraints from current curve to the other one
    void RestoreConstraintsCombine( CurveGroup &in_crvGroupOrig, CurveGroup &in_crvRestored, const Array<int> &in_aiPinIds, const int in_iTimeStamp );
	/// Undo/Rollback: Undo a combine
	void RestoreCombine( const StrokeHistory &in_sh, CurveGroup &io_crvGroupOrig );
	/// Undo/rollback: Undo a stroke (put constraints back)
	void RestoreStroke(  const StrokeHistory &in_sh, CurveGroup &io_crvGroupOrig );
	/// Undo constraint cleanup
	void UndoCleanup( const StrokeHistory &in_sh );
    /// Undo: Put constraint back
    void AddConstraintToCurve( CurveGroup &in_crvGroup, const int in_iHashId, const int in_iTimeStamp );
    /// Undo: Take constraint away
    void RemoveConstraintFromCurve( CurveGroup &in_crvGroup, const int in_iHashId );
    /// Undo: undo last pin action
    void RollbackConstraintAction( const int in_iHashId, const int in_iTimeStamp );
    /// Undo: Update any curves that might have changed due to pin constraint changes
    void ReEstablishConstraints( const Array<int> &in_aiPinIds );
    
    ///
    CurveGroup &SetCurveGroup( const int in_i ) { return *m_aopCrv[ in_i ]; }

    // Don't allow!!!
    CurveNetwork &operator=( const CurveNetwork &in_crvNW );
    CurveNetwork( const CurveNetwork &in_crvNW ) { *this = in_crvNW; }

public:
    /**@name Accessors */
    //@{
    ///
    int NumCrvs() const { return m_aopCrv.num(); }
    ///
    const Curve &GetCurve( const int in_i ) const { return m_aopCrv[ in_i ]->CompositeCurve(); }
    ///
    const CurveRibbon &GetCurveRibbon( const int in_i ) const { return m_aopCrv[ in_i ]->GetCurveRibbon(); }
    ///
    const CurveGroup &GetCurveGroup( const int in_i ) const { return *m_aopCrv[ in_i ]; }
    ///
    bool IsReflected( const int in_i ) const { return m_aopCrv[ in_i ]->IsReflected(); }
	/// Center of the bounding box
	const R3Pt Center() const;
	///
	const std::pair<R3Pt,R3Pt> BoundingBox() const;
	///
	bool IsHidden( const int in_i ) const ;
    /// May be empty
    const CurveGroup &GetLastEditedCurveGroup( ) const;
	///
	bool IsLastEditedCurve( const int in_i ) const ;
	/// 
	bool IsLastActionSmooth( ) const;
	/// Only valid if last action was a stroke (not a drag, erase, etc). In CurveNetwork_Stroke.cpp
	bool IsLastActionStroke( ) const;
	/// Only valid if last action was a stroke (not a drag, erase, etc). In CurveNetwork_Stroke.cpp
	bool IsClickOnLastStroke( const R2Pt &in_ptScreen ) const;
	/// Only valid if last action was a drag or smooth. In CurveNetwork_Stroke.cpp
	bool IsClickOnLastSelectRegion( const R2Pt &in_ptScreen ) const;
	///
	std::string LastStrokeAction() const;
    ///
    int NumConstraints() const { return m_aopPins.num(); }
    ///
    const PointConstraint &GetConstraint( const int in_i ) const { return *m_aopPins[in_i]; }
    ///
    int FindIndexOfCurveGroup( const CurveGroup &in_grp ) const;
    //@}

	/**@name Selection stuff, in CurveNetwork.cpp */
	//@{
    /// Find closest curve, taking z values into account
    double ClosestCurve( const R3Ray &in_ray, const bool in_bSkipHidden, int &out_iWhich, double &out_dT ) const;
	//@}

	/**@name Point and normal constraints. In CurveNetwork_Align.cpp */
	//@{
	///
	void FixAllIntersectionNormals();
	/// Create a constraint and add it to the list and to the curve
	PointConstraint *NewNormalConstraint( const int in_iCrv1, const double in_dCrv1 );
	/// Create a constraint and add it to the list and to the curve
	PointConstraint *NewPinConstraint( const int in_iCrv1, const double in_dCrv1 );
	/// Snap two curves together and create a point constraint
	PointConstraint *NewSnapCurves( const int in_iCrv1, const double in_dCrv1, const int in_iCrv2, const double in_dCrv2 );
	/// Remove curves from constraint then delete the constraint
	void RemoveConstraint( PointConstraint *io_pin );
    ///
    PointConstraint *GetClosestPointConstraint( const R3Ray &in_ray, double &out_dDist, double &out_dZ );
    ///
    PointConstraint *GetClosestNormalConstraint( const R3Ray &in_ray, double &out_dDist, double &out_dZ );
    /// See if there's a normal constraint close to this t value
    PointConstraint *HasConstraint( const int in_iCurve, const double in_dT );
    /// See if there's a normal constraint close to this t value
    const PointConstraint *HasConstraintNoEdit( const int in_iCurve, const double in_dT ) const;
    /// Propagate edits
    void PropagateEdits();
	//@}

	/**@name Editing the curve (not strokes). Mostly just pass the action through to the appropriate CurveGroup In CurveNetwork_Edit.cpp */
	//@{
    ///
    void NewCompositeCurve( const ScreenCurve &in_crv2D, const Curve &in_crv3D );
    ///
    void NewCompositeCurve( const Curve &in_crv3D );
	/// Erase between the t values, possibly creating two curves
	void Erase( const int in_i, const double in_dTStart, const double in_dTEnd );
	/// Just delete.
	void DeleteCurve( const int in_i );
	/// Call when starting a drag or transform
	void StartTransform( const int in_i );
	/// Drag a region. in_vec should be in the plane or perpendicular to it.
    void Drag( const int in_i, const R3Plane &in_plane, const R3Vec &in_vec, const double in_dTDrag, const std::pair<double,double> &in_dTBracket );
	/// A general matrix transform
    void Transform( const int in_i, const R4Matrix &in_xform, const bool in_bReset ); // Shift curve
	/// Drag ends together
	void CloseCurve( const int in_iCrv );
	/// Smooth a region
	void Smooth( const int in_i, const double in_dTStart, const double in_dTEnd );
	/// Repeat
	void RepeatLastSmooth( );
	///
	void Clear();
	//@}
	
    /**@name Undo routines */
    //@{
    ///
    static int CurrentTime() { return m_iCurrentTime; }
    /// 
    static void IncrementTime() { m_iCurrentTime++; }
	/// For just that curve
	void UndoLastAction( const int in_i );
    ///
	void UndoLastAction(  );
	// in_iCurve == -1 means apply to current curve we're editing (if any). Returns edited/new curve, if any
	int ReProcessStroke( const StrokeHistory::MergeType &in_type, const int in_iCurve );
	//@}
    
	/**@name Add a stroke to the curve */
	//@{
	// Initial process
	void ProcessCurve( const ScreenCurve &in_crv2D );
	//@}
	
	/// Remove all stroke histories
	void FinalizeAll();

	/**@name Change state (in CurveNetwork.cpp) */
	//@{
	///
    void ReflectCurve( const int in_i, const R3Plane &in_plane );
	///
    void UnreflectCurve( const int in_i );
	///
	void ToggleHideCurve( const int in_i );
	/// 
    void SetCurveNormals();
	/// For highlighting part of the curve
	void SetSelectionRegion( const int in_i, const std::pair<double,double> &in_adTs );
	/// Unhilight selection
	void ClearSelectionRegion( const int in_i );
	/// Active state
	void UpdateCurveState( const int in_iCursorOverCurve );
    ///
    void ParamDataChanged();
	//@}
    
    /**@name Draw routines */
    //@{
    ///
    void DrawPointConstraints( const PointConstraint *in_opSel ) const;
    ///
    void DrawNormalConstraints( const PointConstraint *in_opSel ) const;
    ///
    void UpdateDrawData();
    //@}
	
    CurveNetwork();
    ~CurveNetwork();

    void CenterAndScale();
    
    /// Check pins/curves to make sure consistent
    void CheckConsistency() const;
    
    void Write( ofstream &out ) const;
    void WriteSimple( ofstream &out ) const;
    void WriteSimple( const string &in_str ) const;
    void WriteMaya( const string &in_str ) const;
    void WriteAdobe( const string &in_str ) const;
    void WriteCurveTubeGeometry( const string &in_str );
    void Read( ifstream &in );
    void Read( const string &in_str );
    void ReadKaran( ifstream &in );
	void ReadMaya( ifstream &in );
    void ReadSimple( ifstream &in );
    void ReadPatchMaker( ifstream &in, Array< Array<int> > &out_aaiPatch, Array< Array<bool> > &out_aabReversePatch );
    void ReadTangentNormal( ifstream &in );
    void AddNormalsFromCurves( const CurveNetwork &in_crvs );
};

#endif
