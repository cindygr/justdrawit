//
//  PatchSurface.cpp
//  SketchCurves
//
//  Created by Cindy Grimm on 12/10/12.
//  Copyright (c) 2012 Washington University in St. Louis. All rights reserved.
//

#include <iostream>
#include "PatchSurface.h"
#include "CurveNetwork.h"
#include <fitting/FitTools_FitEllipse.H>
#include <fitting/FitTools_Least_squares.H>
#include <fitting/Fit_RbfHermiteTC.H>
#include <QHull_Interface.H>
#include "ParamFile.h"
#include "DrawState.h"
#include <OpenGL/OGLDraw3D.H>
#include <FL/GL.H>
#include "UserInterface.h"

void PatchSurface::CreatePolygon( const Array<int> &in_aiCrvs, const Array< pair<double,double> > &in_adStartStop )
{
    if ( in_aiCrvs.num() != in_adStartStop.num() ) {
        cerr << "ERR: PatchSurface::CreatePolygon, number of curves and start/stop don't match " << in_aiCrvs.num() << in_adStartStop.num() << "\n";
        return;
    }
    
    Array<R3Pt> apt;
    
    m_avecNormals.need(0);
    m_aiCorners.need(0);
    R3Vec vecNormalAvg(0,0,0);
    R3Pt ptAvg(0,0,0);
    int iStart = 0, iEnd = 0, iIncr = 0;
    for ( int i = 0; i < in_aiCrvs.num(); i++ ) {
        const CurveRibbon &crvRibbon = m_crvNW.GetCurveRibbon( in_aiCrvs[i] );
        const double dStart = in_adStartStop[i].first;
        const double dEnd = in_adStartStop[i].second;
        if ( dStart < dEnd ) {
            iStart = 0;
            iEnd = crvRibbon.NumPts();
            iIncr = 1;
        } else {
            iStart = crvRibbon.NumPts() - 1;
            iEnd = -1;
            iIncr = -1;
        }
        bool bFoundStart = false;
        for ( int iP = iStart; iP != iEnd; iP += iIncr ) {
            if ( ( dStart < dEnd && crvRibbon.PercAlong(iP) >= dStart && crvRibbon.PercAlong(iP) < dEnd ) ||
                 ( dStart > dEnd && crvRibbon.PercAlong(iP) <= dStart && crvRibbon.PercAlong(iP) > dEnd ) ) {
                R3Pt ptToAdd = crvRibbon.Pt(iP);

                // Blend start and end points
                if ( bFoundStart == false ) {
                    bFoundStart = true;
                    m_aiCorners += apt.num();
                    const R3Pt ptE1 = crvRibbon( dStart );
                    const R3Pt ptE2 = m_crvNW.GetCurveRibbon( in_aiCrvs.wrap(i-1) )( in_adStartStop.wrap(i-1).second );
                    if ( !ApproxEqual(ptE1, ptE2, 1e-6) ) {
                        cerr << "WARNING: End points of patch not quite the same " << ptE1 << " " << ptE2 << "\n";
                    }
                    ptToAdd = Lerp( ptE1, ptE2, 0.5 );
                    m_aptPointConstraints.push_back( ptE1 );
                }
                apt += ptToAdd; 
                for ( int j = 0; j < 3; j++ ) {
                    ptAvg[j] += apt.last()[j];
                }
                m_avecNormals += UnitSafe( crvRibbon.GetNormalAtPoint(iP) );
                vecNormalAvg = vecNormalAvg + m_avecNormals.last();
            }
        }
    }
    if ( apt.num() < 4 ) {
        cerr << "ERR: PatchSurface::CreatePolygon, only found " << apt.num() << " pts\n";
        return;
    }
    
    vecNormalAvg = UnitSafe( vecNormalAvg );
    for ( int i = 0; i < 3; i++ ) {
        ptAvg[i] /= (double) apt.num();
    }
    
    // Now fit a plane to project the normals onto
    FITTools_LS oLS( apt.num() + 1, 4, 1 );
    for ( int i = 0; i < apt.num(); i++ ) {
        for ( int j = 0; j < 3; j++ ) {
            oLS.Desired(i, j) = apt[i][j];
        }
        oLS.Desired(i, 3) = 1;
    }
    for ( int i = 0; i < 4; i++ ) {
        oLS.Desired(apt.num(), i) = 1.0;
    }
    oLS.Rhs( apt.num(), 0) = 1.0;
    if ( ! oLS.Solve() ) {
        cerr << "ERR: PatchSurface::CreatePolygon, unable to fit plane\n";
        
        m_plane = R3Plane( apt[0], vecNormalAvg );
    } else {
        m_plane = R3Plane( oLS.Solution(0,0), oLS.Solution(1,0), oLS.Solution(2,0), oLS.Solution(3,0) ); 
        if ( Dot( m_plane.Normal(), vecNormalAvg ) < 0.0 ) {
            m_plane = R3Plane( -oLS.Solution(0,0), -oLS.Solution(1,0), -oLS.Solution(2,0), -oLS.Solution(3,0) );
        }
    }
    R3Vec vecS, vecT;
    m_plane.TangentPlane( vecS, vecT );
    const R3Matrix matRot = R3Matrix(vecS, vecT, UnitSafe( m_plane.Normal() ) ).Transpose();
    const R3Pt ptCenter = m_plane.ProjectOnPlane(ptAvg);
    m_matToPlane = (R4Matrix) matRot * 
                      R4Matrix::Translation( R3Pt(0,0,0) - ptCenter );
    m_matToPatch = R4Matrix::Translation( ptCenter - R3Pt(0,0,0) ) * (R4Matrix) matRot.Transpose();
    
    int iReverse = 0;
    for ( int iC = 0; iC < m_aiCorners.num(); iC++ ) {
        const R3Vec vecOrientation( Cross( apt[ m_aiCorners.wrap(iC-1) ] - apt[ m_aiCorners[iC] ],
                                           apt[ m_aiCorners.wrap(iC+1) ] - apt[ m_aiCorners[iC] ] ) );
        if ( Dot(vecOrientation, m_plane.Normal()) < 0.0 ) {
            iReverse++;
        }
    }
    cout << iReverse << " " << m_aiCorners.num() << "\n";
    if ( iReverse >= m_aiCorners.num() / 2 ) {
        apt.reverse();
        m_avecNormals.reverse();
        const R3Pt ptStart = apt.last();
        const R3Vec vecStart = m_avecNormals.last();
        apt.addItem( ptStart, 0 );
        m_avecNormals.addItem( vecStart, 0 );
        apt.sub(1);
        m_avecNormals.sub(1);
        const Array<int> aiSaveCorners = m_aiCorners;
        for ( int iC = 0; iC < m_aiCorners.num(); iC++ ) {
            m_aiCorners[iC] = ( apt.num() - aiSaveCorners.wrap( m_aiCorners.num() - iC ) ) % apt.num();
        }
    }

    // Create the 3D polygon
    m_polySurface = R3Polygon( apt );
    
    Array<R2Pt> apt2D( apt.num() );
    for ( int i = 0; i < apt.num(); i++ ) {
        const R3Pt pt = m_matToPlane * apt[i];
        apt2D[i] = R2Pt( pt[0], pt[1] );
    }
    m_polyDomain = R2Polygon( apt2D );
    m_apt2DExtra.need(0);
                        
    m_aptPointConstraints.push_back( ptAvg );
    m_aptPointConstraints.push_back( ptCenter );
    ASSERT( apt.num() == m_avecNormals.num() );
    ASSERT( m_polyDomain.Num_pts() == apt.num() );
    ASSERT( m_polySurface.Num_pts() == apt.num() );
    
    cout << "Done patch\n";
    cout << ptAvg << ptCenter << "\n";
    cout << oLS.Solution(0,0) * ptAvg[0] + oLS.Solution(1,0) * ptAvg[1] + oLS.Solution(2,0) * ptAvg[2] + oLS.Solution(3,0) << " " << m_plane.Evaluate( ptAvg ) << "\n";
    cout << oLS.Solution(0,0) * ptCenter[0] + oLS.Solution(1,0) * ptCenter[1] + oLS.Solution(2,0) * ptCenter[2] + oLS.Solution(3,0) << " " << m_plane.Evaluate(ptCenter ) << "\n";
}

bool PatchSurface::EdgesIntersect( const Array<R2Pt> &in_apt ) const
{
    R2Pt ptOut;
    double dS, dT;
    for ( int i = 0; i < in_apt.num(); i++ ) {
        const R2Line_seg seg( in_apt.wrap(i+1), in_apt[i] );
        
        for ( int j = i+2; j < in_apt.num(); j++ ) {
            const R2Line_seg segNext( in_apt.wrap(j+1), in_apt[j] );
            
            if ( seg.Intersect( segNext, ptOut, dS, dT ) ) {
                if ( ! (i == 0 && j == in_apt.num()-1) ) {
                    return true;
                }
            }
        }
    }
    return false;
}

void PatchSurface::UnwindPolygon( Array<R2Pt> &io_apt, 
                                  const boost::dynamic_bitset<> &in_abFixed, 
                                  const R2Pt_i &in_iptPullApart, 
                                  const int in_iNIters )
{
    int iCount = 500;
    
    Array<R2Pt> apt2D = io_apt;
    
    int iIters = in_iNIters;
    const int iFirst = in_abFixed.find_first();
    const R2Vec vecBtwnFixed = io_apt[ iFirst ] - io_apt[ in_abFixed.find_next( iFirst ) ];
    
    bool bIntersected = EdgesIntersect(apt2D);
    while ( ( bIntersected && iCount > 0 ) || iIters >= 0 ) {
        for ( int i = 0; i < apt2D.num(); i++ ) {
            if ( i == in_iptPullApart[0] ) {
                io_apt[i] = io_apt[i] + R2Vec( vecBtwnFixed[1], -vecBtwnFixed[0] ) * 0.01;
            } else if ( i == in_iptPullApart[1] ) {
                io_apt[i] = io_apt[i] + R2Vec( -vecBtwnFixed[1], vecBtwnFixed[0] ) * 0.01;                
            } else if ( !in_abFixed[i] ) {
                const R2Pt ptMid = Lerp( apt2D.wrap(i-1), apt2D.wrap(i+1), 0.5 );
                io_apt[i] = Lerp( apt2D[i], ptMid, 0.2 );
            }
        }
        apt2D = io_apt;
        bIntersected = EdgesIntersect(apt2D);
        if ( bIntersected == false ) {
            iIters--;
        } else {
            iCount--;
        }
    }
    cout << "Unwind interations " << 500 - iCount << "\n";
}


void PatchSurface::UnwindPolygon()
{
    boost::dynamic_bitset<> abCorners( m_polyDomain.Num_pts() ), abCornersFixed( m_aiCorners.num() );
    Array<R2Pt> aptCorners( m_aiCorners.num() );
    abCorners.reset();
    abCornersFixed.reset();
    double dLenBdry3D = 0.0;
    for ( int i = 0; i < m_aiCorners.num(); i++ ) {
        abCorners.set( m_aiCorners[i] );
        aptCorners[i] = m_polyDomain[ m_aiCorners[i] ];
        dLenBdry3D += ::Length( m_polySurface[ m_aiCorners[i] ] - m_polySurface[ m_aiCorners.wrap(i-1) ] );
    }
    R2Pt_i iptFurthest(0,0);
    double dFurthest = 0.0;
    for ( int i = 0; i < m_aiCorners.num(); i++ ) {
        int iGeo = ( i + m_aiCorners.num() / 2 );
        double dGeo = 1e30;
        for ( int j = i+1; j < i+m_aiCorners.num() - 1; j++ ) {
            double dLenBdry = 0.0;
            for ( int k = i; k < j; k++ ) {
                dLenBdry += ::Length( m_polySurface[ m_aiCorners.wrap(k) ] - m_polySurface[ m_aiCorners.wrap(k+1) ] );
            }
            if ( fabs( dLenBdry3D * 0.5 - dLenBdry ) < dGeo ) {
                iGeo = j % m_aiCorners.num();
                dGeo = abs( dLenBdry3D * 0.5 - dLenBdry );
            }
        }
        for ( int j = iGeo - 1; j <= iGeo+1; j++ ) {
            if ( ::Length( aptCorners[i] - aptCorners.wrap(j) ) > dFurthest ) {
                iptFurthest = R2Pt_i( WINmin(i, j%aptCorners.num()),WINmax(i, j%aptCorners.num()));
                dFurthest = ::Length( aptCorners[i] - aptCorners[j] );
            }
        }
    }
    abCornersFixed.set( iptFurthest[0] );
    abCornersFixed.set( iptFurthest[1] );
    
    R2Pt_i iptPullApart( (int) ( 0.5 * (iptFurthest[0] + iptFurthest[1]) ), ( (int) ( 0.5 * (iptFurthest[0] + iptFurthest[1] + aptCorners.num() ) ) ) % aptCorners.num() );
    const R2Vec vecFurthest = UnitSafe( aptCorners[ iptFurthest[0] ] - aptCorners[iptFurthest[1]] );
    const R2Pt ptMidFurthest = Lerp( aptCorners[ iptFurthest[0] ], aptCorners[iptFurthest[1]], 0.5 );
    double dBest = fabs( Dot( UnitSafe( aptCorners[iptPullApart[0]] - ptMidFurthest ), vecFurthest ) );
    for ( int i = iptFurthest[0] + 1; i < iptFurthest[1]; i++ ) {
        const double dDot = fabs( Dot( UnitSafe( aptCorners[i] - ptMidFurthest ), vecFurthest ) );
        if ( dDot < dBest ) {
            iptPullApart[0] = i;
            dBest = dDot;
        }
    }
    dBest = fabs( Dot( UnitSafe( aptCorners[iptPullApart[1]] - ptMidFurthest ), vecFurthest ) );
    for ( int i = iptFurthest[1] + 1; i < iptFurthest[0] + aptCorners.num(); i++ ) {
        const double dDot = fabs( Dot( UnitSafe( aptCorners.wrap(i) - ptMidFurthest ), vecFurthest ) );
        if ( dDot < dBest ) {
            iptPullApart[1] = i;
            dBest = dDot;
        }
    }
    m_arayTangentConstraints.push_back( R3Ray(m_polySurface[ m_aiCorners[ iptFurthest[0] ] ], R3Vec(0,1,0) ) );
    m_arayTangentConstraints.push_back( R3Ray(m_polySurface[ m_aiCorners[ iptFurthest[1] ] ], R3Vec(0,1,0) ) );
    m_arayTangentConstraints.push_back( R3Ray(m_polySurface[ m_aiCorners[ iptPullApart[0] ] ], R3Vec(1,1,0) ) );
    m_arayTangentConstraints.push_back( R3Ray(m_polySurface[ m_aiCorners[ iptPullApart[1] ] ], R3Vec(1,1,0) ) );
    UnwindPolygon( aptCorners, abCornersFixed, iptPullApart, 10 );
    
    R3Matrix matEdge;
    const Array<R2Pt> aptSave = m_polyDomain.Pts();
    for ( int iSeg = 0; iSeg < m_aiCorners.num(); iSeg++ ) {
        const R2Vec vec = aptCorners.wrap(iSeg+1) - aptCorners[iSeg];
        const R2Vec vecCur = aptSave[ m_aiCorners.wrap(iSeg+1) ] - aptSave[ m_aiCorners[iSeg] ];
        const double dAngBack = atan2( vec[1], vec[0] );
        const double dAngTo = atan2( vecCur[1], vecCur[0] );
        const double dScl = ::Length( vec ) / ::Length( vecCur );
        const R2Matrix matRot = R2Matrix::Rotation( dAngTo ) * R2Matrix::Rotation( -dAngBack );
        cout << UnitSafe( matRot * vecCur ) << " " << UnitSafe( vec ) << "\n";
        matEdge = R3Matrix::Translation( aptCorners[iSeg] - R2Pt(0,0) ) * R3Matrix::Scaling( dScl, dScl, 1.0 ) * (R3Matrix) matRot * R3Matrix::Translation( R2Pt(0,0) - m_polyDomain[ m_aiCorners[iSeg] ] );
        const int iEnd = iSeg == m_aiCorners.num()-1 ? m_polyDomain.Num_pts() : m_aiCorners[iSeg+1];
        
        cout << aptCorners[iSeg] << matEdge * aptSave[ m_aiCorners[iSeg] ] << " " << iEnd << "\n";
        cout << aptCorners.wrap(iSeg+1) << matEdge * aptSave[ m_aiCorners.wrap(iSeg+1) ] << " " << iEnd << "\n";
        for ( int i = m_aiCorners[iSeg]; i < iEnd; i++ ) {
            m_polyDomain[i] = matEdge * m_polyDomain[i];
        }
    }
    
    UnwindPolygon( m_polyDomain.SetPts(), abCorners, R2Pt_i(-1,-1), 50 );
    
/*    
    const R2Ellipse ell = FITTools_FitEllipse::FitImplicitDirect( m_polyDomain.Pts(), false );
    ell.Points( aptCorners );
    const R2Vec v1 = aptCorners.wrap(1) - aptCorners.wrap(0);
    const R2Vec v2 = aptCorners.wrap(-1) - aptCorners.wrap(0);
    const double dDot = v1[1] * v2[0] + v1[0] * v2[1];
    if ( dDot > 0.0 ) {
        aptCorners.reverse();
    }

    int iBestOffset = 0;
    double dBestOffset = 1e30;
    for ( int i = 0; i < aptCorners.num(); i++ ) {
        double dSum = 0.0;
        for ( int j = 0; j < aptCorners.num(); j++ ) {
            dSum += Length( aptCorners.wrap(i+j) - m_polyDomain[ m_aiCorners[j] ] );
        }
        if ( dSum < dBestOffset ) {
            dBestOffset = dSum;
            iBestOffset = i;
        }
    }

    int iIters = 2;
    while ( (EdgesIntersect() && iCount-- > 0) || iIters-- > 0 ) {
        for ( int i = 0; i < m_aiCorners.num(); i++ ) {
            const R2Pt ptMid = Lerp( apt2D[ m_aiCorners.wrap(i-1) ], apt2D[ m_aiCorners.wrap(i+1) ], 0.5 );
            m_polyDomain[m_aiCorners[i]] = Lerp( apt2D[ m_aiCorners[i] ], ptMid, 0.05 );
            m_polyDomain[m_aiCorners[i]] = Lerp( apt2D[ m_aiCorners[i] ], aptCorners.wrap(i+iBestOffset), 0.05 );
        }
        
        for ( int i = 0; i < apt2D.num(); i++ ) {
            if ( !abCorners[i] ) {
                const R2Pt ptMid = Lerp( apt2D.wrap(i-1), apt2D.wrap(i+1), 0.5 );
                m_polyDomain[i] = ptMid;
            }
        }
        apt2D = m_polyDomain.Pts();
    }
 */
}

/* Make a circle who's radius is the circle that circumscribes the points
 * plus the maximum spacing between the points (and a bit more). This 
 * ensures that all of the triangles made by qhull will have edges
 * that lie on the polygon boundaries.
 * Sample that circle at the maximum distance spacing
 */
void PatchSurface::CreateDomainPoints( )
{
    double dRadius = 0.0, dMaxSpacing = 0.0;;
    for ( int i = 0; i < m_polyDomain.Num_pts(); i++ ) {
        const double dLenSq = ::LengthSq( m_polyDomain[i] - R2Pt(0,0) );
        dRadius = WINmax( dRadius, dLenSq );
        
        const double dSpacingSq = ::LengthSq( m_polyDomain[i] - m_polyDomain.PtWrap(i+1) );
        dMaxSpacing = WINmax( dMaxSpacing, dSpacingSq );
    }
    const double dRadiusPolygon = sqrt( dRadius );
    dMaxSpacing = sqrt( dMaxSpacing );
    const double dRadiusCircle = dRadiusPolygon + dMaxSpacing * 1.1;
    
    /* A circle of points around the polygon */
    const int iCircSpacing = WINmax( 16, (int) ( dRadiusCircle * 2.0 * M_PI / dMaxSpacing ) );
    const double dCircSpacing = 2.0 * M_PI / (double) ( iCircSpacing ); 
    m_apt2DExtra.need( iCircSpacing );
    double dTheta = 0.0;
    for ( int i = 0; i < m_apt2DExtra.num(); i++, dTheta += dCircSpacing ) {
        m_apt2DExtra[i] = R2Pt( cos(dTheta) * dRadiusCircle, sin(dTheta) * dRadiusCircle );
    }
    
    /* Now create points in the inside of the polygon on a grid */
    const int iGridSpacing = WINmax( 8, (int) ( 2.0 * dRadiusPolygon / dMaxSpacing ) );
    const double dGridSpacing = 2.0 * dRadiusPolygon / (double) (iGridSpacing);
    m_apt2D.need(0);
    for ( double dX = -dRadiusPolygon - 0.5 * dGridSpacing; dX < dRadiusPolygon + 0.5 * dGridSpacing; dX += dGridSpacing ) {
        for ( double dY = -dRadiusPolygon - 0.5 * dGridSpacing; dY < dRadiusPolygon + 0.5 * dGridSpacing; dY += dGridSpacing ) {
            const R2Pt ptTry(dX, dY);
            if ( m_polyDomain.Inside_winding( ptTry ) ) {
                m_apt2D += ptTry;
            }
        }
    }
    
    // Data for qhull - convert 2D points to 1D array
    Array<double> adPts;
    adPts.need( ( m_apt2D.num() + m_apt2DExtra.num() + m_polyDomain.Num_pts() ) * 2 );
    
    int iIndex = 0;
    m_aptSurface.resize( adPts.num() / 2 );
    for ( int i = 0; i < m_polyDomain.Num_pts(); i++ ) {
        m_aptSurface[ iIndex / 2 ] = R3Pt( m_polyDomain[i][0], m_polyDomain[i][1], 0.0);
        for ( int j = 0; j < 2; j++, iIndex++ )
            adPts[iIndex] = m_polyDomain[i][j];
    }
    for ( int i = 0; i < m_apt2D.num(); i++ ) {
        m_aptSurface[ iIndex / 2 ] = R3Pt( m_apt2D[i][0], m_apt2D[i][1], 0.0 );
        for ( int j = 0; j < 2; j++, iIndex++ )
            adPts[iIndex] = m_apt2D[i][j];
    }
    for ( int i = 0; i < m_apt2DExtra.num(); i++ ) {
        m_aptSurface[ iIndex / 2 ] = R3Pt( m_apt2DExtra[i][0], m_apt2DExtra[i][1], 0.0 );
        for ( int j = 0; j < 2; j++, iIndex++ )
            adPts[iIndex] = m_apt2DExtra[i][j];
    }
    m_avecSurface.resize( m_aptSurface.size() );
    for ( int i = 0; i < m_avecSurface.size(); i++ ) {
        m_avecSurface[i] = R3Vec(0,0,1);
    }
    
   	/* call qhull to make delaunay triangulation*/
	char flags[]= "qhull d Qt Q3"; /* option flags for qhull, see qh_opt.htm */
	int exitcode = qh_new_qhull ( 2, adPts.num() / 2, &adPts[0], False, flags, NULL, NULL);
	VERIFY(exitcode == 0);
    
	facetT *facet = NULL;	          /* set by FORALLfacets */
    setT *vertices = NULL;
    vertexT *vertex = NULL, **vertexp = NULL;

    // Make triangles, keeping only triangles that are attached to interior points
    const int iLastInside = m_apt2D.num() + m_polyDomain.Num_pts();
    R3Pt_i iptFace;
    m_afaceSurface.resize(0);
    
    // For each triangle
	FORALLfacets {
        
        vertices = qh_facet3vertex (facet);
        
        // Don't use triangles that touch the outside circle points
        int i = 0;
        bool bKeep = true, bOnEdge = true;
        FOREACHvertex_(vertices) {
            const int iId = qh_pointid(vertex->point);
            
            if ( iId >= iLastInside )
                bKeep = false;
            if ( i >= 4 )
                bKeep = false;
            else
                iptFace[i] = iId;
            i++;
            
            if ( iId >= m_polyDomain.Num_pts() ) {
                bOnEdge = false;
            }
        }
        if ( bKeep == false )
            continue;

        if ( bOnEdge == true ) {
            R2Pt ptCenter(0,0);
            for ( int j = 0; j < 3; j++ ) {
                ptCenter[0] += m_polyDomain[ iptFace[j] ][0] / 3.0;
                ptCenter[1] += m_polyDomain[ iptFace[j] ][1] / 3.0;
            }
            if ( m_polyDomain.Inside_winding( ptCenter ) ) {
                m_afaceSurface.push_back( iptFace );
            }
        } else {
            m_afaceSurface.push_back( iptFace );
        }
	}
    
    // Free memory
	qh_freeqhull(!qh_ALL);  
    
	int curlong, totlong;	  /* memory remaining after qh_memfreeshort */
	qh_memfreeshort (&curlong, &totlong);
	if (curlong || totlong) {
		cerr << "qhull internal warning (main): did not free " << totlong << "bytes of long memory (" << curlong << " pieces)\n";
    }
}

/* Do a radial basis function fit to boundary points plus one extra point in middle */
void PatchSurface::FitSurface()
{
    FITRbfHermite3D oFH;
    Array<R2Pt> aptTangent2D;
    Array<R3Vec> avecNorm;
    
    //aptTangent2D += R2Pt(0,0);
    //avecNorm += R3Vec(0,0,1);
    /*
    m_arayNormalConstraints.resize(0);
    for ( int i = 0; i < m_polyDomain.Num_pts(); i += 3 ) {
        aptTangent2D += m_polyDomain[i];
        avecNorm += m_avecNormals[i];
        m_arayNormalConstraints.push_back( R3Ray( m_polySurface.Pts()[i], m_avecNormals[i] ) );
    }
     */
    /*
    m_arayNormalConstraints.resize(0);
    for ( int i = 0; i < m_aiCorners.num(); i++ ) {
        aptTangent2D += m_polyDomain[ m_aiCorners[i] ];
        avecNorm += m_avecNormals[  m_aiCorners[i] ];
        m_arayNormalConstraints.push_back( R3Ray( m_polySurface.Pts()[ m_aiCorners[i] ], m_avecNormals[ m_aiCorners[i] ] ) );
    }
     */

    m_arayNormalConstraints.resize(0);
    /*
    for ( int i = 0; i < m_polyDomain.Num_pts(); i += 3 ) {
        
        aptTangent2D += Lerp( m_polyDomain[ i ], m_polyDomain.PtWrap(i+1), 0.5 );
        avecNorm += UnitSafe( m_avecNormals[i] + m_avecNormals.wrap(i+1) );
        m_arayNormalConstraints.push_back( R3Ray( Lerp( m_polySurface[ i ], m_polySurface.PtWrap(i+1), 0.5 ), avecNorm.last() ) );
    }
     */
    
    // Normal constraints on interior of polygon
    for ( int i = 0; i < m_aiCorners.num(); i++ ) {
        const R2Pt ptMid = Lerp( m_polyDomain[ m_aiCorners.wrap(i-1) ], m_polyDomain[ m_aiCorners.wrap(i+1) ], 0.5 );
        if ( !m_polyDomain.Inside_winding( ptMid ) ) {
            continue;
        }
        
        aptTangent2D += ptMid;
        avecNorm += UnitSafe( m_avecNormals[  m_aiCorners.wrap(i-1) ] + m_avecNormals[ m_aiCorners[i] ] + m_avecNormals[ m_aiCorners.wrap(i+1) ] );
        m_arayNormalConstraints.push_back( R3Ray( Lerp( m_polySurface[ m_aiCorners.wrap(i-1) ], m_polySurface[ m_aiCorners.wrap(i+1) ], 0.5 ), avecNorm.last() ) );
    }
    double dAvgSpacing = 0.0;
    double dAvgSpacing2D = 0.0;
    for ( int i = 0; i < m_polySurface.Num_pts(); i++ ) {
        dAvgSpacing += ::Length( m_polySurface.PtWrap(i+1) - m_polySurface[i] );
        dAvgSpacing2D += ::Length( m_polyDomain.PtWrap(i+1) - m_polyDomain[i] );
    }
    dAvgSpacing /= (double) m_polySurface.Num_pts();
    dAvgSpacing2D /= (double) m_polySurface.Num_pts();

    Array<R2Pt> apt2D = m_polyDomain.Pts();
    Array<R3Pt> apt3D = m_polySurface.Pts();

    // Point/normal constraints at corners
    R3Matrix matRot;
    R2Matrix matSpin = R2Matrix::Rotation( -M_PI / 2.0 );
    R3Matrix matSpin3D = R3Matrix::Rotation(2, -M_PI / 2.0 );
    R4Matrix matTrans, matTransBack;
    for ( int i = 0; i < m_polyDomain.Num_pts(); i++ ) {
        if ( m_aiCorners.contains(i) ) {
            continue;
        }
        const R2Vec vec2D = m_polyDomain.PtWrap( i+1 ) - m_polyDomain.PtWrap( i-1 );
        const R3Vec vec3D = UnitSafe( m_polySurface.PtWrap( i+1 ) - m_polySurface.PtWrap( i-1 ) ) * dAvgSpacing;
        
        R3Matrix::MatrixVecToVec( matRot, m_avecNormals[ i ], R3Vec(0,0,1) );
        const R2Vec vecRot = matSpin * vec2D;
        apt2D += m_polyDomain[ i ] + UnitSafe(vecRot) * (0.5 * dAvgSpacing2D);
        const R3Vec vecRot3D = matRot.Transpose() * matSpin3D * matRot * vec3D;
        apt3D += m_polySurface[ i ] + vecRot3D;
        //m_aptPointConstraints.push_back( m_matToPatch * R3Pt( apt2D.last()[0], apt2D.last()[1], 0.0)  );
        m_arayNormalConstraints.push_back( R3Ray( apt3D.last(), m_avecNormals[ i ] ) );
    }
    
    //oFH.SetTangent( apt2D, apt3D, aptTangent2D, avecNorm );
    oFH.Set( apt2D, apt3D );
    
    for ( int i = 0; i < m_polyDomain.Num_pts(); i++ ) {
        m_aptSurface[i] = oFH.Eval( m_polyDomain[i] );
    }
    const int iOffset = m_polyDomain.Num_pts();
    for ( int i = 0; i < m_apt2D.num(); i++ ) {
        m_aptSurface[i+iOffset] = oFH.Eval( m_apt2D[i] );        
    }
    
    R3Pt pt;
    R3Vec vecDs, vecDt;
    for ( int i = 0; i < aptTangent2D.num(); i++ ) {
        oFH.AllEvalSub( aptTangent2D[i], pt, vecDs, vecDt );
        cout << avecNorm[i] << " " << UnitSafe( Cross( vecDs, vecDt ) ) << "\n";
    }
}

void PatchSurface::CreatePatch( const Array<int> &in_aiCrvs, const Array< pair<double,double> > &in_adStartStop )
{
    CreatePolygon( in_aiCrvs, in_adStartStop );
    UnwindPolygon();
    CreateDomainPoints( );
    FitSurface();
}

PatchSurface::PatchSurface( const PatchSurface &in_patch ) 
: m_crvNW( in_patch.m_crvNW ) 
{
}

void PatchSurface::DrawConstraints() const
{
    SurfaceData::DrawConstraints();
    DrawPolygon();
}

void PatchSurface::DrawPolygon() const
{
	const double dScl = 0.5 * g_drawState.StripWidth() * g_paramFile.GetDouble("ScalePointConstraints");
    
    g_drawState.StartShader( DrawState::SURFACE_SHADING );	
    glEnable( GL_CULL_FACE );
	glMatrixMode( GL_MODELVIEW );
    
	R3Matrix matRot;
	R4Matrix matFull;
    
	const UTILSColor colConstraints = UTILSColor::GREY;
    
	if ( g_drawState.m_opUI->m_bShowPointConstraint->value() ) {
        glColor4f( 1.0, 0.0, 1.0, 1.0f );
        for ( int i = 0; i < m_aiCorners.num(); i++ ) {
            const R2Vec vec = m_polyDomain[ m_aiCorners.wrap(i+1) ] - m_polyDomain[ m_aiCorners[i] ];
            const R3Pt pt = m_matToPatch * R3Pt( m_polyDomain[ m_aiCorners[i] ][0], m_polyDomain[ m_aiCorners[i] ][1], 0.0 );

			OGLDrawSphere( R3Sphere( pt, dScl ) );

			glPushMatrix();            
			R3Matrix::MatrixVecToVec( matRot, R3Vec(0,1,0), UnitSafe( m_matToPatch * R3Vec( vec[0], vec[1], 0.0 ) ) );
			matFull = R4Matrix::Translation( pt - R3Pt(0,0,0) ) * (R4Matrix) matRot;
			glMultMatrixd( &matFull(0,0) );
			OGLDrawArrow( R3Pt(0,0,0), ::Length(vec), dScl );
			glPopMatrix();
        }

        glColor4f( colConstraints[0], colConstraints[1], colConstraints[2], 1.0f );
		for ( int i = 0; i < m_polyDomain.Num_pts(); i++ ) {
            const R3Pt pt = m_matToPatch * R3Pt( m_polyDomain[i][0], m_polyDomain[i][1], 0.0 );
			OGLDrawSphere( R3Sphere( pt, dScl ) );
		}
	}
    
    g_drawState.EndShader();
}


