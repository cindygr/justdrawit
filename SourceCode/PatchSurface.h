//
//  PatchSurface.h
//  SketchCurves
//
//  Created by Cindy Grimm on 12/10/12.
//  Copyright (c) 2012 Washington University in St. Louis. All rights reserved.
//

#ifndef SketchCurves_PatchSurface_h
#define SketchCurves_PatchSurface_h

#include <utils/Rn_Polygon.H>
#include <utils/R3_Plane.H>
#include <boost/dynamic_bitset.hpp>

#include "SurfaceData.h"

class PatchSurface : public SurfaceData
{
private:
    const CurveNetwork &m_crvNW;
    
    R2Polygon m_polyDomain;
    R3Polygon m_polySurface;
    Array<int>   m_aiCorners;
    Array<R3Vec> m_avecNormals;
    R3Plane  m_plane;
    R4Matrix m_matToPlane, m_matToPatch;
    Array<R3Pt_i> m_afFaces;
    Array<R2Pt> m_apt2D, m_apt2DExtra;
    
    /**@name creation
     * First create a 2D and 3D polygon from the curves
     * Next, scatter points in the domain
     * Then build the 2D surface
     * Fit the 2D points -> 3D points
     * Convert polygons inside to surface */
    void CreatePolygon( const Array<int> &in_aiCrvs, const Array< pair<double,double> > &in_adStartStop );
    bool EdgesIntersect( const Array<R2Pt> &in_apt ) const;
    void UnwindPolygon( Array<R2Pt> &io_apt, const boost::dynamic_bitset<> &in_abFixed, const R2Pt_i &in_iptPullApart, const int in_iNIters );
    void UnwindPolygon();
    void CreateDomainPoints( );
    void FitSurface();
    
    void DrawPolygon() const;
    
    PatchSurface &operator=( const PatchSurface & ) { return *this; }
    PatchSurface( const PatchSurface &in_patch );
public:
    void CreatePatch( const Array<int> &in_aiCrvs, const Array< pair<double,double> > &in_adStartStop );
    
    void DrawConstraints() const;
    
    PatchSurface( const CurveNetwork &in_crvNW ) : m_crvNW( in_crvNW ) { }
    ~PatchSurface() {}
};

#endif
