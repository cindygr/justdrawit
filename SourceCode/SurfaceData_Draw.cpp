/*
 *  SurfaceData_Draw.cpp
 *  SketchCurves
 *
 *  Created by Cindy Grimm on 2/18/2011
 *  Copyright 2011 Adobe Corp. All rights reserved.
 *
 */

#include "SurfaceData.h"
#include "ParamFile.h"
#include "DrawState.h"
#include <OpenGL/OGLDraw3D.H>
#include <FL/GL.H>
#include "UserInterface.h"

/* Draw the points and arrows */
void SurfaceData::Draw( const int in_iSelected ) const
{
	const double dScl = 1.1 * g_drawState.StripWidth() * g_paramFile.GetDouble("ScalePointConstraints");
	const UTILSColor colFixed = g_paramFile.GetColor("FixedIntersectionColor");
	const UTILSColor colPossible = g_paramFile.GetColor("PossibleIntersectionColor");
	const UTILSColor colSelected = g_paramFile.GetColor("SelectedIntersectionColor");
	const UTILSColor colCrvPts = g_paramFile.GetColor("IntersectionCurvePts");
    
    g_drawState.StartShader( DrawState::SURFACE_SHADING );	
    glEnable( GL_CULL_FACE );
    
	R3Matrix matRot;
	R4Matrix matFull;
	for ( int i = 0; i < m_aptIntersection.size(); i++ ) {
        if ( i == in_iSelected ) {
            glColor4f( colSelected[0], colSelected[1], colSelected[2], 1.0f );
        } else if ( m_aptIntersection[i].m_bNeedsFixing ) {
			glColor4f( colPossible[0], colPossible[1], colPossible[2], 1.0f );
        } else {
			glColor4f( colFixed[0], colFixed[1], colFixed[2], 1.0f );
		}
        
        for ( int j = 0; j < m_aptIntersection[i].m_acrvLocs.size(); j++ ) {
            OGLDrawSphere( R3Sphere( m_aptIntersection[i].m_aptPerCurve[j], dScl ) );
        }
	}
	
	for ( int i = 0; i < m_aptIntersection.size(); i++ ) {
        if ( i == in_iSelected ) {
            glColor4f( colSelected[0], colSelected[1], colSelected[2], 1.0f );
        } else {
            glColor4f( colPossible[0], colPossible[1], colPossible[2], 0.5f );
        }

        for ( int j = 0; j < m_aptIntersection[i].m_aptPerCurve.size(); j++ ) {
            for ( int k = j+1; k < m_aptIntersection[i].m_aptPerCurve.size(); k++ ) {
                const R3Vec vecPts = m_aptIntersection[i].m_aptPerCurve[j] - m_aptIntersection[i].m_aptPerCurve[k];
                glPushMatrix();
                R3Matrix::MatrixVecToVec( matRot, R3Vec(0,1,0), UnitSafe( vecPts ) );
                matFull = R4Matrix::Translation( m_aptIntersection[i].m_aptPerCurve[k] ) * (R4Matrix) matRot;
                glMultMatrixd( &matFull(0,0) );
                OGLDrawCylinder(R3Pt(0,0,0), ::Length(vecPts), dScl * 0.5);
                glPopMatrix();
            }
        }
	}
	
    g_drawState.EndShader();
}

/* Draw the points and tangents and normals used in the rbf computation */
void SurfaceData::DrawConstraints(  ) const
{
	const double dScl = 0.5 * g_drawState.StripWidth() * g_paramFile.GetDouble("ScalePointConstraints");
    
    g_drawState.StartShader( DrawState::SURFACE_SHADING );	
    glEnable( GL_CULL_FACE );
	glMatrixMode( GL_MODELVIEW );
    
	R3Matrix matRot;
	R4Matrix matFull;

	const UTILSColor colConstraints = g_paramFile.GetColor("SurfaceConstraintsColor");
    
	if ( g_drawState.m_opUI->m_bShowPointConstraint->value() ) {
        glColor4f( colConstraints[0], colConstraints[1], colConstraints[2], 1.0f );
		for ( int i = 0; i < m_aptPointConstraints.size(); i++ ) {
			OGLDrawSphere( R3Sphere( m_aptPointConstraints[i], dScl ) );
		}
	}

	if ( g_drawState.m_opUI->m_bShowTangentConstraint->value() ) {
		glColor4f( 0.75f, 0.5f, 0.75f, 1.0f );
		for ( int i = 0; i < m_arayTangentConstraints.size(); i++ ) {
			OGLDrawSphere( R3Sphere( m_arayTangentConstraints[i].Pt(), dScl ) );

			glPushMatrix();
			R3Matrix::MatrixVecToVec( matRot, R3Vec(0,1,0), m_arayTangentConstraints[i].Dir() );
			matFull = R4Matrix::Translation( m_arayTangentConstraints[i].Pt() - R3Pt(0,0,0) ) * (R4Matrix) matRot;
			glMultMatrixd( &matFull(0,0) );
			OGLDrawArrow( R3Pt(0,0,0), 2.0 * dScl, 2.0 * dScl * 0.3 );
			glPopMatrix();
		}
	}
	
	if ( g_drawState.m_opUI->m_bShowNormalConstraints->value() ) {
        glColor4f( colConstraints[0], colConstraints[1], colConstraints[2], 1.0f );
		for ( int i = 0; i < m_arayNormalConstraints.size(); i++ ) {
			OGLDrawSphere( R3Sphere( m_arayNormalConstraints[i].Pt(), dScl ) );
			
			glPushMatrix();
			R3Matrix::MatrixVecToVec( matRot, R3Vec(0,1,0), UnitSafe( m_arayNormalConstraints[i].Dir() ) );
			matFull = R4Matrix::Translation( m_arayNormalConstraints[i].Pt() - R3Pt(0,0,0) ) * (R4Matrix) matRot;
			glMultMatrixd( &matFull(0,0) );
			OGLDrawArrow( R3Pt(0,0,0), 2.0 * dScl, 2.0 * dScl * 0.3 );
			glPopMatrix();
		}
	}
	
    g_drawState.EndShader();
}

/* Draw the actual mesh */
void SurfaceData::DrawSurface() const
{
    g_drawState.StartShader( DrawState::SURFACE_SHADING );	

	if ( g_drawState.m_opUI->m_bSmoothShade->value() ) {
		glShadeModel(GL_SMOOTH);
	} else {
		glShadeModel(GL_FLAT);
	}
	const UTILSColor colSelected = g_paramFile.GetColor("SurfaceColor");
    const double dTransp = g_drawState.m_opUI->m_dTransparency->value();
	glColor4f( colSelected[0], colSelected[1], colSelected[2], dTransp );
	
    glEnable( GL_CULL_FACE );
    glFrontFace(GL_CW);
	glBegin( GL_TRIANGLES );
    for ( int i = 0; i < m_afaceSurface.size(); i++ ) {
		for ( int j = 0; j < 3; j++ ) {
			glNormal3dv( &m_avecSurface[ m_afaceSurface[i][j] ][0] );
			glVertex3dv( &m_aptSurface[ m_afaceSurface[i][j] ][0] );
		}
	}
	glEnd();
    glFrontFace(GL_CCW);
    glBegin( GL_TRIANGLES );
    for ( int i = 0; i < m_afaceSurface.size(); i++ ) {
        for ( int j = 0; j < 3; j++ ) {
            glNormal3dv( &m_avecSurface[ m_afaceSurface[i][j] ][0] );
            glVertex3dv( &m_aptSurface[ m_afaceSurface[i][j] ][0] );
        }
    }
    glEnd();
	
	if ( g_drawState.m_opUI->m_bShowEdges->value() ) {
        glColor4f( colSelected[0] * 0.7f, colSelected[1] * 0.7f, colSelected[2] * 0.7f, dTransp );
		glBegin( GL_LINES );
		for ( int i = 0; i < m_afaceSurface.size(); i++ ) {
			for ( int j = 0; j < 3; j++ ) {
				glNormal3dv( &m_avecSurface[ m_afaceSurface[i][j] ][0] );
				glVertex3dv( &m_aptSurface[ m_afaceSurface[i][j] ][0] );
				glNormal3dv( &m_avecSurface[ m_afaceSurface[i][(j+1)%3] ][0] );
				glVertex3dv( &m_aptSurface[ m_afaceSurface[i][(j+1)%3] ][0] );
			}
		}
		glEnd();
	}
	
    g_drawState.EndShader();
    
    /*
    glBegin( GL_POINTS );
    for ( int i = 0; i < m_aptSurface.size(); i++ ) {
        glVertex3dv( &m_aptSurface[i][0] );
    }
    glEnd();
     */
}