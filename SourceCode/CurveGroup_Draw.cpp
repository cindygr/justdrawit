/*
 *  CurveGroup_Draw.cpp
 *  SketchCurves
 *
 *  Created by Cindy Grimm on 9/2/10.
 *  Copyright 2010 Washington University in St. Louis. All rights reserved.
 *
 */


#include <FL/GL.H>
#include <utils/Utils_Color.H>

#include "CurveGroup.h"
#include "UserInterface.h"

void CurveGroup::DrawFragments( ) const
{
    if ( m_bDoFade ) {
		return;
	}
	
    for ( int i = m_aStrokeHistory.num() - 1; i >= 0; i-- ) {
        // Don't draw the glommed together stroke
        if ( i != 0 && m_aStrokeHistory[i-1].GetMergeType() == StrokeHistory::BLEND_SCRATCH ) {
            continue;
        }
        
        if ( m_aStrokeHistory[i].WasStroke() ) {
            CurveDrawData::DrawAsFragment( m_aStrokeHistory[i].Stroke3D() );
        }
    }
}

void CurveGroup::DrawCurve(  ) const

{
    m_crvDraw.Draw();
    if ( IsReflected() ) {
        m_crvDrawReflect.Draw();
    }
    
    if ( g_drawState.m_opUI->m_bShowShadows->value() ) {
        if ( g_drawState.m_opUI->m_bShowAllShadows->value() || m_crvDraw.IsActive() ) {
            m_crvDraw.DrawShadow();
            if ( IsReflected() ) {
                m_crvDrawReflect.DrawShadow();
            }
        }
    }
}

///
void CurveGroup::SetHidden( const bool in_bHidden )
{
    m_crvDraw.SetHidden( in_bHidden );
    
    if ( IsReflected() ) {
        m_crvDrawReflect.SetHidden( in_bHidden );
    }
}



///
void CurveGroup::ToggleHide()
{
    m_crvDraw.ToggleHide();
    
    if ( IsReflected() ) {
        m_crvDrawReflect.SetHidden( m_crvDraw.IsHidden() );
    }
}

void CurveGroup::SetActive( const bool in_bActive )
{
    m_crvDraw.SetActive( in_bActive );
    
    if ( IsReflected() ) {
        m_crvDrawReflect.SetActive( in_bActive );
    }
}


void CurveGroup::SetShadow( const bool in_bShadow )
{
    m_crvDraw.SetShadow( in_bShadow );
    
    if ( IsReflected() ) {
        m_crvDrawReflect.SetShadow( in_bShadow );
    }
}


/// Color may need to be changed (selected region is yellow)
void CurveGroup::SetSelectionRegion( const std::pair<double,double> &in_dTOnCrv ) 
{
    m_crvDraw.SetSelectionRegion( in_dTOnCrv );
    if ( IsReflected() ) {
        m_crvDrawReflect.SetSelectionRegion( in_dTOnCrv );
    }
    
    for ( int iP = 0; iP < NumConstraints(); iP++ ) {
        m_aopPins[iP]->SelectedRegionChanged();
    }
}

///
void CurveGroup::ClearSelectionRegion()
{
    m_crvDraw.ClearSelectionRegion( );
    if ( IsReflected() ) {
        m_crvDrawReflect.ClearSelectionRegion( );
    }
}

void CurveGroup::UpdateDrawData()
{
    Array<double> adTs;
    for ( int i = 0; i < NumConstraints(); i++ ) {
        if ( GetConstraint(i).IsPoint() ) {
            const double dT = GetConstraint(i).GetTValue( *this );
            adTs += dT;
            if ( RNIsZero( dT ) && CompositeCurve().IsClosed() ) {
                adTs += 1.0;
            }
        }
    }
    m_crvDraw.Update( adTs );
    if ( IsReflected() ) {
        m_crvDrawReflect.Update(adTs);
    }
    
    for ( int i = 0; i < m_crvRibbon.NumPts() - 2; i++ ) {
        if ( ApproxEqual( m_crvRibbon.Pt(i), m_crvRibbon.Pt(i+2), 1e-6 ) ) {
            cerr << "Same pt " << i << "\n";
        }
    }    
}


void CurveGroup::TubeGeometry( std::vector<R3Pt> &io_aptVs, std::vector<R3Vec> &io_avecVNs, std::vector< R3Pt_i > &io_aptFs ) 
{
    UpdateDrawData();
    m_crvDraw.TubeGeometry( io_aptVs, io_avecVNs, io_aptFs );
    if ( IsReflected() ) {
        m_crvDrawReflect.TubeGeometry( io_aptVs, io_avecVNs, io_aptFs );
    }
}
