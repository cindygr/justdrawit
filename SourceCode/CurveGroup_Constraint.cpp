/*
 *  CurveGroup_Constraint.cpp
 *  SketchCurves
 *
 *  Created by Cindy Grimm on 9/20/11.
 *  Copyright 2011 Washington University in St. Louis. All rights reserved.
 *
 */


#include "CurveGroup.h"
#include "PointConstraint.h"
#include "CurveNetwork.h"


/* If we add a pin in, adjust all the upstream pin indexes */
int CurveGroup::SplitSegment( const double in_dT, const double in_dClose )
{
    const int iNPts = m_crvRibbon.NumPts();
    const int iPt = m_crvRibbon.SplitSegment( in_dT, 0.01 );
   
    if ( m_crvRibbon.NumPts() != iNPts ) {
        for ( int i = 0; i < NumConstraints(); i++ ) {
            const int iOldIndex = GetConstraint(i).GetPtIndex( *this );
            const double dOldT = GetConstraint(i).GetTValue( *this );
            if ( iOldIndex > iPt ) {
                SetConstraint(i)->AddCurveGroup( this, iOldIndex+1, dOldT );
            }
        }
    }
    
    return iPt;
}

/* 
 * Find a drag window that is as big as possible, but doesn't touch the other constraints
 * If there are no other pins, just move the entire curve
 *
 *  Otherwise, move the points using Laplacian drag
 *
 * Fix the t/index values of the existing constraints and input constraint
 * Should work regardless of whether or not the constraint is in the list yet
 */
void CurveGroup::DragPointConstraint( const double in_dT, PointConstraint &io_pin, const bool in_bAddToStrokeHistory )
{
    if ( CompositeCurve().NumPts() < 2 ) {
        return;
    }
    
    if ( CurveNetwork::s_bTraceAddingConstraints ) {
        cout << "Curve " << HashId() << " ";
        if ( m_crvRibbon.IsClosed() ) cout << "closed, ";
        cout << "pin " << io_pin.HashId() << " " << io_pin.GetTValue(*this) << " npts " << m_crvRibbon.NumPts() << "\n";
    }
    
    const R3Pt pt = CompositeCurve()( in_dT );
    if ( ApproxEqual( pt, io_pin.GetPt() ) ) {
        // Already interpolate - just split segment or shift end point
        const int iPt = SplitSegment( in_dT, 0.01 );
        
        io_pin.AddCurveGroup( this, iPt, m_crvRibbon.PercAlong( iPt ) );

        if ( CurveNetwork::s_bTraceAddingConstraints ) cout << " Just spliting " << iPt << "\n";

        return;
    }
    
    const int iPt = SplitSegment( in_dT, 0.01 );
    
    const R3Vec vec = io_pin.GetPt() - m_crvRibbon.Pt(iPt);
    if ( RNIsZero( ::Length(vec) ) ) {
        io_pin.AddCurveGroup( this, iPt, m_crvRibbon.PercAlong( iPt ) );

        if ( CurveNetwork::s_bTraceAddingConstraints ) cout << " Just spliting II " << iPt << "\n";
        return;
    }
    
    if ( CurveNetwork::s_bTraceAddingConstraints ) cout << "Split and drag " << iPt << "\n";
    
    const int iNPts = m_crvRibbon.NumPts();
    
    static boost::dynamic_bitset<> abNoCross;
    abNoCross.resize( iNPts );
    abNoCross.reset();
    
    int iCountConstraints = 0;
    for ( int i = 0; i < NumConstraints(); i++ ) {
        if ( !GetConstraint(i).IsPoint() ) {
            continue;
        }
        
        if ( GetConstraint(i) == io_pin ) {
            continue; 
        }

        iCountConstraints++;
        
        const double dTValue = GetConstraint(i).GetTValue( *this );
        const int iIndexValue = CompositeCurve().Floor(dTValue);
        abNoCross.set( iIndexValue );
        if ( dTValue > CompositeCurve().PercAlong( iIndexValue ) ) {
            if ( iIndexValue == iNPts - 1 ) {
                if ( m_crvRibbon.IsClosed() ) {
                    abNoCross.set(0);
                }
            } else {
                abNoCross.set( iIndexValue+1 );
            }
        }
        if ( CurveNetwork::s_bTraceAddingConstraints ) {
            cout << "Pin " << GetConstraint(i).HashId() << " " << GetConstraint(i).GetTValue(*this) << " " << GetConstraint(i).GetPtIndex(*this) << " ";
            cout << " blocked " << iIndexValue << "\n";
        }
    }
    
    double dT = m_crvRibbon.PercAlong(iPt);
    double dTMin = dT - 1e-30;
    double dTMax = dT + 1e-30;
    
    const int iStopLeft = m_crvRibbon.IsClosed() ? iPt - iNPts / 2 : 0;
    const int iStopRight = m_crvRibbon.IsClosed() ? iPt + iNPts / 2 : iNPts;
    
    for ( int i = iPt; i >= iStopLeft; i-- ) {
        if ( abNoCross[ (i + iNPts) % iNPts ] ) {
            break;
        }
        dTMin = m_crvRibbon.PercAlong( (i + iNPts) % iNPts ) + 1e-30;
        if ( i < 0 ) dTMin = dTMin - 1.0;
    }
    for ( int i = iPt; i < iStopRight; i++ ) {
        if ( abNoCross[ i % iNPts ] ) {
            break;
        }
        dTMax = m_crvRibbon.PercAlong( i % iNPts ) - 1e-30;
        if ( i >= iNPts ) dTMax = 1.0 + dTMax;
    }

    if ( CurveNetwork::s_bTraceAddingConstraints ) {
        cout << "left " << iStopLeft << " right " << iStopRight << " ";
        cout << dTMin << " " << dTMax << " ";
        cout << m_crvRibbon.Floor( dTMin < 0.0 ? dTMin + 1.0 : dTMin ) << " ";
        cout << iPt << " ";
        cout << m_crvRibbon.Ceil( dTMax > 1.0 ? dTMax - 1.0 : dTMax ) << "\n";
    }
    
    const R3Vec vecPlaneNorm1 = Cross( UnitSafe(vec), m_crvRibbon.GetTangentAtPoint(iPt) );
    const R3Vec vecPlaneNorm2 = Cross( UnitSafe(vec), m_crvRibbon.GetNormalAtPoint(iPt) );
    const R3Plane plane( m_crvRibbon.Pt( iPt ), RNIsZero( ::Length( vecPlaneNorm1 ), 1e-6 ) ? UnitSafe( vecPlaneNorm2 ) : UnitSafe( vecPlaneNorm1 ) );
            
    if ( iCountConstraints == 0 ) {
        m_crvRibbon.Drag( vec );
    } else {
        m_crvRibbon.Drag( plane, vec, dT, std::pair<double,double>(dTMin, dTMax), true );
    }

    if ( in_bAddToStrokeHistory == true ) {
        bool bDoAdd = true;
        if ( m_aStrokeHistory.num() ) {
            const StrokeHistory &sh = m_aStrokeHistory.last();
            if ( sh.GetMergeType() == StrokeHistory::CHANGED_CONSTRAINT ) {
                if ( sh.PinHashId().num() && sh.PinHashId()[0] == io_pin.HashId() && sh.GetTimeStamp() == CurveNetwork::CurrentTime() ) {
                    bDoAdd = false;
                }
            }
        }
        if ( bDoAdd == true ) {
            m_aStrokeHistory.add(1);
            m_aStrokeHistory.last().ChangedConstraint( io_pin.HashId(), m_crvRibbon );
        } else {
            m_aStrokeHistory.last().CurveChanged( m_crvRibbon );        
        }
    }
    
    // Now fix t values of all constraints
    for ( int i = 0; i < m_aopPins.num(); i++ ) {
        const int iPtIndex = m_aopPins[i]->GetPtIndex( *this );
        m_aopPins[i]->AddCurveGroup( this, iPtIndex, m_crvRibbon.PercAlong( iPtIndex ) );
    }
    
    io_pin.AddCurveGroup( this, iPt, m_crvRibbon.PercAlong( iPt ) );
}

/*
 * Sort by t value
 */
void CurveGroup::SortPointConstraints()
{
    static Array<double> adTs;
    
    adTs.need( m_aopPins.num() );
    for ( int i = 0; i < adTs.num(); i++ ) {
        adTs[i] = m_aopPins[i]->GetTValue( *this );
    }
    
    m_aopPins.sort( adTs );
}

/* Count number of point constraints */
int CurveGroup::NumPointConstraints() const
{
    int iRet = 0;
    for ( int i = 0; i < NumConstraints(); i++ ) {
        if ( GetConstraint(i).IsPoint() ) {
            iRet++;
        }
    }
    
    return iRet;
}

/* Add to point constraint list, then add constraint in
 * If normal constraint, find the closest point and put the normal there, then re-calc normals
 * If point constraint, drag the curve to meet the point constraint
 *   If need be, introduce a new point into the list to make this happen
 * If both, do point first than normal
 * In either case, update the geometry
 */
void CurveGroup::AddConstraint( PointConstraint *io_opPin, const double in_dT, const bool in_bDoDrag )
{
    io_opPin->AddCurveGroup( this, CompositeCurve().ClosestIndex( in_dT ), in_dT );
    m_aopPins += io_opPin;
        
    m_aStrokeHistory.add(1);
    m_aStrokeHistory.last().AddedConstraint( io_opPin->HashId(), m_crvRibbon );

    // will update t and index values 
    if ( io_opPin->IsPoint() && in_bDoDrag == true ) {
        CurveNetwork::IncrementTime(); // Seperate the addition of the point constraint from the drag
        DragPointConstraint( in_dT, *io_opPin, false );
    }
    
    SortPointConstraints();

    GeometryChanged( in_bDoDrag ? StrokeHistory::TRANSFORM : StrokeHistory::ADDED_CONSTRAINT );
    
    m_crvDraw.AddedOrRemovedConstraint();
    m_crvDrawReflect.AddedOrRemovedConstraint();
    
}

/// Return a constraint if it's close
PointConstraint *CurveGroup::HasConstraint( const double in_dT )
{
    const PointConstraint *opPin = HasConstraintNoEdit( in_dT );
    if ( opPin == NULL ) {
        return NULL;
    }
    
    for ( int i = 0; i < m_aopPins.num(); i++ ) {
        if ( opPin == m_aopPins[i] ) {
            return m_aopPins[i];
        }
    }
    
    return NULL;
}

/// Return a constraint if it's close
const PointConstraint *CurveGroup::HasConstraintNoEdit( const double in_dT ) const
{
    const double dTSel = WINmin(0.1, 1.5 * g_drawState.SelectionSize() / WINmax( CompositeCurve().Length(), 0.0001 ) );
    for ( int i = 0; i < m_aopPins.num(); i++ ) {
        const double dT = m_aopPins[i]->GetTValue( *this );
        if ( fabs( dT - in_dT ) < dTSel ) {
            return m_aopPins[i];
        }
        if ( CompositeCurve().IsClosed() ) {
            if ( in_dT < 0.5 ) {
                if ( fabs( dT - (in_dT + 1.0) ) < dTSel ) {
                    return m_aopPins[i];
                }
            } else {
                if ( fabs( dT - (in_dT - 1.0) ) < dTSel ) {
                    return m_aopPins[i];
                }
            }
        }
    }
    
    return NULL;
}

/// Remove self from pin and remove pin from list
void CurveGroup::RemoveConstraint( PointConstraint *in_opPin )
{
    bool bFound = false;
    for ( int i = 0; i < m_aopPins.num(); i++ ) {
        if ( *m_aopPins[i] == *in_opPin ) {
            m_aopPins.del( i, 1 ); // Do this before geometry changed, so won't be in update list

            if ( in_opPin->IsNormal() ) {
                GeometryChanged( StrokeHistory::REMOVED_CONSTRAINT );
            }
            if ( in_opPin->IsPoint() ) {
                m_crvDraw.AddedOrRemovedConstraint();
                if ( IsReflected() ) m_crvDrawReflect.AddedOrRemovedConstraint();
            }
            
            bFound = true;
            i--;
        }
    }
    
    if ( bFound == false ) {    
        cerr << "ERR: CurveGroup::RemoveConstraint, not found " << in_opPin->HashId() << "\n";
    } else {
        m_aStrokeHistory.add(1);
        m_aStrokeHistory.last().RemovedConstraint( in_opPin->HashId(), m_crvRibbon );
    }        
}

/// Fix just this point constraint
void CurveGroup::ReEstablishPointConstraint( PointConstraint &in_pin, const bool in_bDoGeomUpdate )
{
    bool bHasConstraint = false;
    for ( int i = 0; i < NumConstraints(); i++ ) {
        if ( in_pin == GetConstraint(i) ) {
            bHasConstraint = true;
        }
    }
    
    if ( bHasConstraint == false ) {
        cerr << "ERR:  CurveGroup::ReEstablishPointConstraint does not have pin constraint " << in_pin.HashId() << "\n";
    }
    
    const double dT = in_pin.GetTValue(*this);
    
    DragPointConstraint( dT, in_pin, true );
    
	if ( in_bDoGeomUpdate ) {
		GeometryChanged( StrokeHistory::TRANSFORM );
	}
}

void CurveGroup::ReEstablishNormalConstraints()
{
    GeometryChanged( StrokeHistory::CHANGED_CONSTRAINT );
}

PointConstraint *CurveGroup::SetConstraint( const int in_i ) 
{
    if ( in_i < 0 || in_i >= NumConstraints() ) {
        return NULL;
    }
    
    return m_aopPins[in_i];
}

void CurveGroup::RemoveCurveFromPointConstraints()
{
    // This will eventually call RemoveConstraint
    while ( m_aopPins.num() ) {
        m_aopPins[0]->RemoveCurveGroup( this );
    }
    
    if ( m_aopPins.num() != 0 ) {
        cerr << "ERR: CurveGroup::RemoveCurveFromPointConstraints, should have removed all point constraints but didn't: ";
        for ( int i = 0; i < m_aopPins.num(); i++ ) {
            cerr << m_aopPins[i]->HashId() << " ";
        }
        cerr << "\n";
        m_aopPins.need(0);
    }
}

void CurveGroup::CheckConsistency() const
{
    for ( int i = 0; i < NumConstraints(); i++ ) {
        if ( !GetConstraint(i).HasCurveGroup( *this ) ) {
            cerr << "ERR: CurveGroup::CheckConsistency pin " << GetConstraint(i).HashId() << " does not have curve " << HashId() << "\n";
        }
        for ( int j = i+1; j < NumConstraints(); j++ ) {
            if ( GetConstraint(i) == GetConstraint(j) ) {
                cerr << "ERR: CurveGroup::CheckConsistency pin " << GetConstraint(i).HashId() << " duplicated\n";
            }
        }
    }
}

/*
 * What can happen on an edit:
 *   Add another piece to the curve. 
 *     There's some chance that the pin itself will move, but for the most part this should
 *     be just changing the t and index values for the curve.
 *   Overstroke or partial drag
 *     Case 1: the pin is not inside of the edit region. In this case, as above, the t and index
 *             values will change, but not the point itself
 *     Case 2: the pin is inside of the edit region. In this case we need to know where the
 *             point went. For a drag, the number of points will remain the same and we
 *             can simply use the same point index.
 *             For an overstroke, probably the best bet is a percentage along match
 *   Erasing part of the curve
 *     Case 1: The pin is in the erased part. Remove the curve from the pin. Note that this may
 *             trigger the pin itself to be deleted
 *     Case 2: The pin is not in the erased part. Change the t and index values
 *   Smoothing: 
 *      Keep the pins in the same place. Drag the curve back to them after smoothing
 *   Combine: 
 *       This is essentially the same as adding another piece to the curve. However, some of the pins
 *       may come from the old curve. Mostly need to fix ts and indices
 *   Drag/transform:
 *       The point indices are the same. Just update point locations and t values.
 *
 *  Everything we need to know is stored in the stroke history.
 *
 *  What to do:
 *    1) Remove any constraints that are no longer valid
 *    2) Update t and index values
 *    3) Move any points that need to move -> this will fix all point constraints
 */

static bool s_bTracing = false;
void CurveGroup::RemoveDanglingConstraints( const StrokeHistory::MergeType in_action )
{
    if ( in_action != StrokeHistory::ERASE ) {
        return;
    }
    
    if ( s_bTracing ) {
        cout << "Remove dangling constraints\n";
        PrintHistory(true);
    }
    
    R3Pt pt, ptOut;
    double dDist;
    const Curve &crv = m_aStrokeHistory[ m_aStrokeHistory.num() - 1 ].Final();

    const double dSelDist = 0.25 * g_drawState.SelectionSize();
    Array<int> aiDel;
    for ( int i = 0; i < NumConstraints(); i++ ) {
        if ( GetConstraint(i).IsPoint() ) {
            pt = GetConstraint(i).GetPt();
        } else {
            pt = crv( GetConstraint(i).GetTValue( *this ) );
        }
        
        m_crvRibbon.ClosestPointTValue( pt, dDist, ptOut );
        if ( dDist > dSelDist ) {
            aiDel += i;
        }
    }
    
    aiDel.reverse();
    for ( int i = 0; i < aiDel.num(); i++ ) {
        // Will in turn remove this curve group from the pin list by calling RemoveConstraint
        SetConstraint( aiDel[i] )->RemoveCurveGroup( this );
    }
}

/*
 * Things that can happen:
 *    Curve can be extended outside of pin region or moved
 *         Changes t value but index is still valid/Pt(iIndex) == pin point
 *         Just update t value
 *    Curve can have actually moved a pin location
 *          Case 1) Project pin point onto curve and get new t/index value
 *          Case 2) Try to guestimate new t value by using same relative t distance (overstroke)
 */
void CurveGroup::FixTValuesOfConstraints( const StrokeHistory::MergeType in_action )
{
    const Curve &crv = m_aStrokeHistory[ m_aStrokeHistory.num() - 1 ].Final();

    if ( s_bTracing ) {
        cout << "Fix TValues\n";
        PrintHistory(true);
    }
    
    switch ( in_action ) {
        case StrokeHistory::MERGE :
        case StrokeHistory::MERGE_REVERSE :
        case StrokeHistory::JOIN :
        case StrokeHistory::JOIN_REVERSE :
        case StrokeHistory::CLOSE :
        case StrokeHistory::ERASE :
        case StrokeHistory::COMBINE : {
            // For all of these, we simply find the closest point on the curve
            // under the assumption that the curve hasn't moved much
            for ( int i = 0; i < NumConstraints(); i++ ) {
                int iPtIndex = GetConstraint( i ).GetPtIndex( *this );
                const R3Pt &pt = GetConstraint(i).GetPt();
                
                bool bDoClosest = false;
                if ( iPtIndex < 0 || iPtIndex >= m_crvRibbon.NumPts() ) {
                    bDoClosest = true;
                } else {
                    if ( !ApproxEqual( m_crvRibbon.Pt( iPtIndex ), pt ) ) {
                        bDoClosest = true;
                    }
                }
                if ( bDoClosest && m_crvRibbon.NumPts() > 1 ) {
                    const double dT = m_crvRibbon.ClosestPointTValue( pt );
                    iPtIndex = m_crvRibbon.ClosestIndex( dT );
                    SetConstraint(i)->AddCurveGroup( this, iPtIndex, m_crvRibbon.PercAlong(iPtIndex) );
                    if ( !ApproxEqual( m_crvRibbon.Pt( iPtIndex ), pt ) ) {
                        if ( s_bTracing ) cout << "Used closest, moved " << GetConstraint(i).HashId() << "\n";
                    }
                }
            }
            break;
        }
            
            
        case StrokeHistory::OVERSKETCH :
        case StrokeHistory::OVERSKETCH_START : {
            // If the original pt is close, and not in edit range, do same
            // as above. Otherwise, use percentage t value to calculate new t value
            const double dTStart = m_aStrokeHistory.last().OverstrokeStart();
            const double dTEnd = m_aStrokeHistory.last().OverstrokeEnd();
            
            const double dTStartNew = m_crvRibbon.ClosestPointTValue( crv(dTStart) );
            const double dTEndNew = m_crvRibbon.ClosestPointTValue( crv(dTEnd) );
            
            for ( int i = 0; i < NumConstraints(); i++ ) {
                const R3Pt &pt = GetConstraint(i).GetPt();
                int iPtIndex = GetConstraint( i ).GetPtIndex( *this );
                double dT = GetConstraint(i).GetTValue( *this );
                if ( m_crvRibbon.IsClosed() ) {
                    if ( dT < dTStart && dTEnd > 1.0 ) dT += 1.0;
                    if ( dT > dTEnd && dTStart < 0.0 ) dT -= 1.0;
                }
                
                bool bDoClosest = false;
                if ( iPtIndex < 0 || iPtIndex >= m_crvRibbon.NumPts() ) {
                    bDoClosest = true;
                } else {
                    if ( !ApproxEqual( m_crvRibbon.Pt( iPtIndex ), pt ) ) {
                        bDoClosest = true;
                    }
                }
                if ( bDoClosest ) {
                    if ( dT < dTStart || dT > dTEnd || RNApproxEqual( dTStart, dTEnd ) ) {                    
                        dT = m_crvRibbon.ClosestPointTValue( pt );
                    } else {
                        dT = dTStartNew + ( (dT - dTStart) / (dTEnd - dTStart) ) * (dTEndNew - dTStartNew);
                    }
                    iPtIndex = m_crvRibbon.ClosestIndex( dT );
                }
                SetConstraint(i)->AddCurveGroup( this, iPtIndex, m_crvRibbon.PercAlong(iPtIndex) );
                if ( !ApproxEqual( m_crvRibbon.Pt( iPtIndex ), pt ) ) {
                    if ( s_bTracing ) cout << "Used closest, moved " << GetConstraint(i).HashId() << "\n";
                }
            }
            break;
        }
        case StrokeHistory::REMOVED_CONSTRAINT :
            break; // do nothing - just fixing normals
            
        case StrokeHistory::UNDO :
        case StrokeHistory::SMOOTH : 
        case StrokeHistory::TRANSFORM : 
        default: {
            // index is the same, but t value may have changed
            // This should never hurt to do
            for ( int i = 0; i < NumConstraints(); i++ ) {                
                // Update t value
                const int iIndex = GetConstraint(i).GetPtIndex( *this );
                if ( iIndex >= 0 && iIndex < CompositeCurve().NumPts() ) {
                    const double dT = CompositeCurve().PercAlong( iIndex );
                    SetConstraint(i)->AddCurveGroup( this, iIndex, dT );
                } else {
                    cerr << "ERR: CurveGroup::FixTValuesOfConstraints, curve " << HashId() << " bad index " << iIndex << " " << CompositeCurve().NumPts() << "\n";
                    PrintHistory(true);
                    
                    const int iNewIndex = CompositeCurve().ClosestIndex( GetConstraint(i).GetTValue( *this ) );
                    SetConstraint(i)->AddCurveGroup( this, iNewIndex, CompositeCurve().PercAlong( iNewIndex ) );
                }
            }
            break;
        }
    }
}

void CurveGroup::MovePointConstraints( const StrokeHistory::MergeType in_action )
{
    if ( s_bTracing ) {
        cout << "Move Points\n";
        PrintHistory(true);
    }
    
    switch ( in_action ) {
        case StrokeHistory::MERGE :
        case StrokeHistory::MERGE_REVERSE :
        case StrokeHistory::JOIN :
        case StrokeHistory::JOIN_REVERSE :
        case StrokeHistory::CLOSE :
        case StrokeHistory::COMBINE :
        case StrokeHistory::OVERSKETCH :
        case StrokeHistory::OVERSKETCH_START : {
            // Move all the pins. Use the t value since the curve may have changed
            // Assumes the t values have already been re-adjusted
            for ( int i = 0; i < NumConstraints(); i++ ) {
                if ( GetConstraint(i).IsPoint() ) {
                    const double dT = GetConstraint(i).GetTValue( *this );
                    const R3Vec vecMove = CompositeCurve()( dT ) - GetConstraint(i).GetPt();
                    SetConstraint(i)->DragPointConstraint( vecMove, true );
                    
                    if ( CurveNetwork::s_bTraceCombine ) {
                        cout << "Id " << GetConstraint(i).HashId() << " vec " << vecMove << "\n";
                    }
                } else if ( GetConstraint(i).NumCurves() == 1 ) {
                    SetConstraint(i)->CurveMovedReprojectNormal();
                }
            }
            break;
        }
        case StrokeHistory::UNDO :
        case StrokeHistory::TRANSFORM : {
            // Move all the pins. Use the point index, since the edit may be big
            for ( int i = 0; i < NumConstraints(); i++ ) {
                if ( GetConstraint(i).IsPoint() ) {
                    const int iIndex = GetConstraint(i).GetPtIndex( *this);
                    if ( iIndex >= 0 && iIndex < CompositeCurve().NumPts() ) {
                        const R3Vec vecMove = CompositeCurve().Pt( iIndex ) - GetConstraint(i).GetPt();
                        if ( s_bTracing && !RNIsZero( ::Length(vecMove) ) ) {
                            cout << "Actually moving " << GetConstraint(i).HashId() << " " << vecMove << "\n";
                        }
                        SetConstraint(i)->DragPointConstraint( vecMove, true );
                    } else {
                        cerr << "ERR: CurveGroup::MovePointConstraints, bad index " << iIndex << " " << CompositeCurve().NumPts() << " curve " << HashId() << " pin " << GetConstraint(i).HashId() << "\n";
                        PrintHistory(true);
                    }
                } else if ( GetConstraint(i).NumCurves() == 1 ) {
                    SetConstraint(i)->CurveMovedReprojectNormal();
                }
            }
            break;
        }
        case StrokeHistory::SMOOTH : {
            // Move the curve so that the point constraint is satisfied
            for ( int i = 0; i < NumConstraints(); i++ ) {
                if ( GetConstraint(i).IsPoint() ) {
                    DragPointConstraint( GetConstraint(i).GetTValue(*this), *SetConstraint(i), false );
                } else if ( GetConstraint(i).NumCurves() == 1 ) {
                    SetConstraint(i)->CurveMovedReprojectNormal();
                }
            }
            break;
        }
        case StrokeHistory::REMOVED_CONSTRAINT : 
            break; // Do nothing, just updating normals
        default:
            break;
        }
}

void CurveGroup::PropagateEdit( const StrokeHistory::MergeType in_action )
{
    if ( NumConstraints() == 0 || m_aStrokeHistory.num() == 0 ) {
        if ( m_aStrokeHistory.num() == 0 ) {
            cerr << "ERR: CurveGroup::PropagateEdit(), stroke history empty\n";
        }
        return;
    }
    
    if ( CompositeCurve().NumPts() == 0 ) {
        return;
    }

    if ( m_bInGeometryUpdate == true ) {
        return;
    }
    
    if ( CurveNetwork::s_bTracePropagate ||
         ( CurveNetwork::s_bTraceCombine && in_action == StrokeHistory::COMBINE ) || 
         ( CurveNetwork::s_bTraceErase && in_action == StrokeHistory::ERASE ) ||
         ( CurveNetwork::s_bTraceAddingConstraints && in_action == StrokeHistory::ADDED_CONSTRAINT ) ||
         ( CurveNetwork::s_bTraceDrag && in_action == StrokeHistory::TRANSFORM ) ) {
        s_bTracing = true;
        cout << "\nBegin propagate edit " << HashId() << " " << StrokeHistory::GetMergeTypeString( in_action ) << "\n";
        PrintHistory(true);
    } else {
        s_bTracing = false;
    }
    
    m_bInGeometryUpdate = true;
    
    RemoveDanglingConstraints( in_action );
    FixTValuesOfConstraints( in_action );
    MovePointConstraints( in_action );

    if ( s_bTracing ) {
        cout << "Done propagate edit " << HashId() << "\n";
        PrintHistory(true);
    }
    
    m_bInGeometryUpdate = false;
}
