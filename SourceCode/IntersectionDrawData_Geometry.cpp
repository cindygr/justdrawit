/*
 *  IntersectionDrawData_Geometry.cpp
 *  SketchCurves
 *
 *  Created by Cindy Grimm on 7/2/12.
 *  Copyright 2012 Washington University in St. Louis. All rights reserved.
 *
 */

#include "IntersectionDrawData.h"
#include "CurveDrawData.h"
#include "CurveGroup.h"
#include "PointConstraint.h"
#include <utils/Rn_Line_seg.H>

const bool s_bTrace = false;
const bool s_bTraceSpokeData = false;

// The geometry
void IntersectionDrawData::UpdateShadow()
{
}


pair<int,double> IntersectionDrawData::NumSamplesWeb( ) const
{
    int iMax = 1;
    Array<double> adTStart( m_aSpokeData.size() ), adTEnd( m_aSpokeData.size() );
    double dAvgSpacing = 0.0;
    for ( int i = 0; i < m_aSpokeData.size(); i++ ) {
        const int iNPts = abs( m_aSpokeData[i].m_iPin - m_aSpokeData[i].m_iMid );
        iMax = WINmax( iNPts,iMax );
        const Curve &crv = m_pin.GetCurveGroup( m_aSpokeData[i].m_iCurve ).CompositeCurve();
        if ( crv.IsClosed() ) {
            adTStart[i] = crv.PercAlongWrap( m_aSpokeData[i].m_iPin );
            adTEnd[i] = crv.PercAlongWrap( m_aSpokeData[i].m_iMid );        
        } else {
            adTStart[i] = crv.PercAlongClamp( m_aSpokeData[i].m_iPin );
            adTEnd[i] = crv.PercAlongClamp( m_aSpokeData[i].m_iMid );        
        }
        dAvgSpacing += fabs( adTEnd[i] - adTStart[i] ) / (double) (iNPts + 1);
    }
    if ( s_bTrace ) {
        for ( int i = 0; i < adTStart.num(); i++ ) {
            cout << "Curve " << m_aSpokeData[i].m_iCurve << " " << adTStart[i] << " " << adTEnd[i] << "\n";
        }
    }
    
    return pair<int,double>( iMax, dAvgSpacing / (double) m_aSpokeData.size() );
}
    
void IntersectionDrawData::SetWebbing( const pair<int,double> in_iNSamples )
{
    const int iMax = in_iNSamples.first;
    const double dAvgSpacing = in_iNSamples.second;
    
    Array<double> adTStart( m_aSpokeData.size() ), adTEnd( m_aSpokeData.size() );
    for ( int i = 0; i < m_aSpokeData.size(); i++ ) {
        const Curve &crv = m_pin.GetCurveGroup( m_aSpokeData[i].m_iCurve ).CompositeCurve();
        if ( crv.IsClosed() ) {
            adTStart[i] = crv.PercAlongWrap( m_aSpokeData[i].m_iPin );
            adTEnd[i] = crv.PercAlongWrap( m_aSpokeData[i].m_iMid );        
        } else {
            adTStart[i] = crv.PercAlongClamp( m_aSpokeData[i].m_iPin );
            adTEnd[i] = crv.PercAlongClamp( m_aSpokeData[i].m_iMid );        
        }
    }

    m_aafPolygonFaces.resize( iMax-1 );
    
    m_aafColWebbing.resize( iMax );
    m_aaptTopPolygon.resize( iMax );
    m_aavecTopPolygonNorm.resize( iMax );
    m_aaptBotPolygon.resize( iMax );
    m_aavecBotPolygonNorm.resize( iMax );

    m_aafColWebbing[0].resize(1);
    m_aaptTopPolygon[0].resize(1);
    m_aavecTopPolygonNorm[0].resize(1);
    m_aaptBotPolygon[0].resize(1);
    m_aavecBotPolygonNorm[0].resize(1);

    const double dHeightMiddle = m_dStripWidth;
    m_aaptTopPolygon[0][0] = m_pin.GetPt() + m_pin.GetNormal() * dHeightMiddle;
    m_aavecTopPolygonNorm[0][0] = m_pin.GetNormal();
    m_aaptBotPolygon[0][0] = m_pin.GetPt() - m_pin.GetNormal() * dHeightMiddle;
    m_aavecBotPolygonNorm[0][0] = m_pin.GetNormal();
    for ( int i = 0; i < 3; i++ ) {
        m_aafColWebbing[0][0][i] = (float) s_colPointConstraint[i];
    }

    m_aafColWebbing[0][0][3] = 1.0f;

    Array<int> aiAroundLast( m_aSpokeData.size() ), aiAround( m_aSpokeData.size() ), aiStartSpoke( m_aSpokeData.size() );
    Array<R3Pt> apt( m_aSpokeData.size() ) ;
    Array<R3Vec> avecTang( m_aSpokeData.size() );
    Array<R3Vec> avecNorm( m_aSpokeData.size() );
    Array<R4Pt_f> acol( m_aSpokeData.size() );
    Array<double> adRing( m_aSpokeData.size() ), adT( m_aSpokeData.size() );
    int iTotal = 2 * m_aSpokeData.size(), iTotalLast = 0;
    
    m_aafPolygonFaces[0].resize( 0 );
    for ( int i = 0; i < iTotal; i++ ) {
        m_aafPolygonFaces[0].push_back( R4Pt_i( 0, 0, i, (i+1) % iTotal ) );
    }
    
    for ( int i = 0; i < aiStartSpoke.num(); i++ ) {
        aiStartSpoke[i] = m_aSpokeData[i].NumPts() - m_aSpokeData[i].NumWeb();
    }
    
    aiAround.fill( 2 );
    const double dStepSize = dAvgSpacing * 2.0;
    CRVHermite3 crvTop( 2 ), crvBot( 2 );
    R3Matrix matRot;
    for ( int iR = 1; iR < iMax; iR++ ) {
        // First fill in the point data for the current ring
        m_aaptTopPolygon[iR].resize( iTotal );
        m_aafColWebbing[iR].resize( iTotal );
        m_aaptTopPolygon[iR].resize( iTotal );
        m_aavecTopPolygonNorm[iR].resize( iTotal );
        m_aaptBotPolygon[iR].resize( iTotal );
        m_aavecBotPolygonNorm[iR].resize( iTotal );
        const double dFromTop = (double) iR / (double) (iMax);
        if ( s_bTrace ) cout << "Total " << iTotal << " From top " << dFromTop << "\n";
        for ( int iS = 0; iS < aiAround.num(); iS++ ) {
            adT[iS] = dFromTop * adTEnd[iS] + (1.0 - dFromTop) * adTStart[iS];
            const CurveRibbon &crv = m_pin.GetCurveGroup( m_aSpokeData[iS].m_iCurve ).GetCurveRibbon();
            apt[iS] = crv( adT[iS] );
            avecTang[iS] = crv.Tangent( adT[iS] );
            if ( m_aSpokeData[iS].m_bReverseDir ) {
                avecTang[iS] *= -1.0;
            }
            avecNorm[iS] = crv.Normal( adT[iS] );
            const int iIndex = crv.ClosestIndex( adT[iS] );
            acol[iS] = m_pin.GetCurveGroup( m_aSpokeData[iS].m_iCurve ).GetCurveDrawData().GetColor( iIndex );
        }
        int iPtIndex = 0;
        
        const double dPinch = 1.0 - pow( 1.0 - dFromTop, 2 );
        for ( int iS = 0; iS < aiAround.num(); iS++ ) {
            const int iRRev = iMax - iR - 1;
            const int iSNext = (iS+1) % aiAround.num();
            const int iTopSpokeIndex = !m_aSpokeData[iS].m_bReverseDir ? 0 : s_iTubeDivs / 2 - 1;
            const int iBotSpokeIndex = !m_aSpokeData[iS].m_bReverseDir ? s_iTubeDivs-1 : s_iTubeDivs / 2;
            const int iTopSpokeEndIndex = !m_aSpokeData[iSNext].m_bReverseDir ? s_iTubeDivs / 2 - 1 : 0;
            const int iBotSpokeEndIndex = !m_aSpokeData[iSNext].m_bReverseDir ? s_iTubeDivs / 2 : s_iTubeDivs-1;

            const R3Pt &ptTop = m_aaaptSpokes[iS][iTopSpokeIndex][ aiStartSpoke[iS] + iRRev ];
            const R3Pt &ptBot = m_aaaptSpokes[iS][iBotSpokeIndex][ aiStartSpoke[iS] + iRRev ];
            const R3Pt &ptTopEnd = m_aaaptSpokes[iSNext][iTopSpokeEndIndex][ aiStartSpoke[iSNext] + iRRev ];
            const R3Pt &ptBotEnd = m_aaaptSpokes[iSNext][iBotSpokeEndIndex][ aiStartSpoke[iSNext] + iRRev ];
            const R3Pt ptSquish = Lerp( ptTop, ptBot, 0.5 );
            const R3Pt ptSquishEnd = Lerp( ptTopEnd, ptBotEnd, 0.5 );
            crvTop.SetPt(0) = Lerp( ptTop, ptSquish, dPinch ); // apt[iS];
            crvTop.SetPt(1) = Lerp( ptTopEnd, ptSquishEnd, dPinch ); //apt.wrap(iS+1);
            crvBot.SetPt(0) = Lerp( ptBot, ptSquish, dPinch ); // apt[iS];
            crvBot.SetPt(1) = Lerp( ptBotEnd, ptSquishEnd, dPinch ); //apt.wrap(iS+1);
            R3Vec vecBtwn = apt.wrap(iS+1) - apt[iS];
            const double dLenScale = ::Length( vecBtwn );
            vecBtwn = UnitSafe( vecBtwn );

            R3Vec vecRot0 = Cross( avecTang[iS], vecBtwn );
            R3Vec vecRot1 = Cross( -avecTang[iSNext], vecBtwn );
            if ( RNIsZero( ::Length( vecRot0 ) ) ) vecRot0 = m_pin.GetNormal();
            if ( RNIsZero( ::Length( vecRot1 ) ) ) vecRot1 = m_pin.GetNormal();
            const double dAng0 = dFromTop * DotToAngle( Dot( vecBtwn, avecTang[iS] ) );
            const double dAng1 = dFromTop * DotToAngle( Dot( vecBtwn, -avecTang[iSNext] ) );

            crvTop.SetVec(0) = ( R3Matrix::Rotation( vecRot0, dAng0 ) * vecBtwn ) * dLenScale;
            crvTop.SetVec(1) = ( R3Matrix::Rotation( vecRot1, dAng1 ) * vecBtwn ) * dLenScale;
            crvBot.SetVec(0) = ( R3Matrix::Rotation( vecRot0, dAng0 ) * vecBtwn ) * dLenScale;
            crvBot.SetVec(1) = ( R3Matrix::Rotation( vecRot1, dAng1 ) * vecBtwn ) * dLenScale;
            //crv.SetVec(0) = avecTang[iS] * dLenScale;
            //crv.SetVec(1) = avecTang.wrap(iS+1) * -dLenScale;
            //crv.SetVec(0) = vecBtwn * dLenScale;
            //crv.SetVec(1) = vecBtwn * dLenScale;
                        
            for ( int j = 0; j < aiAround[iS]; j++, iPtIndex++ ) {
                const double dPercAround = (double) j / (double) ( aiAround[iS] - 1.0 );
                const R3Vec vecNormAvg = UnitSafe( avecNorm[iS] * (1.0 - dPercAround) + avecNorm[iSNext] * dPercAround );
                const R3Vec vecNorm = UnitSafe( vecNormAvg * dFromTop + m_pin.GetNormal() * (1.0 - dFromTop ) );

                const R3Pt ptTop = crvTop( dPercAround );
                const R3Pt ptBot = crvBot( dPercAround );
                
                ASSERT( iPtIndex >= 0 && iPtIndex < m_aaptTopPolygon[iR].size() );
                m_aaptTopPolygon[iR][iPtIndex] = ptTop;// + vecNorm * dFlat;
                m_aavecTopPolygonNorm[iR][iPtIndex] = vecNorm;
                m_aaptBotPolygon[iR][iPtIndex] = ptBot;// - vecNorm * dFlat;
                m_aavecBotPolygonNorm[iR][iPtIndex] = vecNorm;
                
                for ( int i = 0; i < 4; i++ ) {
                    const double dBlend = acol[iS][i] * (1.0 - dPercAround) + acol.wrap(iS+1)[i] * dPercAround;
                    m_aafColWebbing[iR][iPtIndex][i] = dFromTop * dBlend + (1.0 - dFromTop) * m_aafColWebbing[0][0][i];
                }
            }
        }
        if ( iTotal != iPtIndex ) {
            cerr << "Warning, wrong number of points set\n";
        }

        if ( iR != 1 ) {
            vector< R4Pt_i > &af = m_aafPolygonFaces[iR-1];
            af.resize( 0 );
            int iTop = 0, iBottom = 0;
            for ( int iS = 0; iS < aiAround.num(); iS++ ) {
                int iIndexTop = 0, iIndexBot = 0;
                const R3Line_seg seg( apt[iS], apt.wrap(iS+1) ); // Use two points on bigger span
                
                // the two starting points
                const int iLoop = aiAround[iS] + aiAroundLast[iS];
                for ( int i = 0; i < iLoop; i++ ) {
                    bool bDoTop = true;
                    if ( iIndexTop == aiAroundLast[iS] ) {
                        bDoTop = false;
                    } else if ( iIndexBot != aiAround[iS] ) {
                        ASSERT( iTop >= 0 && iTop < m_aaptTopPolygon[iR-1].size() );
                        ASSERT( iBottom >= 0 && iBottom < m_aaptTopPolygon[iR].size() );
                        
                        const double dTTop = seg.Projected_dist_on_seg( m_aaptTopPolygon[iR-1][iTop] );
                        const double dTBot = seg.Projected_dist_on_seg( m_aaptTopPolygon[iR][iBottom] );    
                        if ( dTBot < dTTop ) {
                            bDoTop = false;
                        }
                    }

                    // Move one point along either top or bottom
                    if ( bDoTop ) {
                        af.push_back( R4Pt_i( 1, iBottom % m_aaptTopPolygon[iR].size(), (iTop+1) % m_aaptTopPolygon[iR-1].size(), iTop % m_aaptTopPolygon[iR-1].size() ) );
                        iTop++;
                        iIndexTop++;
                    } else {
                        af.push_back( R4Pt_i( 0, iTop % m_aaptTopPolygon[iR-1].size(), (iBottom+1) % m_aaptTopPolygon[iR].size(), iBottom % m_aaptTopPolygon[iR].size() ) );
                        iBottom++;
                        iIndexBot++;
                    }
                }
                ASSERT( iTop >= 0 && iTop <= m_aaptTopPolygon[iR-1].size() );
                ASSERT( iBottom >= 0 && iBottom <= m_aaptTopPolygon[iR].size() );
            }
            
            ASSERT( iTop == m_aaptTopPolygon[iR-1].size() );
            ASSERT( iBottom == m_aaptTopPolygon[iR].size() );
        }
        
        if ( iR == iMax - 1 ) {
            break;
        }
        
        const double dFromTopNext = (double) (iR+1) / (double) (iMax);
        for ( int iS = 0; iS < aiAround.num(); iS++ ) {
            adT[iS] = dFromTopNext * adTEnd[iS] + (1.0 - dFromTopNext) * adTStart[iS];
            const CurveRibbon &crv = m_pin.GetCurveGroup( m_aSpokeData[iS].m_iCurve ).GetCurveRibbon();
            apt[iS] = crv( adT[iS] );
        }
        // Save for making faces next round
        iTotalLast = iTotal;
        aiAroundLast = aiAround;
        
        iTotal = 0;
        for ( int iS = 0; iS < m_aSpokeData.size(); iS++ ) {
            adRing[iS] = ::Length( apt.wrap(iS+1) - apt[iS] );
            
            const int iAdd = (int) ( adRing[iS] / dStepSize - aiAround[iS] );
            if ( iAdd > 0 ) {
                aiAround[iS] += WINmin( iAdd, aiAroundLast[iS] );
            }
            iTotal += aiAround[iS];
        }
        
    }
}


int IntersectionDrawData::SpokeData::NumPts() const
{
    return m_bReverseDir ? m_iStart - m_iPin : m_iPin - m_iStart;
}

void IntersectionDrawData::SetSpoke( const int in_iS, const int in_iNSamplesWeb )
{
    static vector<double> s_adBlend;
    if ( s_adBlend.size() != s_iTubeDivs ) {
        const double dAround = 1.0 / (double) (s_iTubeDivs);
        s_adBlend.resize( s_iTubeDivs );
        for ( int i = 0; i < s_iTubeDivs; i++ ) {
            const double dTAround = (i+0.5) * dAround;
            if ( dTAround < 0.25 ) {
                s_adBlend[i] = dTAround * 4.0; 
            } else if (dTAround < 0.5 ) {
                s_adBlend[i] = 1.0 - (dTAround-0.25) * 4.0; 
            } else if (dTAround < 0.75 ) {
                s_adBlend[i] = (dTAround-0.5) * 4.0; 
            } else {
                s_adBlend[i] = 1.0 -(dTAround-0.75) * 4.0; 
            }                
            if ( s_bTraceSpokeData ) cout << s_adBlend[i] << " ";
        }
        if ( s_bTraceSpokeData ) cout << "\n";
    }
    
    vector< vector< R3Pt > > & aaptSpoke = m_aaaptSpokes[in_iS];
    vector< vector< R3Vec > > & aavecSpokeNorm = m_aaavecSpokesNorm[in_iS];
    vector< R4Pt_f > & acolSpoke = m_aafColSpoke[in_iS];
    static vector< double > adScaleLeftRight, adScaleUpDown;
    const SpokeData &sd = m_aSpokeData[in_iS];
    
    aaptSpoke.resize( s_iTubeDivs );
    aavecSpokeNorm.resize( s_iTubeDivs );
    
    const int iIncr = sd.m_bReverseDir ? -1 : 1;
    const int iNTube = sd.NumPts() - sd.NumWeb();
    const int iNPts = iNTube + in_iNSamplesWeb;
    for ( int i = 0; i < aaptSpoke.size(); i++ ) {
        aaptSpoke[i].resize( iNPts );
        aavecSpokeNorm[i].resize( iNPts );
    }
    acolSpoke.resize( iNPts );
    adScaleLeftRight.resize( iNPts );
    adScaleUpDown.resize( iNPts );
    
    int iIndex = 0;
    R3Matrix matRot, matRotBack;
    const CurveRibbon &crv = m_pin.GetCurveGroup( sd.m_iCurve ).GetCurveRibbon();
    const CurveDrawData &cdd = m_pin.GetCurveGroup( sd.m_iCurve ).GetCurveDrawData();
    const double dScaleAtPinLeftRight = m_dStripWidth * 0.1;
    const double dScaleAtPinUpDown = m_dStripWidth;
    for ( int i = sd.m_iStart; i != sd.m_iMid; i += iIncr, iIndex++ ) {
        const double dScaleMid = cdd.GetScaling( i % crv.NumPts() );
        if ( sd.m_bReverseDir == false ) {
            // Up to where webbing starts
            const double dPerc = (double) (i - sd.m_iStart) / (double) (sd.m_iMid - sd.m_iStart );
            const double dRatio = 1.0 - pow( 1.0 - dPerc, 2 );
            
            adScaleUpDown[iIndex] = adScaleLeftRight[iIndex] = (1.0 - dRatio) * dScaleMid + dRatio * sd.m_dRadius;
        } else {
            // Up to where webbing starts
            const double dPerc = (double) (i - sd.m_iStart) / (double) (sd.m_iMid - sd.m_iStart );
            const double dRatio = 1.0 - pow( 1.0 - dPerc, 2 );
            
            adScaleUpDown[iIndex] = adScaleLeftRight[iIndex] = (1.0 - dRatio) * dScaleMid + dRatio * sd.m_dRadius;
        }
    }
    // Webbing to pin
    for ( int i = 0; i < in_iNSamplesWeb; i++, iIndex++ ) {
        const double dPerc = (double) (i) / (double) (in_iNSamplesWeb);
        const double dRatio = pow( dPerc, 2 );
        
        adScaleLeftRight[iIndex] = (1.0 - dRatio) * sd.m_dRadius + dRatio * dScaleAtPinLeftRight;
        adScaleUpDown[iIndex] = (1.0 - dPerc) * sd.m_dRadius + dPerc * dScaleAtPinUpDown;
    }
    
    ASSERT( iIndex == iNPts );
    iIndex = 0;

    R3Pt pt;
    R3Vec vecTang, vecNorm;
    double dPercWeb = fabs( crv.PercAlongClamp( sd.m_iMid ) - crv.PercAlongClamp( sd.m_iPin ) );
    if ( crv.IsClosed() ) {
        dPercWeb = crv.PercAlongWrap( sd.m_iPin ) - crv.PercAlongWrap( sd.m_iMid );
        if ( sd.m_bReverseDir ) dPercWeb *= -1.0;
        if ( dPercWeb < 0.0 ) {
            cerr << "IntersectionDrawData::SetSpoke bad perc web " << dPercWeb << "\n";
        }
    }
    double dPercFlat = 0.0;
    for ( int i = 0; i < iNPts; i++ ) {
        if ( i < iNTube ) {
            pt = crv.PtClamp( sd.m_iStart + iIncr * i );
            vecNorm = crv.GetNormalAtPointClamp( sd.m_iStart + iIncr * i );
            vecTang = crv.GetTangentAtPointClamp( sd.m_iStart + iIncr * i );
            dPercFlat = 0.0;
        } else {
            const double dPercAlongWeb = (double) (i - iNTube) / (double) in_iNSamplesWeb;
            double dT = crv.PercAlongClamp( sd.m_iMid ) + iIncr * ( dPercWeb * dPercAlongWeb );
            pt = crv( dT );
            vecNorm = crv.Normal( dT );
            vecTang = crv.Tangent( dT );
            dPercFlat = pow( dPercAlongWeb, 2 );
        }
        if ( s_bTraceSpokeData ) {
            cout << pt << vecTang << vecNorm << "\n";
        }
        
        const R3Vec vecBiTang = UnitSafe( Cross( vecNorm, vecTang ) );
        R3Matrix::MatrixVecToVec(matRot, vecTang, vecNorm, R3Vec(0,0,1), R3Vec(0,1,0));
        matRotBack = matRot.Transpose();
        
        for ( int j = 0; j < s_iTubeDivs; j++ ) {
            const double dScale = (1.0 - s_adBlend[j]) * adScaleLeftRight[i] + s_adBlend[j] * adScaleUpDown[i];
            const double dFat = dScale * CurveDrawData::WebbingScaling( j );
            const R3Vec vecToNormY = matRot * vecNorm;
            const R3Vec vecOut = matRotBack * CurveDrawData::WebbingRotate( j ) * vecToNormY;
            const R3Pt ptTube = pt + vecOut * dFat;
            
            const double dAround = (double) j / (double) s_iTubeDivs;
            const R3Pt ptFlatTop = pt + vecNorm * dScaleAtPinUpDown + vecBiTang * ( adScaleLeftRight[i] * (dAround * 4.0 - 1.0) );
            const R3Pt ptFlatBot = pt - vecNorm * dScaleAtPinUpDown - vecBiTang * ( adScaleLeftRight[i] * (dAround * 4.0 - 3.0) );
            const R3Pt ptFlat = ( j < s_iTubeDivs / 2 ) ? ptFlatTop : ptFlatBot;
            aaptSpoke[j][i] = Lerp( ptTube, ptFlat, dPercFlat );
            
            const R3Vec vecRot = matRotBack * CurveDrawData::WebbingRotateNormal( j ) * vecToNormY;
            const R3Vec vecFlatNorm = ( j < s_iTubeDivs / 2 ) ? m_pin.GetNormal() : m_pin.GetNormal() * -1.0;
            aavecSpokeNorm[j][i] = UnitSafe( vecRot * (1.0 - dPercFlat ) + vecFlatNorm * (dPercFlat + 0.25) );
            if ( j >= s_iTubeDivs / 2 ) aavecSpokeNorm[j][i] *= -1.0;            
        }
        
    }
    
}

void IntersectionDrawData::SetSpokeRadius()
{
    Array<R3Pt> aptSpoke( m_aSpokeData.size() );
    for ( int iS = 0; iS < m_aSpokeData.size(); iS++ ) {
        const Curve &crv = m_pin.GetCurveGroup( m_aSpokeData[iS].m_iCurve ).CompositeCurve();
        aptSpoke[iS] = crv.PtClamp( m_aSpokeData[iS].m_iMid );
    }
    for ( int iS = 0; iS < m_aSpokeData.size(); iS++ ) {
        const double dLen1 = ::Length( aptSpoke[iS] - aptSpoke.wrap(iS-1) );
        const double dLen2 = ::Length( aptSpoke[iS] - aptSpoke.wrap(iS+1) );
        
        const Curve &crv = m_pin.GetCurveGroup( m_aSpokeData[iS].m_iCurve ).CompositeCurve();
        const CurveDrawData &cdd = m_pin.GetCurveGroup( m_aSpokeData[iS].m_iCurve ).GetCurveDrawData();
        const int iNPts = abs( m_aSpokeData[iS].m_iMid - m_aSpokeData[iS].m_iStart );
        const double dLenMidToSpoke = crv.AverageSegLength() * iNPts;
        if ( dLenMidToSpoke < 1.5 * m_dStripWidth ) {
            m_aSpokeData[iS].m_dRadius = 1.05 * cdd.GetScaling( m_aSpokeData[iS].m_iStart % crv.NumPts() );
        } else {
            m_aSpokeData[iS].m_dRadius = WINmin( m_dStripWidth, 0.3 * WINmin( dLen1, dLen2 ) );
        }
    }
}

// Assumes we're closed
void IntersectionDrawData::SpokeData::InsureOrdered( const int in_iNPts )
{
    if ( m_bReverseDir ) {
        if ( m_iPin >= in_iNPts ) {
            m_iPin -= in_iNPts;
        }
        
        if ( m_iMid < m_iPin ) {
            m_iMid += in_iNPts;
        }
        if ( m_iStart < m_iPin ) {
            m_iStart += in_iNPts;
        }
        if ( m_iStart < m_iMid ) {
            cerr << "ERR: IntersectionDrawData::SpokeData::InsureOrdered, not ordered rev " << m_iStart << " " << m_iMid << " " << m_iPin << " " << in_iNPts << "\n";
        }
    } else {
        if ( m_iStart >= in_iNPts ) {
            m_iStart -= in_iNPts;
        }
        
        if ( m_iPin < m_iStart ) {
            m_iPin += in_iNPts;
        }
        if ( m_iMid < m_iStart ) {
            m_iMid += in_iNPts;
        }
        if ( m_iMid > m_iPin ) {
            m_iMid -= in_iNPts;
        }
        if ( !( m_iStart <= m_iMid && m_iMid <= m_iPin ) ) {
            cerr << "ERR: IntersectionDrawData::SpokeData::InsureOrdered, not ordered " << m_iStart << " " << m_iMid << " " << m_iPin << " " << in_iNPts << "\n";
        }
    }
}

void IntersectionDrawData::SetSpokeData( )
{
    const int iNCrvs = m_pin.NumCurves();
    Array<int>  aiMap( 2 * iNCrvs );
    Array<double> adTheta( 2 * iNCrvs );
    Array< int > aiStart( 2 * iNCrvs ), aiMid( 2 * iNCrvs ), aiPin( 2 * iNCrvs );
    
    const R3Vec &vecNormal = m_pin.GetNormal();
    
    R3Matrix matRot;
    R3Matrix::MatrixVecToVec( matRot, vecNormal, R3Vec(0,0,1) );
    
    if ( s_bTraceSpokeData ) cout << "\nNCrvs " << iNCrvs << "\n";
    /// Figure out which segment we're actually in
    for ( int i = 0; i < iNCrvs; i++ ) {
        const CurveDrawData &cdd = m_pin.GetCurveGroup(i).GetCurveDrawData();
        const CurveRibbon &crv = m_pin.GetCurveGroup(i).GetCurveRibbon();
        const double dT = m_pin.GetTValue(i);
        aiPin[2*i] = aiPin[2*i+1] = crv.ClosestIndexWrap( dT );
        bool bFound = false;
        const int iStopLoop = crv.IsClosed() ? cdd.NumWebbingSplits() : cdd.NumWebbingSplits() - 1;
        if ( s_bTraceSpokeData ) cout << "Crv " << i << " T " << dT << " closed " << ( crv.IsClosed() ? " t " : " f ") << " " << crv.NumPts() << "\n";
        for ( int iS = 0; iS < iStopLoop; iS++ ) {
            const CurveDrawData::WebbingSplit &ws = cdd.GetWebbingSplit( iS );
            const CurveDrawData::WebbingSplit &wsNext = cdd.GetWebbingSplit( (iS+1) % cdd.NumWebbingSplits() );
            double dTStart = crv.PercAlongWrap( ws.End() - 1 );
            double dTEnd = crv.PercAlongWrap( wsNext.Start() );
            if ( crv.IsClosed() ) {
                if ( dTStart > dTEnd ) {
                    if ( RNIsZero( dTEnd ) && RNApproxEqual(dTStart, 1.0) ) {
                        dTStart = 0.0;
                        dTEnd = 1.0;
                    } else {
                        dTEnd += 1.0;
                    }
                } 
            }
            if ( s_bTraceSpokeData ) cout << " " << dTStart << ", " << wsNext.Start() << "  " << dTEnd << ", " << ws.End() << "\n";
            if ( (dTStart <= dT && dT <= dTEnd) ) {
                bFound = true;
            } else if ( crv.IsClosed() && dTStart <= dT + 1.0 && dT + 1.0 <= dTEnd ) {
                bFound = true;
            }
            if ( bFound == true ) {
                if ( s_bTraceSpokeData ) cout << " found\n";

                aiStart[2*i] = ws.Mid() - 1;
                if ( crv.IsClosed() ) {
                    if ( aiStart[2*i] < 0 ) aiStart[2*i] += crv.NumPts();
                } else if ( aiStart[2*i] < 0 ) aiStart[2*i] = 0;
                
                aiStart[2*i+1] = wsNext.Mid();
                aiMid[2*i] = ws.End();
                aiMid[2*i+1] = wsNext.Start();
                break;
            }                
        }

        if ( bFound == false ) {
            for ( int iS = 0; iS < cdd.NumWebbingSplits(); iS++ ) {
                cerr << cdd.GetWebbingSplit(iS).Start() << " " << cdd.GetWebbingSplit(iS).End() << "  ";
            }
            cerr << "\nt value not found " << dT << "\n";
            aiStart[2*i] = aiStart[2*i+1] = aiPin[2*i];
            aiMid[2*i] = aiMid[2*i+1] = aiPin[2*i];
        }
        /// Use tangent information for finding theta if we didn't manage to actually walk out at all
        const R3Vec vecTang = matRot * crv.Tangent( dT );
        R2Vec vecTang2D( vecTang[0], vecTang[1] );
        if ( RNIsZero( ::Length( vecTang2D ) ) ) {
            cerr << "WARNING: IntersectionDrawData::UpdateGeometry Non-flat spoke\n";
            vecTang2D = R2Vec(1,0);
        }
        
        adTheta[2*i] = atan2( vecTang2D[1], vecTang2D[0] );
        adTheta[2*i+1] = atan2( -vecTang2D[1], -vecTang2D[0] );
        aiMap[2*i] = 2*i;
        aiMap[2*i+1] = 2*i+1;
    }

    if ( s_bTrace ) {
        cout << "Thetas\n";
        for ( int i = 0; i < adTheta.num(); i++ ) {
            cout << adTheta[i] << " ";
        }
        cout << "\n";
    }
    aiMap.sort( adTheta );
    
    m_aSpokeData.resize( 2 * iNCrvs );
    for ( int i = 0; i < m_aSpokeData.size(); i++ ) {
        const int iIndex = aiMap[i];
        m_aSpokeData[i].m_iCurve = iIndex / 2;
        m_aSpokeData[i].m_iStart = aiStart[iIndex];
        m_aSpokeData[i].m_iMid = aiMid[iIndex];
        m_aSpokeData[i].m_iPin = aiPin[iIndex];
        m_aSpokeData[i].m_bReverseDir = iIndex % 2 ? true : false;
        m_aSpokeData[i].m_dTheta = adTheta[i];
        m_aSpokeData[i].m_dRadius = m_dStripWidth;
        m_aSpokeData[i].m_iCurve = iIndex / 2;
        m_aSpokeData[i].m_iCurve = iIndex / 2;
        const Curve &crv = m_pin.GetCurveGroup(iIndex/2).CompositeCurve();
        if ( crv.IsClosed() ) {
            m_aSpokeData[i].InsureOrdered( crv.NumPts() );
        }
        if ( s_bTrace ) {
            cout << "Crv " << m_aSpokeData[i].m_iCurve << " rev " << ( m_aSpokeData[i].m_bReverseDir ? " t " : " f " );
            cout << m_aSpokeData[i].m_iStart << " " << m_aSpokeData[i].m_iMid << " " << m_aSpokeData[i].m_iPin << "\n";
        }
    }
}    

void IntersectionDrawData::UpdateGeometry( )
{
    SetSpokeRadius();
    const pair<int,double> iSize = NumSamplesWeb();
    
    m_aaaptSpokes.resize( m_aSpokeData.size() );
    m_aaavecSpokesNorm.resize( m_aSpokeData.size() );
    m_aafColSpoke.resize( m_aSpokeData.size() );
    
    for ( int i = 0; i < m_aaaptSpokes.size(); i++ ) {
        SetSpoke( i, iSize.first );
    }
    SetWebbing( iSize );
}

