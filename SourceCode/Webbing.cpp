/*
 *  Webbing.cpp
 *  SketchCurves
 *
 *  Created by Cindy Grimm on 6/27/12.
 *  Copyright 2012 Washington University in St. Louis. All rights reserved.
 *
 */

#include "Webbing.h"
#include <utils/Polygonizer.H>
#include "UserInterface.h"

const CurveNetwork *Webbing::s_aopCrvs = NULL;
double Webbing::m_dFalloff = 0.1;
double Webbing::m_dSpacing = 0.1;
double s_dScl = 1.0;

static CRVHermite1 s_crv;

double Webbing::SrfFunc( const R3Pt &in_pt )
{
    double dSum = 0.0;
    R3Pt ptClosest;
    double dDist;
    for ( int i = 0; i < s_aopCrvs->NumCrvs(); i++ ) {
        const CurveRibbon &crvRibbon = s_aopCrvs->GetCurveRibbon(i);
        const double dT = crvRibbon.ClosestPointTValue(in_pt, dDist, ptClosest );
        
        const R3Vec vecToPt = UnitSafe( in_pt - ptClosest );
        const R3Vec vecNorm = UnitSafe( crvRibbon.Normal( dT ) );
        const double dDot = Dot( vecToPt, vecNorm );
        const double dFat = s_crv( ( dDot + 1.0 ) / 2.0 )[0];
        const double dWeight = exp( -dDist / m_dFalloff ) * s_dScl;
        
        if ( RNIsZero( dDist, 1e-6 ) ) {
            dSum = -100.0;
            break;
        }
        
        const double dDistLog = log( m_dSpacing / dDist ); // zero at crossing, negative inside
        dDist = dFat * dDist / m_dSpacing; // 1.0 is now m_dSpacing units out
        
        dSum += dDistLog * dWeight;
        //cout << dWeight << " " << dDistLog << "  ";
    }
    //cout << dSum << "\n";  
    return dSum;
}

void Webbing::BuildSurface( const CurveNetwork &in_crv )
{
    double dSpacing = 0.0;
    
    if ( s_crv.NumPts() == 0 ) {
        s_crv.Add( R1Pt(1.5), R1Vec(1.0) );
        s_crv.Add( R1Pt(1.0), R1Vec(1.0) );
        s_crv.Add( R1Pt(1.5), R1Vec(1.0) );
        s_crv.Add( R1Pt(0.5), R1Vec(1.0) );
        s_crv.Add( R1Pt(1.5), R1Vec(1.0) );
    }
    
    m_ptMin = R3Pt(1e30, 1e30, 1e30);
	m_ptMax = R3Pt( -1e30, -1e30, -1e30 );

    s_aopCrvs = &in_crv;
    int iCount = 0;
    for ( int i = 0; i < in_crv.NumCrvs(); i++ ) {
        const CurveRibbon &crvRibbon = in_crv.GetCurveRibbon(i);
        for ( int j = 0; j < crvRibbon.NumPts()-1; j++ ) {
            dSpacing += ::Length( crvRibbon.Pt(j+1) - crvRibbon.Pt(j) );
            iCount++;

            for ( int k = 0; k < 3; k++ ) {
                m_ptMin[k] = WINmin( m_ptMin[k], crvRibbon.Pt(j)[k] );
                m_ptMax[k] = WINmax( m_ptMax[k], crvRibbon.Pt(j)[k] );
            }
        }
    }
    
    if ( iCount != 0 ) {
        dSpacing /= (double) iCount;
    }
    
    m_dFalloff = 2.0 * pow( dSpacing * 15.0, 2.0 );
    m_dSpacing = 2.0 * dSpacing;
    cout << "Falloff " << m_dFalloff << "\n";
    s_dScl = 1.0 / exp( -(m_dSpacing / m_dFalloff) );
    for ( int i = 1; i < 30; i++ ) {
        cout << exp( -(i * dSpacing) / m_dFalloff ) * s_dScl << " ";
    }
    cout << "\n";
    
	const R3Pt ptMid = Lerp( m_ptMin, m_ptMax, 0.5 );
    R3Pt ptBest = ptMid;
	double dDist = 1e30;
    for ( int i = 0; i < in_crv.NumCrvs(); i++ ) {
        const CurveRibbon &crvRibbon = in_crv.GetCurveRibbon(i);
        for ( int j = 0; j < crvRibbon.NumPts(); j++ ) {
            const double dDistPt = Length( ptMid - crvRibbon.Pt(j));
            if ( dDistPt < dDist ) {
                dDist = dDistPt;
                ptBest = crvRibbon.Pt(j);
            }
        }
	}
	
	cout << "Done building points\n";
	
	s_srfData = this;
    
	m_aptSurface.resize(0);
	m_avecSurface.resize(0);
	m_afaceSurface.resize(0);
	
	const R3Vec vec = m_ptMax - m_ptMin;
	const double dWidth = WINmax( vec[0], WINmax( vec[1], vec[2] ) );
	const double dSize = 0.2 * g_drawState.m_opUI->m_iNumCubes->value() * dWidth;
	double dDistToBdry = 0.0;
	for ( int j = 0; j < 3; j++ ) {
		dDistToBdry = WINmax( ptBest[j] - m_ptMin[j], dDistToBdry );
		dDistToBdry = WINmax( m_ptMax[j] - ptBest[j], dDistToBdry );
	}
    
	const int iBounds = (int) ( dDistToBdry / (0.25 * dSize + 1e-6) );
	polygonize( Webbing::SrfFunc, dSize, iBounds, ptBest, TriProc, VertProc );
    
	FlipSurface();
    SmoothSurface();

	cout << "Done building surface " << iBounds
	<< " faces " << m_afaceSurface.size() << "\n";
    
}
