/*
 *  CurveOverlap.h
 *  SketchCurves
 *
 *  Created by Cindy Grimm on 1/20/11.
 *  Copyright 2011 Adobe Corp.. All rights reserved.
 *
 */

#ifndef _CURVE_OVERLAP_DEFS_H
#define _CURVE_OVERLAP_DEFS_H

#include <utils/Rn_Defs.H>
#include <vector>

#include "ScreenCurve.h"
class Curve;

/*
 * Handle the stroke-curve comparisons and blend/scratch detection
 */
class CurveOverlap {
public:
	// What type of join, if any. Any of the first
	// five options are no overlap/join; not really used
	// in any way, but they're triggered at different stages,
	//  like no valid geometry in the stroke.
	typedef enum {
		BAD_STROKE = -5,
		BAD_CURVE,
		BAD_JOIN,
		BAD_OVERLAP,
		BAD_OVERHANG,
		NO_OVERLAP,
		JOIN,  // Gap
		BLEND, // Scratch back and forth
		OVERSTROKE,
		OVERSTROKE_START, // Overlaps one end, no overhang
		MERGE, // Overlaps one end, overhangs
		CLOSES // Overlaps and overhangs both ends
	} OverlapType;
		
	// Keep track of what end we're working with.
	typedef enum {
		NO_ENDS = -1,
		START_CURVE,
		END_CURVE,
        BOTH_ENDS
	} CurveEndType;
    
    static void PrintCurveEnd( const CurveEndType in_end );
    
	/* Store all the overlap info. Calculated once for each stroke end.
	 * Stroke-centric, ie., in direction of stroke, not curve */
    class OverlapInfo {
    public:
        std::pair<double,double> m_dCrv, m_dStroke; // Overlap start and stop t values
        double m_dAvgDist, m_dMaxDist, m_dAvgAng, m_dMaxAng; // Scoring values (distance and angles of merge)
        double m_dStrokeLength, m_dCurveLength; // Amount of stroke that overlaps
        CurveEndType m_crvEnd, m_strokeEnd; // Stroke end is set by which end, Curve end is a guess
        bool m_bStrokeReversed; // Is this stroke reversed wrt the curve?
        
        void ZeroOut();
        void ForceOverlapped();
		
        bool IsOverlapped(  ) const; // Does the stroke merge with curve?
        bool IsMergeEnd(  ) const; // Do the orientations make sense? 
		
		double Score() const; // Just the merge part of the score
        
        OverlapInfo() { ZeroOut(); }
        ~OverlapInfo() {}
        
        void Print() const;
    };
	
	/* The vectors will be oriented the correct way. 
	 * If this stroke starts at the end of the curve then vecJoin goes from the curve to the stroke
	 * Otherwise, it goes from the stroke to the curve
	 * If the stroke is reversed wrt the curve, then vecStroke will also be reversed
	 */
    class JoinInfo {
    public:
        double m_dCrvT, m_dStrokeT; // Join start and stop t values
		R2Pt   m_ptCrv, m_ptStroke; // Points to join
		R2Vec  m_vecJoin, m_vecCrv, m_vecStroke; // Vector between and tangent vectors
        double m_dDist, m_dAngCrv, m_dAngStroke, m_dScore; // For scoring
        CurveEndType m_crvEnd, m_strokeEnd; // Ends to join 
        bool m_bStrokeReversed; // Is this stroke reversed wrt the curve?
        
        void ZeroOut();
		void SetScore(); // Set m_dScore from m_dAng* and m_dDist
		
        bool IsJoined( const bool in_bPrintReason = false ) const;
		bool IsBetter( const JoinInfo &in_info, const bool in_bPrintReason = false ) const; // For search
		
		double Score() const; // This is a global score for comparing to merges, not the local search score
        
        JoinInfo() { ZeroOut(); }
        ~JoinInfo() {}
        
        void Print() const;
    };
	
private:
	// How much debugging to turn on
	static bool s_bTraceJoin, s_bTraceJoinDetails, s_bTraceMerge, s_bTraceOverstroke, s_bTraceTree, s_bTraceTs, s_bTraceScore, s_bTraceBlend;
	
	// The returned overlap type. Set by DecisionTree*, FindBest*, and Try*
	// Also set to BAD* in geometry if there's a problem with the stroke/projected curve
	OverlapType m_overlapType;
	
	/* Data set in the SetGeometryData routine */
	bool m_bCurveClosed;                     // Is the input curve closed?
	ScreenCurve m_crvStroke, m_crvProjected; // Input stroke curve and projected 3D curve
	std::vector<double> m_adTsMap;           // Mapping from the 3D curve's t values to the projected curve's t values
	                                         // Used in Merge* to map join t values from the screen curve back to the 3D curve
										     // Also used to set m_adTsOnOriginalCurve below
	
	std::vector<double> m_adDistToCurve;     // The distance to the projected screen curve for each stroke point
	std::vector<double> m_adTsOnCurve, m_adTsOnOriginalCurve;   // T values of the stroke on the projected screen curve and the original 3D curve
	double m_dTMin, m_dTMax;                 // Start and stop t values on curve
	
	// Data for determining which stroke is a "better" match
	double m_dDepthProj, m_dDepthMin, m_dDepthMax; // Stats on z depth of projection
	double m_dRatioScreenLengthToCurveLength; // For the overlapped part of the curve
	
    /* Information on how the stroke meets the curve (if it does). Set in SetGeometryData routine
	 * OverlapInfo has the geometric information of the overlap (t values, is it reveresed, minimum distance)
	 * JoinInfo has the vector and t values that best joins the stroke to the curve
	 *   Note that for each of these the stroke could join to the start OR the end of the curve
	 *     For OverlapInfo, the join could also be somewhere in the middle of the curve (overstroke)
	 */
    OverlapInfo m_strokeEnd1, m_strokeEnd2;         // Geometric overlap information
    JoinInfo    m_strokeJoinEnd1, m_strokeJoinEnd2; // Geometric join information

    /* This information is used by StrokeHistory to actually join the curves together.
	 * This information is already reversed (ie, assumes stroke curve direction matches curve direction)
	 *    Set in SetCurveAndStrokeEnds 
	 * CurveEndType is passed to stroke history for use in the join and merge operations
	 *   Note that these assume that the stroke has been reversed according to m_bReverseStroke
	 *   Ie, if the stroke meets the curve at the end of the stroke, but the stroke is reversed, m_strokeEnd will be START_CURVE
	 * m_bReverseStroke is based on the orientation of the stroke relative to the curve
	 *   Already corrected for stroke being reversed
	 * Curve/Stroke Join t values (set in SetJoinTValues)
	 *   The curve values for the first join will always be less than the t values for the second join (overstroke*)
	 *   The stroke t values will be reversed if m_bReverseStroke is true
	 */
	CurveEndType m_strokeEnd, m_curveEnd;           // Which end of the stroke/curve
	bool m_bReverseStroke;                          // Do we need to reverse the stroke to get the t values to line up?
	std::pair<double, double> m_dTsStrokeFirstJoin; // Bracketing t values for first join
	std::pair<double, double> m_dTsCurveFirstJoin;  // Bracketing t values for first join
	std::pair<double, double> m_dTsStrokeSecondJoin; // Only needed if OVERSTROKE* or CLOSES (second join)
	std::pair<double, double> m_dTsCurveSecondJoin;  // Only needed if OVERSTROKE* or CLOSES (second join)
	
	/* Set up the geometric overlap data CurveOverlap_Geometry.cpp */
	// Helper function - the tangent is found by walking out a set amount of the screen distance
	// returns false if no valid tangent found ("curve" is a point)
	bool GetScreenTangent( const ScreenCurve &in_curve, const double in_dT, R2Vec &out_vecTang ) const;
	bool ProjectCurveToScreen( const Curve &in_curve );
	// Sets t and distance values
	bool ProjectStrokeOnScreenCurve( );
	// Set up CurveOverlap::OverlapInfo for each end (CurveOverlap_OverlapInfo.cpp)
    void SetOverlapData( OverlapInfo &out_oi, const CurveEndType in_strokeEnd, const double in_dClipHeight ) const;
	// Search to find the best vector/t values that join the ends. (CurveOverlap_JoinInfo.cpp)
	void FindBestJoin( JoinInfo &out_info,
					   const CurveEndType &in_curveEndType1, const CurveEndType &in_curveEndType2,
					   const bool in_bReverseSecondCrv ) const ;
	// Set up Join info. (CurveOverlap_JoinInfo.cpp)
    void SetJoinData( ) ;
	// Call all of the above. CurveOverlap_Geometry.cpp
	bool SetGeometryData( const Curve &in_curve );
	
	/* Decision making, CurveOverlap_DecisionTree.cpp */
	// Does the stroke overlap the curve at all?
	bool Overlaps() const;
	// Does the entire stroke overlap the curve?
	bool AllOfStrokeOverlapsCurve(  ) const;
	// Is the stroke the same size as the curve? (possible blend)
	bool StrokeCoversCurve(  ) const;
	// Stroke starts in middle, rather than at end of curve (possible Overstroke_start)
	bool StrokeInMiddle() const;
	// Is there a join?
	bool IsJoin( const bool in_bAllowBackwardJoin ) ;
	// Helper function - does the stroke overhang one or both of the ends of the curve?
    CurveEndType FindOverhangs() const;
    // Check for stroke crossing close-together curve ends
    bool CoversEndOfCurve() ;
	// Special case of above for closed curves/closing strokes
	bool Closes() ;
    
	// The major decision tree
	OverlapType DecisionTree( const bool in_bAllowBackwardJoin  ) ;
	// Called if the curve is closed
	OverlapType DecisionTreeClosedCurve( ) ;
	// Just look for the best merge possible (on a forced merge/join)
    CurveEndType FindBestMerge() const;

	/* Merge information - set what range of the stroke/curve to
	 * search over when joining them up. CurveOverlap_Merge.cpp
	 * Note: these assume the stroke is oriented and ends, overlap type known */
	// Helper function, does one end
	void SetJoinPoints( const CurveEndType in_strokeEnd, const bool in_bFirstJoin, const OverlapInfo &in_oi ) ;
	// helper function: sets m_bReversed and m_crvEnd and m_strokeEnd. Since there's only
	// a fixed number of combinations, it's sufficient to send the stroke end in. Whether or not
	// it's reversed will be found from the overlap or join info. Once those two things are known,
	// there's only one direction the curve end can be.
	void SetJoinInfo( const CurveEndType &in_strokeEnd, const bool in_bUseJoinOrOverlapInfo );
	// Figure out which overlap info to use to calculate the actual t values. 
	void SetJoinTValues( );
	
	// Convert 2D data back to 3D data using t values (CurveOverlap_TValues.cpp)
	// A single projected curve value
	double ConvertTValue( const double in_dT ) const;
	/// All of the t values on the projected curve
	void ConvertOriginalCurveTValues( );
	// Convert the t values for the merge/join regions
	void ConvertCurveJoinTValues( );
	/// For StrokeHistory - 
	double ConvertStrokeTValue( const Curve &in_crv3D, const double in_dT ) const;
	
	// Set depth values (m_dDepth*) (CurveOverlap.cpp)
	void SetDepthValues(const Curve &in_curve);
	
	// Helper function - does the overlap region fold over?
	bool IsFoldOver( const OverlapType & in_ot, const bool in_bReverseStroke, const CurveEndType &in_strokeEnd,
					 const std::pair<double,double> &in_dTsStrokeFirstJoin, 
					 const std::pair<double,double> &in_dTsStrokeSecondJoin,
					 const ScreenCurve &in_crvScreen ) const;
	
	/// Force the stroke end to be the given one (used in combine curves)
	void ForceStrokeEnd( const CurveEndType &in_endType ) ;

public:
	OverlapType GetOverlapType() const { return m_overlapType; }
	
	const ScreenCurve &ProjectedCurve() const { return m_crvProjected; }

	// These assume that the stroke direction is the same as the curve direction,
	// ie, the tangents align
	CurveEndType StrokeEnd() const { return m_strokeEnd; }
	CurveEndType CurveEnd() const { return m_curveEnd; }

	// For closed curve - do we have a join or a merge?
	bool IsJoinStartStroke() const;
	bool IsJoinEndStroke() const;
		
	/* These are used by CurveNetwork when the user over rides the last decision */
	void FindBestMergeOrJoin( const bool in_bAllowClosed );
	void FindBestOverstroke( );
	void ForceClosedCurve( const ScreenCurve &in_stroke );
	void FindBestClosed();
	void FindBestCombined( CurveOverlap &io_oi );

	/* T values of the 2D and 3D curves in the join regions */
	double MinTCurve() const { return m_dTMin; }
	double MaxTCurve() const { return m_dTMax; }
	// Used for mapping t values on the last 3D stroke to the actual full curve
	void MapTsToComposite( const Curve &in_crvComposite, const Curve &in_crvLast );
	// T value for that stroke point on the original 3D curve
	double TOnOriginalCurve( const int in_iPtOnStroke ) const ;
	// These are start and stop regions on the _promoted_ 3D stroke
	std::pair<double,double> StrokeMergeTsFirst( const Curve &in_crvStroke3D ) const;
	std::pair<double,double> StrokeMergeTsSecond( const Curve &in_crvStroke3D ) const;
	// These are t values for the merge regions on the original stroke/curve. 
	// Oriented correctly based on stroke orientation
	// First and Second are used for overstroke and closing, otherwise, just first
	const std::pair<double,double> &StrokeMergeTsFirst( ) const { return m_dTsStrokeFirstJoin; }
	const std::pair<double,double> &StrokeMergeTsSecond( ) const { return m_dTsStrokeSecondJoin; }
	const std::pair<double,double> &CurveMergeTsFirst() const { return m_dTsCurveFirstJoin; }
	const std::pair<double,double> &CurveMergeTsSecond() const { return m_dTsCurveSecondJoin; }

	/* Scoring stuff */
	double DepthMin() const { return m_dDepthMin; }
	double DepthMax() const { return m_dDepthMax; }
	double Score( ) const; // No z
	double Score( const double in_dZMin, const double in_dZMax ) const;
	// For deciding how much smoothing to do on the join 
	double DistanceApart( const CurveOverlap::CurveEndType in_endType ) const;
	// Project the curve and see if it's behind the draw plane
	bool   IsBehindDrawPlane( const Curve &in_crv ) const;
	
	// Basically, are the t values monotonically increasing/decreasing, or is there a fold over?
	bool IsFoldOver() const;
		
	/// See if this curve overlaps itself; if so, return the sections
	static std::vector<ScreenCurve> SplitAtFoldbacks( const ScreenCurve &in_crv2D ) ;
	
	// First t value of overstroke 
	double StartOverstroke() const { return m_bReverseStroke ? m_adTsOnCurve.back() : m_adTsOnCurve[0]; }
	// Is the stroke reversed?
	bool ReverseStroke() const { return m_bReverseStroke; }
	
	// Compare two overlaps
	bool IsBetterMatch( const CurveOverlap &in_oMatch, const double in_dZMin, const double in_dZMax ) const;
	
	// Normal starting point (called from constructor)
	// in_bAllowBackwardJoin is false if this is testing the stroke against the last stroke
	bool TryMatch( const Curve &in_curve, const ScreenCurve &in_stroke, const bool in_bAllowBackwardJoin = false );
	// Alternative starting point if explicitly looking for closed curve
	bool TryClosedCurve( const ScreenCurve &in_stroke, const bool in_bForced = false );
	
	CurveOverlap & operator=( const CurveOverlap & );

	CurveOverlap( const CurveOverlap &in_crvOverlap ) { *this = in_crvOverlap; }
	CurveOverlap( const Curve &in_curve, const ScreenCurve &in_stroke, const bool in_bAllowBackwardJoin = false ) { TryMatch( in_curve, in_stroke, in_bAllowBackwardJoin ); }
	CurveOverlap( ) : m_overlapType(NO_OVERLAP) { }
	
	~CurveOverlap() {}
	
	void PrintState() const;
};
	
#endif