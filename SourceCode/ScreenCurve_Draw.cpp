/*
 *  ScreenCurve_Draw.cpp
 *  SketchCurves
 *
 *  Created by Cindy Grimm on 5/19/11 (copied from ScreenCurve.cpp).
 *  Copyright 2011 Adobe. All rights reserved.
 *
 */

#include "ScreenCurve.h"
#include <OpenGL/OGLObjs_Camera.H>
#include <OpenGL/OGLDraw2D.H>
#include "DrawState.h"

#include <FL/GL.H>
#include <utils/Utils_Color.H>

/*
 * Used primarily for feedback for the user so they
 * see what they're drawing
 */
void ScreenCurve::Draw() const
{
    if ( m_apt.num() < 2 )
        return;
	
	const double dScl = g_drawState.ScreenStripeWidth();
	const double dTaperWidth = 2.0 * g_drawState.ScreenStripeWidth();
	
	OGLBegin2D( g_drawState.GetCamera().Width(), g_drawState.GetCamera().Height() );
	g_drawState.StartShader( DrawState::SCREENSPACE_STROKE );
	glDisable( GL_DEPTH_TEST );	
	
    glBegin( GL_QUAD_STRIP );
    glColor4f( 0.0f, 0.0f, 0.0f, 1.0f );
	
	const int iNPts = m_apt.num();
    for ( int i = 0; i < m_apt.num(); i++  ) {    
		R2Vec vecTang = m_apt[ WINmin( iNPts - 1, i+1 ) ] - m_apt[ WINminmax( i-1, iNPts - 2, 0 ) ];
		if ( RNIsZero( ::Length( vecTang ) ) ) {
			vecTang = R2Vec(1,0);
		} else {
			vecTang.Normalize();
		}
		
		const R2Vec vecUp = R2Vec( -vecTang[1], vecTang[0] ) * dScl;
		
		double dTaper = 1.0;
		if ( DistAlong(i) < dTaperWidth ) {
			dTaper = DistAlong(i) / dTaperWidth;
		} else if ( Length() - DistAlong(i) < dTaperWidth ) {
			dTaper = (Length() - DistAlong(i)) / dTaperWidth;
		}
		
		glTexCoord2f( DistAlong(i), 1.0f );    
		glVertex2dv( &(m_apt[i] + vecUp * dTaper)[0] );
		
		glTexCoord2f( DistAlong(i), 0.0f );    
		glVertex2dv( &(m_apt[i] - vecUp * dTaper)[0] );
    }
	
	glEnd();
	
	g_drawState.EndShader();
	
	OGLEnd2D();
}
