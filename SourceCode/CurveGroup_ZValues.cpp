/*
 *  CurveGroup_ZValues.cpp
 *  SketchCurves
 *
 *  Created by Cindy Grimm on 3/16/11.
 *  Copyright 2011 Adobe Corp.. All rights reserved.
 *
 */


/*
 *  CurveGroup_AddStroke.cpp
 *  SketchCurves
 *
 *  Created by Cindy Grimm on 1/20/11.
 *  Copyright 2011 Adobe Corp. All rights reserved.
 *
 */


#include "CurveGroup.h"
#include "CurveOverlap.h"
#include "ScreenCurve.h"
#include "DrawState.h"
#include "ParamFile.h"
#include "UserInterface.h"
#include <OpenGL/OGLObjs_Camera.H>

const bool s_bTraceZValues = false;
const bool s_bTraceDecisions = false;

/* Look to see if this composite curve is planar.
 * If it is, look to see if the current drawing plane is similar enough to the planar curve
 * If so, consider snapping to this plane
 * Returns the best-fit plane of the composite curve and true if similar to the current draw plane
 */
bool CurveGroup::BestPlaneForPlanarCurve( const Curve &in_crvComposite, R3Plane &out_plane ) const
{
	R3Vec vecLine, vecNormal;
	const bool bIsPlanar = in_crvComposite.IsPlanar(vecLine, vecNormal );
	
	if ( bIsPlanar == false ) {
		return false;
	}

	out_plane = R3Plane( in_crvComposite(0.5), vecNormal );
	if ( !g_drawState.m_opUI->m_bShowDrawingPlane->value() ) {
		const double dAng = DotToAngle( fabs( Dot( vecNormal, g_drawState.GetCamera().Look() ) ) );
		
		if ( dAng < M_PI / 8.0 ) {
			return true;
		}
		return false;
	}
	
	const R3Plane &planeCur = g_drawState.DrawingPlane();
	
	const double dAng = DotToAngle( fabs( Dot( vecNormal, planeCur.Normal() ) ) );
	
	if ( dAng < M_PI / 8.0 ) {
		return true;
	}
	return false;
}

/* 
 * Use the plane to add the z values.
 * Just takes a ray through the plane
 */
Curve CurveGroup::AddZValue( const ScreenCurve &in_crvScreen, const R3Plane &in_plane ) const
{
	Curve crv;
	
	if ( s_bTraceZValues ) cout << "Z values plane ";

	const OGLObjsCamera &cam = g_drawState.GetCamera();
	for ( int i = 0; i < in_crvScreen.NumPts(); i++ ) {
		const R3Ray ray( cam.From(), cam.RayFromEye( in_crvScreen.Pt(i) ) );
		const R3Pt ptPlane = in_plane.IntersectRay( ray );
		crv.AddPoint( ptPlane );
	}
	if ( crv.NumPts() != in_crvScreen.NumPts() ) {
		cerr << "ERR: CurveGroup Add Z value, number of points not same!\n";
	}
	
	crv.SetAllData();
	return crv;
}

/*
 * Build a curve from the t values on the lines. The lines are the
 * rays from the eye through the screen points. The distances are how
 * far along the line to place the point.
 *
 * Filter a lot, then create the curve.
 * Note, only filtering in depth, so screen-space projection should be the same
 */
Curve CurveGroup::AddZValue( const Array<R3Line> &in_aline, const Array<double> &in_adDist ) const
{
	static Array<double> adDist, adDistTemp;
	adDist = in_adDist;
	
	if ( s_bTraceZValues ) cout << "Smoothing z values: ";
	const int iLoopSmooth = 2 + (int) (g_paramFile.GetInteger("SmoothingMaximumLoops") * g_drawState.m_opUI->m_dSmoothing->value() );
	for ( int iLoop = 0; iLoop < iLoopSmooth; iLoop++ ) {
		adDistTemp = adDist;
		double dAvgDiff = 0.0;
		/* Don't interpolate the depth values directly, but rather, take
		 * the points that you would get, average them, then project them
		 * back onto the lines */
		for ( int i = 1; i < adDist.num() - 1; i++ ) {
			const R3Pt ptAvgNeigh = Lerp( in_aline[i]( adDistTemp[i-1] ), in_aline[i]( adDistTemp[i+1] ), 0.5 );
			const R3Pt ptMe = in_aline[i]( adDistTemp[i] );
			const R3Pt ptAvg = Lerp( ptMe, ptAvgNeigh, 0.25 );
			dAvgDiff += ::Length( ptMe - ptAvg );
			adDist[i] = in_aline[i].Projected_dist_on_line( ptAvg );
		}
		if ( s_bTraceZValues ) cout << dAvgDiff / (double) adDist.num() << " ";
	}
	if ( s_bTraceZValues ) cout << "\n";
	
	Curve crv;
	for ( int i = 0; i < in_aline.num(); i++ ) {
		crv.AddPoint( in_aline[i]( adDist[i] ) );
	}
	crv.SetAllData();
	return crv;
}


/*
 * Simplest case - just project this screen curve onto the current drawing plane
 * One caveat: Don't use the draw plane if it's at too much of an angle
 */
Curve CurveGroup::AddZValue( const ScreenCurve &in_crvScreen ) const
{
	// Note: This function returns the draw plane if it is visible, otherwise, the view plane
	R3Plane plane = g_drawState.DrawingPlane();
	
	const double dAng = DotToAngle( fabs( Dot( plane.Normal(), g_drawState.GetCamera().Look() ) ) );
	
	// If the drawing plane is at an extreme angle wrt the view, may want to use the view normal
	if ( dAng > 0.75 * M_PI / 2.0 ) {
		const ShadowBox &sb = g_drawState.GetShadowBox();
		const R3Pt ptLL = sb.GetDrawingPlaneCenter() - sb.GetDrawingPlaneXVec() - sb.GetDrawingPlaneYVec();
		const R3Pt ptUR = sb.GetDrawingPlaneCenter() + sb.GetDrawingPlaneXVec() + sb.GetDrawingPlaneYVec();
		const R2Pt ptEdgeLL = g_drawState.GetCamera().CameraPt( ptLL );
		const R2Pt ptEdgeUR = g_drawState.GetCamera().CameraPt( ptUR );
		const double dXLeft = WINmin( ptEdgeLL[0], ptEdgeUR[0] );
		const double dXRight = WINmax( ptEdgeLL[0], ptEdgeUR[0] );
		const double dYBot = WINmin( ptEdgeLL[1], ptEdgeUR[1] );
		const double dYTop = WINmax( ptEdgeLL[1], ptEdgeUR[1] );
		int iOutside = 0;
		double dOutside = 0.0;
		for ( int i = 0; i < in_crvScreen.NumPts(); i++ ) {
			const R2Pt &pt = in_crvScreen.Pt(i);
			if ( pt[0] < dXLeft || pt[0] > dXRight || pt[1] < dYBot || pt[1] > dYTop ) {
				iOutside++;
				dOutside = WINmax( WINmax( dXLeft - pt[0], pt[0] - dXRight ), dOutside );
				dOutside = WINmax( WINmax( dYBot - pt[1], pt[1] - dYTop ), dOutside );
			}
		}
		
		if ( dAng > 0.85 * M_PI / 2.0 && iOutside > 0 ) {
			if ( s_bTraceZValues ) cout << "Add Z Value new curve1: using view plane " << iOutside << " " << dOutside << " " << dAng << "\n";
			
			plane = R3Plane( g_drawState.GetCamera().At(), g_drawState.GetCamera().Look() );
		} else if ( iOutside > 0.1 * in_crvScreen.NumPts() || dOutside > 0.1 || dAng > 0.95 * M_PI / 2.0 ) {
			if ( s_bTraceZValues ) cout << "Add Z Value new curve2: using view plane " << iOutside << " " << dOutside << " " << dAng << "\n";
			
			plane = R3Plane( g_drawState.GetCamera().At(), g_drawState.GetCamera().Look() );
		}
	} 
	
	return AddZValue( in_crvScreen, plane );
}

/*
 * Add z value to the screen curve
 *
 * We know this curve overlaps the original, so the undefined z values can
 * be gotten from the original curve.
 *
 * If the projection of the screen curve onto the composite curve folds back on itself
 * then interpolate the z values from the two join points. Otherwise, use the actual projected
 * closest points
 *
 * If the original curve happens to be planar, and the plane is roughly perpendicular to the
 * viewer, simply use the plane. This keeps everything planar.
 *
 * If not, then get depth values from the existing composite curve.
 *   1) Find ptC on the composite curve that corresponds to the screen curve point (gotten from the overlap class)
 *   2) Project that point onto a line from the eye through the screen point
 *   3) Record the distance and the line
 *
 * Note on t values: If the projection of the stroke onto the curve does not fold, use that t value. Otherwise,
 * Use arc-length parameterization
 */
Curve CurveGroup::AddZValueOversketch( const CurveOverlap &in_crvOverlap, const Curve &in_crvComposite, const ScreenCurve &in_crvScreen ) const
{
	if ( s_bTraceZValues ) cout << "Z values oversketch ";
	
	cout.precision(3);
	R3Plane planeBest;
	if ( BestPlaneForPlanarCurve( in_crvComposite, planeBest ) ) {
		if ( s_bTraceZValues ) cout << " Using best-fit plane\n";
		return AddZValue( in_crvScreen, planeBest );
	}
	if ( s_bTraceZValues ) cout << " Using two views\n";
	
	const OGLObjsCamera &cam = g_drawState.GetCamera();
	
	/* Easy fix didn't work - now do hard one */

	Array<R3Line> aline( in_crvScreen.NumPts() );
	Array<double> adDist( in_crvScreen.NumPts() );

	const bool bIsFolded = in_crvOverlap.IsFoldOver();
	if ( s_bTraceZValues && bIsFolded ) cout << "Is folded, using linear interp \n";
	const double dCrvTStart = in_crvOverlap.TOnOriginalCurve(0);
	const double dCrvTWidth = WINmax( 1e-6, in_crvOverlap.TOnOriginalCurve( in_crvScreen.NumPts() - 1) - dCrvTStart );
	
	for ( int i = 0; i < in_crvScreen.NumPts(); i++ ) {
		aline[i] = R3Line( cam.From(), UnitSafe( cam.RayFromEye( in_crvScreen.Pt(i) ) ) );
		 
		const double dT = bIsFolded ? (dCrvTStart + in_crvScreen.PercAlong(i) * dCrvTWidth) : in_crvOverlap.TOnOriginalCurve(i);
		const R3Pt ptCrv = in_crvComposite( dT );
		
		if ( bIsFolded ) {
			const R3Line lineThroughCrv( cam.From(), UnitSafe( ptCrv - cam.From() ) );
			adDist[i] = lineThroughCrv.Projected_dist_on_line( ptCrv );
		} else {
			adDist[i] = aline[i].Projected_dist_on_line( ptCrv );
		}
		if ( s_bTraceZValues ) {
			cout << adDist[i] << "  ";
			cout << ptCrv << "   " << ::Length( ptCrv - aline[i](adDist[i]) ) << "\n";
		}
	}
	if ( s_bTraceZValues ) cout << "\n";
	
	cout.precision(6);
	
	return AddZValue( aline, adDist );
}


/*
 * Add z value to the screen curve
 *
 * If the curve we're editing is planar and the view plane is roughly the same as that plane,
 * Just project the screen curve to that plane
 *
 * If not, for the merge area where the stroke overlaps the curve, do exactly as teh
 * same for overstroke above.
 *
 * If the drawing plane is not visible, do an extension. Basically, look at the change in 
 * depth values for the end of the overlap region and just keep incrementing in that amount.
 *
 * If the drawing plane is visible, 
 */
Curve CurveGroup::AddZValueExtend( const CurveOverlap &in_crvOverlap, const Curve &in_crvComposite, const ScreenCurve &in_crvScreen ) const
{
	cout.precision(3);
	const OGLObjsCamera &cam = g_drawState.GetCamera();
	
	R3Plane planeBest;
	if ( BestPlaneForPlanarCurve( in_crvComposite, planeBest ) ) {
		if ( s_bTraceZValues ) cout << " Using best-fit plane\n";
		return AddZValue( in_crvScreen, planeBest );
	}
	if ( s_bTraceZValues ) cout << " Using extend\n";
	
	Array<R3Line> aline( in_crvScreen.NumPts() );
	Array<double> adDist( in_crvScreen.NumPts() );
	adDist.fill(0.0);

	/* First do part of stroke that overlaps curve */
	int iStartExtrapolate = 0;
	int iEndExtrapolate = in_crvScreen.NumPts();
	R3Pt ptLast = ( in_crvOverlap.CurveEnd() == CurveOverlap::START_CURVE ) ? in_crvComposite(0.0) : in_crvComposite(1.0);
	if ( in_crvOverlap.GetOverlapType() == CurveOverlap::MERGE ) {
		const bool bIsFolded = in_crvOverlap.IsFoldOver();
		if ( s_bTraceZValues && bIsFolded ) cout << "Is folded, using linear interp \n";
		
		if ( in_crvOverlap.StrokeEnd() == CurveOverlap::START_CURVE ) {
			for ( int i = 0; i < in_crvScreen.NumPts(); i++ ) {
				if ( in_crvOverlap.TOnOriginalCurve(i) < 1e-6 || in_crvOverlap.TOnOriginalCurve(i) > 1.0 - 1e-6 ) {
					break;
				}
				iStartExtrapolate = i;
			}
		} else {
			for ( int i = in_crvScreen.NumPts() - 1; i >= 0; i-- ) {
				if ( in_crvOverlap.TOnOriginalCurve(i) < 1e-6 || in_crvOverlap.TOnOriginalCurve(i) > 1.0 - 1e-6 ) {
					break;
				}
				iEndExtrapolate = i;
			}
		}
		const double dCrvTStart = ( in_crvOverlap.CurveEnd() == CurveOverlap::START_CURVE ) ? 0.0 : in_crvOverlap.TOnOriginalCurve(0);
		const double dCrvTEnd = ( in_crvOverlap.CurveEnd() == CurveOverlap::START_CURVE ) ? in_crvOverlap.TOnOriginalCurve(0) : 1.0;
		const double dCrvTWidth = WINmax( 1e-6, dCrvTEnd - dCrvTStart );

		// Just do all of them - will over ride in extrapolate code
		for ( int i = 0; i < in_crvScreen.NumPts(); i++ ) {
			aline[i] = R3Line( cam.From(), UnitSafe( cam.RayFromEye( in_crvScreen.Pt(i) ) ) );
			
			const double dT = bIsFolded ? (dCrvTStart + in_crvScreen.PercAlong(i) * dCrvTWidth) : in_crvOverlap.TOnOriginalCurve(i);
			const R3Pt ptCrv = in_crvComposite( dT );
			
			if ( bIsFolded ) {
				const R3Line lineThroughCrv( cam.From(), UnitSafe( ptCrv - cam.From() ) );
				adDist[i] = lineThroughCrv.Projected_dist_on_line( ptCrv );
			} else {
				adDist[i] = aline[i].Projected_dist_on_line( ptCrv );
			}
		}
		if ( in_crvOverlap.StrokeEnd() == CurveOverlap::START_CURVE ) {
			ptLast = aline[iStartExtrapolate]( adDist[iStartExtrapolate] );
		} else {
			ptLast = aline.clamp(iEndExtrapolate)( adDist.clamp(iEndExtrapolate) );
		}
	} else {
		for ( int i = 0; i < in_crvScreen.NumPts(); i++ ) {
			aline[i] = R3Line( cam.From(), UnitSafe( cam.RayFromEye( in_crvScreen.Pt(i) ) ) );
		}
	}

	const int iStartLoop = ( in_crvOverlap.StrokeEnd() == CurveOverlap::START_CURVE ) ? iStartExtrapolate : 0;
	const int iEndLoop = ( in_crvOverlap.StrokeEnd() == CurveOverlap::START_CURVE ) ? in_crvScreen.NumPts() : iEndExtrapolate;
	
	// Already have lines, just need to set distances
	if ( g_drawState.ProjectToPlane() ) {
		if ( s_bTraceZValues ) cout << "Project to plane\n";
		// TODO: As in the curve creation case, should check to see if the stroke actually projects to the drawing plane
		const R3Plane planeProject( ptLast, g_drawState.DrawingPlane().Normal() );
		
		R3Pt ptOut;
		for ( int i = iStartLoop; i < iEndLoop; i++ ) {
			planeProject.Intersect( aline[i], adDist[i], ptOut );
		}
	} else {
		if ( s_bTraceZValues ) cout << "Walk along end\n";
		
		const R3Line &line = ( in_crvOverlap.StrokeEnd() == CurveOverlap::START_CURVE ) ? aline[iStartExtrapolate] : aline.clamp(iEndExtrapolate);
		const double dTCrv = g_drawState.SelectionSize() / in_crvComposite.Length();
		double dTDeltaDist = 0.0, dLen = g_drawState.SelectionSize();
		if ( in_crvOverlap.CurveEnd() == CurveOverlap::START_CURVE ) {
			const R3Pt ptE1 = in_crvComposite( 0.0 );
			const R3Pt ptE2 = in_crvComposite( dTCrv );
			dTDeltaDist = line.Projected_dist_on_line( ptE1 ) - line.Projected_dist_on_line( ptE2 );
			dLen = ::Length( cam.CameraPt( ptE2 ) - cam.CameraPt(ptE1) );
		} else {
			const R3Pt ptE1 = in_crvComposite( 1.0 - dTCrv );
			const R3Pt ptE2 = in_crvComposite( 1.0 );
			dTDeltaDist = line.Projected_dist_on_line( ptE1 ) - line.Projected_dist_on_line( ptE2 );
			dLen = ::Length( cam.CameraPt( ptE2 ) - cam.CameraPt(ptE1) );
		}
		
		if ( in_crvOverlap.StrokeEnd() == CurveOverlap::START_CURVE ) {
			const double dDistAtEnd = line.Projected_dist_on_line( in_crvComposite(1.0) );
			for ( int i = iStartLoop; i < iEndLoop; i++ ) {
				const double dLenTraveled = in_crvScreen.DistAlong( i ) - in_crvScreen.DistAlong( iStartLoop );
				adDist[i] = dDistAtEnd - dTDeltaDist * dLenTraveled / dLen;
			}
		} else {
			const double dDistAtEnd = line.Projected_dist_on_line( in_crvComposite(0.0) );
			for ( int i = iEndLoop - 1; i >= iStartLoop; i-- ) {
				const double dLenTraveled = in_crvScreen.DistAlong( iEndLoop ) - in_crvScreen.DistAlong( i );
				adDist[i] = dDistAtEnd + dTDeltaDist * dLenTraveled / dLen;
			}
		}			
	}
	
	if ( s_bTraceZValues ) {
		cout << iStartLoop << " " << iEndLoop << "\n";
		for ( int i = 0; i < adDist.num(); i++ ) {
			cout << i << " " << adDist[i] << "\n";
		} 
		cout << "\n";
	}
	
	cout << adDist.clamp( iStartExtrapolate - 1 ) << " " << adDist.clamp( iStartExtrapolate ) << " " << adDist.clamp( iStartExtrapolate + 1 ) << "\n";
	cout.precision(6);
	
	return AddZValue( aline, adDist );
}

/*
 * Basically a combination of the oversketch and the merge. Interpolate depth values in the area
 * between the end points. Basic idea is to fit a hermite curve to the two curve end points
 * then use that as a proxy for projection.
 *
 */
Curve CurveGroup::AddZValueClosed( const CurveOverlap &in_crvOverlap, const Curve &in_crvComposite, const ScreenCurve &in_crvScreen ) const
{
	cout.precision(3);
	
	R3Plane planeBest;
	if ( BestPlaneForPlanarCurve( in_crvComposite, planeBest ) ) {
		if ( s_bTraceZValues ) cout << " Using best-fit plane\n";
		return AddZValue( in_crvScreen, planeBest );
	}
	const OGLObjsCamera &cam = g_drawState.GetCamera();
	CRVHermite3 crvApproxZ(2);
	
	const double dTStartStroke = 0.5 * ( in_crvOverlap.StrokeMergeTsFirst().first + in_crvOverlap.StrokeMergeTsFirst().second );
	const double dTEndStroke = 0.5 * ( in_crvOverlap.StrokeMergeTsSecond().first + in_crvOverlap.StrokeMergeTsSecond().second );
	const int iStartStroke = in_crvScreen.Floor( dTStartStroke );
	const int iEndStroke = in_crvScreen.Ceil( dTEndStroke );

	const double dTStart = in_crvOverlap.TOnOriginalCurve( iStartStroke );
	const double dTEnd = in_crvOverlap.TOnOriginalCurve( iEndStroke );
	
	crvApproxZ.SetPt(0) = in_crvComposite( dTStart );
	crvApproxZ.SetPt(1) = in_crvComposite( dTEnd );
	const R3Vec vecD1 = in_crvComposite.Tangent( dTStart );
	const R3Vec vecD2 = in_crvComposite.Tangent( dTEnd );
	
	const double dLen = ::Length( crvApproxZ.GetPt(1) - crvApproxZ.GetPt(0) );
	crvApproxZ.SetVec(0) = vecD1 * (dLen * 1.3);
	crvApproxZ.SetVec(1) = vecD2 * (dLen * 1.3);
	
	/* Easy fix didn't work - now do hard one */
	Array<R3Line> aline( in_crvScreen.NumPts() );
	Array<double> adDist( in_crvScreen.NumPts() );
	
	//const bool bIsFolded = in_crvOverlap.IsFoldOver();
	//if ( s_bTraceZValues && bIsFolded ) cout << "Is folded, using linear interp \n";
	
	for ( int i = 0; i < in_crvScreen.NumPts(); i++ ) {
		aline[i] = R3Line( cam.From(), UnitSafe( cam.RayFromEye( in_crvScreen.Pt(i) ) ) );
		
		const double dT = in_crvOverlap.TOnOriginalCurve(i);
		if ( i < iStartStroke || i > iEndStroke || iStartStroke == iEndStroke ) { 
			const R3Pt ptCrv = in_crvComposite( dT );
			adDist[i] = aline[i].Projected_dist_on_line( ptCrv );
			
		} else {
			const double dTAlong = ( in_crvScreen.PercAlong(i) - in_crvScreen.PercAlong(iStartStroke) ) / (in_crvScreen.PercAlong(iEndStroke) - in_crvScreen.PercAlong(iStartStroke));
			ASSERT( dTAlong >= 0.0 && dTAlong <= 1.0 );
			cout << dTAlong << " ";
			const R3Pt ptCrv = crvApproxZ( dTAlong );
			adDist[i] = aline[i].Projected_dist_on_line( ptCrv );
		}
	}
	if ( s_bTraceZValues ) cout << "\n";
	
	cout.precision(6);
	
	return AddZValue( aline, adDist );
}

/* Interpolate the depth from one curve to the other.
 * The only really tricky part is getting the t values in the right order
 */
Curve CurveGroup::AddZValueCombine( const CurveOverlap &in_crvOverlapMe, const Curve &in_crvMe, 
								    const CurveOverlap &in_crvOverlapOther, const Curve &in_crvOther,
									const ScreenCurve &in_crvScreen ) const
{
	cout.precision(3);
	
	R3Plane planeBest1, planeBest2;
	const bool bPlane1 = BestPlaneForPlanarCurve( in_crvMe, planeBest1 );
	const bool bPlane2 = BestPlaneForPlanarCurve( in_crvOther, planeBest2 );
	const bool bSameNormal = fabs( Dot( planeBest1.Normal(), planeBest2.Normal() ) ) > 0.99;
	const R3Pt ptCrvOther = in_crvOther.CenterPoint();
	const double dClose = 0.01 * in_crvMe.Length();
	const bool bOnPlane = planeBest1.DistToPlane(ptCrvOther) < dClose;
	if ( bPlane1 && bPlane2 && bSameNormal && bOnPlane ) {
		if ( s_bTraceZValues ) cout << " Using best-fit plane\n";
		return AddZValue( in_crvScreen, planeBest1 );
	}
	
	const OGLObjsCamera &cam = g_drawState.GetCamera();
	CRVHermite3 crvApproxZ(2);
	
	/* Do ts based on current orientation of stroke */
	double dTE1 = 0.5 * ( in_crvOverlapMe.StrokeMergeTsFirst().first + in_crvOverlapMe.StrokeMergeTsFirst().second );
	double dTE2 = 0.5 * ( in_crvOverlapOther.StrokeMergeTsFirst().first + in_crvOverlapOther.StrokeMergeTsFirst().second );
	if ( in_crvOverlapMe.ReverseStroke() ) {
		dTE1 = 1.0 - dTE1;
	}
	if ( in_crvOverlapOther.ReverseStroke() ) {
		dTE2 = 1.0 - dTE2;
	}
		
	const int iStrokeE1 = in_crvScreen.Floor( dTE1 );
	const int iStrokeE2 = in_crvScreen.Ceil( dTE2 );

	int iEndMe = 0;
	if ( in_crvOverlapMe.StrokeEnd() == CurveOverlap::END_CURVE && !in_crvOverlapMe.ReverseStroke() ) {
		iEndMe = 1;
	}
	if ( in_crvOverlapMe.StrokeEnd() == CurveOverlap::START_CURVE && in_crvOverlapMe.ReverseStroke() ) {
		iEndMe = 1;
	}
	const int iEndOther = (iEndMe == 0) ? 1 : 0;
	
	if ( in_crvOverlapMe.ReverseStroke() ) {		
		crvApproxZ.SetPt(iEndMe) = in_crvMe( in_crvOverlapMe.TOnOriginalCurve( in_crvScreen.NumPts() - iStrokeE1 - 1 ) );
		crvApproxZ.SetVec(iEndMe) = in_crvMe.Tangent( in_crvOverlapMe.TOnOriginalCurve( in_crvScreen.NumPts() - iStrokeE1 - 1 ) ) * -1.0;
	} else {
		crvApproxZ.SetPt(iEndMe) = in_crvMe( in_crvOverlapMe.TOnOriginalCurve( iStrokeE1 ) );
		crvApproxZ.SetVec(iEndMe) = in_crvMe.Tangent( in_crvOverlapMe.TOnOriginalCurve( iStrokeE1 ) );
	}
	
	if ( in_crvOverlapOther.ReverseStroke() ) {		
		crvApproxZ.SetPt(iEndOther) = in_crvOther( in_crvOverlapOther.TOnOriginalCurve( in_crvScreen.NumPts() - iStrokeE2 - 1 ) );
		crvApproxZ.SetVec(iEndOther) = in_crvOther.Tangent( in_crvOverlapOther.TOnOriginalCurve( in_crvScreen.NumPts() - iStrokeE2 - 1 ) ) * -1.0;
	} else {
		crvApproxZ.SetPt(iEndOther) = in_crvOther( in_crvOverlapOther.TOnOriginalCurve( iStrokeE2 ) );
		crvApproxZ.SetVec(iEndOther) = in_crvOther.Tangent( in_crvOverlapOther.TOnOriginalCurve( iStrokeE2 ) );
	}

	const double dLen = ::Length( crvApproxZ.GetPt(1) - crvApproxZ.GetPt(0) );
	crvApproxZ.SetVec(0) = crvApproxZ.GetVec(0) * dLen * 1.3;
	crvApproxZ.SetVec(1) = crvApproxZ.GetVec(1) * dLen * 1.3;
	
	if ( s_bTraceZValues ) {
		cout << "Join curve Z combine\n";
		cout << crvApproxZ.GetPt(0) << " " << crvApproxZ.GetPt(1) << "\n";
		cout << crvApproxZ.GetVec(0) << " " << crvApproxZ.GetVec(1) << "\n";
		cout << cam.CameraPt( crvApproxZ.GetPt(0) ) << in_crvScreen.Pt(0) << "\n";
		cout << cam.CameraPt( crvApproxZ.GetPt(1) ) << in_crvScreen.Pt( in_crvScreen.NumPts() - 1 ) << "\n";
	}
	
	/* Easy fix didn't work - now do hard one */
	Array<R3Line> aline( in_crvScreen.NumPts() );
	Array<double> adDist( in_crvScreen.NumPts() );
	
	//const bool bIsFolded = in_crvOverlap.IsFoldOver();
	//if ( s_bTraceZValues && bIsFolded ) cout << "Is folded, using linear interp \n";
	if ( iStrokeE1 < iStrokeE2 ) {
		for ( int i = 0; i < iStrokeE1; i++ ) {
			aline[i] = R3Line( cam.From(), UnitSafe( cam.RayFromEye( in_crvScreen.Pt(i) ) ) );
			
			const double dT = in_crvOverlapMe.TOnOriginalCurve( in_crvOverlapMe.ReverseStroke() ? in_crvScreen.NumPts() - i - 1 : i);
			const R3Pt ptCrv = in_crvMe( dT );
			if ( s_bTraceZValues ) cout << in_crvScreen.Pt(i) << " " << cam.CameraPt( ptCrv ) << "\n";
			adDist[i] = aline[i].Projected_dist_on_line( ptCrv );
		}
		for ( int i = iStrokeE2; i < in_crvScreen.NumPts(); i++ ) {
			aline[i] = R3Line( cam.From(), UnitSafe( cam.RayFromEye( in_crvScreen.Pt(i) ) ) );
			
			const double dT = in_crvOverlapOther.TOnOriginalCurve(in_crvOverlapOther.ReverseStroke() ? in_crvScreen.NumPts() - i - 1 : i);
			const R3Pt ptCrv = in_crvOther( dT );
			if ( s_bTraceZValues ) cout << in_crvScreen.Pt(i) << " " << cam.CameraPt( ptCrv ) << "\n";
			adDist[i] = aline[i].Projected_dist_on_line( ptCrv );
		}
	} else {
		for ( int i = 0; i < iStrokeE1; i++ ) {
			aline[i] = R3Line( cam.From(), UnitSafe( cam.RayFromEye( in_crvScreen.Pt(i) ) ) );
			
			const double dT = in_crvOverlapOther.TOnOriginalCurve(in_crvOverlapOther.ReverseStroke() ? in_crvScreen.NumPts() - i - 1 : i);
			const R3Pt ptCrv = in_crvOther( dT );
			if ( s_bTraceZValues ) cout << in_crvScreen.Pt(i) << " " << cam.CameraPt( ptCrv ) << "\n";
			adDist[i] = aline[i].Projected_dist_on_line( ptCrv );
		}
		for ( int i = iStrokeE2; i < in_crvScreen.NumPts(); i++ ) {
			aline[i] = R3Line( cam.From(), UnitSafe( cam.RayFromEye( in_crvScreen.Pt(i) ) ) );
			
			const double dT = in_crvOverlapMe.TOnOriginalCurve(in_crvOverlapMe.ReverseStroke() ? in_crvScreen.NumPts() - i - 1 : i);
			const R3Pt ptCrv = in_crvMe( dT );
			if ( s_bTraceZValues ) cout << in_crvScreen.Pt(i) << " " << cam.CameraPt( ptCrv ) << "\n";
			adDist[i] = aline[i].Projected_dist_on_line( ptCrv );
		}
	}
	const int iStart = WINmin( iStrokeE1, iStrokeE2 );
	const int iEnd = WINmax( iStrokeE1, iStrokeE2 );
	for ( int i = iStart; i < iEnd; i++ ) {
		aline[i] = R3Line( cam.From(), UnitSafe( cam.RayFromEye( in_crvScreen.Pt(i) ) ) );
		
		const double dTAlong = ( in_crvScreen.PercAlong(i) - in_crvScreen.PercAlong(iStart) ) / (in_crvScreen.PercAlong(iEnd) - in_crvScreen.PercAlong(iStart));
		ASSERT( dTAlong >= 0.0 && dTAlong <= 1.0 );
		cout << dTAlong << " ";
		const R3Pt ptCrv = crvApproxZ( dTAlong );
		adDist[i] = aline[i].Projected_dist_on_line( ptCrv );
	}
	if ( s_bTraceZValues ) cout << "\n";
	
	cout.precision(6);
	
	return AddZValue( aline, adDist );
}


