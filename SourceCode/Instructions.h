/*
 *  Instructions.h
 *  SketchCurves
 *
 *  Created by Cindy Grimm on 4/4/12.
 *  Copyright 2012 Washington University in St. Louis. All rights reserved.
 *
 */

#ifndef _INSTRUCTIONS_DEFS_H_
#define _INSTRUCTIONS_DEFS_H_

class Instructions {
    private:
    void SetOverviewText();
    void Set2DText();
    void Set2DMenuText();
    void Set2DCameraText();
    void Set2DTipsText();
    void Set3DCurveMenuText();
    void Set3DText();
    void Set3DDrawText();
    void Set3DDrawPlaneText();
    void Set3DDrawInflationText();
    void Set3DDrawExtrusionText();
    void Set3DTipsText();
    void SetCameraText();
    void SetCameraMenuText();
    void SetSurfaceText();
    void SetSurfacePointsText();
    void SetSurfaceNormalsText();
    void SetSurfaceTipsText();
    
    public:
    
    void ChangeRadioButton();
    Instructions() { };
    ~Instructions() { };
};

#endif