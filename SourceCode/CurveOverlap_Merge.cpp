/*
 *  CurveOverlap_Merge.cpp
 *  SketchCurves
 *
 *  Created by Cindy Grimm on 1/25/11.
 *  Copyright 2011 Adobe Corp.. All rights reserved.
 *
 */


#include "CurveOverlap.h"
#include "Curve.h"
#include "ScreenCurve.h"
#include "ParamFile.h"

const bool s_bTrace = false;

/* 
 * Case 1: Curve and stroke are parallel/overlapping
 *    Use middle of overlap region as split point
 * Case 2: Stroke intersects curve exactly once
 *    Use intersection point as split point, bigger join search space
 * Case 3: No intersections
 *    Use divergent point (if any) as split point
 * Case 4: Wiggles back and forth, who knows
 *    Use middle of overlap region and bigger join search space
 */

void CurveOverlap::SetJoinPoints( const CurveEndType in_strokeEnd, const bool in_bFirstJoin, const OverlapInfo &in_oi ) 
{
	const double dScore = in_oi.Score();
	const double dMapToSearch = WINmin( 4.0, 1.0 + dScore * 3.0 );
	if ( s_bTrace ) cout << "Score " << dScore << " Search " << dMapToSearch << "\n";

	const double dDistSelStroke = WINmin( 0.1 * m_crvStroke.Length(), g_drawState.ScreenSelectionSize() );
	
	const double dMaxSearchDistStroke = WINmin( 0.25 * m_crvStroke.Length(), dMapToSearch * g_drawState.ScreenSelectionSize() );
	const double dMaxSearchDistCurve = WINmin( 0.25 * m_crvProjected.Length(), dMapToSearch * g_drawState.ScreenSelectionSize() );
	    
	std::pair<double, double> &dStroke = in_bFirstJoin ? m_dTsStrokeFirstJoin : m_dTsStrokeSecondJoin;
	std::pair<double, double> &dCurve = in_bFirstJoin ? m_dTsCurveFirstJoin : m_dTsCurveSecondJoin;

	// Do we want to go forward from the curve join or backwards?
	bool bCrvForward = true;
	if ( in_bFirstJoin && ( m_overlapType == OVERSTROKE || m_overlapType == OVERSTROKE_START ) ) {
		bCrvForward = false;
	}
	if ( ( m_overlapType == MERGE || m_overlapType == CLOSES ) && in_strokeEnd == START_CURVE && m_bReverseStroke == false ) {
		bCrvForward = false;
	}
	if ( ( m_overlapType == MERGE || m_overlapType == CLOSES ) && in_strokeEnd == END_CURVE && m_bReverseStroke == true ) {
		bCrvForward = false;
	}
	
	
    OverlapInfo oi;
    SetOverlapData( oi, in_strokeEnd, dDistSelStroke * 0.25 );
    
    const double dTStroke = dMaxSearchDistStroke / m_crvStroke.Length();
    const double dTCurve = WINmax( dMaxSearchDistCurve, in_oi.m_dAvgDist ) / m_crvProjected.Length(); 
    double dTMidStroke = 0.5 * ( in_oi.m_dStroke.first + in_oi.m_dStroke.second );
	//double dTMaxStroke = WINmax( in_oi.m_dStroke.first, in_oi.m_dStroke.second );
	//double dTMinStroke = WINmin( in_oi.m_dStroke.first, in_oi.m_dStroke.second );
	//double dTMaxCurve = WINmax( in_oi.m_dCrv.first, in_oi.m_dCrv.second );
	//double dTMinCurve = WINmin( in_oi.m_dCrv.first, in_oi.m_dCrv.second );
	double dTStartOff = g_paramFile.GetDouble("MergeSearchStartOff");
    double dTEndOff = g_paramFile.GetDouble("MergeSearchEndOff");
    
    if ( oi.m_dAvgDist < dDistSelStroke * 0.2 && oi.m_dStrokeLength > 0.1 * dDistSelStroke ) {
        dTMidStroke = 0.5 * ( oi.m_dStroke.first + oi.m_dStroke.second );
		//dTMaxStroke = WINmax( oi.m_dStroke.first, oi.m_dStroke.second );
		//dTMinStroke = WINmin( oi.m_dStroke.first, oi.m_dStroke.second );
		//dTMaxCurve = WINmax( oi.m_dCrv.first, oi.m_dCrv.second );
		//dTMinCurve = WINmin( oi.m_dCrv.first, oi.m_dCrv.second );

    }
    
	// Be sure to do this before reversing stroke t value
	double dDist = 0.0;
	const double dTCrvMidStroke = m_crvProjected.ClosestPointTValue( m_crvStroke( dTMidStroke ), dDist );
	
	// Stroke will be reversed - reverse t value
	if ( m_bReverseStroke == true ) {
		dTMidStroke = 1.0 - dTMidStroke;
	}
	
	if ( bCrvForward == true ) {
		// Curve goes forward, stroke goes backward
		dStroke.first = WINmax( 0.0, dTMidStroke - dTStroke * dTEndOff );
		dStroke.second = WINmax( 0.0, dTMidStroke - dTStroke * dTStartOff );
	} else {
		// Stroke goes forward, curve goes backward
		dStroke.first = WINmax( 0.0, dTMidStroke + dTStroke * dTStartOff );
		dStroke.second = WINmax( 0.0, dTMidStroke + dTStroke * dTEndOff );
	}
	if ( m_bCurveClosed ) {
		if ( bCrvForward == true ) {
			// Curve goes forward, stroke goes backward
			dCurve.first = dTCrvMidStroke + dTCurve * dTStartOff;
			dCurve.second = dTCrvMidStroke + dTCurve * dTEndOff;
		} else {
			// Stroke goes forward, curve goes backward
			dCurve.first = dTCrvMidStroke - dTCurve * dTEndOff;
			dCurve.second = dTCrvMidStroke - dTCurve * dTStartOff;
		}
	} else {
		if ( bCrvForward == true ) {
			// Curve goes forward, stroke goes backward
			dCurve.first = WINmin( 1.0, dTCrvMidStroke + dTCurve * dTStartOff );
			dCurve.second = WINmin( 1.0, dTCrvMidStroke + dTCurve * dTEndOff );
		} else {
			// Stroke goes forward, curve goes backward
			dCurve.first = WINmin( 1.0, dTCrvMidStroke - dTCurve * dTEndOff );
			dCurve.second = WINmin( 1.0, dTCrvMidStroke - dTCurve * dTStartOff );
		}

		// We've accounted for the stroke being reversed - so all t values should be increasing
		if ( !( dCurve.first <= dCurve.second ) ) {
			cerr << "ERR: CurveOVerlap::SetJoinPoints, curve values in the wrong order " << dCurve.first << " " << dCurve.second << "\n";
		}
		if ( !( dStroke.first <= dStroke.second ) ) {
			cerr << "ERR: CurveOVerlap::SetJoinPoints, stroke values in the wrong order " << dStroke.first << " " << dStroke.second << "\n";
		}
	}
	
	if ( s_bTrace ) {
		cout << ( bCrvForward ? " forward " : " backward " );
		cout << "Curve ts: " << dCurve.first << ", " << dCurve.second;
		cout << ",  Stroke ts: " << dStroke.first << ", " << dStroke.second;
		cout << (m_bReverseStroke ? " t\n " : " f\n");
	}
}

/*
 * T values on the screen curves.
 * On the curve: The t values will be increasing.
 * On the stroke: The t values will be increasing. If the stroke is to be
 *   reversed, then the t values will be reversed already.
 *
 * Sets: 
 *   m_dTsCurveFirstJoin, m_dTsStrokeFirstJoin
 *   m_dTsCurveSecondJoin, m_dTsStrokeSecondJoin (for overstroke and overstroke start)
 * 
 * m_bReverseStroke should be set already
 * m_strokeEnd, m_crvEnd should also be set (m_strokeEnd is *already* adjusted for stroke being reversed)
 *
 * Uses the t values from the overlap region *Except* for overstroke_start,
 *   which projects the other end of the stroke down onto the curve. See CalculatingTs.psd
 */
void CurveOverlap::SetJoinTValues( )
{
	if ( m_overlapType == JOIN || m_overlapType == MERGE || m_overlapType == OVERSTROKE || m_overlapType == CLOSES ) {
		if ( m_curveEnd == START_CURVE && m_strokeEnd == START_CURVE ) {
			cerr << "ERR: GetJoinTValues, both ends start\n";
		} else if ( m_curveEnd == END_CURVE && m_strokeEnd == END_CURVE ) {
			cerr << "ERR: GetJoinTValues, both ends end\n";
		}
	}

	// Join is already done - we know it's the ends of the curve/stroke
	if ( m_overlapType == JOIN ) {
		return;
	} else if ( m_overlapType == BLEND ) {
		// So we know how much overlaps and we can ignore
		double dDist = 0.0;
		m_dTsStrokeFirstJoin.first = m_crvStroke.ClosestPointTValue( m_crvProjected.Pt(0), dDist );
		m_dTsStrokeFirstJoin.second = m_crvStroke.ClosestPointTValue( m_crvProjected.Pt( m_crvProjected.NumPts() - 1 ), dDist );
		m_dTsCurveFirstJoin.first = m_dTMin;
		m_dTsCurveFirstJoin.second = m_dTMax;
        
        return;
	}
	     
	m_dTsStrokeSecondJoin.first = 0.0;
	m_dTsStrokeSecondJoin.second = 0.0;
	m_dTsCurveSecondJoin.first = 0.0;
	m_dTsCurveSecondJoin.second = 0.0;
	
	if ( m_overlapType == OVERSTROKE ) {
		// Two ends - either reverse both or none 
		// First end is at start of curve, second end is at end of curve
		if ( s_bTrace ) cout << "\nGetJoinValues, overstroke first end: ";
		SetJoinPoints( m_bReverseStroke ? END_CURVE : START_CURVE, true, m_bReverseStroke ? m_strokeEnd2 : m_strokeEnd1 );
		if ( s_bTrace ) cout << "GetJoinValues, overstroke second end: ";
        SetJoinPoints( m_bReverseStroke ? START_CURVE : END_CURVE, false, m_bReverseStroke ? m_strokeEnd1 : m_strokeEnd2 );

		if ( m_dTsCurveFirstJoin.second > m_dTsCurveSecondJoin.first ) {
			// Split the difference
			const double dMid = 0.5 * ( m_dTsCurveFirstJoin.first + m_dTsCurveSecondJoin.second );
			m_dTsCurveFirstJoin.second = dMid - 1e-30;
			m_dTsCurveSecondJoin.first = dMid + 1e-30;
			if ( s_bTrace ) cout << "WARNING: overstroke t values overlapped\n";
		}
	} else if ( m_overlapType == CLOSES ) {
		// Two ends - either reverse both or none 
		// First end is at end of curve, second end is at start
	
		if ( s_bTrace ) cout << "\nGetJoinValues, CLOSES first end: ";
		const double dDistSel = WINmin( 0.1 * m_crvStroke.Length(), g_drawState.ScreenSelectionSize() );
		const double dTCrv = dDistSel / m_crvProjected.Length();
		const double dTStroke = dDistSel / m_crvStroke.Length();
		if ( IsJoinStartStroke() ) {
			m_dTsCurveFirstJoin.first = WINmax( 0.5, 1.0 - dTCrv );
			m_dTsCurveFirstJoin.second = 1.0;
			m_dTsStrokeFirstJoin.first = 0.0;
			m_dTsStrokeFirstJoin.second = WINmin( 0.5, dTStroke );
		} else {
			if ( m_bReverseStroke ) {
				SetJoinPoints( END_CURVE, true, m_strokeEnd2 );
			} else {
				SetJoinPoints( START_CURVE, true, m_strokeEnd1 );
			}
		}
		if ( s_bTrace ) cout << "\nGetJoinValues, CLOSES second end: ";
		if ( IsJoinEndStroke() ) {
			m_dTsCurveSecondJoin.first = 0.0;
			m_dTsCurveSecondJoin.second = WINmin( 0.5, dTCrv );
			m_dTsStrokeSecondJoin.first = WINmax( 0.5, 1.0 - dTStroke );
			m_dTsStrokeSecondJoin.second = 1.0;
		} else {
			if ( m_bReverseStroke ) {
				SetJoinPoints( START_CURVE, false, m_strokeEnd1 );
			} else {
				SetJoinPoints( END_CURVE, false, m_strokeEnd2 );
			}
		}
		
		if ( m_dTsCurveFirstJoin.first < m_dTsCurveSecondJoin.second && m_dTsCurveFirstJoin.first > m_dTsCurveSecondJoin.first ) {
			// Split the difference
			const double dMid = 0.5 * ( m_dTsCurveFirstJoin.first + m_dTsCurveSecondJoin.second );
			m_dTsCurveSecondJoin.second = dMid - 1e-30;
			m_dTsCurveFirstJoin.first = dMid + 1e-30;
			if ( s_bTrace ) cout << "WARNING: close t values overlapped\n";
		}
	} else if ( m_overlapType == OVERSTROKE_START ) {
		// Four cases, based on which end is closest and the orientation of the stroke
		// Dangling end is special-cased:
		//   1) Project dangling end of stroke onto curve
		//   2) Assure that curve t values are still increasing
		if ( s_bTrace ) cout << "\nGetJoinValues, overstroke start first end: ";
		
		const double dDistSel = 2.0 * g_drawState.ScreenSelectionSize();		
		const double dTCrv = 2.0 * dDistSel / m_crvProjected.Length();
		const double dTStroke = dDistSel / m_crvStroke.Length();

		bool bCheckEnd2 = true;
		double dDist = 0.0;
		
		if ( m_strokeEnd == START_CURVE && m_bReverseStroke == false ) {
			SetJoinPoints( START_CURVE, true, m_strokeEnd1 );

			m_dTsStrokeSecondJoin.first = WINmax(0.5, 1.0 - dTStroke);
			m_dTsStrokeSecondJoin.second = 1.0;
			m_dTsCurveSecondJoin.first = m_crvProjected.ClosestPointTValue( m_crvStroke(1.0), dDist );
			m_dTsCurveSecondJoin.second = WINmin( 1.0, m_dTsCurveSecondJoin.first + dTCrv );

		} else if ( m_strokeEnd == START_CURVE && m_bReverseStroke == true ) {
			// Same case as above, but they drew the stroke the other direction 
			SetJoinPoints( END_CURVE, true, m_strokeEnd2 );
			
			m_dTsStrokeSecondJoin.first = WINmax(0.5, 1.0 - dTStroke);
			m_dTsStrokeSecondJoin.second = 1.0;
			m_dTsCurveSecondJoin.first = m_crvProjected.ClosestPointTValue( m_crvStroke(0.0), dDist );
			m_dTsCurveSecondJoin.second = WINmin( 1.0, m_dTsCurveSecondJoin.first + dTCrv );

		} else if ( m_strokeEnd == END_CURVE && m_bReverseStroke == false ) {
			// It's the end of the stroke that hits the curve; first t values are set by projection
			SetJoinPoints( END_CURVE, false, m_strokeEnd2 );
			
			m_dTsStrokeFirstJoin.first = 0.0;
			m_dTsStrokeFirstJoin.second = WINmin(0.5, dTStroke);
			m_dTsCurveFirstJoin.second = m_crvProjected.ClosestPointTValue( m_crvStroke(0.0), dDist );
			m_dTsCurveFirstJoin.first = WINmax( 0.0, m_dTsCurveFirstJoin.second - dTCrv );
			bCheckEnd2 = false;

		} else {
			// Same case as above, but they drew the stroke the other way so project start of stroke onto curve
			SetJoinPoints( START_CURVE, false, m_strokeEnd1 );
			
			m_dTsStrokeFirstJoin.first = 0.0;
			m_dTsStrokeFirstJoin.second = WINmin(0.5, dTStroke);
			m_dTsCurveFirstJoin.second = m_crvProjected.ClosestPointTValue( m_crvStroke(1.0), dDist );
			m_dTsCurveFirstJoin.first = WINmax( 0.0, m_dTsCurveFirstJoin.second - dTCrv );
			bCheckEnd2 = false;
			
		}

		// A last check - make sure the curve t values are all in increasing order 
		//  FirstJoin.first FirstJoin.second SecondJoin.first SecondJoin.second
		if ( bCheckEnd2 == true ) {
			if ( m_dTsCurveFirstJoin.first > m_dTsCurveSecondJoin.second ) {
				// Stroke end didn't project to end of curve - let the merge try everything
				// from the end of the first overlap on
				m_dTsCurveSecondJoin.first = WINmin( 1.0, m_dTsCurveFirstJoin.second + dTCrv );
				m_dTsCurveSecondJoin.second = 1.0;
			}
		} else {
			if ( m_dTsCurveFirstJoin.first > m_dTsCurveSecondJoin.second ) {
				// Stroke end didn't project to end of curve - let the merge try everything
				// from the start of the curve to the overlap region
				m_dTsCurveFirstJoin.first = 0.0;
				m_dTsCurveFirstJoin.second = WINmax( 0.0, m_dTsCurveSecondJoin.first - dTCrv );
			}
		}
		// Now have the outer t values in line - check the inner
		if ( m_dTsCurveFirstJoin.second > m_dTsCurveSecondJoin.first ) {
			// Split the difference
			const double dMid = 0.5 * ( m_dTsCurveFirstJoin.first + m_dTsCurveSecondJoin.second );
			m_dTsCurveFirstJoin.second = dMid - 1e-30;
			m_dTsCurveSecondJoin.first = dMid + 1e-30;
			if ( s_bTrace ) cout << "WARNING: overstroke t values overlapped\n";
		}
	} else if ( m_overlapType == MERGE ) {
		
		// Four cases, based on which end is closest and the orientation of the stroke
		if ( s_bTrace ) cout << "\nGetJoinValues, merge  " << (m_strokeEnd == START_CURVE ? "start" : "end") << (m_bReverseStroke ? " t: " : " f: ");
		if ( m_strokeEnd == START_CURVE && m_bReverseStroke == false ) {
			SetJoinPoints( START_CURVE, true, m_strokeEnd1 );
		} else if ( m_strokeEnd == START_CURVE && m_bReverseStroke == true ) {
			SetJoinPoints( END_CURVE, true, m_strokeEnd2 );
		} else if ( m_strokeEnd == END_CURVE && m_bReverseStroke == false ) {
			SetJoinPoints( END_CURVE, true, m_strokeEnd2 );
		} else {
			SetJoinPoints( START_CURVE, true, m_strokeEnd1 );
		}
	} else if ( m_overlapType == NO_OVERLAP ) {
		return;
	}
	
	if ( m_dTsCurveFirstJoin.second < m_dTsCurveFirstJoin.first ) {
        if ( m_overlapType != CLOSES )
            cerr << "ERR: GetJoinTValues curve first t values in the wrong order \n";
	} else if ( m_overlapType == OVERSTROKE || m_overlapType == OVERSTROKE_START ) {
		if ( m_dTsCurveSecondJoin.second < m_dTsCurveSecondJoin.first ) {
			cerr << "ERR: GetJoinTValues curve second t values in the wrong order \n";
		}
		if ( m_dTsCurveSecondJoin.first < m_dTsCurveFirstJoin.second ) {
			cerr << "ERR: GetJoinTValues curve overlap segment values in the wrong order \n";
		}
	}
}


void CurveOverlap::SetJoinInfo( const CurveEndType &in_strokeEnd, const bool in_bUseJoin )
{
	const OverlapInfo &oi = (in_strokeEnd == START_CURVE) ? m_strokeEnd1 : m_strokeEnd2;
	const JoinInfo &ji = (in_strokeEnd == START_CURVE) ? m_strokeJoinEnd1 : m_strokeJoinEnd2;
	
	if ( in_bUseJoin == true ) {
		m_bReverseStroke = ji.m_bStrokeReversed;
	} else {
		m_bReverseStroke = oi.m_bStrokeReversed;
	}
	
	m_strokeEnd = in_strokeEnd;
	if ( m_bReverseStroke == true ) {
		m_strokeEnd = (in_strokeEnd == START_CURVE) ? END_CURVE : START_CURVE;
	}
	m_curveEnd = (m_strokeEnd == START_CURVE) ? END_CURVE : START_CURVE;
}

