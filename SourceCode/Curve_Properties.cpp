/*
 *  Curve_Properties.cpp
 *  SketchCurves
 *
 *  Created by Cindy Grimm on 12/30/10.
 *  Copyright 2010 Washington University in St. Louis. All rights reserved.
 *
 */

#include "Curve.h"
#include <fitting/FITTools_FitLine.H>

const bool s_bTrace = false;

/* Actually calculate the bounding box. Used in SetDistanceData */
const std::pair<R3Pt,R3Pt> Curve::CalcBoundingBox() const
{
	R3Pt ptLL(1e30,1e30,1e30), ptUR(-1e30,-1e30,-1e30);
	
	if ( NumPts() == 0 ) {
		return std::pair<R3Pt,R3Pt>( R3Pt(0,0,0), R3Pt(0,0,0) );
	}
	
	for ( int i = 0; i < NumPts(); i++ ) {
		for ( int j = 0; j < 3; j++ ) {
			ptLL[j] = WINmin( ptLL[j], m_apt[i][j] );
			ptUR[j] = WINmax( ptUR[j], m_apt[i][j] );
		}
	}
	return std::pair<R3Pt,R3Pt>( ptLL, ptUR );
}


/* Sum of length of curve. Uses pre-calc'd version if possible */
double Curve::Length() const 
{ 
    if ( m_apt.num() < 2 ) {
        return 0.0;
    } else if ( m_apt.num() == 2 ) {
        return ::Length( m_apt[1] - m_apt[0] );
    }
    
    if ( m_bPercData == true ) {
        return m_adDist.last();        
    }
    
    double dSum = 0.0;
    for ( int i = 0; i < m_apt.num() - 1; i++ ) {
        dSum += ::Length( m_apt[i+1] - m_apt[i] );
    }
    
    return dSum / ( m_apt.num() - 1.0 );        
}

/* Average of points */
R3Pt  Curve::CenterPoint() const
{
    if ( m_apt.num() == 0 ) {
        return R3Pt(0,0,0);
    }
    
    R3Pt ptCenter(0.0, 0.0, 0.0);
    for ( int i = 0; i < m_apt.num(); i++ ) {
        for ( int j = 0; j < 3; j++ ) {
            ptCenter[j] += m_apt[i][j];
        }
    }
    for ( int j = 0; j < 3; j++ ) {
        ptCenter[j] /= (double) m_apt.num();
    }
    return ptCenter;
}


/*
 * First fit a line to the curve. If it's linear, return the best-fit line and just
 * pick a random perpendicular vector to be the normal
 * If it's not linear, look for the point that's furthest from the best-fit line
 * Use this to create a normal vector
 * See if every point projects to the plane
 */
bool Curve::IsPlanar( R3Vec &out_vecLine, R3Vec &out_vecNormal ) const
{
	out_vecLine = R3Vec(1,0,0);
	out_vecNormal = R3Vec(0,1,0);
	
	if ( m_apt.num() < 2 ) {
		return true;
	}
	FITTools_FitLine oFitLine;
	
	oFitLine.Fit( m_apt );
	
	R3Vec vecBest(0,1,0);
	double dBest = 0.0;
	const R3Line &line = oFitLine.Line3D();
	out_vecLine = line.Vec();
	
	for ( int i = 0; i < m_apt.num(); i++ ) {
		const double dDist = line.Dist_to_line( m_apt[i] );
		if ( dDist > dBest ) {
			dBest = dDist;
			vecBest = m_apt[i] - line.Project( m_apt[i] );
		}
	}
	
	// If it's linear, it's planar...
	if ( RNIsZero( dBest, 1e-12 ) ) {
		const R3Vec vecCenter = CenterPoint() - R3Pt(0,0,0);
		if ( RNIsZero(::Length( vecCenter ), 1e-6) ) {
			out_vecNormal = UnitSafe( line.Vec().Perpendicular() );
		}
		
		out_vecNormal = Rejection( line.Vec(), CenterPoint() - R3Pt(0,0,0) );
		if ( RNIsZero(::Length( out_vecNormal ), 1e-6) ) {
			out_vecNormal = UnitSafe( line.Vec().Perpendicular() );
		} else {
			out_vecNormal.Normalize();
		}
		return true;
	}			
	
	out_vecNormal = UnitSafe( Cross( vecBest, out_vecLine ) );
	const R3Plane plane( m_apt[ m_apt.num() / 2 ], out_vecNormal );
	const double dLenCap = 0.01 * Length();
	for ( int i = 0; i < m_apt.num(); i++ ) {
		const double dDist = plane.DistToPlane(m_apt[i]);
		if ( dDist > dLenCap ) {
			return false;
		}
	}
	
	// Fit line sometimes gets a bit off; this will force a truely planar curve to return a correct plane
	if ( ::Length( m_apt[ m_apt.num() / 2 ] - m_apt[0] ) > 1e-6 ) {
		const R3Vec vec = UnitSafe( m_apt[ m_apt.num() / 2 ] - m_apt[0] );
		out_vecNormal = UnitSafe( Rejection( vec, out_vecNormal ) );
	}
	return true;
}

/* 
 * First point same as last
 */
bool Curve::IsClosed() const
{
	if ( m_apt.num() < 3 ) {
		return false;
	}
	
	return ApproxEqual( m_apt[0], m_apt.last() );
}

/* Average segment length. Use pre-calc'd data if possible */
double Curve::AverageSegLength() const
{
    if ( m_apt.num() < 2 ) {
        return 0.0;
    } else if ( m_apt.num() == 2 ) {
        return ::Length( m_apt[1] - m_apt[0] );
    }
    
    if ( m_bPercData == true ) {
        return m_adDist.last() / ( m_apt.num() - 1.0 );        
    }
    
    double dSum = 0.0;
    for ( int i = 0; i < m_apt.num() - 1; i++ ) {
        dSum += ::Length( m_apt[i+1] - m_apt[i] );
    }
    
    return dSum / ( m_apt.num() - 1.0 );        
}

/*
 * Calculations of discrete curvature:
 *  1) For a circle, the curvature is 1/R, where R is the radius of the circle.
 *  2) Curvature is also change in angle over chane in length (d Theta / d s)
 *     Change in angle is approximated by the exterior angle between v1 and v2
 *     ie, acos( dot<v1,v2> / ||v1|| ||v2|| )
 *                  / u2
 *        -  u1  ->/ theta
 *     Change in length is approximated by the average length of v1 and v2
 *  3) Can also get by taking cross product and measuring lengths
 */
double Curve::Curvature( const R3Pt &in_ptPrev, const R3Pt &in_pt, const R3Pt &in_ptNext ) 
{
    const R3Vec vecV1 = ( in_pt - in_ptPrev );
    const R3Vec vecV2 = ( in_ptNext - in_pt );
    const double dLenV1 = ::Length( vecV1 );
    const double dLenV2 = ::Length( vecV2 );
    if ( RNIsZero( dLenV1 ) || RNIsZero( dLenV2 ) )
        return 0.0;
    
    if ( RNIsZero( ::Length( in_ptNext - in_ptPrev ) ) ) {
        return 1e10;
    }
    
    const double dDot = Dot( vecV1, vecV2 ) / (dLenV1 * dLenV2);
    const double dTheta = acos( WINminmax( dDot, -1.0 + RNEpsilon_d, 1.0 - RNEpsilon_d ) );	
    //const double dRet = 2.0 * sin( dTheta / 2.0 ) / sqrt( dLenV1 * dLenV2 ); 
    const double dRet = 2.0 * dTheta / ( 0.5 * (dLenV1 + dLenV2) );
    
    /*
	 const R2Line line( R2Pt( in_ptPrev[0], in_ptPrev[1]), R2Pt( in_ptNext[0], in_ptNext[1]) );
	 const double dDist = line.Dist_to_line( R2Pt(in_pt[0], in_pt[1]) ); // / (dLenV1 + dLenV2);
     */
    
    /*
	 const R3Vec vecCross = Cross( vecV1, vecV2 );
	 const double dRet = 2.0 * vecCross[2] / ( dLenV1 * dLenV2 * Length( vecV1 + vecV2 ) );
     */
	
    //cout << "Theta " << dTheta << " len " << Length( in_ptPrev - in_ptNext ) << "\n";
    /*
	 if ( RNApproxEqual( Dot( vecV1, vecV2 ) / ( dLenV1 * dLenV2), 1.0 ) ) {
	 cout << dRet << " " << dDist << " " << 0.0 << "\n";
	 } else {
	 //const R2Sphere circ( R2Pt( in_ptPrev[0], in_ptPrev[1] ), R2Pt(in_pt[0], in_pt[1]), R2Pt(in_ptNext[0], in_ptNext[1]) );
	 //const double dRet = 1.0 / circ.Radius();
	 //cout << dRet << " dist " << dDist << " circ " << 1.0 / circ.Radius() << " cross " << dAng << " crv " << 2.0 * dTheta / (dLenV1 + dLenV2) << " " << dTheta / Length( in_ptPrev - in_ptNext) <<  "\n";
	 cout << dRet << " dist " << dDist << "\n";
	 }
     */
    //ASSERT( dRet >= 0.0 && dRet < 1000.0 );
    return dRet;
}

/* Curvature at point */
double Curve::Curvature( const int in_iIndex ) const
{
    if ( m_apt.num() < 3 ) {
        return 0;
    }
    
    if ( in_iIndex == 0 ) {
        return Curvature( m_apt[0], m_apt[1], m_apt[2] );
    } else if ( in_iIndex == m_apt.num() - 1 ) {
        return Curvature( m_apt[ m_apt.num() - 3 ], m_apt[ m_apt.num() - 2 ], m_apt[ m_apt.num() - 1 ] );
    }
    return Curvature( m_apt[ in_iIndex - 1 ], m_apt[ in_iIndex ], m_apt[ in_iIndex + 1 ] );
}

/* Average of the curvature values */
double Curve::AverageCurvature() const
{
    if ( m_apt.num() < 4 ) return 0.0;
    
    double dAvgCrv = 0.0;
    for ( int i = 1; i < m_apt.num() - 1; i++ ) {
        dAvgCrv += Curvature( m_apt[i-1], m_apt[i], m_apt[i+1] );
    }
    
    return dAvgCrv / (m_apt.num() - 2.0);
}


/* Average of tangents */
R3Vec Curve::AverageTangent() const
{
    if ( m_apt.num() < 2 ) {
        return R3Vec(0,0,1);
    } else if ( m_apt.num() == 2 ) {
        return UnitSafe( m_apt[1] - m_apt[0] );
    }
    
    R3Vec vecAvg(0,0,0);
    for ( int i = 0; i < m_avecTangent.num(); i++ ) {
        vecAvg += m_avecTangent[i];
    }
    
    return UnitSafe( vecAvg );
}




/* Legacy: I use the 2D version in Screen curve
 * Find all points that might be corners.
 * Essentially the short straw algorithm
 * in_dDist is the sample distance to use, i.e., the straw goes from pt +- in_dDist
 */
boost::dynamic_bitset<> Curve::Corners( const double in_dDist ) const
{
    boost::dynamic_bitset<> abCorner( m_apt.num() );
    abCorner.reset();
    
    // Find all places where there is an angle change
    Array<double> adBend( m_apt.num() ), adCurvature( m_apt.num() );
    const double dTDelta = in_dDist / Length();
	if ( s_bTrace ) cout << "corner M_PI / 3.0\n";
    for ( int i = 0; i < m_apt.num(); i++ ) {        
        if ( PercAlong(i) > dTDelta && PercAlong(i) < 1.0 - dTDelta ) {
            const R3Pt ptForward = (*this)( PercAlong(i) + dTDelta );
            const R3Pt ptBackward = (*this)( PercAlong(i) - dTDelta );
            const double dDot = Dot( UnitSafe( ptForward - m_apt[i] ), UnitSafe( ptBackward - m_apt[i] ) );
            adBend[i] = M_PI - acos( WINminmax( dDot, -1.0 + RNEpsilon_d, 1.0 - RNEpsilon_d ) );
			adCurvature[i] = Curvature(i);
            if ( s_bTrace ) cout << i << " " << adBend[i] << " " << dDot << " " << Curvature(i) << "\n";
        } else {
            if ( s_bTrace ) cout << i << " 0 0 0 \n";
            adBend[i] = 0.0;
			adCurvature[i] = 0.0;
        }
    }
    if ( s_bTrace ) cout << "\n";
    
    // Keep only the biggest in the local area (prevents multiple local corners)
    for ( int i = 1; i < m_apt.num()-1; i++ ) {
        if ( adBend[i] > M_PI / 3.0 ) {
            bool bIsMax = true;
            for ( int j = i-1; j >= 0; j-- ) {
                if ( PercAlong(j) < PercAlong(i) - dTDelta ) {
                    break;
                }
                if ( adCurvature[j] > adCurvature[i] ) {
                    bIsMax = false;
                    break;
                }
            }
            for ( int j = i+1; j < adCurvature.num(); j++ ) {
                if ( PercAlong(j) > PercAlong(i) + dTDelta ) {
                    break;
                }
                if ( adCurvature[j] > adCurvature[i] ) {
                    bIsMax = false;
                    break;
                }
            }
            if ( bIsMax ) {
                if ( s_bTrace ) cout << i << " " << PercAlong(i) << " Ang " << adBend[i] << " crv " << Curvature( i ) << "\n";
                abCorner.set(i);
            }
        }
    }
    return abCorner;
}


