/*
 *  ActionEvents.h
 *  SketchCurves
 *
 *  Created by Cindy Grimm on 9/9/10.
 *  Copyright 2010 Washington University in St. Louis. All rights reserved.
 *
 */

#ifndef _ACTION_EVENTS_DEFS_H__
#define _ACTION_EVENTS_DEFS_H__

#include <OpenGL/OGLObjs_Camera.H>

#include <time.h>

#include "Curve.h"
#include "ScreenCurve.h"
#include "CurveNetwork.h"
#include "DrawState.h"
#include "SurfaceData.h"
#include "Webbing.h"
#include "InflationSurface.h"
#include "PointConstraint.h"
#include "PatchSurface.h"

/*
 * This class handles almost all of the event handling and logging. What it doesn't handle is timer callbacks (handled
 * by MeshViewer) and calling some of the draw stuff (also handled by MeshViewer).
 *
 * It also handles drawing the drag icons, the smoothing/last stroke circles, and the menus.
 *
 * It handles selection and menus. There is a one-level stack for actions; this is only used in the case
 * of camera actions when dragging a curve
 *
 * Undo: Things that can be undone
 *   1) Roll-back stroke
 *      1a) Stroke applies to single curve (join, overstroke, merge, close): Pop top of stack off, recover
 *          2D stroke
 *      1b) New stroke: Delete created curve. Recover 2D stroke.
 *      1c) Combine two strokes: Pop top of stack off of main curve, add deleted curve back in again, recover
 *          2D stroke
 *  Note: Blend->2D stroke->stroke action is always rolled back (ie, can't undo just a blend)
 *   2) Curve edits
 *     2a) Drag/rotate/scale: Question, does this undo the entire affine transformation, from mouse down
 *       to click to clear? Probably. Pop off last action
 *     2b) Smoothing:  Pop off last action (clears entire smoothing session)
 *     2c) Erasing: i) put entire curve back ii) put erased part of curve back iii) delete new created curve and restore old
 *   3) Constraint edits
 *     3a) Adding/removing point/normal constraint. Need to restore curves to pre-moved state.
 *     3b) Changing point constraint: Essentially a drag. Restore positions
 *     3c) Changing normal constraint: Put normals back where they were
 *     3d) Change constraint type
 *
 *  Trickiness:
 *    1) If pin point exists, and other curves altered because pin moved, then need to undo those moves as well
 *    2) Pins may be created/deleted by curve actions
 *
 * 12/11: Moved the undo functionality into CurveNetwork
 *
 * Undo solution: 
 *     1) Extended stroke history to be a complete history, including constraint edits
 *        1a) Some actions, like repeated drags, are treated as a single, combined action
 *        1b) StrokeHistory stores needed information to do undo (eg, pin and curve ids involved in the action)
 *     2) Added a history stack to the constraints
 *     3) Added a semaphore flag around undo actions so that they are not put on the stack
 *     4) Undos are specifically marked in the history stacks (history stacks are not deleted). This
 *        should allow for re-do after an undo
 *     5) Undo information is tracked per curve and per constraint, not globally
 *
 * 1/12: Curve selection
 *   1) m_iCursorOverCurve has the index of the currently selected curve (-1 if not selected)
 *       Set primarily in SetSelectionState, but also on mouse down over normal constraint
 *   2) m_dCursorOverCurve is the t value of the selection point (-1 if not selected). If a 
 *       region is selected, then the t value will be clamped to the selected region. Always in the range
 *       [0,1], even for closed curves
 *   3) m_dCursorOverCurveTs are the bracketing values of the selected region (if any). If no
 *       region is selected than .first == .second. If the curve is closed than the t values
 *       may be [-, second] or [first, > 1], ie, first < second always.
 *   4) On a partial drag we record the t values of the start and stop regions in
 *       m_iCursorOverCurveTs and of the drag t value in m_iCursorOverCurveIndex (StartDrag). This
 *       is because the t values will change as the length of the curve changes. 
 *       This happens in ActivateHotSpot on a mouse down. All indices are cleared and the
 *       new t values put back into m_dCursorOverCurve* on a mouse up (EndDrag()).
 *       Encapsulated in the StartDrag function. All indices in the range [0, npts -1]
 */
class ActionEvents {
public:
    /* Specific action we are performing
     * Active when mouse is down or in extended action
     *   - Dragging curve
	 *   - Inflation surface
     *   - Menus
	 * The camera states are invoked from the camera menu and clicking on the shadow box
	 * The menu states reflect what stage we're in for menus (started mouse drag, know we're selecting a curve, know we're selecting a curve region)
	 *   menu has secondary state stored in m_menuSelection, m_menuWhich
	 * The drawplane, Edit, smooth, states are invoked by clicking on the respective interface elements
	 * Drag is curve dragging. Secondary state in m_bIsPerpendicularDrag and *OverCurve* (which curve and t values)
	 * Inflation surface is inflation/extrusion surface drawing; it has a secondary state in m_inflationSrfEditAction 
	 *
	 * If you add an action item or re-arrange, make sure to adjust the corresponding
	 * text strings in ActionEvents.cpp, s_astrAction
     *
     * Editing normal constraints: There are two types of edits, a free-form rotation (EDIT_INTERSECTION_NORMAL)
     * and a rotate the normal around the tangent of the curve (EDIT_CURVE_NORMAL). The latter is chosen
     * if 1) there is only one curve or 2) the normal was selected from a drag in the curve menu.
     *
     * Either way, the actual normal constraint is stored in m_opCursorOverCurveNormalConstraint.
	 */
	typedef enum {
        NO_ACTION = 0,      // Mouse is up and no state/mode selected. Mouse is down (no selection) and dragging but hasn't moved enough to be a draw
        MENU,               // Activates after bringing up a menu by tapping. Deactivated by leaving menu or mouse down in menu
        MENU_DRAG,          // Activates after mouse down in menu. Deactivated on mouse release or switch to selecting curve/curve region.
		MENU_SELECT_CURVE,  // Activated after mouse down in some menus. Searching for curve.
		MENU_SELECT_CURVE_REGION, // Activated after mouse down in curve menu. Selecting curve region.
		CAMERA_SCALE_ROTATE, // Activates after mouse down in camera menu. Deactivated on mouse release.
		CAMERA_PAN,         // Activates after mouse down in camera menu. Deactivated on mouse release.
		CAMERA_DOLLY_ZOOM,  // Activates after mouse down in camera menu. Deactivated on mouse release.
		CAMERA_UP_DOWN,     // Activates after mouse down in camera menu or shadow box hot spot. Deactivated on mouse release.
		CAMERA_LEFT_RIGHT,  // Activates after mouse down in camera menu or shadow box hot spot. Deactivated on mouse release.
		CAMERA_SPIN,        // Activates after mouse down on shadow box hot spot. Deactivated on mouse release.
		CAMERA_ZOOM,        // Activates after mouse down on shadow box hot spot. Deactivated on mouse release.
		CAMERA_TRACKBALL,   // Activated by shift mouse down and moving more than a tap. Deactivated on mouse release
		CAMERA_ALIGN,       // Activated by tap in align menu or drag to curve. Deactivated by tap.
        DRAG_CURVE,         // Activated by mouse down in curve drag menu. Deactivated by tap in background.
                            // The following three are activated from DRAG_CURVE by clicking on the appropriate icon
                            // Scale/rotate/rotate-scale Icon appears when cursor is close to curve but not near drag icon
                            //    On curve: rotate, Above right: rotate-scale, Below right: scale
        ROTATE_CURVE,       // Activated from DRAG_CURVE. Deactivated by mouse release
        SCALE_CURVE,        // Activated from DRAG_CURVE. Deactivated by mouse release
        ROTATE_SCALE_CURVE, // Activated from DRAG_CURVE. Deactivated by mouse release
		EDIT_CURVE_NORMAL,  // Activated from curve menu then drag (either making new constraint or editing old one). Deactivated by mouse release.
		EDIT_CURVE_POINT_CONSTRAINT,  // Activated from curve menu then drag (either making new constraint or editing old one). Deactivated by mouse release.
                                      // Also activated by clicking and dragging on a curve normal with only one curve
		EDIT_INTERSECTION_NORMAL,  // Activated by clicking and dragging on intersection surface normal.
		SMOOTHING,          // Activated by mouse down in select region circles after smooth. Deactivated by mouse release
        DRAWING,            // Activated by mouse down when no other action selected and moving more than a tap
        EXTRUSION_SURFACE,  // Activated by gui toggle. Pick direction for surface, draw on surface. m_inflationSrfEditAction has type. Deactivated by tap or gui toggle
        INFLATION_SURFACE,  // Activated by gui toggle. Alternate drawing left and right boundaries, drawing on surface. m_inflationSrfEditAction has type. Deactivated by tap or gui toggle
        
		DRAWPLANE_ALIGN,    // Activated by drag in curve menu. Deactivated by mouse release
        DRAWPLANE_DRAG, // Mouse down on selected box on drawing plane. Deactivated on mouse release.
        DRAWPLANE_ROTATE, // Mouse down on selected cylinder on drawing plane side. Deactivated on mouse release.
		NUM_ACTIONS
    } Action;
    
    /* Menu selection
	 * First item is at the top of the menu, items are then listed in counter-clockwise order
	 * Center selection is the last one
	 *
	 * If you add a menu/menu item or re-arrange, make sure to adjust the corresponding
	 * text strings in ActionEvents_Menu.cpp, s_astrMenu
	 */
    typedef enum {
        NO_MENU_SELECTED = 0,
		
		MENU_LAST_STROKE,
		MENU_LAST_STROKE_COMBINE,
        MENU_LAST_STROKE_MERGE_CLOSE,
        MENU_LAST_STROKE_MERGE,
        MENU_LAST_STROKE_BLEND,
        MENU_LAST_STROKE_DELETE,
        MENU_LAST_STROKE_NEW_CLOSED,
        MENU_LAST_STROKE_OVERSTROKE,
        MENU_LAST_STROKE_SMOOTH,
        MENU_LAST_STROKE_NEW_CURVE,
		
		MENU_CURVE,
		MENU_CURVE_SNAP,
		MENU_CURVE_GROUP,
		MENU_CURVE_SMOOTH,
		MENU_CURVE_ALIGN,
		MENU_CURVE_ERASE,
		MENU_CURVE_UNDO,
		MENU_CURVE_HIDE,
		MENU_CURVE_NORMAL,
		MENU_CURVE_DRAG,
		
		MENU_CAMERA,
		MENU_CAMERA_ROTATE_UP_DOWN,
		MENU_CAMERA_DOLLY_ZOOM,
		MENU_CAMERA_SCALE_ROTATE,
		MENU_CAMERA_ALIGN,
		MENU_CAMERA_CENTER,
		MENU_CAMERA_PAN,
		MENU_CAMERA_ROTATE_LEFT_RIGHT,
		MENU_CAMERA_TRACKBALL,
		MENU_CAMERA_UNDO,
		
		NUM_MENU
		
    } MenuSelection;
    
    /** What the mouse is over/selected. Currently used only for debugging;
     * Should make this the selection option */
    typedef enum {
        NO_SELECTION = 0,
        SEL_CURVE,
        SEL_POINT_CONSTRAINT,
        SEL_NORMAL_CONSTRAINT,
        SEL_POTENTIAL_INTERSECTION,
        SEL_SHADOW_BOX,
        SEL_LAST_STROKE,
        SEL_SMOOTHING_CIRCLE,
        SEL_INFLATION_SURFACE,
        SEL_ALIGN_CAMERA,
        SEL_DRAG,
        SEL_PERPENDICULAR_DRAG,
        SEL_ROTATE,
        SEL_ROTATE_AND_SCALE,
        SEL_SCALE,
        NUM_SELECTION_TYPES
    } ActiveSelection;
    
    /// For debugging
    static const bool s_bTraceCurveSelection;
	
private:
	/* If we do a camera motion during a drag, push the last active motion onto the "stack" to pop when done */
    Action        m_action, m_pushAction;
	/* Which menu is selected (if any) and which sub menu item (if any) */
    MenuSelection m_menuSelection, m_menuWhich;
	
	bool          m_bInFocus; /// Cursor in the window?
	bool          m_bIsTap;	  /// Set to true on mouse down. Will be set to false after a sufficiently long drag 
    
	/* 2D screen coordiantes in [-1,1]X[-1,1] coordinate system */
    R2Pt          m_ptScreenLast, m_ptScreenDown, m_ptScreenUp, m_ptMenuDown;
	/* Equivalent rays from the camera through the pixel */
    R3Ray         m_rayLast, m_rayDown, m_rayMenuDown;
	
	/* Accumulate the 2D screen coordinates  in [-1,1]X[-1,1] coordinate system */
    ScreenCurve  m_curveTempScreen;
    
    /* If doing a snap, store first curve here */
    std::pair<int,double> m_iSnapCurveFirst;
		
	/* 
	 * Selection information. Set in SetSelectState and ActivateHotspot. 
	 * Curve region and axis information may be updated in MouseDrag as well.
	 */
    ActiveSelection           m_activeSelection;
    
    /// Surface intersection data
    int                       m_iCursorOverIntersection;
	/// Curve data. During a menu drag keeps track of the biggest and smallest ts found
    int                       m_iCursorOverCurve; /// which curve
	double                    m_dCursorOverCurve; /// t value at cursor. If curve is closed, and selecting region, will range between min and max t value
	std::pair<double, double> m_dCursorOverCurveTs; /// bracketing t values. If curve is closed, min t value may go negative or max t value may go greater than 1
	std::pair<int, int>       m_iCursorOverCurveTs; /// Actual index of closest points on curve. Used to re-establish t values at start of new drag.
    int                       m_iCursorOverCurveIndex; // Actual index of original drag point
	PointConstraint           *m_opCursorOverCurveNormalConstraint, *m_opCursorOverCurvePointConstraint; /// Curve point/normal constraints
	R3Matrix                  m_matAlignNormalConstraint; /// Data for rotating normal constraint. Matrix for rotating around tangent vector.
	double                    m_dAngAtStartNormalConstraint; /// What angle the normal vector starts at
    int                       m_iCursorOverInflationSrf; /// Infation surface hot spots (curve ends, surface) 
    InflationSurface::EditAction m_inflationSrfEditAction; /// Which state we're in (drawing left/right boundary, drawing on surface)
	bool                      m_bCursorOverLastStroke, m_bCursorOverLastSmooth; /// Last stroke or smoothing circles
    int                       m_iCursorOverShadowBox; /// Shadow box corners, edges, drawing plane corners, edges
	/// Picking an axis. The vectors are created by rotating the canonical axes
    int                       m_iAlignCamera;  /// Which of the six arrows is currently selected
	std::vector<R4Matrix>     m_amatAlignAxis; /// The six matrices that represent the rotation of the six canonical axes
    R3Vec                     m_vecAlignCameraLook, m_vecAlignCameraUp; /// Which matrices/arrows best correspond to the current look/up vectors
    /// Dragging, rotating, and scaling parameters. First is set on mouse up after drag selection,
    ///   second is set on a mouse down,
    ///   last four are set on mouse move when dragging is active
    R3Pt                      m_ptRotateScaleFixed; // 3D fixed point of drag
    int                       m_iPtIndexRotateScale; // Index of point closest to click for rotate-scale
    std::pair<R2Pt, R2Pt>     m_ptRotateScale; // Click point and fixed point
    double                    m_dTDragClick; // T-point of drag-click
    pair<Action, ShadowBox::Surface> m_actionDragClick; // Drag, rotate-scale, scale, rotate, or no action (if cursor off curve)
	bool                      m_bIsPerpendicularDrag; /// If they clicked at the bottom/top of the drag icon, go in/out of the screen and left/right
	
    bool m_bCheckConsistency;
    
	/* Other geometry in the scene. */
    ShadowBox        m_shadowbox;   /// Box and drawing plane
    InflationSurface m_srfInflation;   /// Inflation and extrusion surface for drawing on
    SurfaceData      m_srfData;        /// Finds curve-curve intersections. Generates the surface.
    Webbing          m_srfWeb;
    CurveNetwork     m_crvNetwork;     /// The actual curves
    Array< Array<int> > m_aaiPatches;
    Array< Array<bool> > m_aabReversePatch;
    Array<PatchSurface *> m_aopPatchSurfaces;
	
	
    /* Logging of actions for playback */
    string   m_strDataDir, m_strLoggingDir;
    ofstream m_outLogFile;
    ifstream m_inLogFile;

	// For printing menus
	static char s_astrMenu[NUM_MENU][60], s_astrAction[NUM_ACTIONS+1][50], s_astrSelection[NUM_SELECTION_TYPES+1][50];
	
	/* Undo stack In ActionEvents.cpp. Keeps curve (if any) and action performed */
    Array< std::pair< R2Pt_i, StrokeHistory::MergeType > > m_aUndo;
    void PushUndo( const int in_iCrv, const StrokeHistory::MergeType in_action );

	/* Menu options. */
	void SetDrawingPlaneTwoPoints(); // Set the drawing plane from two points on a curve
	void SetDrawingPlaneBestFit();	 // Pick the best drawing plane based on one point of the curve
    void CenterCameraRegion();       // For lasso/region selection on the camera
	
	/// Called internally to open up the log file
	void StartLogging();
	/// Called internally to close the log file
	void EndLogging();
    /// put the current action and state into the gui debug boxes
	void UpdateGUIState() const;  

	/* Dragging and snapping curves. In ActionEvents_Curve.cpp */
    void StartDrag(); // Set the indices of the drag (m_iCursorOverCurveTs, m_iCursorOverCurveIndex).
    void EndDrag();   // Reset the t values from the indices (m_iCursorOverCurveTs, m_iCursorOverCurveIndex).
    void MoveCurve( const R2Pt &in_ptScreen, const R3Ray &in_ray ); 
    void RotateCurve( const R2Pt &in_ptScreen ); 
    void RotateScaleCurve( const R2Pt &in_ptScreen ); 
    void ScaleCurve( const R2Pt &in_ptScreen, const R3Ray &in_ray ); 
    pair<double,double> AdjustSnapTValues() const;
	void SnapCurvesTogether();    
    
    /* Curve selection routines. Only the first five are used currently; the others are legacy. In ActionEvents_Curve.cpp  */
    double CurveSelected( const R3Ray &in_ray, int &out_iWhich, double &out_dT ) const; /// Pick closest, non-hidden curve
	void SetCursorOverTs( ); // Given the current selected value, update the selected region
	void SelectedCurveRegion( const R3Ray &in_ray ); /// Update the current point and selected region. Calls SetCursorOverTs()
    void SetCurveSelected( const int in_iCurve, const double in_dT ); /// Also set the color correctly
	void ClearCurveSelected(); /// Clear curve selected and region selected
	/// Following four are legacy
    double CurveDrawingWallSelected( const R3Ray &in_ray, int &out_iWhich, double &out_dT ) const;
    double CurveShadowSelected( const R3Ray &in_ray, ShadowBox::Surface &out_srf, int &out_iWhich, double &out_dT ) const;
    // Returns distance. Does curve selection with selected plane
    double CurveSelect( const R3Ray &in_ray, double &out_dT ) const;    
    bool CurveSelectedBest( const R3Ray &in_ray, ShadowBox::Surface &out_srf, int &out_iWhich, double &out_dT ) const;  
    const R3Matrix CurveAlignment() const;
	
	/* Menu stuff, in ActionEvents_Menu.cpp */
	/// Everything but the kitchen sink. Pass in tap (rather than use m_bWasTap) so that it can
	/// be forced to be true/false
    void DoMenuItem( const bool in_bWasTap, const bool in_bWasPassThrough ); // Process the menu option
    MenuSelection SelectMenu( const R2Pt &in_pt ) const; // Figure out which option is selected
    MenuSelection SelectMenuClosest( const R2Pt &in_pt ) const; // Same as above, but force a selection
	void DrawMenuCircle( const DrawState::TextureNames in_shader ) const; // Draw the menu
    
	/* Mouse stuff, in ActionEvents_Mouse.cpp */
	/// Workhorse selection routines. Figures out what is valid to select, clears anything that shouldn't
	/// be selected, and picks the closest selected thing
    void SetSelectionState( const R2Pt &in_ptScreen, const R3Ray &in_ray );
	/// For activating camera/draw plane controls while in drag/intersection surface
	void ActivateHotspot( const R2Pt &in_ptScreen );

	/* Normals and alignment, in ActionEvents_Align.cpp */
	/* These two methods handle creating the 6 vectors and then figuring out which one was selected  */
    void CreateAxis( const R2Pt &in_ptScreen, const R3Ray &in_ray );
    void BestAxis( const R2Pt &in_ptScreen );
	
	/* These two routines handle rotating the curve normal constraint. One for intersection normals, one for curve normal constraints */
	void CalcGeometryAtStartNormalConstraint(); // Rotate normal around in plane perpendicular to tangent
	R3Vec CalcNewNormalConstraint( const R2Pt &in_ptScreen ) ;
	/// This one handles the surface intersection normal (full spherical rotation)
	R3Vec CalcNewIntersectionNormal( const R2Vec &in_vecScreen ) const;
	
	/* Supplementary draw routines in ActionEvents_Draw.cpp */
	void DrawSelectCircle( const R2Pt &in_pt, const UTILSColor &in_color ) const; // Circle around cursor
	void DrawSelectCross( const R2Pt &in_pt, const UTILSColor &in_color ) const; // Cross around cursor
	bool IsOverTopBottomDragIcon( const R2Pt &in_pt ) const;
    
    /// for selecting rotate, scale, scale-rotate, or drag
    Action WhichDragAction( const R3Plane &in_plane, const R2Pt &in_pt, double &out_dT ) const; 
    pair<Action, ShadowBox::Surface>  WhichDragAction( const R2Pt &in_pt, double &out_dT ) const; 
    
    R4Matrix MatrixDragIcon( const double in_dTCrv, const R3Plane &in_plane, const bool in_bIsPerp ) const;
    R4Matrix MatrixRotateScaleIcon( const R3Plane &in_plane, const double in_dTOnCrv ) const;
    void DrawDragIcon( const double in_dTCrv, const R3Plane &in_plane, const bool in_bDrawArrows, const bool in_bIsSelected ) const;
    void DrawRotateScaleIcon( const R3Plane &in_plane, const Action in_action ) const;
    void DrawDragIcon( const double in_dTCrv ) const; 
    void DrawTick( const double in_dTCrv ) const; // boundaries of selected region	
    /// If which curve is active changed or selected point changed, use this to update the colors
	void UpdateCurveSelectionColors( );
	
    
public:
    /**@name Access */
    //@{
    ///
    const ShadowBox &GetShadowBox() const { return m_shadowbox; }
    ///
    ShadowBox &SetShadowBox() { return m_shadowbox; }
    ///
    const ScreenCurve &GetScreenCurve() const { return m_curveTempScreen; }
    ///
    int NumCrvs() const { return m_crvNetwork.NumCrvs(); }
    ///
    const CurveNetwork &GetCurveNetwork() const { return m_crvNetwork; }
    //@}
	
    ///
    void ParamDataChanged();
    
    /**@name Selected Curve and curve regions In ActionEvents_Tick.cpp*/
    //@{
    /// Do we have a curve selected?
    bool OverCurve() const { return m_iCursorOverCurve >= 0 && m_iCursorOverCurve < m_crvNetwork.NumCrvs(); }
    /// Entire curve selected or just a subset?
    bool IsSubset() const; 
    /// Sorted (StartRegion <= EndRegion)  
    double StartRegion() const ;
    /// The actual t value of the selection 
    double TOverCurve() const { return m_dCursorOverCurve; }
    /// 
    double EndRegion() const ;
    ///
    bool IsCursorCloseToActiveCurve( const R3Plane &in_plane, double &out_dT ) const;
    //@}
    
    /**@name What are we doing? In ActionEvents.cpp */
    //@{
	///
	bool IsCameraAction() const;
	///
	bool IsDrawingAction() const;
    //@}
    
    /**@name Curve actions. In ActionEvents_Curve.cpp */
    //@{
    ///
    void Undo();
	/// Delete all
	void Clear();
	/// Un-hide all
	void ShowAll();
	/// Hide all not closed
	void ShowAllClosed();
	/// Hide all
	void HideAll();
	/// Delete stroke history for all
	void FinalizeAll();
    //@}
    
    /**@name Surface actions. In ActionEvents_Surface.cpp */
    //@{
	///
	void CheckForIntersections();
    ///
    void FindIntersections( const double in_d );
	/// Actually build the surface
	void CalcSurface();
	///
	void FixAllIntersections();
	///
	void FixAllIntersectionNormals();
	///
	void FlipSurface();
    ///
    void StartInflationSurface();
    ///
    void StartExtrusionSurface();
    ///
    void MakePatchSurface();
    //@}
    
    /**@name Menu routines, In ActionEvents_Menu.cpp */
	//@{    
	///
	void PrintMenuState() const;
    //@}
    
	/**@name Handle events from GUI (mouse, buttons). In */
    //@{
    ///
    void MouseDown( const R2Pt &in_ptScreen, const R3Ray &in_ray, const int in_eventModifier ) ;
    /// 
    void MouseMove( const R2Pt &in_ptScreen, const R3Ray &in_ray, const int in_eventModifier ) ;
    ///
    void MouseDrag( const R2Pt &in_ptScreen, const R3Ray &in_ray, const int in_eventModifier ) ;
    /// 
    void MouseUp( const int in_eventModifier);
	///
	void SetFocus();
	///
	void SetUnfocus();
    //@}
	
	/**@name Logging. In ActionEvents_logging.cpp */
	//@{
	/// What file are we using?
	const std::string CurrentLogfileName() const;
	/// Log all key strokes
	void Keystroke( const unsigned char in_c, const int in_eventModifier );
	/// Log any change to the camera from the gui (the dials in the upper left )
	void LogCamera();
	/// Print out all state that isn't drawing related
	void LogGUI();
	/// Does a flush and writes out the curves
	void LogFileFlush();
	/// Open up the given log file and start reading actions. Closes the current log file.
	bool StartPlayback( const char * in_str );
	///
	bool InPlayback() const { return m_inLogFile.good(); }
	///
	bool NextStepPlayback();
	//@}
    
    /**@name Drawing. Called from MeshViewer::draw. Mostly in ActionEvents_Draw.cpp */
    //@{
    /// In ActionEvents_Menu.cpp 
    void DrawMenu() const;
    ///
    void DrawShadowBox() const;
    ///
    void DrawDrawingPlane() const;
    ///
    void DrawTicks( ) const;
    ///
    void DrawAlignmentAxes() const;
    ///    
    void DrawCurves(  ) const;
	/// Snap between curves
	void DrawSnapCylinder( ) const;
	///
	void DrawSelectCursor() const;
	///
	void DrawLastStrokeCircles( ) const;
	/// circles for smoothing repeat
	void DrawRepeatSmooth( ) const;
	/// Draw all the m_srfData stuff
    void DrawIntersections() const;
    /// Whatever surfaces we have
    void DrawSurface() const;
    ///
    void DrawConstraints() const;
    /// Draw the intersection surface stuff
    void DrawInflationSurface() const;
    /// Update all curve draw data before drawing
    void UpdateDrawData();
    //@}
    
    ActionEvents();
    ~ActionEvents();
    
	/// Print current action, menu selected, etc
    void PrintState() const;
    
    /// See if log file exists. If it doesn't, make it and ask about consent, data directory
    /// Call after windows are initialized
    void SetupFilesAtStart();
    void DoneConsentForm();
    void WriteProfile( const bool in_bProgramEnding ) ;
    // Actually send the data, if it's ok to send
    void SendData( const bool in_bIsOkToSend ) const;
    // See if we should query to send the data; if so, bring up query box, otherwise, send
    bool QuerySend() const;
    void StartCrashRecovery() ;
    void ReadCrashCurves();
    void UndoCrashRecovery();
    void SetCurvesDirectory( const string &in_str ) ;
    void SetMayaCurvesDirectory( const string &in_str ) ;
    void SetAdobeCurvesDirectory( const string &in_str ) ;
    void SetSurfacesDirectory( const string &in_str ) ;

    const string DataDir() const { return m_strDataDir; }
    
    void Write( ofstream &out ) const;
    void WriteSimple( ofstream &out ) const { m_crvNetwork.WriteSimple(out); }
    void WriteMayaCurves( const string &in_str ) ;
    void WriteAdobeCurves( const string &in_str ) ;
	// Writes in stl format.
	void WriteSTLSurface( const char *in_str ) const { m_srfData.WriteSTLSurface( in_str ); }
	void WriteOBJSurface( const char *in_str ) const;
    void WriteNurbs( const char *in_str ) ;
    /// Write out the curve tubes as off
    void WriteCurvesAsOffFile( const char *in_str );
    void Read( ifstream &in );
    void ReadSimple( const string &in_strFile );
    void ReadPatchMaker( const string &in_strFile );
    void ReadMaya( ifstream &in );
    void ReadSurface( ifstream &in );
    void AddNormalsFromCurves( const string &in_strCrvFile );
};

#endif
