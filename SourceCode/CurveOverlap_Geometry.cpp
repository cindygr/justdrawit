/*
 *  CurveOverlap_Geometry.cpp
 *  SketchCurves
 *
 *  Created by Cindy Grimm on 1/25/11.
 *  Copyright 2011 Adobe Corp.. All rights reserved.
 *
 */


#include "CurveOverlap.h"
#include "Curve.h"
#include "ScreenCurve.h"
#include <OpenGL/OGLObjs_Camera.H>

const bool s_bTrace = false;

// Returns false if there's something drastically wrong, like the curve/projected
// curve lengths are zero, or can't find good screen tangent vectors
bool CurveOverlap::ProjectCurveToScreen( const Curve &in_curve )
{
	if ( in_curve.NumPts() < 2 ) {
		m_overlapType = BAD_CURVE;
		return false;
	}
	
	const OGLObjsCamera &cam = g_drawState.GetCamera();
	
	m_crvProjected.Clear();
	m_adTsMap.resize(0);
	
	// Project the curve onto the screen. Keep track of the depth values.
	m_dDepthProj = 0.0;
	m_dDepthMin = 1e30;
	m_dDepthMax = -1e30;
	for ( int i = 0; i < in_curve.NumPts(); i++ ) {
		const R3Pt ptProj = cam.ProjectedPt( in_curve.Pt(i) );
		m_dDepthProj += ptProj[2];
		m_dDepthMin = WINmin( m_dDepthMin, ptProj[2] );
		m_dDepthMax = WINmax( m_dDepthMax, ptProj[2] );
		m_crvProjected.AddPoint( R2Pt( ptProj[0], ptProj[1] ) );
		
		if ( m_adTsMap.size() < m_crvProjected.NumPts() ) {
			m_adTsMap.push_back( in_curve.PercAlong(i) );
		}
	}
	m_dDepthProj /= (double) in_curve.NumPts();
	
	// Get the ratio of the projected curve to the real one.
	const double dCrvLen = in_curve.Length();
	const double dScreenLen = m_crvProjected.Length();
	if ( RNIsZero( in_curve.Length() ) || RNIsZero( dScreenLen, 1e-6 ) ) {
		m_overlapType = BAD_CURVE;
		return false;
	}
	// The bigger this ratio is, the more parallel the curve is to the screen.
	m_dRatioScreenLengthToCurveLength = dScreenLen / dCrvLen;
	
	return true;
}

bool CurveOverlap::GetScreenTangent( const ScreenCurve &in_curve, const double in_dT, R2Vec &out_vecTang ) const
{
	const double dTStep = g_drawState.ScreenSelectionSize() / in_curve.Length();
	
	double dTTry = dTStep;
	if ( dTStep >= 1.0 ) {
		out_vecTang = in_curve(1.0) - in_curve(0.0);
	} else {
		out_vecTang = R2Vec(0,0);
	}
	while ( RNIsZero( ::LengthSq( out_vecTang ), 1e-6 ) && dTTry < 1.0 ) {
		out_vecTang = in_curve( WINmax(0.0, in_dT + dTTry * 0.5 ) ) - in_curve( WINmin( 1.0, in_dT - dTTry * 0.5 ) );
		dTTry *= 2.0;
	}
	
	if ( RNIsZero( ::LengthSq( out_vecTang ), 1e-6 ) ) {
		return false;
	}
	out_vecTang.Normalize();
	
	return true;
}


// Returns false if there's something drastically wrong, like the 
// stroke length is zero, or can't find good screen tangent vectors
// Overhang calculation: An overhang starts when the t values go from 
//  interior to end (0 or 1)
// Want the longest overhang possible. Start from the end of the curve
//  and walk in. Look for the last transition from interior to end.
// Additional criteria: End of curve must be at least sel distance from curve.
bool CurveOverlap::ProjectStrokeOnScreenCurve( )
{
	const double dDistSel = g_drawState.ScreenSelectionSize();

	// Project onto the screen curve
	m_adDistToCurve.resize( m_crvStroke.NumPts() );
	m_adTsOnCurve.resize( m_crvStroke.NumPts() );
	
	/* Project each stroke point onto the curve. Keep track of
	 * both the t value and the dist.
	 * Keep track of the minimum and maximum t values found on the curve
	 * Record when the stroke starts and stops projecting onto the curve (what's the overhang, if any?)
	 */
	m_dTMin = 1e30;
	m_dTMax = -1e30;
	for ( int i = 0; i < m_crvStroke.NumPts(); i++ ) {
		m_adTsOnCurve[i] = m_crvProjected.ClosestPointTValue( m_crvStroke.Pt(i), m_adDistToCurve[i] );
		
		m_dTMin = WINmin( m_dTMin, m_adTsOnCurve[i] );
		m_dTMax = WINmax( m_dTMax, m_adTsOnCurve[i] );
		
		if ( s_bTrace ) {
			if ( m_adDistToCurve[i] < dDistSel ) cout << "1";
			else                                 cout << "0";
		}
	}
	if ( s_bTrace ) cout << "\n";

	return true;
}

bool CurveOverlap::SetGeometryData( const Curve &in_curve )
{
	m_bCurveClosed = in_curve.IsClosed();
	
	if ( ProjectCurveToScreen( in_curve ) == false ) return false;
	if ( ProjectStrokeOnScreenCurve() == false ) return false;
	
	const double dDistSel = g_drawState.ScreenSelectionSize();
    SetOverlapData( m_strokeEnd1, START_CURVE, 1.5 * dDistSel );
    SetOverlapData( m_strokeEnd2, END_CURVE, 1.5 * dDistSel );
	SetJoinData();
    
	if ( s_bTraceTree == true ) {
		m_strokeEnd1.Print();
		m_strokeEnd2.Print();
		m_strokeJoinEnd1.Print();
		m_strokeJoinEnd2.Print();
	}
	
	ConvertOriginalCurveTValues();
	
	return true;
}
