/*
 *  CurveDrawData.h
 *  SketchCurves
 *
 *  Created by Cindy Grimm on 3/8/11.
 *  Copyright 2011 AdobeCorp. All rights reserved.
 *
 */

#ifndef _CURVE_DRAW_DATA_H
#define _CURVE_DRAW_DATA_H

#include "DrawData.h"

class Curve;
class CurveRibbon;

/*
 * Calculate, and store, the geometry used to make the different renderings
 * of the curves
 *
 * Lazy evaluation; data only changes on one of the Update calls. The
 * screen-space ribbons need to change if the camera changes. The geometry
 * only changes if the curve itself (or its normals) changes. The color
 * may change based on state change (hidden to not, active to not)
 *
 * DrawData reads and stores the param data file information, so if that
 * file is re-read, need to update the local params here (and recalc the curves)
 */
class CurveDrawData : public DrawData
{
public:
    class WebbingSplit {
    public:
        std::pair<int,int> m_iBracketPt;
        
        int Start() const { return m_iBracketPt.first; }
        int End() const { return m_iBracketPt.second; }
        int Mid() const { return m_iBracketPt.first + (m_iBracketPt.second - m_iBracketPt.first) / 2; }
        int NumPts() const { return (m_iBracketPt.second - m_iBracketPt.first)  - 1; }
        
        WebbingSplit() : m_iBracketPt(0,0) {}
        ~WebbingSplit() {}
    };
    
private:
    // The curve
    const CurveRibbon &m_crv;
    
	// Which region is selected
	std::pair<double,double> m_dTOnCrv; // What part of the curve is selected (if any)
	
	/* Store colors, t values for texture mapping, and scale values.
     * Only updated when Update called
     * Changed by:
     *  State change (hidden/active): changes color
     *  Point constraints: Changes color and scale (pinches in at pin)
     *  Camera zoom factor: Changes scale
     *  Strip width in GUI: Changes scale
     *  Geometry changes: Changes t values
     */
	std::vector<R4Pt_f> m_afCol;
	std::vector< double > m_adScale;
	
	/* Geometry-specific data for the different draw types
     * This is not kept up to date; only updated when drawing type is active
     * Changed by:
     *   Point constraints: Scales change for all types
     *   Camera orientation: Changes top/bottom for view-facing strip, shadows
     *   Camera zoom: Changes scales for all types
     *   Strip width in GUI: Changes scale for everyone
     *   Geometry changes: Changes all geometry for everyone
     */
	std::vector< R3Pt > m_aptTopStrip, m_aptBotStrip; // 2D view-facing strip
	std::vector< R3Pt > m_aaptTopShadow[3], m_aaptBotShadow[3], m_aaptMidShadow[3]; // Shadow strip
	std::vector< std::vector<R3Pt> >  m_aaptTubeStrip;  // Tube 
	std::vector< std::vector<R3Vec> > m_aavecTubeStripNorm;  // Tube 
	std::vector< std::vector<R3Pt> > m_aaptRibbonStrip;  // Ribbon 
	std::vector< std::vector<R3Vec> > m_aavecRibbonStripNorm;  // Ribbon 
    std::vector< WebbingSplit > m_aiWebbingSplits, m_aiWebbingDraw; // Webbing;
    std::vector< double > m_adWebbingScales;
	std::vector< std::vector<R3Pt> > m_aaptWebbingStrip;  // Webbing 
	std::vector< std::vector<R3Vec> > m_aavecWebbingStripNorm;  // Webbing 
    bool m_bUpdateView, m_bUpdateTube, m_bUpdateRibbon, m_bUpdateWebbingSplits, m_bUpdateWebbing, m_bUpdateShadow;

	// Just the colors
    void AddColorRamp( const double in_dLeft, const double in_dRight, const UTILSColor &in_col );
	void UpdateColors( const Array<double> &in_adPins );
	// Just the scales
    void AddScaleRamp( const double in_dT );
	void UpdateScales( const Array<double> &in_adPins );
	/// Camera/gui may have changed - update curve's screen space vectors. May update all if width/zoom has changed
    void WorldStateUpdate( const bool in_bWidthSame, const bool in_bHeightSame, const bool in_bViewSame );
	// The tube/ribbon/strip geometry
    void UpdateStrip();
    void UpdateTube();
    void SetWebbingSplits( const Array<double> &in_adPins );
    void UpdateWebbing();
    void UpdateRibbon();
    void UpdateShadow();
	void UpdateGeometry( );
    
    /* Just spit out geometry, some of which is stored in m_drawData */
    void DrawPoints( ) const;
	void DrawLine( ) const;
	void DrawViewFacingStrip( ) const;
	void DrawRibbon( ) const;
	void DrawWebbing( ) const;
	void DrawTube( ) const;
	void DrawVectors() const;
	
    CurveDrawData &operator=( const CurveDrawData &in_data );
    CurveDrawData( const CurveDrawData & );
	
public:
	/**@name Access for the Curve draw stuff that is not handled here */
	//@{
    ///
    const CurveRibbon &GetCurve() const { return m_crv; }
    ///
    int NumWebbingSplits( ) const { return m_aiWebbingSplits.size(); }
    ///
    const WebbingSplit &GetWebbingSplit( const int in_iWhich ) const { return m_aiWebbingSplits[in_iWhich]; }
    ///
    double GetScaling( const int in_iPt ) const { return m_adScale[in_iPt]; }
    ///
    bool NeedsColorUpdate() const { return m_bUpdateColor; }
    ///
    const R4Pt_f &GetColor( const int in_iPt ) const { return m_afCol[ (in_iPt + m_afCol.size()) % m_afCol.size() ]; }
    /// For both of these, the tube starts at the x axis and goes around, so 0..s_iTubeDivs/2 is the top half
    /// Rotation around z axis
    static const R3Matrix &WebbingRotateNormal( const int in_iAroundTube ); 
    /// Rotation around z axis
    static const R3Matrix &WebbingRotate( const int in_iAroundTube ); 
    ///
    static double WebbingScaling( const int in_iAroundTube );
	//@}
	
	/**@name Might need to update */
	//@{
    /// Just set shadow state
    void SetShadow( const bool in_bShadow );
	/// Color may need to be changed (selected region is yellow)
	void SetSelectionRegion( const std::pair<double,double> &in_dTOnCrv );
	/// Sets colors
	void ClearSelectionRegion( );
    /// Sets geometry
    void GeometryChanged();
    ///
    void ForceUpdate();
    
	/// Call just before drawing to make sure everything is up to date
    void Update( const Array<double> &in_adPins );
	//@}
    
    
	/**@name Drawing routines. In CurveDrawData_Draw */
	//@{
	/// Draw with given style in g_drawState
    void Draw( ) const;
	/// Draw as a shadow (projected and black)
    void DrawShadow( ) const;
	/// 2D stroke shading
	void DrawAsFragment( ) const;
    ///
    void TubeGeometry( std::vector<R3Pt> &io_aptVs, std::vector<R3Vec> &io_avecVNs, std::vector< R3Pt_i > &io_aptFs ) const;
	/// 2D stroke shading
	static void DrawAsFragment( const Curve &in_crv );
	/// Boundary curve of inflation surface
    static void DrawAsBoundary( const Curve &in_crv ) ;
	//@}
    
	CurveDrawData( const CurveRibbon &in_crv );
	~CurveDrawData() {}
};

#endif