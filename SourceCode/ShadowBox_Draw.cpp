/*
 *  ShadowBox_Draw.cpp
 *  SketchCurves
 *
 *  Created by Cindy Grimm on 9/7/10.
 *  Copyright 2010 Washington University in St. Louis. All rights reserved.
 *
 */

#include <FL/GL.H>
#include <utils/Rn_Line_seg.H>

#include "ShadowBox.h"
#include "DrawState.h"
#include "UserInterface.h"
#include <OpenGL/OGLDraw3D.H>

static float s_afColButton[3] = { 0.6f, 0.6f, 0.3f };
static float s_afColButtonSel[3] = { 1.0f, 0.4f, 0.4f };

/* Draw a square, lower left at in_pt */
void ShadowBox::DrawSquare( const R3Pt &in_pt, const R3Vec &in_vecX, const R3Vec &in_vecY ) const
{
    glVertex3dv( &in_pt[0] );
    glVertex3dv( &(in_pt + in_vecX)[0] );
    glVertex3dv( &(in_pt + in_vecX + in_vecY)[0] );
    glVertex3dv( &(in_pt + in_vecY)[0] );
}

/* Draw a filled in square, colored based on whether or not the element is selected */
void ShadowBox::DrawSelectSquare( const R3Pt &in_pt, const R3Vec &in_vecX, const R3Vec &in_vecY, const bool in_bCursorOver ) const
{
    glBegin( GL_QUADS );
    glColor3fv( in_bCursorOver ? s_afColButtonSel : s_afColButton );
    DrawSquare( in_pt, in_vecX, in_vecY );
    glEnd();

    glLineWidth( in_bCursorOver ? 2.0f : 1.0f );

    glBegin( GL_LINE_LOOP );
    glColor3fv( in_bCursorOver ? s_afColButtonSel : s_afColButton );        
    DrawSquare( in_pt, in_vecX, in_vecY );
    glEnd();
}

/* Draw the spheres at the corners of the shadow box. Lower left corner is an arrow */
void ShadowBox::DrawBoxCorners( const int in_iBoxCorner ) const
{
	const double dRad = m_dSizeCorners * g_drawState.PixelWidth();

	static std::vector<R3Pt> s_aptCorner(5);
	s_aptCorner[0] = m_ptCorner + m_vecX;
	s_aptCorner[1] = m_ptCorner + m_vecY;
	s_aptCorner[2] = m_ptCorner + m_vecX + m_vecY;
	s_aptCorner[3] = m_ptCorner + m_vecY + m_vecZ;
	s_aptCorner[4] = m_ptCorner + m_vecX + m_vecZ;
	
	for ( int i = 0; i < s_aptCorner.size(); i++ ) {
		if ( in_iBoxCorner == i ) {
			glColor3fv( s_afColButtonSel );
		} else {
			glColor3fv( s_afColButton );
		}
		OGLDrawSphere( R3Sphere( s_aptCorner[i], dRad ) );
	}
	
	if ( in_iBoxCorner == 5 ) {
		glColor3fv( s_afColButtonSel );
	} else {
		glColor3fv( s_afColButton );
	}

	const double dHeight = m_dSizeAxis * g_drawState.PixelWidth();
	const R3Vec vecBack = UnitSafe( m_vecZ ) * ( dHeight / 2.0 );
	const R4Matrix matRot = (R4Matrix) R3Matrix( UnitSafe( m_vecX ), UnitSafe( m_vecY ), UnitSafe( m_vecZ ) );

	const R4Matrix matZ = R4Matrix::Translation( (m_vecZ - vecBack) + (m_ptCorner - R3Pt(0,0,0)) ) * matRot * (R4Matrix) R3Matrix::Rotation(0, -M_PI / 2.0);
	
	glPushMatrix();
	glMultMatrixd( & matZ(0,0) );
	OGLDrawArrow( R3Pt(0,0,0), dHeight, dRad );
	glPopMatrix();
	
}

/* Draw a grid on the floor and left wall */
void ShadowBox::DrawGrid() const
{
	const double dDivs = g_drawState.m_opUI->m_dGridSpacing->value();
	const double dLenX = ::Length( m_vecX );
	const double dLenY = ::Length( m_vecY );
	const double dLenZ = ::Length( m_vecZ );
	const double dMaxLen = WINmax( dLenZ, WINmax( dLenY, dLenX ) );
	const double dSpacing = dMaxLen / dDivs;
	
	glLineWidth( (GLfloat) 1.5f );
	glColor4f( 0.4f, 0.4f, 0.4f, 1.0f );
	
	glBegin( GL_LINES );
	glNormal3dv( &m_vecZ[0] );
	const R3Vec vecX( UnitSafe( m_vecX ) * dSpacing );
	const R3Vec vecY( UnitSafe( m_vecY ) * dSpacing );
	const R3Vec vecZ( UnitSafe( m_vecZ ) * dSpacing );
	R3Pt ptE1( m_ptCorner + vecX ), ptE2( m_ptCorner + vecX + m_vecZ );
	R3Pt ptE3( m_ptCorner + vecX + m_vecY );
	for ( double dX = dSpacing; dX < dLenX; dX += dSpacing ) {
		glVertex3dv( &ptE1[0] );
		glVertex3dv( &ptE2[0] );
		//glVertex3dv( &ptE1[0] );
		//glVertex3dv( &ptE3[0] );
		ptE1 += vecX;
		ptE2 += vecX;
		ptE3 += vecX;
	}
	
	ptE1 = R3Pt( m_ptCorner + vecY );
	ptE2 = R3Pt( m_ptCorner + vecY + m_vecZ );
	ptE3 = R3Pt( m_ptCorner + vecY + m_vecX );
	for ( double dY = dSpacing; dY < dLenY; dY += dSpacing ) {
		glVertex3dv( &ptE1[0] );
		glVertex3dv( &ptE2[0] );
		//glVertex3dv( &ptE1[0] );
		//glVertex3dv( &ptE3[0] );
		ptE1 += vecY;
		ptE2 += vecY;
		ptE3 += vecY;
	}
	
	ptE1 = R3Pt( m_ptCorner + vecZ );
	ptE2 = R3Pt( m_ptCorner + vecZ + m_vecY );
	ptE3 = R3Pt( m_ptCorner + vecZ + m_vecX );
	for ( double dZ = dSpacing; dZ < dLenY; dZ += dSpacing ) {
		glVertex3dv( &ptE1[0] );
		glVertex3dv( &ptE2[0] );
		glVertex3dv( &ptE1[0] );
		glVertex3dv( &ptE3[0] );
		ptE1 += vecZ;
		ptE2 += vecZ;
		ptE3 += vecZ;
	}
	glEnd();
	
}

/* Draw a grid on the drawing plane */
void ShadowBox::DrawGridDrawingPlane() const
{
	const double dDivs = g_drawState.m_opUI->m_dGridSpacing->value();
	const double dLenX = 2.0 * ::Length( m_vecDrawingPlaneX );
	const double dLenY = 2.0 * ::Length( m_vecDrawingPlaneY );
	const double dMaxLen = WINmax( dLenY, dLenX );
	const double dSpacing = dMaxLen / dDivs;
	
	glLineWidth( (GLfloat) 1.5f );
	glColor4f( 0.4f, 0.4f, 0.4f, 1.0f );
	
	glBegin( GL_LINES );
	glNormal3dv( &m_vecPlaneNorm[0] );
	const R3Vec vecX( UnitSafe( m_vecDrawingPlaneX ) * dSpacing );
	const R3Vec vecY( UnitSafe( m_vecDrawingPlaneY ) * dSpacing );
	R3Pt ptE1( m_ptDrawingPlaneCenter - m_vecDrawingPlaneX - m_vecDrawingPlaneY + vecX );
	R3Pt ptE2( m_ptDrawingPlaneCenter - m_vecDrawingPlaneX + m_vecDrawingPlaneY + vecX );
	for ( double dX = dSpacing; dX < dLenX; dX += dSpacing ) {
		glVertex3dv( &ptE1[0] );
		glVertex3dv( &ptE2[0] );
		ptE1 += vecX;
		ptE2 += vecX;
	}
	
	ptE1 = R3Pt( m_ptDrawingPlaneCenter - m_vecDrawingPlaneX - m_vecDrawingPlaneY + vecY );
	ptE2 = R3Pt( m_ptDrawingPlaneCenter + m_vecDrawingPlaneX - m_vecDrawingPlaneY + vecY );
	for ( double dY = dSpacing; dY < dLenY; dY += dSpacing ) {
		glVertex3dv( &ptE1[0] );
		glVertex3dv( &ptE2[0] );
		//glVertex3dv( &ptE1[0] );
		//glVertex3dv( &ptE3[0] );
		ptE1 += vecY;
		ptE2 += vecY;
	}
	glEnd();
	
}

/* Draw the box. There's some attempt to use polygon offset to prevent depth
 * aliasing effects, but it doesn't seem to work spectacuarly well
 */
void ShadowBox::Draw( const int in_iSelected ) const
{

	g_drawState.StartShader( DrawState::BOX_SHADING );
    glDepthFunc( GL_LEQUAL );
	
    glEnable( GL_POLYGON_OFFSET_FILL );
    glPolygonOffset( 1.0f, 1.0f );
	
    // 3 walls 
    glBegin( GL_QUADS );
	glColor4f( 0.75f, 0.75f, 0.75f, 1.0f );
	glNormal3dv( &m_vecZ[0] );
    DrawSquare( m_ptCorner, m_vecX, m_vecY );
    
	glColor4f( 0.5f, 0.5f, 0.5f, 1.0f );
	glNormal3dv( &m_vecX[0] );
    DrawSquare( m_ptCorner, m_vecY, m_vecZ );

	glColor4f( 0.6f, 0.6f, 0.6f, 1.0f );
	glNormal3dv( &m_vecY[0] );
    DrawSquare( m_ptCorner, m_vecZ, m_vecX );
    
    glEnd();

    glDisable( GL_POLYGON_OFFSET_FILL );
	if ( g_drawState.m_opUI->m_bShowGrid->value() ) {
		DrawGrid();
	}
	
	// The two little squares sticking out the bottom
	const double dSelSize = m_dSizeSquare * g_drawState.PixelWidth();
	const R3Vec vecShiftX = UnitSafe( m_vecX ) * dSelSize;
	const R3Vec vecShiftZ = UnitSafe( m_vecZ ) * dSelSize;

	DrawSelectSquare( m_ptCorner + m_vecZ + vecShiftX * 0.5 + vecShiftZ * 0.5, vecShiftX, vecShiftZ, in_iSelected == 20 );
	DrawSelectSquare( m_ptCorner + m_vecZ + m_vecX - vecShiftX * 1.5 + vecShiftZ * 0.5, vecShiftX, vecShiftZ, in_iSelected == 21 );
	
	g_drawState.EndShader( );
	
	// The spheres at the corners and the cylinders at the edges
	g_drawState.StartShader( DrawState::SURFACE_SHADING );
	DrawBoxCorners( in_iSelected - 4 );
	DrawBoxAxis( in_iSelected - 10 );
	g_drawState.EndShader( );
}

inline void DrawCylinder( const R3Pt &in_pt, const R3Vec &in_vec, const double in_dLen, const bool in_bSel ) 
{
	R3Matrix matRot;
	R3Matrix::MatrixVecToVec( matRot, R3Vec(0,1,0), UnitSafe( in_vec ) );
	const R4Matrix mat = R4Matrix::Translation( in_pt ) * (R4Matrix) matRot;

	glColor3fv( in_bSel ? s_afColButtonSel : s_afColButton );
	
	glPushMatrix();
	glMultMatrixd( & mat(0,0) );
	OGLDrawCylinder( R3Pt(0,-0.5 * in_dLen,0), in_dLen, in_dLen * 0.25 );
	glPopMatrix();
}

/* Draw the drawing plane */
void ShadowBox::DrawDrawingPlane( const int in_iCorner ) const
{
	// Drawing plane itself is transparent 
	g_drawState.StartShader( DrawState::BOX_SHADING );	
    glShadeModel( GL_FLAT );
    glDepthFunc( GL_LEQUAL );
    glEnable( GL_POLYGON_OFFSET_FILL );
    glPolygonOffset( 1.0f, 1.0f );
    
    const double dSclDrawingPlane = 1.0;
    const R3Vec vecDrawX = m_vecDrawingPlaneX * dSclDrawingPlane;
    const R3Vec vecDrawY = m_vecDrawingPlaneY * dSclDrawingPlane;

    const R3Pt ptCorner = m_ptDrawingPlaneCenter - vecDrawX - vecDrawY;
    
    glBegin( GL_QUADS );
    
    // Main plane
    glColor4f(0.7f, 0.7f, 0.8f, 0.6f);
    glNormal3dv( &m_vecZ[0] );
    DrawSquare( ptCorner, vecDrawX * 2.0, vecDrawY * 2.0 );
    glEnd();    

    glDisable( GL_POLYGON_OFFSET_FILL );
    
    // Outline Drawing plane
    glLineWidth( 2.0f);
    
    
    glBegin( GL_LINE_LOOP );
    
    glColor4f(0.2f, 0.2f, 0.3f, 0.8f);
    DrawSquare( ptCorner, vecDrawX * 2.0, vecDrawY * 2.0 );
    
    glEnd();

    // four corners
	const double dSelSize = m_dSizeSquare * g_drawState.PixelWidth();
	const R3Vec vecShiftRight = UnitSafe( m_vecDrawingPlaneX ) * dSelSize;
	const R3Vec vecShiftUp = UnitSafe( m_vecDrawingPlaneY ) * dSelSize;
	
    DrawSelectSquare( ptCorner,                                                                vecShiftRight, vecShiftUp, in_iCorner == 0 ? true : false );
    DrawSelectSquare( ptCorner + 2.0 * vecDrawX - vecShiftRight,                               vecShiftRight, vecShiftUp, in_iCorner == 1 ? true : false );
    DrawSelectSquare( ptCorner + 2.0 * vecDrawX - vecShiftRight + 2.0 * vecDrawY - vecShiftUp, vecShiftRight, vecShiftUp, in_iCorner == 2 ? true : false );
    DrawSelectSquare( ptCorner + 2.0 * vecDrawY - vecShiftUp,                                  vecShiftRight, vecShiftUp, in_iCorner == 3 ? true : false );
	
	g_drawState.EndShader( );

	g_drawState.StartShader( DrawState::SURFACE_SHADING );	
	glMatrixMode( GL_MODELVIEW );

	// Four rotate bars on draw plane
    DrawCylinder( m_ptDrawingPlaneCenter + vecDrawX, vecDrawY, dSelSize, in_iCorner == 16 ? true : false );
    DrawCylinder( m_ptDrawingPlaneCenter - vecDrawX, vecDrawY, dSelSize, in_iCorner == 17 ? true : false );
    DrawCylinder( m_ptDrawingPlaneCenter + vecDrawY, vecDrawX, dSelSize, in_iCorner == 18 ? true : false );
    DrawCylinder( m_ptDrawingPlaneCenter - vecDrawY, vecDrawX, dSelSize, in_iCorner == 19 ? true : false );
	
	g_drawState.EndShader( );
	
	if ( g_drawState.m_opUI->m_bShowGridDrawPlane->value() ) {
		DrawGridDrawingPlane();
	}
}

inline void DrawCylinderX( const R3Pt &in_pt, const R4Matrix &in_matRot, const double in_dLen, const double in_dRad )
{
	const R4Matrix matX = R4Matrix::Translation( in_pt - R3Pt(0,0,0) ) * in_matRot * (R4Matrix) R3Matrix::Rotation(2, M_PI / 2.0);
	
	glPushMatrix();
	glMultMatrixd( & matX(0,0) );
	OGLDrawCylinder( R3Pt(0,0,0), in_dLen, in_dRad );
	glPopMatrix();
}

inline void DrawCylinderY( const R3Pt &in_pt, const R4Matrix &in_matRot, const double in_dLen, const double in_dRad )
{
	const R4Matrix matY = R4Matrix::Translation( in_pt - R3Pt(0,0,0) ) * in_matRot;
	
	glPushMatrix();
	glMultMatrixd( & matY(0,0) );
	OGLDrawCylinder( R3Pt(0,0,0), in_dLen, in_dRad );
	glPopMatrix();
}

inline void DrawCylinderZ( const R3Pt &in_pt, const R4Matrix &in_matRot, const double in_dLen, const double in_dRad )
{
	const R4Matrix matZ = R4Matrix::Translation( in_pt - R3Pt(0,0,0) ) * in_matRot * (R4Matrix) R3Matrix::Rotation(0, -M_PI / 2.0);
	
	glPushMatrix();
	glMultMatrixd( & matZ(0,0) );
	OGLDrawCylinder( R3Pt(0,0,0), in_dLen, in_dRad );
	glPopMatrix();
}

/* Draw the cylinders along the edges of the shadow box */
const double cdScaleDown = 0.5;
void ShadowBox::DrawBoxAxis( const int in_iAxis ) const
{
	const double dRad = m_dSizeCorners * g_drawState.PixelWidth();
	const double dLenSel = m_dSizeAxis * g_drawState.PixelWidth();
	
	const double dLenX = ::Length( m_vecX );
	const double dLenY = ::Length( m_vecY );
	const double dLenZ = ::Length( m_vecZ );
	
    glMatrixMode( GL_MODELVIEW );
	const R4Matrix matRot = (R4Matrix) R3Matrix( UnitSafe( m_vecX ), UnitSafe( m_vecY ), UnitSafe( m_vecZ ) );

	glColor4f( 0.5f, 0.5f, 0.75f, 1.0f );
	DrawCylinderX( m_ptCorner, matRot, dLenX, dRad * cdScaleDown );
	DrawCylinderX( m_ptCorner + m_vecY, matRot, dLenX, dRad * cdScaleDown );
	DrawCylinderX( m_ptCorner + m_vecZ, matRot, dLenX, dRad * cdScaleDown );
	
	DrawCylinderY( m_ptCorner, matRot, dLenY, dRad );
	DrawCylinderY( m_ptCorner + m_vecX, matRot, dLenY, dRad * cdScaleDown );
	DrawCylinderY( m_ptCorner + m_vecZ, matRot, dLenY, dRad * cdScaleDown );
	
	DrawCylinderZ( m_ptCorner, matRot, dLenZ, dRad );
	DrawCylinderZ( m_ptCorner + m_vecX, matRot, dLenZ, dRad * cdScaleDown );
	DrawCylinderZ( m_ptCorner + m_vecY, matRot, dLenZ, dRad * cdScaleDown );

	const R3Vec vecXBack = UnitSafe( m_vecX ) * (0.5 * dLenSel);
	const R3Vec vecYBack = UnitSafe( m_vecY ) * (0.5 * dLenSel);
	const R3Vec vecZBack = UnitSafe( m_vecZ ) * (0.5 * dLenSel);

	glColor3fv( in_iAxis == 0 ? s_afColButtonSel : s_afColButton );
	DrawCylinderX( m_ptCorner + m_vecZ + m_vecX * 0.5 - vecXBack, matRot, dLenSel, dRad );
	glColor3fv( in_iAxis == 1 ? s_afColButtonSel : s_afColButton );
	DrawCylinderX( m_ptCorner + m_vecY + m_vecX * 0.5 - vecXBack, matRot, dLenSel, dRad );

	glColor3fv( in_iAxis == 2 ? s_afColButtonSel : s_afColButton );
	DrawCylinderY( m_ptCorner + m_vecX + m_vecY * 0.5 - vecYBack, matRot, dLenSel, dRad );
	glColor3fv( in_iAxis == 3 ? s_afColButtonSel : s_afColButton );
	DrawCylinderY( m_ptCorner + m_vecZ + m_vecY * 0.5 - vecYBack, matRot, dLenSel, dRad );
	
	glColor3fv( in_iAxis == 4 ? s_afColButtonSel : s_afColButton );
	DrawCylinderZ( m_ptCorner + m_vecX + m_vecZ * 0.5 - vecZBack, matRot, dLenSel, dRad );
	glColor3fv( in_iAxis == 5 ? s_afColButtonSel : s_afColButton );
	DrawCylinderZ( m_ptCorner + m_vecY + m_vecZ * 0.5 - vecZBack, matRot, dLenSel, dRad );
}
