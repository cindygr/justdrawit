/*
 *  CurveOverlap_OverlapInfo.cpp
 *  SketchCurves
 *
 *  Created by Cindy Grimm on 2/4/11.
 *  Copyright 2011 Washington University in St. Louis. All rights reserved.
 *
 */

#include "CurveOverlap.h"
#include "DrawState.h"

const bool s_bTrace = false;

void CurveOverlap::OverlapInfo::ZeroOut()
{
    m_dAvgDist = 0.0;
    m_dAvgAng = 0.0;
    m_dMaxDist = 0.0;
    m_dMaxAng = 0.0;
    m_dCurveLength = 0.0;
    m_dStrokeLength = 0.0;
    
    m_dCrv.first = m_dCrv.second = 0.5;
    m_dStroke.first = m_dStroke.second = 0.5;
    
    m_crvEnd = m_strokeEnd = NO_ENDS;
    m_bStrokeReversed = false;
}

void CurveOverlap::OverlapInfo::Print() const
{
    cout << "Curve overlap: " << m_dCrv.first << " " << m_dCrv.second;
    cout << " Stroke overlap: " << m_dStroke.first << " " << m_dStroke.second;
    cout << " Dist " << m_dAvgDist << " " << m_dMaxDist;
    cout << " Ang " << m_dAvgAng << " " << m_dMaxAng;
    cout << "\n  Length: " << m_dStrokeLength << " crv " << m_dCurveLength << " reversed " << ( m_bStrokeReversed ? " t " : "f " );
    cout << " Crv end: "; 
    PrintCurveEnd( m_crvEnd );
    cout << " Stroke end: ";
    PrintCurveEnd( m_strokeEnd );
	if ( IsOverlapped() ) {
		cout << " Overlaps ";
	} else {
		cout << " NO overlap ";
	}
	if ( IsMergeEnd() ) {
		cout << " Merge end\n";
	} else {
		cout << "\n";
	}
}

void CurveOverlap::OverlapInfo::ForceOverlapped()
{
	const double dDistSel = g_drawState.ScreenSelectionSize();
    m_dStrokeLength = dDistSel;
    m_dAvgDist = WINmin( m_dAvgDist, dDistSel * 1.1 );
    m_dMaxDist = WINmin( m_dMaxDist, dDistSel * 1.5 );
    m_dAvgAng = WINmin( m_dAvgAng, 3.0 * M_PI / 4.0 );
    m_dMaxAng = WINmin( m_dMaxAng, M_PI / 2.0 );
    m_dCurveLength = WINmin( m_dCurveLength, 2.0 * m_dStrokeLength );
}

bool CurveOverlap::OverlapInfo::IsOverlapped(  ) const
{
	const double dDistSel = g_drawState.ScreenSelectionSize();
    
    if ( m_dStrokeLength < 0.75 * dDistSel && m_dAvgDist > 0.2 * dDistSel ) {
        return false;
    }
    
    if ( m_dStrokeLength < 0.25 * dDistSel && m_dAvgDist > 0.1 * dDistSel ) {
        return false;
    }
    
    if ( RNIsZero(m_dStrokeLength ) ) {
        return false;
    }
    
    if ( m_dAvgDist > 1.1 * dDistSel ) {
        return false;
    }
    
    if ( m_dMaxDist > dDistSel * 1.5 ) {
        return false;
    }
    
    if ( m_dAvgAng > 3.0 * M_PI / 4.0 ) {
        return false;
    }
    
    if ( m_dMaxAng > M_PI / 2.0 ) {
        return false;
    }
    
    if ( m_dCurveLength > 2.0 * m_dStrokeLength ) {
        return false;
    }
	
	return true;
}

bool CurveOverlap::OverlapInfo::IsMergeEnd(  ) const
{
	if ( m_crvEnd == START_CURVE ) {
		if ( m_strokeEnd == START_CURVE && m_bStrokeReversed == false ) {
			return false;
		}
		if ( m_strokeEnd == END_CURVE && m_bStrokeReversed == true ) {
			return false;
		}
	} else if ( m_crvEnd == END_CURVE ) {
		if ( m_strokeEnd == START_CURVE && m_bStrokeReversed == true ) {
			return false;
		}
		if ( m_strokeEnd == END_CURVE && m_bStrokeReversed == false ) {
			return false;
		}
	}
		
    return true;
}

double CurveOverlap::OverlapInfo::Score() const
{
	const double dDistScore = m_dAvgDist / g_drawState.ScreenSelectionSize(); // If overlap region large, discount score
	const double dAngScore = m_dAvgAng / (3.0 * M_PI / 4.0);
	
	if ( s_bTrace ) cout << "Dist " << dDistScore << " ang " << dAngScore << " " << m_dStrokeLength << "\n";
	
	return dDistScore * 0.7 + dAngScore * 0.3;
}

/*
 * Start from one end of the stroke. Walk in. As long as the following criteria are true, keep accumulating points:
 *   1) Stroke and curve tangents are in the same direction
 *   2) Distance between curve and stroke is within clip height
 *   3) Haven't fallen off of end of curve or stroke
 *
 * As soon as that is not true, break and start looking for another overlap region
 * Keep the longest overlap region found
 */
void CurveOverlap::SetOverlapData( OverlapInfo &out_oi, const CurveEndType in_strokeEnd, const double in_dClipHeight ) const
{
    const double dDistSel = g_drawState.ScreenSelectionSize();

    out_oi.m_strokeEnd = in_strokeEnd;
    
    const int iTStartStroke = (in_strokeEnd == START_CURVE) ? 0 : m_crvStroke.NumPts() - 1;
    const int iStepStroke = (in_strokeEnd == START_CURVE) ? 1 : -1;
    
    const double dWalkAlong = WINmin( 3.0 * dDistSel,  0.3 * m_crvStroke.Length() );
    double dSumLen = 0.0;
    out_oi.ZeroOut();
    OverlapInfo oiCurrent;
	R2Vec vecStroke, vecCurve;
    int iReverse = 0, iVisited = 0;
    
    oiCurrent.ZeroOut();
	bool bIsSet = false;
	bool bCrossedEnd = false;
	bool bCrossedJoinPos = false;
	bool bCrossedJoinNeg = false;
	double dOverhang = 0.0;
    for ( int i = iTStartStroke; i >= 0 && i < m_crvStroke.NumPts(); i += iStepStroke ) {
		const bool bCrvTang = GetScreenTangent( m_crvProjected, m_adTsOnCurve[i], vecCurve );
		const bool bStrokeTang = GetScreenTangent( m_crvStroke, m_crvStroke.PercAlong(i), vecStroke );
        const double dDot = Dot( vecStroke, vecCurve );
        double dAng = 0.0;
   
		if ( m_adTsOnCurve[i] > 0.0 && m_adTsOnCurve[i] < 1.0 ) {
			bCrossedEnd = true;
		}
		
		// Looking for overhang amount
		if ( bCrossedEnd == false ) {
			dOverhang = dSumLen;
		}
		// This bit is to handle the case when the stroke is reversed wrt the curve
		// Basically, if we're reversed on the first point, flip all tangent vectors after that
        if ( iVisited == 0 ) {
            dAng = DotToAngle( fabs( dDot ) );
            oiCurrent.m_dCrv.first = m_adTsOnCurve[i];
            oiCurrent.m_dStroke.first = m_crvStroke.PercAlong(i);
            
            if ( dDot < 0.0 ) {
                iReverse++;
            }
        } else {
            dAng = DotToAngle( iReverse > 0 ? -dDot : dDot );
        }
        const double dLen = ::Length( m_crvStroke.Pt(i) - m_crvStroke.Pt( WINminmax( i + iStepStroke, 0, m_crvStroke.NumPts() - 1 ) ) );
        dSumLen += dLen;
        
        if ( bCrvTang && bStrokeTang ) {
            oiCurrent.m_dMaxAng = WINmax( oiCurrent.m_dMaxAng, dAng );
            oiCurrent.m_dAvgAng += dAng;
            oiCurrent.m_dMaxDist = WINmax( oiCurrent.m_dMaxDist, m_adDistToCurve[i] );
            oiCurrent.m_dAvgDist += m_adDistToCurve[i];
			if ( iVisited == 0 ) {
				oiCurrent.m_dStrokeLength = 0.0;
			} else {
				oiCurrent.m_dStrokeLength += dLen;
			}

			if ( bCrossedJoinNeg ) {
				oiCurrent.m_dCrv.second = m_adTsOnCurve[i] - 1.0;
			} else if ( bCrossedJoinPos ) {
				oiCurrent.m_dCrv.second = m_adTsOnCurve[i] + 1.0;
			} else {
				oiCurrent.m_dCrv.second = m_adTsOnCurve[i];
			}
            oiCurrent.m_dStroke.second = m_crvStroke.PercAlong(i);
        }
		
		bool bOffEnd = false;
		if ( m_bCurveClosed ) {
			if ( m_adTsOnCurve[i] < 0.5 && m_adTsOnCurve[WINminmax( i + iStepStroke, 0, m_crvStroke.NumPts() - 1 )] > 0.5 ) {
				bCrossedJoinNeg = true;
			}
			if (m_adTsOnCurve[i] > 0.5 && m_adTsOnCurve[WINminmax( i + iStepStroke, 0, m_crvStroke.NumPts() - 1 )] < 0.5 ) {
				bCrossedJoinPos = true;
			}
		} else {
			if ( m_adTsOnCurve[i] < 0.5 && m_adTsOnCurve[WINminmax( i + iStepStroke, 0, m_crvStroke.NumPts() - 1 )] > 0.5 ) {
				bOffEnd = true;
			}
			if ( m_adTsOnCurve[i] > 0.5 && m_adTsOnCurve[WINminmax( i + iStepStroke, 0, m_crvStroke.NumPts() - 1 )] < 0.5 ) {
				bOffEnd = true;
			}
		}			
			
		if ( oiCurrent.m_dCrv.first < 0.02 && oiCurrent.m_dCrv.second > 0.9 ) {
			cout << "bad\n";
		}
					
        iVisited++;

        if ( !bCrvTang || !bStrokeTang || m_adDistToCurve[i] > in_dClipHeight || dAng > 3.0 * M_PI / 4.0 || dSumLen > dWalkAlong || bOffEnd ) {
            // Start new sequence
			const bool bIsCloser = oiCurrent.m_dAvgDist < out_oi.m_dAvgDist;
			const bool bIsLonger = oiCurrent.m_dStrokeLength > out_oi.m_dStrokeLength;
			const bool bIsZero = RNIsZero( oiCurrent.m_dStrokeLength );
            if ( !bIsSet || (bIsCloser && bIsZero) || (!bIsZero && bIsLonger) ) {
                out_oi = oiCurrent;
                out_oi.m_bStrokeReversed = iReverse > 0;
                out_oi.m_dAvgAng /= (double) iVisited;
                out_oi.m_dAvgDist /= (double) iVisited;
				bIsSet = true;
            }
            oiCurrent.ZeroOut();
            iReverse = iVisited = 0;
			bCrossedJoinNeg = bCrossedJoinPos = false;
        }
        if ( dSumLen > dWalkAlong ) {
            break;
        }
    }
	// TODO: Maybe, if zero length set reverse by an approximate tangent
    
    const double dTE1 = WINmin( out_oi.m_dCrv.first, out_oi.m_dCrv.second );
    const double dTE2 = WINmax( out_oi.m_dCrv.first, out_oi.m_dCrv.second );
    
    if ( dTE1 < 0.25 && dTE2 > 0.75 ) {
        out_oi.m_crvEnd = BOTH_ENDS;
    } else if ( dTE1 < 0.25 ) {
        out_oi.m_crvEnd = START_CURVE;
    } else if ( dTE2 > 0.75 ) {
        out_oi.m_crvEnd = END_CURVE;
    } else {
        out_oi.m_crvEnd = NO_ENDS;
    }
    
    out_oi.m_strokeEnd = in_strokeEnd;
    out_oi.m_dCurveLength = m_crvProjected.Length() * (dTE2 - dTE1);
}
