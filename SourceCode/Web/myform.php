<?php
  if($_POST['submit'] == "send")
  {

    $survey = $_POST['survey'];
    $mouse = $_FILES["mouse"]["tmp_name"];
    $curve = $_FILES["curve"]["tmp_name"];
    $id = $_POST['id'];
    $magic = $_POST['magic'];

    if ($magic == "JustDrawItUISend") {
       $surveyName = "./SketchData/" . $id . "_survey.txt";
       $fp = fopen( $surveyName, "w" );
       fwrite($fp, $survey);
       fclose($fp);
       echo("<p>Writing file " . $surveyName . "\n");

       $mouseName = "./SketchData/" . $id . "_mouse.txt";
       move_uploaded_file($mouse, $mouseName );
       echo("<p>Writing file " . $mouseName . "\n");

       $curveName = "./SketchData/" . $id . "_curve.crv";
       move_uploaded_file($curve, $curveName );
       echo("<p>Writing file " . $curveName . "\n");

    }
    exit;
  }
?>
