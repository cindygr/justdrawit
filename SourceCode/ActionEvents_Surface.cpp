/*
 *  ActionEvents_Surface.cpp
 *  SketchCurves
 *
 *  Created by Cindy Grimm on 5/25/11.
 *  Copyright 2011 __MyCompanyName__. All rights reserved.
 *
 */

#include "ActionEvents.h"
#include "UserInterface.h"

/* Pass on to surface data */
void ActionEvents::CheckForIntersections()
{
    if ( !g_drawState.m_opUI || !g_drawState.m_opUI->m_bShowIntersections->value() ) {
        return;
    }
    m_srfData.FindIntersections( m_crvNetwork, g_drawState.m_opUI->m_dIntersectionSize->value() );
}

void ActionEvents::FindIntersections( const double in_d )
{
	if ( m_outLogFile.good() ) m_outLogFile << "FindIntersections " << in_d << "\n";
    m_srfData.FindIntersections( m_crvNetwork, in_d );
}


void ActionEvents::FixAllIntersections()
{
	if ( m_outLogFile.good() ) m_outLogFile << "FixAllIntersections\n";
	m_srfData.FixAllIntersections( m_crvNetwork );
}

void ActionEvents::FixAllIntersectionNormals()
{
	if ( m_outLogFile.good() ) m_outLogFile << "FixAllIntersectionNormals\n";
    m_crvNetwork.FixAllIntersectionNormals();
}

///
void ActionEvents::FlipSurface()
{
	if ( m_outLogFile.good() ) m_outLogFile << "FlipSurface\n";
	m_srfData.FlipSurface( );
}


void ActionEvents::CalcSurface()
{
	if ( m_outLogFile.good() ) m_outLogFile << "CalcSurface " << g_drawState.m_opUI->m_iNumCubes->value() << "\n";
	m_srfData.CalcConstraints( m_crvNetwork );
    //m_srfWeb.BuildSurface( m_crvNetwork );
}


/* Draw potential and found intersections and intersection normals */
void ActionEvents::DrawIntersections() const
{
    if ( !g_drawState.m_opUI ) {
        return;
    }
    
	if ( g_drawState.m_opUI->m_bShowIntersections->value() ) {    
		m_srfData.Draw( m_iCursorOverIntersection );
	}
	if ( g_drawState.m_opUI->m_bShowConstraints->value() ) {    
		m_srfData.DrawConstraints(  );
        for ( int i = 0; i < m_aopPatchSurfaces.num(); i++ ) {
            m_aopPatchSurfaces[i]->DrawConstraints(  );
        }
	}
}

/* On a click in the checkbox for start inflation surface.
 * Reset state back to drawing the left curve (or clear)
 * If inflation surface is on, then clicking on a curve brings up the arrows for the extrusion surface
 */
void ActionEvents::StartInflationSurface()
{
    // Log the gui first, so that when we call this from a playback, the gui's set right
    LogGUI();
	if ( m_outLogFile.good() ) m_outLogFile << "StartInflationSurface\n";

    if ( g_drawState.m_opUI->m_bShowInflationSrf->value() ) {
        m_action = INFLATION_SURFACE;
        m_inflationSrfEditAction = InflationSurface::EDIT_LEFT_CURVE;
    } else {
        m_action = NO_ACTION;
    }    
	m_srfInflation.Clear();
}

/* On a click in the checkbox for start inflation surface.
 * Reset state back to drawing the left curve (or clear)
 * If inflation surface is on, then clicking on a curve brings up the arrows for the extrusion surface
 */
void ActionEvents::StartExtrusionSurface()
{
    // Log the gui first, so that when we call this from a playback, the gui's set right
    LogGUI();
	if ( m_outLogFile.good() ) m_outLogFile << "StartExtrusionSurface\n";

    if ( g_drawState.m_opUI->m_bShowExtrusionSrf->value() ) {
        m_action = EXTRUSION_SURFACE;
        m_inflationSrfEditAction = InflationSurface::EDIT_LEFT_CURVE;
    } else {
        m_action = NO_ACTION;
    }    
	m_srfInflation.Clear();
}

void ActionEvents::MakePatchSurface()
{
    if ( m_aaiPatches.num() == 0 ) {
        m_aaiPatches.need(1);
        m_aabReversePatch.need(1);
        m_aaiPatches[0].need( m_crvNetwork.NumCrvs() );
        m_aabReversePatch[0].need( m_crvNetwork.NumCrvs() );
        for ( int i = 0; i < m_crvNetwork.NumCrvs(); i++ ) {
            m_aaiPatches[0][i] = i;
            m_aabReversePatch[0][i] = false;
        }
    }
    if ( m_aopPatchSurfaces.num() != m_aaiPatches.num() ) {
        for ( int i = 0; i < m_aopPatchSurfaces.num(); i++ ) {
            delete m_aopPatchSurfaces[i];
        }
        m_aopPatchSurfaces.need(m_aaiPatches.num());
        for ( int i = 0; i < m_aopPatchSurfaces.num(); i++ ) {
            m_aopPatchSurfaces[i] = new PatchSurface( m_crvNetwork );
        }
    }

    Array< pair<double,double> > adCrvPieces( m_crvNetwork.NumCrvs() );
    for ( int iP = 0; iP < m_aaiPatches.num(); iP++ ) {
        adCrvPieces.need( m_aaiPatches[iP].num() );
        for ( int i = 0; i < m_aaiPatches[iP].num(); i++ ) {
            if ( m_aabReversePatch[iP][i] ) {
                adCrvPieces[i] = pair<double,double>(1.0, 0.0);
            } else {
                adCrvPieces[i] = pair<double,double>(0.0, 1.0);
            }
        }
        m_aopPatchSurfaces[iP]->CreatePatch( m_aaiPatches[iP], adCrvPieces );
    }
}

