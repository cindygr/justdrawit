// MeshViewer.cpp: implementation of the MeshViewer class.
//
//////////////////////////////////////////////////////////////////////

#include <FL/GL.H>
#include "MeshViewer.h"
#include "UserInterface.h"
#include <OpenGL/OGLRoutines.H>
#include <OpenGL/OGLRoutines_State.H>
#include <time.h>

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

const bool s_bTrace = false;

void MeshViewer::SetUI( UserInterface *in_opUI ) 
{ 
	m_opUI = in_opUI; 
    g_drawState.m_opUI = in_opUI;
}

void MeshViewer::MakeCylinder()
{
    m_cyl.Create( m_events.GetCurveNetwork() );
}

void MeshViewer::MakeCylinderSurface()
{
    m_cyl.BuildSurface( );
}

void MeshViewer::SetCylinderOffset()
{
    m_cyl.SetCylinderOffset();
}

void MeshViewer::ChangedParamFile()
{
    g_paramFile.ChangedParamFile();
    m_events.ParamDataChanged();
}

void MeshViewer::ChangedParamChoice()
{
    g_paramFile.ChangedParamChoice();
}

void MeshViewer::ReadCurves( const char * in_str )
{
    ifstream in( in_str, ios::in );
    
    if ( in.good() ) {
        m_events.Read(in);        
        in.close();
        m_events.SetCurvesDirectory( in_str );
		
		g_drawState.CenterCamera( m_events.GetCurveNetwork().BoundingBox(), true );
		StartCameraAnimation();
    } else {
        cerr << "ERR: Read curves, File not found " << in_str << "\n";
    }
}

void MeshViewer::ReadCurvesSimple( const char * in_str )
{
    ifstream in( in_str, ios::in );
    
    if ( in.good() ) {
        m_events.ReadSimple( string(in_str) );        
        in.close();
        m_events.SetCurvesDirectory( in_str );
		
		g_drawState.CenterCamera( m_events.GetCurveNetwork().BoundingBox(), true );
		StartCameraAnimation();
    } else {
        cerr << "ERR: Read curves, File not found " << in_str << "\n";
    }
}

void MeshViewer::ReadMayaCurves( const char * in_str )
{
    ifstream in( in_str, ios::in );
    if ( in.good() ) {
        m_events.ReadMaya(in);
        in.close();
		
		g_drawState.CenterCamera( m_events.GetCurveNetwork().BoundingBox(), true );
    } else {
        cerr << "ERR: Read Maya curves, File not found " << in_str << "\n";
    }
}

void MeshViewer::ReadSurface( const char *in_str ) 
{
    ifstream in( in_str, ios::in );
    if ( in.good() ) {
        m_events.ReadSurface(in);
        in.close();
        m_events.SetSurfacesDirectory( in_str );
    } else {
        cerr << "ERR: Read Surface, File not found " << in_str << "\n";
    }
}


void MeshViewer::WriteCurves( const char *in_str )  
{ 
    ofstream out(in_str, ios::out);
    if ( out.good() ) {    
        out.precision(12);
        m_events.Write( out ); 
        out.close();
        m_events.SetCurvesDirectory( in_str );
    } else {
        cerr << "ERR: Write curves, File not found " << in_str << "\n";
    }
    
    string str( in_str );
    const size_t iEnd = str.find_last_of( ".crv" );
    if ( !iEnd == string::npos ) {
        str.replace( iEnd - 4, iEnd, ".txt" );
    }
}


void MeshViewer::WriteSimple( const char *in_str )  
{ 
    ofstream out(in_str, ios::out);
    if ( out.good() ) {    
        out.precision(12);
        m_events.WriteSimple( out ); 
        out.close();
    } else {
        cerr << "ERR: Write curves simple, File not found " << in_str << "\n";
    }
}

void MeshViewer::WriteSTLSurface( const char *in_str ) 
{
	m_events.WriteSTLSurface( in_str );
    m_events.SetSurfacesDirectory( in_str );
}

void MeshViewer::WriteOBJSurface( const char *in_str ) 
{
	m_events.WriteOBJSurface( in_str );
    m_events.SetSurfacesDirectory( in_str );
}

void MeshViewer::WritePatchmaker( const char *in_str ) const
{
	ofstream out( in_str, ios::out );
	if ( !out.good() ) {
		cerr << "ERR: MeshViewer::WritePatchmaker Trying to write to file " << in_str << ": Failed\n";
		return;
	}
	
	const CurveNetwork &crvNw = m_events.GetCurveNetwork();
	out << crvNw.NumCrvs() << "\n";
	for ( int i = 0; i < crvNw.NumCrvs(); i++ ) {
		const Curve &crv = crvNw.GetCurve(i);
		out << crv.NumPts() << "\n";
		for ( int j = 0; j < crv.NumPts(); j++ ) {
			out << crv.Pt(j) << "\n";
		}
		out << "\n";
	}
	out.close();
}

void MeshViewer::WriteCurvesAsOffFile( const char *in_str )
{
    
    SetActionEvents().WriteCurvesAsOffFile(in_str);
}


/* Hack to make sure redraw is called enough times */
int MeshViewer::s_iCountRedraw = 2;

void MeshViewer::Redraw_cb( void *in_data )
{
    MeshViewer *opMe = static_cast<MeshViewer *>( in_data );
	
    if ( opMe->s_iCountRedraw >= 0 ) {
        Fl::repeat_timeout( 0.05, MeshViewer::Redraw_cb, (void *) opMe );
		
        opMe->redraw();
    }
}

void MeshViewer::Redraw()
{
    s_iCountRedraw = 2;
	
    Fl::add_timeout( 0.001, MeshViewer::Redraw_cb, this );
	
    redraw();
}

/* Start camera animation, afterwhich the call back will add another timer */
void MeshViewer::StartCameraAnimation()
{
	s_iCountRedraw = -1;
	
    Fl::add_timeout( 0.001, MeshViewer::CameraAnimation_cb, this );
	
    redraw();
}

void MeshViewer::CameraAnimation_cb( void *in_data )
{
    MeshViewer *opMe = static_cast<MeshViewer *>( in_data );
	
    if ( g_drawState.NextCamera() == false ) {
		const double dNCrvs = WINminmax( opMe->m_events.GetCurveNetwork().NumCrvs() / 10.0, 0.0, 1.0 );
		const double dTimeStep = 0.0001 * dNCrvs + 0.03 * (1.0 - dNCrvs);
        Fl::repeat_timeout( dTimeStep, MeshViewer::CameraAnimation_cb, (void *) opMe );
		
        opMe->Redraw();
	} else {
        // Done with camera animation - log the current camera
        opMe->m_events.LogCamera();
    }
}


MeshViewer::MeshViewer(int X, int Y, int W, int H, const char *L) : 
Fl_Gl_Window(X, Y, W, H, L),
m_opUI(NULL)
{	
    mode( FL_RGB | FL_DEPTH | FL_DOUBLE | FL_ALPHA );

	g_drawState.ResizeCamera( W, H );
    Redraw();
}

MeshViewer::~MeshViewer()
{
    cout << "Quitting\n";
}


void MeshViewer::LeftRight( const double in_d )
{
    g_drawState.LeftRight( in_d );
	
	m_events.LogCamera();
}

void MeshViewer::UpDown( const double in_d )
{
    g_drawState.UpDown( in_d );
	
	m_events.LogCamera();
}


void MeshViewer::SetZoom( const double in_d )
{
    g_drawState.SetZoom( in_d );

	m_events.LogCamera();
}

int MeshViewer::handle(int event) 
{
    const R2Pt ptScreen = g_drawState.GetCamera().FlTkToCamera( Fl::event_x(), Fl::event_y() );
    const R3Ray ray( g_drawState.GetCamera().From(), g_drawState.GetCamera().RayFromEye(ptScreen) );
    
    if ( m_events.InPlayback() && ! m_opUI->m_bPausePlayback->value() ) {
        return 1;
    }
    
    switch(event) {
    case FL_PUSH: {
      m_events.MouseDown( ptScreen, ray, Fl::event_state() );
        //m_events.PrintState();

        Redraw();
        return 1;
        }
    case FL_MOVE : {
        m_events.MouseMove( ptScreen, ray, Fl::event_state() );
		if ( g_drawState.IsCameraAnimation() ) {
			if ( s_bTrace ) cout << "Starting animation call back\n";
			StartCameraAnimation();
		}
		Redraw();
        return 1;
                   }

    case FL_DRAG: {
        m_events.MouseDrag( ptScreen, ray, Fl::event_state() );
        Redraw(); // always redraw
        //m_events.PrintState();
        return 1;
        }
    case FL_RELEASE:   {
      m_events.MouseUp( Fl::event_state() );
        //m_events.PrintState();
		if ( g_drawState.IsCameraAnimation() ) {
			if ( s_bTrace ) {
				if ( s_bTrace ) cout << "Starting animation call back\n";
			}
			StartCameraAnimation();
		}
        Redraw();

        return 1;
        }
		
	case FL_ENTER :
		m_events.SetFocus();
		return 1;
	case FL_LEAVE :
		m_events.SetUnfocus();
		return 1;
    case FL_FOCUS :
    case FL_UNFOCUS :
        // Return 1 if you want keyboard events, 0 otherwise
        return 1; 
           
	case FL_KEYUP: {
        //  a, s, d : toggle drawing walls
		///
        /// Camera:  arrow keys rotatate
        //           x,X,y,Y,z,Z point camera down axis
        //           +- zoom in/out
        ///
        /// Other:   l Turns display list on
	    m_events.Keystroke( Fl::event_key(), Fl::event_state() );
		Redraw();
        return 1;
        }
    case FL_CLOSE : {
        m_events.WriteProfile(true);
        return Fl_Gl_Window::handle(event);
    }
    default:
        // pass other events to the base class...
        return Fl_Gl_Window::handle(event);
    }
}

void MeshViewer::resize( int x, int y, int w, int h )
{
    Fl_Gl_Window::resize( x, y, w, h );
	g_drawState.ResizeCamera( w, h );

	m_events.LogCamera();
    Redraw();
}


