/*
 *  ActionEvents_Tick.cpp
 *  SketchCurves
 *
 *  Created by Cindy Grimm on 9/13/10.
 *  Copyright 2010 Washington University in St. Louis. All rights reserved.
 *
 */


#include "ActionEvents.h"

///
bool ActionEvents::IsSubset() const
{
	return !RNApproxEqual( m_dCursorOverCurveTs.first, m_dCursorOverCurveTs.second );
}

///
double ActionEvents::StartRegion() const 
{ 
	if ( !IsSubset() ) return 0.0;
	return WINmin( m_dCursorOverCurveTs.first, m_dCursorOverCurveTs.second ); 
}
///
double ActionEvents::EndRegion() const 
{ 
	if ( !IsSubset() ) return 1.0;
	return WINmax( m_dCursorOverCurveTs.first, m_dCursorOverCurveTs.second ); 
}


bool ActionEvents::IsCursorCloseToActiveCurve( const R3Plane &in_plane, double &out_dT ) const
{
    if ( !OverCurve() ) return false;
    
    double dDist;
    
    const Curve &crv = m_crvNetwork.GetCurve(m_iCursorOverCurve);
    const Curve crvProject = crv.Project( in_plane );
    crvProject.ClosestPoint( m_rayLast, out_dT, dDist );
    
    if ( IsSubset() ) {
        if ( crv.IsClosed() ) {
        } else {
            out_dT = WINminmax( out_dT, StartRegion(), EndRegion() );
        }
        
        const R3Line line( m_rayLast.Pt(), UnitSafe( m_rayLast.Dir() ) );

        dDist = line.Dist_to_line( crvProject( out_dT ) );
    }
    
    if ( dDist < g_drawState.SelectionSize() ) {
        return true;
    }
    
    return false;
}


