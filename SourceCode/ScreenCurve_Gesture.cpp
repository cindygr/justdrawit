/*
 *  ScreenCurve_Gesture.cpp
 *  SketchCurves
 *
 *  Created by Cindy Grimm on 5/19/11 (copied from ScreenCurve.cpp).
 *  Copyright 2010 Washington University in St. Louis. All rights reserved.
 *
 */


#include "ScreenCurve.h"
#include <Fitting/FITTools_FitLine.H>
#include <Fitting/FITTools_FitEllipse.H>
#include "DrawState.h"
#include "ParamFile.h"

const bool s_bTrace = false;

/* Constants used in determining relative ratios */
const double cdEndScaleFactor = 0.35;
const double cdSizeFactor = 1.0;
const double cdErrFitFactor = 0.1;

/*
 * Legacy
 * Is the stroke a line?
 * Uses:
 * a) The difference between the length of the line and the length of the segment connecting
 *     the end points 
 * b) Fit distance (basically the residual of the least-squares fit)
 * c) The line itself is short enough
 * Note: Some of this info is set by GetGesture, so does not work stand-alone
 */
bool ScreenCurve::IsLine( const double in_dDist ) const
{
    const double dSegLen = m_segGesture.Length();
    
    const double dDiff = WINmax( Length(), dSegLen ) - WINmin( Length(), dSegLen );
    const double dCutoff = dSegLen * 0.2;
    
    if ( s_bTrace ) {
		cout << " Line: seg len " << Length() << " " << dSegLen << "/" << in_dDist * cdSizeFactor;
		cout << " diff " << dDiff << "/" << dCutoff;
		cout << " Line error " << m_dErrLine << "/" << in_dDist * cdErrFitFactor;
	}
    if ( dDiff < WINmin( Length(), dSegLen ) * 0.15 && m_dErrLine < in_dDist * cdErrFitFactor && dSegLen < in_dDist * cdSizeFactor ) {
        if ( s_bTrace ) cout << " Line\n";
        return true;
    }
    if ( s_bTrace ) cout << "\n";
    return false;
}

/*
 * Legacy
 * Is the stroke a circle?
 * Uses:
 * a) Radius must be non-zero
 * b) Distance between end points must be small (ie, closed curve)
 * c) Fit distance to circle
 * Note: Some of this info is set by GetGesture, so does not work stand-alone
 */
bool ScreenCurve::IsCircle( const double in_dDist ) const
{
    const double dEndsLen = ::Length( m_apt[0] - m_apt.last() );
    
    if ( s_bTrace ) {
		cout << " Circle: dEndsLen " << dEndsLen << "/" << in_dDist * cdEndScaleFactor;
		cout << " Circle error " << m_dErrCircle << "/" << in_dDist * cdErrFitFactor;
		cout << " Circle radius " << m_circGesture.Radius();
	}
    if ( dEndsLen < in_dDist * cdEndScaleFactor && m_dErrCircle < in_dDist * cdErrFitFactor && m_circGesture.Radius() < in_dDist * cdSizeFactor ) {
        if ( s_bTrace ) cout << " Circle\n";
        return true;
    }
    if ( s_bTrace ) cout << "\n";
    return false;
}

/*
 * Legacy
 * Is the stroke a squiggle or scratch back and forth
 * Uses:
 * a) Has corners
 * b) Segments between corners largely overlap
 * c) Starts and stops at the end points
 * d) If it looks somewhat like a line
 * Note: Some of this info is set by GetGesture, so does not work stand-alone
 */
bool ScreenCurve::IsSquiggle( const double in_dDist, const boost::dynamic_bitset<> &in_abCorners ) 
{
    /* 
     * Now need to distinguish Squiggle
     * Measure 1: Corner distance to left and right points
     * Measure 2: Fit to line
     */
    const double dBadCorner = 0.2 * m_segGesture.Length();
    const double dBadAvgCorner = 0.15 * m_segGesture.Length();
    const double dBadLine = 0.05 * m_segGesture.Length();
    
    m_dErrSquiggle = 0.0;
    int iBigCorner = 0;
    const R2Pt ptLeft = m_apt[0];
    R2Pt ptRight = m_apt.last();
    
    for ( int i = in_abCorners.find_first(); i != -1; i = in_abCorners.find_next(i) ) {
        if ( ::Length( ptLeft - m_apt[i] ) > ::Length( ptLeft - ptRight ) ) {
            ptRight = m_apt[i];
        }
    }
    for ( int i = 0; i != -1; i = in_abCorners.find_next(i) ) {
        const double dLen = WINmin( ::Length( m_apt[i] - ptRight ), ::Length( m_apt[i] - ptLeft ) );
        if ( dLen > dBadCorner )
            iBigCorner++;
        
        m_dErrSquiggle += dLen;
    }
    m_dErrSquiggle /= (double) ( in_abCorners.count() );
    
    if ( s_bTrace ) cout << " Corner err " << m_dErrSquiggle << "/" << dBadAvgCorner << " " << iBigCorner << "/" << dBadCorner << " " << m_dErrLine << "/" << dBadLine;
    if ( iBigCorner == 0 && m_dErrLine < dBadLine ) {
        if ( s_bTrace ) cout << " Squiggle MC\n";
        return true;
    }
    if ( m_dErrSquiggle < dBadAvgCorner && m_dErrLine < dBadLine ) {
        if ( s_bTrace ) cout << " Squiggle AC\n";
        return true;
    }
    return false;
}

/*
 * Legacy
 * Erase gesture (wiggle up and down)
 * Note: Some of this info is set by GetGesture, so does not work stand-alone
 */
bool ScreenCurve::IsErase( const double in_dDist, const boost::dynamic_bitset<> &in_abCorners ) 
{
    /* Fit a line through the middle of the segments
     * Pretends the gesture consists of a polyline
     */
    Array<R2Pt> aptMid;
    for ( int i = in_abCorners.find_first(); i != m_apt.num() - 1; i = in_abCorners.find_next(i) ) {
        aptMid += R2Line_seg( m_apt[i], m_apt[in_abCorners.find_next(i)] )(0.5);
    }
    const FITTools_FitLine fitMidPoints( aptMid );
    
    /*
     * Make the fitted segment be the fitted line, but a bit longer to ensure it passes through the stroke
     */
    const R2Line_seg &segMid = fitMidPoints.Line_seg2D();
	
    const R2Vec vecDir = UnitSafe( segMid.P2() - segMid.P1() );
    const R2Pt ptProjStart = fitMidPoints.Line2D().Project( m_apt[0] );
    const R2Pt ptProjEnd = fitMidPoints.Line2D().Project( m_apt.last() );
    const double dLenEnd1 = WINmin( ::Length( segMid.P1() - ptProjStart ), ::Length( segMid.P1() - ptProjEnd ) );
    const double dLenEnd2 = WINmin( ::Length( segMid.P2() - ptProjStart ), ::Length( segMid.P2() - ptProjEnd ) );
    
    m_segGesture = R2Line_seg( segMid.P1() - vecDir * dLenEnd1, segMid.P2() + vecDir * dLenEnd2 );
    
    /*
     * Now do a half-plane test with the corner points. Half should be
     * above, half below
     */
    int iCornersAbove = 0, iCornersBelow = 0;
    for ( int i = 0; i != -1; i = in_abCorners.find_next(i) ) {
        const R2Vec vecToPt = m_apt[i] - m_segGesture.P1();
        const double dCross = vecToPt[0] * vecDir[1] - vecToPt[1] * vecDir[0];
        if ( dCross < 0.0 ) {
            iCornersAbove++;
        } else {
            iCornersBelow++;
        }
    }
	
    /*
     * Now look at how the polyline segments cross the mid point line
     * Should cross roughly at the middle
     */
    R2Pt ptOut;
    double dTLast = -1e30;
    m_dErrErase = 0.0;
    double dS, dT;
    int iSegDecrease = 0;
    if ( s_bTrace ) cout << "\n";
    for ( int i = in_abCorners.find_first(); i != m_apt.num() - 1; i = in_abCorners.find_next(i) ) {
        const R2Line_seg seg( m_apt[i], m_apt[in_abCorners.find_next(i)] );
		
        if ( !m_segGesture.Intersect( seg, ptOut, dS, dT ) ) {
            if ( !( i == in_abCorners.find_first() || in_abCorners.find_next(i) == m_apt.num() - 1 ) ) {
                cout << " Not crossing middle\n";
                return false;
            }
            m_dErrErase += ::Length( m_segGesture(dS) - seg(dT) );
        }
        if ( dS < dTLast ) {
            iSegDecrease++;
        }
        dTLast = dS;
        if (s_bTrace ) cout << " seg " << i << " " << dS << " " << ::Length( m_segGesture(dS) - seg(dT) ) << "\n";
    }
    m_dErrErase /= ( in_abCorners.count() - 1.0 );
    
	if ( s_bTrace ) {
		cout << iCornersAbove << " " << iCornersBelow << " ";
		cout << " dec " << iSegDecrease <<  " ";
		cout << m_dErrErase << "/" << cdErrFitFactor * m_segGesture.Length() << "\n";
	}
	
    if ( ! (iSegDecrease == 0 || iSegDecrease == in_abCorners.count() - 2) ) {
        if ( s_bTrace ) cout << "Not in a line " << iSegDecrease << "\n";
        return false;
    }
    if ( abs( iCornersAbove - iCornersBelow ) < 2 && m_dErrErase < cdErrFitFactor * m_segGesture.Length() ) {
        if ( s_bTrace ) cout << " Erase\n";
        return true;
    }
    return false;
}

/*
 * Calculations of discrete curvature:
 *  1) For a circle, the curvature is 1/R, where R is the radius of the circle.
 *  2) Curvature is also change in angle over chane in length (d Theta / d s)
 *     Change in angle is approximated by the exterior angle between v1 and v2
 *     ie, acos( dot<v1,v2> / ||v1|| ||v2|| )
 *                  / u2
 *        -  u1  ->/ theta
 *     Change in length is approximated by the average length of v1 and v2
 *  3) Can also get by taking cross product and measuring lengths
 * 
 * This could be a method on the 
 */
static double CurvatureStatic( const R2Pt &in_ptPrev, const R2Pt &in_pt, const R2Pt &in_ptNext ) 
{
    const R2Vec vecV1 = ( in_pt - in_ptPrev );
    const R2Vec vecV2 = ( in_ptNext - in_pt );
    const double dLenV1 = ::Length( vecV1 );
    const double dLenV2 = ::Length( vecV2 );
    if ( RNIsZero( dLenV1 ) || RNIsZero( dLenV2 ) )
        return 0.0;
    
    if ( RNIsZero( ::Length( in_ptNext - in_ptPrev ) ) ) {
        return 1e10;
    }
    
    const double dDot = Dot( vecV1, vecV2 ) / (dLenV1 * dLenV2);
    const double dTheta = acos( WINminmax( dDot, -1.0 + RNEpsilon_d, 1.0 - RNEpsilon_d ) );	
    //const double dRet = 2.0 * sin( dTheta / 2.0 ) / sqrt( dLenV1 * dLenV2 ); 
    const double dRet = dTheta / ( 0.5 * (dLenV1 + dLenV2) );
    
    return dRet;
}


double ScreenCurve::Curvature( const int in_iPt ) const
{
	return CurvatureStatic( m_apt.clamp(in_iPt-1), m_apt[in_iPt], m_apt.clamp(in_iPt+1) );
}


double ScreenCurve::CurvatureCircle( const int in_iPt ) const
{
    static Array<R2Pt> aptFit(5);
    
    const int iLeft = WINmax( in_iPt - 2, 0 );
    const int iRight = WINmin( in_iPt + 3, m_apt.num() );
    aptFit.need( iRight - iLeft );
    for ( int i = iLeft; i < iRight; i++ ) {
        aptFit[i - iLeft] = m_apt[i];
    }
    
    if ( aptFit.num() < 3 ) {
        return Curvature( in_iPt );
    }

    const R2Ellipse ell = FITTools_FitEllipse::FitImplicitCircle( aptFit );
    if ( ell.XRadius() < 1e-6 ) {
        return 1.0 / 1e-6;
    }
    return 1.0 / ell.XRadius();
}

/* Find all points that might be corners.
 * Essentially the short straw algorithm
 * in_dDist is the sample distance to use, i.e., the straw goes from pt +- in_dDist
 */
boost::dynamic_bitset<> ScreenCurve::Corners( const double in_dDist, const double in_dAng ) const
{
    static boost::dynamic_bitset<> abCorner;
	abCorner.resize( m_apt.num() );
    abCorner.reset();
    
    // Find all places where there is an angle change
    static Array<double> adBend( m_apt.num() ), adCurvature( m_apt.num() );
	adBend.need( m_apt.num() );
	adCurvature.need( m_apt.num() );
	
    const double dTDelta = in_dDist / Length();
	if ( s_bTrace ) cout << "corner M_PI / 3.0\n";
    for ( int i = 0; i < m_apt.num(); i++ ) {        
        if ( PercAlong(i) > dTDelta && PercAlong(i) < 1.0 - dTDelta ) {
            const R2Pt ptForward = (*this)( PercAlong(i) + dTDelta );
            const R2Pt ptBackward = (*this)( PercAlong(i) - dTDelta );
            const double dDot = Dot( UnitSafe( ptForward - m_apt[i] ), UnitSafe( ptBackward - m_apt[i] ) );
            adBend[i] = M_PI - acos( WINminmax( dDot, -1.0 + RNEpsilon_d, 1.0 - RNEpsilon_d ) );
			adCurvature[i] = Curvature( i );
            if ( s_bTrace ) cout << i << " " << adBend[i] << " " << dDot << " " << adCurvature[i] << "\n";
        } else {
            if ( s_bTrace ) cout << i << " 0 0 0 \n";
            adBend[i] = 0.0;
			adCurvature[i] = 0.0;
        }
    }
    if ( s_bTrace ) cout << "\n";
    
    // Keep only the biggest in the local area (prevents multiple local corners)
	// Use the curvature calculation here, not the bend angle
    for ( int i = 1; i < m_apt.num()-1; i++ ) {
        if ( adBend[i] > M_PI / 3.0 ) {
            bool bIsMax = true;
            for ( int j = i-1; j >= 0; j-- ) {
                if ( PercAlong(j) < PercAlong(i) - dTDelta ) {
                    break;
                }
                if ( adCurvature[j] > adCurvature[i] ) {
                    bIsMax = false;
                    break;
                }
            }
            for ( int j = i+1; j < adCurvature.num(); j++ ) {
                if ( PercAlong(j) > PercAlong(i) + dTDelta ) {
                    break;
                }
                if ( adCurvature[j] > adCurvature[i] ) {
                    bIsMax = false;
                    break;
                }
            }
            if ( bIsMax ) {
                if ( s_bTrace ) cout << i << " " << PercAlong(i) << " Ang " << adBend[i] << " crv " << adCurvature[i] << "\n";
                abCorner.set(i);
            }
        }
    }
    return abCorner;
}

/*
 * A tap is both short and has few points
 * Does NOT need data calculated in GetGesture
 * Uses parameters in the ParamFile class.
 */
bool ScreenCurve::IsTap(  ) const
{
    const double dStrokeLen = Length();
    const double dEndsLen = ::Length( m_apt[0] - m_apt.last() );
    
	if ( s_bTrace ) {
		cout << " Tap: dEndsLen " << dEndsLen;
		cout << " stroke len " << dStrokeLen << " ";
	}
	const int iNMaxPts = g_paramFile.GetInteger( "NumPtsIsTap");
    if ( m_apt.num() < iNMaxPts ) {
        if ( s_bTrace ) cout << " Tap n pixels " << m_apt.num() << " " << iNMaxPts << "\n";
        return true;
    }
	
	const double dLenMax = g_drawState.PixelWidth2D() * g_paramFile.GetInteger( "ScreenLengthIsTapInPixels" );
    if ( s_bTrace ) cout << " ScreenLenPixs " << dLenMax;
	if ( dStrokeLen < dLenMax ) {
        if ( s_bTrace ) cout << " Tap len " << dStrokeLen << " " << dLenMax << "\n";
		return true;
	}
	
    if ( dEndsLen < dLenMax && dStrokeLen < 1.2 * dEndsLen ) {
        if ( s_bTrace ) cout << " Tap ends len " << dEndsLen << " " << dLenMax << "\n";
        return true;
    }
	
    if ( s_bTrace ) cout << "\n";
    return false;
}

/*
 * Legacy
 * Try out all gestures and sort by their score
 * Since all of the gestures require similar data, this calculates it all at once
 *  m_segGesture is the best segment that fits the gesture
 * This would be a const method except we store a bunch of the data (the fitted
 * line, circle, best-guess segment, gestures found) for drawing later.
 */
const Array<ScreenCurve::Gesture> &ScreenCurve::GetGesture( const double in_dDist ) 
{
    m_aGesture.need(0);
    
    if ( m_apt.num() == 0 ) {
        m_aGesture += NO_GESTURE;
        return m_aGesture;
    }
    
    if ( s_bTrace) cout << "Screen Gesture: Length " << m_dLength << " pts " << NumPts() << " time: " << m_dEndTime - m_dStartTime;
	
    if ( m_apt.num() < 3 || RNIsZero( m_dLength ) ) {
        m_circGesture = R2Sphere( m_apt[0], in_dDist * 0.5 );
        if ( s_bTrace) cout << " Tap\n";
        m_aGesture += TAP;
        if ( !RNIsZero( m_dLength ) ) {
            m_aGesture += LINE;
        }
        return m_aGesture;
    }
    
    /*
     * Now have enought points to fit line/circle
     */
    const FITTools_FitLine fitLine( m_apt );
    m_segGesture = fitLine.Line_seg2D();
    m_dErrLine = fitLine.Error();
	
    const R2Ellipse ell = FITTools_FitEllipse::FitImplicitCircle( m_apt );
    m_circGesture = R2Sphere( ell.Center(), ell.XRadius() );               
	
    m_dErrCircle = 0.0;
    for ( int i = 0; i < m_apt.num(); i++ ) {
        const double dLen = ::Length( m_apt[i] - m_circGesture.Center() );
        m_dErrCircle += fabs( dLen - m_circGesture.Radius() );
    }
    m_dErrCircle /= (double) m_apt.num();
    
    if ( IsTap(  ) ) {
        m_aGesture += TAP;
        if ( m_dErrLine < m_dErrCircle ) {
            if ( IsLine( in_dDist ) ) {
                m_aGesture += LINE;
            }
            if ( IsCircle( in_dDist ) ) {
                m_aGesture += CIRCLE;
            }
        }
    } else if ( IsLine( in_dDist  ) ) {
        m_aGesture += LINE;//
        if ( IsCircle( in_dDist ) ) {
            m_aGesture += CIRCLE;
        }
    } else if ( IsCircle( in_dDist ) ) {
        m_aGesture += CIRCLE;//
    }
	
    boost::dynamic_bitset<> abCorners = Corners( 1.5 * m_dLength / (double) m_apt.num() );
	
    // Either a line or a stroke
    if ( abCorners.count() == 0 || m_aGesture.num() ) {
        if ( s_bTrace) cout << " No corners\n";
        if ( m_aGesture.num() == 0 )
            m_aGesture += NO_GESTURE;
        return m_aGesture;
    }
    
    abCorners.set(0);
    abCorners.set( m_apt.num() - 1 );
    
    if ( IsSquiggle( in_dDist, abCorners ) ) {
        m_aGesture += SQUIGGLE;
    }
    const R2Line_seg segSave = m_segGesture;
    
    if ( abCorners.count() > 3 && ::Length( m_apt[0] - m_apt.last() ) > 0.5 * in_dDist * cdEndScaleFactor ) {
        if ( IsErase( in_dDist, abCorners ) ) {
            m_aGesture += ERASE;
        }
    }
    if ( m_aGesture.num() == 0 ) {
        m_aGesture += NO_GESTURE;
        if ( m_dErrSquiggle < m_dErrErase ) {
            m_aGesture += SQUIGGLE;
        } else {
            m_aGesture += ERASE;
        }
    }
	
    return m_aGesture;
}

