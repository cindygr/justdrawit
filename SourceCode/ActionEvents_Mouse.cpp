/*
 *  ActionEvents_Mouse.cpp
 *  SketchCurves
 *
 *  Created by Cindy Grimm on 9/9/10.
 *  Copyright 2010 Washington University in St. Louis. All rights reserved.
 *
 */


#include "ActionEvents.h"
#include "PointConstraint.h"
#include "UserInterface.h"
#include <FL/Fl.H>
#include <time.h>
#include "ParamFile.h"

const bool s_bTrace = false;


/* Set what's selected during a mouse move, mouse drag
 * Precedence order:
 *   Last stroke/smooth circles
 *   Intersection normals
 *   Curve constraint normals
 *   Intersection points
 *   Curve constraint points
 *   Curve
 *   Shadow box geometry
 *
 * Exceptions to this:
 *   If the corresponding show button is off, skip that one
 *   If in a menu drag 
 *      Selecting curve if SELECT_CURVE action
 *      Selecting a curve region if SELECT_CURVE_REGION
 *
 * If you're adding something new to select, make sure you add it to bOtherSelection in MouseUp as well.
 */
void ActionEvents::SetSelectionState( const R2Pt &in_ptScreen, const R3Ray &in_ray )
{
	/* Some actions keep state (what was selected). Use these to keep the state based on the action */
	bool bResetState = false; // Everything but the shadow box
	bool bResetCurveState = false;
	bool bResetShadowboxState = false;
	
	if ( m_action == MENU_SELECT_CURVE ) {
		bResetState = true;
		bResetShadowboxState = true;
	} else if ( m_action == MENU_SELECT_CURVE_REGION ) {
		bResetState = true;
		bResetShadowboxState = true;
	} else if ( m_action == NO_ACTION ) {
		bResetState = true;
		bResetCurveState = true;
		bResetShadowboxState = true;
	} else if ( m_action == DRAG_CURVE || m_action == MENU_SELECT_CURVE_REGION || m_action == MENU_SELECT_CURVE ) {
		bResetState = true;
		bResetShadowboxState = true;
	} else if ( m_action == CAMERA_ALIGN || m_action == DRAWPLANE_ALIGN ) {
		bResetState = true;
		bResetShadowboxState = true;
	} else if ( m_action == INFLATION_SURFACE || m_action == EXTRUSION_SURFACE ) {
        bResetState = true;
        bResetCurveState = true;
        bResetShadowboxState = true;
    }
	
	/// Doesn't matter what our current action is (some form of camera action) - keep the current curve state
	if ( m_pushAction == DRAG_CURVE ) {
		bResetCurveState = false;
	}
	
	if ( bResetState == true ) {
        m_iCursorOverIntersection = -1;
		m_opCursorOverCurveNormalConstraint = NULL;
		m_opCursorOverCurvePointConstraint = NULL;
        m_iCursorOverInflationSrf = -1;
		m_iCursorOverShadowBox = -1;
		m_iAlignCamera = -1;
		m_bCursorOverLastStroke = false;
		m_bCursorOverLastSmooth = false;
        m_activeSelection = NO_SELECTION;
	}
	if ( bResetCurveState == true ) {
		ClearCurveSelected();
        if ( m_activeSelection == SEL_CURVE ) {
            m_activeSelection = NO_SELECTION;
        }
	}
	if( bResetShadowboxState == true ) {
		m_iCursorOverShadowBox = -1;
        if ( m_activeSelection == SEL_SHADOW_BOX ) {
            m_activeSelection = NO_SELECTION;
        }
	}
		
	/* Based on the current action, determine what (if anything) we should look for for selections */
	const OGLObjsCamera &cam = g_drawState.GetCamera();
	if ( m_action == DRAG_CURVE ) {
        double dZShadowBox = 0.0;
        m_iCursorOverShadowBox = m_shadowbox.CursorOverSelectable( in_ptScreen, dZShadowBox );
        
        m_bIsPerpendicularDrag = false;

        m_actionDragClick = WhichDragAction( in_ptScreen, m_dTDragClick );
        if ( m_actionDragClick.first == DRAG_CURVE ) {
            m_activeSelection = SEL_DRAG;
        } else if ( m_actionDragClick.first == ROTATE_CURVE ) {
            m_activeSelection = SEL_ROTATE;
        } else if ( m_actionDragClick.first == SCALE_CURVE ) {
            m_activeSelection = SEL_SCALE;
        } else if ( m_actionDragClick.first == ROTATE_SCALE_CURVE ) {
            m_activeSelection = SEL_ROTATE_AND_SCALE;
        }
        if ( m_actionDragClick.first == DRAG_CURVE && m_actionDragClick.second == ShadowBox::DRAWING_PLANE ) {
            m_bIsPerpendicularDrag = IsOverTopBottomDragIcon( in_ptScreen );
            if ( m_bIsPerpendicularDrag ) {
                m_activeSelection = SEL_PERPENDICULAR_DRAG;
            }
            
            if (IsSubset()) {
                if ( m_crvNetwork.GetCurve( m_iCursorOverCurve ).IsClosed() ) {
                    if ( m_dCursorOverCurveTs.first < 0.0 ) {                        
                        // Unselected region is between second and first + 1.0
                        if ( m_dTDragClick > m_dCursorOverCurveTs.second && m_dTDragClick < m_dCursorOverCurveTs.first + 1.0 ) {
                            if ( m_dTDragClick - m_dCursorOverCurveTs.second < m_dCursorOverCurveTs.first + 1.0 - m_dTDragClick ) {
                                m_dTDragClick = m_dCursorOverCurveTs.second;
                            } else {
                                m_dTDragClick = m_dCursorOverCurveTs.first + 1.0;
                            }
                        }
                    } else if ( m_dCursorOverCurveTs.second > 1.0 ) {
                        // Unselected region is between first and second - 1.0
                        if ( m_dTDragClick > m_dCursorOverCurveTs.second - 1.0 && m_dTDragClick < m_dCursorOverCurveTs.first ) {
                            if ( m_dTDragClick - ( m_dCursorOverCurveTs.second - 1.0 ) < m_dCursorOverCurveTs.first - m_dTDragClick ) {
                                m_dTDragClick = m_dCursorOverCurveTs.second - 1.0;
                            } else {
                                m_dTDragClick = m_dCursorOverCurveTs.first;
                            }
                        }
                    } else {
                        m_dTDragClick = WINminmax( m_dTDragClick, m_dCursorOverCurveTs.first, m_dCursorOverCurveTs.second );
                    } 
                } else {
                    m_dTDragClick = WINminmax( m_dTDragClick, m_dCursorOverCurveTs.first, m_dCursorOverCurveTs.second );
                }

                m_dCursorOverCurve = m_dTDragClick;
                if ( s_bTraceCurveSelection ) {
                    cout << "ActionEvents::SetSelectionState ts: " << m_dCursorOverCurveTs.first << " " << m_dCursorOverCurve << " " << m_dCursorOverCurveTs.second << "\n";
                }
            }
        }
        
        // Determine which takes precedence by evaluating z values of curve
        if ( m_iCursorOverShadowBox != -1 && m_actionDragClick.first != NO_ACTION ) {
            const R3Pt ptCrv = m_crvNetwork.GetCurve( m_iCursorOverCurve )( m_actionDragClick.first == DRAG_CURVE ? m_dCursorOverCurve : m_dTDragClick );
            const R3Pt ptPlane = GetShadowBox().GetPlane( m_actionDragClick.second ).ProjectOnPlane( ptCrv );
            const double dZCrv = cam.ProjectedPt( ptPlane )[2];
            if ( dZCrv < dZShadowBox ) {
                m_iCursorOverShadowBox = -1;
            } else {
                m_actionDragClick.first = NO_ACTION;
                m_activeSelection = SEL_SHADOW_BOX;
            }
        }
    } else if ( m_action == MENU_SELECT_CURVE_REGION ) {
		// Update the t values of the selection region and current selected point
        SelectedCurveRegion( in_ray );		
        m_activeSelection = SEL_CURVE;
		
	} else if ( m_action == MENU_SELECT_CURVE ) {
		CurveSelected( in_ray, m_iCursorOverCurve, m_dCursorOverCurve );

        if ( s_bTraceCurveSelection ) {
            cout << "ActionEvents::SetSelectionState ts: " << m_dCursorOverCurveTs.first << " " << m_dCursorOverCurve << " " << m_dCursorOverCurveTs.second << "\n";
        }

		if ( m_menuSelection == MENU_CURVE_ALIGN ) { // Look for second curve
			SetDrawingPlaneTwoPoints();
            m_activeSelection = SEL_CURVE;
		}
	} else if ( m_action == CAMERA_ALIGN || m_action == DRAWPLANE_ALIGN ) {
		// Sets m_iAlignCamera. Looking for which axes is selected
		BestAxis( in_ptScreen );
        m_activeSelection = SEL_ALIGN_CAMERA;
    } else if ( m_action == INFLATION_SURFACE || m_action == EXTRUSION_SURFACE ) {
		// Allow selection of inflation surface and shadow box 
        double dZInfl = 0.0;
        m_iCursorOverInflationSrf = m_srfInflation.SelectSurface( in_ptScreen, dZInfl );

		double dZShadow = 0.0;
		m_iCursorOverShadowBox = m_shadowbox.CursorOverSelectable( in_ptScreen, dZShadow );
		
		// If conflict, pick closer
        m_activeSelection = SEL_INFLATION_SURFACE;
		if ( m_iCursorOverShadowBox != -1 && m_iCursorOverInflationSrf != -1 ) {
			if ( dZInfl < dZShadow ) {
				m_iCursorOverShadowBox = -1;
			} else {
				m_iCursorOverInflationSrf = -1;
                m_activeSelection = SEL_SHADOW_BOX;
			}
		}
	} else if ( m_action == NO_ACTION ) {

		// These two over-ride all others
		m_bCursorOverLastStroke = m_crvNetwork.IsClickOnLastStroke( in_ptScreen );
		m_bCursorOverLastSmooth = m_crvNetwork.IsClickOnLastSelectRegion( in_ptScreen );
		
        if ( m_bCursorOverLastStroke ) {
            m_activeSelection = SEL_LAST_STROKE;
        } else if ( m_bCursorOverLastSmooth ) {
            m_activeSelection = SEL_SMOOTHING_CIRCLE;
        }
		if ( ! m_bCursorOverLastStroke && ! m_bCursorOverLastSmooth ) {
			/* Do NOT rearrange this order. Or, if you need to, make sure the if (dZ < 0.99 * dZValue) if
			 * statements correctly zero out all of the preceding selections
			 */
            double dDistNorm = 1e30, dZValue = 1e30;
			m_opCursorOverCurveNormalConstraint = m_crvNetwork.GetClosestNormalConstraint( in_ray, dDistNorm, dZValue );

			m_iCursorOverIntersection = m_srfData.ClosestIntersection( in_ray );
			if ( m_iCursorOverIntersection != -1 ) {
				const double dZ = cam.ProjectedPt( m_srfData.IntersectionPt(m_iCursorOverIntersection) )[2];
				if ( dZ < 0.99 * dZValue ) {
					m_opCursorOverCurveNormalConstraint = NULL;
					dZValue = dZ;
				} else {
					m_iCursorOverIntersection = -1;
				}
			}

            int iCurve = -1;
            double dCurveT = -1.0;
            
			const double dDistCurve = CurveSelected( in_ray, iCurve, dCurveT );
			if ( dDistCurve < g_drawState.SelectionSize() ) {

				const double dZ = cam.ProjectedPt( m_crvNetwork.GetCurve( iCurve )(dCurveT ) )[2];
				if ( dZ < 0.99 * dZValue ) {
					m_iCursorOverIntersection = -1;
					m_opCursorOverCurveNormalConstraint = NULL;
					dZValue = dZ;

                    SetCurveSelected( iCurve, dCurveT );
                    
                    if ( s_bTraceCurveSelection ) {
                        cout << "ActionEvents::SetSelectionState ts: " << m_dCursorOverCurveTs.first << " " << m_dCursorOverCurve << " " << m_dCursorOverCurveTs.second << "\n";
                    }
				}
			}
			double dZ = 0.0;
			m_iCursorOverShadowBox = m_shadowbox.CursorOverSelectable( in_ptScreen, dZ );
			if ( m_iCursorOverShadowBox != -1 && dZ < 0.99 * dZValue ) {
                m_opCursorOverCurveNormalConstraint = NULL;
				m_iCursorOverIntersection = -1;
				ClearCurveSelected();
				dZValue = dZ;
			} else {
				m_iCursorOverShadowBox = -1;
			}
			dZ = 0.0;
			m_iCursorOverInflationSrf = m_srfInflation.SelectSurface( in_ptScreen, dZ );
			if ( m_iCursorOverInflationSrf != -1 && dZ < 0.99 * dZValue ) {
                m_opCursorOverCurveNormalConstraint = NULL;
				m_iCursorOverIntersection = -1;
				m_iCursorOverShadowBox = -1;
				ClearCurveSelected();
				dZValue = dZ;
			} else {
				m_iCursorOverInflationSrf = -1;
			}
		
            if ( m_opCursorOverCurveNormalConstraint != NULL ) {
                m_activeSelection = SEL_NORMAL_CONSTRAINT;
            } else if ( m_iCursorOverIntersection != -1 ) {
                m_activeSelection = SEL_POTENTIAL_INTERSECTION;
            } else if ( m_opCursorOverCurvePointConstraint != NULL ) {
                m_activeSelection = SEL_POINT_CONSTRAINT;
            } else if ( m_iCursorOverShadowBox != -1 ) {
                m_activeSelection = SEL_SHADOW_BOX;
            } else if ( m_iCursorOverInflationSrf != -1 ) {
                m_activeSelection = SEL_INFLATION_SURFACE;
            } else if ( OverCurve() ) {
                m_activeSelection = SEL_CURVE;
            }
		}
	}
        
}


/* When draging or inflation surface editing, use this to activate hot spots on mouse down (if any) */
void ActionEvents::ActivateHotspot( const R2Pt &in_ptScreen )
{
	if ( m_action == DRAG_CURVE ) {
		if ( m_iCursorOverCurve == -1 ) {
			cerr << "ERR: ActionEvents::MouseDown, no curve selected for DrawCurve\n";
			m_action = NO_ACTION;
		} else {
			// Set up additional data for drag/rotate/scale. Most of this
            // is initially set in SetSelectionState
			const OGLObjsCamera &cam = g_drawState.GetCamera();
            const Curve &crv = m_crvNetwork.GetCurve( m_iCursorOverCurve );
            m_ptRotateScaleFixed = crv(m_dCursorOverCurve );

            m_iPtIndexRotateScale = m_crvNetwork.GetCurve( m_iCursorOverCurve ).Floor( m_dTDragClick );

            const R3Plane plane = g_drawState.GetShadowBox().GetPlane( m_actionDragClick.second );
            m_ptRotateScale.first = cam.CameraPt( plane.ProjectOnPlane( m_ptRotateScaleFixed ) );
            m_ptRotateScale.second = cam.CameraPt( plane.ProjectOnPlane( m_crvNetwork.GetCurve( m_iCursorOverCurve )( m_dTDragClick ) ) );

            if ( m_iCursorOverShadowBox == -1 ) {
                m_action = m_actionDragClick.first;
                StartDrag();
            }
        }
	} else if ( m_action == INFLATION_SURFACE || m_action == EXTRUSION_SURFACE ) {
		double dZ = 0.0;
		m_iCursorOverShadowBox = m_shadowbox.CursorOverSelectable( in_ptScreen, dZ );
	}
	
	// A subset of the actions
	if ( m_iCursorOverShadowBox != -1 ) {
		m_pushAction = m_action;
		
		if ( m_shadowbox.IsTrackball( m_iCursorOverShadowBox ) ) {
			m_action = CAMERA_TRACKBALL;
		} else if ( m_shadowbox.IsLeftRightRotation( m_iCursorOverShadowBox ) ) {
			m_action = CAMERA_LEFT_RIGHT;
		} else if ( m_shadowbox.IsUpDownRotation( m_iCursorOverShadowBox ) ) {
			m_action = CAMERA_UP_DOWN;
		} else if ( m_shadowbox.IsSpinRotation( m_iCursorOverShadowBox ) ) {
			m_action = CAMERA_SPIN;
		} else if ( m_shadowbox.IsZoom( m_iCursorOverShadowBox ) ) {
			g_drawState.StartDollyZoom(); // save camera
			m_action = CAMERA_ZOOM;
		} else if ( m_shadowbox.IsSpinDrawPlane( m_iCursorOverShadowBox ) ) {
			m_shadowbox.StartRotateDrawingPlane( m_iCursorOverShadowBox ); // save camera
			m_action = DRAWPLANE_ROTATE;
		} else if ( m_shadowbox.IsDraggingPlane( m_iCursorOverShadowBox ) ) {
			m_action = DRAWPLANE_DRAG;
			m_shadowbox.StartDragDrawingPlane( m_iCursorOverShadowBox );
		}
	}
}

/* Mouse down
 * Default action is to start drawing. Exceptions are:
 * In Drag mode, in which case mouse move will be interpreted as a drag
 * In Menu drag mode, in which case we're doing an extended menu selection
 * The shadow box is drawn and we're over a hot spot
 * If shift is down wait and see - could be menu (tap), could be trackball
 * If drawing, don't flag as draw until no tap
 */
void ActionEvents::MouseDown( const R2Pt &in_ptScreen, const R3Ray &in_ray, const int in_eventModifier ) 
{
    m_ptScreenLast = in_ptScreen;
	m_ptScreenDown = in_ptScreen;
    
    m_rayDown = in_ray;
    m_rayLast = in_ray;
    
    // Always capture the stroke
    m_curveTempScreen.Clear();
    m_curveTempScreen.AddPoint( in_ptScreen, 0.0 );
    
    //const R3Pt ptPlane = m_shadowbox.DrawingPlane().IntersectRay( in_ray );
	m_bIsTap = true;

	SetSelectionState( in_ptScreen, in_ray );

    /* They've clicked in the background in the middle of a drag curve.
     * Clear the action and start afresh */
	if ( m_action == DRAG_CURVE && m_actionDragClick.first == NO_ACTION && m_iCursorOverShadowBox == -1 ) {
        m_pushAction = DRAG_CURVE;
        m_action = NO_ACTION;
    }
    
    switch ( m_action ) {
        case NO_ACTION :
			if ( m_bCursorOverLastSmooth ) {
				m_action = SMOOTHING;
				m_crvNetwork.RepeatLastSmooth();
				
            } else if ( m_opCursorOverCurveNormalConstraint != NULL ) {
                // User has picked an intersection normal...
                if ( m_opCursorOverCurveNormalConstraint->NumCurves() == 1 ) {
                    // Only one curve - rotate curve normal around tangent
                    // Set all curve data as if curve was selected
                    m_action = EDIT_CURVE_NORMAL;
                    const CurveGroup &crvGrp = m_opCursorOverCurveNormalConstraint->GetCurveGroup(0);
                    SetCurveSelected( m_crvNetwork.FindIndexOfCurveGroup( crvGrp ), m_opCursorOverCurveNormalConstraint->GetTValue( crvGrp ) );
                    CalcGeometryAtStartNormalConstraint();
                    
                    if ( s_bTraceCurveSelection ) {
                        cout << "ActionEvents::MouseDown ts: " << m_dCursorOverCurveTs.first << " " << m_dCursorOverCurve << " " << m_dCursorOverCurveTs.second << "\n";
                    }
                } else {
                    /// Free rotation
                    m_action = EDIT_INTERSECTION_NORMAL;				
                }
			} 
			if ( m_iCursorOverShadowBox != -1 ) {
				if ( m_shadowbox.IsDraggingPlane( m_iCursorOverShadowBox ) ) {
					m_action = DRAWPLANE_DRAG;
					m_shadowbox.StartDragDrawingPlane( m_iCursorOverShadowBox );
				} else if ( m_shadowbox.IsTrackball( m_iCursorOverShadowBox ) ) {
					m_action = CAMERA_TRACKBALL;
				} else if ( m_shadowbox.IsLeftRightRotation( m_iCursorOverShadowBox ) ) {
					m_action = CAMERA_LEFT_RIGHT;
				} else if ( m_shadowbox.IsUpDownRotation( m_iCursorOverShadowBox ) ) {
					m_action = CAMERA_UP_DOWN;
				} else if ( m_shadowbox.IsSpinRotation( m_iCursorOverShadowBox ) ) {
					m_action = CAMERA_SPIN;
				} else if ( m_shadowbox.IsZoom( m_iCursorOverShadowBox ) ) {
					g_drawState.StartDollyZoom(); // save camera
					m_action = CAMERA_ZOOM;
				} else if ( m_shadowbox.IsSpinDrawPlane( m_iCursorOverShadowBox ) ) {
					m_shadowbox.StartRotateDrawingPlane( m_iCursorOverShadowBox ); // save camera
					m_action = DRAWPLANE_ROTATE;
				}
			}
			break;
        case INFLATION_SURFACE : 
        case EXTRUSION_SURFACE : 
			ActivateHotspot( in_ptScreen );
            break;
            
        case DRAG_CURVE : {
			ActivateHotspot( in_ptScreen );
            /// Always reset the start of the drawing
            PushUndo( m_iCursorOverCurve, StrokeHistory::TRANSFORM );
            m_crvNetwork.StartTransform( m_iCursorOverCurve );
            }
            break;
        case MENU : {
            // See if we're over a selection; if not, get closest selection
            // For some of the menu selections the mouse down will set the current action
            // otherwise, just do a drag until we get more info
            m_menuSelection = SelectMenuClosest( in_ptScreen );
            m_action = MENU_DRAG;
            if ( m_menuWhich == MENU_CAMERA ) {
                if ( m_menuSelection == MENU_CAMERA_SCALE_ROTATE ) {
                    m_action = CAMERA_SCALE_ROTATE;
                } else if ( m_menuSelection == MENU_CAMERA_PAN ) {
                    m_action = CAMERA_PAN;
                } else if ( m_menuSelection == MENU_CAMERA_DOLLY_ZOOM ) {
					g_drawState.StartDollyZoom(); // Save current camera
                    m_action = CAMERA_DOLLY_ZOOM;
                } else if ( m_menuSelection == MENU_CAMERA_ROTATE_UP_DOWN ) {
                    m_action = CAMERA_UP_DOWN;
                } else if ( m_menuSelection == MENU_CAMERA_ROTATE_LEFT_RIGHT ) {
                    m_action = CAMERA_LEFT_RIGHT;
				} else if ( m_menuSelection == MENU_CAMERA_TRACKBALL ) {
					m_action = CAMERA_TRACKBALL;
				}
                
            } else if ( m_menuWhich == MENU_CURVE ) {
                if ( m_menuSelection == MENU_CURVE_SMOOTH || m_menuSelection == MENU_CURVE_ERASE || m_menuSelection == MENU_CURVE_DRAG ) {
                    m_action = MENU_SELECT_CURVE_REGION;
                } else if ( m_menuSelection == MENU_CURVE_NORMAL ) {
					if ( s_bTrace ) cout << "Curve normal editing\n";
                    // Check to see if a constraint already exists here.
                    m_opCursorOverCurveNormalConstraint = m_crvNetwork.HasConstraint( m_iCursorOverCurve, m_dCursorOverCurve );
                    
					DoMenuItem(false, false);
                    // When we come out could be editing just one curve (spin
                    // normal around curve) or, if there are multiple curves through
                    // this constraint, then free rotaion
                    if ( m_action == EDIT_CURVE_NORMAL ) {
                        CalcGeometryAtStartNormalConstraint();
                    }
				}
            } else if ( m_menuWhich == MENU_LAST_STROKE ) {
				if (  m_menuSelection == MENU_LAST_STROKE_COMBINE ) {
					m_action = MENU_SELECT_CURVE;
				}
			}
			if ( m_action == MENU_SELECT_CURVE_REGION ) {
				m_dCursorOverCurveTs.first = m_dCursorOverCurve - 1e-6;
				m_dCursorOverCurveTs.second = m_dCursorOverCurve + 1e-6;
			}
            
            break;
        }
		case DRAWPLANE_ALIGN :
		case CAMERA_ALIGN :
            break;
        default :
            // This shouldn't happen because a mouse up should release these options
            cerr << "ERR: ActionEvents::Mouse down: Warning: Shouldn't be here\n";
            PrintState();
            break;
			
    }	
	if ( s_bTrace ) {
		cout << "Mouse down: ";
		PrintState();
	}
    if ( m_outLogFile.good() ) {
        m_outLogFile << "MouseDown " << in_eventModifier << " " << in_ptScreen << in_ray.Pt() << in_ray.Dir() << "\n";
    }
	UpdateGUIState();
}

/*
 * Mouse is just moving. The only thing that we care about is:
 *  If we've invoked a menu, then change menu selection if need be
 *  If we have the shadow box up, set the select state based on what we're over
 *  If we're dragging a curve, set the closest t value and rotate/scale information
 */
void ActionEvents::MouseMove( const R2Pt &in_ptScreen, const R3Ray &in_ray, const int in_eventModifier ) 
{
	if ( ApproxEqual(m_ptScreenLast, in_ptScreen) ) {
		return;
	}

	// Update the selection state to indicate what we're over (if anything)
	SetSelectionState( in_ptScreen, in_ray );
	
    // Check to see if in menu selection process, and if so, if we've passed through/out of the menu
    // a menu selection    
    if ( m_action == MENU ) {
        const double dMenuSize = g_drawState.MenuSize2D();
		const R2Vec vecActual = R3Matrix::Scaling( 1.0, 1.0 / g_drawState.GetCamera().GetAspectRatio(), 1.0 ) * ( in_ptScreen - m_ptMenuDown );

        if ( ::Length( vecActual ) < dMenuSize ) {
            // Still inside menu selection region
            const MenuSelection curSel = SelectMenu( in_ptScreen );
            if ( curSel != m_menuSelection ) {
                m_menuSelection = curSel;
                if ( s_bTrace ) PrintMenuState();
            }
        } else {
			const double dLenMouseMove = ::Length( m_ptScreenLast - in_ptScreen );
			const int iStep = (int) ( dLenMouseMove / (0.25 * dMenuSize) );
			// May have skipped over menu action
			for ( int iInterp = 0; iInterp < iStep; iInterp++ ) {
				const double dPerc = (double) iInterp / (double) iStep;
				const R2Pt pt = Lerp( m_ptScreenLast, in_ptScreen, dPerc );
				SelectMenu( pt );
			}
            DoMenuItem( true, true );

			m_action = m_pushAction;
            m_pushAction = NO_ACTION;
        }
    }
	
	if ( m_action == NO_ACTION || m_action == DRAG_CURVE ) {
		// Update the selection regions on the curves
		UpdateCurveSelectionColors();
	}

    m_ptScreenLast = in_ptScreen;
    m_rayLast = in_ray;
    
    if ( m_outLogFile.good() ) {
        m_outLogFile << "MouseMove " << in_eventModifier << " " << in_ptScreen << in_ray.Pt() << in_ray.Dir() << "\n";
    }
    UpdateGUIState();
}

/* Drawing curve once have moved more than a tap, unless:
 *   In menu drag mode, in which case we're doing one of the following once
 *     we've moved enough so that it isn't a tap
 *      Last stroke menu: Looking for curve to join to
 *      Curve menu (drag and smooth): Looking for ts of selected curve region
 *      
 *    Unless OverCurve is true, in which case we're 
 *      Last stroke menu, camera menu align: find closest curve
 *      Curve menu: set ts of currently selected curve
 *      Camera menu: change camera
 *   In curve drag mode, in which case we drag the curve
 *   In trackball, move camera
 *   If shift down and more movement than a tap, trigger trackball
 */
void ActionEvents::MouseDrag( const R2Pt &in_ptScreen, const R3Ray &in_ray, const int in_eventModifier ) 
{
    
	if ( ApproxEqual(m_ptScreenLast, in_ptScreen) ) {
		return;
	}
	
	m_curveTempScreen.AddPoint(in_ptScreen );
	// Once it's false it stays false
	if ( m_bIsTap == true ) {
		m_bIsTap = m_curveTempScreen.IsTap();
	}

	if ( m_action != DRAG_CURVE ) {
		SetSelectionState( in_ptScreen, in_ray );
	}

    switch ( m_action ) {
        case NO_ACTION : {
            if ( m_bIsTap == false ) {
                m_action = DRAWING;
				if ( s_bTrace ) cout << "Starting drawing\n";
            }
            break;
        }
        case CAMERA_TRACKBALL : {
            g_drawState.Trackball( m_ptScreenLast, in_ptScreen );
            break;
        }
        case CAMERA_LEFT_RIGHT : {
            g_drawState.RotateLeftRight( m_ptScreenLast, in_ptScreen );
            break;
        }
        case CAMERA_UP_DOWN : {
            g_drawState.RotateUpDown( m_ptScreenLast, in_ptScreen );
            break;
        }
        case CAMERA_SPIN : {
            g_drawState.RotateSpin( m_ptScreenLast, in_ptScreen );
            break;
        }
        case CAMERA_ZOOM : {
            g_drawState.Zoom( m_ptScreenLast, in_ptScreen );
            break;
        }
        case DRAG_CURVE : {
			MoveCurve( in_ptScreen, in_ray );
            break;
        }
        case ROTATE_SCALE_CURVE : {
            RotateScaleCurve( in_ptScreen );
            break;
        }
        case ROTATE_CURVE : {
            RotateCurve( in_ptScreen );
            break;
        }
        case SCALE_CURVE : {
            ScaleCurve( in_ptScreen, in_ray );
            break;
        }
		case EDIT_CURVE_NORMAL : {
			const R3Vec vecNorm = CalcNewNormalConstraint( in_ptScreen );
            if ( m_opCursorOverCurveNormalConstraint ) {
                m_opCursorOverCurveNormalConstraint->EditNormal( vecNorm, false );
            } else {
                cerr << "ERR: ActionEvents::MouseDrag, EDIT_CURVE_NORMAL, no normal\n";
            }
			break;
		}
		case EDIT_CURVE_POINT_CONSTRAINT : {
            if ( m_opCursorOverCurvePointConstraint ) {
                const R3Plane plane( m_opCursorOverCurvePointConstraint->GetPt(), g_drawState.DrawingPlane().Normal() );
                const R3Pt pt1 = plane.ProjectOnPlane( m_opCursorOverCurvePointConstraint->GetPt() );
                const R3Pt pt2 = plane.IntersectRay( in_ray );
                m_opCursorOverCurvePointConstraint->DragPointConstraint( pt2 - pt1, false );
            } else {
                cerr << "ERR: ActionEvents::MouseDrag, EDIT_CURVE_POINT_CONSTRAINT, no point selected\n";
            }
			break;
		}
		case EDIT_INTERSECTION_NORMAL : {
            if ( m_opCursorOverCurveNormalConstraint && m_bIsTap == false ) {
				const R2Pt ptBase = g_drawState.GetCamera().CameraPt( m_opCursorOverCurveNormalConstraint->GetPt() );
				const R2Vec vecDir = ( in_ptScreen - ptBase ) * 4.0;
				const R3Vec vecNorm = CalcNewIntersectionNormal(vecDir);

                m_opCursorOverCurveNormalConstraint->EditNormal( vecNorm, false );
            } else if ( m_opCursorOverCurveNormalConstraint == NULL ) {
                cerr << "ERR: ActionEvents::MouseDrag, EDIT_CURVE_NORMAL, no normal\n";
            }
		}
			break;
        case DRAWPLANE_DRAG :
            m_shadowbox.DragDrawingPlane( m_ptScreenDown, in_ptScreen);
            break;
        case DRAWPLANE_ROTATE :
            m_shadowbox.RotateDrawingPlane( m_ptScreenDown, in_ptScreen);
            break;
        case CAMERA_PAN : {
            g_drawState.Pan( m_ptScreenLast, in_ptScreen );
            break;
        }
        case CAMERA_DOLLY_ZOOM : {
            g_drawState.DollyZoom( m_ptScreenDown, in_ptScreen );
            break;
        }
        case CAMERA_SCALE_ROTATE : {
            g_drawState.RotateScale( m_ptScreenLast, in_ptScreen );
            break;
        }
		case DRAWPLANE_ALIGN :
        case MENU_SELECT_CURVE : 
        case MENU_SELECT_CURVE_REGION : 
			// Show selected curve
			UpdateCurveSelectionColors(  );
			break;
        case CAMERA_ALIGN : {
			// Will be set in SetSelectionState
            break;
        }
        case INFLATION_SURFACE :
        case EXTRUSION_SURFACE :
            break;

        case MENU_DRAG : {
			// Menu selection is determined by mouse down location
			// Can switch into selecting curve or curve region
            if ( m_menuSelection == NO_MENU_SELECTED ) {
                // Shouldn't be here, but force a selection
                cerr << "ERR: Mouse drag, no menu selection in menu drag\n";
                m_menuSelection = SelectMenuClosest( in_ptScreen );
                PrintMenuState();
            }
            // Switch to doing curve select actions if we aren't a tap
			if ( m_bIsTap == false ) {
				if ( m_menuWhich == MENU_CAMERA ) {
					if ( m_menuSelection == MENU_CAMERA_ALIGN ) {
						if ( s_bTrace ) cout << "Looking for curve\n";
						m_action = MENU_SELECT_CURVE;
					} else if ( m_menuSelection == MENU_CAMERA_CENTER ) {
						if ( s_bTrace ) cout << "Looking for curve\n";
						m_action = MENU_SELECT_CURVE;
					}
				} else if ( m_menuWhich == MENU_LAST_STROKE ) {
					if ( m_menuSelection == MENU_LAST_STROKE_BLEND || m_menuSelection == MENU_LAST_STROKE_MERGE_CLOSE || m_menuSelection == MENU_LAST_STROKE_MERGE || m_menuSelection == MENU_LAST_STROKE_OVERSTROKE ) {
						m_action = MENU_SELECT_CURVE;
					}
					if ( m_menuSelection == MENU_LAST_STROKE_SMOOTH ) {
						m_action = SMOOTHING;
					}
				} else if ( m_menuWhich == MENU_CURVE ) {
					if ( m_menuSelection == MENU_CURVE_SMOOTH || m_menuSelection == MENU_CURVE_ERASE || m_menuSelection == MENU_CURVE_DRAG ) {
						m_action = MENU_SELECT_CURVE_REGION;
					} 
					if ( m_menuSelection == MENU_CURVE_ALIGN ) {
						m_action = MENU_SELECT_CURVE;
						if ( !g_drawState.m_opUI->m_bShowDrawingPlane->value() ) {
							g_drawState.m_opUI->m_bShowDrawingPlane->value(1);
							LogGUI();
						}
					}
					if ( m_menuSelection == MENU_CURVE_SNAP ) {
                        // Two cases - if dragging in point then we're either
                        // draging the point constraint (there is one)
                        // or we're snapping two curves together
                        m_opCursorOverCurvePointConstraint = m_crvNetwork.HasConstraint( m_iCursorOverCurve, m_dCursorOverCurve );
                        cout << "Setting PointConstraint\n";

						if ( m_opCursorOverCurvePointConstraint != NULL ) {
							m_action = EDIT_CURVE_POINT_CONSTRAINT;
                            cout << "Drag->Started editing point constraint\n";
						} else {
							m_action = MENU_SELECT_CURVE;
                            m_iSnapCurveFirst.first = m_iCursorOverCurve;
                            m_iSnapCurveFirst.second = m_dCursorOverCurve;
                            cout << "Drag->Started selecting curve\n";
						}
					}
				}
			}
			if ( m_action == MENU_SELECT_CURVE ) {
                CurveSelected( in_ray, m_iCursorOverCurve, m_dCursorOverCurve );
                if ( s_bTraceCurveSelection ) {
                    cout << "ActionEvents::MouseDrag ts: " << m_dCursorOverCurveTs.first << " " << m_dCursorOverCurve << " " << m_dCursorOverCurveTs.second << "\n";
                }
            } else if ( m_action == MENU_SELECT_CURVE_REGION ) {
                SelectedCurveRegion( in_ray );
            }
        }
			break;
            
		case SMOOTHING :
			m_crvNetwork.RepeatLastSmooth();
			break;
			
        case DRAWING : 
            break;
        default :         
            cerr << "Mouse drag: Shouldn't be here\n";
            PrintState();
            break;
    }
    
    m_ptScreenLast = in_ptScreen;
    m_rayLast = in_ray;

    if ( m_outLogFile.good() ) {
        m_outLogFile << "MouseDrag " << in_eventModifier << " " << in_ptScreen << in_ray.Pt() << in_ray.Dir() << "\n";
    }
	UpdateGUIState();
}

/* If we haven't selected an action, now is the time to do so
 * Tap: Trigger a menu, select a menu option, or clear curve dragging
 * Most other actions we just go back to no state
 * Exceptions are:
 *   DRAG_CURVE: Keep dragging until tap
 *   
 */
void ActionEvents::MouseUp( const int in_eventModifier )
{
    m_ptScreenUp = m_ptScreenLast;
    bool bResetMenu = true;
    bool bResetState = true;
    bool bPopAction = false;
	bool bChangedCamera = false;
	bool bChangedCurves = true;
    m_bCheckConsistency = true;
    
    // Grab and start doing a trackball if we start doing a shift-mouse move
	if ( OverCurve() == false && m_bIsTap == true && ( m_action == DRAG_CURVE || m_action == INFLATION_SURFACE || m_action == EXTRUSION_SURFACE ) ) {
        int iCursor = -1;
        double dCursor = 0.0;
        if ( CurveSelected( m_rayDown, iCursor, dCursor ) > g_drawState.SelectionSize() ) {
            m_pushAction = m_action;
            if ( s_bTrace ) cout << "Starting camera menu\n";
            bResetState = false;
            m_action = NO_ACTION;
            bChangedCurves = false;
        }
	} 
	
	const int iLoopSmooth = (int) (g_paramFile.GetInteger("SmoothingMaximumLoops") * g_drawState.m_opUI->m_dSmoothing->value() );
    m_curveTempScreen.Smooth(iLoopSmooth);
	
    switch ( m_action ) {
        case CAMERA_TRACKBALL :
        case CAMERA_SCALE_ROTATE :
        case CAMERA_DOLLY_ZOOM :
        case CAMERA_UP_DOWN :
        case CAMERA_LEFT_RIGHT :
        case CAMERA_PAN :
        case CAMERA_ZOOM :
        case CAMERA_SPIN :
            bPopAction = true;
			bChangedCamera = true;
			bChangedCurves = false;
            m_bCheckConsistency = false;
            break;
        case MENU_DRAG :
			if ( m_menuWhich == MENU_CAMERA && m_pushAction == DRAG_CURVE ) {
				bPopAction = true;
			}
            // This may set m_action to be DRAG_CURVE
            DoMenuItem( m_bIsTap, false );
            if ( m_action == DRAG_CURVE ) {
                bResetState = false;
            }
			if ( m_action == CAMERA_ALIGN || m_action == DRAWPLANE_ALIGN ) {
                bResetState = false;
				bResetMenu = false;
				bChangedCamera = true;
			}
			bChangedCurves = false;
            break;
        case DRAG_CURVE :
            if ( m_bIsTap == false ) {
                bResetState = false;
            } else {
                if ( m_shadowbox.IsCenterDrawPlane( m_iCursorOverShadowBox ) ) {
					m_shadowbox.ResetDrawingPlane();
				} else if ( m_shadowbox.IsShowHideDrawPlane( m_iCursorOverShadowBox ) ) {
					g_drawState.m_opUI->m_bShowDrawingPlane->value( !g_drawState.m_opUI->m_bShowDrawingPlane->value() );
				} else if ( m_actionDragClick.first != NO_ACTION ) {
                    m_action = NO_ACTION; // dismiss
                } 
			}
            EndDrag();
            
            break;
        case ROTATE_CURVE : 
        case ROTATE_SCALE_CURVE : 
        case SCALE_CURVE : {
            if ( m_bIsTap == false ) {
                bResetState = false;
                m_action = DRAG_CURVE; // So selection knows what to do
            } else {
                m_action = NO_ACTION; // Dismiss
            }
            EndDrag();
            break;
        }
        case MENU_SELECT_CURVE :
        case MENU_SELECT_CURVE_REGION :
            DoMenuItem( m_bIsTap, false );
            
            if ( m_action == DRAG_CURVE ) {
                bResetState = false;
            }
			if ( m_action == CAMERA_ALIGN ) {
                bResetState = false;
				bResetMenu = false;
			}
			bChangedCurves = false;
            break;
		case SMOOTHING :
		case EDIT_CURVE_NORMAL :
        case EDIT_CURVE_POINT_CONSTRAINT :
			break;
		case EDIT_INTERSECTION_NORMAL :
			if ( m_bIsTap && m_opCursorOverCurveNormalConstraint ) {
                m_opCursorOverCurveNormalConstraint->EditNormal( m_opCursorOverCurveNormalConstraint->GetNormal() * -1.0, false );
				bResetMenu = true;
				bResetState = true;
				ClearCurveSelected();
				
			}
			break;
        case CAMERA_ALIGN :
            DoMenuItem( m_bIsTap, false );
			bChangedCamera = true;
			bChangedCurves = false;
            break;
		case DRAWPLANE_ALIGN :
			if ( OverCurve() && m_iAlignCamera != -1 ) {
                if ( g_drawState.m_opUI->m_bShowExtrusionSrf->value() ) {
                    m_srfInflation.AlignToCurve( m_crvNetwork.GetCurve(m_iCursorOverCurve), m_vecAlignCameraLook );
                    m_inflationSrfEditAction = InflationSurface::DRAW_CURVE;
                    m_action = EXTRUSION_SURFACE;
                    bResetState = false;
                    ClearCurveSelected();
                } else {
                    m_shadowbox.SetDrawingPlane( m_crvNetwork.GetCurve(m_iCursorOverCurve)(m_dCursorOverCurve), m_vecAlignCameraLook );
					if ( !g_drawState.m_opUI->m_bShowDrawingPlane->value() ) {
						g_drawState.m_opUI->m_bShowDrawingPlane->value(1);
					}
                }
			}
			bChangedCurves = false;
            m_bCheckConsistency = false;
			break;
        case DRAWPLANE_DRAG :
			bChangedCurves = false;
			bPopAction = true;
            m_bCheckConsistency = false;
            break;
        case DRAWPLANE_ROTATE :
            if ( m_bIsTap ) {
                g_drawState.CenterCameraOnDrawingPlane( m_crvNetwork.BoundingBox() );
				bChangedCamera = true;
			}
			bChangedCurves = false;
            bPopAction = true;
            m_bCheckConsistency = false;
            break;
        case DRAWING :
            PushUndo( m_crvNetwork.NumCrvs(), StrokeHistory::FIRST_STROKE );
            if ( s_bTrace ) cout << "\nProcess curve\n";
            
            if ( g_drawState.m_opUI->m_bShowInflationSrf->value() && m_srfInflation.IsValid() ) {
                const Curve crv = m_srfInflation.DrawOnSurface( m_curveTempScreen);
                if ( crv.NumPts() ) {
                    m_crvNetwork.NewCompositeCurve( m_curveTempScreen, crv );
                }
            } else {
                m_crvNetwork.ProcessCurve( m_curveTempScreen );
            }
                
            m_pushAction = NO_ACTION;
            break;
            
        case MENU :
            cerr << "ERR: Mouse Up, shouldn't be here\n";
            PrintState();
			bChangedCurves = false;
            break;
            
        case EXTRUSION_SURFACE :
            if ( m_bIsTap ) {
				// If click is on curve, bring up the arrows for extrusion surface
				// OR, if click not over anything, go back to no inflation surface editing
                // pop up the arrows
                int iCursorOverCurve = -1;
                double dCursorOverCurve = 0.0;
                if ( m_srfInflation.IsSelectedLeftCurve( m_iCursorOverInflationSrf ) ) {
                    m_action = NO_ACTION;
                    g_drawState.m_opUI->m_bShowExtrusionSrf->value(0);
                } else if ( CurveSelected( m_rayDown, iCursorOverCurve, dCursorOverCurve ) < g_drawState.SelectionSize() ) {
                    SetCurveSelected( iCursorOverCurve, dCursorOverCurve );
                    m_action = DRAWPLANE_ALIGN;
                    CreateAxis( m_ptScreenDown, m_rayDown );
                    m_inflationSrfEditAction = InflationSurface::DRAW_CURVE;
                }

            } else {
				// Draw curves
                const Curve crv = m_srfInflation.DrawOnSurface( m_curveTempScreen);
                if ( crv.NumPts() ) {
                    m_crvNetwork.NewCompositeCurve( m_curveTempScreen, crv );
                }
            }
            if ( m_action != NO_ACTION ) {
                bResetState = false;
            }
            break;
        case INFLATION_SURFACE :
            if ( m_bIsTap ) {
				// In inflation surface mode tap can select which curve (left-right) to edit, or go back to drawing on surface
				// OR, if click not over anything, go back to no inflation surface editing
                if ( m_srfInflation.IsSelectedLeftCurve( m_iCursorOverInflationSrf ) ) {
                    m_inflationSrfEditAction = InflationSurface::EDIT_LEFT_CURVE;
                } else if ( m_srfInflation.IsSelectedRightCurve( m_iCursorOverInflationSrf ) ) {
                    m_inflationSrfEditAction = InflationSurface::EDIT_RIGHT_CURVE;
                } else if ( m_srfInflation.IsSelectedSurface( m_iCursorOverInflationSrf ) ) {
                    m_inflationSrfEditAction = InflationSurface::DRAW_CURVE;
                }
            } else {
				// Automatically go to the next mode - left curve, then right curve, then draw curves
                switch ( m_inflationSrfEditAction ) {
                    case InflationSurface::NO_EDIT :
                        break;
                    case InflationSurface::EDIT_LEFT_CURVE :
                        m_srfInflation.CreateBoundaryCurve( m_crvNetwork, m_curveTempScreen, true );
                        m_inflationSrfEditAction = InflationSurface::EDIT_RIGHT_CURVE;
                        break;
                    case InflationSurface::EDIT_RIGHT_CURVE :
                        m_srfInflation.CreateBoundaryCurve( m_crvNetwork, m_curveTempScreen, false );
                        m_inflationSrfEditAction = InflationSurface::DRAW_CURVE;
                        break;
                    case InflationSurface::DRAW_CURVE :
                        const Curve crv = m_srfInflation.DrawOnSurface( m_curveTempScreen);
                        if ( crv.NumPts() ) {
                            m_crvNetwork.NewCompositeCurve( m_curveTempScreen, crv );
                        }
                        break;
                }
            }
            if ( m_action != NO_ACTION ) {
                bResetState = false;
            }
            break;
            
        case NO_ACTION :
            //probably a tap
            if ( m_bIsTap ) {
                if ( s_bTrace ) PrintMenuState();
				
                bResetMenu = false;
                bResetState = false;
                
                m_ptMenuDown = m_ptScreenDown;
				m_rayMenuDown = m_rayDown;
                
				/* Make sure if you add another selection to add it to this boolean */
				const bool bOtherSelection = OverCurve() || 
											m_srfData.IsValidIntersection(m_iCursorOverIntersection) || 
											m_opCursorOverCurveNormalConstraint != NULL ||
											m_opCursorOverCurvePointConstraint != NULL ||
											m_bCursorOverLastStroke || m_bCursorOverLastSmooth || m_iCursorOverShadowBox != -1 ||
                                            m_iCursorOverInflationSrf != -1;
                
                if ( in_eventModifier & FL_SHIFT || bOtherSelection == false || m_pushAction == DRAG_CURVE ) {
                    if ( s_bTrace ) cout << "Camera menu\n";
                    m_menuWhich = MENU_CAMERA;
                    m_menuSelection = MENU_CAMERA_ALIGN;
                    bChangedCurves = false;
					
                } else if ( m_bCursorOverLastStroke ) {
                    if ( s_bTrace ) cout << "Stroke menu\n";
                    m_menuWhich = MENU_LAST_STROKE;
                    m_menuSelection = MENU_LAST_STROKE_NEW_CURVE;
                    
				} else if ( m_bCursorOverLastSmooth ) {
					// Done smoothing - clear state
					bResetMenu = true;
					bResetState = true;
					
                } else if ( m_iCursorOverInflationSrf != -1 ) {
                    m_action = INFLATION_SURFACE;
					bResetMenu = true;
                    bResetState = false;
					// Automatically go to the selected mode - left curve, right curve, or draw curves
                    if ( m_srfInflation.IsSelectedSurface( m_iCursorOverInflationSrf ) ) {
                        m_inflationSrfEditAction = InflationSurface::DRAW_CURVE;
                    } else if ( m_srfInflation.IsSelectedLeftCurve( m_iCursorOverInflationSrf ) ) {
                        m_inflationSrfEditAction = InflationSurface::EDIT_LEFT_CURVE;
                        bChangedCurves = false;
                    } else if ( m_srfInflation.IsSelectedRightCurve( m_iCursorOverInflationSrf ) ) {
                        m_inflationSrfEditAction = InflationSurface::EDIT_RIGHT_CURVE;
                        bChangedCurves = false;
                    } 
                } else if ( m_iCursorOverIntersection != -1 ) {
					PushUndo( -1, StrokeHistory::CHANGED_CONSTRAINT );
					
					if ( s_bTrace ) cout << "Fixing intersection\n";
                    m_srfData.FixIntersection( m_crvNetwork, m_iCursorOverIntersection );
					bResetMenu = true;
					bResetState = true;
					
                } else if ( m_iCursorOverShadowBox != -1 ) {
					if ( m_shadowbox.IsCenterDrawPlane( m_iCursorOverShadowBox ) ) {
						m_shadowbox.ResetDrawingPlane();
						bResetMenu = true;
						bResetState = true;
					} else if ( m_shadowbox.IsShowHideDrawPlane( m_iCursorOverShadowBox ) ) {
						g_drawState.m_opUI->m_bShowDrawingPlane->value( !g_drawState.m_opUI->m_bShowDrawingPlane->value() );
						LogGUI();
						bResetMenu = true;
						bResetState = true;
					}
                    bChangedCurves = false;
				} else if ( OverCurve() || m_opCursorOverCurveNormalConstraint != NULL || m_opCursorOverCurvePointConstraint != NULL ) {
                    SetCursorOverTs();
                    if ( s_bTrace ) cout << "Curve menu\n";
                    m_menuWhich = MENU_CURVE;
                    m_menuSelection = MENU_CURVE_DRAG;
                    bChangedCurves = false;
                }				
                
				if ( bResetMenu == false ) {
					m_action = MENU;
				}
            } else {
                cerr << "ERR: Mouse up, no action but not tap\n";
            }
            break;
        default :
            cerr << "ERR: Mouse up, unknown action " << m_action << "\n";
			PrintState();
            bChangedCurves = false;
            break;
    }
    
    if ( bResetState == true ) {
        m_action = bPopAction ? m_pushAction : NO_ACTION;
        if ( bPopAction ) {
            m_pushAction = NO_ACTION;
        } else {
			ClearCurveSelected();
			SetSelectionState( m_ptScreenLast, m_rayLast );
		}
		if ( s_bTrace && bPopAction == true ) {
			cout << "Popping action\n";
		}
    }

	if ( bChangedCurves ) {
        m_srfData.RemoveInvalidAndFixed( m_crvNetwork );
	}
	
    if ( bResetMenu == true ) {
        m_menuWhich = NO_MENU_SELECTED;
        m_menuSelection = NO_MENU_SELECTED;
    }
    /*
   
    m_curveTempScreen.FillGaps( m_curveTempScreen.Length() / ( m_curveTempScreen.NumPts() + 1.0 ));
    m_curveTempScreen.SmoothEnds( 3);
    m_curveTempScreen.Smooth( 3 );
    
    m_curveTempScreen.MakeUnitLength( false );
    
    ofstream out( "/Users/cindygrimm/Dropbox/Curves/kShape.txt", ios::out );
    
    for ( int i = 0; i < m_curveTempScreen.NumPts(); i++ ) {
        out << m_curveTempScreen.PercAlong(i) << " ";
    }
    out << "\n";
    double dSign = 1.0;
    for ( int i = 0; i < m_curveTempScreen.NumPts(); i++ ) {
        if ( i > 0 && i < m_curveTempScreen.NumPts()-1 ) {
            const R2Vec v1 = m_curveTempScreen.Pt( i+1) - m_curveTempScreen.Pt(i);
            const R2Vec v2 = m_curveTempScreen.Pt( i ) - m_curveTempScreen.Pt(i-1);
            const double dCross = v1[1] * v2[0] - v1[0] * v2[1];
            dSign = dCross < 0.0 ? 1.0 : -1.0;
        } else {
            dSign = 1.0;
        }
        out << dSign * m_curveTempScreen.Curvature(i) << " ";
    }
    out << "\n";
    for ( int i = 0; i < m_curveTempScreen.NumPts(); i++ ) {
        out << m_curveTempScreen.Pt(i)[0] << " ";
    }
    out << "\n";
    for ( int i = 0; i < m_curveTempScreen.NumPts(); i++ ) {
        out << m_curveTempScreen.Pt(i)[1] << " ";
    }
    out << "\n";
    
    out.close();
 */
    m_curveTempScreen.Clear();
    
	if ( s_bTrace ) {
		cout << "Mouse up: ";
		PrintState();
	}
    if ( m_outLogFile.good() ) {
        m_outLogFile << "MouseUp " << in_eventModifier << "\n";
		if ( bChangedCurves ) {
			LogFileFlush(); // Write curves out
		} else {
			m_outLogFile.flush();
		}
    }
	
	m_bIsTap = true;
	const int iCursor = ( OverCurve() && (m_menuWhich == MENU_CAMERA || m_menuWhich == MENU_CURVE ) ) ? m_iCursorOverCurve : -1;
	m_crvNetwork.UpdateCurveState( iCursor );
	UpdateGUIState();
}

// There are times when it's handy to know if the mouse has gone out of focus
void ActionEvents::SetFocus() 
{
    m_bInFocus = true; 
    
    if ( m_outLogFile.good() )
        m_outLogFile << "Focus t\n";
}
///
void ActionEvents::SetUnfocus() 
{ 
    m_bInFocus = false; 
    
    if ( m_outLogFile.good() )
        m_outLogFile << "Focus f\n";
}


